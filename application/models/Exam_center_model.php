<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class : Login_model (Login Model)
 * Login model class to get to authenticate user credentials 
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class Exam_center_model extends CI_Model
{
    
    
    function addExamStart($data)
    {
        $this->db->trans_start();
        $this->db->insert('exam_center_start_exam', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }


    function getAllExamEventList($currenttime) {
         $this->db->select('e.*');
         $this->db->from('exam_event as e');
         $this->db->where('e.exam_date', date('Y-m-d',strtotime($currenttime)));
         $this->db->where('e.from_tm', date('H:i',strtotime($currenttime)));
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

   
}

?>