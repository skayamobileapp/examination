<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class : Login_model (Login Model)
 * Login model class to get to authenticate user credentials 
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class Student_login_model extends CI_Model
{
    
    function loginStudent($email, $password)
    {
        $this->db->select('stu.*');
        $this->db->from('student as stu');
        $this->db->where('stu.email_id', $email);
        // $this->db->where('stu.applicant_status', 'Approved');
        // $this->db->where('stu.isDeleted', 0);
        $query = $this->db->get();
        
        $user = $query->row();
        
        return $user;
    }


    function checktokenlogin($token){
        $currentDate = date('Y-m-d');
         $this->db->select('b.*');
        $this->db->from('exam_student_tagging as a');
        $this->db->join('student as b', 'a.id_student = b.id');
        $this->db->join('exam_event as c', 'a.id_exam_event = c.id');
        $this->db->where('a.token', $token);
        $this->db->where('c.exam_date', $currentDate);
         $query = $this->db->get();
         $result = $query->row();
         // print_r($result);exit();      
         return $result;
    }


    function getToken($moduleid,$assmentid) {
        $mqmstype = "CPIF";
        $this->db->select('b.token');
        $this->db->from('exam_student_tagging as b');
        $this->db->join('exam_event as a', 'b.id_exam_event = a.id');
        $this->db->where('b.mqms_user_module_id', $moduleid);
        $this->db->where('b.mqms_assessment_component_id', $assmentid);
        $this->db->where('a.mqms_type', $mqmstype);
         $query = $this->db->get();
         $result = $query->row();
         // print_r($result);exit();      
         return $result;
    }

     function getMasterClassToken($moduleid,$assmentid) {
        $mqmstype = "OTHERS";
        $this->db->select('b.token');
        $this->db->from('exam_student_tagging as b');
        $this->db->join('exam_event as a', 'b.id_exam_event = a.id');
        $this->db->where('b.mqms_user_module_id', $moduleid);
        $this->db->where('b.mqms_assessment_component_id', $assmentid);
        $this->db->where('a.mqms_type', $mqmstype);
         $query = $this->db->get();
         $result = $query->row();
         // print_r($result);exit();      
         return $result;
    }

    function studentLastLoginInfo($id_student)
    {
        $this->db->select('BaseTbl.created_dt_tm');
        $this->db->where('BaseTbl.id_student', $id_student);
        $this->db->order_by('BaseTbl.id', 'DESC');
        $this->db->limit(1);
        $query = $this->db->get('student_last_login as BaseTbl');

        return $query->row();
    }


    function checkStudentEmailExist($email)
    {
        $this->db->select('userId');
        $this->db->where('email', $email);
        $this->db->where('isDeleted', 0);
        $query = $this->db->get('student');

        if ($query->num_rows() > 0){
            return true;
        } else {
            return false;
        }
    }

    function resetPasswordUser($data)
    {
        $result = $this->db->insert('tbl_reset_password', $data);

        if($result) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    function getStudentInfoByEmail($email)
    {
        $this->db->select('userId, email, name');
        $this->db->from('student');
        $this->db->where('isDeleted', 0);
        $this->db->where('email', $email);
        $query = $this->db->get();

        return $query->row();
    }

    function checkActivationDetails($email, $activation_id)
    {
        $this->db->select('id');
        $this->db->from('tbl_reset_password');
        $this->db->where('email', $email);
        $this->db->where('activation_id', $activation_id);
        $query = $this->db->get();
        return $query->num_rows();
    }

    function createPasswordUser($email, $password)
    {
        $this->db->where('email', $email);
        $this->db->where('isDeleted', 0);
        $this->db->update('tbl_users', array('password'=>getHashedPassword($password)));
        $this->db->delete('tbl_reset_password', array('email'=>$email));
    }

    function addStudentLastLogin($loginInfo)
    {
        $this->db->trans_start();
        $this->db->insert('student_last_login', $loginInfo);
        $this->db->trans_complete();
    }
}

?>