<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Grader_student_tag_model extends CI_Model
{
    function salutationListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('salutation_setup');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }


    function getIdExamStudentTagging($idstudent,$idexamevent) {
          $this->db->select('est.*');
        // , qs.short_name as qualification_code, qs.name as qualification_name
        $this->db->from('exam_student_tagging as est');

        $this->db->where('est.id_student', $idstudent);
        $this->db->where('est.id_exam_event', $idexamevent);
       
        $query = $this->db->get();
        $results = $query->row(); 
        return $results;

    }

    function examCenterListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('exam_center');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function examCenterListByGraderId($id_grader)
    {
        $this->db->select('DISTINCT(gm.id_exam_center) as id_exam_center');
        $this->db->from('grader_exam as gm');
        $this->db->join('exam_center as e','gm.id_exam_center = e.id');
        $this->db->join('exam_event as ee','gm.id_exam_event = ee.id');
        $this->db->where('gm.id_grader', $id_grader);
        // $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $results = $query->result();  
            
        $details = array();

        foreach ($results as $result)
        {
            $id_exam_center = $result->id_exam_center;

            $exam_center = $this->getExamCenter($id_exam_center);

            if($exam_center)
            {
                array_push($details, $exam_center);
            }
        }

        return $details;
    }

    function getExamEventByExamCenter($data)
    {
        $this->db->select('DISTINCT(gm.id_exam_event) as id_exam_event');
        $this->db->from('grader_exam as gm');
        $this->db->join('exam_center as e','gm.id_exam_center = e.id');
        $this->db->join('exam_event as ee','gm.id_exam_event = ee.id');
        $this->db->where('gm.id_grader', $data['id_grader']);
        $this->db->where('gm.id_exam_center', $data['id_exam_center']);
        // $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $results = $query->result(); 

        $details = array();

        foreach ($results as $result)
        {
            $id_exam_event = $result->id_exam_event;

            $exam_event = $this->getExamEvent($id_exam_event);

            if($exam_event)
            {
                array_push($details, $exam_event);
            }
        }

        return $details;
    }

    function getExamCenter($id)
    {
        $this->db->select('*');
        $this->db->from('exam_center');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getGraderExam($id)
    {
        $this->db->select('gm.*, ee.name as exam_event_name, ee.exam_date as exam_event_date,g.full_name as gradername,es.exam_sitting_name as examsittingname');
        $this->db->from('grader_exam as gm');
        $this->db->join('exam_event as ee','gm.id_exam_event = ee.id');
        $this->db->join('exam_sitting as es','ee.id_exam_sitting = es.id');
        $this->db->join('grader as g','g.id = gm.id_grader');
        $this->db->where('gm.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getExamEvent($id)
    {
        $this->db->select('*');
        $this->db->from('exam_event');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function graderStudentListSearch($data)
    {
        $this->db->select('a.*');
        $this->db->from('grader as a');
        if ($data['name'] != '')
        {
            $likeCriteria = "(a.full_name  LIKE '%" . $data['name'] . "%' or a.nric  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("a.full_name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }
    
    function getGrader($id)
    {
        $this->db->select('*');
        $this->db->from('grader');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }


    function getGraderStudentTagList($id_grader_exam)
    {
        $this->db->select('gm.*, s.full_name as student_name, s.nric,s.batch,s.membership_number, ee.name as exam_event_name, ee.exam_date as exam_event_date,c.name as coursename,c.code as coursecode,gm.id as graderexamid');
        $this->db->from('grader_student_tag as gm');
        $this->db->join('student as s','gm.id_student = s.id');
        $this->db->join('exam_event as ee','gm.id_exam_event = ee.id');

        $this->db->join('examset as es','ee.id_exam_set = es.id');
        $this->db->join('course as c','es.id_course = c.id');


        $this->db->where('gm.id_grader_exam', $id_grader_exam);
        // $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }
    
    function addNewGraderStudentTag($data)
    {
        $this->db->trans_start();
        $this->db->insert('grader_student_tag', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function deleteGraderModule($id)
    {
        $this->db->where('id', $id);
        $result = $this->db->delete('grader_modules');
        return $result;
    }

    function studentSearch($data)
    {
        $this->db->select('DISTINCT(s.id) as id_student');
        // , qs.short_name as qualification_code, qs.name as qualification_name
        $this->db->from('exam_student_tagging as est');
        $this->db->join('student as s', 'est.id_student = s.id');
        $this->db->join('exam_event as ee', 'est.id_exam_event = ee.id');
        if ($data['full_name'] != '')
        {
            $likeCriteria = "(s.full_name  LIKE '%" . $data['full_name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['email_id'] != '')
        {
            $likeCriteria = "(s.full_name  LIKE '%" . $data['email_id'] . "%')";
            $this->db->where($likeCriteria);
        }
        
        if ($data['id_exam_event'] != '')
        {
            $this->db->where('est.id_exam_event', $data['id_exam_event']);
        }
       
        
        // if ($data['id_intake'] != '')
        // {
        //     $this->db->where('s.id_intake', $data['id_intake']);
        // }
        $query = $this->db->get();
        $results = $query->result(); 

        // echo "<Pre>"; print_r($this->db);exit();

        $details = array();

        foreach ($results as $result)
        {
            $id_student = $result->id_student;

            $student = $this->getStudent($id_student);

            if($student)
            {
                array_push($details, $student);
            }
        }

        return $details;

    }

    function getStudent($id)
    {
        $this->db->select('*');
        $this->db->from('student');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }


     function getStudentDetailsbyidexamstudenttagging($id)
    {
        $this->db->select('*');
        $this->db->from('exam_student_tagging');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }



    
}
