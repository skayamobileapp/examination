<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Exam_api_model extends CI_Model
{
    
    function getIMQSCourseId($id) {
         $this->db->select('*');
        $this->db->from('course');
        $this->db->where('mqms_id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function deleteexamevent($moduleId,$examdate) {
     
     $this->db->where('exam_date', $examdate);
     $this->db->where('id_course', $moduleId);
     
        $result = $this->db->delete('exam_event');
        return $result;
        
    }

    function checkExamAlreayExist($type,$scheduleId){
         $this->db->select('*');
        $this->db->from('exam_event');
        $this->db->where('mqms_schedule_id', $scheduleId);
        $this->db->where('mqms_type', $type);

                 $query = $this->db->get();
        return $query->row();
    }



    
    function deleteOlData() {
        
        $this->db->query("TRUNCATE `exam_center_start_exam`");
        $this->db->query("TRUNCATE `exam_student_tagging`");
        $this->db->query("TRUNCATE `exam_student_tagging_lock`");
        $this->db->query("TRUNCATE `grader_student_tag`");
        $this->db->query("TRUNCATE `student`");
        $this->db->query("TRUNCATE `student_exam_attempt`");
        $this->db->query("TRUNCATE `student_question_set`");
        $this->db->query("TRUNCATE `student_question_set_marks`");
        $this->db->query("TRUNCATE `exam_event`");
        
    }
    
    
    function getmarks($scheduleId) {
          $this->db->select('*');
        $this->db->from('exam_student_tagging');
        $this->db->where('mqm_schedule_id', $scheduleId);

                 $query = $this->db->get();
         $result = $query->result();


        return $result;
        
    }
    
    
      function deletemqmsid($usermqmsid) {
     
     $this->db->where('mqms_user_module_id', $usermqmsid);
     
        $result = $this->db->delete('exam_student_tagging');
        return $result;
        
    }
    
    
    function addModule($data){
         $this->db->trans_start();
        $this->db->insert('course', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }
   
   function addNewCourse($data)
    {
        $this->db->trans_start();
        $this->db->insert('course', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }


    function editCourse($data, $id)
    {
        $this->db->where('id', $id);
        $result = $this->db->update('course', $data);
        return $result;
    }
    
    
      function editExamCenterName($name)
    {
        
         $this->db->select('*');
        $this->db->from('exam_center');
        $this->db->where('name', $name);
        $query = $this->db->get();
        return $query->row();
       
    }
    
    
    
    function updateUser($data,$id){
          $this->db->where('id', $id);
        $result = $this->db->update('student', $data);

        return $result;
    }

    function updateExamEvent($data,$id){
          $this->db->where('id', $id);
        $result = $this->db->update('exam_event', $data);

        return $result;
    }
    
    
    
    function getIMQSExamCenterId($id) {
         $this->db->select('*');
        $this->db->from('exam_center');
        $this->db->where('mqms_id', $id);
        $query = $this->db->get();
        return $query->row();
    }
   
    
     function addExamCenter($data)
    {
        $this->db->trans_start();
        $this->db->insert('exam_center', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }



    function editExamCenter($data, $id)
    {
        $this->db->where('id', $id);
        $result = $this->db->update('exam_center', $data);
        return $result;
    }



     function checkuser($id,$idexamevent) {


         $this->db->select('s.*');
        $this->db->from('student as s');
        $this->db->join('exam_student_tagging as ee','s.id = ee.id_exam_event');
        $this->db->where('s.email_id', $id);
        $this->db->where('ee.id_exam_event', $idexamevent);
        $query = $this->db->get();
        return $query->row();

    }

     function addNewExamUser($data)
    {
        $this->db->trans_start();
        $this->db->insert('student', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }
    
    function getExamSetDetailsByCourseId($id){
          $this->db->select('*');
        $this->db->from('examset as e');
        $this->db->where('e.id_course', $id);
        $this->db->where("e.status='1'");
        
        $query = $this->db->get();
                return $query->row();
    }
    
    function deleteStudents($examevetId) {
     
       
     $this->db->where('id_exam_event', $examevetId);
     $result = $this->db->delete('exam_student_tagging');
        return $result;
        
        
    }
    
    
    function deleteStudentsExamEvent($idexamevent) {
        
        $this->db->query('Delete from exam_student_tagging where id_exam_event not in (select id from exam_event)');
    }
    
    
    
    
     function getExamEventIdAPI($date,$idcourse) {
        
         $this->db->select('e.*');
        $this->db->from('exam_event as e');
        $this->db->where('e.id_course', $idcourse);
        $this->db->where('e.exam_date=', $date);

        $query = $this->db->get();
                return $query->row();
        
        
    }
    
    
    function getExamEventId($date,$enddate, $examcenterid,$idcourse) {
        
         $this->db->select('e.*');
        $this->db->from('exam_event as e');
        $this->db->join('examset as t','t.id = e.id_exam_set');
        $this->db->where('e.id_exam_center', $examcenterid);
        $this->db->where('t.id_course', $idcourse);
        $this->db->where('date(e.exam_date)>=', $date);
        $this->db->where('date(e.exam_end_date)<=', $enddate);

        $query = $this->db->get();
                return $query->row();
        
        
    }
    
    
    function checkeventcreated($student,$exameventi) {
          $this->db->select('*');
        $this->db->from('exam_student_tagging');
        $this->db->where('id_student', $student);
        $this->db->where('id_exam_event', $exameventi);
        
        $query = $this->db->get();
        return $query->row();
    }
    
    
      function addExamEvent($data)
    {
        $this->db->trans_start();
        $this->db->insert('exam_student_tagging', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }


    function addNewExamEvent($data){
         $this->db->trans_start();
        $this->db->insert('exam_event', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }
    
    function addNewExamName($datas){
         $this->db->trans_start();
        $this->db->insert('exam_name', $datas);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }



    
}

