<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Grader_exam_model extends CI_Model
{
    function salutationListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('salutation_setup');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }


    function getGraderAssigedDetails($idgraderexam) {
          $this->db->select('*');
        $this->db->from('grader_exam as a');
        $this->db->where('id', $idgraderexam);
        $query = $this->db->get();
        $result = $query->row();
        return $result;
    }

    function getAssignedGraderDetails($id){

         $this->db->select('*');
        $this->db->from('grader_student_tag as a');
        $this->db->where('id', $id);
        $query = $this->db->get();
        $result = $query->row();
        return $result;
        
    }


 function getCountOfQuestionsByGraderExamId($id) {
         $this->db->select('count(a.id) as totalcount');
        $this->db->from('grader_student_tag as a');
        $this->db->join('exam_student_tagging as g','g.id = a.id_exam_student_tagging');
        $this->db->where('g.final_total','>',0);
        $this->db->where("g.result_status='15'");

        $this->db->where('a.id_grader_exam', $id);
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function getpendingCountStatus($id){
        $this->db->select('count(a.id) as totalcount');
        $this->db->from('grader_student_tag as a');
        $this->db->join('exam_student_tagging as g','g.id = a.id_exam_student_tagging');
        $this->db->where('a.id_grader_exam', $id);
        $this->db->where('g.final_total','>',0);        
        $this->db->where("g.result_status='15'");
        $query = $this->db->get();
         $str = $this->db->last_query();
        $result = $query->result();
        return $result;
    }

function getCountOfQuestionsByGraderExamIdStatus($id) {
         $this->db->select(' count(a.id) as totalcount');
        $this->db->from('grader_student_tag as a');
        $this->db->where('id_grader_exam', $id);
        $this->db->where("status='1'");
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }


    function getCountOfQuestions($id) {
         $this->db->select(' count(a.id) as totalcount');
        $this->db->from('exam_student_tagging as a');
        $this->db->where('id_exam_event', $id);
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function getGraderCount($id){
         $this->db->select('a.*,g.full_name as gradername');
        $this->db->from('grader_exam as a');
        $this->db->join('grader as g','g.id = a.id_grader');
        $this->db->where('id_exam_event', $id);
        $query = $this->db->get();
        $result = $query->result();
        return $result;
        
    }

    function getGraderStudentCount($id){
        
         $this->db->select('a.*,est.essay_status,est.result_status');
        $this->db->from('grader_student_tag as a');
        $this->db->join('exam_student_tagging as est','est.id_student = a.id_student');
        $this->db->where('id_grader_exam', $id);
        $query = $this->db->get();
        $result = $query->result();
        return $result;

    }

    function listofExamEvent($formData){
        $this->db->select('a.*,c.name as coursename,c.code as coursecode,exs.exam_sitting_name');
        $this->db->from('exam_event as a');
        $this->db->join('examset as es','a.id_exam_set = es.id');
        $this->db->join('exam_sitting as exs','a.id_exam_sitting = exs.id');
        $this->db->join('course as c','es.id_course = c.id');
        $this->db->order_by("a.exam_date", "ASC");

        if($formData['id_exam_sitting']>0) {
        $this->db->where('exs.id', $formData['id_exam_sitting']);

        }

          if($formData['exam_event_name']!='') {
        $this->db->where("a.name like '%$formData[exam_event_name]%'");

        }
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

      function getExamEventById($id){
        $this->db->select('a.*,c.name as coursename,c.code as coursecode');
        $this->db->from('exam_event as a');
        $this->db->join('examset as es','a.id_exam_set = es.id');
        $this->db->join('course as c','es.id_course = c.id');
        $this->db->where('a.id', $id);
        $query = $this->db->get();
        $result = $query->row();
        return $result;
    }

    function examEventListSearch() {
         $this->db->select('a.*');
        $this->db->from('exam_event as a');
       
        $this->db->order_by("a.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;

    }

    function examCenterListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('exam_center');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getStudentList(){
        $this->db->select('*');
        $this->db->from('student');
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }


    function graderListByEvent($id_event)
    {
        $this->db->select('g.*');
        $this->db->from('grader as g');
        $this->db->join('grader_modules as gm','gm.id_grader = g.id');
        $this->db->join('exam_event as ee','gm.id_course = ee.id_course');

        $this->db->where('ee.id', $id_event);
        $this->db->order_by("full_name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }


     function graderAllListByEvent()
    {
        $this->db->select('g.*');
        $this->db->from('grader as g');
        $this->db->order_by("full_name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function graderListByStatus($id_course)
    {
        $this->db->select('g.*');
        $this->db->from('grader as g');
        $this->db->join('grader_modules as gm','gm.id_grader = g.id');

        $this->db->where('gm.id_course', $id_course);
        $this->db->order_by("full_name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function graderExamListSearch($data)
    {
        $this->db->select('g.*, gm.id, ee.name as exam_event_name, ee.exam_date as exam_event_date');
        $this->db->from('grader_exam as gm');
        $this->db->join('grader as g','gm.id_grader = g.id');
        $this->db->join('exam_event as ee','gm.id_exam_event = ee.id');
        if ($data['name'] != '')
        {
            $likeCriteria = "(g.full_name  LIKE '%" . $data['name'] . "%' or g.nric  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['id_exam_center'] != '')
        {
            $this->db->where('ee.id_exam_center', $data['id_exam_center']);
        }
        
        $this->db->order_by("g.full_name", "ASC");
         $query = $this->db->get();
         $results = $query->result();

         $details = array();

         foreach ($results as $result)
         {
            $id_grader_exam = $result->id;

            $student_count = $this->getTaggedStudentCount($id_grader_exam);

            $result->student_count = $student_count;

            array_push($details, $result);
         }
         return $details;
    }
    
    function getGrader($id)
    {
        $this->db->select('*');
        $this->db->from('grader');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function checkDuplicateGraderExamEntry($data)
    {
        $this->db->select('*');
        $this->db->from('grader_exam');
        $this->db->where('id_exam_center', $data['id_exam_center']);
        $this->db->where('id_exam_event', $data['id_exam_event']);
        $this->db->where('id_grader', $data['id_grader']);
        $query = $this->db->get();
        return $query->row();
    }

    function getTaggedStudentCount($id)
    {
        $this->db->select('distinct(id_student) as id_student');
        $this->db->from('grader_student_tag');
        $this->db->where('id_grader_exam', $id);
        $query = $this->db->get();
        return $query->num_rows();
    }

    function getGraderExamList($id_grader)
    {
        $this->db->select('gm.*, e.name as exam_center_name, ee.name as exam_event_name, ee.exam_date as exam_event_date');
        $this->db->from('grader_exam as gm');
        $this->db->join('exam_center as e','gm.id_exam_center = e.id');
        $this->db->join('exam_event as ee','gm.id_exam_event = ee.id');
        $this->db->where('gm.id_grader', $id_grader);
        // $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

     function editGraderExam($data, $id)
    {
        $this->db->where('id', $id);
        $result = $this->db->update('grader_exam', $data);
        return $result;
    }
    function deleteStudent($id) {
        $this->db->where('id', $id);
        $result = $this->db->delete('grader_student_tag');
        return $result;

    }

    function deletegraderexam($id) {
        $this->db->where('id', $id);
        $result = $this->db->delete('grader_exam');
        return $result;
    }

    function checkstudentForGraderExam($id) {
         $this->db->select('ee.*');
        $this->db->from('grader_student_tag as ee');
        $this->db->where('ee.id_grader_exam', $id);
        // $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getStudentDetails($id) {
        $this->db->select('ee.*');
        $this->db->from('grader_student_tag as ee');
        $this->db->where('ee.id', $id);
        // $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->row();  
        return $result;
    }

    function getExamStudentTagging($id){
         $this->db->select('ee.*');
        $this->db->from('exam_student_tagging as ee');
        $this->db->where('ee.id', $id);
        // $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->row();  
        return $result;
    }


    function getgraderDetails($id) {
          $this->db->select('ee.*');
        $this->db->from('grader_exam as ee');
        $this->db->where('ee.id', $id);
        // $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->row();  
        return $result;
    }



    function getExamSittingByStudentId($std,$idexamevent) {
         $this->db->select('ee.*');
        $this->db->from('exam_student_tagging as ee');

        $this->db->where('ee.id_exam_event', $idexamevent);
        $this->db->where('ee.id_student', $std);
        // $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->row();  
        return $result;
    }


    function checkgraderalreadyassigntoexamevent($idgrader,$idexamevent) {
          $this->db->select('ee.*');
        $this->db->from('grader_exam as ee');

        $this->db->where('ee.id_exam_event', $idexamevent);
        $this->db->where('ee.id_grader', $idgrader);
        // $this->db->where('ee.end_date', $enddate);
        $query = $this->db->get();
        $result = $query->row();  
        return $result;
    }
    
    function addNewGraderExam($data)
    {
        $this->db->trans_start();
        $this->db->insert('grader_exam', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function deleteGraderModule($id)
    {
        $this->db->where('id', $id);
        $result = $this->db->delete('grader_modules');
        return $result;
    }

    function getExamEventByExamCenter($id_exam_center)
    {
        $this->db->select('ee.*');
        $this->db->from('exam_event as ee');
        $this->db->where('ee.id_exam_center', $id_exam_center);
        $this->db->where('ee.status', '1');
        // $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result();  
        return $result;
    }


    function fetchStudentByExameventId($idexamevent)
    {
        $this->db->select('ee.*,s.full_name,s.email_id,s.nric,s.batch,s.membership_number');
        $this->db->from('exam_student_tagging as ee');
        $this->db->join('student as s','ee.id_student = s.id');

        $this->db->where('ee.id_exam_event', $idexamevent);

        $this->db->where("s.id not in ( select id_student from grader_student_tag
         where id_exam_event='$idexamevent')");
        $query = $this->db->get();
        $result = $query->result();  
        return $result;
    }
}