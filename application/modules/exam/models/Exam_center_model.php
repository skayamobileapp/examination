<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Exam_center_model extends CI_Model
{
    function examCenterList()
    {
        $this->db->select('a.*, b.name as stateName, c.name as countryName, ecl.name as location');
        $this->db->from('exam_center as a');
        $this->db->join('state as b', 'a.id_state = b.id');
        $this->db->join('country as c', 'a.id_country = c.id');
        $this->db->join('exam_center_location as ecl', 'a.id_location = ecl.id');
        $this->db->order_by("a.id", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();      
         return $result;
    }




  function editExamAbsent($data,$id) {
         $this->db->where('id', $id);
        $result = $this->db->update('exam_absent', $data);
        return $result;
    }

    




 function getExamAbsent(){
        $id = 1;
        $this->db->select('*');
        $this->db->from('exam_absent');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }



    function getlimit(){
        $id = 1;
        $this->db->select('*');
        $this->db->from('exam_set');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }

     function getlimitLogout(){
        $id = 1;
        $this->db->select('*');
        $this->db->from('exam_logout');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function examCenterLocationListForRegistration()
    {
        $this->db->select('a.*, ecl.name as location');
        $this->db->from('exam_center as a');
        $this->db->join('state as b', 'a.id_state = b.id');
        $this->db->join('country as c', 'a.id_country = c.id');
        $this->db->join('exam_center_location as ecl', 'a.id_location = ecl.id');
        $this->db->order_by("a.id", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();      
         return $result;
    }


    function examCenterListSearch($data)
    {
        // $date = 
        $this->db->select('a.*, b.name as stateName, c.name as countryName, ecl.name as location');
        $this->db->from('exam_center as a');
        $this->db->join('state as b', 'a.id_state = b.id');
        $this->db->join('country as c', 'a.id_country = c.id');
        $this->db->join('exam_center_location as ecl', 'a.id_location = ecl.id');
        if ($data['name'] != '')
        {
            $likeCriteria = "(name  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['id_location'] != '')
        {
            $this->db->where('a.id_location', $data['id_location']);
        }
        $this->db->order_by("a.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getExamCenterList($id)
    {
        $this->db->select('*');
        $this->db->from('exam_center');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addExamCenter($data)
    {
        $this->db->trans_start();
        $this->db->insert('exam_center', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function addRoomDetails($data){
        $this->db->trans_start();
        $this->db->insert('exam_center_has_room', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }
    function getRoomDetails($id_exam_center)
    {
        $this->db->select('*');
        $this->db->from('exam_center_has_room');
        $this->db->where('id_exam_center', $id_exam_center);
        $query = $this->db->get();
        return $query->result();
    }

    function getRoomById($id){
         $this->db->select('*');
        $this->db->from('exam_center_has_room');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function updateRoomDetails($data,$id){
         $this->db->where('id', $id);
        $result = $this->db->update('exam_center_has_room', $data);
        return $result;
    }

    function editExamSetupModel($data,$id) {
         $this->db->where('id', $id);
        $result = $this->db->update('exam_set', $data);
        return $result;
    }


    function editExamLogout($data,$id) {
         $this->db->where('id', $id);
        $result = $this->db->update('exam_logout', $data);
        return $result;
    }

    




    function editExamCenter($data, $id)
    {
        $this->db->where('id', $id);
        $result = $this->db->update('exam_center', $data);
        return $result;
    }



    function countryListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('country');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function stateListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('state');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function getStateByCountryId($id_country)
    {
        $this->db->select('*');
        $this->db->from('state');
        $this->db->where('id_country', $id_country);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function examCenterLocationListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('exam_center_location');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }


    function examCenterLocationList()
    {
        $this->db->select('*');
        $this->db->from('exam_center_location');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

      function deleteCourseHasClo($id)
    {
        $this->db->where('id', $id);
         $this->db->delete('exam_center_has_room');
    }
    
}