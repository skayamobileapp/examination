<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Final_marks_approval_model extends CI_Model
{
    function salutationListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('salutation_setup');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getstudentDetailsByExamsittingId($idexamsitting){

         $this->db->select('a.*,e.*,s.*,a.id as examstudenttagging');
            $this->db->from('exam_student_tagging as a');
            $this->db->join('exam_event as e', 'a.id_exam_event=e.id');
            $this->db->join('student as s', 's.id=a.id_student');
            $this->db->where('a.id', $idexamsitting);
            $query = $this->db->get();
            $result = $query->row();
         return $result;

    }

  


     function listofExamEvent($formData){
        $this->db->select('a.*,c.full_name as gradername,d.name as coursename,d.code as coursecode,ge.id as graderexamid,ge.approval_status,exs.exam_sitting_name');
        $this->db->from('exam_event as a');
        $this->db->join('exam_sitting as exs','a.id_exam_sitting = exs.id');

        $this->db->join('grader_exam as ge','a.id = ge.id_exam_event');
        $this->db->join('grader as c','ge.id_grader = c.id');

        $this->db->join('examset as es','a.id_exam_set = es.id');
        $this->db->join('course as d','es.id_course = d.id');
        //     $this->db->where('a.status', 1);
        //      if($formData['id_exam_sitting']>0) {
        // $this->db->where('exs.id', $formData['id_exam_sitting']);

        // }

          if($formData['exam_event_name']!='') {
        $this->db->where("a.name like '%$formData[exam_event_name]%'");

        }




        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

      function listofExamEventHistory(){
        $this->db->select('a.*,c.full_name as gradername,d.name as coursename,d.code as coursecode,ge.id as graderexamid,ge.approval_status');
        $this->db->from('exam_event as a');
        $this->db->join('grader_exam as ge','a.id = ge.id_exam_event');
        $this->db->join('grader as c','ge.id_grader = c.id');

        $this->db->join('examset as es','a.id_exam_set = es.id');
        $this->db->join('course as d','es.id_course = d.id');
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

     function getShowStudentQuestionSetForGrader($id_exam_student_tagging)
    {

        // , qs.short_name as qualification_code, qs.name as qualification_name
        $this->db->select('sqs.*, q.question,q.answer_scheme,q.marks as questionmarks,s.full_name');
        $this->db->from('student_question_set as sqs');
        $this->db->join('question as q', 'sqs.id_question = q.id');
        $this->db->join('student as s', 's.id = sqs.id_student');

        $this->db->where('sqs.id_exam_student_tagging',$id_exam_student_tagging);

        $this->db->where('q.question_type', '2');
        
        $query = $this->db->get();
        $results = $query->result(); 


        $details = array();

        foreach ($results as $result)
        {
            $id_student_question_set = $result->id;

            $student_question_set = $this->getStudentQuestionSet($id_student_question_set);


            $result->marks = '';
            if($student_question_set)
            {
                $result->marks = $student_question_set->marks;
            }

            array_push($details, $result);
        }

        return $details;
    }

    function getExamEventDetails($id){
         $this->db->select('ee.*');
        $this->db->from('exam_event as ee');
        $this->db->join('grader_exam as ge','ge.id_exam_event = ee.id');
        $this->db->where('ge.id', $id);
        $query = $this->db->get();
        return $query->row();
    }


    function getStudentQuestionSet($id)
    {
        $this->db->select('*');
        $this->db->from('student_question_set_marks');
        $this->db->where('id_student_question_set', $id);
        $query = $this->db->get();
        return $query->row();
    }


      function exammcaQuestionsByData($id_exam_tagging)
    {
        $this->db->select('sqs.*, que.question, qho.option_description as answer');
        $this->db->from('student_question_set as sqs');
        $this->db->join('question as que','sqs.id_question = que.id','left');
        $this->db->join('question_has_option as qho','sqs.id_answer = qho.id','left');
        $this->db->join('exam_student_tagging as est','sqs.id_exam_student_tagging = est.id','left');
        $this->db->join('student_exam_attempt as sea','sqs.id_student_exam_attempt = sea.id','left');
        // $this->db->where('sqs.id_student', $data['id_student']);
        $this->db->where('sqs.id_exam_student_tagging', $id_exam_tagging);
        $this->db->where("que.question_type='1'");
         $query = $this->db->get();
         $results = $query->result();
        
        // echo "<Pre>"; print_r($results);exit();

         $details = array();

         foreach ($results as $result)
         {

            $id_question = $result->id_question;
            $id_answer = $result->id_answer;

            // echo "<Pre>"; print_r($id_answer);exit();


            $result->correct_answer = '';
            $result->is_correct_answer = '0';

            $answer = $this->getAnswerToTheQuestion($id_question);

            if($answer)
            {
                $result->correct_answer = $answer->option_description;

                if($id_answer == $answer->id)
                {
                    $result->is_correct_answer = '1';
                }
            }

            array_push($details, $result);

            // echo "<Pre>"; print_r($result);exit();
         }

         return $details;
    }

    function examCenterListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('exam_center');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function graderListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('grader');
        $this->db->where('status', $status);
        $this->db->order_by("full_name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }


    function examCenterListByGraderId($id_grader)
    {
        $this->db->select('DISTINCT(gm.id_exam_center) as id_exam_center');
        $this->db->from('grader_exam as gm');
        $this->db->join('exam_center as e','gm.id_exam_center = e.id');
        $this->db->join('exam_event as ee','gm.id_exam_event = ee.id');
        $this->db->where('gm.id_grader', $id_grader);
        // $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $results = $query->result();  
            
        $details = array();

        foreach ($results as $result)
        {
            $id_exam_center = $result->id_exam_center;

            $exam_center = $this->getExamCenter($id_exam_center);

            if($exam_center)
            {
                array_push($details, $exam_center);
            }
        }

        return $details;
    }

    function getExamCenterByGrader($id_grader)
    {
        $this->db->select('DISTINCT(gm.id_exam_center) as id_exam_center');
        $this->db->from('grader_exam as gm');
        $this->db->join('exam_center as e','gm.id_exam_center = e.id');
        $this->db->join('exam_event as ee','gm.id_exam_event = ee.id');
        $this->db->where('gm.id_grader', $id_grader);
        // $this->db->where('gm.id_exam_center', $id_exam_center);
        // $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $results = $query->result(); 

        $details = array();

        foreach ($results as $result)
        {
            $id_exam_center = $result->id_exam_center;

            $exam_center = $this->getExamCenter($id_exam_center);

            if($exam_center)
            {
                array_push($details, $exam_center);
            }
        }

        return $details;
    }

    function getExamCenterByExamCenterNIdGrader($data)
    {
        $this->db->select('DISTINCT(gm.id_exam_event) as id_exam_event');
        $this->db->from('grader_exam as gm');
        $this->db->join('exam_center as e','gm.id_exam_center = e.id');
        $this->db->join('exam_event as ee','gm.id_exam_event = ee.id');
        $this->db->where('gm.id_grader', $data['id_grader']);
        $this->db->where('gm.id_exam_center', $data['id_exam_center']);
        // $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $results = $query->result(); 

        $details = array();

        foreach ($results as $result)
        {
            $id_exam_event = $result->id_exam_event;

            $exam_event = $this->getExamEvent($id_exam_event);

            if($exam_event)
            {
                array_push($details, $exam_event);
            }
        }

        return $details;
    }

    function getExamEventByExamCenter($id_exam_center)
    {
        $this->db->select('DISTINCT(gm.id_exam_event) as id_exam_event');
        $this->db->from('grader_exam as gm');
        $this->db->join('exam_center as e','gm.id_exam_center = e.id');
        $this->db->join('exam_event as ee','gm.id_exam_event = ee.id');
        // $this->db->where('gm.id_grader', $data['id_grader']);
        $this->db->where('gm.id_exam_center', $id_exam_center);
        // $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $results = $query->result(); 

        $details = array();

        foreach ($results as $result)
        {
            $id_exam_event = $result->id_exam_event;

            $exam_event = $this->getExamEvent($id_exam_event);

            if($exam_event)
            {
                array_push($details, $exam_event);
            }
        }

        return $details;
    }




    function getExamCenter($id)
    {
        $this->db->select('*');
        $this->db->from('exam_center');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getExamEvent($id)
    {
        $this->db->select('*');
        $this->db->from('exam_event');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function graderStudentListSearch($data)
    {
        $this->db->select('a.*');
        $this->db->from('grader as a');
        if ($data['name'] != '')
        {
            $likeCriteria = "(a.full_name  LIKE '%" . $data['name'] . "%' or a.nric  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("a.full_name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }
    
    function getGrader($id)
    {
        $this->db->select('*');
        $this->db->from('grader');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getGraderStudentTagList($id_grader)
    {
        $this->db->select('gm.*, s.full_name as student_name, s.nric, e.name as exam_center_name, ee.name as exam_event_name, ee.exam_date as exam_event_date');
        $this->db->from('grader_student_tag as gm');
        $this->db->join('student as s','gm.id_student = s.id');
        $this->db->join('exam_center as e','gm.id_exam_center = e.id');
        $this->db->join('exam_event as ee','gm.id_exam_event = ee.id');
        $this->db->where('gm.id_grader', $id_grader);
        // $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }
    
    function addNewGraderStudentTag($data)
    {
        $this->db->trans_start();
        $this->db->insert('grader_student_tag', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function deleteGraderModule($id)
    {
        $this->db->where('id', $id);
        $result = $this->db->delete('grader_modules');
        return $result;
    }

    function studentSearch($data)
    {
        $this->db->select('DISTINCT(s.id) as id_student');
        // , qs.short_name as qualification_code, qs.name as qualification_name
        $this->db->from('exam_student_tagging as est');
        $this->db->join('student as s', 'est.id_student = s.id');
        $this->db->join('exam_event as ee', 'est.id_exam_event = ee.id');
        if ($data['full_name'] != '')
        {
            $likeCriteria = "(s.full_name  LIKE '%" . $data['full_name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['email_id'] != '')
        {
            $likeCriteria = "(s.full_name  LIKE '%" . $data['email_id'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['id_grader'] != '')
        {
            $likeCriteria = " s.id NOT IN (SELECT cr.id_student FROM grader_student_tag cr where cr.id_grader = " . $data['id_grader'] . " and cr.id_exam_center = " . $data['id_exam_center'] . " and cr.id_exam_event =" . $data['id_exam_event'] . ")";
            $this->db->where($likeCriteria);            
        }
        if ($data['id_exam_event'] != '')
        {
            $this->db->where('est.id_exam_event', $data['id_exam_event']);
        }
        if ($data['id_exam_center'] != '')
        {
            $this->db->where('ee.id_exam_center', $data['id_exam_center']);
        }
        
        // if ($data['id_intake'] != '')
        // {
        //     $this->db->where('s.id_intake', $data['id_intake']);
        // }
        $query = $this->db->get();
        $results = $query->result(); 

        // echo "<Pre>"; print_r($this->db);exit();

        $details = array();

        foreach ($results as $result)
        {
            $id_student = $result->id_student;

            $student = $this->getStudent($id_student);

            if($student)
            {
                array_push($details, $student);
            }
        }

        return $details;

    }

    function getStudent($id)
    {
        $this->db->select('*');
        $this->db->from('student');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }




    function getGraderStudentTagListByData($idgrader)
    {

        // exam_student_tagging

        $this->db->select('gm.*,s.*,est.id as examstudenttagging,est.result_status,u.name as username,est.approved_date_time,est.mcq_total,est.essay_total');
        $this->db->from('grader_student_tag as gm');
        $this->db->join('student as s','gm.id_student = s.id','left');

        $this->db->join('exam_student_tagging as est','gm.id_exam_student_tagging = est.id','left');


        $this->db->join('exam_event as ee','gm.id_exam_event = ee.id');        
        $this->db->join('users as u','u.id = est.approved_by','left');        
        $this->db->where('gm.id_grader_exam',$idgrader );

         $this->db->order_by("s.full_name", "ASC");
         $query = $this->db->get();
         $results = $query->result();  

         return $results;
    }

    function getGraderStudentTagListByDatanew($idgrader)
    {

        // exam_student_tagging

        $this->db->select('gm.*,s.*,est.id as examstudenttagging,est.result_status,u.name as username,est.approved_date_time,est.mcq_total,est.essay_total,est.final_total');
        $this->db->from('grader_student_tag as gm');
        $this->db->join('student as s','gm.id_student = s.id','left');

        $this->db->join('exam_student_tagging as est','gm.id_exam_student_tagging = est.id','left');


        $this->db->join('exam_event as ee','gm.id_exam_event = ee.id');        
        $this->db->join('users as u','u.id = est.approved_by','left');        
        $this->db->where('gm.id_grader_exam',$idgrader );

         $this->db->order_by("s.full_name", "ASC");
         $query = $this->db->get();
         $results = $query->result();  

         // echo $this->db->last_query();;exit;

         return $results;
    }

    function getExamStudentQuestionTagCountByData($data)
    {
        $this->db->select('DISTINCT(sqe.id_question) as id_question, sqe.*');
        $this->db->from('student_question_set as sqe');
        $this->db->join('student_exam_attempt as sea','sqe.id_student_exam_attempt = sea.id');        
        $this->db->join('student as s','sqe.id_student = s.id');
        $this->db->join('exam_student_tagging as est','sqe.id_exam_student_tagging = est.id');
        $this->db->where('sqe.id_student', $data['id_student']);
        $this->db->where('sqe.id_exam_student_tagging', $data['id_exam_student_tagging']);
        $this->db->where('sqe.marks', '');
        // $this->db->where('gm.id_grader', $data['id_grader']);
        // $this->db->where('ee.id_exam_center', $data['id_exam_center']);
        // $this->db->where('gm.id_exam_event', $data['id_exam_event']);
        // $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->num_rows();

         return $result;
    }


      function getAllEssayQuestions($id_exam_student_tagging)
    {
        $this->db->select('sqe.*,sqsm.marks,q.question');
        $this->db->from('student_question_set as sqe');
        $this->db->join('student_question_set_marks as sqsm','sqe.id = sqsm.id_student_question_set','left');
         
        $this->db->join('question  as q','q.id = sqe.id_question','left');

        $this->db->where("q.question_type='2'");

        $this->db->where('sqe.id_exam_student_tagging', $id_exam_student_tagging);
         $query = $this->db->get();
         $results = $query->result();

      
         return $results;
    }



    function getExamStudentMarksTagCountByData($data)
    {
        $this->db->select('DISTINCT(sqe.id_question) as id_question, sqe.*,sqsm.marks');
        $this->db->from('student_question_set as sqe');
        $this->db->join('student_question_set_marks as sqsm','sqe.id = sqsm.id_student_question_set','left');
        $this->db->join('student as s','sqe.id_student = s.id');
        $this->db->join('exam_student_tagging as est','sqe.id_exam_student_tagging = est.id');
        $this->db->join('student_exam_attempt as sea','est.id = sea.id_exam_student_tagging');
        $this->db->where('sqe.id_student', $data['id_student']);
        $this->db->where('sqe.id_exam_student_tagging', $data['id_exam_student_tagging']);
        $this->db->where('sqe.marks !=', '');

        // $this->db->where('gm.id_grader', $data['id_grader']);
        // $this->db->where('ee.id_exam_center', $data['id_exam_center']);
        // $this->db->where('gm.id_exam_event', $data['id_exam_event']);
        // $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $results = $query->result();

         $marks = 0;
         foreach ($results as $result)
         {
            $marks = $marks + $result->marks;
         }

         return $marks;
    }

    function getExamStudentAttempt($id)
    {
        $this->db->select('gm.*');
        $this->db->from('student_exam_attempt as gm');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function moveStudentQuestionSetMarksToExamSetMarks($data)
    {
        $question_set_data = $this->getStudentQuestionsetByData($data);

        // echo "<Pre>";print_r($question_set_data);exit();
        
        foreach ($question_set_data as $question_set)
        {
            $id_student_question_set = $question_set->id;

            $questionset_marks = $this->getQuestionSetMarksByQuestionSetId($id_student_question_set);
            
            if($questionset_marks)
            {

                // echo "<Pre>";print_r($questionset_marks);exit();

                $marks = $questionset_marks->marks;
                $update_marks['marks'] = $marks;

                $this->editStudentQuestionSet($update_marks,$id_student_question_set);

            }
        }

        return TRUE;
    }

    function getStudentQuestionsetByData($data)
    {
        $this->db->select('DISTINCT(sqe.id_question) as id_question, sqe.*');
        $this->db->from('student_question_set as sqe');
        $this->db->join('student_exam_attempt as sea','sqe.id_student_exam_attempt = sea.id');        
        $this->db->join('student as s','sqe.id_student = s.id');
        $this->db->join('exam_student_tagging as est','sqe.id_exam_student_tagging = est.id');
        $this->db->where('sqe.id_student', $data['id_student']);
        $this->db->where('sqe.id_exam_student_tagging', $data['id_exam_student_tagging']);
        $this->db->where('sqe.marks', '');
        $query = $this->db->get();
        $results = $query->result();

        return $results;
    }

    function getQuestionSetMarksByQuestionSetId($id)
    {
        $this->db->select('gm.*');
        $this->db->from('student_question_set_marks as gm');
        $this->db->where('gm.id_student_question_set', $id);
        $this->db->order_by("gm.id", "DESC");
        $query = $this->db->get();
        return $query->row();
    }

    function editStudentQuestionSet($data,$id)
    {
        $this->db->where('id', $id);
        $result = $this->db->update('student_question_set',$data);
        return $result;
    }

    function getGradeByMarks($marks)
    {
            // echo "<Pre>";print_r($marks); exit;
        $this->db->select('gm.*');
        $this->db->from('grade as gm');
        $this->db->where('gm.min_percentage <=', $marks);
        $this->db->where('gm.max_percentage >=', $marks);
        $this->db->order_by("gm.id", "DESC");
        $query = $this->db->get();
        return $query->row();
    }

     function getAnswerToTheQuestion($id_question)
    {
        $this->db->select('qho.*');
        $this->db->from('question_has_option as qho');
        $this->db->where('qho.id_question', $id_question);
        $this->db->where('qho.is_correct_answer', 1);
        // $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->row();

         return $result;
    }

    function getOptionsByQuestionId($id_question)
    {
        $this->db->select('qho.*');
        $this->db->from('question_has_option as qho');
        $this->db->where('qho.id_question', $id_question);
        // $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();

         return $result;
    }
}