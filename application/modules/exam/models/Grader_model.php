<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Grader_model extends CI_Model
{
    function salutationListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('salutation_setup');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getModuleNameByGraderid($id) {
        
           $this->db->select('gm.*, c.name as course_name, c.code as course_code');
        $this->db->from('grader_modules as gm');
        $this->db->join('course as c','gm.id_course = c.id');
        $this->db->where('gm.id_grader', $id);
        // $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function courseListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('course');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function graderList()
    {
        $this->db->select('*');
        $this->db->from('grader');
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function graderListSearch($data,$type)
    {

        
        $this->db->select('a.*, cre.name as created_by, upd.name as updated_by');
        $this->db->from('grader as a');
        $this->db->join('users as cre', 'a.created_by = cre.id','left');
        $this->db->join('users as upd', 'a.updated_by = upd.id','left');

        if ($data['name'] != '')
        {
            $likeCriteria = "(a.full_name  LIKE '%" . $data['name'] . "%' or a.nric  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
                $this->db->where('type', $type);

        $this->db->order_by("a.first_name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }
    
    function getGrader($id)
    {
        $this->db->select('*');
        $this->db->from('grader');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addNewGrader($data)
    {
        $this->db->trans_start();
        $this->db->insert('grader', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editGrader($data, $id)
    {
        $this->db->where('id', $id);
        $result = $this->db->update('grader', $data);
        return $result;
    }

    function getSalutation($id)
    {
        $this->db->select('*');
        $this->db->from('salutation_setup');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function addGraderModuleData($data)
    {
        $this->db->trans_start();
        $this->db->insert('grader_modules', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function deleteGraderModule($id)
    {
        $this->db->where('id', $id);
        $result = $this->db->delete('grader_modules');
        return $result;
    }

    

    function getGraderModulesList($id_grader)
    {
        $this->db->select('gm.*, c.name as course_name, c.code as course_code');
        $this->db->from('grader_modules as gm');
        $this->db->join('course as c','gm.id_course = c.id');
        $this->db->where('gm.id_grader', $id_grader);
        // $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }
}