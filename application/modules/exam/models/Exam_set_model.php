<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Exam_set_model extends CI_Model
{
    function examSetList()
    {
        $this->db->select('*');
        $this->db->from('exam_set');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function examSetListSearch($search)
    {
        $this->db->select('*');
        $this->db->from('exam_set');
        if (!empty($search))
        {
            $likeCriteria = "(name  LIKE '%" . $search . "%' or instruction  LIKE '%" . $search . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getExamSet($id)
    {
        $this->db->select('*');
        $this->db->from('exam_set');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function insertChat($data) {
        $this->db->trans_start();
        $this->db->insert('chat', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }
    
    function addNewExamSet($data)
    {
        $this->db->trans_start();
        $this->db->insert('exam_set', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editExamSet($data, $id)
    {
        $this->db->where('id', $id);
        $result = $this->db->update('exam_set', $data);
        return $result ;
    }
}

