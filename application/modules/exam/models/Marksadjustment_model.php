<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Marksadjustment_model extends CI_Model
{
    function listofmarksadjustment() {
        $this->db->select('gm.*, s.full_name as student_name, s.nric,ee.name as examname,ee.exam_date');
        $this->db->from('grader_student_tag as gm');
        $this->db->join('student as s','gm.id_student = s.id');
        $this->db->join('exam_student_tagging as e','gm.id_exam_student_tagging = e.id');
                $this->db->join('exam_event as ee','e.id_exam_event = ee.id');        

        $this->db->where("e.result_status='11'");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function listofmarksadjustmentforapproval() {
        $this->db->select('gm.*, s.full_name as student_name, s.nric,ee.name as examname,ee.exam_date');
        $this->db->from('grader_student_tag as gm');
        $this->db->join('student as s','gm.id_student = s.id');
        $this->db->join('exam_student_tagging as e','gm.id_exam_student_tagging = e.id');
                $this->db->join('exam_event as ee','e.id_exam_event = ee.id');        

        $this->db->where("e.result_status='12'");
        // $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }



     function listofmarksadjustmenthistory() {
        $this->db->select('gm.*, s.full_name as student_name, s.nric,ee.name as examname,ee.exam_date,gm.id as id_exam_student_tagging');
        $this->db->from('exam_student_tagging as gm');
        $this->db->join('student_question_set as sqs','sqs.id_exam_student_tagging = gm.id');
        $this->db->join('student_question_set_marks_adjustment as sqsma','sqsma.id_student_question_set = sqs.id');
                $this->db->join('exam_event as ee','gm.id_exam_event = ee.id');        
                $this->db->join('student as s','gm.id_student = s.id');        

        $this->db->group_by("gm.id");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }


   

    function listofmarksadjustmentbyExamstudenttagging($id_exam_student_tagging) {
        $this->db->select('gm.*, s.full_name as student_name, s.nric,ee.name as examname,ee.exam_date');
        $this->db->from('grader_student_tag as gm');
        $this->db->join('student as s','gm.id_student = s.id');
        $this->db->join('exam_student_tagging as e','gm.id_exam_student_tagging = e.id');
        $this->db->where('e.id', $id_exam_student_tagging);

        // $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }


     function totalMarksAdustmentList($id_exam_student_tagging){
        $this->db->select('est.*,qu.marks as originalmarks,q.marks as answermarks');
        $this->db->from('student_question_set as est');
        $this->db->join('student_question_set_marks_adjustment  as q','q.id_student_question_set = est.id');       
        $this->db->join('question  as qu','qu.id = est.id_question');        

        $this->db->where('est.id_exam_student_tagging', $id_exam_student_tagging);
        $this->db->where("qu.question_type='2'");        
        $query = $this->db->get();
        // print_r($this->db->last_query());    

        $results = $query->result(); 
        return $results;
     }

     function totalMCQQuestionMarked($id_exam_student_tagging){
        $this->db->select('est.*,qu.marks as originalmarks,q.marks as answermarks');
        $this->db->from('student_question_set as est');
        $this->db->join('student_question_set_marks  as q','q.id_student_question_set = est.id');       
        $this->db->join('question  as qu','qu.id = est.id_question');        

        $this->db->where('est.id_exam_student_tagging', $id_exam_student_tagging);
        $this->db->where("qu.question_type='2'");        
        $query = $this->db->get();
        // print_r($this->db->last_query());    

        $results = $query->result(); 
        return $results;
    }

     function totalMCQQuestion($id_exam_student_tagging) {
        $this->db->select('est.*');
        $this->db->from('student_question_set as est');
        $this->db->join('question  as q','q.id = est.id_question');        
        $this->db->where('est.id_exam_student_tagging', $id_exam_student_tagging);
        $this->db->where("q.question_type='2'");
        $query = $this->db->get();
        $results = $query->result(); 
        return $results;
    }
     function addNewStudentQuestionSetMarks($data)
    {
        $this->db->trans_start();
        $this->db->insert('student_question_set_marks_adjustment', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }


    function getShowStudentQuestionSetForGrader($id_exam_student_tagging)
    {

            $this->db->select('sqs.*, q.question,q.answer_scheme,q.marks as questionmarks,s.full_name');
        $this->db->from('student_question_set as sqs');
        $this->db->join('question as q', 'sqs.id_question = q.id');
        $this->db->join('student as s', 's.id = sqs.id_student');

        $this->db->where('sqs.id_exam_student_tagging',$id_exam_student_tagging);

        $this->db->where('q.question_type', '2');
        
        $query = $this->db->get();
        $results = $query->result(); 


        $details = array();

        foreach ($results as $result)
        {
            $id_student_question_set = $result->id;

            $student_question_set = $this->getStudentQuestionSet($id_student_question_set);


            $result->marks = '';
            if($student_question_set)
            {
                $result->marks = $student_question_set->marks;
            }

            array_push($details, $result);
        }

        return $details;
    }


     function getStudentQuestionSet($id)
    {
        $this->db->select('*');
        $this->db->from('student_question_set_marks');
        $this->db->where('id_student_question_set', $id);
        $query = $this->db->get();
        return $query->row();
    }

     function getadjmarks($id)
    {
        $this->db->select('*');
        $this->db->from('student_question_set_marks_adjustment');
        $this->db->where('id_student_question_set', $id);
        $query = $this->db->get();
        return $query->row();
    }

    


      function deletemarks($id)
    { 
        // echo "<Pre>";  print_r($id);exit;
       $this->db->where('id_student_question_set', $id);
       $this->db->delete('student_question_set_marks_adjustment');
    }



}