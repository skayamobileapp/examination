<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Exam_event_model extends CI_Model
{
    function getCentersByLocatioin($id_location)
    {
        $this->db->select('DISTINCT(a.id) as id, a.*');
        $this->db->from('exam_center as a');
        $this->db->join('exam_center_location as ecl', 'a.id_location = ecl.id');
            $this->db->where('a.id_location', $id_location);
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();      
         return $result;
    }

      function deleteStudentQuestionSetById($id) {
         $this->db->where('id_student_question_set', $id);
        $result = $this->db->delete('student_question_set_marks');
        return $result;
    }

      function addNewStudentQuestionSetMarks($data)
    {
        $this->db->trans_start();
        $this->db->insert('student_question_set_marks', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }


    function examStudentTaggingData($id) {
         $this->db->select('c.*');
        $this->db->from('exam_student_tagging as c');
        $this->db->where('c.id', $id);
        $query = $this->db->get();
        $result = $query->row();

        return $result;
    }

    function graderStudentTag($id) {
         $this->db->select('c.*');
        $this->db->from('grader_student_tag as c');
        $this->db->where('c.id_exam_student_tagging', $id);
        $query = $this->db->get();
        $result = $query->row();

        return $result;
    }
  function getCourseListByType($id) {
         $this->db->select('*');
        $this->db->from('course');
        $this->db->order_by("name", "ASC");
        $this->db->where('type', $id);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }


     function getExamSitting() {
         $this->db->select('*');
        $this->db->from('exam_sitting');
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }



     function examStudentTaggingListSearch($id_exam_event)
    {
        // $date = 
        $this->db->select('a.*, s.full_name as student_name, s.nric,s.batch,s.membership_number, ee.name as exam_event, ee.exam_date,ee.from_tm,c.name as coursename,s.ip,s.mac_id,s.location,s.email_id,ee.mqms_type');
        $this->db->from('exam_student_tagging as a');
        $this->db->join('exam_event as ee', 'a.id_exam_event = ee.id','left');
        $this->db->join('examset as es', 'ee.id_exam_set = es.id','left');
        $this->db->join('course as c', 'ee.id_course = c.id','left');
        $this->db->join('student as s', 'a.id_student = s.id','left');
       
            $this->db->where('a.id_exam_event', $id_exam_event);
       
        $query = $this->db->get();
        $result = $query->result();

        return $result;
    }




      function getExamsetById($id)
    {
        $this->db->select('c.*');
        $this->db->from('examset as c');
        $this->db->where('c.id', $id);
        $query = $this->db->get();
        $result = $query->row();

        return $result;
    }

     function getExamSetByCourse($id)
    {
        $this->db->select('c.*');
        $this->db->from('examset as c');
        $this->db->where('c.id_course', $id);
        $this->db->where("c.status='1'");
        $query = $this->db->get();
        $result = $query->result();

        return $result;
    }


    

    function examEventListSearch($data)
    {
        // $date = 
        $this->db->select('DISTINCT(a.id) as id, a.*,esit.exam_sitting_name as exam_sitting_name,c.name as coursename,c.code as coursecode,ecl.name as examcenternae');
        $this->db->from('exam_event as a');
        $this->db->join('exam_center as ecl', 'a.id_exam_center = ecl.id','left');
        $this->db->join('exam_sitting as esit', 'a.id_exam_sitting = esit.id','left');
        $this->db->join('examset as es', 'es.id=a.id_exam_set','left');
        $this->db->join('course as c', 'c.id=a.id_course','left');

        if ($data['name'] != '')
        {
            $likeCriteria = "(name  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['id_location'] != '')
        {
            $this->db->where('a.id_location', $data['id_location']);
        }
        if ($data['id_exam_center'] != '')
        {
            $this->db->where('a.id_exam_center', $data['id_exam_center']);
        }

        if($data['type']=='Past') {
            $this->db->where("date(a.exam_date) <= ", date('Y-m-d'));

        } else {
            $this->db->where("date(a.exam_date) >= ", date('Y-m-d'));

        }
            $this->db->where("a.status", 1);

        $this->db->order_by("a.exam_date", "asc");
         $query = $this->db->get();
         $list = $query->result();  
         return $list;
    }


    function examEventIssueSearch(){
         $this->db->select('a.*,ee.name as exameventname,ee.exam_date');
        $this->db->from('issue as a ');
        $this->db->join('exam_event as ee', 'a.id_exam_event=ee.id');

        $this->db->order_by("a.id", "DESC");
        $query = $this->db->get();
        $result = $query->result();
        return $result;

    }

    function getExamEvent($id)
    {
        $this->db->select('*');
        $this->db->from('exam_event');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    
    function addExamEvent($data)
    {
        $this->db->trans_start();
        $this->db->insert('exam_event', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editExamEvent($data, $id)
    {
        $this->db->where('id', $id);
        $result = $this->db->update('exam_event', $data);
        return $result;
    }

    function updateToken($data, $id)
    {
        $this->db->where('id', $id);
        $result = $this->db->update('exam_student_tagging', $data);
        return $result;
    }

    function updateAttendence($data, $id)
    {
        $this->db->where('id', $id);
        $result = $this->db->update('exam_student_tagging', $data);
        return $result;
    }


    function getStateByCountryId($id_country)
    {
        $this->db->select('*');
        $this->db->from('state');
        $this->db->where('id_country', $id_country);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function examCenterLocationListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('exam_center_location');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getExamSet() {
        $this->db->select('*');
        $this->db->from('examset');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }


    function examCenterLocationList()
    {
        $this->db->select('*');
        $this->db->from('exam_center_location');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function examCenterListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('exam_center');
        $this->db->order_by("name", "ASC");
        $this->db->where('status', $status);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }


    function examCenterList()
    {
        $this->db->select('*');
        $this->db->from('exam_center');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getExamCenterNLocationByCenterId($id)
    {
        $this->db->select('*');
        $this->db->from('exam_center');
        $this->db->where('id', $id);
        $query = $this->db->get();
        $data['exam_center'] = $query->row()->name;



        $id_location = $query->row()->id_location;

        $this->db->select('*');
        $this->db->from('exam_center_location');
        $this->db->where('id', $id_location);
        $query = $this->db->get();
        $data['location'] = $query->row()->name;

        return $data;
    }

      function getShowStudentQuestionSetForGrader($id_exam_student_tagging,$idexamgrader)
    {

        // , qs.short_name as qualification_code, qs.name as qualification_name
        $this->db->select('sqs.*, q.question,q.answer_scheme,q.marks as questionmarks,s.membership_number');
        $this->db->from('student_question_set as sqs');
        $this->db->join('question as q', 'sqs.id_question = q.id');
        $this->db->join('student as s', 's.id = sqs.id_student');

        $this->db->where('sqs.id_exam_student_tagging',$id_exam_student_tagging);

        $this->db->where('q.question_type', '2');
        
        $query = $this->db->get();
        $results = $query->result(); 

        // echo "<Pre>"; print_r($this->db);exit();

        $details = array();

        foreach ($results as $result)
        {
            $id_student_question_set = $result->id;

            $student_question_set = $this->getStudentQuestionSet($id_student_question_set,$idexamgrader);


            $result->marks = '';
            if($student_question_set)
            {
                $result->marks = $student_question_set->marks;
            }

            array_push($details, $result);
        }

        return $details;
    }

     function getStudentQuestionSet($id,$idexamgrader)
    {
        $this->db->select('*');
        $this->db->from('student_question_set_marks');
        $this->db->where('id_student_question_set', $id);
        $this->db->where('id_grader_exam', $idexamgrader);
        $query = $this->db->get();
        return $query->row();
    }

       function getExamEventForPdf($id_grader)
    {
        $this->db->select('ee.*,c.name as coursename,c.code as modulecode');
        $this->db->from('exam_event as ee');
        $this->db->join('course as c','ee.id_course = c.id');

        $this->db->where('ee.id', $id_grader);
        // $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->row();  
         return $result;
    }

     function getExamDetailsByIdGrader($idexamgrader) {
        $this->db->select('gm.*, e.name as exam_center_name, ee.name as exam_event_name, ee.exam_date as exam_event_date,c.name as coursename,c.code as coursecode');
        $this->db->from('grader_exam as gm');
        $this->db->join('exam_event as ee','gm.id_exam_event = ee.id');
        $this->db->join('course as c','ee.id_course = c.id');
        $this->db->join('exam_center as e','ee.id_exam_center = e.id');
        $this->db->where('gm.id', $idexamgrader);
        // $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }


    function getExamStudentTagging($id)
    {
        $this->db->select('est.*, ee.id as id_exam_event, ec.id as id_exam_center');
        $this->db->from('exam_student_tagging as est');
        $this->db->join('exam_event as ee','est.id_exam_event = ee.id');        
        $this->db->join('exam_center as ec','ee.id_exam_center = ec.id');
        $this->db->where('est.id', $id);
        $query = $this->db->get();
        return $query->row();
    }
  function getGraderStudentTagList($id_grader,$id_exam_event,$idexamgrader)
    {
        $this->db->select('gm.*, s.full_name as student_name, s.nric,s.membership_number, e.name as exam_center_name, ee.name as exam_event_name, ee.exam_date as exam_event_date,est.id as id_exam_student_tagging,c.name as coursename,c.code as coursecode');
        $this->db->from('grader_student_tag as gm');
        $this->db->join('student as s','gm.id_student = s.id');
        $this->db->join('exam_event as ee','gm.id_exam_event = ee.id');
        $this->db->join('exam_center as e','ee.id_exam_center = e.id');
        $this->db->join('course as c','ee.id_course = c.id');
        $this->db->join('exam_student_tagging as est','s.id = est.id_student');
        $this->db->where('gm.id_grader', $id_grader);
        $this->db->where('gm.id_grader_exam', $idexamgrader);
        $this->db->where('gm.id_exam_event', $id_exam_event);
        $query = $this->db->get();
        $result = $query->result();  
        return $result;
    }


}