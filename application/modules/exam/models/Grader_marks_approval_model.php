<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Grader_marks_approval_model extends CI_Model
{
    function salutationListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('salutation_setup');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getQuestionById($id) {
         $this->db->select('a.*,q.question,q.answer_scheme');
        $this->db->from('student_question_set as a');
        $this->db->join('question as q','a.id_question = q.id');
        $this->db->where('a.id', $id);
         $query = $this->db->get();
         $result = $query->row();  
         return $result;
    }

    function examCenterListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('exam_center');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function graderListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('grader');
        $this->db->where('status', $status);
        $this->db->order_by("full_name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }


    function examCenterListByGraderId($id_grader)
    {
        $this->db->select('DISTINCT(gm.id_exam_center) as id_exam_center');
        $this->db->from('grader_exam as gm');
        $this->db->join('exam_center as e','gm.id_exam_center = e.id');
        $this->db->join('exam_event as ee','gm.id_exam_event = ee.id');
        $this->db->where('gm.id_grader', $id_grader);
        // $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $results = $query->result();  
            
        $details = array();

        foreach ($results as $result)
        {
            $id_exam_center = $result->id_exam_center;

            $exam_center = $this->getExamCenter($id_exam_center);

            if($exam_center)
            {
                array_push($details, $exam_center);
            }
        }

        return $details;
    }

    function getExamCenterByGrader($id_grader)
    {
        $this->db->select('DISTINCT(gm.id_exam_center) as id_exam_center');
        $this->db->from('grader_exam as gm');
        $this->db->join('exam_center as e','gm.id_exam_center = e.id');
        $this->db->join('exam_event as ee','gm.id_exam_event = ee.id');
        $this->db->where('gm.id_grader', $id_grader);
        // $this->db->where('gm.id_exam_center', $id_exam_center);
        // $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $results = $query->result(); 

        $details = array();

        foreach ($results as $result)
        {
            $id_exam_center = $result->id_exam_center;

            $exam_center = $this->getExamCenter($id_exam_center);

            if($exam_center)
            {
                array_push($details, $exam_center);
            }
        }

        return $details;
    }

    function getExamCenterByExamCenterNIdGrader($data)
    {
        $this->db->select('DISTINCT(gm.id_exam_event) as id_exam_event');
        $this->db->from('grader_exam as gm');
        $this->db->join('exam_center as e','gm.id_exam_center = e.id');
        $this->db->join('exam_event as ee','gm.id_exam_event = ee.id');
        $this->db->where('gm.id_grader', $data['id_grader']);
        $this->db->where('gm.id_exam_center', $data['id_exam_center']);
        // $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $results = $query->result(); 

        $details = array();

        foreach ($results as $result)
        {
            $id_exam_event = $result->id_exam_event;

            $exam_event = $this->getExamEvent($id_exam_event);

            if($exam_event)
            {
                array_push($details, $exam_event);
            }
        }

        return $details;
    }

    function getExamEventByExamCenter($id_exam_center)
    {
        $this->db->select('DISTINCT(gm.id_exam_event) as id_exam_event');
        $this->db->from('grader_exam as gm');
        $this->db->join('exam_center as e','gm.id_exam_center = e.id');
        $this->db->join('exam_event as ee','gm.id_exam_event = ee.id');
        // $this->db->where('gm.id_grader', $data['id_grader']);
        $this->db->where('gm.id_exam_center', $id_exam_center);
        // $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $results = $query->result(); 

        $details = array();

        foreach ($results as $result)
        {
            $id_exam_event = $result->id_exam_event;

            $exam_event = $this->getExamEvent($id_exam_event);

            if($exam_event)
            {
                array_push($details, $exam_event);
            }
        }

        return $details;
    }

    function getExamCenter($id)
    {
        $this->db->select('*');
        $this->db->from('exam_center');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getExamEvent($id)
    {
        $this->db->select('*');
        $this->db->from('exam_event');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function graderStudentListSearch($data)
    {
        $this->db->select('a.*');
        $this->db->from('grader as a');
        if ($data['name'] != '')
        {
            $likeCriteria = "(a.full_name  LIKE '%" . $data['name'] . "%' or a.nric  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("a.full_name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }
    
    function getGrader($id)
    {
        $this->db->select('*');
        $this->db->from('grader');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getGraderStudentTagList($id_grader)
    {
        $this->db->select('gm.*, s.full_name as student_name, s.nric, e.name as exam_center_name, ee.name as exam_event_name, ee.exam_date as exam_event_date');
        $this->db->from('grader_student_tag as gm');
        $this->db->join('student as s','gm.id_student = s.id');
        $this->db->join('exam_center as e','gm.id_exam_center = e.id');
        $this->db->join('exam_event as ee','gm.id_exam_event = ee.id');
        $this->db->where('gm.id_grader', $id_grader);
        // $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }
    
    function addNewGraderStudentTag($data)
    {
        $this->db->trans_start();
        $this->db->insert('grader_student_tag', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function deleteGraderModule($id)
    {
        $this->db->where('id', $id);
        $result = $this->db->delete('grader_modules');
        return $result;
    }

    function studentSearch($data)
    {
        $this->db->select('DISTINCT(s.id) as id_student');
        // , qs.short_name as qualification_code, qs.name as qualification_name
        $this->db->from('exam_student_tagging as est');
        $this->db->join('student as s', 'est.id_student = s.id');
        $this->db->join('exam_event as ee', 'est.id_exam_event = ee.id');
        if ($data['full_name'] != '')
        {
            $likeCriteria = "(s.full_name  LIKE '%" . $data['full_name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['email_id'] != '')
        {
            $likeCriteria = "(s.full_name  LIKE '%" . $data['email_id'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['id_grader'] != '')
        {
            $likeCriteria = " s.id NOT IN (SELECT cr.id_student FROM grader_student_tag cr where cr.id_grader = " . $data['id_grader'] . " and cr.id_exam_center = " . $data['id_exam_center'] . " and cr.id_exam_event =" . $data['id_exam_event'] . ")";
            $this->db->where($likeCriteria);            
        }
        if ($data['id_exam_event'] != '')
        {
            $this->db->where('est.id_exam_event', $data['id_exam_event']);
        }
        if ($data['id_exam_center'] != '')
        {
            $this->db->where('ee.id_exam_center', $data['id_exam_center']);
        }
        
        // if ($data['id_intake'] != '')
        // {
        //     $this->db->where('s.id_intake', $data['id_intake']);
        // }
        $query = $this->db->get();
        $results = $query->result(); 

        // echo "<Pre>"; print_r($this->db);exit();

        $details = array();

        foreach ($results as $result)
        {
            $id_student = $result->id_student;

            $student = $this->getStudent($id_student);

            if($student)
            {
                array_push($details, $student);
            }
        }

        return $details;

    }

    function getStudent($id)
    {
        $this->db->select('*');
        $this->db->from('student');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }


    function totalonlyMCQQuestionAnswered($id_exam_student_tagging) {

        $this->db->select('sum(qu.marks) as totalmcq');
        $this->db->from('student_question_set as est');
        $this->db->join('question  as qu','qu.id = est.id_question');        
        $this->db->where('est.marks>0');
        $this->db->where('est.id_exam_student_tagging', $id_exam_student_tagging);
        $query = $this->db->get();

        $results = $query->result(); 
        return $results;

    }

    function getautomcqmarks($id_exam_student_tagging) {
         $this->db->select('est.*,qu.marks as questionmarks');
        $this->db->from('student_question_set as est');
        $this->db->join('question  as qu','qu.id = est.id_question');        
        $this->db->where('est.id_exam_student_tagging', $id_exam_student_tagging);
        $this->db->where("qu.question_type='1'");        
        $query = $this->db->get();
        $results = $query->result(); 
        return $results;
    }

    function totalMCQQuestionMarked($id_exam_student_tagging){
        $this->db->select('est.*,qu.marks as originalmarks,q.marks as answermarks');
        $this->db->from('student_question_set as est');
        $this->db->join('student_question_set_marks  as q','q.id_student_question_set = est.id');       
        $this->db->join('question  as qu','qu.id = est.id_question');        

        $this->db->where('est.id_exam_student_tagging', $id_exam_student_tagging);
        $this->db->where("qu.question_type='2'");        
        $query = $this->db->get();
        $results = $query->result(); 
        return $results;
    }


    function getGraderStudentTagListByData($data)
    {

        // exam_student_tagging

        $idgrader = $data['id_grader'];


        $this->db->select('gm.*,s.*');
        $this->db->from('grader_student_tag as gm');
        $this->db->join('student as s','gm.id_student = s.id');

        $this->db->join('exam_student_tagging as est','gm.id_exam_student_tagging = est.id');


        $this->db->join('exam_event as ee','gm.id_exam_event = ee.id');        
        $this->db->where('est.result_status', 1);
        $this->db->where('gm.id_grader',$idgrader );

         $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $results = $query->result();  
                 
  

         return $results;
    }

    function getExamStudentQuestionTagCountByData($data)
    {
        $this->db->select('DISTINCT(sqe.id_question) as id_question, sqe.*');
        $this->db->from('student_question_set as sqe');
        $this->db->join('student_exam_attempt as sea','sqe.id_student_exam_attempt = sea.id');        
        $this->db->join('student as s','sqe.id_student = s.id');

        $this->db->join('question  as q','sqe.id_question = q.id');


        $this->db->join('exam_student_tagging as est','sqe.id_exam_student_tagging = est.id');
        $this->db->where('sqe.id_student', $data['id_student']);
        $this->db->where('q.question_type', 2);
        $this->db->where('sqe.id_exam_student_tagging', $data['id_exam_student_tagging']);
        $this->db->where('sqe.marks', '');
        // $this->db->where('gm.id_grader', $data['id_grader']);
        // $this->db->where('ee.id_exam_center', $data['id_exam_center']);
        // $this->db->where('gm.id_exam_event', $data['id_exam_event']);
        // $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->num_rows();

         return $result;
    }

    function getExamStudentMarksTagCountByData($data)
    {
        $this->db->select('DISTINCT(sqe.id_question) as id_question, sqe.*,sqsm.marks');
        $this->db->from('student_question_set as sqe');
        $this->db->join('student_question_set_marks as sqsm','sqe.id = sqsm.id_student_question_set','left');
        $this->db->join('student_exam_attempt as sea','sqe.id_student_exam_attempt = sea.id');        
        $this->db->join('student as s','sqe.id_student = s.id');
                $this->db->join('question  as q','sqe.id_question = q.id');


        $this->db->join('exam_student_tagging as est','sqe.id_exam_student_tagging = est.id');
        $this->db->where('sqe.id_student', $data['id_student']);
                $this->db->where('q.question_type', 2);

        $this->db->where('sqe.id_exam_student_tagging', $data['id_exam_student_tagging']);
        $this->db->where('sqe.marks', '');

        // $this->db->where('gm.id_grader', $data['id_grader']);
        // $this->db->where('ee.id_exam_center', $data['id_exam_center']);
        // $this->db->where('gm.id_exam_event', $data['id_exam_event']);
        // $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $results = $query->result();

         $marks = 0;
         foreach ($results as $result)
         {
            $marks = $marks + $result->marks;
         }

         return $marks;
    }

    function getExamStudenttagging($id)
    {
        $this->db->select('gm.*');
        $this->db->from('exam_student_tagging as gm');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function moveStudentQuestionSetMarksToExamSetMarks($data)
    {
        $question_set_data = $this->getStudentQuestionsetByData($data);

        // echo "<Pre>";print_r($question_set_data);exit();
        
        foreach ($question_set_data as $question_set)
        {
            $id_student_question_set = $question_set->id;

            $questionset_marks = $this->getQuestionSetMarksByQuestionSetId($id_student_question_set);
            
            if($questionset_marks)
            {

                // echo "<Pre>";print_r($questionset_marks);exit();

                $marks = $questionset_marks->marks;
                $update_marks['marks'] = $marks;

                $this->editStudentQuestionSet($update_marks,$id_student_question_set);

            }
        }

        return TRUE;
    }

    function getStudentQuestionsetByData($data)
    {
        $this->db->select('DISTINCT(sqe.id_question) as id_question, sqe.*');
        $this->db->from('student_question_set as sqe');
        $this->db->join('student_exam_attempt as sea','sqe.id_student_exam_attempt = sea.id');        
        $this->db->join('student as s','sqe.id_student = s.id');
        $this->db->join('exam_student_tagging as est','sqe.id_exam_student_tagging = est.id');
        $this->db->where('sqe.id_student', $data['id_student']);
        $this->db->where('sqe.id_exam_student_tagging', $data['id_exam_student_tagging']);
        $this->db->where('sqe.marks', '');
        $query = $this->db->get();
        $results = $query->result();

        return $results;
    }

    function getQuestionSetMarksByQuestionSetId($id)
    {
        $this->db->select('gm.*');
        $this->db->from('student_question_set_marks as gm');
        $this->db->where('gm.id_student_question_set', $id);
        $this->db->order_by("gm.id", "DESC");
        $query = $this->db->get();
        return $query->row();
    }

    function editStudentQuestionSet($data,$id)
    {
        $this->db->where('id', $id);
        $result = $this->db->update('student_question_set',$data);
        return $result;
    }

    function updateExamStudentTagging($data,$id)
    {
        $this->db->where('id', $id);
        $result = $this->db->update('exam_student_tagging',$data);
        return $result;
    }

    
}