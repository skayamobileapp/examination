<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class GraderExam extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('grader_exam_model');
        $this->load->model('exam_event_model');
        $this->load->model('grader_student_tag_model');

        $this->isLoggedIn();
                error_reporting(0);

    }

    function list()
    {
        if ($this->checkAccess('grader_exam.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $formData['id_exam_sitting'] = $this->security->xss_clean($this->input->post('id_exam_sitting')); 
            $formData['exam_event_name'] = $this->security->xss_clean($this->input->post('exam_event_name')); 
            $data['searchParam'] = $formData;
            $data['examSitting'] = $this->exam_event_model->getExamSitting();
            // $data['examSitting'] = $this->exam_event_model->getExamEvent();


            $data['examEventList'] = $this->grader_exam_model->listofExamEvent($formData);


            $this->global['pageCode'] = 'grader_exam.list';
            $this->global['pageTitle'] = 'Examination Management System : Grader Exam List';
            $this->loadViews("grader_exam/list", $this->global, $data, NULL);
        }
    }


    function edit($id) {
            $user_id = $this->session->userId;

        if ($this->checkAccess('grader_exam.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
             if($this->input->post())
            {


               

                $id_grader = $this->security->xss_clean($this->input->post('id_grader'));
                $start_date = $this->security->xss_clean($this->input->post('start_date'));
                $end_date = $this->security->xss_clean($this->input->post('end_date'));


               
                $data = array(
                    'id_grader' => $id_grader,
                    'status' => 1,
                    'start_date' => date('Y-m-d',strtotime($start_date)),
                    'end_date' => date('Y-m-d',strtotime($end_date)),
                    'updated_by' => $user_id
                );
            
             $id_grader_exam = $this->grader_exam_model->editGraderExam($data,$id);


            
                                        redirect('/exam/graderExam/list');

            }
          



            $data['graderexamdetails'] =  $this->grader_exam_model->getGraderAssigedDetails($id);



            $data['graderList'] = $this->grader_exam_model->graderAllListByEvent();





            $this->global['pageCode'] = 'grader_exam.list';
            $this->global['pageTitle'] = 'Examination Management System : Grader Exam List';
            $this->loadViews("grader_exam/edit", $this->global, $data, NULL);
        }


        
    }




    function add($id_exam_event)
    {
        if ($this->checkAccess('grader_exam.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            
            $user_id = $this->session->userId;
             if($this->input->post())
            {

               if($_POST['stdid']==''){
                  echo "<script>alert('Please select atleast one student')</script>";
                  echo "<script>parent.location='/exam/graderExam/add/$id_exam_event'</script>";
                  exit;

               }

                $id_grader = $this->security->xss_clean($this->input->post('id_grader'));
                $start_date = $this->security->xss_clean($this->input->post('start_date'));
                $end_date = $this->security->xss_clean($this->input->post('end_date'));
                // $tm_12hr  = date("H:i", strtotime($exam_time));
                $start_date =  date('Y-m-d',strtotime($start_date));
                $end_date = date('Y-m-d',strtotime($end_date));


                $graderExamArray = $this->grader_exam_model->checkgraderalreadyassigntoexamevent($id_grader,$id_exam_event);

                if($graderExamArray) {
                        $id_grader_exam = $graderExamArray->id;
                } else {
                $data = array(
                    'id_grader' => $id_grader,
                    'id_exam_event' => $id_exam_event,
                    'status' => 1,
                    'start_date' => date('Y-m-d',strtotime($start_date)),
                    'end_date' => date('Y-m-d',strtotime($end_date)),
                    'created_by' => $user_id
                );
            
             $id_grader_exam = $this->grader_exam_model->addNewGraderExam($data);
         }

                for($i=0;$i<count($_POST['stdid']);$i++) {
                    $stdid = $_POST['stdid'][$i];

                    //get Exam sitting 

                    $examSittingSql = $this->grader_exam_model->getExamSittingByStudentId($stdid,$id_exam_event);

                      $datas = array(
                    'id_grader' => $id_grader,
                    'id_grader_exam' => $id_grader_exam,
                    'id_student' => $stdid,
                    'id_exam_event' => $id_exam_event,
                    'id_exam_student_tagging' => $examSittingSql->id,
                    'created_by' => $user_id
                );
            
                    $this->grader_student_tag_model->addNewGraderStudentTag($datas);
                }
                    redirect('/exam/graderExam/list/');

            }
            $examEventDetails = $this->grader_exam_model->getExamEventById($id_exam_event);


            $data['graderList'] = $this->grader_exam_model->graderListByStatus($examEventDetails->id_course);
            $data['examEventDetails'] = $examEventDetails;
            $data['studentList'] = $this->grader_exam_model->fetchStudentByExameventId($id_exam_event);

            $this->global['pageCode'] = 'grader_exam.add';
            $this->global['cairo_pattern_get_filter(pattern)le'] = 'Examination Management System : Edit Exam';
            
            $this->loadViews("grader_exam/add", $this->global, $data, NULL);
        }
    }
    
    function addExam($id = NULL)
    {
        if ($this->checkAccess('grader_exam.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            
            $user_id = $this->session->userId;

            if($this->input->post())
            {

                $id_exam_center = $this->security->xss_clean($this->input->post('id_exam_center'));
                $id_exam_event = $this->security->xss_clean($this->input->post('id_exam_event'));
                $exam_time = $this->security->xss_clean($this->input->post('exam_time'));
                $exam_date = $this->security->xss_clean($this->input->post('exam_date'));

                // $tm_12hr  = date("H:i", strtotime($exam_time));


                $data = array(
                    'id_grader' => $id,
                    'id_exam_center' => $id_exam_center,
                    'id_exam_event' => $id_exam_event,
                    // 'exam_time' => $tm_12hr,
                    // 'exam_date' => date("d-m-Y", strtotime($exam_date)),
                    'status' => 1,
                    'created_by' => $user_id,
                );
            
                $result = $this->grader_exam_model->addNewGraderExam($data);
                if ($result > 0)
                {
                    $this->session->set_flashdata('success', 'New GraderExam created successfully');
                }
                 else
                {
                    $this->session->set_flashdata('error', 'GraderExam creation failed');
                }
                    redirect('/exam/graderExam/addExam/' . $id);
            }
            $data['grader'] = $this->grader_exam_model->getGrader($id);
            $data['graderExamList'] = $this->grader_exam_model->getGraderExamList($id);

            $data['salutationList'] = $this->grader_exam_model->salutationListByStatus('1');
            $data['examCenterList'] = $this->grader_exam_model->examCenterListByStatus('1');

            $this->global['pageCode'] = 'grader_exam.add';
            $this->global['pageTitle'] = 'Examination Management System : Add Exam';
            $this->loadViews("grader_exam/add", $this->global, $data, NULL);
        }
    }


    function edita($id = NULL)
    {
        if ($this->checkAccess('grader_exam.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/exam/grader_exam/list');
            }
            $user_id = $this->session->userId;

            if($this->input->post())
            {
                $salutation = $this->security->xss_clean($this->input->post('salutation'));
                $first_name = $this->security->xss_clean($this->input->post('first_name'));
                $last_name = $this->security->xss_clean($this->input->post('last_name'));
                $email = $this->security->xss_clean($this->input->post('email'));
                $nric = $this->security->xss_clean($this->input->post('nric'));
                $mobile = $this->security->xss_clean($this->input->post('mobile'));
                $password = $this->security->xss_clean($this->input->post('password'));
                $status = $this->security->xss_clean($this->input->post('status'));

                $salutation_data = $this->grader_exam_model->getSalutation($salutation);

                $data = array(
                    'salutation' => $salutation,
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'full_name' => $salutation_data->name . ". " . $first_name . " " . $last_name,
                    // 'email' => $email,
                    'nric' => $nric,
                    'mobile' => $mobile,
                    // 'password' => md5($password),
                    'status' => $status,
                    'updated_by' => $user_id,
                );

                $result = $this->grader_exam_model->editGraderExam($data,$id);
                if ($result) {
                    $this->session->set_flashdata('success', 'GraderExam edited successfully');
                } else {
                    $this->session->set_flashdata('error', 'GraderExam edit failed');
                }
                redirect('/exam/grader_exam/list');
            }

            $data['grader_exam'] = $this->grader_exam_model->getGraderExam($id);
            $data['grader_examModulesList'] = $this->grader_exam_model->getGraderExamModulesList($id);
            $data['salutationList'] = $this->grader_exam_model->salutationListByStatus('1');
            $data['courseList'] = $this->grader_exam_model->courseListByStatus('1');

            // echo "<Pre>";print_r($data['salutationList']);exit();

            $this->global['pageCode'] = 'grader_exam.edit';
            $this->global['pageTitle'] = 'Examination Management System : Edit Exam';
            
            $this->loadViews("grader_exam/edit", $this->global, $data, NULL);
        }
    }

    function addGraderExamModuleData()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        // echo "<Pre>"; print_r($tempData);exit();
        $inserted_id = $this->grader_exam_model->addGraderExamModuleData($tempData);

        if($inserted_id)
        {
            echo "Success";exit;
        }
    }

      function delete($id,$garderexam)
    {
            // echo "<Pre>"; print_r($id);exit();
     $inserted_id = $this->grader_exam_model->deleteStudent($id);
        echo "<script>parent.location='/exam/graderStudentTag/add/$garderexam'</script>";
        echo exit;
    }


    function deleteGraderExamModule($id)
    {
            // echo "<Pre>"; print_r($id);exit();
        $inserted_id = $this->grader_exam_model->deleteGraderExamModule($id);
        echo "Success";exit;
    }



    function getExamEventByExamCenter($id_exam_center)
    {
        $results = $this->grader_exam_model->getExamEventByExamCenter($id_exam_center);

        $table="   
            <script type='text/javascript'>
                 $('select').select2();
             </script>";

        $table.="
        <select name='id_exam_event' id='id_exam_event' class='form-control'>
            <option value=''>Select</option>
            ";

        for($i=0;$i<count($results);$i++)
        {

        $id = $results[$i]->id;
        $exam_date = $results[$i]->exam_date;
        $name = $results[$i]->name;
        $table.="<option value=".$id.">".$exam_date.
                "</option>";

        }
        $table.="

        </select>";

        echo $table;
            exit;
    }

    function getstudentlist(){
           $studentListArray = $this->grader_exam_model->getStudentList();
            

          $table="<table class='table' width='100%'>
                   <tr> <th>Sl No</th><th>Name </th><th>NRIC </th><th>Email </th></tr>";    
          $j = 1;
           for($i=0;$i<count($studentListArray);$i++) {
            $id = $studentListArray[$i]->id;
            $full_name = $studentListArray[$i]->full_name;
            $nric = $studentListArray[$i]->nric;
            $email_id = $studentListArray[$i]->email_id;
              $table.="<tr><td>$j<input type='checkbox' name='selectedStudent[]' 
              value='<?php echo $id;?>' /></td><td>$full_name</td><td>$nric</td><td>$email_id</td></tr>";
              $j++;
           }
           $table.="</table>";

           echo $table;
    }
}
