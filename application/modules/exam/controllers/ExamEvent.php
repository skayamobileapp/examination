<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class ExamEvent extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('exam_event_model');
        $this->load->model('exam_name_model');
        $this->load->model('final_marks_approval_model');
        $this->isLoggedIn();
        error_reporting(0);
    }

    function list()
    {
        if ($this->checkAccess('exam_event.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        { 
            $formData['id_location'] = $this->security->xss_clean($this->input->post('id_location'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['type'] = $this->security->xss_clean($this->input->post('type'));
            $formData['id_exam_center'] = $this->security->xss_clean($this->input->post('id_exam_center'));

            $data['searchParam'] = $formData;

            $data['examEventList'] = $this->exam_event_model->examEventListSearch($formData);
            $data['locationList'] = $this->exam_event_model->examCenterLocationList();
            $data['examCenterList'] = $this->exam_event_model->examCenterList();

            $this->global['pageCode'] = 'exam_event.list';
            $this->global['pageTitle'] = 'Examination Management System : Exam Events';
            $this->loadViews("exam_event/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('exam_event.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                // echo "<Pre>";print_r($this->input->post());exit();


                $user_id = $this->session->userId;

                $id_exam_name = $this->security->xss_clean($this->input->post('id_exam_name'));
                $name = $this->security->xss_clean($this->input->post('name'));
                $exam_date = $this->security->xss_clean($this->input->post('exam_date'));
                $exam_end_date = $this->security->xss_clean($this->input->post('exam_end_date'));
                $id_location = $this->security->xss_clean($this->input->post('id_location'));
                $id_exam_center = $this->security->xss_clean($this->input->post('id_exam_center'));
                $to_tm = $this->security->xss_clean($this->input->post('to_tm'));
                $from_tm = $this->security->xss_clean($this->input->post('from_tm'));
                $type = $this->security->xss_clean($this->input->post('type'));
                $max_count = $this->security->xss_clean($this->input->post('max_count'));
                $id_exam_set = $this->security->xss_clean($this->input->post('id_exam_set'));
                $status = $this->security->xss_clean($this->input->post('status'));
                $id_course = $this->security->xss_clean($this->input->post('id_course'));
                $id_exam_sitting = $this->security->xss_clean($this->input->post('id_exam_sitting'));
                $type_of_exam = $this->security->xss_clean($this->input->post('type_of_exam'));



                $from_tm_12hr  = date("H:i", strtotime($from_tm));
                $to_tm_12hr  = date("H:i", strtotime($to_tm));

              //   $examset = $this->exam_event_model->getExamsetById($id_exam_set);

              //     $durationtime =  '+ '.$examset->duration.' minutes';
              //    $time = strtotime($from_tm);

              // $fromdatetime = date('Y-m-d H:i',strtotime($exam_date.' '.$from_tm_12hr));

              //   $to_tm_12hr =  date('H:i',strtotime($durationtime,strtotime($fromdatetime)));





                
                $data = array(
                   'name' => $name,
                    'exam_date' => date('Y-m-d', strtotime($exam_date)),
                    'exam_end_date' => date('Y-m-d', strtotime($exam_end_date)),
                    'id_location' => $id_location,
                    'id_exam_center' => $id_exam_center,
                    'to_tm' => $to_tm_12hr,
                    'from_tm' => $from_tm_12hr,
                    'type' => $type,
                    'max_count' => $max_count,
                    'id_exam_set'=>$id_exam_set,
                    'id_exam_name'=>$id_exam_name,
                    'status' => $status,
                    'id_exam_sitting' => $id_exam_sitting,
                    'type_of_exam' => $type_of_exam,
                    'id_course' => $id_course,
                    'created_by' => $user_id
                );
            
                // echo "<Pre>";print_r($data);exit();


                $result = $this->exam_event_model->addExamEvent($data);
                if ($result > 0) {
                    $this->session->set_flashdata('success', 'New Exam Event created successfully');
                } else {
                    $this->session->set_flashdata('error', 'Exam Event creation failed');
                }
                redirect('/exam/examEvent/list');
            }

            $data['locationList'] = $this->exam_event_model->examCenterLocationListByStatus('1');
            $data['examCenterList'] = $this->exam_event_model->examCenterListByStatus('1');
            $data['examSetList'] = $this->exam_event_model->getExamSet('1');
            $data['examSitting'] = $this->exam_event_model->getExamSitting();

            $searchparam['name']='';

            $data['examNameList'] = $this->exam_name_model->examNameListSearch($searchparam);

            $this->global['pageCode'] = 'exam_event.add';
            $this->global['pageTitle'] = 'Examination Management System : Add Exam Event';
            $this->loadViews("exam_event/add", $this->global, $data, NULL);
        }
    }



    function viewStudentQuestionSetDownload($id_exam_student_tagging)
    {                    

        $this->getMpdfLibrary();

        $id_grader = $this->session->userdata('id_grader');
        $grader_name = $this->session->userdata('grader_name');
        $grader_email = $this->session->userdata('grader_email');

        $examStudentTaggingDetails = $this->exam_event_model->examStudentTaggingData($id_exam_student_tagging);



        $graderStudentTagDetails = $this->exam_event_model->graderStudentTag($id_exam_student_tagging);


        

        $idexamgrader = $graderStudentTagDetails->id_grader_exam;
        $id_exam_event = $graderStudentTagDetails->id_exam_event;

        $showStudentQuestionSetForGrader = $data['showStudentQuestionSetForGrader'] = $this->exam_event_model->getShowStudentQuestionSetForGrader($id_exam_student_tagging,$idexamgrader);




     
    
        
        $data['idexamgrader'] = $idexamgrader;



        for($i=0;$i<count($data['showStudentQuestionSetForGrader']);$i++) {
             if($data['showStudentQuestionSetForGrader'][$i]->answer_text=='') {
                $id_student_question_set = $data['showStudentQuestionSetForGrader'][$i]->id;
                $datas = array(
                    'id_grader' => $id_grader,
                    'marks' => 0,
                    'id_student_question_set' => $id_student_question_set,
                    'status' => 1,
                    'id_grader_exam' =>$idexamgrader 
                );
                            $this->exam_event_model->deleteStudentQuestionSetById($id_student_question_set);

                $this->exam_event_model->addNewStudentQuestionSetMarks($datas);
                


             }
        }




        $examDetails = $this->exam_event_model->getExamEventForPdf($id_exam_event);


         if($examDetails->mqms_type=='CPIF') {
                 $programmeName = $examDetails->programme_level_ciif."<br/>".$examDetails->programme_name_ciif."<br/>".$examDetails->name;
              } else  {
                 $programmeName = $examDetails->name;
              }



      

        $data['id_exam_event'] = $id_exam_event;
        $data['id_exam_student_tagging'] = $id_exam_student_tagging;

        $data['graderDetails'] = $this->exam_event_model->getExamDetailsByIdGrader($idexamgrader);
        
        


        $data['examStudentTaggingDetails'] = $this->exam_event_model->getExamStudentTagging($id_exam_student_tagging);


          $data['graderStudentTagList'] = $this->exam_event_model->getGraderStudentTagList($id_grader,$id_exam_event,$idexamgrader);

          $studentName  = $data['graderDetails'][0]->coursename;
          $membershipNumber  = $data['showStudentQuestionSetForGrader'][0]->membership_number;

         $examdate = date('d-m-Y',strtotime($examDetails->exam_date));


            $q=1;  
            for($i=0;$i<count($showStudentQuestionSetForGrader);$i++) {  
$question = $showStudentQuestionSetForGrader[$i]->question_number.' - '.$showStudentQuestionSetForGrader[$i]->question;
                $answerscheme = $showStudentQuestionSetForGrader[$i]->answer_scheme;
                $answer_text = $showStudentQuestionSetForGrader[$i]->answer_text;
                $questionmarks = $showStudentQuestionSetForGrader[$i]->questionmarks;
                $j = $i+1;
                $t = count($showStudentQuestionSetForGrader)+1;
                                   

                          $table.= "<table width='100%'>
         <tr>
         <th><img src='https://ciifiscore.ciif-global.org/assets/img/logocpif-1.png' style='height:70px;' align='left'/></th>
           <th style='text-align:right;width:75%;'> Page No - $j/$t </th>
         </tr>
         </table>
         <table width='100%'>
         <tr>
           <th style='background-color:#e9ecef'>Exam Name </th> 
           <th style='background-color:#e9ecef'>Candidate ID </th>
           <th style='background-color:#e9ecef'>Exam Date </th>
         </tr>

         <tr> <td style='text-align:center;'>$programmeName</td><td style='text-align:center;'>$membershipNumber</td><td style='text-align:center;'>$examdate</td>
         </tr>
        

         </table><br/>";



                          $table.= "<table width='100%' border='1' border-collapse: collapse; style='page-break-after:avoid'>
                           <tr><td colspan='4' style='color:blue;'>$question</td></tr>
                           <tr>
                             <th style='width:35%;background-color:#e9ecef'>Answer Scheme</th>
                             <th style='width:35%;background-color:#e9ecef'>Answer</th>
                             <th style='width:15%;background-color:#e9ecef'>Total Marks</th>
                             <th style='width:15%;background-color:#e9ecef'>Marks Obtained</th>
                            </tr>

                           <tr>
                            <td>$answerscheme</td>
                          
                             
                            <td style='color:green;'>$answer_text</td>

                              <td style='valign:center;text-align:center;'>$questionmarks</td>
                              <td></td>


                               

                          </tr>
                          </table>
                         "; 
                       
                      }



  $table.= "<table width='100%'>
         <tr>
           <th style='text-align:right'> Page No - $t/$t </th>
         </tr>
         </table>
         <table width='100%'>
         <tr>
           <th style='background-color:#e9ecef'>Module Name </th> 
           <th style='background-color:#e9ecef'>Candidate ID </th>
           <th style='background-color:#e9ecef'>Exam Event Name </th>
         </tr>

         <tr> <td style='text-align:center;'>$studentName</td><td style='text-align:center;'>$membershipNumber</td><td style='text-align:center;'>$examNamePdf</td>
         </tr>
          <tr>
           <th style='background-color:#e9ecef'>Programme </th> 
           <th style='background-color:#e9ecef'>Level </th>
           <th style='background-color:#e9ecef'>Exam date </th>
         </tr>

         <tr> <td style='text-align:center;'>$studentName</td><td style='text-align:center;'>$examDetails->modulecode</td><td style='text-align:center;'>$examdate</td>
         </tr>

         </table><br/>
         <table width='100%' border='1' border-collapse: collapse;>
          <tr>
                             <td><br/><br/>Total Marks</td>
                             </tr>
                           <tr>
                             <td>Overall Comments</td>
                             </tr>
                             <tr>
                             <td><br/><br/><br/><br/><br/><br/></td>
                             </tr>

                         
                      </table>
                      <table width='100%' border='1' border-collapse: collapse;>
                         
                          <tr>
                             <td><br/><br/><br/>Grader signatory</td>
                             </tr>
                        <tr>
                             <td><br/><br/><br/>Grader name</td>
                             </tr>
                        <tr>
                             <td><br/><br/><br/>Institutions</td>
                             </tr>
                        <tr>
                             <td><br/><br/>Date graded</td>
                             </tr>
                      </table>
                      "; 
                     

                   
                        $table.= "";


                 $mpdf=new \Mpdf\Mpdf(); 

        $mpdf->WriteHTML($table);
            $mpdf->Output('Exam_slip.pdf', 'D');
            exit;
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('exam_event.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/exam/examEvent/list');
            }
            if($this->input->post())
            {
               $user_id = $this->session->userId;

               $user_id = $this->session->userId;

                $id_exam_name = $this->security->xss_clean($this->input->post('id_exam_name'));
                $name = $this->security->xss_clean($this->input->post('name'));
                $exam_date = $this->security->xss_clean($this->input->post('exam_date'));
                $exam_end_date = $this->security->xss_clean($this->input->post('exam_end_date'));
                $id_location = $this->security->xss_clean($this->input->post('id_location'));
                $id_exam_center = $this->security->xss_clean($this->input->post('id_exam_center'));
                $to_tm = $this->security->xss_clean($this->input->post('to_tm'));
                $from_tm = $this->security->xss_clean($this->input->post('from_tm'));
                $type = $this->security->xss_clean($this->input->post('type'));
                $max_count = $this->security->xss_clean($this->input->post('max_count'));
                $id_exam_set = $this->security->xss_clean($this->input->post('id_exam_set'));
                $status = $this->security->xss_clean($this->input->post('status'));
                $id_course = $this->security->xss_clean($this->input->post('id_course'));
                $id_exam_sitting = $this->security->xss_clean($this->input->post('id_exam_sitting'));
                $type_of_exam = $this->security->xss_clean($this->input->post('type_of_exam'));



                $from_tm_12hr  = date("H:i", strtotime($from_tm));
                $to_tm_12hr  = date("H:i", strtotime($to_tm));

              //   $examset = $this->exam_event_model->getExamsetById($id_exam_set);

              //     $durationtime =  '+ '.$examset->duration.' minutes';
              //    $time = strtotime($from_tm);

              // $fromdatetime = date('Y-m-d H:i',strtotime($exam_date.' '.$from_tm_12hr));

              //   $to_tm_12hr =  date('H:i',strtotime($durationtime,strtotime($fromdatetime)));





                
                $data = array(
                   'name' => $name,
                    'exam_date' => date('Y-m-d', strtotime($exam_date)),
                    'exam_end_date' => date('Y-m-d', strtotime($exam_end_date)),
                    'id_location' => $id_location,
                    'id_exam_center' => $id_exam_center,
                    'to_tm' => $to_tm_12hr,
                    'from_tm' => $from_tm_12hr,
                    'type' => $type,
                    'max_count' => $max_count,
                    'id_exam_set'=>$id_exam_set,
                    'id_exam_name'=>$id_exam_name,
                    'status' => $status,
                    'id_exam_sitting' => $id_exam_sitting,
                    'type_of_exam' => $type_of_exam,
                    'id_course' => $id_course,
                    'created_by' => $user_id
                );            
               
                $result = $this->exam_event_model->editExamEvent($data,$id);
                if ($result) {
                    $this->session->set_flashdata('success', 'Exam Event edited successfully');
                } else {
                    $this->session->set_flashdata('error', 'Exam Event edit failed');
                }
                redirect('/exam/examEvent/list');
            }

            $data['locationList'] = $this->exam_event_model->examCenterLocationListByStatus('1');
            $data['examCenterList'] = $this->exam_event_model->examCenterListByStatus('1');
            $searchparam['name']='';
            $data['examSitting'] = $this->exam_event_model->getExamSitting();

            $data['examNameList'] = $this->exam_name_model->examNameListSearch($searchparam);
            $data['getExamEvent'] = $this->exam_event_model->getExamEvent($id);
            
            if(!$data['getExamEvent']->id_exam_set) {
                $data['getExamEvent']->id_exam_set = 0;
            }
            if(!$data['getExamEvent']->type_of_exam) {
                $data['getExamEvent']->type_of_exam = 0;
            }
            // echo "<Pre>";print_r($data['getExamEvent']);exit();

            $this->global['pageTitle'] = 'Examination Management System : Edit Exam Event';
            $this->global['pageCode'] = 'exam_event.edit';

            $this->loadViews("exam_event/edit", $this->global, $data, NULL);
        }
    }


    function students($id) {


                    $data['examStudentTaggingList'] = $this->exam_event_model->examStudentTaggingListSearch($id);
  $this->global['pageTitle'] = 'Examination Management System : Edit Exam Event';
            $this->global['pageCode'] = 'exam_event.edit';

            $data['idexamevent'] = $id;

            $this->loadViews("exam_event/student", $this->global, $data, NULL);

    }

    function generateToken($id) {

                    $examStudentTaggingList = $this->exam_event_model->examStudentTaggingListSearch($id);
                   for($i=0;$i<count($examStudentTaggingList);$i++) {
                     if($examStudentTaggingList[$i]->token=='') {

                        $token = rand(100000,999999);
                        $data['token'] = $token;
                        $this->exam_event_model->updateToken($data,$examStudentTaggingList[$i]->id);
                     }
                   }

            echo "<script>alert('Token Generated')</script>";
            echo "<script>parent.location='/exam/examEvent/students/$id'</script>";
    }


    function getCentersByLocatioin($id_location)
    {
            $results = $this->exam_event_model->getCentersByLocatioin($id_location);

            $table="   
                <script type='text/javascript'>
                     $('select').select2();
                 </script>
         ";

            $table.="
            <select name='id_exam_center' id='id_exam_center' class='form-control'>
                <option value=''>Select</option>
                ";

            for($i=0;$i<count($results);$i++)
            {

            $id = $results[$i]->id;
            $name = $results[$i]->name;
            $table.="<option value=".$id.">".$name.
                    "</option>";

            }
            $table.="

            </select>";

            echo $table;
            exit;
    }


     function mcqlist($id_exam_student_tagging,$backid){

         $data['backid'] = $backid;



          $data['studentDetails'] = $this->final_marks_approval_model->getstudentDetailsByExamsittingId($id_exam_student_tagging);

          $data['examQuestionsByData'] = $this->final_marks_approval_model->exammcaQuestionsByData($id_exam_student_tagging);
                      $this->loadViews("exam_event/mcqlist", $this->global, $data, NULL);

    }

     function answerlist($id_exam_student_tagging,$backid){

        
         $data['backid'] = $backid;
         $data['id_exam_student_tagging'] = $id_exam_student_tagging;
          $data['studentDetails'] = $this->final_marks_approval_model->getstudentDetailsByExamsittingId($id_exam_student_tagging);


        $data['showStudentQuestionSetForGrader'] = $this->final_marks_approval_model->getShowStudentQuestionSetForGrader($id_exam_student_tagging);

                      $this->loadViews("exam_event/answerlist", $this->global, $data, NULL);




    }





     function getexamsetlist(){
         $idcourse = $_POST['id'];
            $results = $this->exam_event_model->getExamSetByCourse($idcourse);


        

            $table.="
            <select name='id_exam_set' id='id_exam_set' class='form-control' required>
                <option value=''>Select</option>
                ";

            for($i=0;$i<count($results);$i++)
            {

            $id = $results[$i]->id;
            $name = $results[$i]->name;
            $checked="";
            if($id==$selectedid) {
                $checked ="selected=selected";
            }
            $table.="<option value=".$id." $checked>".$name.
                    "</option>";

            }
            $table.="

            </select>";

            echo $table;
            exit;

     }


     function getmoduledropdown() {
        $type = $_POST['id'];
        if($_POST['selectedid']) {
            $selectedid = $_POST['selectedid'];
        } else {
            $selectedid = 0;
        }

         $results = $this->exam_event_model->getCourseListByType($type);

         

            $table.="
            <select name='id_course' id='id_course' class='form-control' required onchange='getexamset()'>
                <option value=''>Select</option>
                ";

            for($i=0;$i<count($results);$i++)
            {

            $id = $results[$i]->id;
            $name = $results[$i]->name;
            $checked="";
            if($id==$selectedid) {
                $checked ="selected=selected";
            }
            $table.="<option value=".$id." $checked>".$name.
                    "</option>";

            }
            $table.="

            </select>";

            echo $table;
            exit;


    }

}
