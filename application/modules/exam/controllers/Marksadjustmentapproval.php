<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Marksadjustmentapproval extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('marksadjustment_model');
                $this->load->model('grader_marks_approval_model');
         error_reporting();
        
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('marksadjustmentapproval.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $data['graderStudentTagList'] = $this->marksadjustment_model->listofmarksadjustmentforapproval();

            $this->global['pageCode'] = 'marksadjustmentapproval.list';
            $this->global['pageTitle'] = 'Examination Management System : Grader List';
            $this->loadViews("marksadjustmentapproval/list", $this->global, $data, NULL);
        }
    }
    
    function add($id_exam_student_tagging)
    {
        if ($this->checkAccess('marksadjustmentapproval.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

              if($this->input->post())
            {
                $adjmarks = $this->security->xss_clean($this->input->post('adjmarks'));
                $questionset = $this->security->xss_clean($this->input->post('questionset'));


                $totalMCQ = $this->grader_marks_approval_model->totalonlyMCQQuestionAnswered($id_exam_student_tagging);
                $totalMCQAnswered = $totalMCQ[0]->totalmcq;


   $totalmarkedQuestionadj = $this->marksadjustment_model->totalMarksAdustmentList($id_exam_student_tagging);

                            $answermarksadj = 0;
                            for($m=0;$m<count($totalmarkedQuestionadj);$m++) {
                              $answermarksadj = $answermarksadj + $totalmarkedQuestionadj[$m]->answermarks;                              

                            }
                                      $totalEssayMarks = $answermarksadj    ;

                  
                  $totalMarksObtained = $totalMCQAnswered + $totalEssayMarks;
               

                      $studentexamtagging = array(
                            'result_status'=>15,
                            'mcq_total'=>$totalMCQAnswered,
                            'essay_total'=>$totalEssayMarks,
                            'final_total'=>$totalMarksObtained,
                         );
                        $this->grader_marks_approval_model->updateExamStudentTagging($studentexamtagging,$id_exam_student_tagging);
                
            }
        $data['showStudentQuestionSetForGrader'] = $this->marksadjustment_model->getShowStudentQuestionSetForGrader($id_exam_student_tagging);


                       $this->global['pageCode'] = 'marksadjustment.list';
            $this->global['pageTitle'] = 'Examination Management System : Grader List';

            $this->loadViews("marksadjustmentapproval/add", $this->global, $data, NULL);
        }
    }




  
}
