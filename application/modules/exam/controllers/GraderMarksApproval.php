<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class GraderMarksApproval extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('grader_marks_approval_model');
        $this->load->model('grader_exam_model');
        $this->load->model('grader_student_tag_model');

        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('grader_marks_approval.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name')); 
            $data['searchParam'] = $formData;

            $data['graderStudentList'] = $this->grader_marks_approval_model->graderStudentListSearch($formData);

            $this->global['pageCode'] = 'grader_marks_approval.list';
            $this->global['pageTitle'] = 'Examination Management System : Grader Exam List';
            $this->loadViews("grader_marks_approval/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('grader_marks_approval.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            
            $user_id = $this->session->userId;

            if($this->input->post())
            {
                
                $id_exam_student_taggings = $this->security->xss_clean($this->input->post('id_exam_student_tagging'));
                $type = $this->security->xss_clean($this->input->post('type'));
                $id_grader_two = $this->security->xss_clean($this->input->post('id_grader_two'));
                $start_date = $this->security->xss_clean($this->input->post('start_date'));
                $end_date = $this->security->xss_clean($this->input->post('end_date'));

// print_r($id_exam_student_taggings);exit;


               for($i=0;$i<count($id_exam_student_taggings);$i++) {


                   // get mcq marks

 





                   // total marks
                    if($type=='11') {
                        // print_R($studentexamtagging);exit;
                          $studentexamtagging = array(
                            'result_status'=>11
                         );

                        
                        $this->grader_marks_approval_model->updateExamStudentTagging($studentexamtagging,$id_exam_student_taggings[$i]);                    
                    }
                    if($type=='15') {

                    $totalMCQ = $this->grader_marks_approval_model->totalonlyMCQQuestionAnswered($id_exam_student_taggings[$i]);
                    $totalMCQAnswered = $totalMCQ[0]->totalmcq;
                    $totalmarkedQuestion = $this->grader_marks_approval_model->totalMCQQuestionMarked($id_exam_student_taggings[$i]);
                            $answermarks = 0;
                    for($m=0;$m<count($totalmarkedQuestion);$m++) {
                              $answermarks = $answermarks + $totalmarkedQuestion[$m]->answermarks;                              
                    }
                    $totalEssayMarks = $answermarks;
                    $totalMarksObtained = $totalMCQAnswered + $totalEssayMarks;                        
                        // print_R($studentexamtagging);exit;
                         $studentexamtagging = array(
                            'result_status'=>15,
                            'mcq_total'=>$totalMCQAnswered,
                            'essay_total'=>$totalEssayMarks,
                            'final_total'=>$totalMarksObtained,
                         );

                       
                        $this->grader_marks_approval_model->updateExamStudentTagging($studentexamtagging,$id_exam_student_taggings[$i]);                    
                    }
                    if($type=='13') {


                        $studentexamtagging = array(
                            'result_status'=>13
                         );
                        $this->grader_marks_approval_model->updateExamStudentTagging($studentexamtagging,$id_exam_student_taggings[$i]);


                        $graderexamdetsils = $this->grader_student_tag_model->getStudentDetailsbyidexamstudenttagging($id_exam_student_taggings[$i]);



                        $datas = array(
                            'id_grader' => $id_grader_two,
                            'id_exam_center' => $id_exam_center,
                            'id_exam_event' => $graderexamdetsils->id_exam_event,
                            'status' => 1,
                            'id_exam_name' =>$id_exam_name,
                            'created_by' => $user_id,
                            'start_date' => $start_date,
                            'end_date' => $end_date,
                            'student_selection' => '1'
                        );

                        $last_id = $this->grader_exam_model->addNewGraderExam($datas);
   $id_exam_student_tagging= 0;

                    $resultexamtagging = $this->grader_student_tag_model->getIdExamStudentTagging($graderexamdetsils->id_student,$graderexamdetsils->id_exam_event);

                        $id_exam_student_tagging = $resultexamtagging->id;

                         $data = array(
                        'id_grader' => $id_grader_two,
                        'id_exam_center' => 0,
                        'id_exam_event' => $graderexamdetsils->id_exam_event,
                        'id_grader_exam' => $last_id,
                        'id_student' =>$graderexamdetsils->id_student,
                        // 'exam_time' => $tm_12hr,
                        // 'exam_date' => date("d-m-Y", strtotime($exam_date)),
                        'status' => 0,
                        'created_by' => $user_id,
                        'id_exam_student_tagging' =>$id_exam_student_tagging
                    );
                
                    $last_id = $this->grader_student_tag_model->addNewGraderStudentTag($data);


                

                

                    }

                }


                if ($result > 0)
                {
                    $this->session->set_flashdata('success', 'New GraderExam created successfully');
                }
                 else
                {
                    $this->session->set_flashdata('error', 'GraderExam creation failed');
                }
                    redirect('/exam/graderMarksApproval/add');
            }


            $data['graderList'] = $this->grader_marks_approval_model->graderListByStatus('1');
            $data['examCenterList'] = $this->grader_marks_approval_model->examCenterListByStatus('1');

            $this->global['pageCode'] = 'grader_marks_approval.add';
            $this->global['pageTitle'] = 'Examination Management System : Add Exam';
            $this->loadViews("grader_marks_approval/add", $this->global, $data, NULL);
        }
    }


    function getExamCenterByGrader($id)
    {
        $results = $this->grader_marks_approval_model->getExamCenterByGrader($id);

        // echo "<Pre>";print_r($results);exit();

        $table="   
            <script type='text/javascript'>
                 $('select').select2();
             </script>";

        $table.="
        <select name='id_exam_center' id='id_exam_center' class='form-control' onchange='getExamEventByExamCenterNIdGrader()'>
            <option value=''>Select</option>
            ";

        for($i=0;$i<count($results);$i++)
        {

        $id = $results[$i]->id;
        // $exam_date = $results[$i]->exam_date;
        $name = $results[$i]->name;
        $table.="<option value=".$id.">". $name.
                "</option>";

        }
        $table.="

        </select>";

        echo $table;
            exit;
    }

    function getExamCenterByExamCenterNIdGrader()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        $results = $this->grader_student_tag_model->getExamCenterByExamCenterNIdGrader($tempData);

        $table="   
            <script type='text/javascript'>
                 $('select').select2();
             </script>";

        $table.="
        <select name='id_exam_event' id='id_exam_event' class='form-control'>
            <option value=''>Select</option>
            ";

        for($i=0;$i<count($results);$i++)
        {

        $id = $results[$i]->id;
        $exam_date = $results[$i]->exam_date;
        $name = $results[$i]->name;
        $table.="<option value=".$id.">".$exam_date . " - " . $name.
                "</option>";

        }
        $table.="

        </select>";

        echo $table;
            exit;
    }



    function getExamEventByExamCenter($id)
    {
        $results = $this->grader_marks_approval_model->getExamEventByExamCenter($id);

        $table="   
            <script type='text/javascript'>
                 $('select').select2();
             </script>";

        $table.="
        <select name='id_exam_event' id='id_exam_event' class='form-control'>
            <option value=''>Select</option>
            ";

        for($i=0;$i<count($results);$i++)
        {

        $id = $results[$i]->id;
        $exam_date = $results[$i]->exam_date;
        $name = $results[$i]->name;
        $table.="<option value=".$id.">".$exam_date . " - " . $name.
                "</option>";

        }
        $table.="

        </select>";

        echo $table;
            exit;
    }

    function searchStudents()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        

        $student_data = $this->grader_marks_approval_model->getGraderStudentTagListByData($tempData);

        if(!empty($student_data))
        {

       
           $table = "<h4 class='form-group-title'> Select Students For Grader Marks Approval</h4>
         <div class='custom-table'>
         <table  class='table' id='list-table'>
                  <tr>
                    <th>Sl. No</th>
                    <th>Student Name</th>
                    <th>Student Email</th>
                    <th>Student NRIC</th>
                    <th>Total Question</th>
                    <th>Total Marks</th>
                    <th>Obtained Marks</th>
                    <th style='text-align: center;'><input type='checkbox' id='checkAll' name='checkAll'> Check All</th>
                </tr>";

            for($i=0;$i<count($student_data);$i++)
            {
                $id = $student_data[$i]->id;
                $id_student = $student_data[$i]->id_student;
                $full_name = $student_data[$i]->full_name;
                $student_email = $student_data[$i]->full_name;
                $nric = $student_data[$i]->nric;



                            $totalmarkedQuestion = $this->grader_marks_approval_model->totalMCQQuestionMarked($student_data[$i]->id_exam_student_tagging);
                 // print_r($totalmarkedQuestion);exit;

                            $originalMarks = 0;
                            $answermarks = 0;
                            for($m=0;$m<count($totalmarkedQuestion);$m++) {
                              $originalMarks = $originalMarks + $totalmarkedQuestion[$m]->originalmarks;
                              $answermarks = $answermarks + $totalmarkedQuestion[$m]->answermarks;                              

                            }

                            $markedQuestionCount = count($totalmarkedQuestion);






                $j = $i+1;
                $table .= "
                <tr>
                    <td>$j</td>
                    <td>$full_name</td>
                    <td>$student_email</td>                    
                    <td>$nric</td>                    
                      <td>$markedQuestionCount</td>
                            <td>$originalMarks</td>
                            <td>$answermarks</td>
                          
                    
                    <td class='text-center'>
                        <input type='checkbox' id='id_exam_student_tagging[]' name='id_exam_student_tagging[]' class='check' value='". $student_data[$i]->id_exam_student_tagging ."'>
                    </td>
               
                </tr>";
            }

         $table.= "</table>";
        }
        else
        {
            $table= "<h4> No Data Found For Your Search</h4>";
        }
        echo $table;exit;
    }
}
