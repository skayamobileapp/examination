<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class SecondGraderStudentTag extends BaseController
{
    public function __construct()
    {

        parent::__construct();
        $this->load->model('second_grader_student_tag_model');
        $this->load->model('grader_exam_model');
        $this->load->model('exam_name_model');

        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('grader_student_tag.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name')); 
            $data['searchParam'] = $formData;

            $data['graderStudentList'] = $this->second_grader_student_tag_model->graderStudentListSearch($formData);

            $this->global['pageCode'] = 'grader_student_tag.list';
            $this->global['pageTitle'] = 'Examination Management System : Grader Exam List';
            $this->loadViews("second_grader_student_tag/list", $this->global, $data, NULL);
        }
    }
    
    function add($id_grader = NULL)
    {
        if ($this->checkAccess('grader_student_tag.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            
            $user_id = $this->session->userId;

            $graderExam = $this->second_grader_student_tag_model->getGraderExam($id_grader);
            // $id_grader = $graderExam->id_grader;
            $data['examCenterList'] = $this->grader_exam_model->examCenterListByStatus('1');
            $data['examNameList'] = $this->exam_name_model->examNameList();

            if($this->input->post())
            {
                
                $id_students = $this->security->xss_clean($this->input->post('id_student'));


                $id_exam_center = $this->security->xss_clean($this->input->post('id_exam_center'));
                $id_exam_event = $this->security->xss_clean($this->input->post('id_exam_event'));

                // $tm_12hr  = date("H:i", strtotime($exam_time));


                foreach ($id_students as $id_student)
                {
                    $data = array(
                        'id_grader' => $id_grader,
                        'id_exam_center' => $id_exam_center,
                        'id_exam_event' => $id_exam_event,
                        'id_student' => $id_student,
                        'status' => 0,
                        'created_by' => $user_id,
                    );
                
                    $result = $this->second_grader_student_tag_model->addNewGraderStudentTag($data);
                }

                if ($result > 0)
                {
                    $this->session->set_flashdata('success', 'New GraderExam created successfully');
                }
                 else
                {
                    $this->session->set_flashdata('error', 'GraderExam creation failed');
                }
                    redirect('/exam/secondGraderStudentTag/add/' . $id_grader_exam);
            }


            // $data['grader'] = $this->grader_student_tag_model->getGrader($id_grader);
         $data['graderStudentTagList'] = $this->second_grader_student_tag_model->getGraderStudentTagList($id_grader);

             // echo "<Pre>";print_r($data);exit();
            // // echo "<Pre>";print_r($data['graderStudentTagList']);exit();

            // $data['salutationList'] = $this->grader_student_tag_model->salutationListByStatus('1');
            // $data['examCenterList'] = $this->grader_student_tag_model->examCenterListByGraderId($id_grader);
            // // $data['examCenterList'] = $this->grader_student_tag_model->examCenterListByStatus('1');


            $this->global['pageCode'] = 'grader_student_tag.add';
            $this->global['pageTitle'] = 'Examination Management System : Add Exam';
            $this->loadViews("second_grader_student_tag/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('grader_student_tag.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/exam/grader_student_tag/list');
            }
            $user_id = $this->session->userId;

            if($this->input->post())
            {
                $salutation = $this->security->xss_clean($this->input->post('salutation'));
                $first_name = $this->security->xss_clean($this->input->post('first_name'));
                $last_name = $this->security->xss_clean($this->input->post('last_name'));
                $email = $this->security->xss_clean($this->input->post('email'));
                $nric = $this->security->xss_clean($this->input->post('nric'));
                $mobile = $this->security->xss_clean($this->input->post('mobile'));
                $password = $this->security->xss_clean($this->input->post('password'));
                $status = $this->security->xss_clean($this->input->post('status'));

                $salutation_data = $this->grader_student_tag_model->getSalutation($salutation);

                $data = array(
                    'salutation' => $salutation,
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'full_name' => $salutation_data->name . ". " . $first_name . " " . $last_name,
                    // 'email' => $email,
                    'nric' => $nric,
                    'mobile' => $mobile,
                    // 'password' => md5($password),
                    'status' => $status,
                    'updated_by' => $user_id,
                );

                $result = $this->grader_student_tag_model->editGraderExam($data,$id);
                if ($result) {
                    $this->session->set_flashdata('success', 'GraderExam edited successfully');
                } else {
                    $this->session->set_flashdata('error', 'GraderExam edit failed');
                }
                redirect('/exam/grader_student_tag/list');
            }

            $data['grader_student_tag'] = $this->grader_student_tag_model->getGraderExam($id);
            $data['grader_student_tagModulesList'] = $this->grader_student_tag_model->getGraderExamModulesList($id);
            $data['salutationList'] = $this->grader_student_tag_model->salutationListByStatus('1');
            $data['courseList'] = $this->grader_student_tag_model->courseListByStatus('1');

            // echo "<Pre>";print_r($data['salutationList']);exit();

            $this->global['pageCode'] = 'grader_student_tag.edit';
            $this->global['pageTitle'] = 'Examination Management System : Edit Exam';
            
            $this->loadViews("grader_student_tag/edit", $this->global, $data, NULL);
        }
    }

    function addGraderExamModuleData()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        // echo "<Pre>"; print_r($tempData);exit();
        $inserted_id = $this->grader_student_tag_model->addGraderExamModuleData($tempData);

        if($inserted_id)
        {
            echo "Success";exit;
        }
    }

    function deleteGraderExamModule($id)
    {
            // echo "<Pre>"; print_r($id);exit();
        $inserted_id = $this->grader_student_tag_model->deleteGraderExamModule($id);
        echo "Success";exit;
    }



    function getExamEventByExamCenterNIdGrader()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        $results = $this->grader_student_tag_model->getExamEventByExamCenter($tempData);

        $table="   
            <script type='text/javascript'>
                 $('select').select2();
             </script>";

        $table.="
        <select name='id_exam_event' id='id_exam_event' class='form-control'>
            <option value=''>Select</option>
            ";

        for($i=0;$i<count($results);$i++)
        {

        $id = $results[$i]->id;
        $exam_date = $results[$i]->exam_date;
        $name = $results[$i]->name;
        $table.="<option value=".$id.">".$exam_date . " - " . $name.
                "</option>";

        }
        $table.="

        </select>";

        echo $table;
            exit;
    }

    function searchStudents()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        
        $student_data = $this->second_grader_student_tag_model->studentSearch($tempData);

        // echo "<Pre>";print_r($student_data);exit();
        if(!empty($student_data))
        {

         $table = "
         <br>
         <h4> Select Students For Grader Tagging</h4>
         <table  class='table' id='list-table'>
                  <tr>
                    <th>Sl. No</th>
                    <th>Student Name</th>
                    <th>Student NRIC</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>City</th>
                    <th>Status</th>
                    <th style='text-align: center;'><input type='checkbox' id='checkAll' name='checkAll'> Check All</th>
                </tr>";

            for($i=0;$i<count($student_data);$i++)
            {
                $id = $student_data[$i]->id;
                $full_name = $student_data[$i]->full_name;
                $nric = $student_data[$i]->nric;
                $email_id = $student_data[$i]->email_id;
                $phone = $student_data[$i]->phone;
                $permanent_zipcode = $student_data[$i]->permanent_zipcode;
                $city = $student_data[$i]->permanent_city;
                $status = $student_data[$i]->applicant_status;


                $j = $i+1;
                $table .= "
                <tr>
                    <td>$j</td>
                    <td>$full_name</td>
                    <td>$nric</td>                    
                    <td>$email_id</td>
                    <td>$phone</td>
                    <td>$city</td>
                    <td>$status</td>                 
                    
                    <td class='text-center'>
                        <input type='checkbox' id='id_student[]' name='id_student[]' class='check' value='".$id."'>
                    </td>
               
                </tr>";
            }

         $table.= "</table>";
        }
        else
        {
            $table= "<h4> No Data Found For Your Search</h4>";
        }
        echo $table;exit;
    }
}
