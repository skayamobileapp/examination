<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH . '/libraries/BaseController.php';
require 'vendor/autoload.php';

class Api extends BaseController
{
         protected $token;


    // public function __construct()
    // {
    //     parent::__construct();
    //     $this->load->model('exam_api_model');
    //     $this->isLoggedIn();
    // }
    

     public function __construct()
     {
         error_reporting(0);
         parent::__construct();
         $this->load->model('exam_api_model');
      $client = new GuzzleHttp\Client(['base_uri' => 'https://register.ciif-global.org/oauth/token']);
        // $headers = [
        //     'Authorization' => 'Bearer ' . $token,        
        // ];
        $res = $client->request('POST', 'token', [
             'form_params' => [
            'grant_type' => 'client_credentials',
            'client_id'=> 3,
            'client_secret'=>'HbKKl7GV2Yo61uxNp2xJcS4XDQoImauZf30uluZF'
        ]]);
        $data = json_decode($res->getBody());
        
        $this->token = $data->access_token;
     }
 
 
    
    
    
        function postschedule($idscheduleId)
    {
        $client = new GuzzleHttp\Client(['base_uri' => 'https://register.ciif-global.org/api/exam/mark']);
        $headers = [
            'Authorization' => 'Bearer ' . $this->token,        
        ];
        
        
        $getAllMarks = $this->exam_api_model->getmarks($idscheduleId);
        

       for($i=0;$i<count($getAllMarks);$i++) {
           
           
           if($getAllMarks[$i]->attendence_status=='1') {
           $final_total = 0;
           
           $exam_schedule_id = $getAllMarks[$i]->mqm_schedule_id;
           $user_module_id = $getAllMarks[$i]->mqms_user_module_id;
           $final_total = $getAllMarks[$i]->final_total;

                $res = $client->request('POST', 'mark', [
                    'headers' => $headers,
                     'form_params' => [
                    'exam_schedule_id' => $exam_schedule_id,
                    'assessment_component_id' => '1',
                    'user_module_id' => $user_module_id,
                    'mark_percentage' => $final_total
                    ]
                   
            
                ]);
                
        
                $data = json_decode($res->getBody())->data;
                
           }
                
                       

       }
        
    }

      function postscheduleother($idscheduleId)
    {
        $client = new GuzzleHttp\Client(['base_uri' => 'https://register.ciif-global.org/api/exam/mark-other']);
        $headers = [
            'Authorization' => 'Bearer ' . $this->token,        
        ];
        
        
        $getAllMarks = $this->exam_api_model->getmarks($idscheduleId);
        

       for($i=0;$i<count($getAllMarks);$i++) {
           
           
           if($getAllMarks[$i]->attendence_status=='1') {
           $final_total = 0;
           
           $exam_schedule_id = $getAllMarks[$i]->mqm_schedule_id;
           $user_module_id = $getAllMarks[$i]->mqms_user_module_id;
           $final_total = $getAllMarks[$i]->final_total;

                $res = $client->request('POST', 'mark-other', [
                    'headers' => $headers,
                     'form_params' => [
                    'exam_schedule_id' => $exam_schedule_id,
                    'assessment_component_id' => '1',
                    'user_masterclass_id' => $user_module_id,
                    'mark_percentage' => $final_total
                    ]
                   
            
                ]);
                
        
                $data = json_decode($res->getBody())->data;

                console.log($data);
                
           }
                
                       

       }
        
    }
    
    
      function lock($idscheduleId)
    {
        $client = new GuzzleHttp\Client(['base_uri' => 'https://register.ciif-global.org/api/exam/attendance/lock']);
        $headers = [
            'Authorization' => 'Bearer ' . $this->token,        
        ];
        
        $res = $client->request('POST', 'lock', [
            'headers' => $headers,
             'form_params' => [
            'exam_schedule_id' => $idscheduleId,
            'user_id' => $_SESSION['mqms_id']
            ]
        ]);
        
        $data = json_decode($res->getBody())->data;
    }
    
    
    
      function approvemarks($idscheduleId)
    {
        $client = new GuzzleHttp\Client(['base_uri' => 'https://register.ciif-global.org/api/exam/marks/lock-approve']);
        $headers = [
            'Authorization' => 'Bearer ' . $this->token,        
        ];
        
        $res = $client->request('POST', 'lock-approve', [
            'headers' => $headers,
             'form_params' => [
            'exam_schedule_id' => $idscheduleId,
            'user_id' => $_SESSION['mqms_id'],
            'remarks'=>'Approved'
            ]
        ]);
        
        $data = json_decode($res->getBody())->data;
    }


      function approvemarksother($idscheduleId)
    {
        $client = new GuzzleHttp\Client(['base_uri' => 'https://register.ciif-global.org/api/exam/marks-other/lock-approve']);
        $headers = [
            'Authorization' => 'Bearer ' . $this->token,        
        ];
        
        $res = $client->request('POST', 'lock-approve', [
            'headers' => $headers,
             'form_params' => [
            'exam_schedule_id' => $idscheduleId,
            'user_id' => $_SESSION['mqms_id'],
            'remarks'=>'Approved'
            ]
        ]);
        
        $data = json_decode($res->getBody())->data;
        
    }
    
    
    
        function postattendance($idscheduleId)
    {
        $client = new GuzzleHttp\Client(['base_uri' => 'https://register.ciif-global.org/api/exam/attendances']);
        $headers = [
            'Authorization' => 'Bearer ' . $this->token,        
        ];
        
        
        $getAllMarks = $this->exam_api_model->getmarks($idscheduleId);
        
        
           $exam_schedule_id = $idscheduleId;

       for($i=0;$i<count($getAllMarks);$i++) {
           
           $user_module_id = $getAllMarks[$i]->mqms_user_module_id;
           $final_total = $getAllMarks[$i]->final_total;
           $attendance_reason = $getAllMarks[$i]->attendance_reason;
           
           
           
           if($getAllMarks[$i]->attendence_status=='1') {
               $attendance='ATTENDED';
           } else if ($getAllMarks[$i]->attendence_status=='2'){
                $attendance='ABSENT';
           } else {
               $attendance='ABSENT_WITH_REASON';
           }
           

        $res = $client->request('POST', 'attendances', [
            'headers' => $headers,
             'form_params' => [
             'exam_schedule_id' => $exam_schedule_id,
             'students[0][user_module_id]' => $user_module_id,
             'students[0][status]' => $attendance,
             'students[0][reason]' => $attendance_reason  
            ]
           
    
        ]);
        
                $data = json_decode($res->getBody())->data;
       }

    }
    
    
     function getuser(){
        
        
        $client = new GuzzleHttp\Client(['base_uri' => 'https://register.ciif-global.org/api/exam/user']);
        $headers = [
            'Authorization' => 'Bearer ' . $this->token,        
        ];
        $res = $client->request('GET', 'user', [
            'headers' => $headers
        ]);
        $data = json_decode($res->getBody())->data;
        print_r($data);exit;
        
            
    }


    function getAllModules(){
        

        $client = new GuzzleHttp\Client(['base_uri' => 'https://register.ciif-global.org/api/exam/modules']);
        $headers = [
            'Authorization' => 'Bearer ' . $this->token,        
        ];
        $res = $client->request('GET', 'modules', [
            'headers' => $headers
        ]);


        $data = json_decode($res->getBody())->data;;
        
        foreach($data as $row){


            $moduleData = array(
                'name'=>$row->name,
                'code'=>$row->code,
                'mqms_id'=>$row->id,
                'created_by'=>1,
                'created_dt_tm'=>date('Y-m-d H:i:s')
            );

                $this->exam_api_model->addModule($moduleData);

        }
       
            
    }
    
    
    function result($id){
         $this->global['pageCode'] = 'api.fetchschedule';
        $this->global['pageTitle'] = 'Campus Management System : Schedule Fetch API';
        
        $this->postattendance($id);
        
        
        $this->lock($id);
        
        $this->postschedule($id);
        
        $this->approvemarks($id);



 echo "Marks uploaded";
        exit;
                $this->loadViews("api/result", $this->global, NULL, NULL);
    }

     function resultmaster($id){
         $this->global['pageCode'] = 'api.fetchschedule';
        $this->global['pageTitle'] = 'Campus Management System : Schedule Fetch API';
        
        $this->postscheduleother($id);

        
        $this->approvemarksother($id);
        
        echo "Marks uploaded";
        exit;
    }


    function fetchschedule()
    {



        $client = new GuzzleHttp\Client(['base_uri' => 'https://register.ciif-global.org/api/exam/schedule']);
        $headers = [
            'Authorization' => 'Bearer ' . $this->token,        
        ];
        $res = $client->request('GET', 'schedule', [
            'headers' => $headers
        ]);
        $data = json_decode($res->getBody())->data;





        foreach($data as $row){


            if($row->provider=='CIIFiScore') {

                if($row->type=='CPIF') {

                    if(count($row->user_module)==0)
                         continue;

                    $mqmsscheduleId = $row->id;
                    $moduleData = array();
                    $moduleDataname = $row->module[0]->name;
                    $moduleDatacode = $row->module[0]->code;
                    $moduleDatamqms_id = $row->module[0]->id;

                    $programmeName = $row->programme->name;
                    $level_name = $row->programme->level_name;
                    
                    // check if the imqsid is already present
                    $courseData = $this->exam_api_model->getIMQSCourseId($row->module[0]->id);
                    $venueData = array();
                    $venueData['name'] = $row->venue->name;
                    $examcenterid = $this->exam_api_model->editExamCenterName($row->venue->name);

                   

                    $scheduleData = array();
                    
                    $examstartDate = new DateTime($row->exam_start_date, new DateTimeZone('UTC'));

                    // change the timezone of the object without changing its time
                    $examstartDate->setTimezone(new DateTimeZone('Asia/Kuala_Lumpur'));
                    $examdate = $examstartDate->format('Y-m-d');       
                    $currentdate = date('Y-m-d');
                     if(date('Y-m-d',strtotime($examdate))<date('Y-m-d',strtotime($currentdate))) {

                        continue;
                    }


                    $examEndDate = new DateTime($row->exam_end_date, new DateTimeZone('UTC'));
                    // change the timezone of the object without changing its time
                    $examEndDate->setTimezone(new DateTimeZone('Asia/Kuala_Lumpur'));

                    $examdate = $examEndDate->format('Y-m-d');
                    $examenddate = date('Y-m-d',strtotime($row->exam_end_date));
                    $idexamsitting = date('m',strtotime($row->exam_start_date));
                    $moduleId = $courseData->id;
                    $examcenterid = $examcenterid->id;
                    //check exam event ID
                    $exameventid = $this->exam_api_model->getExamEventIdAPI($examdate,$moduleId);
                    $examEventId = $exameventid->id;
                    $from_tm_12hr  = $examstartDate->format('H:i');
                    $to_tm_12hr  = $examEndDate->format('H:i');

                    $examexists = $this->exam_api_model->checkExamAlreayExist('CPIF',$mqmsscheduleId);

                    $data = array(
                           'id_course'=>$moduleId,
                           'programme_level_ciif'=>$programmeName,
                           'programme_name_ciif'=>$level_name,
                           'id_exam_sitting'=>$idexamsitting,
                            'name' => $moduleDataname.'-'.$moduleDatacode,
                            'exam_date' => $examdate,
                            'exam_end_date' => $examenddate,
                            'id_location' => $venueData['id_location'],
                            'id_exam_center' => $examcenterid,
                            'to_tm' => $to_tm_12hr,
                            'from_tm' => $from_tm_12hr,
                            'type' => 3,
                            'source' => 'MQMS',
                            'max_count' => 100,
                            'status' => 1,
                            'created_by' => 1,
                            'mqms_schedule_id'=>$mqmsscheduleId,
                            'mqms_type'=>"CPIF"
                    );

                      if(!$examexists) {
                            $exameventid = $this->exam_api_model->addNewExamEvent($data);
                                            $examEventId = $exameventid;

                        } else {
                            $this->exam_api_model->updateExamEvent($data,$examexists->id);
                                            $examEventId = $examexists->id;

                            
                        }


                


                    foreach($row->user_module as $user){
                    
                        $userData = array();
                        $userData['full_name'] = $user->name;
                        $userData['email_id'] = $user->email;
                        $userData['nric'] = $user->nirc_passport;
                        $userData['password'] = md5($user->nirc_passport);
                        $userData['membership_number'] = $user->current_membership_no;
                        $userData['permanent_country'] = '1';
                        $userData['permanent_state'] = '1';
                        $userData['status']='1';
                        if($user->batch->name) {
                            $userData['batch']=$user->batch->name;
                        }
                        
                        $idstudent =  $this->exam_api_model->addNewExamUser($userData);
                        
                   
                        $examevent = array();
                        $examevent['id_student'] = $idstudent;
                        $examevent['id_exam_event'] = $examEventId;
                        $examevent['mqm_schedule_id'] = $mqmsscheduleId;
                        $examevent['mqms_assessment_component_id'] = $row->assessment_component_id;
                        $examevent['mqms_user_module_id'] = $user->id;
                        $examevent['status']='1';
                        $examevent['token']=rand(000000,999999);
                        $eventExist = $this->exam_api_model->checkeventcreated($idstudent,$exameventid->id);
                        if(!$eventExist) {
                            $this->exam_api_model->addExamEvent($examevent);
                        }
                    }

                } else {

                      if(count($row->user_masterclass)==0)
                         continue;


                    $mqmsscheduleId = $row->id;
                    $moduleData = array();
                    $moduleDataname = $row->programme->name;
                    // $moduleDatacode = "Master Class";// $row->module[0]->code;
                    // $moduleDatamqms_id = "Master Class";/   /$row->module[0]->id;
                                        $programmeName = $row->programme->name;
                    $level_name = $row->programme->level_name;

                    // check if the imqsid is already present
                    // $courseData = $this->exam_api_model->getIMQSCourseId($row->module->id);
                    $venueData = array();
                    $venueData['name'] = $row->venue->name;
                    $examcenterid = $this->exam_api_model->editExamCenterName($row->venue->name);

                   

                    $scheduleData = array();
                    
                    $examstartDate = new DateTime($row->exam_start_date, new DateTimeZone('UTC'));

                    // change the timezone of the object without changing its time
                    $examstartDate->setTimezone(new DateTimeZone('Asia/Kuala_Lumpur'));
                    $examdate = $examstartDate->format('Y-m-d');            
                    $examEndDate = new DateTime($row->exam_end_date, new DateTimeZone('UTC'));
                    // change the timezone of the object without changing its time
                    $examEndDate->setTimezone(new DateTimeZone('Asia/Kuala_Lumpur'));


                    if(date('Y-m-d',strtotime($examdate))<date('Y-m-d',strtotime($currentdate))) {
                        continue;
                    }

                    $examdate = $examEndDate->format('Y-m-d');
                    $examenddate = date('Y-m-d',strtotime($row->exam_end_date));
                    $idexamsitting = date('m',strtotime($row->exam_start_date));
                    $moduleId = $courseData->id;
                    $examcenterid = $examcenterid->id;
                    //check exam event ID
                    $exameventid = $this->exam_api_model->getExamEventIdAPI($examdate,$moduleId);
                    $examEventId = $exameventid->id;
                    // $this->exam_api_model->deleteStudents($examEventId);
                    // $this->exam_api_model->deleteexamevent($moduleId,$examdate);
                    $from_tm_12hr  = $examstartDate->format('H:i');
                    $to_tm_12hr  = $examEndDate->format('H:i');


                    $examexists = $this->exam_api_model->checkExamAlreayExist('OTHERS',$mqmsscheduleId);

                    $data = array(
                           'id_exam_sitting'=>$idexamsitting,
                            'programme_level_ciif'=>$programmeName,
                            'programme_name_ciif'=>$level_name,                           
                            'name' => $moduleDataname,
                            'exam_date' => $examdate,
                            'exam_end_date' => $examenddate,
                            'id_exam_center' => $examcenterid,
                            'to_tm' => $to_tm_12hr,
                            'from_tm' => $from_tm_12hr,
                            'type' => 3,
                            'source' => 'MQMS',
                            'max_count' => 100,
                            'status' => 1,
                            'created_by' => 1,
                            'mqms_schedule_id'=>$mqmsscheduleId,
                            'mqms_type'=>"OTHERS"
                    );
                

                    if(!$examexists) {
                        // $data['id_course'] = 0;
                        // $data['type_of_exam'] = 1;
                        // $data['id_location'] = 0;
                            $exameventid = $this->exam_api_model->addNewExamEvent($data);
                                            $examEventId = $exameventid;

                        } else {
                            $this->exam_api_model->updateExamEvent($data,$examexists->id);
                                            $examEventId = $examexists->id;

                            
                        }


                    // $exameventid = $this->exam_api_model->getExamEventIdAPI($examdate,$moduleId);
                    foreach($row->user_masterclass as $user){
                    
                        $userData = array();
                        $userData['full_name'] = $user->name;
                        $userData['email_id'] = $user->email;
                        $userData['nric'] = $user->nirc_passport;
                        $userData['password'] = md5($user->nirc_passport);
                        $userData['membership_number'] = $user->current_membership_no;
                        $userData['permanent_country'] = '1';
                        $userData['permanent_state'] = '1';
                        $userData['status']='1';
                         if($user->batch->name) {
                            $userData['batch']=$user->batch->name;
                        }
                        
                        $studentId  = $this->exam_api_model->addNewExamUser($userData);

                        $examevent = array();
                        $userexist = $this->exam_api_model->checkuser($user->email,$examEventId);
                        $examevent['id_student'] = $studentId;
                        $examevent['id_exam_event'] = $examEventId;
                        $examevent['mqm_schedule_id'] = $mqmsscheduleId;
                        $examevent['mqms_assessment_component_id'] = 1;
                        $examevent['mqms_user_module_id'] = $user->id;
                        $examevent['status']='1';
                        $examevent['token']=rand(000000,999999);
                        $eventExist = $this->exam_api_model->checkeventcreated($studentId,$examEventId);
                        if(!$eventExist) {
                            $this->exam_api_model->addExamEvent($examevent);
                        }
                    }

                }
            }
                
            }
        
                      $this->exam_api_model->deleteStudentsExamEvent($examEventId);

        echo "<script>parent.location='/dashboard/Examdashboard/list'</script>";
        exit;
    }
}
