<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class ExamAbsent extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('exam_center_model');
        $this->isLoggedIn();
        error_reporting(0);
    }

    function list()
    {
        if($_POST) {
               $id = 1;
               $data = array(
                    'name' => $_POST['name']
                );
               $result = $this->exam_center_model->editExamAbsent($data,$id);
        }


            $data['examSetList'] = $this->exam_center_model->getExamAbsent();
            $this->global['pageCode'] = 'exam_absent.list';
            $this->global['pageTitle'] = 'Examination Management System : Exam Setup';

            $this->loadViews("exam_absent/list", $this->global, $data, NULL); 
    }


}
