<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Examdashboard extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('exam_event_model');
        $this->load->model('exam_name_model');
        $this->isLoggedIn();
        error_reporting(0);
    }

    function list()
    {
        if ($this->checkAccess('exam_event.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        { 
            $formData['id_location'] = $this->security->xss_clean($this->input->post('id_location'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_exam_center'] = $this->security->xss_clean($this->input->post('id_exam_center'));

            $data['searchParam'] = $formData;

            $data['examEventList'] = $this->exam_event_model->examEventListSearch($formData);
            $data['locationList'] = $this->exam_event_model->examCenterLocationList();
            $data['examCenterList'] = $this->exam_event_model->examCenterList();

            $this->global['pageCode'] = 'examdashboard.list';
            $this->global['pageTitle'] = 'Examination Management System : Exam Events';
            $this->loadViews("examdashboard/list", $this->global, $data, NULL);
        }
    }

      function students($id) {


                    $data['examStudentTaggingList'] = $this->exam_event_model->examStudentTaggingListSearch($id);
  $this->global['pageTitle'] = 'Examination Management System : Edit Exam Event';
            $this->global['pageCode'] = 'exam_event.edit';

            $this->loadViews("examdashboard/student", $this->global, $data, NULL);

    }
   
}
