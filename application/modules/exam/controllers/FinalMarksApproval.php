<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class FinalMarksApproval extends BaseController
{
    public function __construct()
    {
        error_reporting(0);
        parent::__construct();
        $this->load->model('final_marks_approval_model');
        $this->load->model('grader_marks_approval_model');
                $this->load->model('grader_exam_model');

                $this->load->model('exam_event_model');
        $this->load->model('exam_name_model');
        $this->load->model('grader_student_tag_model');


        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('final_marks_approval.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

             $formData['id_exam_sitting'] = $this->security->xss_clean($this->input->post('id_exam_sitting')); 
            $formData['exam_event_name'] = $this->security->xss_clean($this->input->post('exam_event_name')); 
            $data['searchParam'] = $formData;
            $data['examSitting'] = $this->exam_event_model->getExamSitting();
            // $data['examSitting'] = $this->exam_event_model->getExamEvent();

            $data['examEventList'] = $this->final_marks_approval_model->listofExamEvent($formData);


            $this->global['pageCode'] = 'final_marks_approval.list';
            $this->global['pageTitle'] = 'Examination Management System : Grader Exam List';
            $this->loadViews("final_marks_approval/list", $this->global, $data, NULL);
        }
    }


     function history()
    {
       
            $data['examEventList'] = $this->final_marks_approval_model->listofExamEventHistory();


            $this->global['pageCode'] = 'final_marks_approval_history.list';
            $this->global['pageTitle'] = 'Examination Management System : Grader Exam List';
            $this->loadViews("final_marks_approval/history", $this->global, $data, NULL);
        
    }


     function approvehistory($idgraderExam) {
                $data['student_data'] = $this->final_marks_approval_model->getGraderStudentTagListByData($idgraderExam);

                $data['examEventDetails'] = $this->final_marks_approval_model->getExamEventDetails($idgraderExam);

 
            $data['idgraderExam'] = $idgraderExam;
            $this->global['pageCode'] = 'final_marks_approval_history.list';
            $this->global['pageTitle'] = 'Examination Management System : Grader Exam List';

        $this->loadViews("final_marks_approval/approvehistory",$this->global,$data,NULL);
    }

    
      function attendance()
    {
        if ($this->checkAccess('finalmarksapproval.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
           $formData['id_location'] = $this->security->xss_clean($this->input->post('id_location'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_exam_center'] = $this->security->xss_clean($this->input->post('id_exam_center'));

            $formData['type']='Past';
            $data['searchParam'] = $formData;

            $data['examEventList'] = $this->exam_event_model->examEventListSearch($formData);
            $data['locationList'] = $this->exam_event_model->examCenterLocationList();
            $data['examCenterList'] = $this->exam_event_model->examCenterList();

            $this->global['pageCode'] = 'finalmarksapproval.list';

            $this->loadViews("final_marks_approval/attendance", $this->global, $data, NULL);
        }
    }
    
    
    function pushexam($id){
        
        if($_POST) {

            for($i=0;$i<count($_POST['tag']);$i++) {
                $scheduleId = $_POST['schedule'][$i];
                $mqms_type = $_POST['mqms_type'][$i];
                $examstudentTaggingId = $_POST['tag'][$i];
                $attendanceStatus = $_POST['attendance'][$examstudentTaggingId];
                
                $data['attendence_status'] = $attendanceStatus;
                $this->exam_event_model->updateToken($data,$examstudentTaggingId);
            }
            
            if($mqms_type=='CPIF') {
                    echo "<script>parent.location='/exam/api/result/$scheduleId'</script>";
                    exit;

            } else  if($mqms_type=='OTHERS') {
                    echo "<script>parent.location='/exam/api/resultmaster/$scheduleId'</script>";
            exit;

            }
        }
          $data['examStudentTaggingList'] = $this->exam_event_model->examStudentTaggingListSearch($id);
  $this->global['pageTitle'] = 'Examination Management System : Edit Exam Event';
            $this->global['pageCode'] = 'push.fetchschedule';

            $this->loadViews("final_marks_approval/student", $this->global, $data, NULL);
    }


    function mcqlist($id_exam_student_tagging,$backid){

         $data['backid'] = $backid;



          $data['studentDetails'] = $this->final_marks_approval_model->getstudentDetailsByExamsittingId($id_exam_student_tagging);

          $data['examQuestionsByData'] = $this->final_marks_approval_model->exammcaQuestionsByData($id_exam_student_tagging);
                      $this->loadViews("final_marks_approval/mcqlist", $this->global, $data, NULL);

    }

     function answerlist($id_exam_student_tagging,$backid){

        
         $data['backid'] = $backid;
          $data['studentDetails'] = $this->final_marks_approval_model->getstudentDetailsByExamsittingId($id_exam_student_tagging);


        $data['showStudentQuestionSetForGrader'] = $this->final_marks_approval_model->getShowStudentQuestionSetForGrader($id_exam_student_tagging);

                      $this->loadViews("final_marks_approval/answerlist", $this->global, $data, NULL);




    }


    
     function approve($idgraderExam) {
                $data['student_data'] = $this->final_marks_approval_model->getGraderStudentTagListByData($idgraderExam);
            $user_id = $this->session->userId;


                $data['examEventDetails'] = $this->final_marks_approval_model->getExamEventDetails($idgraderExam);

                if(count($data['student_data'])>0) {
                    $id_exam_event = $data['student_data'][0]->id_exam_event;
                }

                if($this->input->post())
                {


                    $id_exam_student_taggings = $this->security->xss_clean($this->input->post('student_exam_tagging'));
                    $type = $this->security->xss_clean($this->input->post('type'));
                    $id_grader_two = $this->security->xss_clean($this->input->post('id_grader_two'));
                    $start_date = $this->security->xss_clean($this->input->post('start_date'));
                    $end_date = $this->security->xss_clean($this->input->post('end_date'));

                    for($i=0;$i<count($id_exam_student_taggings);$i++) {

                        if($type=='15') {

                            $totalMCQ = $this->grader_marks_approval_model->totalonlyMCQQuestionAnswered($id_exam_student_taggings[$i]);
                            $totalMCQAnswered = $totalMCQ[0]->totalmcq;
                            $totalmarkedQuestion = $this->grader_marks_approval_model->totalMCQQuestionMarked($id_exam_student_taggings[$i]);
                                    $answermarks = 0;
                            for($m=0;$m<count($totalmarkedQuestion);$m++) {

                                   if($totalmarkedQuestion[$m]->answermarks<0) {
                                    $totalmarkedQuestion[$m]->answermarks = 0;
                                }
                                      $answermarks = $answermarks + $totalmarkedQuestion[$m]->answermarks;                              
                            }
                            $totalEssayMarks = $answermarks;
                            $totalMarksObtained = $totalMCQAnswered + $totalEssayMarks;                        
                                 $studentexamtagging = array(
                                    'result_status'=>15,
                                    'mcq_total'=>$totalMCQAnswered,
                                    'essay_total'=>$totalEssayMarks,
                                    'final_total'=>$totalMarksObtained,
                                    'approved_by'=>$user_id,
                                    'approved_date_time' =>date('Y-m-d H:i:s')

                                 );


                                $this->grader_marks_approval_model->updateExamStudentTagging($studentexamtagging,$id_exam_student_taggings[$i]);                    
                        }
                         if($type=='11') {
                        // print_R($studentexamtagging);exit;
                          $studentexamtagging = array(
                            'result_status'=>11
                         );

                        
                        $this->grader_marks_approval_model->updateExamStudentTagging($studentexamtagging,$id_exam_student_taggings[$i]);                    
                    }

                       if($type=='13') {

                                            $data = array(
                    'id_grader' => $id_grader_two,
                    'id_exam_event' => $id_exam_event,
                    'status' => 1,
                    'start_date' => date('Y-m-d',strtotime($start_date)),
                    'end_date' => date('Y-m-d',strtotime($end_date)),
                    'created_by' => $user_id
                );
            
             $id_grader_exam = $this->grader_exam_model->addNewGraderExam($data);
                   for($i=0;$i<count($_POST['student_exam_tagging']);$i++) {
                    $id_exam_student_tagging = $_POST['student_exam_tagging'][$i];

                    $studentDetails = $this->grader_exam_model->getExamStudentTagging($id_exam_student_tagging);

                   

                      $datas = array(
                    'id_grader' => $id_grader_two,
                    'id_grader_exam' => $id_grader_exam,
                    'id_student' => $studentDetails->id_student,
                    'id_exam_event' => $id_exam_event,
                    'id_exam_student_tagging' => $id_exam_student_tagging,
                    'created_by' => $user_id
                );
            
                    $this->grader_student_tag_model->addNewGraderStudentTag($datas);
                }



                       }
                    
                    }
                                        redirect('/exam/finalMarksApproval/approve/'.$idgraderExam);

                }
            $data['graderList'] = $this->grader_exam_model->graderListByStatus($data['examEventDetails']->id_course);

            $data['idgraderExam'] = $idgraderExam;
            $this->global['pageCode'] = 'final_marks_approval.list';
            $this->global['pageTitle'] = 'Examination Management System : Grader Exam List';

        $this->loadViews("final_marks_approval/approve",$this->global,$data,NULL);
    }

    

}
