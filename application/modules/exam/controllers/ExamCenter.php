<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class ExamCenter extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('exam_center_model');
        $this->isLoggedIn();
        error_reporting(0);
    }

    function list()
    {
        if ($this->checkAccess('exam_center.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        { 
            $formData['id_location'] = $this->security->xss_clean($this->input->post('id_location'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
 

            $data['searchParam'] = $formData;


            $data['examCenterList'] = $this->exam_center_model->examCenterListSearch($formData);
            $data['locationList'] = $this->exam_center_model->examCenterLocationList();
            $this->global['pageCode'] = 'exam_center.list';
            $this->global['pageTitle'] = 'Campus Management System : Exam Centers';
            $this->loadViews("exam_center/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('exam_center.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if($this->input->post())
            {
                // echo "<Pre>";print_r($this->input->post());exit();

                $id_session = $this->session->my_session_id;
                $user_id = $this->session->userId;

                $name = $this->security->xss_clean($this->input->post('name'));
                $address = $this->security->xss_clean($this->input->post('address'));
                $contact_person = $this->security->xss_clean($this->input->post('contact_person'));
                $contact_number = $this->security->xss_clean($this->input->post('contact_number'));
                $id_country = $this->security->xss_clean($this->input->post('id_country'));
                $id_state = $this->security->xss_clean($this->input->post('id_state'));
                $city = $this->security->xss_clean($this->input->post('city'));
                $zipcode = $this->security->xss_clean($this->input->post('zipcode'));
                $id_location = $this->security->xss_clean($this->input->post('id_location'));
                $exam_type = $this->security->xss_clean($this->input->post('exam_type'));
                $email = $this->security->xss_clean($this->input->post('email'));
                $user_name = $this->security->xss_clean($this->input->post('user_name'));
                $status = $this->security->xss_clean($this->input->post('status'));
                $password = $this->security->xss_clean($this->input->post('password'));
                $rooms = $this->security->xss_clean($this->input->post('rooms'));
                // $id_location = $this->security->xss_clean($this->input->post('id_location'));
                
                $data = array(
                    'name' => $name,
                    'address' => $address,
                    'contact_person' => $contact_person,
                    'contact_number' => $contact_number,
                    'id_country' => $id_country,
                    'id_state' => $id_state,
                    'city' => $city,
                    'zipcode' => $zipcode,
                    'id_location' => $id_location,
                    'exam_type' => $exam_type,
                    'email' => $email,
                    'user_name' => $user_name,
                    'password' => md5($password),
                    'rooms' => $rooms,
                    'status' => $status,
                    'source' => 'Online',                    
                    'created_by' => $user_id
                );

                // echo "<Pre>";print_r($data);exit();

                $result = $this->exam_center_model->addExamCenter($data);
                if ($result > 0) {
                    $this->session->set_flashdata('success', 'New Exam Center created successfully');
                } else {
                    $this->session->set_flashdata('error', 'Exam Center creation failed');
                }
                redirect('/exam/examCenter/editclo'.$result);
            }

            $data['countryList'] = $this->exam_center_model->countryListByStatus('1');
            $data['stateList'] = $this->exam_center_model->stateListByStatus('1');
            $data['locationList'] = $this->exam_center_model->examCenterLocationListByStatus('1');


            $this->global['pageCode'] = 'exam_center.add';
            $this->global['pageTitle'] = 'Campus Management System : Add Exam Center';
            $this->loadViews("exam_center/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('exam_center.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/exam/examCenter/list');
            }
            if($this->input->post())
            {
                // echo "<Pre>";print_r($this->input->post());exit();

                $id_session = $this->session->my_session_id;
                $user_id = $this->session->userId;
                
                $name = $this->security->xss_clean($this->input->post('name'));
                $address = $this->security->xss_clean($this->input->post('address'));
                $contact_person = $this->security->xss_clean($this->input->post('contact_person'));
                $contact_number = $this->security->xss_clean($this->input->post('contact_number'));
                $id_country = $this->security->xss_clean($this->input->post('id_country'));
                $id_state = $this->security->xss_clean($this->input->post('id_state'));
                $city = $this->security->xss_clean($this->input->post('city'));
                $zipcode = $this->security->xss_clean($this->input->post('zipcode'));
                $id_location = $this->security->xss_clean($this->input->post('id_location'));
                $exam_type = $this->security->xss_clean($this->input->post('exam_type'));
                $email = $this->security->xss_clean($this->input->post('email'));
                $user_name = $this->security->xss_clean($this->input->post('user_name'));
                $status = $this->security->xss_clean($this->input->post('status'));
                $password = $this->security->xss_clean($this->input->post('password'));
                $rooms = $this->security->xss_clean($this->input->post('rooms'));
                // $id_location = $this->security->xss_clean($this->input->post('id_location'));
                
                $data = array(
                    'name' => $name,
                    'address' => $address,
                    'contact_person' => $contact_person,
                    'contact_number' => $contact_number,
                    'id_country' => $id_country,
                    'id_state' => $id_state,
                    'city' => $city,
                    'zipcode' => $zipcode,
                    'id_location' => $id_location,
                    'exam_type' => $exam_type,
                    'email' => $email,
                    'user_name' => $user_name,
                    'rooms' => $rooms,
                    'status' => $status,
                    'source' => 'Online',
                    'created_by' => $user_id
                );

                // echo "<Pre>";print_r($data);exit();

                $result = $this->exam_center_model->editExamCenter($data,$id);
echo $password;exit;
                if($password)

                {

                  $datapwd = array(
                    'password' => md5($password)
                );

                $result = $this->exam_center_model->editExamCenter($datapwd,$id);

              }
                if ($result) {
                    $this->session->set_flashdata('success', 'Exam Center edited successfully');
                } else {
                    $this->session->set_flashdata('error', 'Exam Center edit failed');
                }
                redirect('/exam/examCenter/list');
            }
            $data['id_examcenter'] = $id;
            $data['countryList'] = $this->exam_center_model->countryListByStatus('1');
            $data['stateList'] = $this->exam_center_model->stateListByStatus('1');
            $data['locationList'] = $this->exam_center_model->examCenterLocationListByStatus('1');
            
            $data['getExamCenterList'] = $this->exam_center_model->getExamCenterList($id);

            // echo "<Pre>";print_r($data['getExamCenterList']);exit();


            $this->global['pageTitle'] = 'Campus Management System : Edit Exam Center';
            $this->global['pageCode'] = 'exam_center.edit';

            $this->loadViews("exam_center/edit", $this->global, $data, NULL);
        }
    }


      function editclo($id = NULL,$cloid=NULL)
    {
        if ($this->checkAccess('exam_center.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/question/programme/list');
            }

            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;

            if($this->input->post())
            {



                 $room = $this->security->xss_clean($this->input->post('room'));
                 $capacity = $this->security->xss_clean($this->input->post('capacity'));
               
                 $data = array(
                    'id_exam_center' => $id,
                    'room' => $room,
                    'capacity' => $capacity
                );
                 if($cloid) {
                $result = $this->exam_center_model->updateRoomDetails($data,$cloid);

                 } else {
                
                $result = $this->exam_center_model->addRoomDetails($data);




            }

                            $courseDetails = $this->exam_center_model->getRoomDetails($id);
                            $capacity = 0;
                for($i=0;$i<count($courseDetails);$i++) {
                    $capacity = $capacity + $courseDetails[$i]->capacity;
                }

                $datas = array('rooms'=>$capacity);
                    $this->exam_center_model->editExamCenter($datas,$id);

                if ($result) {
                    $this->session->set_flashdata('success', 'Course edited successfully');
                } else {
                    $this->session->set_flashdata('error', 'Course edit failed');
                }
                redirect('/exam/examCenter/editclo/'.$id);
            }


            $courseDetails = $this->exam_center_model->getRoomDetails($id);
            if($cloid) {
                $clodetails = $this->exam_center_model->getRoomById($cloid);
            $data['clodetails'] = $clodetails;

            } else {
                $data['clodetails'] = $clodetails;
            }

            $data['getExamCenterList'] = $this->exam_center_model->getExamCenterList($id);

            $data['courseid'] = $id;
            $data['courseDetails'] = $courseDetails;
            $this->global['pageCode'] = 'exam_center.edit';
            $this->global['pageTitle'] = 'Examination Management System : Edit Course';
            $this->loadViews("exam_center/plo", $this->global, $data, NULL);
        }
    }

     function deleteclo(){

        $id = $_POST['id'];
        $centerid = $_POST['centerid'];

        $this->exam_center_model->deleteCourseHasClo($id);



        $courseDetails = $this->exam_center_model->getRoomDetails($centerid);
        $capacity = 0;
        for($i=0;$i<count($courseDetails);$i++) {
            $capacity = $capacity + $courseDetails[$i]->capacity;
        }

        $datas = array('rooms'=>$capacity);
        $this->exam_center_model->editExamCenter($datas,$centerid);

        return 1;


    }


    function getStateByCountry($id_country)
    {
            $results = $this->exam_center_model->getStateByCountryId($id_country);

            $table="   
                <script type='text/javascript'>
                     $('select').select2();
                 </script>
         ";

            $table.="
            <select name='id_state' id='id_state' class='form-control'>
                <option value=''>Select</option>
                ";

            for($i=0;$i<count($results);$i++)
            {

            $id = $results[$i]->id;
            $name = $results[$i]->name;
            $table.="<option value=".$id.">".$name.
                    "</option>";

            }
            $table.="

            </select>";

            echo $table;
            exit;
    }


        function addroom(){
        if($this->input->post())
            {

                $id_examcenter = $this->security->xss_clean($this->input->post('id_examcenter'));
                $room_name = $this->security->xss_clean($this->input->post('room_name'));
                $room_capacity = $this->security->xss_clean($this->input->post('room_capacity'));

                 $data = array(
                    'id_exam_center' => $id_examcenter,
                    'room' => $room_name,
                    'capacity' => $room_capacity
                );
                
                $result = $this->exam_center_model->addRoomDetails($data);
            }

            $this->viewroomtable($id_examcenter);


    }

    function viewroomtable($id){


            $courseDetails = $this->exam_center_model->getRoomDetails($id);
            

          $table="<table class='table' width='100%'>
                   <tr> <th>Sl No</th><th>Room Name </th><th>Capacity </th></tr>";    
          $j = 1;
           for($i=0;$i<count($courseDetails);$i++) {
            $id = $courseDetails[$i]->id;
            $room = $courseDetails[$i]->room;
            $capacity = $courseDetails[$i]->capacity;
              $table.="<tr><td>$j</td><td>$room</td><td>$capacity</td></tr>";
              $j++;
           }
           $table.="</table>";

           echo $table;

    }

}
