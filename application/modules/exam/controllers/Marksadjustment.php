<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Marksadjustment extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('marksadjustment_model');
                $this->load->model('grader_marks_approval_model');
        error_reporting(0);
        
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('marksadjustment.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $data['graderStudentTagList'] = $this->marksadjustment_model->listofmarksadjustment();

            $this->global['pageCode'] = 'marksadjustment.list';
            $this->global['pageTitle'] = 'Examination Management System : Grader List';
            $this->loadViews("marksadjustment/list", $this->global, $data, NULL);
        }
    }
    
    function add($id_exam_student_tagging)
    {
        if ($this->checkAccess('marksadjustment.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

              if($this->input->post())
            {
                $adjmarks = $this->security->xss_clean($this->input->post('adjmarks'));
                $questionset = $this->security->xss_clean($this->input->post('questionset'));


              
                for($l=0;$l<count($adjmarks);$l++) {
                     $data = array(
                        'marks' => $adjmarks[$l],
                        'id_student_question_set' => $questionset[$l],
                        'status' => 0
                    );

                     $this->marksadjustment_model->deletemarks($questionset[$l]);

                    $this->marksadjustment_model->addNewStudentQuestionSetMarks($data);
                }

                if($_POST['Submit']=='Submit') {
                      $studentexamtagging = array(
                            'result_status'=>12
                         );
                        $this->grader_marks_approval_model->updateExamStudentTagging($studentexamtagging,$id_exam_student_tagging);
                }
            }
        $data['showStudentQuestionSetForGrader'] = $this->marksadjustment_model->getShowStudentQuestionSetForGrader($id_exam_student_tagging);
                       $this->global['pageCode'] = 'marksadjustment.list';
            $this->global['pageTitle'] = 'Examination Management System : Grader List';

            $this->loadViews("marksadjustment/add", $this->global, $data, NULL);
        }
    }


    function viewquestion($id = NULL)
    {
         $data['qtns'] = $this->grader_marks_approval_model->getQuestionById($id);
                       $this->global['pageCode'] = 'marksadjustment.list';
            $this->global['pageTitle'] = 'Examination Management System : Grader List';

            $this->loadViews("marksadjustment/viewquestion", $this->global, $data, NULL);

    }




    function edit($id = NULL)
    {
        if ($this->checkAccess('grader.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/exam/grader/list');
            }
            $user_id = $this->session->userId;

            if($this->input->post())
            {
                $salutation = $this->security->xss_clean($this->input->post('salutation'));
                $first_name = $this->security->xss_clean($this->input->post('first_name'));
                $last_name = $this->security->xss_clean($this->input->post('last_name'));
                $email = $this->security->xss_clean($this->input->post('email'));
                $nric = $this->security->xss_clean($this->input->post('nric'));
                $mobile = $this->security->xss_clean($this->input->post('mobile'));
                $password = $this->security->xss_clean($this->input->post('password'));
                $status = $this->security->xss_clean($this->input->post('status'));

                $salutation_data = $this->grader_model->getSalutation($salutation);

                $data = array(
                    'salutation' => $salutation,
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'full_name' => $salutation_data->name . ". " . $first_name . " " . $last_name,
                    // 'email' => $email,
                    'nric' => $nric,
                    'mobile' => $mobile,
                    // 'password' => md5($password),
                    'status' => $status,
                    'updated_by' => $user_id,
                    'type'=>1
                );

                $result = $this->grader_model->editGrader($data,$id);
                if ($result) {
                    $this->session->set_flashdata('success', 'Grader edited successfully');
                } else {
                    $this->session->set_flashdata('error', 'Grader edit failed');
                }
                redirect('/exam/grader/list');
            }

            $data['grader'] = $this->grader_model->getGrader($id);
            $data['graderModulesList'] = $this->grader_model->getGraderModulesList($id);
            $data['salutationList'] = $this->grader_model->salutationListByStatus('1');
            $data['courseList'] = $this->grader_model->courseListByStatus('1');

            // echo "<Pre>";print_r($data['salutationList']);exit();

            $this->global['pageCode'] = 'grader.edit';
            $this->global['pageTitle'] = 'Examination Management System : Edit Exam';
            
            $this->loadViews("grader/edit", $this->global, $data, NULL);
        }
    }

    function addGraderModuleData()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        // echo "<Pre>"; print_r($tempData);exit();
        $inserted_id = $this->grader_model->addGraderModuleData($tempData);

        if($inserted_id)
        {
            echo "Success";exit;
        }
    }

    function deleteGraderModule($id)
    {
            // echo "<Pre>"; print_r($id);exit();
        $inserted_id = $this->grader_model->deleteGraderModule($id);
        echo "Success";exit;
    }
}
