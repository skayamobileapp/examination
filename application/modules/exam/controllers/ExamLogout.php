<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class ExamLogout extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('exam_center_model');
        $this->isLoggedIn();
        error_reporting(0);
    }

    function list()
    {
        if($_POST) {
               $id = 1;
               $data = array(
                    'name' => $_POST['name']
                );
               $result = $this->exam_center_model->editExamLogout($data,$id);
        }


            $data['examSetList'] = $this->exam_center_model->getlimitLogout();
            $this->global['pageCode'] = 'exam_logout.list';
            $this->global['pageTitle'] = 'Examination Management System : Exam Setup';

            $this->loadViews("exam_logout/list", $this->global, $data, NULL); 
    }


}
