<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class SecondGrader extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('grader_model');
        $this->isLoggedIn();
    }

    function list()
    {

        if ($this->checkAccess('secondgrader.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name')); 
            $data['searchParam'] = $formData;
            
            $data['graderList'] = $this->grader_model->graderListSearch($formData,'2');

            $this->global['pageCode'] = 'secondgrader.list';
            $this->global['pageTitle'] = 'Examination Management System : Grader List';
            $this->loadViews("second_grader/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('secondgrader.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            
            $user_id = $this->session->userId;

            if($this->input->post())
            {
                $salutation = $this->security->xss_clean($this->input->post('salutation'));
                $first_name = $this->security->xss_clean($this->input->post('first_name'));
                $last_name = $this->security->xss_clean($this->input->post('last_name'));
                $email = $this->security->xss_clean($this->input->post('email'));
                $nric = $this->security->xss_clean($this->input->post('nric'));
                $mobile = $this->security->xss_clean($this->input->post('mobile'));
                $password = $this->security->xss_clean($this->input->post('password'));
                $status = $this->security->xss_clean($this->input->post('status'));

                $salutation_data = $this->grader_model->getSalutation($salutation);

                $data = array(
                    'salutation' => $salutation,
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'full_name' => $salutation_data->name . ". " . $first_name . " " . $last_name,
                    'email' => $email,
                    'nric' => $nric,
                    'mobile' => $mobile,
                    'password' => md5($password),
                    'status' => $status,
                    'created_by' => $user_id,

                    'type'=>2
                );
            
                $result = $this->grader_model->addNewGrader($data);
                if ($result > 0)
                {
                    $this->session->set_flashdata('success', 'New Grader created successfully');
                    redirect('/exam/second_grader/edit/'.$result);
                }
                 else
                {
                    $this->session->set_flashdata('error', 'Grader creation failed');
                    redirect('/exam/second_grader/list');
                }
            }

            $data['salutationList'] = $this->grader_model->salutationListByStatus('1');

            $this->global['pageCode'] = 'grader.add';
            $this->global['pageTitle'] = 'Examination Management System : Add Exam';
            $this->loadViews("grader/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('secondgrader.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/exam/second_grader/list');
            }
            $user_id = $this->session->userId;

            if($this->input->post())
            {
                $salutation = $this->security->xss_clean($this->input->post('salutation'));
                $first_name = $this->security->xss_clean($this->input->post('first_name'));
                $last_name = $this->security->xss_clean($this->input->post('last_name'));
                $email = $this->security->xss_clean($this->input->post('email'));
                $nric = $this->security->xss_clean($this->input->post('nric'));
                $mobile = $this->security->xss_clean($this->input->post('mobile'));
                $password = $this->security->xss_clean($this->input->post('password'));
                $status = $this->security->xss_clean($this->input->post('status'));

                $salutation_data = $this->grader_model->getSalutation($salutation);

                $data = array(
                    'salutation' => $salutation,
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'full_name' => $salutation_data->name . ". " . $first_name . " " . $last_name,
                    // 'email' => $email,
                    'nric' => $nric,
                    'mobile' => $mobile,
                    // 'password' => md5($password),
                    'status' => $status,
                    'updated_by' => $user_id,
                    'type'=>2
                );

                $result = $this->grader_model->editGrader($data,$id);
                if ($result) {
                    $this->session->set_flashdata('success', 'Grader edited successfully');
                } else {
                    $this->session->set_flashdata('error', 'Grader edit failed');
                }
                redirect('/exam/second_grader/list');
            }

            $data['grader'] = $this->grader_model->getGrader($id);
            $data['graderModulesList'] = $this->grader_model->getGraderModulesList($id);
            $data['salutationList'] = $this->grader_model->salutationListByStatus('1');
            $data['courseList'] = $this->grader_model->courseListByStatus('1');

            // echo "<Pre>";print_r($data['salutationList']);exit();

            $this->global['pageCode'] = 'grader.edit';
            $this->global['pageTitle'] = 'Examination Management System : Edit Exam';
            
            $this->loadViews("grader/edit", $this->global, $data, NULL);
        }
    }

    function addGraderModuleData()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));

        // echo "<Pre>"; print_r($tempData);exit();
        $inserted_id = $this->grader_model->addGraderModuleData($tempData);

        if($inserted_id)
        {
            echo "Success";exit;
        }
    }

    function deleteGraderModule($id)
    {
            // echo "<Pre>"; print_r($id);exit();
        $inserted_id = $this->grader_model->deleteGraderModule($id);
        echo "Success";exit;
    }
}
