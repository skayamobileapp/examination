<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Marksadjustmenthistory extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('marksadjustment_model');
                $this->load->model('grader_marks_approval_model');
        error_reporting(0);
        
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('marksadjustment.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $data['graderStudentTagList'] = $this->marksadjustment_model->listofmarksadjustmenthistory();
            $this->global['pageCode'] = 'marksadjustmenthistory.list';
            $this->global['pageTitle'] = 'Examination Management System : Grader List';
            $this->loadViews("marksadjustmenthistory/list", $this->global, $data, NULL);
        }
    }
    
    function add($id_exam_student_tagging)
    {
        if ($this->checkAccess('marksadjustment.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

              if($this->input->post())
            {
                $adjmarks = $this->security->xss_clean($this->input->post('adjmarks'));
                $questionset = $this->security->xss_clean($this->input->post('questionset'));


              
                for($l=0;$l<count($adjmarks);$l++) {
                     $data = array(
                        'marks' => $adjmarks[$l],
                        'id_student_question_set' => $questionset[$l],
                        'status' => 0
                    );

                     $this->marksadjustment_model->deletemarks($questionset[$l]);

                    $this->marksadjustment_model->addNewStudentQuestionSetMarks($data);
                }

                if($_POST['Submit']=='Submit') {
                      $studentexamtagging = array(
                            'result_status'=>12
                         );
                        $this->grader_marks_approval_model->updateExamStudentTagging($studentexamtagging,$id_exam_student_tagging);
                }
            }
        $data['showStudentQuestionSetForGrader'] = $this->marksadjustment_model->getShowStudentQuestionSetForGrader($id_exam_student_tagging);
                       $this->global['pageCode'] = 'marksadjustmenthistory.list';
            $this->global['pageTitle'] = 'Examination Management System : Grader List';

            $this->loadViews("marksadjustmenthistory/add", $this->global, $data, NULL);
        }
    }


    function viewquestion($id = NULL)
    {
         $data['qtns'] = $this->grader_marks_approval_model->getQuestionById($id);
                       $this->global['pageCode'] = 'marksadjustmenthistory.list';
            $this->global['pageTitle'] = 'Examination Management System : Grader List';

            $this->loadViews("marksadjustmenthistory/viewquestion", $this->global, $data, NULL);

    }




   
}
