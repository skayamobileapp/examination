<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class ExamSetup extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('exam_center_model');
        $this->isLoggedIn();
        error_reporting(0);
    }

    function list()
    {
        if($_POST) {
               $id = 1;
               $data = array(
                    'name' => $_POST['name'],
                    'name_second' => $_POST['name_second'],
                    'name_third' => $_POST['name_third'],
                    'name_fourth' => $_POST['name_fourth'],
                    
                );
               $result = $this->exam_center_model->editExamSetupModel($data,$id);
        }


            $data['examSetList'] = $this->exam_center_model->getlimit();
            $this->global['pageCode'] = 'exam_setup.list';
            $this->global['pageTitle'] = 'Examination Management System : Exam Setup';

            $this->loadViews("exam_setup/list", $this->global, $data, NULL); 
    }


}
