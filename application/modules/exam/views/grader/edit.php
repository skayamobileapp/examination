<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
        <h1 class="h3">Edit Grader</h1>
        
        <a href='../list' class="btn btn-link ml-auto">
            <i class="fa fa-chevron-left" aria-hidden="true"></i>
          Back
        </a>

    </div>
    


  <div class="page-container">

          <div class="tabs-view">
             <h4><a href="/exam/grader/edit/<?php echo $courseid;?>"  class="active">Grader Details</a></h4>
            <h4><a href="/exam/grader/editclo/<?php echo $courseid;?>">Grader Module</a></h4>
          </div>

            <div class="form-container">
                    
                    <form id="form_main" action="" method="post" autocomplete="off" enctype="multipart/form-data">


                                <div class="row">

                                    <div class="col-lg-6">
                                      <div class="form-group row">
                                        <label class="col-sm-4 col-form-label">Salutation<span class="text-danger">*</span></label>
                                        <div class="col-sm-8">
                                          <select name="salutation" id="salutation" class="form-control">
                                            <option value="">Select</option>
                                            <?php
                                            if (!empty($salutationList))
                                            {
                                                foreach ($salutationList as $record)
                                                {
                                                ?>
                                                    <option value="<?php echo $record->id;  ?>"
                                                        <?php
                                                        if($record->id == $grader->salutation)
                                                        {
                                                             echo "selected";
                                                        };?>
                                                        >
                                                        <?php echo $record->name;  ?>        
                                                    </option>
                                                <?php
                                                }
                                            }
                                            ?>
                                          </select>
                                        </div>
                                      </div>
                                    </div>


                                    <div class="col-lg-6">
                                      <div class="form-group row">
                                        <label class="col-sm-4 col-form-label">First Name<span class="text-danger">*</span></label>
                                        <div class="col-sm-8">
                                        <input type="text" class="form-control" id="first_name" name="first_name" placeholder="First Name" value="<?php echo $grader->first_name; ?>">
                                        </div>
                                      </div>
                                    </div>

                                </div>


                                <div class="row">

                                    <div class="col-lg-6">
                                      <div class="form-group row">
                                        <label class="col-sm-4 col-form-label">Last Name<span class="text-danger">*</span></label>
                                        <div class="col-sm-8">
                                        <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Last Name" value="<?php echo $grader->last_name; ?>">
                                        </div>
                                      </div>
                                    </div>



                                    <div class="col-lg-6">
                                      <div class="form-group row">
                                        <label class="col-sm-4 col-form-label">NRIC / Passport</label>
                                        <div class="col-sm-8">
                                        <input type="text" class="form-control" id="nric" name="nric" placeholder="NRIC" value="<?php echo $grader->nric; ?>">
                                        </div>
                                      </div>
                                    </div>


                                </div>


                                <div class="row">


                                    <div class="col-lg-6">
                                      <div class="form-group row">
                                        <label class="col-sm-4 col-form-label">Mobile Number</label>
                                        <div class="col-sm-8">
                                        <input type="number" class="form-control" id="mobile" name="mobile" placeholder="Mobile Number" value="<?php echo $grader->mobile; ?>">
                                        </div>
                                      </div>
                                    </div>



                                    <div class="col-lg-6">
                                      <div class="form-group row">
                                        <label class="col-sm-4 col-form-label">Email<span class="text-danger">*</span></label>
                                        <div class="col-sm-8">
                                        <input type="email" class="form-control" id="email" name="email" placeholder="Email" value="<?php echo $grader->email; ?>" readonly>
                                        </div>
                                      </div>
                                    </div>


                                </div>

                                <div class="row">
                                    

                                    <div class="col-lg-6">
                                      <div class="form-group row">
                                        <label class="col-sm-4 col-form-label">Password <span class="text-danger">*</span></label>
                                        <div class="col-sm-8">
                                        <input type="password" class="form-control" id="password" name="password" placeholder="Password" value="" >
                                        </div>
                                      </div>
                                    </div>



                      <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Document</label>
                        <div class="col-sm-8">
                        <input type="file" class="form-control" id="document" name="document" placeholder="Password">

                        <?php if($grader->document!='') {?>

                            <a href="/assets/images/<?php echo $grader->document;?>" target="_blank">View</a>
                        <?php } ?> 
                        </div>
                      </div>
                    </div>



                                
                                   <div class="col-lg-6">
                                      <div class="form-group row align-items-center">
                                        <label class="col-sm-4 col-form-label">Status <span class="text-danger">*</span></label>
                                        <div class="col-sm-8">
                                          <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="customRadioInline1" name="status" class="custom-control-input" value="1"  
                                            <?php if($grader->status==1)
                                            {
                                                 echo "checked=checked";
                                            };?>
                                              >
                                            <label class="custom-control-label" for="customRadioInline1">Active</label>
                                          </div>
                                          <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="customRadioInline2" name="status" class="custom-control-input" value="0"
                                            <?php if($grader->status==0)
                                            {
                                                 echo "checked=checked";
                                            };?>

                                            >
                                            <label class="custom-control-label" for="customRadioInline2">In-Active</label>
                                          </div>
                                        </div>
                                      </div>
                                    </div>

                                </div>       


                                  
                                <div class="button-block clearfix">
                                  <div class="bttn-group">
                                      <button type="submit" class="btn btn-primary">Save</button>
                                  </div>

                                </div> 

                            </div>                                
                        </div>
                    </form>




                </div>



            </div>
    

    </div>



                
</main>

<script>

    $('select').select2();


    function addGraderModuleData()
    {
        // alert('11');
        if($('#form_module').valid())
        {
            var formData = {};

            formData['id_grader'] = <?php echo $grader->id;?>;
            formData['status'] = <?php echo 1;?>;
            formData['id_course'] = $("#id_course").val();

             $.ajax({
                url: "/exam/grader/addGraderModuleData",
                type: "post",
                data:
                {
                    tempData: formData
                },
                success: function(data)
                {
                    // alert(data);
                    window.location.reload();
                    // $("#clotalediv").html(d);
                }
            });
        }
    }


  


    $(document).ready(function() {
        $("#form_module").validate({
            rules: {
                id_course: {
                    required: true
                }
            },
            messages: {
                id_course: {
                    required: "<p class='error-text'>Select Course</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });



    $(document).ready(function() {
        $("#form_main").validate({
            rules: {
                first_name: {
                    required: true
                },
                 last_name: {
                    required: true
                },
                 salutation: {
                    required: true
                },
                 status: {
                    required: true
                }
            },
            messages: {
                first_name: {
                    required: "<p class='error-text'>First Name required</p>",
                },
                last_name: {
                    required: "<p class='error-text'>Last Name Required</p>",
                },
                salutation: {
                    required: "<p class='error-text'>Select Salutation</p>",
                },
                status: {
                    required: "<p class='error-text'>Select Status</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>