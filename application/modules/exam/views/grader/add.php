<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
        <h1 class="h3">Add Grader</h1>
        
        <a href='list' class="btn btn-link ml-auto">
            <i class="fa fa-chevron-left" aria-hidden="true"></i>
          Back
        </a>

    </div>
    

                    <form id="form_main" action="" method="post" autocomplete="off" enctype="multipart/form-data">
        <div class="page-container">

          <div>
            <h4 class="form-title">Grader details</h4>
          </div>

            <div class="form-container">


                <div class="row">

                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Salutation<span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                          <select name="salutation" id="salutation" class="form-control">
                            <option value="">Select</option>
                            <?php
                            if (!empty($salutationList))
                            {
                                foreach ($salutationList as $record)
                                {
                                ?>
                                    <option value="<?php echo $record->id;  ?>">
                                        <?php echo $record->name;  ?>        
                                    </option>
                                <?php
                                }
                            }
                            ?>
                          </select>
                        </div>
                      </div>
                    </div>


                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">First Name<span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="first_name" name="first_name" placeholder="First Name">
                        </div>
                      </div>
                    </div>

                </div>

                <div class="row">

                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Last Name<span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Last Name">
                        </div>
                      </div>
                    </div>


                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">NRIC / Passport</label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="nric" name="nric" placeholder="NRIC">
                        </div>
                      </div>
                    </div>


                </div>

                <div class="row">



                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Mobile Number</label>
                        <div class="col-sm-8">
                        <input type="number" class="form-control" id="mobile" name="mobile" placeholder="Mobile Number">
                        </div>
                      </div>
                    </div>
                    

                

                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Email<span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="email" class="form-control" id="email" name="email" placeholder="Email">
                        </div>
                      </div>
                    </div>


                </div>

                <div class="row">



                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Password <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                        </div>
                      </div>
                    </div>

                      <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Document <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="file" class="form-control" id="document" name="document" placeholder="Password">
                        </div>
                      </div>
                    </div>

                

                
                   <div class="col-lg-6">
                      <div class="form-group row align-items-center">
                        <label class="col-sm-4 col-form-label">Status <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                          <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="customRadioInline1" name="status" class="custom-control-input" value="1" checked="checked">
                            <label class="custom-control-label" for="customRadioInline1">Active</label>
                          </div>
                          <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="customRadioInline2" name="status" class="custom-control-input" value="0">
                            <label class="custom-control-label" for="customRadioInline2">In-Active</label>
                          </div>
                        </div>
                      </div>
                    </div>

                </div>


                  
                <div class="button-block clearfix">
                  <div class="bttn-group">
                      <button type="submit" class="btn btn-primary">Save</button>
                        <!-- <a onclick="reloadPage()" class="btn btn-link">Clear All Fields</a> -->
                      <button onclick="reloadPage()" class="btn btn-link">Clear All Fields</button>
                  </div>

                </div> 

            </div>                                
        </div>
    </form>
</main>

<script>

    $('select').select2();
    

    $(document).ready(function() {
        $("#form_main").validate({
            rules: {
                first_name: {
                    required: true
                },
                 last_name: {
                    required: true
                },
                 salutation: {
                    required: true
                },
                 status: {
                    required: true
                },
                 email: {
                    required: true
                },
                 password: {
                    required: true
                }
            },
            messages: {
                first_name: {
                    required: "<p class='error-text'>First Name required</p>",
                },
                last_name: {
                    required: "<p class='error-text'>Last Name Required</p>",
                },
                salutation: {
                    required: "<p class='error-text'>Select Salutation</p>",
                },
                status: {
                    required: "<p class='error-text'>Select Status</p>",
                },
                email: {
                    required: "<p class='error-text'>Email Id Required</p>",
                },
                password: {
                    required: "<p class='error-text'>Password Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
<script type="text/javascript">

    $('select').select2();



    function getStateByCountry(id)
    {

        $.get("/exam/examCenter/getStateByCountry/"+id, function(data, status){
       
            $("#view_state").html(data);
            // $("#view_programme_details").html(data);
            // $("#view_programme_details").show();
        });
    }


    function reloadPage()
    {
      window.location.reload();
    }
</script>