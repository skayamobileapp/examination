<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
        <h1 class="h3">Edit Programme</h1>
        
        <a href='../list' class="btn btn-link ml-auto">
            <i class="fa fa-chevron-left" aria-hidden="true"></i>
          Back
        </a>

    </div>


    <form id="form_main" action="" method="post">

        <div class="page-container">

          <div class="tabs-view">
             <h4><a href="/exam/grader/edit/<?php echo $courseid;?>">Grader Details</a></h4>
            <h4><a href="/exam/grader/editclo/<?php echo $courseid;?>" class="active">Grader Module</a></h4>
          </div>

            <div class="form-container">


                <div class="row">

                   <div class="col-lg-6">
                                          <div class="form-group row">
                                            <label class="col-sm-4 col-form-label">Course<span class="text-danger">*</span></label>
                                            <div class="col-sm-8">
                                              <select name="id_course" id="id_course" class="form-control" style="width:300px;">
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($courseList))
                                                {
                                                    foreach ($courseList as $record)
                                                    {
                                                    ?>
                                                        <option value="<?php echo $record->id;  ?>">
                                                            <?php echo $record->name;  ?>        
                                                        </option>
                                                    <?php
                                                    }
                                                }
                                                ?>
                                              </select>
                                            </div>
                                          </div>
                                        </div>
                </div>



           

                  

            <div class="button-block clearfix">
              <div class="bttn-group">
                  <button type="submit" class="btn btn-primary">Save</button>
                  <a href='add' class="btn btn-link">Clear All Fields</a>
                  
              </div>

            </div> 
            </div>                                
        </div>
    </form>
    <div class="row">
                <div class="col-sm-12">
          <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Sl. No</th>
            <th>Programme Name</th>
        
            <th class="text-center">Status</th>
            <th style="text-align: center;">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($courseDetails)) {
            $i = 1;
            foreach ($courseDetails as $record) {
          ?>
              <tr>
                <td><?php echo $i ?></td>
                <td><?php echo $record->course_name; ?></td>
               
                <td style="text-align: center;">
                  <?php if ($record->status == '1')
                  {
                    echo "Active";
                  } else {
                    echo "In-Active";
                  }
                  ?>
                </td>
                <td class="text-center">

                  <a href="<?php echo $record->id_course.'/' . $record->id; ?>" class="btn" type="" data-toggle="tooltip" data-placement="top" title="Edit">
                    <i class="fa fa-pencil-square-o" aria-hidden="true">
                    </i>
                  </a>


                    <a href="javascript:deleteclo(<?php echo $record->id; ?>)" class="btn" type="" data-toggle="tooltip" data-placement="top" title="Edit">
                    <i class="fa fa-trash" aria-hidden="true">
                    </i>
                  </a>

                </td>
              </tr>
          <?php
              $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>
</div>
    </div>

</main>

<script>
    $(document).ready(function() {
        $("#form_main").validate({
            rules: {
                code: {
                    required: true
                },
                name: {
                    required: true
                }
            },
            messages: {
                code: {
                    required: "<p class='error-text'>Code Required</p>",
                },
                name: {
                    required: "<p class='error-text'>Name Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });




    function deleteclo(id) {
           var formData = {
                'id':id //for get email 
            };
        var cnf = confirm("Do you really want to delete?");
        if(cnf==true) {
            $.get("/exam/grader/deleteGraderModule/"+id, function(data, status)
            {
                // alert(data);
                window.location.reload();
                // $("#clotalediv").html(data);
            });
        }
    }
</script>
<script type="text/javascript">
    function reloadPage()
    {
      window.location.reload();
    }
</script>