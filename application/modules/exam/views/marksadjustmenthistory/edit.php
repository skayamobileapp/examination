<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
        <h1 class="h3">Edit Grader</h1>
        
        <a href='../list' class="btn btn-link ml-auto">
            <i class="fa fa-chevron-left" aria-hidden="true"></i>
          Back
        </a>

    </div>
    

    <div class="form-container">


        <div class="clearfix">

            <ul class="nav nav-tabs" role="tablist" >
                <li role="presentation" class="nav-item" ><a href="#tab_one" class="nav-link active"
                        aria-controls="tab_one" aria-selected="true"
                        role="tab" data-toggle="tab">Grader Details</a>
                </li>
                <li role="presentation" class="nav-item"><a href="#tab_two" class="nav-link"
                        aria-controls="tab_two" role="tab" data-toggle="tab">Modules</a>
                </li>
            </ul>



             <div class="tab-content offers-tab-content">



                <div role="tabpanel" class="tab-pane active" id="tab_one">


                    
                    <form id="form_main" action="" method="post">

                        <div class="page-container">

                          <div>
                            <h4 class="form-title">Grader details</h4>
                          </div>

                            <div class="form-container">


                                <div class="row">

                                    <div class="col-lg-6">
                                      <div class="form-group row">
                                        <label class="col-sm-4 col-form-label">Salutation<span class="text-danger">*</span></label>
                                        <div class="col-sm-8">
                                          <select name="salutation" id="salutation" class="form-control">
                                            <option value="">Select</option>
                                            <?php
                                            if (!empty($salutationList))
                                            {
                                                foreach ($salutationList as $record)
                                                {
                                                ?>
                                                    <option value="<?php echo $record->id;  ?>"
                                                        <?php
                                                        if($record->id == $grader->salutation)
                                                        {
                                                             echo "selected";
                                                        };?>
                                                        >
                                                        <?php echo $record->name;  ?>        
                                                    </option>
                                                <?php
                                                }
                                            }
                                            ?>
                                          </select>
                                        </div>
                                      </div>
                                    </div>


                                    <div class="col-lg-6">
                                      <div class="form-group row">
                                        <label class="col-sm-4 col-form-label">First Name<span class="text-danger">*</span></label>
                                        <div class="col-sm-8">
                                        <input type="text" class="form-control" id="first_name" name="first_name" placeholder="First Name" value="<?php echo $grader->first_name; ?>">
                                        </div>
                                      </div>
                                    </div>

                                </div>


                                <div class="row">

                                    <div class="col-lg-6">
                                      <div class="form-group row">
                                        <label class="col-sm-4 col-form-label">Last Name<span class="text-danger">*</span></label>
                                        <div class="col-sm-8">
                                        <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Last Name" value="<?php echo $grader->last_name; ?>">
                                        </div>
                                      </div>
                                    </div>



                                    <div class="col-lg-6">
                                      <div class="form-group row">
                                        <label class="col-sm-4 col-form-label">NRIC<span class="text-danger">*</span></label>
                                        <div class="col-sm-8">
                                        <input type="text" class="form-control" id="nric" name="nric" placeholder="NRIC" value="<?php echo $grader->nric; ?>">
                                        </div>
                                      </div>
                                    </div>


                                </div>


                                <div class="row">


                                    <div class="col-lg-6">
                                      <div class="form-group row">
                                        <label class="col-sm-4 col-form-label">Mobile Number<span class="text-danger">*</span></label>
                                        <div class="col-sm-8">
                                        <input type="number" class="form-control" id="mobile" name="mobile" placeholder="Mobile Number" value="<?php echo $grader->mobile; ?>">
                                        </div>
                                      </div>
                                    </div>



                                    <div class="col-lg-6">
                                      <div class="form-group row">
                                        <label class="col-sm-4 col-form-label">Email<span class="text-danger">*</span></label>
                                        <div class="col-sm-8">
                                        <input type="email" class="form-control" id="email" name="email" placeholder="Email" value="<?php echo $grader->email; ?>" readonly>
                                        </div>
                                      </div>
                                    </div>


                                </div>

                                <div class="row">
                                    

                                    <div class="col-lg-6">
                                      <div class="form-group row">
                                        <label class="col-sm-4 col-form-label">Password <span class="text-danger">*</span></label>
                                        <div class="col-sm-8">
                                        <input type="password" class="form-control" id="password" name="password" placeholder="Password" value="<?php echo $grader->password; ?>" readonly>
                                        </div>
                                      </div>
                                    </div>


                                
                                   <div class="col-lg-6">
                                      <div class="form-group row align-items-center">
                                        <label class="col-sm-4 col-form-label">Status <span class="text-danger">*</span></label>
                                        <div class="col-sm-8">
                                          <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="customRadioInline1" name="status" class="custom-control-input" value="1"  
                                            <?php if($grader->status==1)
                                            {
                                                 echo "checked=checked";
                                            };?>
                                              >
                                            <label class="custom-control-label" for="customRadioInline1">Active</label>
                                          </div>
                                          <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="customRadioInline2" name="status" class="custom-control-input" value="0"
                                            <?php if($grader->status==0)
                                            {
                                                 echo "checked=checked";
                                            };?>

                                            >
                                            <label class="custom-control-label" for="customRadioInline2">In-Active</label>
                                          </div>
                                        </div>
                                      </div>
                                    </div>

                                </div>       


                                  
                                <div class="button-block clearfix">
                                  <div class="bttn-group">
                                      <button type="submit" class="btn btn-primary">Save</button>
                                  </div>

                                </div> 

                            </div>                                
                        </div>
                    </form>




                </div>

                <div role="tabpanel" class="tab-pane" id="tab_two">

                

                        <div class="page-container">

                            <div>
                                <h4 class="form-title">Module details</h4>
                            </div>

                            
                            <form id="form_module" action="" method="post">

                                <div class="form-container">

                                    <div class="row">

                                        <div class="col-lg-6">
                                          <div class="form-group row">
                                            <label class="col-sm-4 col-form-label">Course<span class="text-danger">*</span></label>
                                            <div class="col-sm-8">
                                              <select name="id_course" id="id_course" class="form-control">
                                                <option value="">Select</option>
                                                <?php
                                                if (!empty($courseList))
                                                {
                                                    foreach ($courseList as $record)
                                                    {
                                                    ?>
                                                        <option value="<?php echo $record->id;  ?>">
                                                            <?php echo $record->name;  ?>        
                                                        </option>
                                                    <?php
                                                    }
                                                }
                                                ?>
                                              </select>
                                            </div>
                                          </div>
                                        </div>


                                


                                        <div class="col-lg-6">
                                          <div class="bttn-group">
                                              <button type="button" class="btn btn-primary" onclick="addGraderModuleData()">Save</button>
                                          </div>
                                        </div>

                                    </div>

                                </div>

                            </form>



                    

                        </div>




                    <?php

                    if(!empty($graderModulesList))
                    {
                        ?>

                        <div class="form-container">
                                <h4 class="form-group-title">Grader Module Details</h4>

                            

                              <div class="custom-table">
                                <table class="table">
                                    <thead>
                                        <tr>
                                        <th>Sl. No</th>
                                         <th>Course Code</th>
                                         <th>Course Name</th>
                                         <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                         <?php
                                     $total = 0;
                                      for($i=0;$i<count($graderModulesList);$i++)
                                     { ?>
                                        <tr>
                                        <td><?php echo $i+1;?></td>
                                        <td><?php echo $graderModulesList[$i]->course_code;?></td>
                                        <td><?php echo $graderModulesList[$i]->course_name;?></td>
                                        <td><a onclick="deleteGraderModule(<?php echo $graderModulesList[$i]->id; ?>)">Delete</a>
                                        </td>

                                         </tr>
                                      <?php 
                                  } 
                                  ?>
                                    </tbody>
                                </table>
                              </div>

                            </div>


                    <?php
                    
                    }
                     ?>




                </div>


                </div>



            </div>
    

    </div>



                
</main>

<script>

    $('select').select2();


    function addGraderModuleData()
    {
        // alert('11');
        if($('#form_module').valid())
        {
            var formData = {};

            formData['id_grader'] = <?php echo $grader->id;?>;
            formData['status'] = <?php echo 1;?>;
            formData['id_course'] = $("#id_course").val();

             $.ajax({
                url: "/exam/grader/addGraderModuleData",
                type: "post",
                data:
                {
                    tempData: formData
                },
                success: function(data)
                {
                    // alert(data);
                    window.location.reload();
                    // $("#clotalediv").html(d);
                }
            });
        }
    }


    function deleteGraderModule(id_grader_course)
    {
        
        $.get("/exam/grader/deleteGraderModule/"+id_grader_course, function(data, status)
        {
            // alert(data);
            window.location.reload();
            // $("#clotalediv").html(data);
        });
    }



    $(document).ready(function() {
        $("#form_module").validate({
            rules: {
                id_course: {
                    required: true
                }
            },
            messages: {
                id_course: {
                    required: "<p class='error-text'>Select Course</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });



    $(document).ready(function() {
        $("#form_main").validate({
            rules: {
                first_name: {
                    required: true
                },
                 last_name: {
                    required: true
                },
                 salutation: {
                    required: true
                },
                 status: {
                    required: true
                },
                mobile: {
                    required: true
                },
                nric: {
                    required: true
                }
            },
            messages: {
                first_name: {
                    required: "<p class='error-text'>First Name required</p>",
                },
                last_name: {
                    required: "<p class='error-text'>Last Name Required</p>",
                },
                salutation: {
                    required: "<p class='error-text'>Select Salutation</p>",
                },
                status: {
                    required: "<p class='error-text'>Select Status</p>",
                },
                mobile: {
                    required: "<p class='error-text'>Mobile Number Required</p>",
                },
                nric: {
                    required: "<p class='error-text'>NRIC Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>