<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
  <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
    <h1 class="h3">Grader Exam List</h1>
  </div>
  <div class="page-container">
     <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="">
              Advanced Search
              <i class="fa fa-angle-right" aria-hidden="true"></i>
              <i class="fa fa-angle-down" aria-hidden="true"></i>
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">

          <div id="collapseOne" class="panel-collapse collapse show" role="tabpanel" aria-labelledby="headingOne" aria-expanded="true" style="">
            <div class="panel-body p-2">

              <div class="row">
                <div class="col-lg-6">
                  <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Exam Sitting</label>
                    <div class="col-sm-9">

                       <select name="id_exam_sitting" id="id_exam_sitting" class="form-control" >
                            <option value="">Select</option>
                            <?php
                            if (!empty($examSitting))
                            {
                                foreach ($examSitting as $record)
                                {
                                    $class="";
                                    if($record->id==$searchParam['id_exam_sitting']) {
                                       $class="selected=selected";
                                    } ?>
                             <option value="<?php echo $record->id;  ?>" <?php echo $class;?>>
                                <?php echo $record->exam_sitting_name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                  </div>
                </div>

                 <div class="col-lg-6">
                  <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Exam Event</label>
                    <div class="col-sm-9">
                      <input type="text" class="form-control" id="exam_event_name" name="exam_event_name"  value="<?php echo $searchParam['exam_event_name'] ?>">
                    </div>
                  </div>
                </div>

            
              </div>

              <hr />
              <div class="d-flex justify-content-center">
                <button type="submit" class="btn btn-primary">Search</button>
                <a href="list" class="btn btn-link" >Clear All Fields</a>
              </div>
            </div>
          </div>

        </form>

      </div>
    </div>


    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Sl. No</th>
            <th>Exam Sitting</th>
            <th>Exam Event</th>
            <th>Module Name</th>
            <th>Module Code</th>
            
            <th>Exam Date</th>
            <th>Start Time</th>
            <th>End Date</th>
            <th>End Time</th>
            <th>Total Students</th>
            <th>Grader Name</th>
            <th>Grading Status</th>
                     </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($examEventList)) {
              
            $i = 1;
                    $this->load->model('grader_exam_model');

            foreach ($examEventList as $record) {

             

                $totalassignedstudentstograder = 0;
                  $markedStatus = 0;
                  $resultStatus = 0;


              $totalStudentforexamevent = 0;
              $totalAssignedGrader = 0;
              $totalStudentPergrader = 0;


              $countOfStudent = $this->grader_exam_model->getCountOfQuestions($record->id);
              if($countOfStudent) {
                $totalStudentforexamevent = $countOfStudent[0]->totalcount;
              }


               $getGraderForExamEvent = $this->grader_exam_model->getGraderCount($record->id);
               
                $details="";
               if($getGraderForExamEvent) {

                for($l=0;$l<count($getGraderForExamEvent);$l++) {

                  $gradername = $getGraderForExamEvent[$l]->gradername;

                  $id_grader_exam = $getGraderForExamEvent[$l]->id;


                  $getGraderForExamEventResults = $this->grader_exam_model->getGraderStudentCount($id_grader_exam);


                  for($m=0;$m<count($getGraderForExamEventResults);$m++) {
                    if($getGraderForExamEventResults[$m]->essay_status=='Marked') {
                      $markedStatus = $markedStatus + 1;
                    }

                     if($getGraderForExamEventResults[$m]->result_status=='15') {
                      $resultStatus = $resultStatus + 1;
                    }
                  }


                   $totalStudentPergrader = count($getGraderForExamEventResults);

                    $totalAssignedGrader = $totalAssignedGrader + $totalStudentPergrader;

                    $details.="<p>$gradername / <a href='/exam/graderStudentTag/add/$id_grader_exam'>$totalStudentPergrader</a></p>";

                    $totalassignedstudentstograder = $totalassignedstudentstograder + $getGraderForExamEventResults[0]->totalcount;
                }
              }

           $idexamevent = $record->id;
          ?>
              <tr>
                <td><?php echo $i ?></td>
                <td><?php echo $record->exam_sitting_name;?></td>
                <td><?php echo $record->name; ?></td>
                <td><?php echo $record->coursename; ?></td>
                <td><?php echo $record->coursecode; ?></td>
                <td><?php echo date('d-m-Y',strtotime($record->exam_date)); ?></td>
                <td><?php echo $record->from_tm;?></td>
                <td><?php echo date('d-m-Y',strtotime($record->exam_end_date)); ?></td>

                <td><?php echo $record->to_tm;?></td>
                <td><?php echo $totalStudentforexamevent;?>
                     <br/>
                     <?php if ($totalStudentforexamevent!=$totalAssignedGrader) { ?> 
                          <a href="/exam/graderExam/add/<?php echo $idexamevent;?>"><?php echo $totalStudentforexamevent-$totalAssignedGrader;?> Unassigned Students</a>

                      <?php } ?>
                </td>

                <?php if($totalAssignedGrader=='0') { ?>
                               <td>Unassigned</td>

                  <?php } else { ?>
                    <td><?php echo $details;?></td> 
                  <?php } ?> 

                              <td>
                                
                                <?php  if ($resultStatus==$totalStudentforexamevent) {  
                                  echo "Approved";
                                }
                                else if($totalAssignedGrader=='0') {
                                  echo "Not assigned";
                                } else if ($totalStudentforexamevent==$totalassignedstudentstograder) {  
                                  echo "All Assigned";
                                } else if ($markedStatus==$totalStudentforexamevent) {  
                                  echo "Submitted pending Approval";
                                } else if ($markedStatus>0 && $markedStatus<$totalStudentforexamevent) {  
                                  echo "Partially gradded";
                                }?>
                              </td> 

              </tr>
          <?php
              $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>)
</main>
<script>
  $('select').select2();

  function clearSearchForm() {
    window.location.reload();
  }
</script>