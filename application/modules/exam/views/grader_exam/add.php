<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
        <h1 class="h3">Add Grader Exam</h1>
        
        <a href='/exam/graderExam/list' class="btn btn-link ml-auto">
            <i class="fa fa-chevron-left" aria-hidden="true"></i>
          Back
        </a>

    </div>
    









        <form id="form_main" action="" method="post" autocomplete="off">

            <div class="page-container">

              <div>
                <h4 class="form-title">Exam Event Details</h4>
              </div>
                <div class="form-container">


                    <div class="row">
                      <div class="col-sm-12">
                       <div class="custom-table">
                          <table class="table" id="list-table">
                            <thead>
                              <tr>
                                <th>Sl. No</th>
                                <th>Exam Event Name</th>
                                <th>Module Code</th>
                                <th>Module Name</th>
                                <th>Exam Date</th>
                                <th>Start Time</th>
                                <th>Exam End Date</th>
                                <th>End Time</th>
                                         </tr>
                            </thead>
                            <tbody>
                               <tr>
                                <td>1.</td>
                                <td><?php echo $examEventDetails->name;?></td>
                                <td><?php echo $examEventDetails->coursecode;?></td>
                                <td><?php echo $examEventDetails->coursename;?></td>
                                <td><?php echo date('d-m-Y',strtotime($examEventDetails->exam_date));?></td>
                                <td><?php echo $examEventDetails->from_tm;?></td>
                                <td><?php echo date('d-m-Y',strtotime($examEventDetails->exam_end_date));?></td>

                                <td><?php echo $examEventDetails->to_tm;?></td>
                               </tr>
                            </tbody>
                          </table>
                        </div>
                    </div>
                  </div>
                </div>



            <div class="page-container">

              <div>
                <h4 class="form-title">Grader details</h4>
              </div>

                <div class="form-container">


                    <div class="row">

                      <div class="col-lg-6">
                          <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Grader <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                              <select name="id_grader" id="id_grader"  class="form-control" required="">
                              <!-- class="form-control" onchange="getExamCenterByGrader(this.value)" -->
                                <option value="">Select</option>
                                <?php
                                if (!empty($graderList))
                                {
                                    foreach ($graderList as $record)
                                    {
                                    ?>
                                        <option value="<?php echo $record->id;  ?>">
                                            <?php echo $record->full_name;  ?>        
                                        </option>
                                    <?php
                                    }
                                }
                                ?>
                              </select>
                            </div>
                          </div>
                        </div>




                        <div class="col-lg-6">

                         <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Grader Type <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                              <select name="id_grader_type" id="id_grader_type" class="form-control"
                              
                                required="">
                                <option value="">Select</option>
                                <option value="1">1st Grader</option>
                                <option value="1">2nd Grader</option>

                              </select>
                            </div>
                          </div>

                        </div>


                           <div class="col-lg-6">
                            <div class="form-group row">
                              <label class="col-sm-4 col-form-label">Marking Start Date <span class="text-danger">*</span></label>
                              <div class="col-sm-8">
                                <span id="view_exam_event">
                                    <input type='text' class="form-control datepicker" id='start_date' name='start_date'  required="">
                                </span>
                              </div>
                            </div>
                        </div>


                           <div class="col-lg-6">
                            <div class="form-group row">
                              <label class="col-sm-4 col-form-label">Marking End Date <span class="text-danger">*</span></label>
                              <div class="col-sm-8">
                                <span id="view_exam_event">
                                    <input type='text' class="form-control datepicker" id='end_date' name='end_date'  required="">
                                </span>
                              </div>
                            </div>
                        </div>
                    </div>

     


                      
                    <div class="button-block clearfix">
                      <div class="bttn-group">
                          <button type="submit" class="btn btn-primary">Save</button>
                            <!-- <a onclick="reloadPage()" class="btn btn-link">Clear All Fields</a> -->
                          <button onclick="reloadPage()" class="btn btn-link">Clear All Fields</button>
                      </div>

                    </div> 

                </div>                                
            </div>
     




            <div class="form-container">
                    <h4 class="form-group-title">Student Details</h4>

                

                  <div class="custom-table">
                    <table class="table">
                        <thead>
                            <tr>
                              <th><input type='checkbox' name='checkAll' id='checkAll' onclick="validate()" />Check All</th>
                            <th>Sl. No</th>
                             <th>Name</th>
                             <th>Email</th>
                             <th>NRIC</th>
                             <th>Membership Number</th>
                             <th>Batch</th>
                             <!-- <th>Action</th> -->
                            </tr>
                        </thead>
                        <tbody>
                             <?php
                         $total = 0;
                          for($i=0;$i<count($studentList);$i++)
                         { ?>
                            <tr>
                            <td><input type='checkbox' name='stdid[]' value='<?php echo $studentList[$i]->id_student;?>'/></td>
                            <td><?php echo $i+1;?></td>
                            <td><?php echo $studentList[$i]->full_name;?></td>
                            <td><?php echo $studentList[$i]->email_id;?></td>
                            <td><?php echo $studentList[$i]->nric;?></td>
                            <td><?php echo $studentList[$i]->membership_number;?></td>
                            <td><?php echo $studentList[$i]->batch;?></td>
                            <!-- <td><a onclick="deleteGraderModule(<?php echo $graderExamList[$i]->id; ?>)">Delete</a> -->
                            </td>

                             </tr>
                          <?php 
                      } 
                      ?>
                        </tbody>
                    </table>
                  </div>

                </div>

   </form>




</main>

<script>

    $('select').select2();

    $( function() {
     

      $("#start_date").datepicker({
        numberOfMonths: 1,
        dateFormat: 'dd-mm-yy',
        onSelect: function (selected) {
            var dt = new Date(selected);
            dt.setDate(dt.getDate() + 1);
            $("#end_date").datepicker("option", "minDate", dt);
        }
    });
    $("#end_date").datepicker({
        numberOfMonths: 1,
        dateFormat: 'dd-mm-yy',        
        onSelect: function (selected) {
            var dt = new Date(selected);
            dt.setDate(dt.getDate() - 1);
            $("#start_date").datepicker("option", "maxDate", dt);
        }
    });

    });




    function reloadPage()
    {
      window.location.reload();
    }


    function getExamEventByExamCenter(id)
    {
      if(id)
      {
        $.get("/exam/graderExam/getExamEventByExamCenter/"+id, function(data, status)
        {  
            $("#view_exam_event").html(data);
        });
      }
    }


    function getExamEventByExamCenterNIdGrader(id)
    {
        var tempPR = {};
    
        tempPR['id_grader'] = $("#id_grader").val();
        tempPR['id_exam_center'] = $("#id_exam_center").val();

        if(id != '')
        {
          $.ajax(
          {
             url: '/exam/graderStudentTag/getExamEventByExamCenterNIdGrader',
             type: 'POST',
             data:
             {
              tempData: tempPR
             },
             error: function()
             {
              alert('Something is wrong');
             },
             success: function(result)
             {
              $("#view_exam_event").html(result);
              // alert(result);
              // $('#myModal').modal('hide');
              // var ta = $("#total_detail").val();
              // alert(ta);
              // $("#amount").val(ta);
             }
          });
        }        
    }

    

    $(document).ready(function() {
        $("form_main").submit(function(){
           if ($('input:checkbox').filter(':checked').length < 1){
                  alert("Check at least one Game!");
           return false;
           }
        });

        $("#form_main").validate({
            rules: {
                id_exam_center: {
                    required: true
                },
                 id_exam_event: {
                    required: true
                },
                 id_grader: {
                    required: true
                }
                ,
                 id_grader_type: {
                    required: true
                }
                ,
                 start_date: {
                    required: true
                }
                ,
                 end_date: {
                    required: true
                }
            },
            messages: {
                id_exam_center: {
                    required: "<p class='error-text'>Select Exam Center</p>",
                },
                id_exam_event: {
                    required: "<p class='error-text'>Select Exam Event</p>",
                },
                id_grader: {
                    required: "<p class='error-text'>Select Grader</p>",
                },
                id_grader_type: {
                    required: "<p class='error-text'>Select Grader Type</p>",
                },
                start_date: {
                    required: "<p class='error-text'>Enter Marking Start Date</p>",
                },
                end_date: {
                    required: "<p class='error-text'>Enter Marking End Date</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

  function validate()
  {
    var statuscheck = $("#checkAll").is(':checked');

    if(statuscheck==true)
    {
        $('input:checkbox').prop('checked', true);
    }

    if(statuscheck==false)
    {
        $('input:checkbox').prop('checked', false);
    }    
  }


</script>