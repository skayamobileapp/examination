<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
      <h1 class="h3">Exam Name List For Question Tagging</h1>
      <!-- <a href="add" class="btn btn-primary ml-auto">+ Add Exam Name</a> -->
    </div>
    <div class="page-container">
      <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingOne">
                <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="">
                        Advanced Search
                        <i class="fa fa-angle-right" aria-hidden="true"></i>
                        <i class="fa fa-angle-down" aria-hidden="true"></i>
                    </a>
                </h4>
            </div>
          <form action="" method="post" id="searchForm">

            <div id="collapseOne" class="panel-collapse collapse show" role="tabpanel" aria-labelledby="headingOne" aria-expanded="true" style="">
            <div class="panel-body p-2">

              <div class="row">
                <div class="col-lg-6">
                  <div class="form-group row">
                    <label class="col-sm-4 col-form-label">Exam</label>
                    <div class="col-sm-8">
                      <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="<?php echo $searchParam['name'] ?>">
                    </div>
                  </div>
                </div>
              </div>
                        
              <hr/>          
                <div class="d-flex justify-content-center">
                    <button type="submit" class="btn btn-primary">Search</button>
                    <a href="list" class="btn btn-link">Clear All Fields</a>
                    <!-- <button type="reset" class="btn btn-link" onclick="clearSearchForm()">Clear All Fields</button> -->
                </div>
            </div>
            </div>

          </form>

        </div>
    </div>            
      <div class="custom-table">
        <table class="table" id="list-table">
            <thead>
                <tr>
                  <th>Sl. No</th>
                  <th>Exam Name</th>
                  <th>Total Marks</th>
                  <th>Total Questions</th>
                  <th>Total Time(Min)</th>
                  <!-- <th>Name In Other Language</th> -->
                  <th class="text-center">Status</th>
                  <th style="text-align: center;">Action</th>
                </tr>
            </thead>
           <tbody>
                <?php
                if (!empty($examNameList)) {
                  $i=1;
                  foreach ($examNameList as $record) {
                ?>
                    <tr>
                      <td><?php echo $i ?></td>
                      <td><?php echo ucfirst($record->name) ?></td>
                      <td><?php echo $record->total_marks ?></td>
                      <td><?php echo $record->total_question ?></td>
                      <td><?php echo $record->total_time ?></td>
                      <td style="text-align: center;"><?php if( $record->status == '1')
                        {
                          echo "Active";
                        }
                        else
                        {
                          echo "In-Active";
                        } 
                        ?>
                      </td>
                      <td class="text-center">

                        <a href="<?php echo 'addQuestion/'.$record->id; ?>" class="btn" type="" data-toggle="tooltip" data-placement="top" title="Add Question" >
                          <i class="fa fa-plus" aria-hidden="true">
                          </i>
                        </a>

                      </td>
                    </tr>
                <?php
                $i++;
                  }
                }
                ?>
              </tbody>
        </table>
    </div>             
  </div>
</main>
<script>

    $('select').select2();
  
    function clearSearchForm()
    {
      window.location.reload();
    }
</script>