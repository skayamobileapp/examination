 <div class="container-fluid">
      <div class="row">
        <nav id="sidebarMenu" class="col-md-3 d-md-block sidebar collapse">
          <div class="sidebar-sticky pt-3">
            <div class="nav-fold">
              <a href="#" class="d-flex align-items-center">
                <span class="user-avatar"><img src="<?php echo BASE_PATH; ?>assets/img/user.jpg"/></span>
                <span>
                  <?php echo $name; ?>
                  <small class="d-block"><?php echo $role; ?></small>
                  <small class="d-block"></small>                  
                </span>
              </a>
            </div>
            <ul class="nav flex-column mb-2">
            
              
              <li class="nav-item">
                <a class="nav-link <?php 
                  if(in_array($pageCode,array('exam_name.list','exam_name.add','exam_name.edit','exam_center.list','exam_center.add','exam_center.edit','exam_event.list','exam_event.add','exam_event.edit','exam_has_question.list','exam_has_question.add','exam_has_question.edit','grade.list','grade.add','grade.edit','location.list','location.add','location.edit'))){  ?>
                  <?php 
                  } else{ 
                  ?>
                  collapsed
                  <?php
                  }
                  ?>" data-toggle="collapse" href="#collapseResults" role="button">
                  <i class="fa fa-book"></i>
                  <span>Examination Setup</span>
                  <i class="fa fa-angle-right ml-auto" aria-hidden="true"></i>
                  <i class="fa fa-angle-down ml-auto" aria-hidden="true"></i>
                </a>
                <ul class="collapse nav <?php 
                  if(in_array($pageCode,array('exam_name.list','exam_name.add','exam_name.edit','exam_center.list','exam_center.add','exam_center.edit','exam_event.list','exam_event.add','exam_event.edit','exam_has_question.list','exam_has_question.add','exam_has_question.edit','grade.list','grade.add','grade.edit','location.list','location.add','location.edit'))){  ?>
                  show
                 <?php 
                  } else{ 
                  ?>
                  
                  <?php
                  }
                  ?>" id="collapseResults">
                   <li class="nav-item">
                    <a href="/exam/examName/list" class="nav-link <?php if(in_array($pageCode,array('exam_name.list','exam_name.edit','exam_name.add'))){echo 'active';}?>">
                      <i class="fa fa-chevron-right"></i>
                      <span>Exam Name</span>                      
                    </a>
                  </li>  
                  
                  <li class="nav-item">
                    <a href="/exam/location/list" class="nav-link <?php if(in_array($pageCode,array('location.list','location.edit','location.add'))){echo 'active';}?>">
                      <i class="fa fa-chevron-right"></i>
                      <span>Exam Location Setup</span>                      
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="/exam/examCenter/list" class="nav-link <?php if(in_array($pageCode,array('exam_center.list','exam_center.edit','exam_center.add'))){echo 'active';}?>">
                      <i class="fa fa-chevron-right"></i>
                      <span>Exam Center Setup</span>                      
                    </a>
                  </li>  

                   <li class="nav-item">
                    <a href="/exam/examEvent/list" class="nav-link <?php if(in_array($pageCode,array('exam_event.list','exam_event.edit','exam_event.add'))){echo 'active';}?>">
                      <i class="fa fa-chevron-right"></i>
                      <span>Exam Event</span>                      
                    </a>
                  </li>  

                                  

                  <!-- <li class="nav-item">
                    <a href="/setup/question/list" class="nav-link">
                      <i class="fa fa-chevron-right"></i>
                      <span>Question</span>                      
                    </a>
                  </li> -->
                </ul>
              </li>   

              <li class="nav-item">
                <a class="nav-link <?php 
                  if(in_array($pageCode,array('grader.list','grader.add','grader.edit','grader_exam.list','grader_exam.edit','grader_exam.add','grader_student_tag.list','grader_student_tag.edit','grader_student_tag.add','grader_marks_approval.add','final_marks_approval.add',''))){  ?>
                  <?php 
                  } else{ 
                  ?>
                  collapsed
                  <?php
                  }
                  ?>" data-toggle="collapse" href="#collapseResults" role="button">
                  <i class="fa fa-book"></i>
                  <span>Grader Setup</span>
                  <i class="fa fa-angle-right ml-auto" aria-hidden="true"></i>
                  <i class="fa fa-angle-down ml-auto" aria-hidden="true"></i>
                </a>
                <ul class="collapse nav <?php 
                  if(in_array($pageCode,array('grader.list','grader.add','grader.edit','grader_exam.list','grader_exam.edit','grader_exam.add','grader_student_tag.list','grader_student_tag.edit','grader_student_tag.add','grader_marks_approval.add','final_marks_approval.add','final_marks_approval.list','secondgrader.list','secondgrader.edit','secondgrader.add'))){  ?>
                  show
                 <?php 
                  } else{ 
                  ?>
                  
                  <?php
                  }
                  ?>" id="collapseResults">
                   <li class="nav-item">
                    <a href="/exam/grader/list" class="nav-link <?php if(in_array($pageCode,array('grader.list','grader.edit','grader.add'))){echo 'active';}?>">
                      <i class="fa fa-chevron-right"></i>
                      <span>Grader List</span>                      
                    </a>
                  </li> 

                  <li class="nav-item">
                    <a href="/exam/graderExam/list" class="nav-link <?php if(in_array($pageCode,array('grader_exam.list','grader_exam.edit','grader_exam.add','grader_student_tag.list','grader_student_tag.edit','grader_student_tag.add'))){echo 'active';}?>">
                      <i class="fa fa-chevron-right"></i>
                      <span>Assign Grader to Exam Set</span>                      
                    </a>
                  </li> 

                  <!-- <li class="nav-item">
                    <a href="/exam/graderStudentTag/list" class="nav-link <?php if(in_array($pageCode,array('grader_student_tag.list','grader_student_tag.edit','grader_student_tag.add'))){echo 'active';}?>">
                      <i class="fa fa-chevron-right"></i>
                      <span>Grader Student Tag</span>                      
                    </a>
                  </li>   -->

                  <li class="nav-item">
                    <a href="/exam/finalMarksApproval/list" class="nav-link <?php if(in_array($pageCode,array('final_marks_approval.add','final_marks_approval.list'))){echo 'active';}?>">
                      <i class="fa fa-chevron-right"></i>
                      <span>Marks Approval</span>                      
                    </a>
                  </li>  

               



                </ul>
              </li>  

                <li class="nav-item">
                <a class="nav-link <?php 
                  if(in_array($pageCode,array('marksadjustment.list'))){  ?>
                  <?php 
                  } else{ 
                  ?>
                  collapsed
                  <?php
                  }
                  ?>" data-toggle="collapse" href="#collapseResults" role="button">
                  <i class="fa fa-book"></i>
                  <span>Marks Adjustment</span>
                  <i class="fa fa-angle-right ml-auto" aria-hidden="true"></i>
                  <i class="fa fa-angle-down ml-auto" aria-hidden="true"></i>
                </a>
                <ul class="collapse nav <?php 
                  if(in_array($pageCode,array('marksadjustment.list','marksadjustmentapproval.list'))){  ?>
                  show
                 <?php 
                  } else{ 
                  ?>
                  
                  <?php
                  }
                  ?>" id="collapseResults">
                   <li class="nav-item">
                    <a href="/exam/marksadjustment/list" class="nav-link <?php if(in_array($pageCode,array('marksadjustment.list'))){echo 'active';}?>">
                      <i class="fa fa-chevron-right"></i>
                      <span>Entry Marks Adjustment</span>                      
                    </a>
                  </li> 

                   <li class="nav-item">
                    <a href="/exam/marksadjustmentapproval/list" class="nav-link <?php if(in_array($pageCode,array('marksadjustmentapproval.list'))){echo 'active';}?>">
                      <i class="fa fa-chevron-right"></i>
                      <span>Marks Adjustment Approval</span>                      
                    </a>
                  </li> 

                  </ul>            

 
              <li class="nav-item" style="">
                <a class="nav-link <?php 
                  if(in_array($pageCode,array('api.fetchschedule'))){  ?>
                  <?php 
                  } else{ 
                  ?>
                  collapsed
                  <?php
                  }
                  ?>" data-toggle="collapse" href="#collapseResults" role="button">
                  <i class="fa fa-book"></i>
                  <span>API</span>
                  <i class="fa fa-angle-right ml-auto" aria-hidden="true"></i>
                  <i class="fa fa-angle-down ml-auto" aria-hidden="true"></i>
                </a>
                <ul class="collapse nav <?php 
                  if(in_array($pageCode,array('api.fetchschedule'))){  ?>
                  show
                 <?php 
                  } else{ 
                  ?>
                  
                  <?php
                  }
                  ?>" id="collapseResults">
                   <li class="nav-item">
                    <a href="/exam/api/fetchschedule" class="nav-link <?php if(in_array($pageCode,array('api.fetchschedule'))){echo 'active';}?>">
                      <i class="fa fa-chevron-right"></i>
                      <span>Fetch Schedule</span>                      
                    </a>
                  </li> 
                   <li class="nav-item">
                    <a href="/exam/api/result" class="nav-link <?php if(in_array($pageCode,array('api.fetchschedule'))){echo 'active';}?>">
                      <i class="fa fa-chevron-right"></i>
                      <span>Push Marks</span>                      
                    </a>
                  </li> 

                  
                  

                </ul>
              </li>               


                                     
          </ul>         
        </div>
      </nav>
    </div>
  </div>
<!-- 
<li><a href="/exam/location/list">Exam Locaction Setup</a></li>
                        <li><a href="/exam/examCenter/list">Exam Center Setup</a></li>
                        <li><a href="/exam/examEvent/list">Exam Event</a></li>
                        <li><a href="/exam/examName/list">Exam Name</a></li>
                        <li><a href="/exam/examHasQuestion/list">Exam Has Question</a></li>


 -->