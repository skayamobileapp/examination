 <div class="container-fluid">
      <div class="row">
        <nav id="sidebarMenu" class="col-md-3 d-md-block sidebar collapse">
          <div class="sidebar-sticky pt-3">
            <div class="nav-fold">
              <a href="#" class="d-flex align-items-center">
                <span class="user-avatar"><img src="<?php echo BASE_PATH; ?>assets/img/user.jpg"/></span>
                <span>
                  <?php echo $name; ?>
                  <small class="d-block"><?php echo $role; ?></small>
                  <small class="d-block"></small>                  
                </span>
              </a>
            </div>
            <ul class="nav flex-column mb-2">
            
              
              <li class="nav-item">
                <a class="nav-link <?php 
                  if(in_array($pageCode,array('exam_name.list','exam_name.add','exam_name.edit','exam_center.list','exam_center.add','exam_center.edit','exam_event.list','exam_event.add','exam_event.edit','exam_has_question.list','exam_has_question.add','exam_has_question.edit','grade.list','grade.add','grade.edit','location.list','location.add','location.edit','exam_setup.list','exam_absent.list'))){  ?>
                  <?php 
                  } else{ 
                  ?>
                  collapsed
                  <?php
                  }
                  ?>" data-toggle="collapse" href="#collapseResults" role="button">
                  <i class="fa fa-book"></i>
                  <span>Examination Setup</span>
                  <i class="fa fa-angle-right ml-auto" aria-hidden="true"></i>
                  <i class="fa fa-angle-down ml-auto" aria-hidden="true"></i>
                </a>


                <ul class="collapse nav <?php 
                  if(in_array($pageCode,array('examdashboard.list','exam_name.list','exam_name.add','exam_name.edit','exam_center.list','exam_center.add','exam_center.edit','exam_event.list','exam_event.add','exam_event.edit','exam_has_question.list','exam_has_question.add','exam_has_question.edit','grade.list','grade.add','grade.edit','location.list','location.add','location.edit','exam_setup.list','exam_logout.list','exam_absent.list'))){  ?>
                  show
                 <?php 
                  } else{ 
                  ?>
                  
                  <?php
                  }
                  ?>" id="collapseResults">

                 
                  

                    <li class="nav-item">
                    <a href="/exam/examSetup/list" class="nav-link <?php if(in_array($pageCode,array('exam_setup.list'))){echo 'active';}?>">
                      <i class="fa fa-chevron-right"></i>
                      <span>Exam / Email Remainder </span>                      
                    </a>
                  </li>  


                   <li class="nav-item">
                    <a href="/exam/examLogout/list" class="nav-link <?php if(in_array($pageCode,array('exam_logout.list'))){echo 'active';}?>">
                      <i class="fa fa-chevron-right"></i>
                      <span>Exam Logout Session </span>                      
                    </a>
                  </li>  

                      <li class="nav-item">
                    <a href="/exam/examAbsent/list" class="nav-link <?php if(in_array($pageCode,array('exam_absent.list'))){echo 'active';}?>">
                      <i class="fa fa-chevron-right"></i>
                      <span>Exam Absent Session </span>                      
                    </a>
                  </li>  
                  
                  <li class="nav-item">
                    <a href="/exam/location/list" class="nav-link <?php if(in_array($pageCode,array('location.list','location.edit','location.add'))){echo 'active';}?>">
                      <i class="fa fa-chevron-right"></i>
                      <span>Exam Location Setup</span>                      
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="/exam/examCenter/list" class="nav-link <?php if(in_array($pageCode,array('exam_center.list','exam_center.edit','exam_center.add'))){echo 'active';}?>">
                      <i class="fa fa-chevron-right"></i>
                      <span>Exam Center Setup</span>                      
                    </a>
                  </li>  

                   <li class="nav-item">
                    <a href="/exam/examEvent/list" class="nav-link <?php if(in_array($pageCode,array('exam_event.list','exam_event.edit','exam_event.add'))){echo 'active';}?>">
                      <i class="fa fa-chevron-right"></i>
                      <span>Exam Event</span>                      
                    </a>
                  </li>  
                </ul>
              </li>   

              <li class="nav-item">
                <a class="nav-link <?php 
                  if(in_array($pageCode,array('grader.list','grader.add','grader.edit','grader_exam.list','grader_exam.edit','grader_exam.add','grader_student_tag.list','grader_student_tag.edit','grader_student_tag.add','grader_marks_approval.add'))){  ?>
                  <?php 
                  } else{ 
                  ?>
                  collapsed
                  <?php
                  }
                  ?>" data-toggle="collapse" href="#collapseResults" role="button">
                  <i class="fa fa-book"></i>
                  <span>Grader Setup</span>
                  <i class="fa fa-angle-right ml-auto" aria-hidden="true"></i>
                  <i class="fa fa-angle-down ml-auto" aria-hidden="true"></i>
                </a>
                <ul class="collapse nav <?php 
                  if(in_array($pageCode,array('grader.list','grader.add','grader.edit','grader_exam.list','grader_exam.edit','grader_exam.add','grader_student_tag.list','grader_student_tag.edit','grader_student_tag.add'))){  ?>
                  show
                 <?php 
                  } else{ 
                  ?>
                  
                  <?php
                  }
                  ?>" id="collapseResults">
                   <li class="nav-item">
                    <a href="/exam/grader/list" class="nav-link <?php if(in_array($pageCode,array('grader.list','grader.edit','grader.add'))){echo 'active';}?>">
                      <i class="fa fa-chevron-right"></i>
                      <span>Grader List</span>                      
                    </a>
                  </li> 

                  <li class="nav-item">
                    <a href="/exam/graderExam/list" class="nav-link <?php if(in_array($pageCode,array('grader_exam.list','grader_exam.edit','grader_exam.add','grader_student_tag.list','grader_student_tag.edit','grader_student_tag.add'))){echo 'active';}?>">
                      <i class="fa fa-chevron-right"></i>
                      <span>Assign Grader to Exam Set</span>                      
                    </a>
                  </li> 


                </ul>
              </li>  


              <li class="nav-item">
                <a class="nav-link <?php 
                  if(in_array($pageCode,array('final_marks_approval.add','final_marks_approval.list'))){  ?>
                  <?php 
                  } else{ 
                  ?>
                  collapsed
                  <?php
                  }
                  ?>" data-toggle="collapse" href="#collapseResults" role="button">
                  <i class="fa fa-book"></i>
                  <span>Marks Approval</span>
                  <i class="fa fa-angle-right ml-auto" aria-hidden="true"></i>
                  <i class="fa fa-angle-down ml-auto" aria-hidden="true"></i>
                </a>
                <ul class="collapse nav <?php 
                  if(in_array($pageCode,array('final_marks_approval.add','final_marks_approval.list','final_marks_approval_history.list'))){  ?>
                  show
                 <?php 
                  } else{ 
                  ?>
                  
                  <?php
                  }
                  ?>" id="collapseResults">
                   
                  <li class="nav-item">
                    <a href="/exam/finalMarksApproval/list" class="nav-link <?php if(in_array($pageCode,array('final_marks_approval.add','final_marks_approval.list'))){echo 'active';}?>">
                      <i class="fa fa-chevron-right"></i>
                      <span>Marks Approval</span>                      
                    </a>
                  </li>  



                  <li class="nav-item">
                    <a href="/exam/finalMarksApproval/history" class="nav-link <?php if(in_array($pageCode,array('final_marks_approval_history.list'))){echo 'active';}?>">
                      <i class="fa fa-chevron-right"></i>
                      <span>Marks Approval History</span>                      
                    </a>
                  </li>  

               



                </ul>
              </li> 

                <li class="nav-item">
                <a class="nav-link <?php 
                  if(in_array($pageCode,array('marksadjustment.list','marksadjustmenthistory.list'))){  ?>
                  <?php 
                  } else{ 
                  ?>
                  collapsed
                  <?php
                  }
                  ?>" data-toggle="collapse" href="#collapseResults" role="button">
                  <i class="fa fa-book"></i>
                  <span>Marks Adjustment</span>
                  <i class="fa fa-angle-right ml-auto" aria-hidden="true"></i>
                  <i class="fa fa-angle-down ml-auto" aria-hidden="true"></i>
                </a>
                <ul class="collapse nav <?php 
                  if(in_array($pageCode,array('marksadjustment.list','marksadjustmentapproval.list','marksadjustmenthistory.list'))){  ?>
                  show
                 <?php 
                  } else{ 
                  ?>
                  
                  <?php
                  }
                  ?>" id="collapseResults">
                   <li class="nav-item">
                    <a href="/exam/marksadjustment/list" class="nav-link <?php if(in_array($pageCode,array('marksadjustment.list'))){echo 'active';}?>">
                      <i class="fa fa-chevron-right"></i>
                      <span>Entry Marks Adjustment</span>                      
                    </a>
                  </li> 

                   <li class="nav-item">
                    <a href="/exam/marksadjustmentapproval/list" class="nav-link <?php if(in_array($pageCode,array('marksadjustmentapproval.list'))){echo 'active';}?>">
                      <i class="fa fa-chevron-right"></i>
                      <span>Marks Adjustment Approval</span>                      
                    </a>
                  </li> 

                  <li class="nav-item">
                    <a href="/exam/marksadjustmenthistory/list" class="nav-link <?php if(in_array($pageCode,array('marksadjustmenthistory.list'))){echo 'active';}?>">
                      <i class="fa fa-chevron-right"></i>
                      <span>Marks Adjustment History</span>                      
                    </a>
                  </li> 


                  </ul>            

 
              <li class="nav-item" style="">
                <a class="nav-link <?php 
                  if(in_array($pageCode,array('api.fetchschedule'))){  ?>
                  <?php 
                  } else{ 
                  ?>
                  collapsed
                  <?php
                  }
                  ?>" data-toggle="collapse" href="#collapseResults" role="button">
                  <i class="fa fa-book"></i>
                  <span>API</span>
                  <i class="fa fa-angle-right ml-auto" aria-hidden="true"></i>
                  <i class="fa fa-angle-down ml-auto" aria-hidden="true"></i>
                </a>
                <ul class="collapse nav <?php 
                  if(in_array($pageCode,array('api.fetchschedule','finalmarksapproval.list','finalmarksapprovalhistory.list'))){  ?>
                  show
                 <?php 
                  } else{ 
                  ?>
                  
                  <?php
                  }
                  ?>" id="collapseResults">
                   <li class="nav-item">
                    <a href="/exam/api/fetchschedule" class="nav-link <?php if(in_array($pageCode,array('api.fetchschedule'))){echo 'active';}?>">
                      <i class="fa fa-chevron-right"></i>
                      <span>Fetch Schedule and Students</span>                      
                    </a>
                  </li> 
                  
                  <li class="nav-item">
                    <a href="/exam/finalMarksApproval/attendance" class="nav-link <?php if(in_array($pageCode,array('finalmarksapproval.list'))){echo 'active';}?>">
                      <i class="fa fa-chevron-right"></i>
                      <span>Push Marks to MQMS</span>                      
                    </a>
                  </li> 

                  <li class="nav-item">
                    <a href="/exam/finalMarksApprovalHistory/attendance" class="nav-link <?php if(in_array($pageCode,array('finalmarksapprovalhistory.list'))){echo 'active';}?>">
                      <i class="fa fa-chevron-right"></i>
                      <span>Push Marks to MQMS History</span>                      
                    </a>
                  </li> 
                  
                  
                  

                  
                  

                </ul>
              </li>               


                                     
          </ul>         
        </div>
      </nav>
    </div>
  </div>
