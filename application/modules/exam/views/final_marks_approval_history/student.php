<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
  <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
    <h1 class="h3">Student Exam Registration</h1>
       <a href='../list' class="btn btn-link ml-auto">
            <i class="fa fa-chevron-left" aria-hidden="true"></i>
          Back
        </a>

  </div>
  <form method="POST" action=""/>
  <div class="form-container">

        <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Exam Event</th>
                        <th>Module</th>

            <th>Exam Date</th>
            <th>Start Time</th>
          </tr>
              <?php
         
            for($i=0;$i<1;$i++) {
          ?>
          <tr>
                                <td><?php echo $examStudentTaggingList[0]->exam_event ?></td>

           <td><?php echo $examStudentTaggingList[0]->coursename ?></td>

                <td><?php echo date('d-m-Y', strtotime($examStudentTaggingList[0]->exam_date));?></td>
                <td><?php echo $examStudentTaggingList[0]->from_tm ?></td>
          </tr>
        <?php } ?> 
        </thead>
        
      </table>
    </div>
</div>
  <div class="form-container">

   
    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Sl. No</th>
            <th>Student</th>
            <th>Email</th>
            <th>Membership Number</th>
            <th>Batch</th>
            <th>Marks</th>
            <th>Attendance</th>

          
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($examStudentTaggingList)) {
            $i = 1;
            foreach ($examStudentTaggingList as $record) {
          ?>
              <tr>
                <td><?php echo $i ?></td>
                <td><?php echo $record->student_name; ?></td>
                <td><?php echo $record->email_id; ?></td>
                <td><?php echo $record->membership_number; ?></td>

                <td><?php echo $record->batch; ?></td>
                <td><?php echo $record->final_total; ?>
                <input type='hidden' name='tag[]' value="<?php echo $record->id;?>"/></td>
                
                <input type='hidden' name='schedule[]' value="<?php echo $record->mqm_schedule_id;?>"/></td>
                
                <td>
                    
                    <input type='radio' name="attendance[<?php echo $record->id;?>]" value='1'<?php if($record->attendence_status=='1') { echo "checked=checked";} ?>>Attended 
                     <input type='radio' name="attendance[<?php echo $record->id;?>]" value='2'<?php if($record->attendence_status=='2') { echo "checked=checked";} ?>>Absent 
                      <input type='radio' name="attendance[<?php echo $record->id;?>]" value='3'<?php if($record->attendence_status=='3') { echo "checked=checked";} ?>>ABsent with valid reason 
                </td>



                               
              </tr>
          <?php
              $i++;
            }
          }
          ?>
        </tbody>
       
      </table>
    </div>
  </div>
  </form>
</main>
<script>
  $('select').select2();

  function clearSearchForm() {
    window.location.reload();
  }
</script>