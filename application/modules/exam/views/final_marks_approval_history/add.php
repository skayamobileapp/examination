<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
        <h1 class="h3">Final Marks Approval</h1>
        
        <!-- <a href='../list' class="btn btn-link ml-auto">
            <i class="fa fa-chevron-left" aria-hidden="true"></i>
          Back
        </a> -->

    </div>
    

        <form id="form_main" action="" method="post">

            <div class="page-container">

              <div>
                <h4 class="form-title">Final Student Marks Approval details</h4>
              </div>

                <div class="form-container">


                    <div class="row">



                        <div class="col-lg-6">
                          <div class="form-group row">
                            <label class="col-sm-4 col-form-label"> Exam Center<span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                              <select name="id_exam_center" id="id_exam_center" class="form-control" onchange="getExamEventByExamCenter(this.value)">
                                <option value="">Select</option>
                                <?php
                                if (!empty($examCenterList))
                                {
                                    foreach ($examCenterList as $record)
                                    {
                                    ?>
                                        <option value="<?php echo $record->id;  ?>">
                                            <?php echo $record->name;  ?>        
                                        </option>
                                    <?php
                                    }
                                }
                                ?>
                              </select>
                            </div>
                          </div>
                        </div>



                        <div class="col-lg-6">
                            <div class="form-group row">
                              <label class="col-sm-4 col-form-label">Exam Event <span class="text-danger">*</span></label>
                              <div class="col-sm-8">
                                <span id="view_exam_event">
                                    <select class="form-control" id='id_exam_event' name='id_exam_event'>
                                      <option value=''></option>
                                    </select>
                                </span>
                              </div>
                            </div>
                        </div>


                    </div>


                    <div class="row">


                        <div class="col-lg-6">
                          <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Student Name </label>
                            <div class="col-sm-8">
                            <input type="text" class="form-control" id="student_name" name="student_name" placeholder="Student Name">
                            </div>
                          </div>
                        </div>

                        <div class="col-lg-6">
                          <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Student Email </label>
                            <div class="col-sm-8">
                            <input type="text" class="form-control" id="student_email" name="student_email" placeholder="Email">
                            </div>
                          </div>
                        </div>

                    </div>




                    <div class="modal-footer">
                       <!-- id="view_search" style="display: none;" -->
                      <button type="button" class="btn btn-default" onclick="searchStudents()">Search</button>
                    </div>


                  <div class="form-container" style="display: none;" id="view_student_display">
                      <h4 class="form-group-title"></h4>


                      <div  id='view_student'>
                      </div>

                  </div>
     


                      
                    <div class="button-block clearfix">
                      <div class="bttn-group">
                          <button type="submit" class="btn btn-primary">Approve</button>
                          <a href="<?php echo '/exam/graderMarksApproval/add' ?>" class="btn btn-link">Clear All Fields</a>
                      </div>

                    </div> 

                </div>                                
            </div>
        </form>


</main>

<script>

    $('select').select2();

    $( function() {
      $( ".datepicker" ).datepicker({
          changeYear: true,
          changeMonth: true,
      });
    });



    function searchStudents()
    {
      if($('#form_main').valid())
        {
          var tempPR = {};
          tempPR['full_name'] = $("#student_name").val();
          tempPR['email_id'] = $("#student_email").val();
          tempPR['id_exam_center'] = $("#id_exam_center").val();
          tempPR['id_exam_event'] = $("#id_exam_event").val();
              $.ajax(
              {
                 url: '/exam/finalMarksApproval/searchStudents',
                 type: 'POST',
                 data:
                 {
                  tempData: tempPR
                 },
                 error: function()
                 {
                  alert('Something is wrong');
                 },
                 success: function(result)
                 {
                  // alert(result);
                  $("#view_student_display").show();
                  $("#view_student").html(result);
                  // $('#myModal').modal('hide');
                  // var ta = $("#total_detail").val();
                  // alert(ta);
                  // $("#amount").val(ta);
                 }
              });
        }
    }


    function getExamEventByExamCenter(id)
    {
      if(id)
      {
        
        $.get("/exam/graderExam/getExamEventByExamCenter/"+id, function(data, status)
        {  
            $("#view_exam_event").html(data);
        });
      }
    }



    

    $(document).ready(function() {
        $("#form_main").validate({
            rules: {
                id_exam_center: {
                    required: true
                },
                 id_exam_event: {
                    required: true
                },
                 id_grader: {
                    required: true
                }
            },
            messages: {
                id_exam_center: {
                    required: "<p class='error-text'>Select Exam Center</p>",
                },
                id_exam_event: {
                    required: "<p class='error-text'>Select Exam Event</p>",
                },
                id_grader: {
                    required: "<p class='error-text'>Select Grader</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>