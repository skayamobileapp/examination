<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
  <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
    <h1 class="h3">Exam Event List</h1>
  </div>
  <div class="page-container">

    <?php
    if ($this->session->flashdata('success')) {
    ?>
      <div class="alert alert-success alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
      </div>
    <?php
    }
    if ($this->session->flashdata('error')) {
    ?>
      <div class="alert alert-danger alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Failure!</strong> <?php echo $this->session->flashdata('error'); ?>
      </div>
    <?php
    }
    ?>
    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Sl. No</th>
            <th>Exam Sitting</th>
            <th>Exam Event Name</th>
            <th>Exam Center</th>
            <th>Event Start Date</th>
            <th>Event Start Time</th>

            <th>Event End Date</th>
            <th>Event End Time</th>
            <th>Total Students</th>
            <!-- <th>Name In Other Language</th> -->
            <th style="text-align: center;">Action</th>

          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($examEventList)) {
            $i = 1;
                    $this->load->model('exam_event_model');

            foreach ($examEventList as $record) {

              $totalStuents = 0;
              $totalStudetnsObject = $this->exam_event_model->examStudentTaggingListSearch($record->id);

              $totalStuents = count($totalStudetnsObject);
          ?>
              <tr>
                <td><?php echo $i ?></td>
                <td><?php echo $record->exam_sitting_name ?></td>
                <td><?php echo $record->name ?></td>
                <td><?php echo $record->examcenternae ?></td>
                <td><?php echo date('d-m-Y', strtotime($record->exam_date)) ?></td>
                <td><?php echo date("H:i", strtotime($record->from_tm)) ?></td>

                  <td><?php echo date('d-m-Y', strtotime($record->exam_end_date)) ?></td>
                <td><?php echo date("H:i", strtotime($record->to_tm)) ?></td>


                <td>
                  <?php echo $totalStuents;?>
                </td>
                  


                  <td class="text-center">

                  <a href="<?php echo 'pushexam/' . $record->id; ?>" class="btn" type="" data-toggle="tooltip" data-placement="top" title="Edit">
                    Data sent to MQMS
                  </a>

                </td>

              </tr>
          <?php
              $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
</main>
<script>
  $('select').select2();

  function clearSearchForm() {
    window.location.reload();
  }
</script>