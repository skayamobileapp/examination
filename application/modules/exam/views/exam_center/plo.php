<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
        <h1 class="h3">Edit Exam Center Details</h1>
        
        <a href='../list' class="btn btn-link ml-auto">
            <i class="fa fa-chevron-left" aria-hidden="true"></i>
          Back
        </a>

    </div>


    <form id="form_main" action="" method="post">

        <div class="page-container">

          <div class="tabs-view">
             <h4><a href="/exam/examCenter/edit/<?php echo $getExamCenterList->id;?>">Exam Center</a></h4>
            <h4><a href="/exam/examCenter/editclo/<?php echo $getExamCenterList->id;?>"  class="active">Room Details</a></h4>
          </div>

            <div class="form-container">


                <div class="row">

                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Room Name <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="room" name="room" placeholder="Room Name" value="<?php echo $clodetails->room;?>">
                        </div>
                      </div>
                    </div>


                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Capacity <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="capacity" name="capacity" placeholder="Capacity" value="<?php echo $clodetails->capacity;?>">
                        </div>
                      </div>
                    </div>

                </div>



                <div class="row">

                    <div class="col-lg-6">
                      <div class="form-group row align-items-center">
                        <label class="col-sm-4 col-form-label">Status <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                          <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="customRadioInline1" name="status" class="custom-control-input" value="1" checked="checked">
                            <label class="custom-control-label" for="customRadioInline1">Active</label>
                          </div>
                          <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="customRadioInline2" name="status" class="custom-control-input" value="0">
                            <label class="custom-control-label" for="customRadioInline2">In-Active</label>
                          </div>
                        </div>
                      </div>
                    </div>

                </div>       


                  

            <div class="button-block clearfix">
              <div class="bttn-group">
                  <button type="submit" class="btn btn-primary">Save</button>
                  <a href='add' class="btn btn-link">Clear All Fields</a>
                  
              </div>

            </div> 
            </div>                                
        </div>
    </form>
    <div class="row">
                <div class="col-sm-12">
          <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Sl. No</th>
            <th>Room Name</th>
            <th>Capacity</th>
        
            <th class="text-center">Status</th>
            <th style="text-align: center;">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($courseDetails)) {
            $i = 1;
            foreach ($courseDetails as $record) {
          ?>
              <tr>
                <td><?php echo $i ?></td>
                <td><?php echo $record->room; ?></td>
                <td><?php echo $record->capacity; ?></td>
               
                <td style="text-align: center;">
                  <?php if ($record->status == '1')
                  {
                    echo "Active";
                  } else {
                    echo "In-Active";
                  }
                  ?>
                </td>
                <td class="text-center">

                  <a href="<?php echo '/exam/examCenter/editclo/'.$record->id_exam_center.'/' . $record->id; ?>" class="btn" type="" data-toggle="tooltip" data-placement="top" title="Edit">
                    <i class="fa fa-pencil-square-o" aria-hidden="true">
                    </i>
                  </a>


                    <a href="javascript:deleteclo(<?php echo $record->id; ?>,<?php echo $record->id_exam_center; ?>)" class="btn" type="" data-toggle="tooltip" data-placement="top" title="Edit">
                    <i class="fa fa-trash" aria-hidden="true">
                    </i>
                  </a>

                </td>
              </tr>
          <?php
              $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>
</div>
    </div>

</main>

<script>
    $(document).ready(function() {
        $("#form_main").validate({
            rules: {
                code: {
                    required: true
                },
                name: {
                    required: true
                }
            },
            messages: {
                code: {
                    required: "<p class='error-text'>Code Required</p>",
                },
                name: {
                    required: "<p class='error-text'>Name Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


    function deleteclo(id,centerid) {
           var formData = {
                'id':id,
                'centerid':centerid //for get email 
            };
        var cnf = confirm("Do you really want to delete?");
        if(cnf==true) {
             $.ajax({
                url: "/exam/examCenter/deleteclo",
                type: "post",
                data: formData,
                success: function(d) {
                     reloadPage();
                }
            });
        }
    }
</script>
<script type="text/javascript">
    function reloadPage()
    {
      window.location.reload();
    }
</script>