<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
        <h1 class="h3">Add Grader Student Exam Tagging</h1>
        
        <a href='/exam/graderExam/list' class="btn btn-link ml-auto">
            <i class="fa fa-chevron-left" aria-hidden="true"></i>
          Back
        </a>

    </div>







            <div class="m-auto text-center">
                <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
            </div>


            <div class="clearfix">

                <ul class="nav nav-tabs" role="tablist" >
                    <li role="presentation" class="nav-item" ><a href="#tab_one" class="nav-link active"
                            aria-controls="tab_one" aria-selected="true"
                            role="tab" data-toggle="tab">Tag New Student</a>
                    </li>
                    <li role="presentation" class="nav-item"><a href="#tab_two" class="nav-link"
                            aria-controls="tab_two" role="tab" data-toggle="tab"> Taged Students</a>
                    </li>
                    
                                     
                </ul>


            





            
                
                <div class="tab-content offers-tab-content">



                    <div role="tabpanel" class="tab-pane active" id="tab_one">
           
                    

                            <form id="form_main" action="" method="post">

                                <div class="page-container">

                                  <div>
                                    <h4 class="form-title">Exam Center details</h4>
                                  </div>

                                    <div class="form-container">


                                        <div class="row">

                                           <div class="col-lg-6">
                                              <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Exam Name <span class="text-danger">*</span></label>
                                                <div class="col-sm-8">
                                                  <select name="id_exam_name" id="id_exam_name" class="form-control">
                                                  <option value="">Select</option>
                                                  <?php
                                                  if (!empty($examNameList))
                                                  {
                                                      foreach ($examNameList as $record)
                                                      {?>
                                                   <option value="<?php echo $record->id;  ?>">
                                                      <?php echo $record->name;?>
                                                   </option>
                                                  <?php
                                                      }
                                                  }
                                                  ?>
                                                </select>
                                                </div>
                                              </div>
                                          </div>




                                            <div class="col-lg-6">
                                              <div class="form-group row">
                                                <label class="col-sm-4 col-form-label"> Exam Center<span class="text-danger">*</span></label>
                                                <div class="col-sm-8">
                                                  <select name="id_exam_center" id="id_exam_center" class="form-control"
                              onchange="getExamEventByExamCenter(this.value)"
                              >
                                                    <option value="">Select</option>
                                                    <?php
                                                    if (!empty($examCenterList))
                                                    {
                                                        foreach ($examCenterList as $record)
                                                        {
                                                        ?>
                                                            <option value="<?php echo $record->id;  ?>"
                                                            
                                                              >
                                                                <?php echo $record->name;  ?>        
                                                            </option>
                                                        <?php
                                                        }
                                                    }
                                                    ?>
                                                  </select>
                                                </div>
                                              </div>
                                            </div>



                                            <div class="col-lg-6">
                                                <div class="form-group row">
                                                  <label class="col-sm-4 col-form-label">Exam Date <span class="text-danger">*</span></label>
                                                  <div class="col-sm-8">
                                                    <span id="view_exam_event">
                                                        <select class="form-control" id='id_exam_event' name='id_exam_event'>
                                                          <option value=''></option>
                                                        </select>
                                                    </span>
                                                  </div>
                                                </div>
                                            </div>




                                        </div>






                                        <div class="row">


                                            <div class="col-lg-6">
                                              <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Student Name </label>
                                                <div class="col-sm-8">
                                                <input type="text" class="form-control" id="student_name" name="student_name" placeholder="Student Name">
                                                </div>
                                              </div>
                                            </div>

                                            <div class="col-lg-6">
                                              <div class="form-group row">
                                                <label class="col-sm-4 col-form-label">Student Email </label>
                                                <div class="col-sm-8">
                                                <input type="text" class="form-control" id="student_email" name="student_email" placeholder="Email">
                                                </div>
                                              </div>
                                            </div>

                                        </div>


                                        <div class="modal-footer">
                                           <!-- id="view_search" style="display: none;" -->
                                          <button type="button" class="btn btn-default" onclick="searchStudents()">Search</button>
                                        </div>


                                      <div class="form-container" style="display: none;" id="view_student_display">
                                          <h4 class="form-group-title"></h4>


                                          <div  id='view_student'>
                                          </div>

                                      </div>
                         


                                          
                                        <div class="button-block clearfix">
                                          <div class="bttn-group">
                                              <button type="submit" class="btn btn-primary">Save</button>
                                                <!-- <a onclick="reloadPage()" class="btn btn-link">Clear All Fields</a> -->
                                          </div>

                                        </div> 

                                    </div>                                
                                </div>
                            </form>




                    </div>









                    <div role="tabpanel" class="tab-pane" id="tab_two">
                        



                        <?php

                        if(!empty($graderStudentTagList))
                        {
                            ?>

                            <div class="form-container">
                                    <h4 class="form-group-title">Grader Student Details</h4>

                                

                                  <div class="custom-table">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                            <th>Sl. No</th>
                                             <th>Student</th>
                                             <th>Exam Center</th>
                                             <th>Exam Name</th>
                                             <th>Exam Event Date</th>
                                             <th>Tagged Date</th>
                                             <!-- <th>Action</th> -->
                                            </tr>
                                        </thead>
                                        <tbody>
                                             <?php
                                         $total = 0;
                                          for($i=0;$i<count($graderStudentTagList);$i++)
                                         { ?>
                                            <tr>
                                            <td><?php echo $i+1;?></td>
                                            <td><?php echo $graderStudentTagList[$i]->student_name . " - " . $graderStudentTagList[$i]->nric;?></td>
                                            <td><?php echo $graderStudentTagList[$i]->exam_center_name;?></td>
                                            <td><?php echo $graderStudentTagList[$i]->examname;?></td>
                                            <td><?php echo $graderStudentTagList[$i]->exam_event_date;?></td>
                                            <td><?php echo date('d-m-Y', strtotime($graderStudentTagList[$i]->created_dt_tm));?></td>
                                            <!-- <td><a onclick="deleteGraderModule(<?php echo $graderExamList[$i]->id; ?>)">Delete</a> -->
                                            </td>

                                             </tr>
                                          <?php 
                                      } 
                                      ?>
                                        </tbody>
                                    </table>
                                  </div>

                                </div>


                        <?php
                        
                        }
                        ?>



                              
                    </div>







                </div>






                


        </div>







</main>



<script>

    $('select').select2();

    $( function() {
      $( ".datepicker" ).datepicker({
          changeYear: true,
          changeMonth: true,
      });
    });




       function searchStudents()
    {
      if($('#form_main').valid())
        {
          var tempPR = {};
          tempPR['full_name'] = $("#student_name").val();
          tempPR['email_id'] = $("#student_email").val();
          tempPR['id_exam_center'] = $("#id_exam_center").val();
          tempPR['id_exam_event'] = $("#id_exam_event").val();
              $.ajax(
              {
                 url: '/exam/secondGraderStudentTag/searchStudents',
                 type: 'POST',
                 data:
                 {
                  tempData: tempPR
                 },
                 error: function()
                 {
                  alert('Something is wrong');
                 },
                 success: function(result)
                 {
                  // alert(result);
                  $("#view_student_display").show();
                  $("#view_student").html(result);
                  // $('#myModal').modal('hide');
                  // var ta = $("#total_detail").val();
                  // alert(ta);
                  // $("#amount").val(ta);
                 }
              });
        }
    }


  

  
    function getExamEventByExamCenter(id)
    {
      if(id)
      {
        $.get("/exam/graderExam/getExamEventByExamCenter/"+id, function(data, status)
        {  
            $("#view_exam_event").html(data);
        });
      }
    }


    

    $(document).ready(function()
    {
      

        $("#form_main").validate({
            rules: {
                id_exam_center: {
                    required: true
                },
                 id_exam_event: {
                    required: true
                },
                 exam_date: {
                    required: true
                }
            },
            messages: {
                id_exam_center: {
                    required: "<p class='error-text'>Select Exam Center</p>",
                },
                id_exam_event: {
                    required: "<p class='error-text'>Select Exam Event</p>",
                },
                exam_date: {
                    required: "<p class='error-text'>Select Exam Date</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>