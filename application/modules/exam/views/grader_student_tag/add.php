<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
        <h1 class="h3">Assigned students to grader</h1>
        
        <a href='/exam/graderExam/list' class="btn btn-link ml-auto">
            <i class="fa fa-chevron-left" aria-hidden="true"></i>
          Back
        </a>

    </div>







            <div class="m-auto text-center">
                <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
            </div>


            <div class="clearfix">

                <ul class="nav nav-tabs" role="tablist" >
                    
                    <li role="presentation" class="nav-item"><a href="#tab_two" class="nav-link active"
                            aria-controls="tab_two" role="tab" data-toggle="tab"> Taged Students</a>
                    </li>
                    
                                     
                </ul>


            





            
                
                <div class="tab-content offers-tab-content">





                    <div role="tabpanel" class="tab-pane active" id="tab_two">
                        
                             <div class="custom-table">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>Exam Sitting</th>
                                                <th>Exam Event Name</th>
                                                <th>Exam Date</th>
                                                <th>Grader Name</th>
                                                <th>Grading Start Date</th>
                                                <th>Grading End Date</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                       <tr>
                                        <td><?php echo $graderExam->examsittingname;?></td>
                                        <td><?php echo $graderExam->exam_event_name;?></td>
                                        <td><?php echo date('d-m-Y',strtotime($graderExam->exam_event_date));?></td>
                                        <td><?php echo $graderExam->gradername;?></td>
                                        <td><?php echo date('d-m-Y',strtotime($graderExam->start_date));?></td>
                                        <td><?php echo date('d-m-Y',strtotime($graderExam->end_date));?></td>

                                            <td><a href="/exam/graderExam/edit/<?php echo $graderExam->id; ?>">Edit</a> 
                                            </td>
                                    </tr>
                                </tbody>
                                </table>
                            </div>



                        <?php

                        if(!empty($graderStudentTagList))
                        {
                            ?>

                            <div>
                               

                                  <div class="custom-table">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                            <th>Sl. No</th>
                                             <th>Student</th>
                                             <th>NRIC</th>
                                             <th>Memership Number</th>
                                             <th>Batch</th>
                                             <th>Module</th>
                                             <th>Download Essay</th>
                                             <th>Delete</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                             <?php
                                         $total = 0;
                                          for($i=0;$i<count($graderStudentTagList);$i++)
                                         { ?>
                                            <tr>
                                            <td><?php echo $i+1;?></td>
                                            <td><?php echo $graderStudentTagList[$i]->student_name;?></td>
                                            <td><?php echo $graderStudentTagList[$i]->nric;?></td>
                                            <td><?php echo $graderStudentTagList[$i]->membership_number;?></td>
                                            <td><?php echo $graderStudentTagList[$i]->batch;?></td>
                                          
                                           <td><?php echo $graderStudentTagList[$i]->coursename;?></td>

                                           <td><a href="/exam/graderExam/viewStudentQuestionSetDownload/<?php echo $graderStudentTagList[$i]->graderexamid; ?>/<?php echo $graderStudentTagList[$i]->id_grader_exam; ?>">Download</a> 
                                            </td>


                                         

                                               <td><a href="/exam/graderExam/delete/<?php echo $graderStudentTagList[$i]->graderexamid; ?>/<?php echo $graderStudentTagList[$i]->id_grader_exam; ?>">Delete</a> 
                                            </td>



                                             </tr>
                                          <?php 
                                      } 
                                      ?>
                                        </tbody>
                                    </table>
                                  </div>

                                </div>


                        <?php
                        
                        }
                        ?>



                              
                    </div>







                </div>






                


        </div>







</main>



<script>

    $('select').select2();

    $( function() {
      $( ".datepicker" ).datepicker({
          changeYear: true,
          changeMonth: true,
      });
    });



    function searchStudents()
    {
      if($('#form_main').valid())
        {
          var tempPR = {};
          tempPR['full_name'] = $("#student_name").val();
          tempPR['email_id'] = $("#student_email").val();
          tempPR['id_exam_event'] = <?php echo $graderExam->id_exam_event ?>;
          tempPR['id_grader'] = <?php echo $grader->id ?>;
          tempPR['id_grader_exam'] = <?php echo $graderExam->id ?>;
              $.ajax(
              {
                 url: '/exam/graderStudentTag/searchStudents',
                 type: 'POST',
                 data:
                 {
                  tempData: tempPR
                 },
                 error: function()
                 {
                  alert('Something is wrong');
                 },
                 success: function(result)
                 {
                  // alert(result);
                  $("#view_student_display").show();
                  $("#view_student").html(result);
                  // $('#myModal').modal('hide');
                  // var ta = $("#total_detail").val();
                  // alert(ta);
                  // $("#amount").val(ta);
                 }
              });
        }
    }






 


    

    $(document).ready(function()
    {
      

        $("#form_main").validate({
            rules: {
                id_exam_center: {
                    required: true
                },
                 id_exam_event: {
                    required: true
                },
                 exam_date: {
                    required: true
                }
            },
            messages: {
                id_exam_center: {
                    required: "<p class='error-text'>Select Exam Center</p>",
                },
                id_exam_event: {
                    required: "<p class='error-text'>Select Exam Event</p>",
                },
                exam_date: {
                    required: "<p class='error-text'>Select Exam Date</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>