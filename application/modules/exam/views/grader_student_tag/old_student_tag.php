<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
        <h1 class="h3">Add Grader Student Exam Tagging</h1>
        
        <a href='/exam/graderExam/list' class="btn btn-link ml-auto">
            <i class="fa fa-chevron-left" aria-hidden="true"></i>
          Back
        </a>

    </div>
    



        <div class="page-container">

              <div>
                <h4 class="form-title">Grader details</h4>
              </div>

                <div class="form-container">


                    <div class="row">

                        <div class="col-lg-6">
                          <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Salutation<span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                              <select name="salutation" id="salutation" class="form-control" disabled>
                                <option value="">Select</option>
                                <?php
                                if (!empty($salutationList))
                                {
                                    foreach ($salutationList as $record)
                                    {
                                    ?>
                                        <option value="<?php echo $record->id;  ?>"
                                            <?php
                                            if($record->id == $grader->salutation)
                                            {
                                                 echo "selected";
                                            };?>
                                            >
                                            <?php echo $record->name;  ?>        
                                        </option>
                                    <?php
                                    }
                                }
                                ?>
                              </select>
                            </div>
                          </div>
                        </div>


                        <div class="col-lg-6">
                          <div class="form-group row">
                            <label class="col-sm-4 col-form-label">First Name<span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                            <input type="text" class="form-control" id="first_name" name="first_name" placeholder="First Name" value="<?php echo $grader->first_name; ?>" readonly>
                            </div>
                          </div>
                        </div>

                    </div>


                    <div class="row">

                        <div class="col-lg-6">
                          <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Last Name<span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                            <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Last Name" value="<?php echo $grader->last_name; ?>" readonly>
                            </div>
                          </div>
                        </div>


                        <div class="col-lg-6">
                          <div class="form-group row">
                            <label class="col-sm-4 col-form-label">NRIC<span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                            <input type="text" class="form-control" id="nric" name="nric" placeholder="NRIC" value="<?php echo $grader->nric; ?>">
                            </div>
                          </div>
                        </div>


                    </div>

                    <div class="row">

                        <div class="col-lg-6">
                          <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Mobile Number<span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                            <input type="text" class="form-control" id="mobile" name="mobile" placeholder="Mobile Number" value="<?php echo $grader->mobile; ?>" readonly>
                            </div>
                          </div>
                        </div>



                        <div class="col-lg-6">
                          <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Email<span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                            <input type="email" class="form-control" id="email" name="email" placeholder="Email" value="<?php echo $grader->email; ?>" readonly>
                            </div>
                          </div>
                        </div>


                    </div>

                    <div class="row">
                      

                        <div class="col-lg-6">
                          <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Password <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                            <input type="password" class="form-control" id="password" name="password" placeholder="Password" value="<?php echo $grader->password; ?>" readonly>
                            </div>
                          </div>
                        </div>


                    
                       <div class="col-lg-6">
                          <div class="form-group row align-items-center">
                            <label class="col-sm-4 col-form-label">Status <span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                              <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="customRadioInline1" name="status" class="custom-control-input" value="1"  
                                <?php if($grader->status==1)
                                {
                                     echo "checked=checked";
                                };?>
                                  >
                                <label class="custom-control-label" for="customRadioInline1">Active</label>
                              </div>
                              <div class="custom-control custom-radio custom-control-inline">
                                <input type="radio" id="customRadioInline2" name="status" class="custom-control-input" value="0"
                                <?php if($grader->status==0)
                                {
                                     echo "checked=checked";
                                };?>

                                >
                                <label class="custom-control-label" for="customRadioInline2">In-Active</label>
                              </div>
                            </div>
                          </div>
                        </div>

                    </div>       


                    

                </div>                                
            

        </div>
















        <form id="form_main" action="" method="post">

            <div class="page-container">

              <div>
                <h4 class="form-title">Exam Center details</h4>
              </div>

                <div class="form-container">


                    <div class="row">

                        <div class="col-lg-6">
                          <div class="form-group row">
                            <label class="col-sm-4 col-form-label"> Exam Center<span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                              <select name="id_exam_center" id="id_exam_center" class="form-control" onchange="getExamEventByExamCenterNIdGrader(this.value)">
                                <option value="">Select</option>
                                <?php
                                if (!empty($examCenterList))
                                {
                                    foreach ($examCenterList as $record)
                                    {
                                    ?>
                                        <option value="<?php echo $record->id;  ?>">
                                            <?php echo $record->name;  ?>        
                                        </option>
                                    <?php
                                    }
                                }
                                ?>
                              </select>
                            </div>
                          </div>
                        </div>



                        <div class="col-lg-6">
                            <div class="form-group row">
                              <label class="col-sm-4 col-form-label">Exam Event <span class="text-danger">*</span></label>
                              <div class="col-sm-8">
                                <span id="view_exam_event">
                                    <select class="form-control" id='id_exam_event' name='id_exam_event'>
                                      <option value=''></option>
                                    </select>
                                </span>
                              </div>
                            </div>
                        </div>



                        <!-- <div class="col-lg-6">
                          <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Exam Time<span class="text-danger">*</span></label>
                            <div class="col-sm-8">
                            <input type="time" class="form-control" id="exam_time" name="exam_time" placeholder="Exam Time">
                            </div>
                          </div>
                        </div> -->

                    </div>






                    <div class="row">


                        <div class="col-lg-6">
                          <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Student Name </label>
                            <div class="col-sm-8">
                            <input type="text" class="form-control" id="student_name" name="student_name" placeholder="Student Name">
                            </div>
                          </div>
                        </div>

                        <div class="col-lg-6">
                          <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Student Email </label>
                            <div class="col-sm-8">
                            <input type="text" class="form-control" id="student_email" name="student_email" placeholder="Email">
                            </div>
                          </div>
                        </div>

                    </div>


                    <div class="modal-footer">
                       <!-- id="view_search" style="display: none;" -->
                      <button type="button" class="btn btn-default" onclick="searchStudents()">Search</button>
                    </div>


                  <div class="form-container" style="display: none;" id="view_student_display">
                      <h4 class="form-group-title"></h4>


                      <div  id='view_student'>
                      </div>

                  </div>
     


                      
                    <div class="button-block clearfix">
                      <div class="bttn-group">
                          <button type="submit" class="btn btn-primary">Save</button>
                            <!-- <a onclick="reloadPage()" class="btn btn-link">Clear All Fields</a> -->
                          <a href="<?php echo '../add/'. $grader->id; ?>" class="btn btn-link">Clear All Fields</a>
                      </div>

                    </div> 

                </div>                                
            </div>
        </form>




        <?php

        if(!empty($graderStudentTagList))
        {
            ?>

            <div class="form-container">
                    <h4 class="form-group-title">Grader Student Details</h4>

                

                  <div class="custom-table">
                    <table class="table">
                        <thead>
                            <tr>
                            <th>Sl. No</th>
                             <th>Student</th>
                             <th>Exam Center</th>
                             <th>Exam Event Name</th>
                             <th>Exam Event Date</th>
                             <th>Tagged Date</th>
                             <!-- <th>Action</th> -->
                            </tr>
                        </thead>
                        <tbody>
                             <?php
                         $total = 0;
                          for($i=0;$i<count($graderStudentTagList);$i++)
                         { ?>
                            <tr>
                            <td><?php echo $i+1;?></td>
                            <td><?php echo $graderStudentTagList[$i]->student_name . " - " . $graderStudentTagList[$i]->nric;?></td>
                            <td><?php echo $graderStudentTagList[$i]->exam_center_name;?></td>
                            <td><?php echo $graderStudentTagList[$i]->exam_event_name;?></td>
                            <td><?php echo $graderStudentTagList[$i]->exam_event_date;?></td>
                            <td><?php echo date('d-m-Y', strtotime($graderStudentTagList[$i]->created_dt_tm));?></td>
                            <!-- <td><a onclick="deleteGraderModule(<?php echo $graderExamList[$i]->id; ?>)">Delete</a> -->
                            </td>

                             </tr>
                          <?php 
                      } 
                      ?>
                        </tbody>
                    </table>
                  </div>

                </div>


        <?php
        
        }
        ?>










</main>

<script>

    $('select').select2();

    $( function() {
      $( ".datepicker" ).datepicker({
          changeYear: true,
          changeMonth: true,
      });
    });



    function searchStudents()
    {
      if($('#form_main').valid())
        {
          var tempPR = {};
          tempPR['full_name'] = $("#student_name").val();
          tempPR['email_id'] = $("#student_email").val();
          tempPR['id_exam_center'] = $("#id_exam_center").val();
          tempPR['id_exam_event'] = $("#id_exam_event").val();
          tempPR['id_grader'] = <?php echo $grader->id ?>;
              $.ajax(
              {
                 url: '/exam/graderStudentTag/searchStudents',
                 type: 'POST',
                 data:
                 {
                  tempData: tempPR
                 },
                 error: function()
                 {
                  alert('Something is wrong');
                 },
                 success: function(result)
                 {
                  // alert(result);
                  $("#view_student_display").show();
                  $("#view_student").html(result);
                  // $('#myModal').modal('hide');
                  // var ta = $("#total_detail").val();
                  // alert(ta);
                  // $("#amount").val(ta);
                 }
              });
        }
    }






    function getExamEventByExamCenterNIdGrader(id)
    {
        var tempPR = {};
        tempPR['id_exam_center'] = $("#id_exam_center").val();
        tempPR['id_grader'] = <?php echo $grader->id ?>;

        var id_exam_event = $("#id_exam_event").val();


        if(id != '')
        {
          $.ajax(
          {
             url: '/exam/graderStudentTag/getExamEventByExamCenterNIdGrader',
             type: 'POST',
             data:
             {
              tempData: tempPR
             },
             error: function()
             {
              alert('Something is wrong');
             },
             success: function(result)
             {
              $("#view_exam_event").html(result);
              // alert(result);
              // $('#myModal').modal('hide');
              // var ta = $("#total_detail").val();
              // alert(ta);
              // $("#amount").val(ta);
             }
          });

          // $.get("/exam/graderStudentTag/getExamEventByExamCenter/"+id, function(data, status)
          // {  
          //     $("#view_exam_event").html(data);
          // });
        }
    }



    

    $(document).ready(function() {
        $("#form_main").validate({
            rules: {
                id_exam_center: {
                    required: true
                },
                 id_exam_event: {
                    required: true
                },
                 exam_date: {
                    required: true
                }
            },
            messages: {
                id_exam_center: {
                    required: "<p class='error-text'>Select Exam Center</p>",
                },
                id_exam_event: {
                    required: "<p class='error-text'>Select Exam Event</p>",
                },
                exam_date: {
                    required: "<p class='error-text'>Select Exam Date</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

    function getStateByCountry(id)
    {

        $.get("/exam/examCenter/getStateByCountry/"+id, function(data, status){
       
            $("#view_state").html(data);
            // $("#view_programme_details").html(data);
            // $("#view_programme_details").show();
        });
    }
</script>