<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
  <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
    <h1 class="h3">Exam Event List</h1>
    <a href="add" class="btn btn-primary ml-auto">+ Add Exam Event</a>
  </div>
  <div class="page-container">
    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="">
              Advanced Search
              <i class="fa fa-angle-right" aria-hidden="true"></i>
              <i class="fa fa-angle-down" aria-hidden="true"></i>
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">

          <div id="collapseOne" class="panel-collapse collapse show" role="tabpanel" aria-labelledby="headingOne" aria-expanded="true" style="">
            <div class="panel-body p-2">

              <div class="row">

                <div class="col-lg-6">
                  <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Exam Event</label>
                    <div class="col-sm-9">
                      <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="<?php echo $searchParam['name'] ?>">
                    </div>
                  </div>
                </div>

                <div class="col-lg-6">
                  <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Location</label>
                    <div class="col-sm-9">
                      <select name="role" id="role" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($locationList)) {
                          foreach ($locationList as $record) {
                            $selected = '';
                            if ($record->id == $searchParam['id_location']) {
                              $selected = 'selected';
                            }
                        ?>
                            <option value="<?php echo $record->id;  ?>" <?php echo $selected;  ?>>
                              <?php echo  $record->name;  ?>
                            </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>

              </div>

              <div class="row">

                <div class="col-lg-6">
                  <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Exam Center</label>
                    <div class="col-sm-9">
                      <select name="id_exam_center" id="id_exam_center" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($examCenterList)) {
                          foreach ($examCenterList as $record) {
                            $selected = '';
                            if ($record->id == $searchParam['id_exam_center']) {
                              $selected = 'selected';
                            }
                        ?>
                            <option value="<?php echo $record->id;  ?>" <?php echo $selected;  ?>>
                              <?php echo  $record->name;  ?>
                            </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Exam Type</label>
                    <div class="col-sm-9">
                      <select name="type" id="type" class="form-control">
                        <option value="">Select</option>
                        <option value="Past" <?php if($searchParam['type']=='Past') {
                            echo "selected=selected";
                                
                            } ?> >Past</option>
                        <option value="Upcoming" <?php if($searchParam['type']=='Upcoming') {
                            echo "selected=selected";
                                
                            } ?>>Upcoming</option>
                       
                      </select>
                    </div>
                  </div>
                </div>

              </div>


              <hr />
              <div class="d-flex justify-content-center">
                <button type="submit" class="btn btn-primary">Search</button>
                <button type="reset" class="btn btn-link" onclick="clearSearchForm()">Clear All Fields</button>
              </div>
            </div>
          </div>

        </form>

      </div>
    </div>
    <?php
    if ($this->session->flashdata('success')) {
    ?>
      <div class="alert alert-success alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
      </div>
    <?php
    }
    if ($this->session->flashdata('error')) {
    ?>
      <div class="alert alert-danger alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Failure!</strong> <?php echo $this->session->flashdata('error'); ?>
      </div>
    <?php
    }
    ?>
    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Sl. No</th>
            <th>Exam Sitting</th>
            <th>Exam Event Name</th>
            <th>Exam Center</th>
            <th>Event Start Date</th>
            <th>Event Start Time</th>

            <th>Event End Date</th>
            <th>Event End Time</th>
            <th>Total Students</th>
            <!-- <th>Name In Other Language</th> -->
            <th class="text-center">Exam Set</th>
            <th style="text-align: center;">Action</th>
                        <th style="text-align: center;">View Students</th>
                        <th style="text-align: center;">Generate Token</th>

          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($examEventList)) {
            $i = 1;
                    $this->load->model('exam_event_model');

            foreach ($examEventList as $record) {

              $totalStuents = 0;
              $totalStudetnsObject = $this->exam_event_model->examStudentTaggingListSearch($record->id);

              $totalStuents = count($totalStudetnsObject);

                $programmeName = '';
              if($record->mqms_type=='CPIF') {
                 $programmeName = $record->programme_level_ciif."<br/>".$record->programme_name_ciif."<br/>".$record->name;
              } else  {
                 $programmeName = $record->name;
              }


          ?>
            
              <tr>
                <td><?php echo $i ?></td>
                <td><?php echo $record->exam_sitting_name ?> - <?php echo date('Y', strtotime($record->exam_date)) ?></td>
                
                <td><?php echo $programmeName;?>   </td>

                <td><?php echo $record->examcenternae ?></td>
                <td><?php echo date('d-m-Y', strtotime($record->exam_date)) ?></td>
                <td><?php echo date("H:i", strtotime($record->from_tm)) ?></td>

                  <td><?php echo date('d-m-Y', strtotime($record->exam_end_date)) ?></td>
                <td><?php echo date("H:i", strtotime($record->to_tm)) ?></td>


                <td>
                  <?php echo $totalStuents;?>
                </td>
                    <td>
                  <?php if($record->id_exam_set) { ?> 

                    <p style='color:green;'>Defined</p>
                   <?php } else { ?>
                     <p><a href='/question/examset/list'  style='color:red;'>Create Exam Set</a></p> / 

                    <p><a href='/exam/examEvent/edit/<?php echo $record->id ?>'  style='color:red;'>Tag Exam set to Event</a></p>

                   <?php } ?> 
                </td>
               
                </td>
                <td class="text-center">

                  <a href="<?php echo 'edit/' . $record->id; ?>" class="btn" type="" data-toggle="tooltip" data-placement="top" title="Edit">
                    <i class="fa fa-pencil-square-o" aria-hidden="true">
                    </i>
                  </a>

                </td>

                 <td class="text-center">

                  <a href="<?php echo 'students/' . $record->id; ?>" class="btn" type="" data-toggle="tooltip" data-placement="top" title="Edit">
                    View Students
                  </a>

                </td>

                  <td class="text-center">

                  <a href="<?php echo 'generateToken/' . $record->id; ?>" class="btn" type="" data-toggle="tooltip" data-placement="top" title="Edit">
                    Generate Token
                  </a>

                </td>

              </tr>
          <?php
              $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
</main>
<script>
  $('select').select2();

  function clearSearchForm() {
    window.location.reload();
  }
</script>