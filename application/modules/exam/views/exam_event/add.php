<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
        <h1 class="h3">Edit Exam Event</h1>
        
        <a href='list' class="btn btn-link ml-auto">
            <i class="fa fa-chevron-left" aria-hidden="true"></i>
          Back
        </a>

    </div>
    
    <form id="form_main" action="" method="post">

        <div class="page-container">

          <div>
            <h4 class="form-title">Exam Event details</h4>
          </div>

            <div class="form-container">


                <div class="row">

<div class="col-lg-6">
               <div class="form-group row align-items-center">
                  <label class="col-sm-4 col-form-label">Type of Exam <span class="text-danger">*</span></label>
                  <div class="col-sm-8">
                     <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="customRadioInline112" name="type_of_exam" class="custom-control-input" value="1" onclick="typeofExam(1)" checked="">
                        <label class="custom-control-label" for="customRadioInline112">Module Based</label>
                     </div>
                     <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="customRadioInline223" name="type_of_exam" class="custom-control-input" value="2" onclick="typeofExam(2)">
                        <label class="custom-control-label" for="customRadioInline223">Programme Based</label>
                     </div>
                  </div>
               </div>
            </div>
             <div class="col-lg-6">
               <div class="form-group row">
                  <label class="col-sm-4 col-form-label"><span id="labeldiv"></span><span class="text-danger">*</span></label>
                  <div class="col-sm-8">
                     <div id='idcourseDiv'></div>
                  </div>
               </div>
            </div>

               <div class="col-lg-6">
               <div class="form-group row">
                  <label class="col-sm-4 col-form-label">Select Exam Set<span class="text-danger">*</span></label>
                  <div class="col-sm-8">
                     <div id='idexamSetDiv'></div>
                  </div>
               </div>
            </div>



                   <div class="col-lg-6">
                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label">Exam Sitting <span class="text-danger">*</span></label>
                          <div class="col-sm-8">
                            <select name="id_exam_sitting" id="id_exam_sitting" class="form-control" >
                            <option value="">Select</option>
                            <?php
                            if (!empty($examSitting))
                            {
                                foreach ($examSitting as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->exam_sitting_name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                          </div>
                        </div>
                    </div>




                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Event Name <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control" id="name" name="name" placeholder="Name">
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Event Start Date <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control datepicker" id="exam_date" name="exam_date" autocomplete="off">
                        </div>
                      </div>
                    </div>

                       <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Event End Date <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="text" class="form-control datepicker" id="exam_end_date" name="exam_end_date" autocomplete="off">
                        </div>
                      </div>
                    </div>

             

                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Event Start Time <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="time" class="form-control" id="from_tm" name="from_tm"  autocomplete="off">
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Event End Time <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="time" class="form-control" id="to_tm" name="to_tm"  autocomplete="off">
                        </div>
                      </div>
                    </div>

            

                    <div class="col-lg-6">
                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label">Location <span class="text-danger">*</span></label>
                          <div class="col-sm-8">
                            <select name="id_location" id="id_location" class="form-control" >
                            <?php
                            if (!empty($locationList))
                            {
                                foreach ($locationList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                          </div>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="form-group row">
                          <label class="col-sm-4 col-form-label">Exam Center <span class="text-danger">*</span></label>
                          <div class="col-sm-8">
                            <select name="id_exam_center" id="id_exam_center" class="form-control">
                            <?php
                            if (!empty($examCenterList))
                            {
                                foreach ($examCenterList as $record)
                                {?>
                             <option value="<?php echo $record->id;  ?>">
                                <?php echo $record->name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                          </select>
                          </div>
                        </div>
                    </div>

               

                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Quota No. <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="number" class="form-control" id="max_count" name="max_count">
                        </div>
                      </div>
                    </div>


                  <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Type <span class="text-danger">*</span></label>
                        <div class="col-sm-8">

                         <select name="type" id="type" class="form-control">
                          <option value="1">Face to Face </option>
                          <option value="2">Supervised Online Exam </option>
                          <option value="3" selected="selected">Online Exam </option>
                         </select>
                        </div>
                      </div>
                    </div>



        

                    <div class="col-lg-6">
                      <div class="form-group row align-items-center">
                        <label class="col-sm-4 col-form-label">Status <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                          <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="customRadioInline1" name="status" class="custom-control-input" value="1" checked="checked" 
                              >
                            <label class="custom-control-label" for="customRadioInline1">Active</label>
                          </div>
                          <div class="custom-control custom-radio custom-control-inline">
                            <input type="radio" id="customRadioInline2" name="status" class="custom-control-input" value="0">
                            <label class="custom-control-label" for="customRadioInline2">In-Active</label>
                          </div>
                        </div>
                      </div>
                    </div>

                </div>       


                  
                <div class="button-block clearfix">
                  <div class="bttn-group">
                      <button type="submit" class="btn btn-primary">Save</button>
                      <a href='add' class="btn btn-link">Clear All Fields</a>
                  </div>

                </div> 

            </div>                                
        </div>
    </form>
</main>

<script>

    $(document).ready(function() {
        $("#form_main").validate({
            rules: {
                name: {
                    required: true
                },
                 from_dt: {
                    required: true
                },
                 from_tm: {
                    required: true
                },
                 id_exam_center: {
                    required: true
                },
                 id_location: {
                    required: true
                },
                 max_count: {
                    required: true
                },
                 status: {
                    required: true
                },
                type : {
                    required: true
                },
                to_tm : {
                    required: true
                },
                id_exam_set : {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "<p class='error-text'>Name required</p>",
                },
                from_dt: {
                    required: "<p class='error-text'>Select Event Date</p>",
                },
                from_tm: {
                    required: "<p class='error-text'>Select Event Start Time</p>",
                },
                id_exam_center: {
                    required: "<p class='error-text'>Select Exam Center</p>",
                },
                id_location: {
                    required: "<p class='error-text'>Select Location</p>",
                },
                max_count: {
                    required: "<p class='error-text'>Quota No. Required</p>",
                },
                status: {
                    required: "<p class='error-text'>Select Status</p>",
                },
                type: {
                    required: "<p class='error-text'>Select Event Type</p>",
                },
                to_tm: {
                    required: "<p class='error-text'>Select Event End Time</p>",
                },
                id_exam_set: {
                    required: "<p class='error-text'>Select Exam Set</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
<script type="text/javascript">


    $( function() {
          typeofExam(1);

      



    } );


    $(document).ready(function () {

        $("#exam_date").datepicker({
            dateFormat: "dd-mm-yy",
            onSelect: function () {
                var dt2 = $('#exam_end_date');
                var startDate = $(this).datepicker('getDate');
                var minDate = $(this).datepicker('getDate');
                var dt2Date = dt2.datepicker('getDate');
                //difference in days. 86400 seconds in day, 1000 ms in second
                var dateDiff = (dt2Date - minDate)/(86400 * 1000);
                
                startDate.setDate(startDate.getDate() +300);
               
                //sets dt2 maxDate to the last day of 30 days window
                dt2.datepicker('option', 'maxDate', startDate);
                dt2.datepicker('option', 'minDate', minDate);
            }
        });
        $('#exam_end_date').datepicker({
            dateFormat: "dd-mm-yy",
        });
    });
    

    $('select').select2();

    function reloadPage()
    {
      window.location.reload();
    }


       function typeofExam(type) {

      if(type=='1') {
        $("#labeldiv").html("Select Module");
      } else {
        $("#labeldiv").html("Select Programme");

      }

       $.ajax({
        url: "/exam/examEvent/getmoduledropdown",
        method: "POST",
        data: {
          id: type
        },
        success: function(data) {
          $("#idcourseDiv").html(data);
        }
      });
    }

    function getexamset() {
      var idcourse = $("#id_course").val();
      $.ajax({
        url: "/exam/examEvent/getexamsetlist",
        method: "POST",
        data: {
          id: idcourse
        },
        success: function(data) {
          $("#idexamSetDiv").html(data);
        }
      });
    }
</script>