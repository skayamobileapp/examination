<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
   <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
      <h1 class="h3">Grader Marks Approval</h1>
      <!-- <a href='../list' class="btn btn-link ml-auto">
         <i class="fa fa-chevron-left" aria-hidden="true"></i>
         Back
         </a> -->
   </div>
   <form id="form_main" action="" method="post">
      <div class="page-container">
      <div>
         <h4 class="form-title">Grader Student Marks Approval details</h4>
      </div>
      <div class="form-container">
         <div class="row">
            <div class="col-lg-6">
               <div class="form-group row">
                  <label class="col-sm-4 col-form-label"> Grader<span class="text-danger">*</span></label>
                  <div class="col-sm-8">
                     <select name="id_grader" id="id_grader" class="form-control" onchange="getExamCenterByGrader(this.value)">
                        <option value="">Select</option>
                        <?php
                           if (!empty($graderList))
                           {
                               foreach ($graderList as $record)
                               {
                               ?>
                        <option value="<?php echo $record->id;  ?>">
                           <?php echo $record->nric . " - " . $record->full_name;  ?>        
                        </option>
                        <?php
                           }
                           }
                           ?>
                     </select>
                  </div>
               </div>
            </div>
            <button type="button" class="btn btn-primary" onclick="searchStudents()">Search</button>
         </div>
         <div style="display: none;" id="view_student_display">
            <h4 class="form-group-title"></h4>
            <div  id='view_student'>
            </div>
         </div>
         <div class="row" id='marksgradbtn' style="display: none;">
            <div class="col-sm-6">
               <label class="col-sm-4 col-form-label"> Marks 
               Status<span class="text-danger">*</span></label>
               <select name="type" id="type" class="form-control" onchange="showgrader(this.value)" style='width:250px;'>
                  <option value="">Select</option>
                  <option value="15">Approve</option>
                  <option value="13">2nd Grader</option>
                  <option value="11">Mark Adjustment</option>
               </select>
            </div>
         </div>
         <div id='divgraderdisplay' style="display: none;">
            <hr/>
            <div class="row">
               <div class="col-sm-12">
               <h4 class='form-group-title'>Assign 2nd Grader Details</h4>
             </div>
            </div>
            <div class="row clearfix">
               <div class="col-sm-4" >
                  <div class="form-group row">
                     <label class="col-sm-4 col-form-label"> Grader<span class="text-danger">*</span></label>
                     <div class="col-sm-8">
                        <select name="id_grader_two" id="id_grader_two" class="form-control" onchange="getExamCenterByGrader(this.value)" style="width:200px;">
                           <option value="">Select</option>
                           <?php
                              if (!empty($graderList))
                              {
                                  foreach ($graderList as $record)
                                  {
                                  ?>
                           <option value="<?php echo $record->id;  ?>">
                              <?php echo $record->nric . " - " . $record->full_name;  ?>        
                           </option>
                           <?php
                              }
                              }
                              ?>
                        </select>
                     </div>
                  </div>
               </div>
               <div class="col-sm-4">
                  <label class="col-sm-4 col-form-label"> Start Date<span class="text-danger">*</span></label>
                  <input type='text' name='start_date' id='start_date' value=''>
               </div>
               <div class="col-sm-4">
                  <label class="col-sm-4 col-form-label"> End Date<span class="text-danger">*</span></label>
                  <input type='text' name='end_date' id='end_date' value=''>
               </div>
            </div>
        </div>
        <div class="button-block clearfix"  id='savebtn' style="display: none;">
           <div class="bttn-group">
              <button type="submit" class="btn btn-primary">Save</button>
              <a href="<?php echo '/exam/graderMarksApproval/add' ?>" class="btn btn-link">Clear All Fields</a>
           </div>
        </div>
         </div>
      </div>
   </form>
</main>
<script>
   function showgrader(val) {
       $("#divgraderdisplay").hide();
   
     if(val=='13'){
       $("#divgraderdisplay").show();
     }
   }
   
   $('select').select2();
   
   $( function() {
     $( ".datepicker" ).datepicker({
         changeYear: true,
         changeMonth: true,
     });
   });
   
   
   
   function searchStudents()
   {
     if($('#form_main').valid())
       {
         var tempPR = {};
         tempPR['full_name'] = $("#student_name").val();
         tempPR['email_id'] = $("#student_email").val();
         tempPR['id_exam_center'] = $("#id_exam_center").val();
         tempPR['id_exam_event'] = $("#id_exam_event").val();
         tempPR['id_grader'] = $("#id_grader").val();
             $.ajax(
             {
                url: '/exam/graderMarksApproval/searchStudents',
                type: 'POST',
                data:
                {
                 tempData: tempPR
                },
                error: function()
                {
                 alert('Something is wrong');
                },
                success: function(result)
                {
                 // alert(result);
                 $("#view_student_display").show();
                 $("#view_student").html(result);
                 console.log(result);
                 $("#marksgradbtn").show();
                 $("#savebtn").show();
                 // $('#myModal').modal('hide');
                 // var ta = $("#total_detail").val();
                 // alert(ta);
                 // $("#amount").val(ta);
                }
             });
       }
   }
   
   
   function getExamCenterByGrader(id)
   {
     if(id)
     {
       
       $.get("/exam/graderMarksApproval/getExamCenterByGrader/"+id, function(data, status)
       {  
           $("#view_exam_center").html(data);
       });
     }
   }
   
   
   function getExamEventByExamCenterNIdGrader(id)
   {
   
         var tempPR = {};
       tempPR['id_grader'] = $("#id_grader").val();
       tempPR['id_exam_center'] = $("#id_exam_center").val();
   
       if(id != '')
       {
         $.ajax(
         {
            url: '/exam/graderStudentTag/getExamEventByExamCenterNIdGrader',
            type: 'POST',
            data:
            {
             tempData: tempPR
            },
            error: function()
            {
             alert('Something is wrong');
            },
            success: function(result)
            {
             $("#view_exam_event").html(result);
             // alert(result);
             // $('#myModal').modal('hide');
             // var ta = $("#total_detail").val();
             // alert(ta);
             // $("#amount").val(ta);
            }
         });
       }        
   }
   
   
   
   
   
   $(document).ready(function() {
       $("#form_main").validate({
           rules: {
               id_exam_center: {
                   required: true
               },
                id_exam_event: {
                   required: true
               },
                id_grader: {
                   required: true
               }
           },
           messages: {
               id_exam_center: {
                   required: "<p class='error-text'>Select Exam Center</p>",
               },
               id_exam_event: {
                   required: "<p class='error-text'>Select Exam Event</p>",
               },
               id_grader: {
                   required: "<p class='error-text'>Select Grader</p>",
               }
           },
           errorElement: "span",
           errorPlacement: function(error, element) {
               error.appendTo(element.parent());
           }
   
       });
   });
</script>