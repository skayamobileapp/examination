<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
        <h1 class="h3">Exam / Email Remainder </h1>
        
       

    </div>
    
    <form id="form_main" action="" method="post">

        <div class="page-container">

        
            <div class="form-container">


                <h5 style="color:#00bcd2;">Student Exam Registration Slip Reminder</h5>
                <div class="row">

                    <div class="col-lg-12">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">1st reminder  day before exam date <span class="text-danger">*</span></label>                        <div class="col-sm-4">
                        <input type="text" class="form-control" id="name" name="name" placeholder="Days" value="<?php echo  $examSetList->name;?>">
                        </div>
                      </div>
                    </div>

                </div>   
                
                   <div class="row">

                    <div class="col-lg-12">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">2nd reminder  day before exam date <span class="text-danger">*</span></label>                        <div class="col-sm-4">
                        <input type="text" class="form-control" id="name_second" name="name_second" placeholder="Days" value="<?php echo  $examSetList->name_second;?>">
                        </div>
                      </div>
                    </div>

                </div>   
                <hr/>
                                <h5 style="color:#00bcd2;">Grader Grading Reminder</h5>



                 <div class="row">

                    <div class="col-lg-12">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Email to Grader No of days before marking deadline<span class="text-danger">*</span></label>                        <div class="col-sm-4">
                        <input type="text" class="form-control" id="name_third" name="name_third" placeholder="Days" value="<?php echo  $examSetList->name_third;?>">
                        </div>
                      </div>
                    </div>

                </div>   

                 <div class="row">

                    <div class="col-lg-12">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Email to exam system admin No of days before marking deadline <span class="text-danger">*</span></label>                        <div class="col-sm-4">
                        <input type="text" class="form-control" id="name_fourth" name="name_fourth" placeholder="Days" value="<?php echo  $examSetList->name_fourth;?>">
                        </div>
                      </div>
                    </div>

                </div>   


                  
                <div class="button-block clearfix">
                  <div class="bttn-group">
                      <button type="submit" class="btn btn-primary">Save</button>
                      <a href='add' class="btn btn-link">Clear All Fields</a>
                  </div>

                </div> 

            </div>                                
        </div>
    </form>
</main>
