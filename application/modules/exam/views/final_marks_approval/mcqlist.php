<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
        <h1 class="h3">Student Question Details</h1>

         <a href="<?php echo '/exam/finalMarksApproval/approve/' . $backid; ?>" class="btn btn-link ml-auto">
            <i class="fa fa-chevron-left" aria-hidden="true"></i>
          Back
        </a>

        
    </div>





            <div class="m-auto text-center">
                <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
            </div>
            <div class="row">
              <div class="col-sm-12">
                 <div class="custom-table">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                             <th>Student Name</th>
                             <th>Email</th>
                             <th>Exam Name</th>
                             <th>Exam Date</th>
                            </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td><?php echo $studentDetails->full_name;?></td>
                            <td><?php echo $studentDetails->email;?></td>
                            <td><?php echo $studentDetails->name;?></td>
                            <td><?php echo date('d-m-Y',strtotime($studentDetails->exam_date));?></td>

                          </tr>
                        </tbody>
                      </table>
                    </div>
              </div>
            </div>



            <div class="clearfix">

               



            
                
                <div class="tab-content offers-tab-content">



                    <div role="tabpanel" class="tab-pane active" id="tab_one">
                        
                        <div class="col-12">



                          <?php

                                if(!empty($examQuestionsByData))
                                {
                                    ?>

                                    <div class="">
                                                                                   

                                          <div class="custom-table">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                    <th valign="top">Sl. No</th>
                                                    <th>Question</th>
                                                    <th>Submitted Answer</th>
                                                    <th>Answered Date & Time</th>
                                                    <th>Correct Answer</th>
                                                    <th>Result Status</th>
                                                    <th>Photo</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                <?php
                                                  for($i=0;$i<count($examQuestionsByData);$i++)
                                                 { ?>
                                                    <tr>
                                                    <td valign="top"><?php echo $i+1;?></td>
                                                    <td><?php echo $examQuestionsByData[$i]->question; ?></td>
                                                    <td><?php if($examQuestionsByData[$i]->id_answer == 0)
                                                    {
                                                      echo ' No Response Added';
                                                    }else
                                                    {
                                                      echo $examQuestionsByData[$i]->answer;
                                                    }
                                                     ?></td>
                                                    <td><?php if($examQuestionsByData[$i]->datetime){
                                                      echo date('d-m-Y h:i:s A', strtotime($examQuestionsByData[$i]->datetime)); } ?></td>
                                                    <td><?php echo $examQuestionsByData[$i]->correct_answer; ?></td>
                                                   <?php 
                                                    if($examQuestionsByData[$i]->id_answer == 0)
                                                    { ?> 
                                                       <td>No Response Added</td>
                                                    <?php }
                                                    elseif($examQuestionsByData[$i]->is_correct_answer ==1)
                                                    {  ?>
                                                      <td style="color:green;">Correct</td>
                                                    <?php }else
                                                    {  ?>
                                                      <td style="color:red;">Wrong</td>
                                                   <?php  }
                                                      ?></td>
                                                       <td><img src="/<?php echo  $examQuestionsByData[$i]->student_image;?>" style="height:75px;" /></td>


                                                    <!-- <td style="text-align: center;">
                                                      <a href="<?php echo '../viewQuestoins/' . $id_exam_tagging . '/' . $examQuestionsByData[$i]->id; ?>" class="btn" type="" data-toggle="tooltip" data-placement="top" title="View Attempts">
                                                        <i class="fa fa-eye" aria-hidden="true">
                                                        </i>
                                                      </a>
                                                    </td> -->

                                                    <!-- <td style="text-align: center;">
                                                      <a onclick="viewAttemptQuestioDetails(<?php echo $examQuestionsByData[$i]->exam_result; ?>" class="btn" type="" data-toggle="tooltip" data-placement="top" title="View Questions">
                                                        <i class="fa fa-eye" aria-hidden="true">
                                                        </i>
                                                      </a>
                                                    </td> -->
                                                    </tr>
                                                  <?php
                                              } 
                                              ?>
                                                </tbody>
                                            </table>
                                          </div>

                                        </div>

                                <?php
                                
                                }
                                else
                                {
                                    ?>

                                    <h4>No Attempt Questions Found</h4>
                                    <?php

                                }
                                 ?>



                        </div> 
                    
                    </div>




                </div>






                


        </div>


    </form>





     <!--  <div class="chat-bot-container" style="display: ;">
          <div class="cb-header">
              Hi, <strong><?php print_r($_SESSION['applicant_name']);?></strong> <br />We are happy to help you,
          </div>
          <ul class="cb-help">
              <li>Type '<strong>1</strong>' : For Application Status</li>
              <li>Type '<strong>2</strong>' : For Fee Amount</li>
              <li>Type '<strong>3</strong>' : For Offer Letter</li>
              <li>Type '<strong>4</strong>' : To know the start date of the semester</li>
              <li>Type '<strong>9</strong>' : To get a call from the representative</li>
          </ul>
         
          <div id='attach'></div>
          <div class="form-group cb-form">
              <input type="text" class="form-control" placeholder="Type here..." name='message' id='message' value=''>
              <button class="btn btn-primary" type="button" onclick='validateData()'>Start</button>
            </div>                                   
      </div> -->




        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer> 
      






</main>


<script>

    $('select').select2();


    $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
    });
    });
</script>