<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
        <h1 class="h3">Student Question Set</h1>
        
       <a href="<?php echo '/exam/finalMarksApproval/approve/' . $backid; ?>" class="btn btn-link ml-auto">
            <i class="fa fa-chevron-left" aria-hidden="true"></i>
          Back
        </a>


    </div>
    
            <div class="m-auto text-center">
                <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
            </div>
            <div class="row">
              <div class="col-sm-12">
                 <div class="custom-table">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                             <th>Student Name</th>
                             <th>Email</th>
                             <th>Exam Name</th>
                             <th>Exam Date</th>
                            </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td><?php echo $studentDetails->full_name;?></td>
                            <td><?php echo $studentDetails->email;?></td>
                            <td><?php echo $studentDetails->name;?></td>
                            <td><?php echo date('d-m-Y',strtotime($studentDetails->exam_date));?></td>

                          </tr>
                        </tbody>
                      </table>
                    </div>
              </div>
            </div>



        <div class="page-container">

                         <?php $q=1;  for($i=0;$i<count($showStudentQuestionSetForGrader);$i++)
                                 {  
                                    ?>
    <div class="row">
              <div class="col-sm-12">
                 <div class="custom-table form-container">
                     <div style="color: #4276ef">
                         <?php echo $showStudentQuestionSetForGrader[$i]->question_number;?><?php echo $showStudentQuestionSetForGrader[$i]->question;?>
                     </div>
                    <table class="table table-bordered" style="width: 100%;">

                        <thead>
                             <tr>
                             <th style="width:45%">Answer Scheme</th>
                             <th style="width:45%">Answer</th>
                             <th style="width:5%">Total Marks</th>
                             <th style="width:5%">Marks Obtained</th>
                            </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td> <?php echo $showStudentQuestionSetForGrader[$i]->answer_scheme;?></td>
                          
                             <?php if($showStudentQuestionSetForGrader[$i]->answer_text=='') { ?>
                                <td style="color: red;"> Candidate did not answer
                                </td>

                             <?php } else { ?>
                             <td style="color: #229116"> 
                                <?php echo $showStudentQuestionSetForGrader[$i]->answer_text;?></td>
                            <?php } ?> 

                              <td> <?php echo $showStudentQuestionSetForGrader[$i]->questionmarks;?></td>

                               <td> <?php echo $showStudentQuestionSetForGrader[$i]->marks;?> </td>

                          </tr>

                   
                        </tbody>
                      </table>
                    </div>
              </div>
            </div>
   <?php  $q++;} ?> 
                          
             <div class="row">
              <div class="col-sm-12">
                Overall Comments
                 <div class="form-group">
                    <textarea class="form-control" name='comments' id='comments' readonly><?php echo $examStudentTaggingDetails->grader_comments;?></textarea>
                 </div>
                </div>
                 <div class="col-sm-12" id='savebuttonid'>
                    <div class="button-block clearfix">
                   

                  </div>    
                </div>
            </div>

        </div>

</form>

</main>

