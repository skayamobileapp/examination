<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
  <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
    <h1 class="h3">Marks Approval</h1>
  </div>
  <div class="page-container">
        <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="">
              Advanced Search
              <i class="fa fa-angle-right" aria-hidden="true"></i>
              <i class="fa fa-angle-down" aria-hidden="true"></i>
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">

          <div id="collapseOne" class="panel-collapse collapse show" role="tabpanel" aria-labelledby="headingOne" aria-expanded="true" style="">
            <div class="panel-body p-2">

              <div class="row">
                <div class="col-lg-6">
                  <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Exam Sitting</label>
                    <div class="col-sm-9">

                       <select name="id_exam_sitting" id="id_exam_sitting" class="form-control" >
                            <option value="">Select</option>
                            <?php
                            if (!empty($examSitting))
                            {
                                foreach ($examSitting as $record)
                                {
                                    $class="";
                                    if($record->id==$searchParam['id_exam_sitting']) {
                                       $class="selected=selected";
                                    } ?>
                             <option value="<?php echo $record->id;  ?>" <?php echo $class;?>>
                                <?php echo $record->exam_sitting_name;?>
                             </option>
                            <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                  </div>
                </div>

                 <div class="col-lg-6">
                  <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Exam Event</label>
                    <div class="col-sm-9">
                      <input type="text" class="form-control" id="exam_event_name" name="exam_event_name"  value="<?php echo $searchParam['exam_event_name'] ?>">
                    </div>
                  </div>
                </div>

            
              </div>

              <hr />
              <div class="d-flex justify-content-center">
                <button type="submit" class="btn btn-primary">Search</button>
                <a href="list" class="btn btn-link" >Clear All Fields</a>
              </div>
            </div>
          </div>

        </form>

      </div>
    </div>


    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Sl. No</th>
            <th>Exam Sitting</th>            
            <th>Exam Event</th>            
            <th>Module Name</th>            
            <th>Module Code</th>
            <th>Exam Date</th>
            <th>Start Time</th>
            <th>End Time</th>
            <th>Total students Vs mark entry</th>
            <th>Grader Name</th>
            <th>Mark Entry Status</th>
                     </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($examEventList)) {
            $i = 1;
                    $this->load->model('grader_exam_model');

            foreach ($examEventList as $record) {


$pendingCount=0;

   $pendingStudentReview = $this->grader_exam_model->getpendingCountStatus($record->graderexamid);
   $pendingCount = $pendingStudentReview[0]->totalcount;

   $countOfStudent = $this->grader_exam_model->getCountOfQuestionsByGraderExamId($record->graderexamid);

              if($countOfStudent) {
                $totalStudentforexamevent = $countOfStudent[0]->totalcount;
              }


              $countOfStudentPending = $this->grader_exam_model->getCountOfQuestionsByGraderExamIdStatus($record->graderexamid);
              if($countOfStudentPending) {
                $totalStudentforexameventPending = $countOfStudentPending[0]->totalcount;
              }

              // if($pendingCount==$totalStudentforexamevent) {
              //   continue;
              // }


$studentdata= array();
            $studentdata = $this->final_marks_approval_model->getGraderStudentTagListByDatanew($record->graderexamid);

            if($totalStudentforexameventPending==0) {
               continue;
            }

           
            if($studentdata[0]->final_total>0) {
               continue;
            }


          ?>
              <tr>
                <td><?php echo $i ?></td>
                                <td><?php echo $record->exam_sitting_name; ?></td>
                                <td><?php echo $record->name; ?></td>

                <td><?php echo $record->coursename; ?></td>
                <td><?php echo $record->coursecode; ?></td>
                <td><?php echo date('d-m-Y',strtotime($record->exam_date)); ?></td>
                <td><?php echo $record->from_tm;?></td>
                <td><?php echo $record->to_tm;?></td>
                <td><?php echo $totalStudentforexamevent;?>   /   <?php echo $totalStudentforexameventPending;?></td>
                <td><?php echo $record->gradername;?></td>
                <td>
              <?php if($totalStudentforexamevent==$totalStudentforexameventPending) {  ?> 
                  <a href="/exam/finalMarksApproval/approve/<?php echo $record->graderexamid;?>">Pending Approval</a>

                <?php } else if($record->approval_status=='2') {  ?> 
                  <a href="">Approved</a>

                 <?php } else { ?>
                  <a href="/exam/finalMarksApproval/approve/<?php echo $record->graderexamid;?>">Approve</a>

                 <?php } ?> 

                </td>
          
              </tr>
          <?php
              $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
</main>
<script>
  $('select').select2();

  function clearSearchForm() {
    window.location.reload();
  }
</script>