<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
  <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
    <h1 class="h3">Mark Approval List</h1>
  </div>
  <form action='' method="POST">
  <div class="page-container">
     <div class='custom-table'>
         <table  class='table' id='list-table'>
                  <tr>
                    <th>Exam Event</th>
                    <th>Exam Date </th>
                    <th>Exam Timings </th>
                </tr>
        <tbody>
            <?php 

  $programmeName = '';
              if($examEventDetails->mqms_type=='CPIF') {
                 $programmeName = $examEventDetails->programme_level_ciif."<br/>".$examEventDetails->programme_name_ciif."<br/>".$examEventDetails->name;
              } else  {
                 $programmeName = $examEventDetails->name;
              }

            ?>
              <tr>
                <td><?php echo $programmeName;?></td>
                <td><?php echo date('d-m-Y',strtotime($examEventDetails->exam_date));?></td>
                <td><?php echo $examEventDetails->from_tm;?> to <?php echo $examEventDetails->to_tm;?></td>
              </tr>
        </tbody>
    </table>


         <div class='custom-table'>
         <table  class='table' id='list-table'>
                  <tr>
                    <th>Sl. No</th>
                    <th>Student Name</th>
                    <th>Student Email</th>
                    <th>Student NRIC</th>
                    <th>Batch Number </th>
                    <th>Membership Number</th>
                    <th>MCQ</th>
                    <th>Essay</th>
                    <th>OverAll (%) </th>
                    <!-- <th>Grade </th> -->
                    <th style='text-align: center;'>Status</th>
                    <th style='text-align: center;'>Approved By</th>
                    <th style='text-align: center;'>Approved Datetime</th>
                </tr>
        <tbody>
          <?php
          
                    $this->load->model('grader_marks_approval_model');
                    $this->load->model('marksadjustment_model');



                for($i=0;$i<count($student_data);$i++)
            {

                $id = $student_data[$i]->id;
                $id_student = $student_data[$i]->id_student;
                $username = $student_data[$i]->username;
                $approved_date_time = $student_data[$i]->approved_date_time;
                $full_name = $student_data[$i]->full_name;
                $batch = $student_data[$i]->batch;
                $membership_number = $student_data[$i]->membership_number;
                $student_email = $student_data[$i]->full_name;
                $nric = $student_data[$i]->nric;
                $idexamstudenttagging = $student_data[$i]->examstudenttagging;
                $result_status = $student_data[$i]->result_status;


                 $totalmcqmarkedQuestion = $this->grader_marks_approval_model->getautomcqmarks($student_data[$i]->id_exam_student_tagging);
                 $mcqmarks = 0; 
                for($m=0;$m<count($totalmcqmarkedQuestion);$m++) {
                    if($totalmcqmarkedQuestion[$m]->marks>0) {
                    $mcqmarks = $mcqmarks + $totalmcqmarkedQuestion[$m]->questionmarks;
                  }
                } 




                $totalmarkedQuestion = $this->grader_marks_approval_model->totalMCQQuestionMarked($student_data[$i]->id_exam_student_tagging);
                
                

                if($result_status!='11') {
                    $originalMarks = 0;
                    $answermarks = 0;
                    for($m=0;$m<count($totalmarkedQuestion);$m++) {
                      $originalMarks = $originalMarks + $totalmarkedQuestion[$m]->originalmarks;

                         if($totalmarkedQuestion[$m]->answermarks<0) {
                            $totalmarkedQuestion[$m]->answermarks = 0;
                         }
                      $answermarks = $answermarks + $totalmarkedQuestion[$m]->answermarks;                              

                    }
                }


                if($result_status=='11') {
                    $originalMarks = 0;
                    $initialmarks = 0;
                    for($m=0;$m<count($totalmarkedQuestion);$m++) {
                      $originalMarks = $originalMarks + $totalmarkedQuestion[$m]->originalmarks;
                      $initialmarks = $initialmarks + $totalmarkedQuestion[$m]->answermarks;                             
                    }


                     $totalmarkedQuestionadj = $this->marksadjustment_model->totalMarksAdustmentList($student_data[$i]->id_exam_student_tagging);

                            $answermarks = 0;
                            for($m=0;$m<count($totalmarkedQuestionadj);$m++) {
                              $answermarks = $answermarks + $totalmarkedQuestionadj[$m]->answermarks;                              

                            }


                }



                            $markedQuestionCount = count($totalmarkedQuestion);


                            $overallMArks = $answermarks+$mcqmarks;
                            if($overallMArks>69.9) {
                              $grade = "PASS";
                            } else {
                              $grade = "FAIL";
                            }
                            
                            if(count($totalmarkedQuestion)==0) {
                                $grade = '-';
                            }


                            if($id_student==99) {
                                $grade = "PASS";
                            }
          ?>
              <tr>
                <td><?php echo $i+1 ?> 
                <?php if($grade!='-') { ?> 


                    <?php if($result_status!='15') { ?> 
                <input type='checkbox' name='student_exam_tagging[]' value='<?php echo $idexamstudenttagging;?>' />

                              <?php } ?> 


              <?php } ?> 

              </td>
                    <td><?php echo $full_name;?></td>
                    <td><?php echo $student_email;?></td>                    
                    <td><?php echo $nric;?></td>                    
                    <td><?php echo $batch;?></td>                    
                    <td><?php echo $membership_number;?></td>                    

                       <td><?php echo $mcqmarks;?></td>


                            <td><?php echo $answermarks;?></td>
                            <td><?php echo $overallMArks;?></td>
                            <!-- <td><?php echo $grade;?></td> -->
                           


                    <td>
                       <?php if($result_status=='15') { ?> 
                       Approved
              <?php } ?> 

               <?php if($result_status=='11') { ?> 
                       Mark Adjustment <Br/>
                       Initial Marks - <?php echo $initialmarks;?> <br/>
                       New  Marks - <?php echo $answermarks;?> <br/>
              <?php } ?> 
                    </td>
                     <td><?php echo $username;?></td>
                            <td><?php  if($approved_date_time!='' && $approved_date_time!='1970-01-01')  { echo date('d-m-Y H:i:s',strtotime($approved_date_time));} ;?></td>

          
              </tr>
         <?php 
          }
          ?>
        </tbody>
      </table>
    </div>

    
</form>
</main>
<script>
  $('select').select2();

  function clearSearchForm() {
    window.location.reload();
  }

   function showgrader(val) {
       $("#divgraderdisplay").hide();
   
     if(val=='13'){
       $("#divgraderdisplay").show();
     }
   }
   

</script>