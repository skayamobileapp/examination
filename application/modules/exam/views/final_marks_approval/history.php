<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
  <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
    <h1 class="h3">Marks Approval History</h1>
  </div>
  <div class="page-container">

    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Sl. No</th>
            <th>Exam Event</th>            
            <th>Module Name</th>            
            <th>Module Code</th>
            <th>Exam Date</th>
            <th>Start Time</th>
            <th>End Time</th>
            <th>Total students Vs mark entry</th>
            <th>Grader Name</th>
            <th>Mark Entry Status</th>
                     </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($examEventList)) {
            $i = 1;
                    $this->load->model('grader_exam_model');

            foreach ($examEventList as $record) {


$pendingCount=0;

   $pendingStudentReview = $this->grader_exam_model->getpendingCountStatus($record->graderexamid);
   $pendingCount = $pendingStudentReview[0]->totalcount;

   $countOfStudent = $this->grader_exam_model->getCountOfQuestionsByGraderExamId($record->graderexamid);
              if($countOfStudent) {
                $totalStudentforexamevent = $countOfStudent[0]->totalcount;
              }


              $countOfStudentPending = $this->grader_exam_model->getCountOfQuestionsByGraderExamIdStatus($record->graderexamid);
              if($countOfStudentPending) {
                $totalStudentforexameventPending = $countOfStudentPending[0]->totalcount;
              }

              if($countOfStudentPending[0]->totalcount==0) {
                continue;
              }

             


          ?>
              <tr>
                <td><?php echo $i ?></td>
                                <td><?php echo $record->name; ?></td>

                <td><?php echo $record->coursename; ?></td>
                <td><?php echo $record->coursecode; ?></td>
                <td><?php echo date('d-m-Y',strtotime($record->exam_date)); ?></td>
                <td><?php echo $record->from_tm;?></td>
                <td><?php echo $record->to_tm;?></td>
                <td><?php echo $totalStudentforexamevent;?>   /   <?php echo $totalStudentforexameventPending;?></td>
                <td><?php echo $record->gradername;?></td>
                <td>
                  <a href="/exam/finalMarksApproval/approvehistory/<?php echo $record->graderexamid;?>">View</a>

               
                
                </td>
          
              </tr>
          <?php
              $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
</main>
<script>
  $('select').select2();

  function clearSearchForm() {
    window.location.reload();
  }
</script>