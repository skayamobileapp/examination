<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
        <h1 class="h3">Exam Logout Time Session  </h1>
        
       

    </div>
    
    <form id="form_main" action="" method="post">

        <div class="page-container">

        
            <div class="form-container">


                <div class="row">

                    <div class="col-lg-8">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Number of Minutes Idle to logout from Exam Session <span class="text-danger">*</span></label>
                        <div class="col-sm-4">
                        <input type="text" class="form-control" id="name" name="name" placeholder="Days" value="<?php echo  $examSetList->name;?>">
                        </div>
                      </div>
                    </div>

                </div>       


                  
                <div class="button-block clearfix">
                  <div class="bttn-group">
                      <button type="submit" class="btn btn-primary">Save</button>
                      <a href='add' class="btn btn-link">Clear All Fields</a>
                  </div>

                </div> 

            </div>                                
        </div>
    </form>
</main>
