<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
  <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
    <h1 class="h3">Marks Adjustment</h1>
  </div>
  <div class="page-container">
  

    <div class="custom-table">
      <table class="table">
                        <thead>
                            <tr>
                            <th>Sl. No</th>
                             <th>Student</th>
<!--                              <th>Exam Center</th>
                             <th>Exam Event Name</th>
 -->                             <th>Exam Event Name</th>
                              <th>Exam Event Date</th>
                             <th>Total Question</th>
                             <th>Total Marks</th>
                             <th>Initial Marks Obtained</th>
                             <th>Adjusted Marks</th>
                             <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                             <?php
                         $total = 0;
                          for($i=0;$i<count($graderStudentTagList);$i++)
                         {

                            $this->load->model('marksadjustment_model');
                            $totalQuestion = $this->marksadjustment_model->totalMCQQuestion($graderStudentTagList[$i]->id_exam_student_tagging);

                            $totalmarkedQuestion = $this->marksadjustment_model->totalMCQQuestionMarked($graderStudentTagList[$i]->id_exam_student_tagging);

                            $originalMarks = 0;
                            $answermarks = 0;
                            for($m=0;$m<count($totalmarkedQuestion);$m++) {
                              $originalMarks = $originalMarks + $totalmarkedQuestion[$m]->originalmarks;
                              $answermarks = $answermarks + $totalmarkedQuestion[$m]->answermarks;                              

                            }



                             $totalmarkedQuestionadj = $this->marksadjustment_model->totalMarksAdustmentList($graderStudentTagList[$i]->id_exam_student_tagging);

                            $answermarksadj = 0;
                            for($m=0;$m<count($totalmarkedQuestionadj);$m++) {
                              $answermarksadj = $answermarksadj + $totalmarkedQuestionadj[$m]->answermarks;                              

                            }




                          ?>
                            <tr>

                          <?php if($graderStudentTagList[$i]->status=='0') { ?>

                            <td> 

                              <input type='checkbox' id='id_grader_student_tag[]' name='id_grader_student_tag[]' class='check' value="<?php echo $graderStudentTagList[$i]->id; ?>"><?php echo $i+1;?></td>

                            <?php } else if($graderStudentTagList[$i]->status=='1') {?> 
                              <td> 

                              <?php echo $i+1;?></td>

                            <?php } ?> 

                            <td><?php echo $graderStudentTagList[$i]->student_name . " - " . $graderStudentTagList[$i]->nric;?></td>
                          <td><?php echo $graderStudentTagList[$i]->examname;?></td>
                             <td><?php echo date('d-m-Y',strtotime($graderStudentTagList[$i]->exam_date));?></td>
                            <td><?php echo count($totalQuestion);?></td>
                            <td><?php echo $originalMarks;?></td>

                            <td><?php echo $answermarks;?></td>
                            <td><?php echo $answermarksadj;?></td>
                          
                            
                         
                            <td>
                              <a href="/exam/marksadjustment/add/<?php echo $graderStudentTagList[$i]->id_exam_student_tagging;?>" class="btn" type="" data-toggle="tooltip" data-placement="top" title="Add Exam">
                               Click to adjust marks
                            </a>
                            </td>
                         
                          


                             </tr>
                          <?php 
                      } 
                      ?>
                        </tbody>
                    </table>

    </div>
  </div>
</main>
<script>
  $('select').select2();

  function clearSearchForm() {
    window.location.reload();
  }
</script>