<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
        <h1 class="h3">Student Question Set</h1>
        
        <a href="/exam/marksadjustment/list" class="btn btn-link ml-auto">
            <i class="fa fa-chevron-left" aria-hidden="true"></i>
          Back
        </a>

    </div>
    



        <div class="page-container">

           

                <div class="form-container">





                <?php

                if(!empty($showStudentQuestionSetForGrader))
                {
                    ?>

                            <h4 class="form-group-title">Grader Student Details</h4>

                        
<form action="" method="POST">
                          <div class="custom-table">
                            <table class="table">
                                <thead>
                                    <tr>
                                    <th>Sl. No</th>
                                     <th>Question</th>
                                     <th>Total Marks</th>
                                     <th>Obtained Marks</th>
                                     <th>Adjustment Marks</th>


                                     <th class="text-center">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                     <?php
                                 $total = 0;
                                         $this->load->model('marksadjustment_model');

                                  for($i=0;$i<count($showStudentQuestionSetForGrader);$i++)
                                 {
                                

                                  $checkadjmarks = $this->marksadjustment_model->getadjmarks($showStudentQuestionSetForGrader[$i]->id);

                                  if($checkadjmarks->marks) {
                                    $adjmarks = $checkadjmarks->marks;
                                  } else {
                                    $adjmarks = $showStudentQuestionSetForGrader[$i]->marks;
                                  }
                                  ?>
                                    <tr>
                                    <td><?php echo $i+1;?></td>
                                    <td><?php echo $showStudentQuestionSetForGrader[$i]->question;?> </td>
                                   
                                    <td><?php echo $showStudentQuestionSetForGrader[$i]->questionmarks;?></td>
                                    <td><?php echo $showStudentQuestionSetForGrader[$i]->marks;?></td>

                                      <td><input type='text' name='adjmarks[]' class='form-control numeric' value="<?php echo $adjmarks;?>" maxlength='2' />
                                      <input type='hidden' name='questionset[]' value="<?php echo $showStudentQuestionSetForGrader[$i]->id;?>" />
                                    </td>
                                
                                    <td class="text-center">
                                      <a href="/exam/marksadjustment/viewquestion/<?php echo $showStudentQuestionSetForGrader[$i]->id;?>" class="btn" type="" data-toggle="tooltip" data-placement="top" title="Add Exam">
                                      View Answer
                                    </a>
                                    </td>

                                     </tr>
                                  <?php 
                              } 
                              ?>
                                 <tr>
                                  <td colspan="4"></td>
                                  <td><input type='submit' name='Save' id='Save' value='Save' class="btn-btn-primary" /><input type='submit' name='Submit' id='Submit' value='Submit'  class="btn-link"/></td>
                                 </tr>
                                </tbody>
                            </table>
                          </div>
                        </form>

                        </div>


                <?php
                
                }
                ?>


                    

                </div>                                
            

        </div>



</main>

<script>

$(document).ready(function () {
  //called when key is pressed in textbox
  $(".numeric").keypress(function (e) {
     //if the letter is not digit then display error and don't type anything
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
        $("#errmsg").html("Digits Only").show().fadeOut("slow");
               return false;
    }
   });
});


    function showStudentQuestionSetForGrader(id_student)
    {
         alert(id_student);
        
        var tempPR = {};
        tempPR['id_student'] = id_student;
        tempPR['id_exam_center'] = '<?php echo $id_exam_center ?>';
        tempPR['id_exam_event'] = '<?php echo $id_exam_event ?>';
            $.ajax(
            {
               url: '/grader/graderStudentTag/showStudentQuestionSetForGrader',
               type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                // alert(result);
                $("#view_student_question_set").show();
                $("#view_student_question_set").html(result);
               }
            });
    }

    function getExamEventByExamCenter(id)
    {
        $.get("/exam/graderStudentTag/getExamEventByExamCenter/"+id, function(data, status)
        {  
            $("#view_exam_event").html(data);
        });
    }
    

    $(document).ready(function() {
        $("#form_main").validate({
            rules: {
                id_exam_center: {
                    required: true
                },
                 id_exam_event: {
                    required: true
                },
                 exam_date: {
                    required: true
                }
            },
            messages: {
                id_exam_center: {
                    required: "<p class='error-text'>Select Exam Center</p>",
                },
                id_exam_event: {
                    required: "<p class='error-text'>Select Exam Event</p>",
                },
                exam_date: {
                    required: "<p class='error-text'>Select Exam Date</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>