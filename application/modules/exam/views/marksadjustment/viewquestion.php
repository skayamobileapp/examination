<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
        <h1 class="h3">Student Question Set</h1>
        
        <a href="/exam/marksadjustment/add/<?php echo $qtns->id_exam_student_tagging;?> " class="btn btn-link ml-auto">
            <i class="fa fa-chevron-left" aria-hidden="true"></i>
          Back
        </a>

    </div>
    



        <div class="page-container">

           

                <div class="form-container">




                          <div class="custom-table">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th colspan="2"><?php echo $qtns->question;?></th>
                                    </tr>
                                    <tr>
                                     <th>Answer scheme</th>
                                     <th>Answer</th>
                                    </tr>
                                </thead>
                                <tbody>
                                   
                                    <tr>
                                    <td><?php echo $qtns->answer_scheme;?> </td>
                                   
                                    <td><?php echo $qtns->answer_text;?> </td>
                                </tbody>
                            </table>
                          </div>
                        </form>

                        </div>


               

                    

                </div>                                
            

        </div>



</main>

<script>

$(document).ready(function () {
  //called when key is pressed in textbox
  $(".numeric").keypress(function (e) {
     //if the letter is not digit then display error and don't type anything
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
        $("#errmsg").html("Digits Only").show().fadeOut("slow");
               return false;
    }
   });
});


    function showStudentQuestionSetForGrader(id_student)
    {
         alert(id_student);
        
        var tempPR = {};
        tempPR['id_student'] = id_student;
        tempPR['id_exam_center'] = '<?php echo $id_exam_center ?>';
        tempPR['id_exam_event'] = '<?php echo $id_exam_event ?>';
            $.ajax(
            {
               url: '/grader/graderStudentTag/showStudentQuestionSetForGrader',
               type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                // alert(result);
                $("#view_student_question_set").show();
                $("#view_student_question_set").html(result);
               }
            });
    }

    function getExamEventByExamCenter(id)
    {
        $.get("/exam/graderStudentTag/getExamEventByExamCenter/"+id, function(data, status)
        {  
            $("#view_exam_event").html(data);
        });
    }
    

    $(document).ready(function() {
        $("#form_main").validate({
            rules: {
                id_exam_center: {
                    required: true
                },
                 id_exam_event: {
                    required: true
                },
                 exam_date: {
                    required: true
                }
            },
            messages: {
                id_exam_center: {
                    required: "<p class='error-text'>Select Exam Center</p>",
                },
                id_exam_event: {
                    required: "<p class='error-text'>Select Exam Event</p>",
                },
                exam_date: {
                    required: "<p class='error-text'>Select Exam Date</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>