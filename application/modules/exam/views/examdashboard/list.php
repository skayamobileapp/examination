<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
  <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
    <h1 class="h3">Exam Event Dashboard</h1>
  </div>
  <div class="page-container">
    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">

    </div>
    <?php
    if ($this->session->flashdata('success')) {
    ?>
      <div class="alert alert-success alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
      </div>
    <?php
    }
    if ($this->session->flashdata('error')) {
    ?>
      <div class="alert alert-danger alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Failure!</strong> <?php echo $this->session->flashdata('error'); ?>
      </div>
    <?php
    }
    ?>
    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Sl. No</th>
            <th>Module</th>
            <th>Module Code</th>
            <th>Exam Sitting</th>
            <th>Exam Event Name</th>
            <th>Exam Center</th>
            <th>Event Start Date</th>
            <th>Event Start Time</th>

            <th>Event End Date</th>
            <th>Event End Time</th>
            <th>Type</th>
            <th>Quota</th>
            <th>Total Students</th>
                        <th style="text-align: center;">View Students</th>
                        <th>Exam Set</th>

          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($examEventList)) {
            $i = 1;
                    $this->load->model('exam_event_model');

            foreach ($examEventList as $record) {

              $totalStuents = 0;
              $totalStudetnsObject = $this->exam_event_model->examStudentTaggingListSearch($record->id);

              $totalStuents = count($totalStudetnsObject);
          ?>
              <tr>
                <td><?php echo $i ?></td>
                <td><?php echo $record->coursename ?></td>
                <td><?php echo $record->coursecode ?></td>
                <td><?php echo $record->exam_sitting_name ?></td>
                <td><?php echo $record->name ?></td>
                <td><?php echo $record->examcenternae ?></td>
                <td><?php echo date('d-m-Y', strtotime($record->exam_date)) ?></td>
                <td><?php echo date("H:i", strtotime($record->from_tm)) ?></td>

                  <td><?php echo date('d-m-Y', strtotime($record->exam_end_date)) ?></td>
                <td><?php echo date("H:i", strtotime($record->to_tm)) ?></td>


                <td><?php  if($record->type=='1') { echo "Face to Face";}
                               if($record->type=='2') { echo "Supervised Online Exam";}
                               if($record->type=='3') { echo "Online Exam";}

                                ?></td>
                <td><?php echo $record->max_count ?></td>

                <td>
                  <?php echo $totalStuents;?>
                </td>

                <td>
                  <?php if($record->id_exam_set) { ?> 

                    <p style='color:green;'>Exam Set Defined</p>
                   <?php } else { ?>
                    <p style='color:red;'>Go to Exam Event and Define Exam Set</p>

                   <?php } ?> 
                </td>
               
                

                 <td class="text-center">

                  <a href="<?php echo 'students/' . $record->id; ?>" class="btn" type="" data-toggle="tooltip" data-placement="top" title="Edit">
                    View Students
                  </a>

                </td>


              </tr>
          <?php
              $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
</main>
<script>
  $('select').select2();

  function clearSearchForm() {
    window.location.reload();
  }
</script>