<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
        <h1 class="h3">Student Attemp Question Details</h1>

        <a href="<?php echo '../../edit/'. $studentDetails->id; ?>" class="btn btn-link ml-auto">
            <i class="fa fa-chevron-left" aria-hidden="true"></i>
          Back
        </a>

        
    </div>


          <div class="form-container">
              <h4 class="form-group-title">Student Details</h4>
              <div class='data-list'>
                  <div class='row'> 
                      <div class='col-sm-4'>
                          <dl>
                              <dt>Student Name :</dt>
                              <dd><?php echo ucwords($studentDetails->full_name);?></dd>
                          </dl>
                                                
                      </div>        
                      
                      <div class='col-sm-4'>                           
                          <dl>
                              <dt>Student Email :</dt>
                              <dd><?php echo $studentDetails->email_id; ?></dd>
                          </dl>
                         
                      </div>

                       <div class='col-sm-4'>                           
                          <dl>
                              <dt>Download Exam sheet</dt>
                              <dd><a href="<?php echo '/records/student/downloadQuestion/'.$id_exam_tagging . '/' . $id_attempt; ?>" target="_blank">Download Here</a></dd>
                          </dl>
                         
                      </div>
                  </div>
              </div>
          </div>




            


<form id="form_applicant" action="" method="post" enctype="multipart/form-data">


            <div class="m-auto text-center">
                <div class="width-4rem height-4 bg-primary rounded mt-4 marginBottom-40 mx-auto"></div>
            </div>


            <div class="clearfix">

               



            
                
                <div class="tab-content offers-tab-content">



                    <div role="tabpanel" class="tab-pane active" id="tab_one">
                        
                        <div class="col-12">



                          <?php

                                if(!empty($examQuestionsByData))
                                {
                                    ?>

                                    <div class="">
                                                                                   

                                          <div class="custom-table">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                    <th valign="top">Sl. No</th>
                                                    <th>Question</th>
                                                    <th>Submitted Answer</th>
                                                    <th>Answered Date & Time</th>
                                                    <th>Correct Answer</th>
                                                    <th>Result Status</th>
                                                    <th>Photo</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                <?php
                                                  for($i=0;$i<count($examQuestionsByData);$i++)
                                                 { ?>
                                                    <tr>
                                                    <td valign="top"><?php echo $i+1;?></td>
                                                    <td><?php echo $examQuestionsByData[$i]->question; ?></td>
                                                    <td><?php if($examQuestionsByData[$i]->id_answer == 0)
                                                    {
                                                      echo ' No Response Added';
                                                    }else
                                                    {
                                                      echo $examQuestionsByData[$i]->answer;
                                                    }
                                                     ?></td>
                                                    <td><?php if($examQuestionsByData[$i]->datetime){
                                                      echo date('d-m-Y h:i:s A', strtotime($examQuestionsByData[$i]->datetime)); } ?></td>
                                                    <td><?php echo $examQuestionsByData[$i]->correct_answer; ?></td>
                                                    <td><?php 
                                                    if($examQuestionsByData[$i]->id_answer == 0)
                                                    {
                                                      echo ' No Response Added';
                                                    }
                                                    elseif($examQuestionsByData[$i]->is_correct_answer ==1)
                                                    {
                                                      echo 'Correct';
                                                    }else
                                                    {
                                                      echo 'Wrong';
                                                    }
                                                      ?></td>
                                                       <td><img src="/<?php echo  $examQuestionsByData[$i]->student_image;?>" style="height:75px;"/></td>


                                                    </tr>
                                                  <?php
                                              } 
                                              ?>
                                                </tbody>
                                            </table>
                                          </div>

                                        </div>

                                <?php
                                
                                }
                                else
                                {
                                    ?>

                                    <h4>No Attempt Questions Found</h4>
                                    <?php

                                }
                                 ?>



                        </div> 

                         <?php $q=1;  for($i=0;$i<count($showStudentQuestionSetForGrader);$i++)
                                 {  
                                    ?>
    <div class="row">
              <div class="col-sm-12">
                 <div class="custom-table form-container">
                     <div style="color: #4276ef">
                         <?php echo $showStudentQuestionSetForGrader[$i]->question_number;?><?php echo $showStudentQuestionSetForGrader[$i]->question;?>
                     </div>
                    <table class="table table-bordered" style="width: 100%;">

                        <thead>
                             <tr>
                             <th style="width:45%">Answer Scheme</th>
                             <th style="width:45%">Answer</th>
                             <th style="width:5%">Total Marks</th>
                             <th style="width:5%">Marks Obtained</th>
                            </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td> <?php echo $showStudentQuestionSetForGrader[$i]->answer_scheme;?></td>
                          
                             <?php if($showStudentQuestionSetForGrader[$i]->answer_text=='') { ?>
                                <td style="color: red;"> Candidate did not answer
                                </td>

                             <?php } else { ?>
                             <td style="color: #229116"> 
                                <?php echo $showStudentQuestionSetForGrader[$i]->answer_text;?></td>
                            <?php } ?> 

                              <td> <?php echo $showStudentQuestionSetForGrader[$i]->questionmarks;?></td>

                               <td> <?php echo $showStudentQuestionSetForGrader[$i]->marks;?> </td>

                          </tr>

                   
                        </tbody>
                      </table>
                    </div>
              </div>
            </div>
   <?php  $q++;} ?> 
                    
                    </div>




                </div>






                


        </div>


    </form>





     <!--  <div class="chat-bot-container" style="display: ;">
          <div class="cb-header">
              Hi, <strong><?php print_r($_SESSION['applicant_name']);?></strong> <br />We are happy to help you,
          </div>
          <ul class="cb-help">
              <li>Type '<strong>1</strong>' : For Application Status</li>
              <li>Type '<strong>2</strong>' : For Fee Amount</li>
              <li>Type '<strong>3</strong>' : For Offer Letter</li>
              <li>Type '<strong>4</strong>' : To know the start date of the semester</li>
              <li>Type '<strong>9</strong>' : To get a call from the representative</li>
          </ul>
         
          <div id='attach'></div>
          <div class="form-group cb-form">
              <input type="text" class="form-control" placeholder="Type here..." name='message' id='message' value=''>
              <button class="btn btn-primary" type="button" onclick='validateData()'>Start</button>
            </div>                                   
      </div> -->




        <footer class="footer-wrapper">
            <p>&copy; 2019 All rights, reserved</p>
        </footer> 
      






</main>


<script>

    $('select').select2();


    $( function() {
    $( ".datepicker" ).datepicker({
        changeYear: true,
        changeMonth: true,
    });
    });
</script>