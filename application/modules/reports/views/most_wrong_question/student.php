<style>
.green {
   color:green;
}
</style>
<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
<div class="page-container">
    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="">
              Advanced Search
              <i class="fa fa-angle-right" aria-hidden="true"></i>
              <i class="fa fa-angle-down" aria-hidden="true"></i>
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">

          <div id="collapseOne" class="panel-collapse collapse show" role="tabpanel" aria-labelledby="headingOne" aria-expanded="true" style="">
            <div class="panel-body p-2">

              <div class="row">

                <div class="col-sm-12">
                                <div class="custom-table">

                    <table class="table">
                       <thead>
                    <tr>
                      <th><?php echo $questionList->question;?></th>

                      </tr>
                    </thead>
                      <?php for($i=0;$i<count($answerList);$i++) {
                        $answeroption ='';
                           if($answerList[$i]->is_correct_answer=='1') {
                              $answeroption = 'green';
                           }

                         ?>
                        <tr>
                          <td class="<?php echo $answeroption;?>"><?php echo $answerList[$i]->option_description;?></td>
                        </tr>

                      <?php } ?> 
                    </table>
                  </div>
                  </div>
                </div>
                
              

          </div>

        </form>

      </div>
    </div>  


            <?php
            if(!empty($studentList))
            {
              ?>


              <div class="custom-table">
                <table class="table" id="list-table">
                  <thead>
                    <tr>
                      <th>Sl. No</th>
                      <th>Student Name</th>
                      <th>NRIC</th>
                      <th>Membership Number</th>
                      <th>Exam Name</th>
                      <th>Exam Date</th>
                      <th>Answered</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    $i=1;
                      foreach ($studentList as $record)
                      {
                    ?>
                        <tr>
                          <td><?php echo $i ?></td>
                          <td><?php echo $record->full_name ?></td>
                          <td><?php echo $record->nric ?></td>
                          <td><?php echo $record->membership_number ?></td>
                          <td><?php echo $record->name ?></td>
                          <td><?php echo date('d-m-Y',strtotime($record->exam_date)) ?></td>
                          <td><?php echo $record->option_description ?></td>

                        </tr>
                    <?php
                      $i++;
                    }
                    ?>
                  </tbody>
                </table>
              </div>   

               <?php
            }
            ?>      


            </div>                                
        </div>
    </form>
</main>


<script type="text/javascript">


    $( function() {
      $( ".datepicker" ).datepicker({
          changeYear: true,
          changeMonth: true,
      });
    } );

    $('select').select2();

    function reloadPage()
    {
      window.location.reload();
    }
</script>

<script type="text/javascript">


    $( function() {
      $( ".datepicker" ).datepicker({
          changeYear: true,
          changeMonth: true,
      });
    } );

   
</script>
<script type="text/javascript">
        $(document).ready(function(){
 
            $('#id_course').change(function(){ 
                var id=$(this).val();
                $.ajax({
                    url : "<?php echo site_url('question/ajax/getCategoryTopics');?>",
                    method : "POST",
                    data : {id: id},
                    async : true,
                    dataType : 'json',
                    success: function(data){
                         
                        var html = '';
                        var i;
                        for(i=0; i<data.length; i++){
                            html += '<option value='+data[i].id+'>'+data[i].name+'-'+data[i].name+'</option>';
                        }
                        $('#id_topic').html(html);
 
                    }
                });
                return false;
            }); 
             
        });
    </script>