<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
  <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
    <h1 class="h3">Exam Issue Reports</h1>
  </div>
  <div class="page-container">
    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="">
              Advanced Search
              <i class="fa fa-angle-right" aria-hidden="true"></i>
              <i class="fa fa-angle-down" aria-hidden="true"></i>
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">

          <div id="collapseOne" class="panel-collapse collapse show" role="tabpanel" aria-labelledby="headingOne" aria-expanded="true" style="">
            <div class="panel-body p-2">

              <div class="row">

                <div class="col-lg-6">
                  <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Exam Event</label>
                    <div class="col-sm-9">
                      <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="<?php echo $searchParam['name'] ?>">
                    </div>
                  </div>
                </div>

                <div class="col-lg-6">
                  <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Location</label>
                    <div class="col-sm-9">
                      <select name="role" id="role" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($locationList)) {
                          foreach ($locationList as $record) {
                            $selected = '';
                            if ($record->id == $searchParam['id_location']) {
                              $selected = 'selected';
                            }
                        ?>
                            <option value="<?php echo $record->id;  ?>" <?php echo $selected;  ?>>
                              <?php echo  $record->name;  ?>
                            </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>

              </div>

              <div class="row">

                <div class="col-lg-6">
                  <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Exam Center</label>
                    <div class="col-sm-9">
                      <select name="id_exam_center" id="id_exam_center" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($examCenterList)) {
                          foreach ($examCenterList as $record) {
                            $selected = '';
                            if ($record->id == $searchParam['id_exam_center']) {
                              $selected = 'selected';
                            }
                        ?>
                            <option value="<?php echo $record->id;  ?>" <?php echo $selected;  ?>>
                              <?php echo  $record->name;  ?>
                            </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Exam Type</label>
                    <div class="col-sm-9">
                      <select name="type" id="type" class="form-control">
                        <option value="">Select</option>
                        <option value="Past" <?php if($searchParam['type']=='Past') {
                            echo "selected=selected";
                                
                            } ?> >Past</option>
                        <option value="Upcoming" <?php if($searchParam['type']=='Upcoming') {
                            echo "selected=selected";
                                
                            } ?>>Upcoming</option>
                       
                      </select>
                    </div>
                  </div>
                </div>

                 <div class="col-lg-6">
                  <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Exam Start Date</label>
                                        <div class="col-sm-9">

                    <input type='date' name='exam_start_date' class="form-control">
                  </div>
                  </div>
                </div>

                 <div class="col-lg-6">
                  <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Exam End Date</label>
                                        <div class="col-sm-9">

                    <input type='date' name='exam_end_date' class="form-control">
                  </div>
                  </div>

              </div>
            </div>


              <hr />
              <div class="d-flex justify-content-center">
                <button type="submit" class="btn btn-primary">Search</button>
                <button type="reset" class="btn btn-link" onclick="clearSearchForm()">Clear All Fields</button>
              </div>
            </div>
          </div>

        </form>

      </div>
    </div>
    <?php
    if ($this->session->flashdata('success')) {
    ?>
      <div class="alert alert-success alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
      </div>
    <?php
    }
    if ($this->session->flashdata('error')) {
    ?>
      <div class="alert alert-danger alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Failure!</strong> <?php echo $this->session->flashdata('error'); ?>
      </div>
    <?php
    }
    ?>
    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Sl. No</th>
            <th>Exam Sitting</th>
            <th>Exam Event Name</th>
            <th>Exam Center</th>
            <th>Event Start Date</th>
            <th>Event Start Time</th>

            <th>Event End Date</th>
            <th>Event End Time</th>
            <th>Number of Issue</th>
            
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($examEventList)) {
            $i = 1;
                    $this->load->model('reports_model');

            foreach ($examEventList as $record) {

             $totalIssuesArray = $this->reports_model->numberofIssues($record->id_exam_center,$record->id);


               $totalIssues = count($totalIssuesArray);

                $programmeName = '';
              if($record->id=='1') {
                 $programmeName = "Professional Ethics Module";
              } else  {
                 $programmeName = "CPIF <br/> Intermediate";
              }


          ?>
            
              <tr>
                <td><?php echo $i ?></td>
                <td><?php echo $record->exam_sitting_name ?></td>
                <td><?php echo $programmeName;?> <br/>
                <?php echo $record->coursename ?> <br/>
                <?php echo $record->coursecode ?>  </td>
                <td><?php echo $record->examcenternae ?></td>
                <td><?php echo date('d-m-Y', strtotime($record->exam_date)) ?></td>
                <td><?php echo date("H:i", strtotime($record->from_tm)) ?></td>

                  <td><?php echo date('d-m-Y', strtotime($record->exam_end_date)) ?></td>
                <td><?php echo date("H:i", strtotime($record->to_tm)) ?></td>


                <td><a href="/reports/examIssue/issue/<?php echo $record->id_exam_center;?>/<?php echo $record->id;?>"><?php echo $totalIssues ?></a></td>
               

                

              </tr>
          <?php
              $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
</main>
<script>
  $('select').select2();

  function clearSearchForm() {
    window.location.reload();
  }
</script>