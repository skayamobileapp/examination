<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Absent extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('student_model');
        $this->isLoggedIn();
        error_reporting(0);

    }

    public function list(){

    	if ($this->checkAccess('absent.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
        	$data = '';

 			if($this->input->post())
            {
	            $formData['full_name'] = $this->security->xss_clean($this->input->post('full_name'));
	            $formData['email_id'] = $this->security->xss_clean($this->input->post('email_id'));
	            $formData['nric'] = $this->security->xss_clean($this->input->post('nric'));
	            $formData['applicant_status'] = $this->security->xss_clean($this->input->post('applicant_status'));
	            
	            $data['searchParam'] = $formData;
	            $data['studentList'] = $this->student_model->studentAbsentList($formData);
	        }
	    }



            $this->global['pageTitle'] = 'Examination Management System : Absent List';
            $this->global['pageCode'] = 'absent.list';
            //print_r($subjectDetails);exit;
            $this->loadViews("absent/list", $this->global, $data, NULL);
        }
    }
