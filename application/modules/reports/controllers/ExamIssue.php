<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class ExamIssue extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('reports_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('exam_issue.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        { 
            $formData = array();
             
            $formData['id_location'] = $this->security->xss_clean($this->input->post('id_location'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['type'] = $this->security->xss_clean($this->input->post('type'));
            $formData['id_exam_center'] = $this->security->xss_clean($this->input->post('id_exam_center'));

            $data['searchParam'] = $formData;

            $data['examEventList'] = $this->reports_model->examEventListSearch($formData);


            $this->global['pageCode'] = 'exam_issue_report.list';
            //print_r($subjectDetails);exit;
            $this->loadViews("exam_issue/list", $this->global, $data, NULL);
        }
    }

      function issue($id_exam_center,$id_exam_event){

          
                  $data['getAllIssue'] = $this->reports_model->getIssueDetails($id_exam_center,$id_exam_event);

        


       
           
            $this->global['pageCode'] = 'exam_issue_report.list';
            //print_r($subjectDetails);exit;
            $this->loadViews("exam_issue/issue", $this->global, $data, NULL);
    }


}
