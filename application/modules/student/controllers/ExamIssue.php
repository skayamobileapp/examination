<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class ExamIssue extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('exam_reports_model');
        $this->load->model('exam/exam_event_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('exam_event.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        { 
            $formData['id_location'] = $this->security->xss_clean($this->input->post('id_location'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_exam_center'] = $this->security->xss_clean($this->input->post('id_exam_center'));

            $formData['type'] = $this->security->xss_clean($this->input->post('type'));

            $data['searchParam'] = $formData;

                $data['getAllIssue'] =  $this->exam_event_model->examEventIssueSearch();

            $this->global['pageCode'] = 'exam_issue.list';
            $this->global['pageTitle'] = 'Examination Management System : Exam Events';
            $this->loadViews("exam_issue/list", $this->global, $data, NULL);
        }
        
    }


}
