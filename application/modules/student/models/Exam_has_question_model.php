<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Exam_has_question_model extends CI_Model
{

    function courseListByStatus($status)
    {
        $this->db->select('c.*');
        $this->db->from('course as c');
        $this->db->where('c.status', $status);
        $this->db->order_by("c.name", "ASC");

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function topicListByStatus($status)
    {
        $this->db->select('c.*');
        $this->db->from('topic as c');
        $this->db->where('c.status', $status);
        $this->db->order_by("c.name", "ASC");

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function courseLearningObjectListByStatus($status)
    {
        $this->db->select('c.*');
        $this->db->from('course_learning_objective as c');
        $this->db->where('c.status', $status);
        $this->db->order_by("c.name", "ASC");

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function bloomTaxonomyListByStatus($status)
    {
        $this->db->select('c.*');
        $this->db->from('bloom_taxonomy as c');
        $this->db->where('c.status', $status);
        $this->db->order_by("c.name", "ASC");

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function difficultLevelListByStatus($status)
    {
        $this->db->select('c.*');
        $this->db->from('difficult_level as c');
        $this->db->where('c.status', $status);
        $this->db->order_by("c.name", "ASC");

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }



    function examHasQuestionList()
    {
        $this->db->select('*');
        $this->db->from('exam_has_question_count');
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();     
         return $result;
    }

    function examHasQuestionListSearch($data)
    {
        $this->db->select('a.*, c.code as course_code, c.name as course_name, t.code as topic_code, t.name as topic_name, b.code as bloom_taxonomy_code, b.name as bloom_taxonomy_name, clo.code as course_learning_objective_code, clo.name as course_learning_objective_name, d.code as difficult_level_code, d.name as difficult_level_name');
        $this->db->from('exam_has_question_count as a');
        $this->db->join('course as c', 'a.id_course = c.id');
        $this->db->join('topic as t', 'a.id_topic = t.id','left');
        $this->db->join('bloom_taxonomy as b', 'a.id_bloom_taxonomy = b.id','left');
        $this->db->join('course_learning_objective as clo', 'a.id_course_learning_objective = clo.id','left');
        $this->db->join('difficult_level as d', 'a.id_difficult_level = d.id','left');
        if ($data['id_course'] != '')
        {
            $this->db->where('a.id_course', $data['id_course']);
        }
        if ($data['id_topic'] != '')
        {
            $this->db->where('a.id_topic', $data['id_topic']);
        }
        if ($data['id_course_learning_objective'] != '')
        {
            $this->db->where('a.id_course_learning_objective', $data['id_course_learning_objective']);
        }
        if ($data['id_bloom_taxonomy'] != '')
        {
            $this->db->where('a.id_bloom_taxonomy', $data['id_bloom_taxonomy']);
        }
        if ($data['id_difficult_level'] != '')
        {
            $this->db->where('a.id_difficult_level', $data['id_difficult_level']);
        }
        // $this->db->order_by("a.name", "ASC");
         $query = $this->db->get();
        //  echo $this->db->error();die;
        // echo "<pre>";print_r($this->db->error());die;
        $result = $query->result();
         return $result;
    }
    
    
    function addNewExamHasQuestion($data)
    {
        $this->db->trans_start();
        $this->db->insert('exam_has_question_count', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }


    function editExamHasQuestionDetails($data, $id)
    {
        $this->db->where('id', $id);
        $result = $this->db->update('exam_has_question_count', $data);
        return $result;
    }

    function moveQuestionsIdToExam($data,$id_exam_has_count_details)
    {
        $question_count = $data['question_count'];
        $questions = $this->getQuestions($data);

        
        foreach ($questions as $question)
        {
            $id_question = $question->id;
            $data['id_question'] = $id_question;
            $data['id_exam_has_count_details'] = $id_exam_has_count_details;

            $inserted_exam_has_uestion = $this->addExamHasQuestion($data);
        }

        return TRUE;

    }

    function getQuestions($data)
    {
        $sql_topic = "";
        $sql_course_learning_objective = "";
        $sql_bloom_taxonomy = "";
        $sql_difficult_level = "";

        if($data['id_topic'] != '')
        {
            $sql_topic = " AND a.id_topic = ".$data['id_topic'];
        }

        if($data['id_course_learning_objective'] != '')
        {
            $sql_course_learning_objective = " AND a.id_course_learning_objective = ".$data['id_course_learning_objective'];
        }

        if($data['id_bloom_taxonomy'] != '')
        {
            $sql_bloom_taxonomy = " AND a.id_bloom_taxonomy = ".$data['id_bloom_taxonomy'];
        }

        if($data['id_difficult_level'] != '')
        {
            $sql_difficult_level = " AND a.id_difficult_level = ".$data['id_difficult_level'];
        }

        $sql_query = "SELECT a.id from question as a WHERE a.id_course = ".$data['id_course'] . $sql_topic . $sql_course_learning_objective . $sql_bloom_taxonomy . $sql_difficult_level . " AND a.id NOT IN(SELECT id_question  FROM exam_has_question) LIMIT  ". $data['question_count'] ;
        $query = $this->db->query($sql_query);
        return $query->result();
    }

    function addExamHasQuestion($data)
    {
        $this->db->trans_start();
        $this->db->insert('exam_has_question', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getExamHasQuestion($id_exam_has_question)
    {
        $this->db->select('*');
        $this->db->from('exam_has_question_count');
        $this->db->where('id', $id_exam_has_question);
        $query = $this->db->get();
        return $query->row();
    }

    function getExamHasQuestionDetails($id_exam_has_question)
    {
        $this->db->select('ehq.*, q.description as question, q.image');
        $this->db->from('exam_has_question as ehq');
        $this->db->join('question as q', 'ehq.id_question = q.id');
        $this->db->where('ehq.id_exam_has_count_details', $id_exam_has_question);
        $query = $this->db->get();
        return $query->result();
    }
}

