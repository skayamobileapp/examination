<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Exam_set_model extends CI_Model
{
    function examSetList()
    {
        $this->db->select('*');
        $this->db->from('exam_set');
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

  function insertChat($data) {
        $this->db->trans_start();
        $this->db->insert('chat', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getChatData($idstudent){
         $this->db->select('*');
        $this->db->from('chat');
        $this->db->where('student_id', $idstudent);
        $this->db->order_by("id", "ASC");
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }
    

    function getMaxSessionTime()
    {
        $id=1;
        $this->db->select('*');
        $this->db->from('exam_logout');
        $this->db->where('id', $id);

        $query = $this->db->get()->row();
        return $query;
    }


     function updatelock($exam_student_tagging)
    {

        $this->db->where('id', $exam_student_tagging);
        $data = array(
            'exam_lock' => 1
        );
        $this->db->update('exam_student_tagging', $data);  

        return;

    }


     function updateextratime($exam_student_tagging)
    {


        $this->db->where('id', $exam_student_tagging);
        $data = array(
            'extra_time_updated_status' => 1
        );
        $this->db->update('exam_student_tagging', $data);  

        return;

    }

    function updateextratimebulk($exam_student_tagging)
    {


        $this->db->where('id', $exam_student_tagging);
        $data = array(
            'bulk_extra_time_status' => 1
        );
        $this->db->update('exam_student_tagging', $data);  

        return;

    }


    function updateextratimetwo($exam_student_tagging)
    {


        $this->db->where('id', $exam_student_tagging);
        $data = array(
            'extra_two_time_status' => 1
        );
        $this->db->update('exam_student_tagging', $data);  

        return;

    }






    function updateextratimebulktwo($exam_student_tagging)
    {


        $this->db->where('id', $exam_student_tagging);
        $data = array(
            'bulk_extra_time_two_status' => 1
        );
        $this->db->update('exam_student_tagging', $data);  

        return;

    }


      function updateexamstatus($exam_student_tagging,$status)
    {

        $this->db->where('id', $exam_student_tagging);
        $data = array(
            'exam_status' => $status 
        );
        $this->db->update('exam_student_tagging', $data);  

        return;

    }






    function getLasttimeAnswered($studentId){
        $this->db->select('*');
        $this->db->from('student_question_set');
        $this->db->where('id_student', $studentId);
        $this->db->order_by('datetime desc');
        $questions = $this->db->get()->row();
        return $questions;
    }

    function examSetListSearch($search)
    {
        $this->db->select('*');
        $this->db->from('exam_set');
        if (!empty($search)) {
            $likeCriteria = "(name  LIKE '%" . $search . "%' or instruction  LIKE '%" . $search . "%')";
            $this->db->where($likeCriteria);
        }
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function checkExamAnswer($id)
    {
        $this->db->select('is_correct_answer');
        $this->db->from('question_has_option');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
    function getExamSet($id)
    {
        $this->db->select('*');
        $this->db->from('exam_set');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function addNewExamSet($data)
    {
        $this->db->trans_start();
        $this->db->insert('exam_set', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }


  

    function saveUserExam($data)
    {
        $this->db->trans_start();
        $this->db->insert('student_question_set', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function studentStartedExam($data)
    {
        $this->db->trans_start();
        $this->db->insert('student_exam_attempt', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }


    


    function saveExamAnswer($data)
    {
        $this->db->select('*');
        $this->db->from('user_exam_details');
        $this->db->where('id_question', $data['id_question']);
        $this->db->where('id_userexam', $data['id_userexam']);
        $answer_id = $this->db->get()->row_array();
        $this->db->trans_start();
        if (!empty($answer_id)) {
            $this->db->where('id', $answer_id['Id']);
            $result = $this->db->update('user_exam_details', $data);
        } else {
            $this->db->insert('user_exam_details', $data);
            $result = $this->db->insert_id();
        }
        $this->db->trans_complete();
        return $result;
    }

    function studentExamClosed($data,$id) {

               $this->db->trans_start();

        $this->db->update('student_exam_attempt', $data);  
        $this->db->where('id_student', $id);
                $this->db->trans_complete();

        return 1;


    }
    function endExam($current_exam_id)
    {
        $this->db->trans_start();

        $this->db->select('count(valid) as marks');
        $this->db->from('user_exam_details');
        $this->db->where('id_userexam', $current_exam_id);
        $result = $this->db->get()->row_array();
        $marks = $result['marks'];

        $this->db->where('id', $current_exam_id);
        $data = array(
            'correct' => $marks,
            'status'  => 1
        );
        $this->db->update('user_exams', $data)              ;

        $this->db->trans_complete();
        return ;
    }
    function failExam($current_exam_id)
    {
        $this->db->trans_start();

        $this->db->select('count(valid) as marks');
        $this->db->from('user_exam_details');
        $this->db->where('id_userexam', $current_exam_id);
        $result = $this->db->get()->row_array();
        $marks = $result['marks'];

        $this->db->where('id', $current_exam_id);
        $data = array(
            'correct' => $marks,
            'status'  => 2
        );
        $this->db->update('user_exams', $data)              ;

        $this->db->trans_complete();
        return ;
    }

    function editExamSet($data, $id)
    {
        $this->db->where('id', $id);
        $result = $this->db->update('exam_set', $data);
        return $result;
    }

    function getCorrectAnswerById($id) {

        $this->db->select('qho.*');
        $this->db->from('question_has_option as qho');
        $this->db->join('student_question_set as a', 'a.id_question = qho.id_question');
        $this->db->where("qho.is_correct_answer='1'");
        $this->db->where('a.id', $id);
        $result = $this->db->get()->row_array();
        
                return $result;

    }

    function updateAnswerOption($data, $id)
    {
        $this->db->where('id', $id);
        $result = $this->db->update('student_question_set', $data);
        return $result;
    }

    function updateProfilePic($data,$id) {
        $this->db->where('id_student', $id);
        $result = $this->db->update('exam_student_tagging', $data);
        return $result;
    }

     function updateAnswer($data, $id)
    {
        $this->db->where('id', $id);
        $result = $this->db->update('exam_student_tagging', $data);
        return $result;
    }


    function getQuestionDetails($id, $exam_id)
    {
        $this->db->select('*');
        $this->db->from('question');
        $this->db->where('id', $id);
        $question = $this->db->get()->row_array();

        $this->db->select('*');
        $this->db->from('question_has_option');
        $this->db->where('id_question', $id);
        $question['options'] = $this->db->get()->result_array();
        $this->db->select('*');
        $this->db->from('user_exam_details');
        $this->db->where('id_question', $id);
        $this->db->where('id_userexam', $exam_id);
        $answered = $this->db->get()->row_array();
        // echo "<pre>";print_r($answered);die;

        if (!empty($answered)) {
            $question['answered'] = $answered['id_answer'];
        } else {
            $question['answered'] = 0;
        }
        return $question;
    }
    function getFirstQuestionDetails($id)
    {
        $this->db->select('id_question');
        $this->db->from('examset_questions');
        $this->db->where('examset_code', $id);
        $this->db->limit(1);
        $exam_set = $this->db->get()->row_array();

        $this->db->select('*');
        $this->db->from('question');
        $this->db->where('id', $exam_set['id_question']);
        $question = $this->db->get()->row_array();

        $this->db->select('*');
        $this->db->from('question_has_option');
        $this->db->where('id_question', $exam_set['id_question']);
        $question['options'] = $this->db->get()->result_array();
        return $question;
    }
    function getExamSetQuestions($exam_id)
    {
        $this->db->select('id_question');
        $this->db->from('examset_questions');
        $this->db->where('examset_code', $exam_id);
        $questions = $this->db->get()->result_array();
        return $questions;
    }

    function getQuestionByTos($idtos) {
        $this->db->select('*');
        $this->db->from('tos_details');
        $this->db->where('id_tos', $idtos);
        $questions = $this->db->get()->result_array();
        return $questions;
    }


    function getQuestionDetailById($id) {
        $this->db->select('*');
        $this->db->from('question');
        $this->db->where('id', $id);
        $question = $this->db->get()->row_array();
        return $question;

    }

    function getAllChildQuestions($id) {
        $this->db->select('s.*');
        $this->db->from('question as s');
        $this->db->where('s.parent_id', $id);
        $questions = $this->db->get()->result_array();
        return $questions;
    }

    function getQuestionByManualTos($idmanualtos) {
        $this->db->select('*');
        $this->db->from('manualtos_has_question');
        $this->db->where('id_manualtos', $idmanualtos);
        $questions = $this->db->get()->result_array();
        return $questions;
    }


  function getNotificationsByExamId($id) {
        $this->db->select('e.*');
        $this->db->from('exam_notification as e');
        $this->db->where('e.exam_id', $id);
        $this->db->order_by('e.id DESC');

         $query = $this->db->get();
         $result = $query->row();  
         return $result;
    }


    function studentExamAttempt($examid,$studentId) {
        $this->db->select('*');
        $this->db->from('student_exam_attempt');
        $this->db->where('id_student', $studentId);
        $this->db->where('id_exam_student_tagging', $examid);
        $this->db->order_by('id desc');
        $questions = $this->db->get()->row();
        return $questions;
    }


    function checkQuestionAssigned($student_exam_attempt) {
        $this->db->select('*');
        $this->db->from('student_question_set');
        $this->db->where('id_student_exam_attempt', $student_exam_attempt);
        $this->db->order_by('id desc');
        $questions = $this->db->get()->row();
        return $questions;
    }
    



    function getAssignedQuestions($idstudentexamattempt) {
        $this->db->select('a.*,s.id as examstudentid,s.id_answer,s.answer_text,s.question_number');
        $this->db->from('student_question_set as s');
        $this->db->where('s.id_student_exam_attempt', $idstudentexamattempt);
        $this->db->join('question as a', 'a.id = s.id_question');
        $this->db->order_by('a.question_type ASC');

        $questions = $this->db->get()->result_array();
        return $questions;
    }


     function getAssignedMockQuestions() {
        $this->db->select('a.*');
        $this->db->from('question as a');
        $this->db->limit(5);

        $questions = $this->db->get()->result_array();
        return $questions;
    }


    function getOldQuestionByAttemptId($id_student_exam_attempt) {
        $this->db->select('s.*');
        $this->db->from('student_question_set as s');
        $this->db->where('s.id_student_exam_attempt', $id_student_exam_attempt);
        $questions = $this->db->get()->result_array();
        return $questions;
    }


    function getOldExamAttempt($id_exam_student_tagging,$attemptnumber) {

         $this->db->select('*');
        $this->db->from('student_exam_attempt');
        $this->db->where('id_exam_student_tagging', $id_exam_student_tagging);
        $this->db->where('attempt_number', $attemptnumber);

        $questions = $this->db->get()->row();
        return $questions;

        
    }


    function getQuestionFromLogic($logic,$questionselected) {
        $this->db->select('*');
        $this->db->from('question');
        $this->db->where('id_course', $logic['id_course']);
        $this->db->where('id_topic', $logic['id_topic']);
        $this->db->where('id_bloom_taxonomy', $logic['id_bloom_taxonomy']);
        $this->db->where('parent_id', 0);


         $likeCriteria = "(id  not in (" . $questionselected . "))";
            $this->db->where($likeCriteria);

        $this->db->order_by('rand()');

        $this->db->limit($logic['questions_selected']);

        $questions = $this->db->get()->result_array();


        return $questions;
    }



    function getAnsweredByStudent($id_student_exam_attempt){
        $this->db->select('q.*');
        $this->db->from('question_has_option as q');
        $this->db->join('student_question_set as s', 'q.id=s.id_answer');
        $this->db->where("s.id_student_exam_attempt",$id_student_exam_attempt);

        $questions = $this->db->get()->result_array();

        return $questions;
    }

    function getAnsweredByStudentEssay($id_student_exam_attempt) {
        $this->db->select('s.*');
        $this->db->from('student_question_set as s');
        $this->db->where("s.id_student_exam_attempt",$id_student_exam_attempt);
        $this->db->where("s.answer_text !=''");

        $questions = $this->db->get()->result_array();

        return $questions;
    }


    function getCorrectAnswer($id_student_exam_attempt) {
        $this->db->select('q.*');
        $this->db->from('question_has_option as q');
        $this->db->join('student_question_set as s', 'q.id=s.id_answer');
        $this->db->where("q.is_correct_answer='1'");
        $this->db->where("s.id_student_exam_attempt",$id_student_exam_attempt);

        $questions = $this->db->get()->result_array();

        return $questions;
    }

      function getCorrectAnswerByExamSitting($id_student_exam_attempt) {
        $this->db->select('q.*');
        $this->db->from('question_has_option as q');
        $this->db->join('student_question_set as s', 'q.id=s.id_answer');
        $this->db->where("q.is_correct_answer='1'");
        $this->db->where("s.id_exam_student_tagging",$id_student_exam_attempt);

        $questions = $this->db->get()->result_array();

        return $questions;
    }

    function getTotalAnswerByExamSitting($id_student_exam_attempt) {
        $this->db->select('q.*');
        $this->db->from('question_has_option as q');
        $this->db->join('student_question_set as s', 'q.id=s.id_answer');
        $this->db->where("s.id_exam_student_tagging",$id_student_exam_attempt);
        $this->db->group_by("q.id_question");

        $questions = $this->db->get()->result_array();

        return $questions;
    }
}
