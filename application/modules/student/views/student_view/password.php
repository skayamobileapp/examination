<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
        <h1 class="h3">Add Student</h1>
        
        <a href='list' class="btn btn-link ml-auto">
            <i class="fa fa-chevron-left" aria-hidden="true"></i>
          Back
        </a>

    </div>
    
    <form id="form_main" action="" method="post">

        <div class="page-container">

          <div>
            <h4 class="form-title">Reset Student Password - <?php echo $getStudent->first_name;?></h4>
          </div>

            <div class="form-container">


                

                <div class="row">




                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Password <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                        </div>
                      </div>
                    </div>
                </div>

                <div class="row">




                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Confirm Password <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="password" class="form-control" id="confirm_password" name="confirm_password" placeholder="Password">
                        </div>
                      </div>
                    </div>
                </div>
   
                <div class="row">


                  
                <div class="button-block clearfix">
                  <div class="bttn-group">
                      <button type="submit" class="btn btn-primary">Save</button>
                        <!-- <a onclick="reloadPage()" class="btn btn-link">Clear All Fields</a> -->
                      <button onclick="reloadPage()" class="btn btn-link">Clear All Fields</button>
                  </div>

                </div> 

            </div>                                
        </div>
    </form>
</main>

<script>

    $(document).ready(function() {
        $("#form_main").validate({
            rules: {
                first_name: {
                    required: true
                },
                last_name: {
                    required: true
                },
                 id_country: {
                    required: true
                },
                 id_state: {
                    required: true
                },
                 city: {
                    required: true
                },
                 zipcode: {
                    required: true
                },
                 address: {
                    required: true
                },
                 status: {
                    required: true
                },
                id_location : {
                    required: true
                },
                 exam_type: {
                    required: true
                },
                 email: {
                    required: true
                },
                 user_name: {
                    required: true
                },
                 password: {
                    required: true
                },
                confirm_password : {
                    required: true
                },
                mobile : {
                    required: true
                },
                address : {
                    required: true
                },
                contact_person : {
                    required: true
                },
                contact_number : {
                    required: true
                },
                phone: {
                    required: true
                }
            },
            messages: {
                first_name: {
                    required: "<p class='error-text'>First Name Required</p>",
                },
                last_name: {
                    required: "<p class='error-text'>Last Name Required</p>",
                },
                id_country: {
                    required: "<p class='error-text'>Country Required</p>",
                },
                id_state: {
                    required: "<p class='error-text'>State Required</p>",
                },
                city: {
                    required: "<p class='error-text'>City Required</p>",
                },
                zipcode: {
                    required: "<p class='error-text'>Zipcode Required</p>",
                },
                address: {
                    required: "<p class='error-text'>Address Required</p>",
                },
                status: {
                    required: "<p class='error-text'>Select Status</p>",
                },
                id_location: {
                    required: "<p class='error-text'>Select Location</p>",
                },
                exam_type: {
                    required: "<p class='error-text'>Select Exam Type</p>",
                },
                email: {
                    required: "<p class='error-text'>Email Required</p>",
                },
                user_name: {
                    required: "<p class='error-text'>Username Required</p>",
                },
                password: {
                    required: "<p class='error-text'>Password Required</p>",
                },
                confirm_password: {
                    required: "<p class='error-text'>confirm password Required</p>",
                },
                mobile: {
                    required: "<p class='error-text'>Mobile No. Required</p>",
                },
                address: {
                    required: "<p class='error-text'>Address Required</p>",
                },
                contact_person: {
                    required: "<p class='error-text'>Contact Person Required</p>",
                },
                contact_number: {
                    required: "<p class='error-text'>Contact Number Required</p>",
                },
                phone: {
                    required: "<p class='error-text'>Contact Number Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
