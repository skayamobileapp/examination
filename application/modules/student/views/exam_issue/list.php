<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
  <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
    <h1 class="h3">Exam Center Issue List</h1>
  </div>
  <div class="page-container">
    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="">
              Advanced Search
              <i class="fa fa-angle-right" aria-hidden="true"></i>
              <i class="fa fa-angle-down" aria-hidden="true"></i>
            </a>
          </h4>
        </div>
    

      </div>
    </div>
    <?php
    if ($this->session->flashdata('success')) {
    ?>
      <div class="alert alert-success alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
      </div>
    <?php
    }
    if ($this->session->flashdata('error')) {
    ?>
      <div class="alert alert-danger alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Failure!</strong> <?php echo $this->session->flashdata('error'); ?>
      </div>
    <?php
    }
    ?>
    <div class="custom-table">
       <table class="table">
              <thead class="thead-light">
                <tr>
                  <th scope="col">Exam Name</th>
                  <th scope="col">Issue related to Student / Others</th>
                  <th scope="col">Issue Description</th>
                  <th scope="col">Date Reported</th>
                  <th scope="col">Issue Type</th>
                  <th scope="col">Other Issue</th>
                </tr>
              </thead>
              <tbody>
                <?php for($i=0;$i<count($getAllIssue);$i++) {?>
                <tr>
                  <td><?php echo $getAllIssue[$i]->exameventname;?></td>
                  <td><?php echo $getAllIssue[$i]->reported_by;?></td>
                  <td><?php echo $getAllIssue[$i]->issue_description;?></td>

                  <td><?php echo $getAllIssue[$i]->issue_date;?></td>
                  <td><?php echo $getAllIssue[$i]->issue_type;?></td>
                  <td><?php echo $getAllIssue[$i]->other_issue;?></td>
                 

                </tr>          
                <?php } ?>                     
              </tbody>
            </table>
    </div>
  </div>
</main>
<script>
  $('select').select2();

  function clearSearchForm() {
    window.location.reload();
  }
</script>