<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
  <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
    <h1 class="h3">Student Attendence List</h1>
  </div>
  <div class="page-container">
    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="">
              Advanced Search
              <i class="fa fa-angle-right" aria-hidden="true"></i>
              <i class="fa fa-angle-down" aria-hidden="true"></i>
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">

          <div id="collapseOne" class="panel-collapse collapse show" role="tabpanel" aria-labelledby="headingOne" aria-expanded="true" style="">
            <div class="panel-body p-2">

              <div class="row">

                <div class="col-lg-6">
                  <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Exam Event</label>
                    <div class="col-sm-9">
                      <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="<?php echo $searchParam['name'] ?>">
                    </div>
                  </div>
                </div>

                <div class="col-lg-6">
                  <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Location</label>
                    <div class="col-sm-9">
                      <select name="role" id="role" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($locationList)) {
                          foreach ($locationList as $record) {
                            $selected = '';
                            if ($record->id == $searchParam['id_location']) {
                              $selected = 'selected';
                            }
                        ?>
                            <option value="<?php echo $record->id;  ?>" <?php echo $selected;  ?>>
                              <?php echo  $record->name;  ?>
                            </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>

              </div>

              <div class="row">

                <div class="col-lg-6">
                  <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Exam Center</label>
                    <div class="col-sm-9">
                      <select name="id_exam_center" id="id_exam_center" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($examCenterList)) {
                          foreach ($examCenterList as $record) {
                            $selected = '';
                            if ($record->id == $searchParam['id_exam_center']) {
                              $selected = 'selected';
                            }
                        ?>
                            <option value="<?php echo $record->id;  ?>" <?php echo $selected;  ?>>
                              <?php echo  $record->name;  ?>
                            </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Exam Type</label>
                    <div class="col-sm-9">
                      <select name="type" id="type" class="form-control">
                        <option value="">Select</option>
                        <option value="Past"  >Past</option>
                        <option value="Upcoming" >Upcoming</option>
                       
                      </select>
                    </div>
                  </div>
                </div>

              </div>


              <hr />
              <div class="d-flex justify-content-center">
                <button type="submit" class="btn btn-primary">Search</button>
                <button type="reset" class="btn btn-link" onclick="clearSearchForm()">Clear All Fields</button>
              </div>
            </div>
          </div>

        </form>

      </div>
    </div>
    <?php
    if ($this->session->flashdata('success')) {
    ?>
      <div class="alert alert-success alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
      </div>
    <?php
    }
    if ($this->session->flashdata('error')) {
    ?>
      <div class="alert alert-danger alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Failure!</strong> <?php echo $this->session->flashdata('error'); ?>
      </div>
    <?php
    }
    ?>
    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Sl. No</th>
            <th>Event Name</th>
            <th>Location</th>
            <th>Exam Center</th>
            <th>Event Start Date</th>
            <th>Event Start Time</th>

            <th>Event End Date</th>
            <th>Event End Time</th>
            <th>Type</th>
            <th>Total Students</th>
            <!-- <th>Name In Other Language</th> -->
                        <th style="text-align: center;">View Students</th>

          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($examEventList)) {
            $i = 1;
                    $this->load->model('exam_event_model');

            foreach ($examEventList as $record) {

              $totalStuents = 0;
              $totalStudetnsObject = $this->exam_event_model->examStudentTaggingListSearch($record->id);

              $totalStuents = count($totalStudetnsObject);
          ?>
              <tr>
                <td><?php echo $i ?></td>
                <td><?php echo $record->name ?></td>
                <td><?php echo $record->location ?></td>
                <td><?php echo $record->exam_center ?></td>
                <td><?php echo date('d-m-Y', strtotime($record->exam_date)) ?></td>
                <td><?php echo date("H:i", strtotime($record->from_tm)) ?></td>

                  <td><?php echo date('d-m-Y', strtotime($record->exam_end_date)) ?></td>
                <td><?php echo date("H:i", strtotime($record->to_tm)) ?></td>


                <td><?php  if($record->type=='1') { echo "Face to Face";}
                               if($record->type=='2') { echo "Supervised Online Exam";}
                               if($record->type=='3') { echo "Online Exam";}

                                ?></td>

                <td>
                  <?php echo $totalStuents;?>
                </td>
                 

                 <td class="text-center">

                  <a href="<?php echo 'students/' . $record->id; ?>" class="btn" type="" data-toggle="tooltip" data-placement="top" title="Edit">
                    View Students
                  </a>

                </td>

               
              </tr>
          <?php
              $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
</main>
<script>
  $('select').select2();

  function clearSearchForm() {
    window.location.reload();
  }
</script>