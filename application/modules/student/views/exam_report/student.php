<form method="post" action="">
<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
  <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
    <h1 class="h3">Mark Student Attendence</h1>
       <a href='../list' class="btn btn-link ml-auto">
            <i class="fa fa-chevron-left" aria-hidden="true"></i>
          Back
        </a>

  </div>
  <div class="page-container">
   
    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Sl. No</th>
            <th>Student</th>
            <th>NRIC</th>
            <th>Membership Number</th>
            <th>Batch</th>
            <th>Token</th>
            <th>Module</th>
            <th>Event Name</th>
            <th>Exam Date</th>
            <th>Exam Started Time</th>
             <th>Attendence</th>
            <th>Reason</th>

          
           
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($examStudentTaggingList)) {
            $i = 1;
            foreach ($examStudentTaggingList as $record) {


          ?>
              <tr>
                <td><?php echo $i ?></td>
                <td><?php echo $record->student_name; ?></td>
                <td><?php echo $record->nric; ?></td>
                <td><?php echo $record->membership_number; ?></td>

                <td><?php echo $record->batch; ?></td>
                <td><?php echo $record->token; ?></td>
                                <td><?php echo $record->coursename ?></td>
                                <td><?php echo $record->examname ?></td>

                <td><?php echo date('d-m-Y', strtotime($record->exam_date));?></td>
                <td>
  <?php if($record->attendence_status=='1') { 
                     echo date('d-m-Y H:i:s', strtotime($record->exam_started_datetime));
                  }  else {
                    echo "N/A";

                   } ?> 
                  


                </td>



           <?php  $currentDate = date('Y-m-d');

            if(date('Y-m-d', strtotime($record->exam_date)) > $currentDate) { ?>

               <td colspan='2'>Not applicable</td>

            <?php } else { ?>

               <td>
                  <?php if($record->attendence_status=='1') {?>
                      Present
                  <?php }  else if($record->attendence_status=='2') {?>
                      Absent
                  <?php } else if($record->attendence_status=='3') {?>
                      Absent With Valid Reason
                  <?php }?> 

                   <?php if($record->attendence_status=='') {?>
                      <input type='radio' name='attendencestatus[<?php echo $record->id;?>][]' value='2'>Absent <br/>
                      <input type='radio' name='attendencestatus[<?php echo $record->id;?>][]' value='3'>Absent with Reason <br/>
                      <input type='hidden' name='attendencelist[]' value="<?php echo $record->id;?>"/>
                  <?php } ?> 
                </td>
                <td>
                  <?php if($record->attendence_status=='') {?>
                                       <input type='text' name='attendencereason[<?php echo $record->id;?>][]' value=""/>

                      <?php }  else {?> 
                        <?php echo $record->attendence_status_reason;?>
                 <?php } ?> 
                </td>

            <?php }  ?> 



               

              </tr>
          <?php
              $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>
      <div class="button-block clearfix">
                  <div class="bttn-group">
                      <button type="submit" class="btn btn-primary">Save</button>
                        <!-- <a onclick="reloadPage()" class="btn btn-link">Clear All Fields</a> -->
                      <button onclick="reloadPage()" class="btn btn-link">Clear All Fields</button>
                  </div>

                </div> 

  </div>
</main>
</form>
<script>
  $('select').select2();

  function clearSearchForm() {
    window.location.reload();
  }
</script>