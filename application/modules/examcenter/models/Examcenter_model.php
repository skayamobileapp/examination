<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Examcenter_model extends CI_Model
{
    function getAllExamsByExamCenterId($id,$date)
    {
        $this->db->select('e.*,es.duration,c.name as coursename,c.code as coursecode');
        $this->db->from('exam_event as e');
        $this->db->join('examset as es', 'e.id_exam_set=es.id');
        $this->db->join('course as c', 'e.id_course=c.id','left');
        $this->db->where('e.id_exam_center', $id);
        $this->db->where('e.exam_date >=', $date);

         $query = $this->db->get();


         $result = $query->result();  
         return $result;
    }

       function editExamCenter($data, $id)
    {
        $this->db->where('id', $id);
        $result = $this->db->update('exam_center', $data);
        return $result;
    }




    function getAllExamsByExamCenterIdIssues($id)
    {
        $this->db->select('e.*,es.duration,c.name as coursename,c.code as coursecode');
        $this->db->from('exam_event as e');
        $this->db->join('examset as es', 'e.id_exam_set=es.id');
        $this->db->join('course as c', 'e.id_course=c.id','left');
        $this->db->where('e.id_exam_center', $id);

         $query = $this->db->get();


         $result = $query->result();  
         return $result;
    }


    function getIssueDetailsById($id) {
        $this->db->select('s.*');
        $this->db->from('issue as s');
        $this->db->where('s.id', $id);

         $query = $this->db->get();


         $result = $query->row();  
         return $result;
    }



    function getAllStudentExamsByExamCenterId($id)
    {
        $this->db->select('s.*');
        $this->db->from('exam_event as e');
        $this->db->join('exam_student_tagging as est', 'est.id_exam_event=e.id');
        $this->db->join('student as s', 'est.id_student=s.id');
        $this->db->where('e.id_exam_center', $id);

         $query = $this->db->get();


         $result = $query->result();  
         return $result;
    }


 function updateissues($datas,$id) {

         $this->db->where('id', $id);
        $this->db->update('issue', $datas);  


    }



    function addissues($datas) {
        $this->db->trans_start();
        $this->db->insert('issue', $datas);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }


    function getIssuebyExamCenter($idcenter){
         $this->db->select('a.*,ee.name as exameventname,ee.exam_date,c.name as coursename');
        $this->db->from('issue as a ');
        $this->db->join('exam_event as ee', 'a.id_exam_event=ee.id');
        $this->db->join('examset as es', 'ee.id_exam_set=es.id');
        $this->db->join('course as c', 'ee.id_course=c.id','left');
        $this->db->where('a.id_exam_center', $idcenter);

        $this->db->order_by("a.id", "DESC");
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }


     function getChatData($idstudent){
         $this->db->select('*');
        $this->db->from('chat');
        $this->db->where('student_id', $idstudent);
        $this->db->order_by("id", "ASC");
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }
    


      function getchat($studentId, $procotr){
        $proctor ='Student';
        $status = '0';
        $this->db->select('a.*');
        $this->db->from('chat as a');
        $this->db->where('a.center_id', $procotr);
        $this->db->where('a.student_id', $studentId);
        $this->db->where('a.request_from', 'Student');
        $this->db->where('a.status_message', $status);
        $query = $this->db->get();
        $result = $query->row();

        return $result;
    }


      function getchatnofication($procotr){
        $proctor ='Student';
        $status = '0';
        $this->db->select('a.*,s.full_name');
        $this->db->from('chat as a');
        $this->db->join('student as s', 'a.student_id=s.id');
        $this->db->where('a.center_id', $procotr);
        $this->db->where('a.request_from', 'Student');
        $this->db->where('a.status_message', $status);
        $query = $this->db->get();
        $result = $query->row();

        return $result;
    }



function numberofchats($idstudent){
    $request_from='Student';
    $status_message = '0';
     $this->db->select('e.*');
        $this->db->from('chat as e');
        $this->db->where('e.student_id', $idstudent);
        $this->db->where('e.request_from', $request_from);
        $this->db->where('e.status_message', $status_message);

         $query = $this->db->get();


         $result = $query->result();  
         return $result;
}

function numberofchatsbyCenterId($idstudent){
    $request_from='Student';
    $status_message = '0';
     $this->db->select('e.*,s.full_name');
        $this->db->from('chat as e');
        $this->db->join('student as s', 'e.student_id=s.id');
        $this->db->where('e.center_id', $idstudent);
        $this->db->where('e.request_from', $request_from);
        $this->db->where('e.status_message', $status_message);

         $query = $this->db->get();


         $result = $query->result();  
         return $result;
}

     function insertChat($data) {
        $this->db->trans_start();
        $this->db->insert('chat', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }


    function updateStudent($idstudent) {

         $this->db->where('student_id', $idstudent);
        $data = array(
            'status_message' => 1
        );
        $this->db->update('chat', $data);  


    }
    

   function getAllChats($id)
   {
        $this->db->select('e.*');
        $this->db->from('chat as e');
        $this->db->where('e.student_id  ', $id);
        $this->db->order_by('e.id ASC');
         $query = $this->db->get();
         $result = $query->result(); 
         return $result;
   }
    

    
    function getLasttimeAnswered($studentId){
        $this->db->select('*');
        $this->db->from('student_question_set');
        $this->db->where('id_exam_student_tagging', $studentId);
        $this->db->order_by('datetime desc');
        $questions = $this->db->get()->row();
        return $questions;
    }


        function removeExamLock($id) {
                $this->db->where('id', $id);
        $data = array(
            'exam_lock' => 0
        );
        $this->db->update('exam_student_tagging', $data);  

        return;
    }



     function addExamLock($data)
    {
        $this->db->trans_start();
        $this->db->insert('exam_student_tagging_lock', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getCountOfStudents($id) {
          $this->db->select('s.*');
        $this->db->from('exam_student_tagging as s');
        $this->db->where('s.id_exam_event', $id);

         $query = $this->db->get();

         $result = $query->result();  
         return $result;
    }


    function getAllExamsListByExamCenterIdandDate($id,$type)
    {
              $examdate = date('Y-m-d');

        $this->db->select('e.*,en.name as examname,es.duration');
        $this->db->from('exam_event as e');
        $this->db->join('exam_name as en', 'e.id_exam_name=en.id');
        $this->db->join('examset as es', 'e.id_exam_set=es.id');
        $this->db->where('e.id_exam_center', $id);

        if($type=='1') {
            $this->db->where('date(e.exam_date) >=', $examdate);
        } else  if($type=='1') {
            $this->db->where('date(e.exam_date) <', $examdate);

        }

         $query = $this->db->get();

         $result = $query->result();  
         return $result;
    }



    function getCountOfmarkedQuestion($id,$type) {
        $this->db->select('sqs.id');
        $this->db->from('student_question_set as sqs');
        $this->db->join('question as q', 'sqs.id_question = q.id');
        $this->db->where('sqs.id_exam_student_tagging',$id);
        $this->db->where('q.question_type', $type);
        $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getAttemptCountOfmarkedQuestion($id,$type) {
        $this->db->select('sqs.id,sqs.speed_test');
        $this->db->from('student_question_set as sqs');
        $this->db->join('question as q', 'sqs.id_question = q.id');
        $this->db->where('sqs.id_exam_student_tagging',$id);
       
        $this->db->where("sqs.datetime!=''");
         
        $this->db->where('q.question_type', $type);
        $this->db->order_by("sqs.datetime", "DESC");

        $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }


    function getExamDetailsById($id) {
        $this->db->select('e.*,es.duration,c.name as coursename,c.code as coursecode');
        $this->db->from('exam_event as e');
        $this->db->join('examset as es', 'e.id_exam_set=es.id');
        $this->db->join('course as c', 'e.id_course=c.id','left');


        $this->db->where('e.id', $id);

         $query = $this->db->get();

         $result = $query->row();  
         return $result;
    }


    function getAllStudentsByExamEventId($id) {
     $this->db->select('s.*,e.id as examstudentaggingid,e.attendence_status,e.extra_time,e.ip as exam_ip,e.mac_id as exam_mac_id,e.location as exam_location,e.image,e.internet_speed,e.ip,st.name as statename,ct.name as cityname,e.exam_lock,e.exam_status,e.extra_two_time,e.extra_two_time_status,e.bulk_extra_time_two,e.bulk_extra_time_two_status,e.extra_time,e.extra_time_reason_one,e.extra_time_reason_two,e.bulk_extra_time,e.extra_time_one_updated_time,e.extra_time_two_updated_time,e.bulk_extra_time_one_updated_time,e.bulk_extra_time_two_updated_time');
        $this->db->from('student as s');
        $this->db->join('exam_student_tagging as e', 'e.id_student=s.id');
        $this->db->join('chat as c', 'c.student_id=s.id','left');
        $this->db->join('state as st', 'e.reg_state=st.id','left');
        $this->db->join('city as ct', 'e.reg_city=ct.id','left');
        $this->db->where('e.id_exam_event', $id);
        $this->db->order_by("s.id", "DESC");
        $this->db->group_by("s.id");

         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }


    function getAllStudentsByExamEventIdOrderbyChat($id){
         $this->db->select('s.*,e.id as examstudentaggingid,e.attendence_status,e.extra_time,e.ip as exam_ip,e.mac_id as exam_mac_id,e.location as exam_location,e.image,e.internet_speed,e.ip,st.name as statename,ct.name as cityname,e.exam_lock,e.exam_status');
        $this->db->from('student as s');
        $this->db->join('exam_student_tagging as e', 'e.id_student=s.id');
        $this->db->join('chat as c', 'c.student_id=s.id','left');
        $this->db->join('state as st', 'e.reg_state=st.id','left');
        $this->db->join('city as ct', 'e.reg_city=ct.id','left');
        $this->db->where('e.id_exam_event', $id);
        $this->db->order_by("c.date_time", "DESC");
        $this->db->order_by("s.id", "DESC");
        $this->db->group_by("s.id");

         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }

    function updateExamAttendence($data,$examstudentaggingid) {
        $this->db->where('id =', $examstudentaggingid);
        $this->db->update('exam_student_tagging', $data);
        return TRUE;
    }


    function getstudentDetailsByExamsittingId($id) {
        $this->db->select('e.*');
        $this->db->from('exam_student_tagging as e');
        $this->db->where('e.id', $id);

         $query = $this->db->get();
         $result = $query->row();  
         return $result;
    }

    function updateExtratimeForAllStudents($data,$id) {
                $this->db->where('id_exam_event =', $id);
        $this->db->update('exam_student_tagging', $data);
        return TRUE;

    }

    function checkexamStarted($id,$date) {
        $this->db->select('e.*');
        $this->db->from('exam_center_start_exam as e');
        $this->db->where('e.id_exam_event', $id);
        $this->db->where('date(e.exam_start_time)', $date);

         $query = $this->db->get();
         $result = $query->row();  
         return $result;
    }

    function getNotificationsByExamId($id) {
        $this->db->select('e.*');
        $this->db->from('exam_notification as e');
        $this->db->where('e.exam_id', $id);
        $this->db->order_by('e.id DESC');

         $query = $this->db->get();
         $result = $query->row();  
         return $result;
    }


    function getAllNotificationsByExamId($id) {
        $this->db->select('e.*');
        $this->db->from('exam_notification as e');
        $this->db->where('e.exam_id', $id);
        $this->db->order_by('e.id DESC');

         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }


    function addExamStart($data) {
         $this->db->trans_start();
        $this->db->insert('exam_center_start_exam', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function addnotification($data) {
         $this->db->trans_start();
        $this->db->insert('exam_notification', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    
}

