<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Examcenter extends BaseController
{
    public function __construct()
    {
        // echo date('d-m-Y H:i');
        error_reporting(0);
        parent::__construct();
        $this->load->model('examcenter_model');


                $this->examCenterID = $this->session->id_exam_center;

                $this->examCenterLoginName = $this->session->contact_person;
    }

    function instructions($id=NULL)
    {
              $data['asdf'] = "asfsd";

           
            $this->loadViews("exam_center/instructions", $this->global, $data, NULL);
        
    }

    function issue($id = NULL){

          $currentDate = date('Y-m-d');
      $data['todaysExamDetails'] = $this->examcenter_model->getAllExamsByExamCenterIdIssues($this->examCenterID);
      $data['studentList'] = $this->examcenter_model->getAllStudentExamsByExamCenterId($this->examCenterID);

         if($_POST) {


            $dataissue['id_exam_event'] = $_POST['id_exam_event'];
            $dataissue['reported_by'] = $_POST['reported_by'];
            $dataissue['issue_description'] = $_POST['description'];
            $dataissue['issue_type'] = $_POST['issue_type'];
            $dataissue['issue_date'] = $_POST['issue_date'];
            $dataissue['id_exam_center'] = $this->examCenterID;
            $dataissue['created_date'] = date('Y-m-d');
            if($id) {
            $this->examcenter_model->updateissues($dataissue,$id);

            } else {
            $this->examcenter_model->addissues($dataissue);
}

         }

         if($id) {
                  $data['getParticularIssue'] = $this->examcenter_model->getIssueDetailsById($id);

         } else {
            $data['getParticularIssue'] = array();
         }

      $data['getAllIssue'] = $this->examcenter_model->getIssuebyExamCenter($this->examCenterID);

       
           
            $this->loadViews("exam_center/issue", $this->global, $data, NULL);

    }


    function examlist(){
        $futureexamListArray = $this->examcenter_model->getAllExamsListByExamCenterIdandDate($this->examCenterID,'1');
        $pastexamListArray = $this->examcenter_model->getAllExamsListByExamCenterIdandDate($this->examCenterID,'2');

$data['futureexamList'] = $futureexamListArray;
$data['pastexamList'] = $pastexamListArray;

           
            $this->loadViews("exam_center/exam-details", $this->global, $data, NULL);

    }

     function studentlist(){

        $idexamid = $_POST['id'];
        $todaysExamDetails = $this->examcenter_model->getExamDetailsById($idexamid);


  

                        $data['currentexam'] = $todaysExamDetails;


      $studentList = $this->examcenter_model->getAllStudentsByExamEventIdOrderbyChat($todaysExamDetails->id);

                    $data['studentList'] = $studentList;

           
           
    $table='';
                        for($l=0;$l<count($studentList);$l++) { 
                          $idstudent = $studentList[$l]->id; 
                          $nric = $studentList[$l]->nric;
                          $full_name = $studentList[$l]->full_name;
                          $messageunread = $this->examcenter_model->numberofchats($idstudent);


                          $table.="<a href='#' class='list-group-item list-group-item-action' onclick='getdata(\"$idstudent\",\"$full_name\")' id='std\"$idstudent\"' name='std[]'>$full_name"; 

                          if(count($messageunread)>0){
                            $cnt = count($messageunread);
                          $table.="<span class='badge badge-info'>$cnt</span>";
                         } 

                          $table.="<br/>$nric</a>";

                       } 
       echo  $table;

    }

    function dashboardnew(){
      $currentDate = date('Y-m-d');
      $todaysExamDetails = $this->examcenter_model->getAllExamsByExamCenterId($this->examCenterID,$currentDate);

      

                  $data['examList'] = $todaysExamDetails;

            $this->loadViews("exam_center/dashboardnew", $this->global, $data, NULL);

    }

     function changepwd(){

         if($_POST) {

         $sentpwd = $this->security->xss_clean($this->input->post('password'));


                  $datapwd = array(
                    'password' => md5($sentpwd)
                );

                $result = $this->examcenter_model->editExamCenter($datapwd,$this->examCenterID);
                echo "<script>alert('Password has been updated Successfully')</script>";
                echo "<script>parent.location='/examcenter/examcenter/dashboardnew'</script>";
                exit;
         }
     

            $this->loadViews("exam_center/changepwd", $this->global, $data, NULL);

    }

     function dashboard($idexamid){
      $currentDate = date('Y-m-d');
      $todaysExamDetails = $this->examcenter_model->getExamDetailsById($idexamid);



      $studentsList = $this->examcenter_model->getAllStudentsByExamEventId($todaysExamDetails->id);

                            $messageunread = $this->examcenter_model->numberofchatsbyCenterId($this->examCenterID);


                        $data['currentexam'] = $todaysExamDetails;
                  $data['studentList'] = $studentsList;
                  $data['chatid'] = $idexamid;
                  $data['messageunread'] = $messageunread;

            $this->loadViews("exam_center/dashboard", $this->global, $data, NULL);

    }


    function checkchat(){
          $user_id = $idstudent;
            $examchat = $this->examcenter_model->getchatnofication($this->examCenterID);
             if($examchat) {
            echo  $examchat->chat.' ('.$examchat->full_name.' )';

             } else {
                echo  0;
             }

             exit;
    }




      function chatdisplay($idstudent){
        $user_id = $idstudent;
        $chatList = $this->examcenter_model->getChatData($user_id);


        $table="";

        $table.="<ol>";

        for($i=0;$i<count($chatList);$i++){
             $class='visitor';
            if($chatList[$i]->request_from=='Student') {
            $class='chatbot';
        }
            $message = $chatList[$i]->chat;
            $datetime = date('d-m-Y H:i:s',strtotime($chatList[$i]->date_time));
              $table.="<li class='$class'>
                <div class='msg'>
                  <div>
                    $message
                  </div>
                  <div class='time'>$datetime</div>
                </div>
              </li>";

       } 
            $table.="</ol>";
            echo $table;

    }

    function chatinsert(){

        $data['chat'] = $_POST['chat'];
        $data['student_id'] = $_POST['id_student'];
        $data['date_time'] = date('Y-m-d H:i:s');
        $data['center_id'] = $this->examCenterID;
        $data['request_from'] ='Proctor';
        $lastquestiontime = $this->examcenter_model->insertChat($data);
        echo "1";
        return;


    }


    function unreadmessage($idexamid){
        $currentDate = date('Y-m-d');
      $todaysExamDetails = $this->examcenter_model->getExamDetailsById($idexamid);




                            $messageunread = $this->examcenter_model->numberofchatsbyCenterId($this->examCenterID);

                 echo  $messageunread = json_encode($messageunread);

            return $messageunread;
    }

    function getstudentlist(){

    }

    function getmessage(){


        $idstudent = $_POST['id'];

        $chatList = $this->examcenter_model->getAllChats($idstudent);

        $this->examcenter_model->updateStudent($idstudent);


        $table="";

        $table.="<ol>";

        for($i=0;$i<count($chatList);$i++){
             $class='visitor';
            if($chatList[$i]->request_from=='Student') {
            $class='chatbot';
        }
            $message = $chatList[$i]->chat;
            $datetime = date('d-m-Y H:i:s',strtotime($chatList[$i]->date_time));
              $table.="<li class='$class'>
                <div class='msg'>
                  <div>
                    $message
                  </div>
                  <div class='time'>$datetime</div>
                </div>
              </li>";

       } 
            $table.="</ol>";



        echo $table;


    }


    function getmessageunit(){


        $idstudent = $_POST['id'];

        $chatList = $this->examcenter_model->getAllChats($idstudent);



        $table="";

        $table.="<ol>";

        for($i=0;$i<count($chatList);$i++){
             $class='visitor';
            if($chatList[$i]->request_from=='Student') {
            $class='chatbot';
        }
            $message = $chatList[$i]->chat;
            $datetime = date('d-m-Y H:i:s',strtotime($chatList[$i]->date_time));
              $table.="<li class='$class'>
                <div class='msg'>
                  <div>
                    $message
                  </div>
                  <div class='time'>$datetime</div>
                </div>
              </li>";

       } 
            $table.="</ol>";



        echo $table;


    }


    function chatsave(){

          $data['chat'] = $_POST['chat'];
        $data['student_id'] = $_POST['student_id'];
        $data['date_time'] = date('Y-m-d H:i:s');
        $data['center_id'] = $this->examCenterID;
        $data['request_from'] ='Proctor';
        $data['status_message'] = 0;
        $lastquestiontime = $this->examcenter_model->insertChat($data);
        exit;


    }


     function chat($idexamid){

        if($_POST['Publish']=='Publish'){

                $data = array(
                    'notification' => $_POST['announcements'],
                    'exam_id' => $idexamid,
                    'date_time' => date('Y-m-d H:i:s')
                );
                $checkstartedExam = $this->examcenter_model->addnotification($data);

        }
      $currentDate = date('Y-m-d');
      $todaysExamDetails = $this->examcenter_model->getExamDetailsById($idexamid);

      $notification = $this->examcenter_model->getNotificationsByExamId($idexamid);
      $notificationByExamId = $this->examcenter_model->getAllNotificationsByExamId($idexamid);

  
                        $data['currentexam'] = $todaysExamDetails;
                        $data['notification'] = $notification;
                        $data['notificationByExamId'] = $notificationByExamId;


      $studentsList = $this->examcenter_model->getAllStudentsByExamEventIdOrderbyChat($todaysExamDetails->id);

                    $data['studentList'] = $studentsList;
                    $data['idexamid'] = $idexamid;


            $this->loadViews("exam_center/chat", $this->global, $data, NULL);

    }

    function updateattendenceforstudent($examtagid,$status) {
       $data['attendence_status'] = $status;
       $tosdetails = $this->examcenter_model->updateExamAttendence($data,$examtagid);
       return 1;  
    }


    function updateextratime($examtagid,$status) {
       $data['extra_time'] = $status;
       $data['extra_time_one_updated_time'] = date('Y-m-d H:i:s');

       $tosdetails = $this->examcenter_model->updateExamAttendence($data,$examtagid);



$studentIdArray = $this->examcenter_model->getstudentDetailsByExamsittingId($examtagid);
          $datas['chat'] = "Extra time of ".$status." min has added";
        $datas['student_id'] = $studentIdArray->id_student;
        $datas['date_time'] = date('Y-m-d H:i:s');
        $datas['center_id'] = $this->examCenterID;
        $datas['request_from'] ='Proctor';
        $datas['status_message'] = 0;
        $this->examcenter_model->insertChat($datas);

       return 1;  
    }

    function updateextratimetwo($examtagid,$status) {
       $data['extra_two_time'] = $status;
       $data['extra_time_updated_status'] = 0;
       $data['extra_time_two_updated_time'] = date('Y-m-d H:i:s');

       $tosdetails = $this->examcenter_model->updateExamAttendence($data,$examtagid);



$studentIdArray = $this->examcenter_model->getstudentDetailsByExamsittingId($examtagid);
          $datas['chat'] = "Extra time of ".$status." min has added";
        $datas['student_id'] = $studentIdArray->id_student;
        $datas['date_time'] = date('Y-m-d H:i:s');
        $datas['center_id'] = $this->examCenterID;
        $datas['request_from'] ='Proctor';
        $datas['status_message'] = 0;
        $this->examcenter_model->insertChat($datas);

       return 1;  
    }



function updateextratimeforalltwo($status,$exameventId,$reason) {
      $currentDate = date('Y-m-d');
            $todaysExamDetails = $this->examcenter_model->getAllExamsByExamCenterId($this->examCenterID,$currentDate);

       $checkstartedExam = $this->examcenter_model->checkexamStarted($todaysExamDetails[0]->id,$currentDate);


        $getStudentList = $this->examcenter_model->getAllStudentsByExamEventId($exameventId);

        for($j=0;$j<count($getStudentList);$j++) {
            $data = array();

            $examstudentaggingid = $getStudentList[$j]->examstudentaggingid;
            $data['extra_time_reason_two'] = urldecode($reason);
            $data['bulk_extra_time_two'] =  $status;
       $data['bulk_extra_time_two_updated_time'] = date('Y-m-d H:i:s');

            $tosdetails = $this->examcenter_model->updateExamAttendence($data,$examstudentaggingid);



            $studentIdArray = $this->examcenter_model->getstudentDetailsByExamsittingId($examstudentaggingid);
          $datas['chat'] = "Extra time of ".$status." min has added";
            $datas['student_id'] = $studentIdArray->id_student;
            $datas['date_time'] = date('Y-m-d H:i:s');
            $datas['center_id'] = $this->examCenterID;
            $datas['request_from'] ='Proctor';
            $datas['status_message'] = 0;
            $this->examcenter_model->insertChat($datas);


        }
                          
       return 1;  
    }


    function updateextratimeforall($status,$exameventId,$reason) {
      $currentDate = date('Y-m-d');
            $todaysExamDetails = $this->examcenter_model->getAllExamsByExamCenterId($this->examCenterID,$currentDate);

       $checkstartedExam = $this->examcenter_model->checkexamStarted($todaysExamDetails[0]->id,$currentDate);


        $getStudentList = $this->examcenter_model->getAllStudentsByExamEventId($exameventId);

        for($j=0;$j<count($getStudentList);$j++) {
            $data = array();

            $examstudentaggingid = $getStudentList[$j]->examstudentaggingid;
            $data['extra_time_reason_one'] = urldecode($reason);
            $data['bulk_extra_time'] =  $status;
       $data['bulk_extra_time_one_updated_time'] = date('Y-m-d H:i:s');

            $tosdetails = $this->examcenter_model->updateExamAttendence($data,$examstudentaggingid);



            $studentIdArray = $this->examcenter_model->getstudentDetailsByExamsittingId($examstudentaggingid);
          $datas['chat'] = "Extra time of ".$status." min has added";
            $datas['student_id'] = $studentIdArray->id_student;
            $datas['date_time'] = date('Y-m-d H:i:s');
            $datas['center_id'] = $this->examCenterID;
            $datas['request_from'] ='Proctor';
            $datas['status_message'] = 0;
            $this->examcenter_model->insertChat($datas);


        }
                          
       return 1;  
    }


    function removelock($id) {

    $checkstartedExam = $this->examcenter_model->removeExamLock($id);

    $data = array(
        'id_exam_student_tagging' => $id,
        'exam_lock' => 1,
        'date_time' => date('Y-m-d H:i:s')
    );
    $checkstartedExam = $this->examcenter_model->addExamLock($data);


    $studentIdArray = $this->examcenter_model->getstudentDetailsByExamsittingId($id);
          $datas['chat'] = "Lock has been removed start the exam";
        $datas['student_id'] = $studentIdArray->id_student;
        $datas['date_time'] = date('Y-m-d H:i:s');
        $datas['center_id'] = $this->examCenterID;
        $datas['request_from'] ='Proctor';
        $datas['status_message'] = 0;
        $this->examcenter_model->insertChat($datas);




    }


    function checkexamstart($examEventId) {
      $currentDate = date('Y-m-d');
      
      $checkstartedExam = $this->examcenter_model->checkexamStarted($examEventId,$currentDate);
      if($checkstartedExam) {
        echo "1";
      } else {
        echo "0";
      }
      exit;

    }


    function examstart($examEventId) {
      $currentDate = date('Y-m-d');
       $data = array(
        'id_exam_event' => $examEventId,
        'exam_start_time' => date('Y-m-d H:i:s'),
        'exam_status' => 1,
        'id_exam_center' => 0
       );
      $checkstartedExam = $this->examcenter_model->addExamStart($data);

    }


     function logout()
    {
        $this->session->sess_destroy ();    
        redirect('examcenterLogin');
    }

   
}
