<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Examcenter extends BaseController
{
    public function __construct()
    {
        error_reporting(0);
        parent::__construct();
        $this->load->model('examcenter_model');


                $this->examCenterID = $this->session->id_exam_center;

                $this->examCenterLoginName = $this->session->contact_person;
    }

    function instructions($id=NULL)
    {
              $data['asdf'] = "asfsd";

           
            $this->loadViews("exam_center/instructions", $this->global, $data, NULL);
        
    }

    function issue(){
                    $data['asdf'] = "asfsd";

           
            $this->loadViews("exam_center/issue", $this->global, $data, NULL);

    }


    function examlist(){
        $futureexamListArray = $this->examcenter_model->getAllExamsListByExamCenterIdandDate($this->examCenterID,'1');
        $pastexamListArray = $this->examcenter_model->getAllExamsListByExamCenterIdandDate($this->examCenterID,'2');

$data['futureexamList'] = $futureexamListArray;
$data['pastexamList'] = $pastexamListArray;

           
            $this->loadViews("exam_center/exam-details", $this->global, $data, NULL);

    }

     function studentlist(){
                    $data['asdf'] = "asfsd";

           
            $this->loadViews("exam_center/studentlist", $this->global, $data, NULL);

    }

    function dashboardnew(){
      $currentDate = date('Y-m-d');
      $todaysExamDetails = $this->examcenter_model->getAllExamsByExamCenterId($this->examCenterID,$currentDate);

      

                  $data['examList'] = $todaysExamDetails;

            $this->loadViews("exam_center/dashboardnew", $this->global, $data, NULL);

    }

     function dashboard($idexamid){
      $currentDate = date('Y-m-d');
      $todaysExamDetails = $this->examcenter_model->getExamDetailsById($idexamid);




      $studentsList = $this->examcenter_model->getAllStudentsByExamEventId($todaysExamDetails->id);

  

                        $data['currentexam'] = $todaysExamDetails;
                  $data['studentList'] = $studentsList;

            $this->loadViews("exam_center/dashboard", $this->global, $data, NULL);

    }

    function updateattendenceforstudent($examtagid,$status) {
       $data['attendence_status'] = $status;
       $tosdetails = $this->examcenter_model->updateExamAttendence($data,$examtagid);
       return 1;  
    }


    function updateextratime($examtagid,$status) {
       $data['extra_time'] = $status;
       $tosdetails = $this->examcenter_model->updateExamAttendence($data,$examtagid);
       return 1;  
    }


    function updateextratimeforall($status) {
      $currentDate = date('Y-m-d');
            $todaysExamDetails = $this->examcenter_model->getAllExamsByExamCenterId($this->examCenterID,$currentDate);
       $checkstartedExam = $this->examcenter_model->checkexamStarted($todaysExamDetails->id);
       $data['extra_time'] = $status;
       $tosdetails = $this->examcenter_model->updateExtratimeForAllStudents($data,$todaysExamDetails->id);
       return 1;  
    }


    function checkexamstart($examEventId) {
      $currentDate = date('Y-m-d');
      $checkstartedExam = $this->examcenter_model->checkexamStarted($examEventId);
      if($checkstartedExam) {
        echo "1";
      } else {
        echo "0";
      }
      exit;

    }


    function examstart($examEventId) {
      $currentDate = date('Y-m-d');
       $data = array(
        'id_exam_event' => $examEventId,
        'exam_start_time' => date('Y-m-d H:i:s'),
        'exam_status' => 1,
        'id_exam_center' => 0
       );
      $checkstartedExam = $this->examcenter_model->addExamStart($data);

    }


     function logout()
    {
        $this->session->sess_destroy ();    
        redirect('examcenterLogin');
    }

   
}
