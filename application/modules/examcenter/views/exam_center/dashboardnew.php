<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <title>Exam</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo BASE_PATH; ?>assets/onlineportal/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo BASE_PATH; ?>assets/onlineportal/css/font-awesome.min.css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css2?family=Ubuntu:wght@300;400;500;700&display=swap" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="<?php echo BASE_PATH; ?>assets/onlineportal/css/main.css" rel="stylesheet" />
  </head>
  <body>

    <nav class="navbar navbar-expand-lg navbar-light main-header">
      <div class="container">
        <a class="navbar-brand" href="#">LOGO</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample02" aria-controls="navbarsExample02" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
      
        <div class="collapse navbar-collapse" id="navbarsExample02">
          <ul class="navbar-nav admin-nav ml-auto align-items-center">

            <li class="nav-item">
              <a href="/examcenter/examcenter/dashboardnew" class="nav-link">Dashboard</a>
            </li> 
             <li class="nav-item">
              <a href="/examcenter/examcenter/issue" class="nav-link">Report Issue</a>
            </li>
             <li class="nav-item">
              <a href="/examcenter/examcenter/examlist" class="nav-link">Exam Details</a>
            </li>  
                                 
            <li class="nav-item">
              Welcome <?php echo $this->session->contact_person;?>
            </li>  
              <li class="nav-item">
              <a href="/examcenter/examcenter/changepwd" class="nav-link">Change Password</a>
            </li>                
            <li class="nav-item">
              <a href="/examcenter/logout" class="nav-link">Logout</a>
            </li>          
          </ul> 
          <br/>

        </div>
        <br/>
      </div>
    </nav> 
    
    <div class="container">
      <div class="pt-4">
        <div class="tab-content" id="myTabContent">
          <div class="tab-pane fade show active" id="studentlist" role="tabpanel" aria-labelledby="studentlist-tab">
            <h3 style="text-align: center;color: #00bcd2;">Today's Exam Details</h3>
            <table class="table">
              <thead class="thead-light">
                <tr>
                  <th scope="col">Exam Name</th>
                  <th scope="col">Day</th>                  
                  <th scope="col">Exam Date</th>
                  <th scope="col">Duration</th>
                  <th scope="col">Exam Timings</th>
                  <th scope="col">Student Count</th>
                  <th scope="col">Action</th>
                </tr>
              </thead>
              <tbody>
                <?php 
                        $this->load->model('examcenter_model');

for($i=0;$i<count($examList);$i++) { 
    $studentsList =array();
                $id = $examList[$i]->id;
      $todaysExamDetails = $this->examcenter_model->getExamDetailsById($id);
                
                      $studentsList = $this->examcenter_model->getAllStudentsByExamEventId($todaysExamDetails->id);


   $programmeName = '';
              if($examList[$i]->mqms_type=='CPIF') {
                 $programmeName = $examList[$i]->programme_level_ciif."<br/>".$examList[$i]->programme_name_ciif."<br/>".$examList[$i]->name;
              } else  {
                 $programmeName = $examList[$i]->name;
              }

          ?>
              <tr>
                <td><?php echo $programmeName ?></td>
                <td><?php echo date('l', strtotime($examList[$i]->exam_date));?></td>
                  <td><?php echo date('d-m-Y',strtotime($examList[$i]->exam_date));?></td>
                   <td><?php echo $examList[$i]->duration;?> min</td>
                  <td><?php echo $examList[$i]->from_tm .'-'.$examList[$i]->to_tm ;?></td>
                                  <td><?php echo count($studentsList);?></td>


                  <td><a href="dashboard/<?php echo $id;?>" target="_blank">View</a></td>
                  
                </tr>
                <?php } ?>
                                        
              </tbody>
            </table>
          </div>
         
        </div>
      </div>
    </div>


  </body>

</html>
