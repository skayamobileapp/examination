
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <title>Exam</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo BASE_PATH; ?>assets/onlineportal/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo BASE_PATH; ?>assets/onlineportal/css/font-awesome.min.css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css2?family=Ubuntu:wght@300;400;500;700&display=swap" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="<?php echo BASE_PATH; ?>assets/onlineportal/css/main.css" rel="stylesheet" />
  </head>
  <body>

    <nav class="navbar navbar-expand-lg navbar-light main-header">
      <div class="container">
        <a class="navbar-brand" href="#">LOGO</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample02" aria-controls="navbarsExample02" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
      
        <div class="collapse navbar-collapse" id="navbarsExample02">
          <ul class="navbar-nav admin-nav ml-auto align-items-center">

            <li class="nav-item">
              <a href="/examcenter/examcenter/dashboardnew" class="nav-link">Dashboard</a>
            </li> 
             <li class="nav-item">
              <a href="/examcenter/examcenter/issue" class="nav-link">Report Issue</a>
            </li>
             <li class="nav-item">
              <a href="/examcenter/examcenter/examlist" class="nav-link">Exam Details</a>
            </li>  
                                 
            <li class="nav-item">
              Welcome <?php echo $this->session->contact_person;?>
            </li>   
             <li class="nav-item">
              <a href="/examcenter/examcenter/changepwd" class="nav-link">Change Password</a>
            </li>         
            <li class="nav-item">
              <a href="/examcenter/logout" class="nav-link">Logout</a>
            </li>          
          </ul> 
          <br/>

        </div>
        <br/>
      </div>
    </nav> 
    
    <form method="POST" action="">
    <div class="container">
      <div class="pt-4">
        <h3 style="text-align: center;color: #00bcd2;">Issue Details</h3>
        <div class="row">
          <div class="col-md-6">
             <label>Exam Name</label>
              <select name="id_exam_event" class="form-control" id="id_exam_event">
                   <option value="">Select</option>
                 <?php for ($i=0;$i<count($todaysExamDetails);$i++) {?>
                  <option value="<?php echo $todaysExamDetails[$i]->id;?>"  
                    <?php if($getParticularIssue->id_exam_event==$todaysExamDetails[$i]->id)
                       { echo "selected=selected";} ?> >
                    <?php echo $todaysExamDetails[$i]->name;?> -  <?php echo date('d-m-Y',strtotime($todaysExamDetails[$i]->exam_date));?>  -  <?php echo $todaysExamDetails[$i]->coursecode;?>
                  </option>
                 <?php } ?> 
                </select>
          </div>
            <div class="col-md-6">
             <label>Issue related to Student / Others</label>

               <select name="reported_by" class="form-control" id="reported_by">
                  <option value="">Select</option>
                  <option value="Others" <?php if($getParticularIssue->reported_by=='Others')
                       { echo "selected=selected";} ?> >Others</option>
                  <option value="All Students" <?php if($getParticularIssue->reported_by=='All Students')
                       { echo "selected=selected";} ?> >All Students</option>
                 <?php for ($i=0;$i<count($studentList);$i++) {?>
                  <option value="<?php echo $studentList[$i]->full_name;?>"  <?php if($getParticularIssue->reported_by==$todaysExamDetails[$i]->full_name)
                       { echo "selected=selected";} ?> 
                    <?php echo $todaysExamDetails[$i]->name;?>>
                    <?php echo $studentList[$i]->full_name;?>
                  </option>
                 <?php } ?> 
                </select>

          </div>

           <div class="col-md-6">
             <label>Date Reported</label>
              <input type='date' name="issue_date" class="form-control" id="issue_date"  value="<?php echo $getParticularIssue->issue_date;?>"/>
          </div>

           <div class="col-md-6">
             <label>Issue Type</label>

               <select name="issue_type" class="form-control" id="issue_type" onchange="showothertext()">
                  <option value="">Select</option>
                  <option value="Cheating"  <?php if($getParticularIssue->issue_type=="Cheating")
                       { echo "selected=selected";} ?> >Cheating</option>
                  <option value="Other Discipline Issue"  <?php if($getParticularIssue->issue_type=="Other Discipline Issue")
                       { echo "selected=selected";} ?> >Other Discipline Issue</option>
                  <option value="System Issue"  <?php if($getParticularIssue->issue_type=="System Issue")
                       { echo "selected=selected";} ?> >System Issue</option>
                  <option value="Internet and Connectivity Issue"  <?php if($getParticularIssue->issue_type=="Internet and Connectivity Issue")
                       { echo "selected=selected";} ?> >Internet and Connectivity Issue</option>
                  <option value="Electric Issue"  <?php if($getParticularIssue->issue_type=="Electric Issue")
                       { echo "selected=selected";} ?> >Electric Issue</option>
                  <option value="Computer Issue"  <?php if($getParticularIssue->issue_type=="Computer Issue")
                       { echo "selected=selected";} ?> >Computer Issue</option>
                  <option value="Others"  <?php if($getParticularIssue->issue_type=="Others")
                       { echo "selected=selected";} ?> >Others</option>
                
                </select>

          </div>

           <div class="col-md-6" style="display:none;" id='otherdivissue'>
             <label>Other Issue</label>
              <input type='text' name="other_issue" class="form-control" id="other_issue"  value="<?php echo $getParticularIssue->other_issue;?>"/>
          </div>
                 
        </div>
        <div class="row">
          <div class="col-md-12">
             <label>Issue Description</label>
            <textarea type="text" class="form-control mr-sm-3" name='description' style="height:200px;"><?php echo $getParticularIssue->issue_description;?></textarea>

          </div>
                 
        </div>
         <div class="row">
          <div class="col-md-12">
            <br/>
            <button type="submit" class="btn btn-primary">Save</button>

          </div>
                 
        </div>
     
     </form>
        <hr/>
         
      </div>
       <div class="tab-content" id="myTabContent">
          <div class="tab-pane fade show active" id="studentlist" role="tabpanel" aria-labelledby="studentlist-tab">
            <table class="table">
              <thead class="thead-light">
                <tr>
                  <th scope="col">Exam Name</th>
                  <th scope="col">Exam Date</th>
                  <th scope="col">Module Name</th>
                  <th scope="col">Issue related to Student / Others</th>
                  <th scope="col">Issue Description</th>
                  <th scope="col">Date Reported</th>
                  <th scope="col">Issue Type</th>
                  <th scope="col">Other Issue</th>
                  <th scope="col">Action</th>
                </tr>
              </thead>
              <tbody>
                <?php for($i=0;$i<count($getAllIssue);$i++) {?>
                <tr>
                  <td><?php echo $getAllIssue[$i]->exameventname;?></td>
                  <td><?php echo $getAllIssue[$i]->exam_date;?></td>
                  <td><?php echo $getAllIssue[$i]->coursename;?></td>
                  <td><?php echo $getAllIssue[$i]->reported_by;?></td>
                  <td><?php echo $getAllIssue[$i]->issue_description;?></td>

                  <td><?php echo date('d-m-Y',strtotime($getAllIssue[$i]->issue_date));?></td>
                  <td><?php echo $getAllIssue[$i]->issue_type;?></td>
                  <td><?php echo $getAllIssue[$i]->other_issue;?></td>
                  <td><a href="/examcenter/examcenter/issue/<?php echo $getAllIssue[$i]->id;?>">Edit</a></td>

                </tr>          
                <?php } ?>                     
              </tbody>
            </table>
          </div>
         
        </div>
    </div>
 

       <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />

  
  <script src="<?php echo BASE_PATH; ?>assets/js/jquery-1.12.4.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>

<script type="text/javascript">
   

   $('select').select2();

  function showothertext() {
    var issue_type = $("#issue_type").val();
    $("#otherdivissue").hide();
    if(issue_type=='Others') {
          $("#otherdivissue").show();
    }
  }
</script>

 </body>
</html>