
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <title>Exam</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo BASE_PATH; ?>assets/onlineportal/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo BASE_PATH; ?>assets/onlineportal/css/font-awesome.min.css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css2?family=Ubuntu:wght@300;400;500;700&display=swap" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="<?php echo BASE_PATH; ?>assets/onlineportal/css/main.css" rel="stylesheet" />
  </head>
  <body onclick="checkExamStarted()">

    <nav class="navbar navbar-expand-lg navbar-light main-header">
      <div class="container">
        <a class="navbar-brand" href="#">LOGO</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample02" aria-controls="navbarsExample02" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
      
        <div class="collapse navbar-collapse" id="navbarsExample02">
          <ul class="navbar-nav admin-nav ml-auto align-items-center">

            <li class="nav-item">
              <a href="/examcenter/examcenter/dashboardnew" class="nav-link">Dashboard</a>
            </li> 
             <li class="nav-item">
              <a href="/examcenter/examcenter/issue" class="nav-link">Report Issue</a>
            </li>
             <li class="nav-item">
              <a href="/examcenter/examcenter/examlist" class="nav-link">Exam Details</a>
            </li>  
                                 
            <li class="nav-item">
              Welcome <?php echo $this->session->contact_person;?>
            </li>            
            <li class="nav-item">
              <a href="/examcenter/logout" class="nav-link">Logout</a>
            </li>          
          </ul> 
          <br/>

        </div>
        <br/>
      </div>
    </nav> 
    
    <?php if($studentList) {?> 
    <div class="container">
      <div class="pt-4">
        <h3 style="text-align: center;color: #00bcd2;"></h3>
        <div class="row admin-stats py-3">
          <div class="col-md-4">
            <h4>Total Students - <span><?php echo count($studentList);?></span></h4>
          </div>
          <div class="col-md-4">
            <h4>Duration of Exam (Min)- <span><?php echo $currentexam->duration;?></span></h4>
          </div> 
           <div class="col-md-4">
            <h4>Start Time - <span><?php echo $currentexam->from_tm;?></span></h4>
          </div>          
        </div>
        <table class="table" border="1" width="100%">
          <thead class="thead-light">
            <tr>
              <th>SL No</th>

              <th width="20%">Student Name</th>
              <th>IP Details</th>
              <th>Location Details</th>

              <th  width="12%">MCQ</th>
              <th  width="12%">Essay</th>
              <th>Internet Speed (in KBPS)</th>
              <th>Profile PIC</th>

              <th>Attendance</th>
              <th>Exam Locked</th>
              <th width="15%">Extra Time in Min</th>
              <th>Exam Status</th>
            </tr>
            
          </thead>
          <tbody>

            <?php for($l=0;$l<count($studentList);$l++) { 

  $this->load->model('examcenter_model');

 $mcaQuestions = $this->examcenter_model->getCountOfmarkedQuestion($studentList[$l]->examstudentaggingid,1);

  $totalmcaQuestions = count($mcaQuestions);
  

   $mcaQuestionsAtempt = $this->examcenter_model->getAttemptCountOfmarkedQuestion($studentList[$l]->examstudentaggingid,1);


  $totalmcaQuestionsAtempt = count($mcaQuestionsAtempt);
$internetspeed = $mcaQuestionsAtempt[0]->speed_test;


   $essayQuestions = $this->examcenter_model->getCountOfmarkedQuestion($studentList[$l]->examstudentaggingid,2);      
  $totalessayQuestions = count($essayQuestions);

 $essayQuestionsEssay = $this->examcenter_model->getAttemptCountOfmarkedQuestion($studentList[$l]->examstudentaggingid,2);
  $totalessayQuestionsEssay = count($essayQuestionsEssay);

  $j = $l+1;


  $lastansweredPic = $this->examcenter_model->getLasttimeAnswered($studentList[$l]->examstudentaggingid);



              ?>
            <tr>
                            <td><?php echo $j;?></td>

              <td><?php echo $studentList[$l]->full_name;?> <br/> <?php echo $studentList[$l]->nric;?></td>
              <td><?php echo $studentList[$l]->ip;?></td>
              <td><?php echo $studentList[$l]->statename;?>  /   <?php echo $studentList[$l]->cityname;?></td>

              <td>  <?php echo $totalmcaQuestionsAtempt;?>  /  <?php echo $totalmcaQuestions;?></td>

              <td>  <?php echo $totalessayQuestionsEssay;?> / <?php echo $totalessayQuestions;?></td>
              <td><?php echo $internetspeed;?></td>


              <td><img src="<?php echo "/".$lastansweredPic->student_image;?>" style='height:100px;'></td>

              <td>
                    <div class="custom-control custom-radio custom-control-inline">
                      <input type="radio" id="customRadioInline<?php echo $studentList[$l]->examstudentaggingid;?>1" name="customRadioInline<?php echo $studentList[$l]->examstudentaggingid;?>" class="custom-control-input" onclick="updateAttendence(<?php echo $studentList[$l]->examstudentaggingid;?>,'1')" <?php if($studentList[$l]->attendence_status=='1') { echo "checked=checked";}?>>
                      <label class="custom-control-label" for="customRadioInline<?php echo $studentList[$l]->examstudentaggingid;?>1">Yes</label>
                    </div>
                    <div class="custom-control custom-radio custom-control-inline">
                      <input type="radio" id="customRadioInline<?php echo $studentList[$l]->examstudentaggingid;?>2" name="customRadioInline<?php echo $studentList[$l]->examstudentaggingid;?>" class="custom-control-input" onclick="updateAttendence(<?php echo $studentList[$l]->examstudentaggingid;?>,'0')" <?php if($studentList[$l]->attendence_status=='0') { echo "checked=checked";}?>>
                      <label class="custom-control-label" for="customRadioInline<?php echo $studentList[$l]->examstudentaggingid;?>2">No</label>
                    </div>
                  </td>
                    <td>

                      <?php if($studentList[$l]->exam_lock=='1') {?>

                          <p>Exam has been locked, Click the button to unlock it</p>
                           <button type="button" class="btn btn-primary" style="margin-top: 5px;" onclick="RemoveLock(<?php echo $studentList[$l]->examstudentaggingid;?>)">Remove Lock</button>


                      <?php } else { ?>


                      <?php } ?>  

             
             </td>

              <td><input type="text" class="form-control" value="<?php echo $studentList[$l]->extra_time;?>"  id="extratime<?php echo $studentList[$l]->examstudentaggingid;?>"> 

             
              <button type="button" class="btn btn-primary" style="margin-top: 5px;" onclick="updateExtratime(<?php echo $studentList[$l]->examstudentaggingid;?>)">update</button></td>
              <td>-</td>
            </tr>
          <?php } ?> 
                                       
          </tbody>
        </table>  
        <hr/>
        <div class="d-flex align-items-center mb-3">
          <button type="button" class="btn btn-primary btn-lg" id="examstartButton" style="display: none;" onclick="startExam()">Start</button>
          <h3 id='examstartdisabled' style="display: none;">Exam has been started</h3>
          <div class="ml-auto mr-2">
            <div class="form-inline">
              <label for="inlineFormInputName2" class="mr-2">Extra Time for All students</label>
              <input type="text" class="form-control mr-sm-3" id="extra_time" placeholder="">
            </div>            
          </div>
          <button type="button" class="btn btn-outline-secondary" onclick="updateextratimeforall()">Save</button>
        </div>      
      </div>
    </div>
  <?php } else { ?>
   <div class="container">
     <h3>No Exam has been scheduled for today</h3>
   </div>
  <?php } ?> 

       <script src="<?php echo BASE_PATH; ?>assets/onlineportal/js/jquery-1.12.4.min.js"></script>

  </body>
</html>

<script>

$( document ).ready(function() {
    checkExamStarted();
});

function updateAttendence(examstudentaggingid,status) {
      $.get("/examcenter/examcenter/updateattendenceforstudent/"+examstudentaggingid+"/"+status, function(data, status){
        alert("Extra time has been added");
           
            });
}


function RemoveLock(id){
   $.get("/examcenter/examcenter/removelock/"+id, function(data, status){
        alert("Lock has been removed");
               location.reload();

            });
}


function updateExtratime(id) {
  var time = $("#extratime"+id).val();
  $.get("/examcenter/examcenter/updateextratime/"+id+"/"+time, function(data, status){
                   alert("Extra time has been added");

            });
}

function updateextratimeforall(id) {
   var time = $("#extra_time").val();
  $.get("/examcenter/examcenter/updateextratimeforall/"+time, function(data, status){
                   alert("Extra time has been added");

            });
}


function startExam() {
      var exameventId = "<?php echo $currentexam->id;?>";

   $.get("/examcenter/examcenter/examstart/"+exameventId, function(data, status){
          checkExamStarted();
    });
}


function checkExamStarted() {
    // $("#examstartdisabled").hide();
    // $("#examstartButton").hide();
    var exameventId = "<?php echo $currentexam->id;?>";
    $.get("/examcenter/examcenter/checkexamstart/"+exameventId, function(data, status){
         if(data=='1') {
            $("#examstartdisabled").show();
         }
         if(data=='0') {
            $("#examstartButton").show();
         }
    });
  }

   setInterval(function() {
                  checkExamStarted();
                }, 10000); 

</script>