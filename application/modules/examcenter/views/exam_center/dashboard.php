
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <title>Exam</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo BASE_PATH; ?>assets/onlineportal/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo BASE_PATH; ?>assets/onlineportal/css/font-awesome.min.css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css2?family=Ubuntu:wght@300;400;500;700&display=swap" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="<?php echo BASE_PATH; ?>assets/onlineportal/css/main.css" rel="stylesheet" />
  </head>
  <body>

    <nav class="navbar navbar-expand-lg navbar-light main-header">
      <div class="container">
        <a class="navbar-brand" href="#">LOGO</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample02" aria-controls="navbarsExample02" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
      
        <div class="collapse navbar-collapse" id="navbarsExample02">
          <ul class="navbar-nav admin-nav ml-auto align-items-center">

            <li class="nav-item">
              <a href="/examcenter/examcenter/dashboardnew" class="nav-link">Dashboard</a>
            </li> 
             <li class="nav-item">
              <a href="/examcenter/examcenter/issue" class="nav-link">Report Issue</a>
            </li>
             <li class="nav-item">
              <a href="/examcenter/examcenter/examlist" class="nav-link">Exam Details</a>
            </li>  


                         <li class="nav-item">
              <a href="/examcenter/examcenter/chat/<?php echo $chatid;?>" class="nav-link">Chat</a>
            </li>  
                                 
            <li class="nav-item">
              Welcome <?php echo $this->session->contact_person;?>
            </li>            
            <li class="nav-item">
              <a href="/examcenter/logout" class="nav-link">Logout</a>
            </li>          
          </ul> 
          <br/>

        </div>
        <br/>
      </div>
    </nav> 
    
    <?php if($studentList) {?> 
    <div class="container">
      <div class="pt-4">
                <div class="d-flex align-items-center mb-3">
          <button type="button" class="btn btn-primary btn-lg" id="examstartButton" style="display: none;" onclick="startExam()">Start</button>

        </div>

        <div class="row">
           <div class="col-sm-8">
        <table class="table" border="1" width="100%">
          <thead class="thead-light">
            <tr>
              <th>Total Students</th>
              <th>Duration of Exam (min)</th>
              <th>Event Name</th>
              <th>Exam Date</th>
              <th>Exam Time</th>
            </tr>
          </thead>
          <tbody>
            <?php 
              $programmeName = '';
              if($currentexam->id=='1') {
                 $programmeName = "Professional Ethics Module";
              } else  {
                 $programmeName = "CPIF <br/> Intermediate";
              }
?>
            <tr>
              <td><?php echo count($studentList);?></td>
              <td><?php echo $currentexam->duration;?></td>
              <td><?php echo $programmeName;?> <br/>
                <?php echo $currentexam->coursename;?> <br/> 
                <?php echo $currentexam->coursecode;?></td>
              <td><?php echo date('d-m-Y',strtotime($currentexam->exam_date));?></td>
              <td><?php echo date('H:i a',strtotime($currentexam->from_tm));?> - <?php echo date('H:i a',strtotime($currentexam->to_tm));?></td>
            </tr>
           </tbody> 
         </table>
       </div>
        <div class="col-sm-4">
          <table class="table" border="1" width="100%">
          <thead class="thead-light">
            <tr>
              <th style="background-color: #8ecf8e;"><img src="https://uxwing.com/wp-content/themes/uxwing/download/communication-chat-call/group-chat-icon.png" style="height:50px;" /></th>
            </tr>
          </thead>
          <tbody>
             <tr>
              <td>
                <div id="chatid" style="height: 120px;color:red;"></div>
                <a href="/examcenter/examcenter/chat/<?php echo $chatid;?>" class="btn-primary">Click to Reply</a>
              </td>
            </tr>
           </tbody>
         </table>
        </div>
     </div>

               <hr/>

        <table class="table" border="1" width="100%">
          <thead class="thead-light">
            <tr>
              <th>SL No</th>

              <th width="20%">Student Name</th>
<!--               <th>IP Details</th>
              <th>Location Details</th>

 -->              <th  width="12%">MCQ</th>
              <th  width="12%">Essay</th>
              <th>Internet Speed (in KBPS)</th>
              <th>Profile PIC</th>
              <th>Last Answered PIC</th>

              <th>Attendance</th>
              <th>Exam Locked</th>
              <th width="15%">Extra Time in Min</th>
              <th>Exam Status</th>
            </tr>
            
          </thead>
          <tbody>

            <?php for($l=0;$l<count($studentList);$l++) { 

  $this->load->model('examcenter_model');

 $mcaQuestions = $this->examcenter_model->getCountOfmarkedQuestion($studentList[$l]->examstudentaggingid,1);

  $totalmcaQuestions = count($mcaQuestions);
  

   $mcaQuestionsAtempt = $this->examcenter_model->getAttemptCountOfmarkedQuestion($studentList[$l]->examstudentaggingid,1);


  $totalmcaQuestionsAtempt = count($mcaQuestionsAtempt);
$internetspeed = $mcaQuestionsAtempt[0]->speed_test;


   $essayQuestions = $this->examcenter_model->getCountOfmarkedQuestion($studentList[$l]->examstudentaggingid,2);      
  $totalessayQuestions = count($essayQuestions);

 $essayQuestionsEssay = $this->examcenter_model->getAttemptCountOfmarkedQuestion($studentList[$l]->examstudentaggingid,2);
  $totalessayQuestionsEssay = count($essayQuestionsEssay);

  $j = $l+1;


  $lastansweredPic = $this->examcenter_model->getLasttimeAnswered($studentList[$l]->examstudentaggingid);


if($studentList[$l]->exam_status=='') {
  $examstatus = 'Not -Started';
} else {
    $examstatus = $studentList[$l]->exam_status;

}

              ?>
            <tr>
                            <td><?php echo $j;?></td>

              <td><?php echo $studentList[$l]->full_name;?> <br/> <?php echo $studentList[$l]->nric;?></td>
             <!--  <td><?php echo $studentList[$l]->ip;?></td>
              <td><?php echo $studentList[$l]->statename;?>  /   <?php echo $studentList[$l]->cityname;?></td>
 -->
              <td>  <?php echo $totalmcaQuestionsAtempt;?>  /  <?php echo $totalmcaQuestions;?></td>

              <td>  <?php echo $totalessayQuestionsEssay;?> / <?php echo $totalessayQuestions;?></td>
              <td><?php echo $internetspeed;?></td>


              <td>
                        <?php if($studentList[$l]->image!='') { ?>
                <img src="/<?php echo $studentList[$l]->image;?>"  style="height: 100px;" />
                                <?php } else { ?>
                  - 
                <?php } ?> 


              <td>
                <?php if($lastansweredPic->student_image!='') { ?>
                  <img src="/<?php echo $lastansweredPic->student_image;?>"  style="height: 100px;" />
                <?php } else { ?>
                  - 
                <?php } ?> 
              </td>

              <td>
                    <div class="custom-control custom-radio custom-control-inline">
                      <input type="radio" id="customRadioInline<?php echo $studentList[$l]->examstudentaggingid;?>1" name="customRadioInline<?php echo $studentList[$l]->examstudentaggingid;?>" class="custom-control-input" onclick="updateAttendence(<?php echo $studentList[$l]->examstudentaggingid;?>,'1')" <?php if($studentList[$l]->attendence_status=='1') { echo "checked=checked";}?>>
                      <label class="custom-control-label" for="customRadioInline<?php echo $studentList[$l]->examstudentaggingid;?>1">Yes</label>
                    </div>
                    <div class="custom-control custom-radio custom-control-inline">
                      <input type="radio" id="customRadioInline<?php echo $studentList[$l]->examstudentaggingid;?>2" name="customRadioInline<?php echo $studentList[$l]->examstudentaggingid;?>" class="custom-control-input" onclick="updateAttendence(<?php echo $studentList[$l]->examstudentaggingid;?>,'0')" <?php if($studentList[$l]->attendence_status=='0') { echo "checked=checked";}?>>
                      <label class="custom-control-label" for="customRadioInline<?php echo $studentList[$l]->examstudentaggingid;?>2">No</label>
                    </div>
                  </td>
                    <td>

                      <?php if($studentList[$l]->exam_lock=='1') {?>

                          <p>Exam has been locked, Click the button to unlock it</p>
                           <button type="button" class="btn btn-primary" style="margin-top: 5px;" onclick="RemoveLock(<?php echo $studentList[$l]->examstudentaggingid;?>)">Remove Lock</button>


                      <?php } else { ?>


                      <?php } ?>  

             
             </td>

              <td>

                <input type="text" class="form-control" value="<?php echo $studentList[$l]->extra_time;?>"  <?php if($studentList[$l]->extra_time>0) { echo "readonly=readonly";} ?>id="extratime<?php echo $studentList[$l]->examstudentaggingid;?>"> 

             <?php if($studentList[$l]->extra_time>0) { ?>
               Updated at - <br/><?php echo date('H:i:s',strtotime($studentList[$l]->extra_time_one_updated_time));?>
            <?php } else { ?>
              <button type="button" class="btn btn-primary" style="margin-top: 5px;" onclick="updateExtratime(<?php echo $studentList[$l]->examstudentaggingid;?>)">update</button>
            <?php } ?> 

            <?php if($studentList[$l]->extra_time>0) { ?>
              <input type="text" class="form-control" value="<?php echo $studentList[$l]->extra_two_time;?>"  <?php if($studentList[$l]->extra_two_time>0) { echo "readonly=readonly";} ?>id="extra_two_time<?php echo $studentList[$l]->examstudentaggingid;?>"> 

             

             <?php if($studentList[$l]->extra_two_time>0) {  ?>

                           Updated at - <br/><?php echo date('H:i:s',strtotime($studentList[$l]->extra_time_two_updated_time));?>
            <?php } else { ?>

              <button type="button" class="btn btn-primary" style="margin-top: 5px;" onclick="updateExtratime2(<?php echo $studentList[$l]->examstudentaggingid;?>)">update</button>
            <?php }  } ?> 


            </td>
              <td><?php echo $examstatus;?></td>
            </tr>
          <?php } ?> 
                                       
          </tbody>
        </table>  

        <table class="table" border="1" width="100%">
          <thead class="thead-light">
            <tr>
              <th>Extra time in Minutes</th>

              <th>Reason</th> 
              <th>Date and Time</th> 
              <th>Action</th>
            </tr>
          </thead>
          <tbody>


            <tr>

                         <?php $selectedone ="";
                         if($studentList[0]->bulk_extra_time>0) {  
                            $selectedone = "disabled=disabled";
                            } ?> 

              <td><input type="text" class="form-control mr-sm-3" id="bulk_extra_time" placeholder="" <?php echo $selectedone;?> value="<?php echo $studentList[0]->bulk_extra_time;?>"></td>

              <td><input type="text" class="form-control mr-sm-3" id="extra_time_reason_one" placeholder="" <?php echo $selectedone;?> value="<?php echo $studentList[0]->extra_time_reason_one;?>"></td>

           <?php if($studentList[0]->bulk_extra_time==0) {?>
              <td></td>
              <td><button type="button btn-primary" class="btn btn-outline-secondary" onclick="updateextratimeforall()">Save</button></td>
            <?php } else { ?>

               <td><input type="text" class="form-control mr-sm-3" id="bulk_extra_time_one_updated_time" disabled="disabled" value="<?php echo date('d-m-Y H:i:s',strtotime($studentList[0]->bulk_extra_time_one_updated_time));?>"></td>
               <td></td>
            <?php } ?> 
            </tr>

           <?php if($studentList[0]->bulk_extra_time>0) {?>
             <tr>
                <?php $selectedone ="";
                         if($studentList[0]->bulk_extra_time_two>0) {  
                            $selectedtwo = "disabled=disabled";
                            } ?> 


              <td><input type="text" class="form-control mr-sm-3" id="bulk_extra_time_two" value="<?php echo $studentList[0]->bulk_extra_time_two;?>" <?php echo $selectedtwo;?>></td>

              <td><input type="text" class="form-control mr-sm-3" id="extra_time_reason_two" value="<?php echo $studentList[0]->extra_time_reason_two;?>" <?php echo $selectedtwo;?>></td>
                         <?php if($studentList[0]->bulk_extra_time_two==0) {?>

              <td><button type="button btn-primary" class="btn btn-outline-secondary" onclick="updateextratimeforalltwo()">Save</button></td>
            <?php } else { ?>

               <td><input type="text" class="form-control mr-sm-3" id="bulk_extra_time_two_updated_time" disabled="disabled" value="<?php echo date('d-m-Y H:i:s',strtotime($studentList[0]->bulk_extra_time_two_updated_time));?>"></td>
               <td></td>
            <?php } ?> 
            </tr>

          <?php } ?> 
          </tbody>
        </table>
         
        </div>      
      </div>
    </div>
  <?php } else { ?>
   <div class="container">
     <h3>No Exam has scheduled for today</h3>
   </div>
  <?php } ?> 

       <script src="<?php echo BASE_PATH; ?>assets/onlineportal/js/jquery-1.12.4.min.js"></script>
  


  </body>
</html>

 <script>

  $( document ).ready(function() {
    $.get("/examcenter/examcenter/checkchat", function(data, status){
            if(data!=0) {
              $("#chatid").html(data);

              $("#showbtnchat").show();
            }
        });
});





function updateAttendence(examstudentaggingid,status) {
      $.get("/examcenter/examcenter/updateattendenceforstudent/"+examstudentaggingid+"/"+status, function(data, status){
           
            });
}


function RemoveLock(id){
   $.get("/examcenter/examcenter/removelock/"+id, function(data, status){
        alert("Lock has been removed");
               location.reload();

            });
}


function updateExtratime(id) {
  var time = $("#extratime"+id).val();
  $.get("/examcenter/examcenter/updateextratime/"+id+"/"+time, function(data, status){
                   alert("Extra time has been added");
location.reload();
            });
}


function updateExtratime2(id) {
  var time = $("#extra_two_time"+id).val();
  $.get("/examcenter/examcenter/updateextratimetwo/"+id+"/"+time, function(data, status){
                   alert("Extra time has been added");
location.reload();
            });
}

function updateextratimeforall(id) {
   var time = $("#bulk_extra_time").val();
   var reason = $("#extra_time_reason_one").val();

   var exameventId = '<?php echo $chatid;?>';
  $.get("/examcenter/examcenter/updateextratimeforall/"+time+"/"+exameventId+"/"+reason, function(data, status){
                   alert("Extra time has been added");
location.reload();
            });
}

function updateextratimeforalltwo(id) {
   var time = $("#bulk_extra_time_two").val();
   var reason = $("#extra_time_reason_two").val();

   var exameventId = '<?php echo $chatid;?>';
  $.get("/examcenter/examcenter/updateextratimeforalltwo/"+time+"/"+exameventId+"/"+reason, function(data, status){
                   alert("Extra time has been added");
location.reload();
            });
}

function startExam() {
      var exameventId = "<?php echo $currentexam->id;?>";

   $.get("/examcenter/examcenter/examstart/"+exameventId, function(data, status){
          
    });
}

function checkExamStarted() {
    var exameventId = "<?php echo $currentexam->id;?>";
    $.get("/examcenter/examcenter/checkexamstart/"+exameventId, function(data, status){
         if(data=='1') {
            $("#examstartdisabled").show();
         }
         if(data=='0') {
            $("#examstartButton").show();
         }
    });
  }


setInterval(function() {
location.reload();
   }, 10 * 1000);

</script>