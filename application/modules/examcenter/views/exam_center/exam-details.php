
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <title>Exam</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo BASE_PATH; ?>assets/onlineportal/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo BASE_PATH; ?>assets/onlineportal/css/font-awesome.min.css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css2?family=Ubuntu:wght@300;400;500;700&display=swap" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="<?php echo BASE_PATH; ?>assets/onlineportal/css/main.css" rel="stylesheet" />
  </head>
  <body>

    <nav class="navbar navbar-expand-lg navbar-light main-header">
      <div class="container">
        <a class="navbar-brand" href="#">LOGO</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample02" aria-controls="navbarsExample02" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
      
        <div class="collapse navbar-collapse" id="navbarsExample02">
          <ul class="navbar-nav admin-nav ml-auto align-items-center">

            <li class="nav-item">
              <a href="/examcenter/examcenter/dashboardnew" class="nav-link">Dashboard</a>
            </li> 
             <li class="nav-item">
              <a href="/examcenter/examcenter/issue" class="nav-link">Report Issue</a>
            </li>
             <li class="nav-item">
              <a href="/examcenter/examcenter/examlist" class="nav-link">Exam Details</a>
            </li>  
                                 
            <li class="nav-item">
              Welcome <?php echo $this->session->contact_person;?>
            </li>    
              <li class="nav-item">
              <a href="/examcenter/examcenter/changepwd" class="nav-link">Change Password</a>
            </li>              
            <li class="nav-item">
              <a href="/examcenter/logout" class="nav-link">Logout</a>
            </li>          
          </ul> 
          <br/>

        </div>
        <br/>
      </div>
    </nav> 
    
    <div class="container">

      <h4>Exam Details List</h4>
             <ul class="nav nav-tabs" id="myTab" role="tablist">
  <li class="nav-item">
    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home"
      aria-selected="true">Upcoming Exam Details</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile"
      aria-selected="false">Completed Exam Details</a>
  </li>
</ul>
<div class="tab-content" id="myTabContent">
  <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab"><div class="tab-content" id="myTabContent">
          <div class="tab-pane fade show active" id="studentlist" role="tabpanel" aria-labelledby="studentlist-tab">
             <table class="table">
              <thead class="thead-light">
                <tr>
                  <th scope="col">Exam Name</th>
                  <th scope="col">Exam Date and time</th>
                  <th>Duration</th>
                  <th scope="col">Total Students</th>
                </tr>
              </thead>
              <tbody>

                <?php 
                       $this->load->model('examcenter_model');

                for($i=0;$i<count($futureexamList);$i++) {
        $countArray = $this->examcenter_model->getCountOfStudents($futureexamList[$i]->id);

        $totalStudent = count($countArray);
?>
                <tr>
                  <td><?php echo $futureexamList[$i]->examname;?></td>
                  <td><?php echo $futureexamList[$i]->exam_date;?></td>
                  <td><?php echo $futureexamList[$i]->duration.' Min';?></td>
                  <td><?php echo $totalStudent;?></td>
                </tr>
              <?php }?> 
                                              
              </tbody>
            </table>
          </div>
         
        </div>
      </div>
  <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab"><div class="tab-content" id="myTabContent">
          <div class="tab-pane fade show active" id="studentlist" role="tabpanel" aria-labelledby="studentlist-tab">
             <table class="table">
              <thead class="thead-light">
                <tr>
                  <th scope="col">Exam Name</th>
                  <th scope="col">Exam Date and time</th>
                  <th>Duration</th>
                  <th scope="col">Total Students</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Demo CIIF Online</td>
                  <td>10-11-2020 10:00</td>
                                    <td>30 Min</td>

                  <td>5</td>
                </tr>
                                            
              </tbody>
            </table>
          </div>
         
        </div></div>

</div>
  </body>

</html>
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
