
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <title>Exam</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo BASE_PATH; ?>assets/onlineportal/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo BASE_PATH; ?>assets/onlineportal/css/font-awesome.min.css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css2?family=Ubuntu:wght@300;400;500;700&display=swap" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="<?php echo BASE_PATH; ?>assets/onlineportal/css/main.css" rel="stylesheet" />
  </head>
  <body>

    <nav class="navbar navbar-expand-lg navbar-light main-header">
      <div class="container">
        <a class="navbar-brand" href="#">LOGO</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample02" aria-controls="navbarsExample02" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
      
        <div class="collapse navbar-collapse" id="navbarsExample02">
          <ul class="navbar-nav admin-nav ml-auto align-items-center">

            <li class="nav-item">
              <a href="/examcenter/examcenter/dashboardnew" class="nav-link">Dashboard</a>
            </li> 
             <li class="nav-item">
              <a href="/examcenter/examcenter/issue" class="nav-link">Report Issue</a>
            </li>
             <li class="nav-item">
              <a href="/examcenter/examcenter/examlist" class="nav-link">Exam Details</a>
            </li>  

            

                                 
            <li class="nav-item">
              Welcome <?php echo $this->session->contact_person;?>
            </li>        
              <li class="nav-item" class="active">
              <a href="/examcenter/examcenter/changepwd" class="nav-link">Change Password</a>
            </li>      
            <li class="nav-item">
              <a href="/examcenter/logout" class="nav-link">Logout</a>
            </li>          
          </ul> 
          <br/>

        </div>
        <br/>
      </div>
    </nav> 
    
    <form method="POST" action="">
    <div class="container">
      <div class="pt-4">
        <h3 style="text-align: center;color: #00bcd2;">Change Password</h3>
        <div class="row">

           <div class="col-md-6">
             <label>New Password</label>
              <input type='password' name="password" class="form-control" id="password" />
          </div>

         
                 
        </div>
        
         <div class="row">
          <div class="col-md-12">
            <br/>
            <button type="submit" class="btn btn-primary">Save</button>

          </div>
                 
        </div>
     
     </form>
       
      </div>
      
 

       <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />

  
  <script src="<?php echo BASE_PATH; ?>assets/js/jquery-1.12.4.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>

<script type="text/javascript">
   

   $('select').select2();

  function showothertext() {
    var issue_type = $("#issue_type").val();
    $("#otherdivissue").hide();
    if(issue_type=='Others') {
          $("#otherdivissue").show();
    }
  }
</script>

 </body>
</html>