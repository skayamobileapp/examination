
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <title>Exam</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo BASE_PATH; ?>assets/onlineportal/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo BASE_PATH; ?>assets/onlineportal/css/font-awesome.min.css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css2?family=Ubuntu:wght@300;400;500;700&display=swap" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="<?php echo BASE_PATH; ?>assets/onlineportal/css/main.css" rel="stylesheet" />
  </head>
  <body>

    <nav class="navbar navbar-expand-lg navbar-light main-header">
      <div class="container">
        <a class="navbar-brand" href="#">LOGO</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample02" aria-controls="navbarsExample02" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
      
        <div class="collapse navbar-collapse" id="navbarsExample02">
          <ul class="navbar-nav admin-nav ml-auto align-items-center">

            <li class="nav-item">
              <a href="/examcenter/examcenter/dashboardnew" class="nav-link">Dashboard</a>
            </li> 
             <li class="nav-item">
              <a href="/examcenter/examcenter/issue" class="nav-link">Report Issue</a>
            </li>
             <li class="nav-item">
              <a href="/examcenter/examcenter/examlist" class="nav-link">Exam Details</a>
            </li>  
                                 
            <li class="nav-item">
              Welcome <?php echo $this->session->contact_person;?>
            </li>            
            <li class="nav-item">
              <a href="/examcenter/logout" class="nav-link">Logout</a>
            </li>          
          </ul> 
          <br/>

        </div>
        <br/>
      </div>
    </nav> 


    
    <?php if($studentList) {?> 
    <div class="container">

      <div class="pt-4">
        <form method="POST" action="">
           <div class="row">
             <table width="25%">
               <tr>
                <td><a href="javascript:showdiv(0)">Current Announcement</a></td>
                <td><a href="javascript:showdiv(1)">History </a></td>
              </tr>
             </table>
           </div>

         <div class="row" id='div0'>
              <div class="col-sm-12">
                <input type='text' name='announcements' class="form-control" value="<?php echo $notification->notification;?>"/> 
                <button type='submit' name='Publish' value='Publish' class="btn btn-primary" >Publish</button>
              </div>
            </div>


            <div class="row" id='div1' style="display:none;">
              <div class="col-sm-12">
                
                <table class="table" border="1"  width="100%">
                  <thead class="thead-light">
                  <tr>

                    <th>Sl.No</th>
                    <th>Notification</th>
                    <th>Date Time</th>
                    <th>Created By</th>
                  </tr>
                </thead>

                  <?php for($i=1;$i<count($notificationByExamId);$i++){?>
                  <tr>
                    <td><?php echo $i;?></td>
                    <td><?php echo $notificationByExamId[$i]->notification;?></td>
                    <td><?php echo date('d-m-Y',strtotime($notificationByExamId[$i]->date_time));?></td>
                    <td>klexamcenter@gmail.com</td>
                  </tr>
                <?php } ?>
                </table>
              </div>
            </div>


         </form>
          <br/>
           <br/>
        <table class="table" border="1" width="100%">
          <thead class="thead-light">
            <tr>
              <th>Total Students</th>
              <th>Duration of Exam (min)</th>
              <th>Event Name</th>
              <th>Exam Date</th>
              <th>Exam Time</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td><?php echo count($studentList);?></td>
              <td><?php echo $currentexam->duration;?></td>
              <td><?php echo $currentexam->name;?></td>
              <td><?php echo date('d-m-Y',strtotime($currentexam->exam_date));?></td>
              <td><?php echo date('H:i a',strtotime($currentexam->from_tm));?> - <?php echo date('H:i a',strtotime($currentexam->to_tm));?></td>
            </tr>
           </tbody> 
         </table>
               <hr/>
               
               <div class="row">
                   <div class="col-md-4">

                    
                   <!--  <div class="list-group student-list">
                        
                        <?php   $this->load->model('examcenter_model');

                        for($l=0;$l<count($studentList);$l++) { 
                          $idstudent = $studentList[$l]->id; 
                          $messageunread = $this->examcenter_model->numberofchats($idstudent)?>
            
                          <a href="#" class="list-group-item list-group-item-action" onclick="getdata(<?php echo $idstudent;?>,'<?php echo $studentList[$l]->full_name;?>')" id="std<?php echo $idstudent;?>" name='std[]'><?php echo $studentList[$l]->full_name;?> 
                          <?php if(count($messageunread)>0){?>
                          <span class="badge badge-info"><?php echo count($messageunread);?></span>
                        <?php } ?> 

                          <br/> <?php echo $studentList[$l]->nric;?>   

                         </a>

                      <?php } ?>                         
                    </div>    -->


                    <div class="list-group student-list" id='studentdetails'>
                    </div>                    
                   </div>
                   <div class="col-md-8">
                    <div class="custom-chatbot teacher-chatbot">
                      <div class="chatbot-container">
                        <div class="chatbot-header">
                          <div class="header-text"><span id='stdnameid'>Student name</span></div>
                        </div>
                        <div class="chat-room">
                                      <div class="message-area" id='chatdetails'>
                                      </div>

                          <div class="input-group mb-3 input-group-lg">
                            <input
                              type="text"
                              class="form-control"
                              id='chat'
                              placeholder="Enter Message"
                            />
                            <div class="input-group-append">
                              <button class="btn btn-primary" type="button" onclick="senddata()">Send</button>
                                            <input type='hidden' id='student_id' value='' />


                            </div>
                          </div>
                        </div>
                      </div>
                    </div>                       
                   </div>
               </div>
       
    </div>
  <?php } else { ?>
   <div class="container">
     <h3>No Exam has been scheduled for today</h3>
   </div>
  <?php } ?> 

       <script src="<?php echo BASE_PATH; ?>assets/onlineportal/js/jquery-1.12.4.min.js"></script>


    <script>

      function showdiv(id) {

        $("#div0").hide();
        $("#div1").hide();
        $("#div"+id).show();
      }

      $( document ).ready(function() {
    getstudentlist();
});



      function getstudentlist(){
        var id= <?php echo $idexamid;?>;
           $.ajax({
        url: "/examcenter/examcenter/studentlist",
        method: "POST",
        data: {
          id: id
        },
        success: function(data) {
         
          $("#studentdetails").html(data);
        }

        })
      }

        function getdata(id,name){

          $("#stdnameid").html(name);
          console.log(id);

          $("#student_id").val(id);


          $(".list-group-item-action").removeClass('active');

          $("#std"+id).addClass('active');

              $.ajax({
        url: "/examcenter/examcenter/getmessage",
        method: "POST",
        data: {
          id: id
        },
        success: function(data) {
         
          $("#chatdetails").html(data);
          getstudentlist();
        }

        })
            }

        function senddata(){

            var chat = $("#chat").val();
            var student_id = $("#student_id").val();

              $.ajax({
        url: "/examcenter/examcenter/chatsave",
        method: "POST",
        data: {
          chat: chat,
          student_id:student_id
        },
        success: function(data) {
         
                          $.ajax({
                  url: "/examcenter/examcenter/getmessageunit",
                  method: "POST",
                  data: {
                    id: student_id
                  },
                  success: function(data) {
                   
                    $("#chatdetails").html(data);
                    $("#chat").val('');
                  }

                  })
                  }
      });


        }

setInterval(function() {
  getstudentlist();
                }, 10000); 
</script>
  </body>
</html>

