<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


class Exam extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('preview_model');
        error_reporting(0);
    }

    function instructions($id)
    {
        

        $examDetails = $this->preview_model->getExamDetailsByEventId($id);
        $data['instruction'] = $examDetails[0]->instructions;
        $data['examsetid'] = $id;



            $this->loadExamViews("exam/instructions", $this->global, $data, NULL);
        
    }

  

    function start($id)
    {
        
         


            $examEvent = $id;

            $examDetails = $this->preview_model->getExamDetailsByEventId($id);
            $attemptnumber = 0;
            $exam = $examDetails[0];




            if($exam->id_manual_tos!='0') {
            $id_manual_tos = $exam->id_manual_tos;
            $id_tos = $exam->id;
            $this->creationofQuestionmanual($id_manual_tos,$id_tos);
                     $tosdetails = $this->preview_model->getTosDetails($id_tos);
          $idtos = $exam->id_tos;
             $question = $this->preview_model->getAssingedQuestionForExam($id_tos);

          } else {

          $idtos = $exam->id_tos;

          
            $this->creationofQuestion($idtos);

                     $tosdetails = $this->preview_model->getTosDetails($idtos);

             $question = $this->preview_model->getAssingedQuestionForExam($idtos);
         }





            $data['question'] = $question;
            $data['time'] = $tosdetails->duration;
            $data['idtos'] = $idtos;
            $data['id'] = $id;

            $this->global['pageTitle'] = 'Student Exam : Start';
            $this->global['userInfo'] = $this->session->get_userdata();
            $this->loadExamViews("exam/singlestart", $this->global, $data, NULL);
        
    }

   

    function getAllQuestionNumber($id){

            $question = $this->preview_model->getAssignedQuestionsTos($id);



           


            $mcqmarks = 0; 
            for($i=0;$i<count($question);$i++) {
                 if($question[$i]['question_type']=='1') {
                        $mcqmarks = $mcqmarks + $question[$i]['marks'];

                 }
             }


             $essaymarks = 0; 
            for($i=0;$i<count($question);$i++) {
                 if($question[$i]['question_type']=='2') {
                        $essaymarks = $essaymarks + $question[$i]['marks'];

                 }
             }


        $table = "<div class='questions-list-container'>
              <br/>
              <h5>MCQ : Total Marks $mcqmarks <button class='btn btn-primary btn-sm close-btn' onclick='togglemenu()'>Close</button></h5>
              <ul class='questions-list'>";
              $j = 0;
              for($i=0;$i<count($question);$i++) {
                 if($question[$i]['question_type']=='1') {
                $j++;
                $idquestion = $question[$i]['id'];
                $question_number = $question[$i]['question_number'];

                  $class ='';
                  if($question[$i]['id_answer']!=0) {
                      $class ='answered';
                  }

                  if($question[$i]['id_answer']==0 && $question[$i]['answer_text']!='') {
                      $class ='answered';                    
                  }
                $table.="<li class='$class' onclick='scrolldiv($idquestion)'>$question_number</li>";
              }
            }

                
               
              $table.="</ul>
                <h5>Essay : Total Marks $essaymarks <button class='btn btn-primary btn-sm close-btn' onclick='togglemenu()'>Close</button></h5>
               <ul class='questions-list'>";
                $j = 0;
              for($i=0;$i<count($question);$i++) {
                 if($question[$i]['question_type']=='2') {
               $j++;
                $idquestion = $question[$i]['id'];
                $question_number = $question[$i]['question_number'];
                  $class ='';
                  if($question[$i]['id_answer']!=0) {
                      $class ='answered';
                  }

                  if($question[$i]['id_answer']==0 && $question[$i]['answer_text']!='') {
                      $class ='answered';                    
                  }
                $table.="<li class='$class' onclick='scrolldiv($idquestion)'>$question_number</li>";
              }
            }

                
               
              $table.="</ul>

               <ul class='question-list-help'>
                <li>Not Attempted</li>
                <li class='answered'>Answered</li>
                <li style='width:100%'>Scroll down at last to submit the answer</li>
              </ul>
            </div>             <div id='my_camera'></div>
";
            echo $table;
            exit;
    }



    function creationofQuestion($idtos){

        $this->preview_model->deletetos($idtos);

        $tosdetails = $this->preview_model->getQuestionByTos($idtos);


              $question = array();
              $questionList = array();
              $questionselected = 0;
              for($i=0;$i<count($tosdetails);$i++) {
                    $questionList = $this->preview_model->getQuestionFromLogic($tosdetails[$i],$questionselected);
                    for($l=0;$l<count($questionList);$l++) {
                          array_push($question, $questionList[$l]);
                          $questionselected = $questionselected.','.$questionList[$l]['id'];
                    }
              }





              $order = 0;
           $question_number = 1;

           $question_number_essay = 1;


            for($q=0;$q<count($question);$q++) {


                $idquestion = $question[$q]['id'];
                $questionDetails = $this->preview_model->getQuestionDetailById($idquestion);

                if($questionDetails['question_type']=='2') {

                    $getChildQuestions = $this->preview_model->getAllChildQuestions($idquestion);

                    for($m=0;$m<count($getChildQuestions);$m++) {

                          if($m==0) {
                             $alphabet = '-A';
                          }
                          if($m==1) {
                             $alphabet = '-B';
                          }
                          if($m==2) {
                             $alphabet = '-C';
                          }
                          if($m==3) {
                             $alphabet = '-D';
                          }
                          if($m==4) {
                             $alphabet = '-E';
                          }

                     $examdata = array(
                        'id_student' => 0,
                        'id_question' => $getChildQuestions[$m]['id'],
                        'question_order' => $order++,
                        'id_exam_student_tagging' => $exam->examstudenttagging,
                        'id_answer' => 0,
                        'id_student_exam_attempt'=>$id_student_exam_attempt,
                        'question_number' =>  $question_number_essay.' '.$alphabet,
                        'id_tos'=>$idtos

                      );


                    $current_exam = $this->preview_model->saveUserExamTos($examdata);
                    }
                                    $question_number_essay++;
                } else {
                         $examdata = array(
                        'id_student' => 0,
                        'id_question' => $idquestion,
                        'question_order' => $order++,
                        'id_exam_student_tagging' => $exam->examstudenttagging,
                        'id_answer' => 0,
                        'id_student_exam_attempt'=>$id_student_exam_attempt,
                        'question_number' =>  $question_number,
                        'id_tos'=>$idtos

                    );

                    $current_exam = $this->preview_model->saveUserExamTos($examdata);
                                    $question_number++;

                }

              } 

    }

     function creationofQuestionmanual($id_manual_tos,$idtos){

        $this->preview_model->deletetos($idtos);

        $question = $this->preview_model->getQuestionByManualTos($id_manual_tos);

        $order = 0;
        $question_number = 1;
        $question_number_essay = 1;


            for($q=0;$q<count($question);$q++) {


                $idquestion = $question[$q]['id'];
                $questionDetails = $this->preview_model->getQuestionDetailById($idquestion);

                if($questionDetails['question_type']=='2') {

                    $getChildQuestions = $this->preview_model->getAllChildQuestions($idquestion);

                    for($m=0;$m<count($getChildQuestions);$m++) {

                          if($m==0) {
                             $alphabet = '-A';
                          }
                          if($m==1) {
                             $alphabet = '-B';
                          }
                          if($m==2) {
                             $alphabet = '-C';
                          }
                          if($m==3) {
                             $alphabet = '-D';
                          }
                          if($m==4) {
                             $alphabet = '-E';
                          }

                     $examdata = array(
                        'id_student' => 0,
                        'id_question' => $getChildQuestions[$m]['id'],
                        'question_order' => $order++,
                        'id_exam_student_tagging' => $exam->examstudenttagging,
                        'id_answer' => 0,
                        'id_student_exam_attempt'=>$id_student_exam_attempt,
                        'question_number' =>  $question_number_essay.' '.$alphabet,
                        'id_tos'=>$idtos

                      );


                    $current_exam = $this->preview_model->saveUserExamTos($examdata);
                    }
                                    $question_number_essay++;
                } else {
                         $examdata = array(
                        'id_student' => 0,
                        'id_question' => $idquestion,
                        'question_order' => $order++,
                        'id_exam_student_tagging' => $exam->examstudenttagging,
                        'id_answer' => 0,
                        'id_student_exam_attempt'=>$id_student_exam_attempt,
                        'question_number' =>  $question_number,
                        'id_tos'=>$idtos

                    );

                    $current_exam = $this->preview_model->saveUserExamTos($examdata);
                                    $question_number++;

                }

              } 

    }


   
   }