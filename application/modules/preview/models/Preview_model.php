<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Preview_model extends CI_Model
{
    function getExamDetailsByEventId($id)
    {
        $this->db->select('*');
        $this->db->from('examset');
        $this->db->where('id', $id);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getQuestionDetailById($id) {
        $this->db->select('*');
        $this->db->from('question');
        $this->db->where('id', $id);
        $question = $this->db->get()->row_array();
        return $question;

    }

      function deletetos($id)
    { 
        // echo "<Pre>";  print_r($id);exit;
       $this->db->where('id_tos', $id);
       $this->db->delete('tos_temp_question_set');
    }

      function saveUserExamTos($data)
    {
        $this->db->trans_start();
        $this->db->insert('tos_temp_question_set', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }


 function getAllChildQuestions($id) {
        $this->db->select('s.*');
        $this->db->from('question as s');
        $this->db->where('s.parent_id', $id);
        $questions = $this->db->get()->result_array();
        return $questions;
    }


  function getQuestionByManualTos($idmanualtos) {
        $this->db->select('q.*,t.name as topicname,b.code as bloomtaxanomycode');
        $this->db->from('manualtos_has_question as a ');
        $this->db->join('question as q', 'q.id=a.id_question');
        $this->db->join('topic as t', 't.id=q.id_topic','left');
        $this->db->join('bloom_taxonomy as b', 'b.id=q.id_bloom_taxonomy','left');

        $this->db->where('id_manualtos', $idmanualtos);
        $questions = $this->db->get()->result_array();
        return $questions;
    }


      function getAssignedQuestionsTos($idstudentexamattempt) {
        $this->db->select('a.*,s.id as examstudentid,s.id_answer,s.answer_text,s.question_number');
        $this->db->from('tos_temp_question_set as s');
        $this->db->where('s.id_tos', $idstudentexamattempt);
        $this->db->join('question as a', 'a.id = s.id_question');
        $this->db->order_by('a.question_type ASC');

        $questions = $this->db->get()->result_array();
        return $questions;
    }
    

     function getAssingedQuestionForExam($idstudentexamattempt) {
        $this->db->select('a.*,s.id as examstudentid,s.id_answer,s.answer_text,s.question_number,t.name as topicname,b.code as bloomtaxanomycode');
        $this->db->from('tos_temp_question_set as s');
        $this->db->where('s.id_tos', $idstudentexamattempt);
        $this->db->join('question as a', 'a.id = s.id_question');
        $this->db->join('topic as t', 't.id=a.id_topic','left');
        $this->db->join('bloom_taxonomy as b', 'b.id=a.id_bloom_taxonomy','left');

        $this->db->order_by('a.question_type ASC');

        $questions = $this->db->get()->result_array();
        return $questions;
    }


    function getTosDetails($idtos) {
        $this->db->select('a.*');
        $this->db->from('examset as a');
        $this->db->where('a.id', $idtos);
        $query = $this->db->get();
        $result = $query->row();
        return $result;
    }

      function getQuestionByTos($idtos) {
        $this->db->select('*');
        $this->db->from('tos_details');
        $this->db->where('id_tos', $idtos);
        $questions = $this->db->get()->result_array();
        return $questions;
    }

     function getAnswersByQuestionId($questionid) {
        $this->db->select('a.*');
        $this->db->from('question_has_option as a');
        $this->db->where('a.id_question', $questionid);
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }


    
    function getQuestionFromLogic($logic,$questionselected) {
        $this->db->select('*');
        $this->db->from('question');
        $this->db->where('id_course', $logic['id_course']);
        $this->db->where('id_topic', $logic['id_topic']);
        $this->db->where('id_bloom_taxonomy', $logic['id_bloom_taxonomy']);
        $this->db->where('parent_id', 0);


         $likeCriteria = "(id  not in (" . $questionselected . "))";
            $this->db->where($likeCriteria);

        $this->db->order_by('rand()');

        $this->db->limit($logic['questions_selected']);

        $questions = $this->db->get()->result_array();


        return $questions;
    }


    
}

