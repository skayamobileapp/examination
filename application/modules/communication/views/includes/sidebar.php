 <div class="container-fluid">
      <div class="row">
        <nav id="sidebarMenu" class="col-md-3 d-md-block sidebar collapse">
          <div class="sidebar-sticky pt-3">
            <div class="nav-fold">
              <a href="#" class="d-flex align-items-center">
                <span class="user-avatar"><img src="<?php echo BASE_PATH; ?>assets/img/user.jpg"/></span>
                <span>
                  <?php echo $name; ?>
                  <small class="d-block"><?php echo $role; ?></small>
                  <small class="d-block"></small>                  
                </span>
              </a>
            </div>
            <ul class="nav flex-column mb-2">
             
              <li class="nav-item">
                <a class="nav-link  <?php 
                  if(in_array($pageCode,array('template.list','template.add','template.edit','group.list','group.add','group.edit','group_recepient.list','group_recepient.add'))){  ?>
                  <?php 
                  }
                  else
                  { 
                  ?>
                  collapsed
                  <?php
                  }
                  ?>"  data-toggle="collapse" href="#collapseExamApplication" role="button">
                  <i class="fa fa-list-alt"></i>
                  <span>Communication</span>
                  <i class="fa fa-angle-right ml-auto" aria-hidden="true"></i>
                  <i class="fa fa-angle-down ml-auto" aria-hidden="true"></i>
                </a>

                <ul class="collapse nav <?php 
                  if(in_array($pageCode,array('template.list','template.add','template.edit','group.list','group.add','group.edit','group_recepient.list','group_recepient.add'))){  ?>
                  show
                 <?php 
                  } else{ 
                  ?>
                  
                  <?php
                  }
                  ?>" id="collapseExamApplication">
                  <li class="nav-item">
                    <a href="/communication/template/list" class="nav-link <?php if(in_array($pageCode,array('template.list','template.add','template.edit'))){echo 'active';}?>">
                      <i class="fa fa-list-alt"></i>
                      <span>Template</span>     
                    </a>
                  </li>

                  <li class="nav-item">
                    <a href="/communication/group/list" class="nav-link <?php if(in_array($pageCode,array('group.list','group.add','group.edit'))){echo 'active';}?>">
                      <i class="fa fa-list-alt"></i>
                      <span>Group</span>     
                    </a>
                  </li>


                  <li class="nav-item">
                    <a href="/communication/group/recepientList" class="nav-link <?php if(in_array($pageCode,array('group_recepient.list','group_recepient.add'))){echo 'active';}?>">
                      <i class="fa fa-list-alt"></i>
                      <span>Group Recepients</span>     
                    </a>
                  </li>
                  
                </ul>
              </li>


              <li class="nav-item">
                <a class="nav-link  <?php 
                  if(in_array($pageCode,array('template_message.list','template_message.add','template_message.edit','group_message.list','group_message.add','group_message.edit','group_message_recepient.list','group_message_recepient.add'))){  ?>
                  <?php 
                  }
                  else
                  { 
                  ?>
                  collapsed
                  <?php
                  }
                  ?>"  data-toggle="collapse" href="#collapseExamApplication" role="button">
                  <i class="fa fa-list-alt"></i>
                  <span>Communication Messsage</span>
                  <i class="fa fa-angle-right ml-auto" aria-hidden="true"></i>
                  <i class="fa fa-angle-down ml-auto" aria-hidden="true"></i>
                </a>

                <ul class="collapse nav <?php 
                  if(in_array($pageCode,array('template_message.list','template_message.add','template_message.edit','group_message.list','group_message.add','group_message.edit','group_message_recepient.list','group_message_recepient.add'))){  ?>
                  show
                 <?php 
                  } else{ 
                  ?>
                  
                  <?php
                  }
                  ?>" id="collapseExamApplication">
                  <li class="nav-item">
                    <a href="/communication/templateMessage/list" class="nav-link <?php if(in_array($pageCode,array('template_message.list','template_message.add','template_message.edit'))){echo 'active';}?>">
                      <i class="fa fa-list-alt"></i>
                      <span>Template</span>     
                    </a>
                  </li>

                  <li class="nav-item">
                    <a href="/communication/groupMessage/list" class="nav-link <?php if(in_array($pageCode,array('group_message.list','group_message.add','group_message.edit'))){echo 'active';}?>">
                      <i class="fa fa-list-alt"></i>
                      <span>Group</span>     
                    </a>
                  </li>


                  <li class="nav-item">
                    <a href="/communication/groupMessage/recepientList" class="nav-link <?php if(in_array($pageCode,array('group_message_recepient.list','group_message_recepient.add'))){echo 'active';}?>">
                      <i class="fa fa-list-alt"></i>
                      <span>Group Recepients</span>     
                    </a>
                  </li>
                  
                </ul>
              </li>
                                     
          </ul>         
        </div>
      </nav>
    </div>
  </div>