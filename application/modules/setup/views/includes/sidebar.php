 <div class="container-fluid">
      <div class="row">
        <nav id="sidebarMenu" class="col-md-3 d-md-block sidebar collapse">
          <div class="sidebar-sticky pt-3">
            <div class="nav-fold">
              <a href="#" class="d-flex align-items-center">
                <span class="user-avatar"><img src="<?php echo BASE_PATH; ?>assets/img/user.jpg"/></span>
                <span>
                  <?php echo $name; ?>
                  <small class="d-block"><?php echo $role; ?></small>
                  <small class="d-block"></small>                  
                </span>
              </a>
            </div>
            <ul class="nav flex-column mb-2">
             
              <li class="nav-item">
                <a class="nav-link  <?php 
                  if(in_array($pageCode,array('role.list','role.add','role.edit','permission.list','permission.add','permission.edit','user.list','user.add','user.edit'))){  ?>
                  <?php 
                  } else{ 
                  ?>
                  collapsed
                  <?php
                  }
                  ?>"  data-toggle="collapse" href="#collapseExamApplication" role="button">
                  <i class="fa fa-list-alt"></i>
                  <span>General Setup</span>
                  <i class="fa fa-angle-right ml-auto" aria-hidden="true"></i>
                  <i class="fa fa-angle-down ml-auto" aria-hidden="true"></i>
                </a>
                <ul class="collapse nav <?php 
                  if(in_array($pageCode,array('role.list','role.add','role.edit','permission.list','permission.add','permission.edit','user.list','user.add','user.edit'))){  ?>
                  show
                 <?php 
                  } else{ 
                  ?>
                  
                  <?php
                  }
                  ?>" id="collapseExamApplication">
                  <li class="nav-item">
                    <a href="/setup/role/list" class="nav-link <?php if(in_array($pageCode,array('role.list','role.edit','role.add'))){echo 'active';}?>">
                      <i class="fa fa-list-alt"></i>
                      <span>Roles</span>                      
                    </a>
                  </li>
                
                  <li class="nav-item">
                    <a href="/setup/user/list" class="nav-link <?php if(in_array($pageCode,array('user.list','user.edit','user.add'))){echo 'active';}?>">
                      <i class="fa fa-file-text"></i>
                      <span>User</span>                      
                    </a>
                  </li>                  
                </ul>
              </li>  
              
           <!--              
              <li class="nav-item">
                    <a target="_blank" href="/setup/user/lms" class="nav-link">
                      <i class="fa fa-file-text"></i>
                      <span>Go to LMS</span>                      
                    </a>
                  </li>        -->                
          </ul>         
        </div>
      </nav>
    </div>
  </div>