<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <title>Quiz</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo BASE_PATH; ?>assets/onlineportal/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo BASE_PATH; ?>assets/onlineportal/css/font-awesome.min.css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css2?family=Ubuntu:wght@300;400;500;700&display=swap" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="<?php echo BASE_PATH; ?>assets/onlineportal/css/main.css" rel="stylesheet" />
  </head>
  <body>

    <nav class="navbar navbar-expand-lg navbar-light main-header">
      <div class="container">
        <a class="navbar-brand" href="#">LOGO</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample02" aria-controls="navbarsExample02" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
      
        <!--<div class="collapse navbar-collapse" id="navbarsExample02">-->
        <!--  <ul class="navbar-nav ml-auto align-items-center">-->
        <!--    <li class="nav-item">-->
        <!--      Welcome-->
        <!--    </li>            -->
        <!--    <li class="nav-item active">-->
        <!--      <a href="/studentexamination/exam/logout" class="nav-link">Logout</a>-->
        <!--    </li>          -->
        <!--  </ul>          -->
        <!--</div>-->
      </div>
    </nav> 

    <div class="container">
    <div class="row align-items-center">
      <div class="col-md-4 mt-5">
        <img src="<?php echo BASE_PATH; ?>assets/img/instructions.svg" alt="" class="img-responsive">
      </div>
      <div class="col-md-8 mt-5">
        <h4>CIIF Exam Logistic Confirmation</h4>
        <ul>
            <li>Thank you for confirming the details</li>
        </ul>

        
             <div class="row">
          <div class="col-md-5 col-lg-6">
          <a href="/studentLogin" class="btn btn-primary start-btn" >Click to Login</a>          
          </div>
          </div>
      </div>
    </div>
  </div>

 <script src="<?php echo BASE_PATH; ?>assets/onlineportal/js/jquery-1.12.4.min.js"></script>

