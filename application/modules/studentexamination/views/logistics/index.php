<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <title>Quiz</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo BASE_PATH; ?>assets/onlineportal/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo BASE_PATH; ?>assets/onlineportal/css/font-awesome.min.css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css2?family=Ubuntu:wght@300;400;500;700&display=swap" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="<?php echo BASE_PATH; ?>assets/onlineportal/css/main.css" rel="stylesheet" />
  </head>
  <body>

    <nav class="navbar navbar-expand-lg navbar-light main-header">
      <div class="container">
        <a class="navbar-brand" href="#">LOGO</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample02" aria-controls="navbarsExample02" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
      
        <div class="collapse navbar-collapse" id="navbarsExample02">
          <ul class="navbar-nav ml-auto align-items-center">
            <!--<li class="nav-item">-->
            <!--  Welcome-->
            <!--</li>            -->
                     
          </ul>          
        </div>
      </div>
    </nav> 
<form action="" method="POST">
    <div class="container">
    <div class="row align-items-center">
      <div class="col-md-4 mt-5">
      </div>
      <div class="col-md-8 mt-5">
        <h4>CIIF Exam Logistic Confirmation</h4> <br/>
        <h5>Dear Respected Member,<Br/><Br/>
               Please confirm the location that you will be taking the CPIF Exam</h5><br/>
          <div class="row">
          <div class="col-md-7 col-lg-6">
            <div class="form-group">
              <label for="token">Select country</label>
              <select name='country' id='country' class="form-control">

                 <option value='1'>Malaysia</option>

              </select>            
            </div>
          </div>
          </div>
             <div class="row">
          <div class="col-md-7 col-lg-6">
            <div class="form-group">
              <label for="token">Select State</label>
              <select name='state' id='state' class="form-control" onchange="getCity()">
                 <option value=''>Select</option>
                <?php 
                  for($i=0;$i<count($state);$i++)  { ?>

                  <option value="<?php echo $state[$i]->id;?>"> <?php echo $state[$i]->name;?> </option>
                 <?php } ?> 

              </select>              
            </div>
          </div>
           </div>
             <div class="row">
          <div class="col-md-7 col-lg-6">
            <div class="form-group">
              <label for="token">Select Location</label>
              <span id='citydiv'></span>            
            </div>
          </div>
           </div>
             <div class="row">
          <div class="col-md-5 col-lg-6">
          <button type='submit' class="btn btn-primary start-btn" name='save' >Click to Submit</button>          
          </div>
          </div>
      </div>
    </div>
  </div>
</form>
 <script src="<?php echo BASE_PATH; ?>assets/onlineportal/js/jquery-1.12.4.min.js"></script>

<script>
function getCity() {
  var idstate = $("#state").val();
    $.get("/studentexamination/Logistics/allcity/"+idstate, function(data, status){
           
               $("#citydiv").html(data);
            });
  }

</script>