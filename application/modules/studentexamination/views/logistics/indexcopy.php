<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <title>Quiz</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo BASE_PATH; ?>assets/onlineportal/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo BASE_PATH; ?>assets/onlineportal/css/font-awesome.min.css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css2?family=Ubuntu:wght@300;400;500;700&display=swap" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="<?php echo BASE_PATH; ?>assets/onlineportal/css/main.css" rel="stylesheet" />
  </head>
  <body>

    <nav class="navbar navbar-expand-lg navbar-light main-header">
      <div class="container">
        <a class="navbar-brand" href="#">LOGO</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample02" aria-controls="navbarsExample02" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
      
        <div class="collapse navbar-collapse" id="navbarsExample02">
          <ul class="navbar-nav ml-auto align-items-center">
            <!--<li class="nav-item">-->
            <!--  Welcome-->
            <!--</li>            -->
                     
          </ul>          
        </div>
      </div>
    </nav> 

    <div class="container">
    <div class="row align-items-center">
      <div class="col-md-4 mt-5">
      </div>
      <div class="col-md-8 mt-5">
        <h4>CIIF Exam: Location Confirmation</h4> <br/>
        <h5>Dear Respected Member,<Br/><Br/>
               Please confirm the location that you will be taking the CPIF Exam</h5><br/>
          <div class="row">
          <div class="col-md-7 col-lg-6">
            <div class="form-group">
              <label for="token">Select country</label>
              <select name='country' id='country' class="form-control">
                 <option value=''>Select</option>

                 <option value='Malaysia'>Malaysia</option>

              </select>            
            </div>
          </div>
          </div>
             <div class="row">
          <div class="col-md-7 col-lg-6">
            <div class="form-group">
              <label for="token">Select State</label>
              <select name='state' id='state' class="form-control">
                 <option value=''>Select</option>

                 <option value='PERLIS'>PERLIS</option>
<option value='KEDAH'>KEDAH</option>
<option value='PULAU PINANG'>PULAU PINANG</option>
<option value='PERAK'>PERAK</option>
<option value='KELANTAN'>KELANTAN</option>
<option value='TERENGGANU'>TERENGGANU</option>
<option value='PAHANG'>PAHANG</option>
<option value='SELANGOR'>SELANGOR</option>
<option value='WP KUALA LUMPUR'>WP KUALA LUMPUR</option>
<option value='LUBNAN'>LUBNAN</option>
<option value='Penang'>Penang</option>
<option value='MELAKA'>MELAKA</option>
<option value='TERENGGANU'>TERENGGANU</option>
<option value='Kuala Lumpur'>Kuala Lumpur</option>
<option value='PUTRAJAYA'>PUTRAJAYA</option>
<option value='Negeri Sembilan'>Negeri Sembilan</option>
<option value='Malacca'>Malacca</option>

              </select>              
            </div>
          </div>
           </div>
             <div class="row">
          <div class="col-md-7 col-lg-6">
            <div class="form-group">
              <label for="token">Select Location</label>
              <select name='city' id='city' class="form-control">
                 <option value=''>Select</option>

                <option value='Ayer Hitam'>Ayer Hitam</option>
<option value='Batu Pahat'>Batu Pahat</option>
<option value='Parit Raja'>Parit Raja</option>
<option value='Parit Sulong'>Parit Sulong</option>
<option value='Rengit'>Rengit</option>
<option value='Semerah'>Semerah</option>
<option value='Senggarang'>Senggarang</option>

              </select>            
            </div>
          </div>
           </div>
             <div class="row">
          <div class="col-md-5 col-lg-6">
          <a href="/studentexamination/logistics/thankyou" class="btn btn-primary start-btn" >Click to submit</a>          
          </div>
          </div>
      </div>
    </div>
  </div>

 <script src="<?php echo BASE_PATH; ?>assets/onlineportal/js/jquery-1.12.4.min.js"></script>

