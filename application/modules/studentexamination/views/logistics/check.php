<?php 
$encrypted = md5(date('Y-m-d'));
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <title>Quiz</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo BASE_PATH; ?>assets/onlineportal/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo BASE_PATH; ?>assets/onlineportal/css/font-awesome.min.css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css2?family=Ubuntu:wght@300;400;500;700&display=swap" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="<?php echo BASE_PATH; ?>assets/onlineportal/css/main.css" rel="stylesheet" />
  </head>
  <body>

    <nav class="navbar navbar-expand-lg navbar-light main-header">
      <div class="container">
        <a class="navbar-brand" href="#">LOGO</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample02" aria-controls="navbarsExample02" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
      
        <div class="collapse navbar-collapse" id="navbarsExample02">
          <ul class="navbar-nav ml-auto align-items-center">
            <!--<li class="nav-item">-->
            <!--  Welcome-->
            <!--</li>            -->
                    
          </ul>          
        </div>
      </div>
    </nav> 

    <div class="container">
        <div class="compatiblity-check">
            <div class="wizard-container">
                <h5>Dear Respected Members,</h5>
                <h4>BECOME CHARTERED PROFESSIONALS IN ISLAMIC FINANCE (CPIF) WITH CIIF</h4>
                <p>Your certification journey starts here.</p>
                <p>Take an exam online from the comfort of your home or office.</p>
                <p>Check out the information below to start your online testing experience smoothly.</p>
                <ul>
                    <li><strong>Perform a System Test</strong></li>
                    <li>Please ensure that you will be using <strong>THE SAME</strong> testing space, computer, and internet connection.</li>
                </ul>
                <div class="alert alert-primary my-5" role="alert">
                    <strong>Alert!</strong> Work computers generally have more restrictions that may prevent a successful test. Ensure you are not behind a corporate firewall, and shut down any <strong>Virtual Private Networks</strong> (VPNs).
                </div>
                
                <div class="d-flex mt-4 justify-content-end">
                    <button class="btn btn-primary next">
                        Next
                    </button>
                </div>
            </div>
            <div class="wizard-container">
                <p>Step 2: Equipment Checks </p>
                <div class="mb-3">
                    <div class="row text-center">
                        <div class="col-md-6">
                             <div id="my_camera" style='display:none'></div>
                             <div id='imagevideo'>
                                <img class="img-placeholder" src="<?php echo BASE_PATH; ?>assets/img/video_img.png" alt="" > 
                             </div>
                            <button class="btn btn-primary ml-3" onclick="clickvide()">
                                Check Camera/Video
                            </button>   
                             <button onclick="hidevideo()" class="btn btn-primary ml-3">Pause Camera/Video</button>   
                        </div>
                        <div class="col-md-6">
                            
                             <div>
                                 <audio id="myAudio">
  <source src="https://ciifiscore.ciif-global.org/assets/mp3/test.mp3" type="audio/mpeg">
</audio>

                                <img class="img-placeholder" src="<?php echo BASE_PATH; ?>assets/img/audio_icon.png" alt="" > 
                             </div>                          
                        <button onclick="play()" class="btn btn-primary ml-3">Play Audio</button>
                         <button onclick="pause()" class="btn btn-primary ml-3">Pause Audio</button>                            
                        </div>
                    </div>
                </div> 
                
                
                <div class="d-flex mt-4 justify-content-between">
                    <button class="btn btn-primary previous">
                        Previous
                    </button>
                    <button class="btn btn-primary next">
                        Next
                    </button>                     
                </div>                
            </div>
            <div class="wizard-container">
                <p>Step 3: Exam Simulation </p>
                <p>Click on the link below to launch and experience the CIIFiScore Exam Simulation to ensure your Online Exam goes smoothly on the Exam Day: Online Exam Simulation Link: <a href="https://exam.camsedu.com/studentexamination/mockexam/instructions/<?php echo $encrypted;?>">Click to take mock exam</a></p>
                <h5 class="pt-4">CIIFiScore Technical Requirements</h5>
                <p>It is your responsibility to review the technical requirements to ensure your system is compatible with the CIIF Online Exam System.If your system does not meet the requirements and causes a problem during the exam, you will have to contact CIIF Admin immediately at +603-2632 5880 or programmes@ciif-global.org.</p>
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th width="200">Technical Requirement</th>
                      <th>Technical Specification</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>Operating System</td>
                      <td>
                          <strong>Windows 11 & 10(64-bit)</strong> (excluding 'S Mode')<br />
                          <strong>Mac OS 10.13 and above</strong> (excluding beta versions)
                          <p><strong>Note:</strong> Windows Operating Systems must pass Genuine Windows Validation.</p>
                          <ul>
                              <li><strong>Windows 8/8.1, Windows 7, Windows Vista, and Windows XP</strong> are not supported for exam delivery</li>
                              <li>All Linux/Unix based Operating Systems are strictly prohibited.</li>
                          </ul>
                      </td>
                    </tr>
                    <tr>
                      <td><strong>Firewall</strong></td>
                      <td>
                          Do not take your exam in a setting with a corporate firewall (including VPN). We recommend testing on a personal computer. Work computers generally have more restrictions that may prevent successful delivery.
                      </td>
                    </tr> 
                    <tr>
                      <td><strong>RAM</strong></td>
                      <td>OS specified minimum RAM<br />4 GB RAM or more
                      </td>
                    </tr>
                    <tr>
                      <td><strong>Internet Browser</strong></td>
                      <td>The newest versions of Microsoft <strong>Edge</strong>, <strong>Safari</strong>, <strong>Chrome</strong>, and <strong>Firefox</strong>.
                      </td>
                    </tr>  
                    <tr>
                      <td><strong>Internet Connection</strong></td>
                      <td>For optimal performance, a reliable and stable connection speed of <strong>3 Mbps down/up</strong> is required. If you are taking the test from home, best to ask others within the household to avoid internet use during your exam session.</td>
                    </tr> 
                    <tr>
                      <td><strong>Webcam</strong></td>
                      <td>Webcam must have a <strong>minimum resolution of 640x480 @ 10 fps</strong>.<br />Webcam may be internal or may be external and connected to the computer.</td>
                    </tr>   
                    <tr>
                      <td><strong>Sound & Microphone</strong></td>
                      <td>Verify the audio and microphone are not set on mute in Windows.</td>
                    </tr>   
                    <tr>
                      <td><strong>Browser Settings</strong></td>
                      <td><strong>Internet Cookies must be enabled.</strong></td>
                    </tr> 
                    <tr>
                      <td><strong>Power</strong></td>
                      <td>Make sure you are connected to a power source before starting your exam to avoid draining your battery during the exam.</td>
                    </tr>                     
                  </tbody>
                </table>  
                <div class="d-flex mt-4 justify-content-between">
                    <button class="btn btn-primary previous">
                        Previous
                    </button>
                     <a class="btn btn-primary next" href="https://register.ciif-global.org/login">
                        Close
                    </a>  
                </div>                
            </div>
            
           
        </div>
  </div>


 <script src="<?php echo BASE_PATH; ?>assets/onlineportal/js/jquery-1.12.4.min.js"></script>
 
   <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/webcamjs/1.0.25/webcam.js"></script>
   
    <script language="JavaScript">
    Webcam.set({
    width: 500,
    height: 254,
    image_format: 'jpeg',
      jpeg_quality: 90
    });
    Webcam.attach( '#my_camera' );
  </script>
  
 <script>
 
 var x = document.getElementById("myAudio"); 


 function play() {
  x.play();
}

function pause() {
  x.pause();
}
 
 function clickvide() {
     $("#imagevideo").hide();
     $("#my_camera").show();
 }
 
 function hidevideo(){
      $("#imagevideo").show();
     $("#my_camera").hide();
 }
    $(document).ready(function(){
    
        var current_fs, next_fs, previous_fs; //fieldsets
        var opacity;
        var current = 1;
        var steps = $(".wizard-container").length;
        
        
        $(".next").click(function(){
        
        current_fs = $(this).closest('.wizard-container');
        next_fs = $(this).closest('.wizard-container').next();
        
        
        //show the next fieldset
        next_fs.show();
        //hide the current fieldset with style
        current_fs.animate({opacity: 0}, {
            step: function(now) {
            // for making fielset appear animation
            opacity = 1 - now;
            
            current_fs.css({
            'display': 'none',
            'position': 'relative'
            });
            next_fs.css({'opacity': opacity});
            },
            duration: 500
        });

        });
        
        $(".previous").click(function(){
        
        current_fs = $(this).closest('.wizard-container');
        previous_fs = $(this).closest('.wizard-container').prev();
        
        
        //show the previous fieldset
        previous_fs.show();
        
        //hide the current fieldset with style
        current_fs.animate({opacity: 0}, {
            step: function(now) {
            // for making fielset appear animation
            opacity = 1 - now;
            
            current_fs.css({
            'display': 'none',
            'position': 'relative'
            });
            previous_fs.css({'opacity': opacity});
            },
            duration: 500
        });
        });
        
        
        $(".submit").click(function(){
            return false;
        })
    
    });    
</script>

