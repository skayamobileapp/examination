<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <title>Quiz</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo BASE_PATH;?>assets/onlineportal/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo BASE_PATH;?>assets/onlineportal/css/font-awesome.min.css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css2?family=Ubuntu:wght@300;400;500;700&display=swap" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="<?php echo BASE_PATH;?>assets/onlineportal/css/main.css" rel="stylesheet" />
  </head>
  <body>

    <nav class="navbar navbar-expand-lg navbar-light main-header">
      <div class="container">
        <a class="navbar-brand" href="#">LOGO</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample02" aria-controls="navbarsExample02" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
      
        <div class="collapse navbar-collapse" id="navbarsExample02">
            <ul class="navbar-nav header-stats align-items-center">
            <li class="nav-item active">
              <img src="<?php echo BASE_PATH; ?>assets/onlineportal/img/total_questions_icon.svg" />Total Questions <span><?php echo count($question)?></span>
            </li>
            <li class="nav-item">
              <img src="<?php echo BASE_PATH; ?>assets/onlineportal/img/total_time_icon.svg" />Total Time(min) <span><?php echo $exam->duration;?></span>
            </li>

          
            <li class="nav-item">
              <img src="<?php echo BASE_PATH; ?>assets/onlineportal/img/total_remainingtime_icon.svg" />Remaining Time  
<span  id="countdown"></span>
            </li>            
          </ul>
          <ul class="navbar-nav ml-auto align-items-center">
            <li class="nav-item">
              Welcome <?php echo $this->session->userdata['student_name'];?>
            </li>            
            <li class="nav-item active">
              <a href="/studentexamination/mockexam/logout" class="nav-link">Logout</a>
            </li>          
          </ul>          
        </div>
      </div>
    </nav> 
    <button class="btn btn-primary see-all-questions-btn">See All Questions</button>
    <div class="quiz-wrapper">
      <div class="container">
        <div class="row">
          <div class="col-md-8 col-lg-9 pt-3">

          <?php $this->load->model('student/exam_student_tagging_model');?>
          <?php 
                $j=0;
             for ($i=0;$i<count($question);$i++) {

            if($question[$i]['question_type']=='1' || $question[$i]['question_type']=='3') { 
               $j++.' ';?>

            <div class="question-container" id="questionDiv<?php echo $question[$i]['id']?>">
               <span style="display:inline;">

               <p style="float: left;margin-right: 5px;"><?php echo $question[$i]['question_number'];?>. </p><?php echo $question[$i]['question'];?></span>



               <?php if($question[$i]['question_type']=='1' || $question[$i]['question_type']=='3') {
                ?>
              <div class="row mt-2">

                 <?php 
                        $answerList = $this->exam_student_tagging_model->getAnswersByQuestionId($question[$i]['id']);

                         $answeredData = $this->exam_student_tagging_model->checkifAnswered($question[$i]['id'],$this->session->id_student_exam_attempt);

              for ($a=0;$a<count($answerList);$a++) { ?>
                  <div class="col-md-12">
                    <div class="answer-radio">
                      <div class="custom-control custom-radio">
                        <input type="radio" id="answer<?php echo $answerList[$a]->id;?>" name="quesion<?php echo $question[$i][id];?>" class="custom-control-input" 
                       <?php if($answeredData->id_answer==$answerList[$a]->id) { echo "checked=checked";} ?> onclick="updateAnswer(<?php echo $answerList[$a]->id;?>,<?php echo $question[$i]['examstudentid'];?>)">
                    <label class="custom-control-label" for="answer<?php echo $answerList[$a]->id;?>"><?php echo $answerList[$a]->option_description;?></label>
                      </div>
                    </div>
                  </div>
                <?php } ?>
                                         
              </div> 
              <?php } ?> 



            </div> 
 <?php }  } ?>



  <?php 
                $j=0;
             for ($i=0;$i<count($question);$i++) {

            if($question[$i]['question_type']=='2') { 
               $j++.' ';?>

            <div class="question-container" id="questionDiv<?php echo $question[$i]['id']?>">
               <span style="display:inline;">

               <p style="float: left;margin-right: 5px;"><?php echo $question[$i]['question_number'];?>. </p><?php echo $question[$i]['question'];?></span>



               <?php if($question[$i]['question_type']=='2') {
                ?>
              <div class="row mt-2">

                 <?php 
                           $answeredData = $this->exam_student_tagging_model->checkifAnswered($question[$i]['id'],$this->session->id_student_exam_attempt); ?>

                <textarea name='written_answer<?php echo $question[$i]['examstudentid'];?>' 
                  id='written_answer<?php echo $question[$i]['examstudentid'];?>' 
                  class='idinstructions' style='width:100%;height:120px;' 
                  onkeypress='updateAnswertext(<?php echo $question[$i]['examstudentid'];?>)'><?php echo $answeredData->answer_text;?></textarea>
                                         
              </div> 
              <?php } ?> 



            </div> 
 <?php }  } ?>

           

            <div class="d-flex py-2" style="float:right;">
              <button type="button" class="btn btn-primary btn-lg" onclick="confirmsubmit()">Submit</button>
            </div>    
          </div>
          <div class="col-md-4 col-lg-3">
           <div id="attachQuestionNumber" style="min-height: 10% !important;"></div>
             <div id="my_camera"></div>

          </div>   

        </div>
      </div>
    </div>
    <script src="<?php echo BASE_PATH; ?>assets/onlineportal/js/jquery-1.12.4.min.js"></script>

    <script src="<?php echo BASE_PATH;?>assets/onlineportal/js/bootstrap.min.js"></script>
    <script src="<?php echo BASE_PATH;?>assets/onlineportal/js/main.js"></script>

    <script type="text/javascript" src="<?php echo BASE_PATH; ?>assets/ckeditor/ckeditor.js"></script>
    <script>
      $(document).ready(function() {




CKEDITOR.replaceClass="idinstructions";

        // Mobile show all question fixed position
        $(window).scroll(function() {
          if ($(document).scrollTop() > 80) {
            $("body").addClass("mobile-questions");
          } else {
            $("body").removeClass("mobile-questions");
          }
        });

        //
        $('.see-all-questions-btn, .close-btn').on('click', function(e) {
          $('body').toggleClass('push-menu');
        });

       

CKEDITOR.on('instanceCreated', function(e) {
        e.editor.on('contentDom', function() {
            e.editor.document.on('keyup', function(event) {
                // keyup event in ckeditor
                updateAnswertext(e.editor.name);
            }
        );
    });
});



      getAllQuestionNumber();

    });


   var seconds, upgradeTime;
   var remainingminute = parseInt(<?php echo $remainingTime + $extraTime;?>);
    seconds = upgradeTime = parseInt(remainingminute)*(60);
   console.log(seconds);

  function timer() {
    var days = Math.floor(seconds / 24 / 60 / 60);
    var hoursLeft = Math.floor((seconds) - (days * 86400));
    var hours = Math.floor(hoursLeft / 3600);
    var minutesLeft = Math.floor((hoursLeft) - (hours * 3600));
    var minutes = Math.floor(upgradeTime / 60);
    var remainingSeconds = seconds % 60;
    // add a 0 in front of single digit seconds
    if (remainingSeconds < 10) {
        remainingSeconds = "0" + remainingSeconds;
    }
    // add a 0 in front of single digit minutes
    if (minutes < 10) {
        minutes = "0" + minutes;
    }

    document.getElementById('countdown').innerHTML =  minutes + ":" + remainingSeconds;

    if (minutes === 0) {
        clearInterval(countdownTimer);
        document.getElementById('countdown').innerHTML = "Completed";
         submitallQuestion();
            // reload page
    } else {
        seconds--;
    }
}
var countdownTimer = setInterval(timer, 1000);



      function togglemenu() {
        $('body').toggleClass('push-menu');
      }

       function scrolldiv(id) {
          $('body').toggleClass('push-menu');

    $('html, body').animate({scrollTop:$('#questionDiv'+id).position().top}, 'slow');


  }



    function getAllQuestionNumber() {
    $.get("/studentexamination/mockexam/getAllQuestionNumber", function(data, status){
           
                $("#attachQuestionNumber").html(data);
            });
  }

  function updateAnswer(answerid,examquestionid){
    updateAnswerImage(examquestionid);
    $.get("/studentexamination/mockexam/markanswer/"+answerid+"/"+examquestionid, function(data, status){
           
                getAllQuestionNumber();
            });
    MeasureConnectionSpeed(examquestionid);
  }






  function updateAnswertext(examquestionid) {

    var answer = CKEDITOR.instances[examquestionid].getData();


var idexam = examquestionid.replace('written_answer','');

    // console.log(value);


    // var answer = $("#written_answer"+examquestionid).val();
    // console.log(answer);
   $.ajax({
        url: "<?php echo site_url('/studentexamination/mockexam/markanswertext'); ?>",
        method: "POST",
        data: {
          answer_written: answer,
          question_id : idexam
        },
        success: function(data) {
         
          getAllQuestionNumber();
        }
      });
  }


  function confirmsubmit() {

    $.get("/studentexamination/mockexam/pendingquestion", function(data, status){
           if(data=='0') {
              var cnf = confirm("Do you really want to submit the answers");
           }
           else {
            var cnf = confirm("You have not answered "+data+" questions. Are you sure you wish to submit?");

           }


           if(cnf==true) {
             alert("Thankyou for taking part in the exam");
              $.get("/studentexamination/mockexam/submitmarks/", function(data, status){
                 
                        parent.location='/studentexamination/mockexam/thankyou';

                  });

          }


       });



     
  }


  function submitallQuestion(){
    $.get("/studentexamination/mockexam/submitmarksauto/", function(data, status){
                 
                        parent.location='/studentexamination/mockexam/thankyou';

                  });
  }

    
    window.onblur = function () {
    // do some stuff after tab was changed e.g.
   // alert('You switched the tab');
}



function updateAnswerImage(examquestionid){
      
     // Webcam.snap( function(data_uri) {
     //        $(".image-tag").val(data_uri);
     //          $.ajax({
     //                url: "/studentexamination/mockexam/answersaveimage",
     //                method: "POST",
     //                data: {
     //                  image: data_uri,
     //                  examquestionid : examquestionid
     //                },
     //                success: function(data) {
                     
     //                }
     //              });
     //    } );
}
 

var imageAddr = "https://www.rv-vlsi.com/VLSI.jpg"; 
var downloadSize = 192924; //bytes

function ShowProgressMessage(msg) {
    if (console) {
        if (typeof msg == "string") {
            console.log(msg);
        } else {
            for (var i = 0; i < msg.length; i++) {
                console.log(msg[i]);
            }
        }
    }
    
    var oProgress = document.getElementById("progress");
    if (oProgress) {
        var actualHTML = (typeof msg == "string") ? msg : msg.join("<br />");
        oProgress.innerHTML = actualHTML;
    }
}

function InitiateSpeedDetection() {
    ShowProgressMessage("Loading the image, please wait...");
    window.setTimeout(MeasureConnectionSpeed, 1);
};    

if (window.addEventListener) {
    window.addEventListener('load', InitiateSpeedDetection, false);
} else if (window.attachEvent) {
    window.attachEvent('onload', InitiateSpeedDetection);
}

function MeasureConnectionSpeed(examquestionid) {
    var startTime, endTime;
    var download = new Image();
    download.onload = function () {
        endTime = (new Date()).getTime();
        showResults(examquestionid);
    }
    
    download.onerror = function (err, msg) {
        ShowProgressMessage("Invalid image, or error downloading");
    }
    
    startTime = (new Date()).getTime();
    var cacheBuster = "?nnn=" + startTime;
    download.src = imageAddr + cacheBuster;
    
    function showResults(examquestionid) {
        var duration = (endTime - startTime) / 1000;
        var bitsLoaded = downloadSize * 8;
        var speedBps = (bitsLoaded / duration).toFixed(2);
        var speedKbps = (speedBps / 1024).toFixed(2);
        var speedMbps = (speedKbps / 1024).toFixed(2);
              $.ajax({
                    url: "/studentexamination/mockexam/internetspeed",
                    method: "POST",
                    data: {
                      image: speedKbps,
                      examquestionid : examquestionid

                    },
                    success: function(data) {
                     
                    }
                  });
       
    }
}
        
        
        


    </script>


     
  <!-- First, include the Webcam.js JavaScript Library -->
     <!--  <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/webcamjs/1.0.25/webcam.js"></script>
 -->
  
  <!-- Configure a few settings and attach camera -->
  <!-- <script language="JavaScript">
    Webcam.set({
      width: 320,
      height: 240,
      image_format: 'jpeg',
      jpeg_quality: 90
    });
    Webcam.attach( '#my_camera' );
  </script> -->
  
  <!-- A button for taking snaps -->

  <!-- Code to handle taking the snapshot and displaying it locally -->
 <!--  <script language="JavaScript">
    function take_snapshot() {
      // take snapshot and get image data
      Webcam.snap( function(data_uri) {
        // display results in page
        document.getElementById('results').innerHTML = 
          '<h2>Here is your image:</h2>' + 
          '<img src="'+data_uri+'"/>';
      } );
    }
  </script> -->


  </body>
</html>
