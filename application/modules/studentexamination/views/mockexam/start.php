<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
      <meta name="description" content="" />
      <title>Quiz</title>
      <!-- Bootstrap core CSS -->
      <link href="http://examination.com/assets/onlineportal/css/bootstrap.min.css" rel="stylesheet" />
      <link href="http://examination.com/assets/onlineportal/css/font-awesome.min.css" rel="stylesheet" />
      <link href="https://fonts.googleapis.com/css2?family=Ubuntu:wght@300;400;500;700&display=swap" rel="stylesheet">
      <!-- Custom styles for this template -->
      <link href="http://examination.com/assets/onlineportal/css/main.css" rel="stylesheet" />
   </head>
   <style type="text/css">
      p{ margin-bottom: 5px !important;}
   </style>
   <body id="specialstuff" style="overflow-x:auto;">
      <nav class="navbar navbar-expand-lg navbar-light main-header" style="padding:2px 2px;">
         <div class="container-fluid">
            <a class="navbar-brand" href="#">LOGO</a>
            <div id="my_camera"></div>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample02" aria-controls="navbarsExample02" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarsExample02" >
               <ul class="navbar-nav header-stats align-items-center" id="headerdatadiv" style="display:none;">
                  <li class="nav-item active">
                     <img src="http://examination.com/assets/onlineportal/img/total_questions_icon.svg" />Total Questions <span>43</span>
                  </li>
                  <li class="nav-item">
                     <img src="http://examination.com/assets/onlineportal/img/total_time_icon.svg" />Total Time(min) <span>120</span>
                  </li>
                  <li class="nav-item">
                     <img src="http://examination.com/assets/onlineportal/img/total_remainingtime_icon.svg" />Remaining Time  
                     <span  id="countdown"></span>
                     <span  id="countdown1"></span>
                  </li>
               </ul>
               <ul class="navbar-nav ml-auto align-items-center">
                  <li class="nav-item">
                     Welcome Dr. Kiran kumar                         
                  </li>
                  <li class="nav-item active">
                     <a href="https://register.ciif-global.org/" class="nav-link">Logout</a>
                  </li>
               </ul>
            </div>
         </div>
         <button class="btn btn-primary see-all-questions-btn">See All Questions</button>      
      </nav>
      <div class="quiz-wrapper" style="background:white;" id="quiz-wrapper">
         <div class="container">
            <div id="staticdivid">
               <div class="row" >
                  <div class="col-sm-4">
                     <img src='/assets/images/photo1.jpeg' class="img-fluid capture-id-img"/>
                     <h4 class="mt-3">Capture your ID / Photo</h4>
                  </div>
                  <div class="col-sm-4">
                     <div id="results"><img src="/" class="img-fluid capture-id-img"/></div>
                     <button id='imagebtn'   type="button" class="btn btn-primary start-btn mt-3" onclick="back()">Capture New Image</button>
                  </div>
                  <div class="col-sm-4" >
                     <div class="row">
                        <div class="col-sm-12">
                           <div class="form-group">
                              <input type='text' name='token' id='token' class="form-control" placeholder="Enter Token" value="125972" readonly>            
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-sm-12">
                           <button id='fsbutton'  type="button" class="btn btn-primary start-btn" >Click to Start</button>  
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-sm-12">
                     <br/>
                     <div class="row exam-message" id='showNotificationDivId' style="display:none;">
                        <div class="col-sm-12">
                           <h5 style="color:red;">Notification</h5>
                           <table class="table">
                              <tr>
                                 <th style="color:red;" id="latestinstruction"></th>
                              </tr>
                           </table>
                        </div>
                     </div>
                     <div class="exam-message" style="font-size:18px;">
                        <h4>Important</h4>
                        <ol>
                           <li>Candidates are required to log on to the examination platform and/or video conferencing
                              platform at least forty-five (45) minutes before the start of the examination. Candidates who log into the examination
                              platform forty-five (45) minutes after an examination has commenced, shall not be permitted to take the examination.
                           </li>
                           <li>
                              Online examinations allow candidates to take one five-minute unscheduled break with the exam clock paused. For
                              online proctored exams, you are only able to leave your exam room/desk/screen during the five-minute ‘Pause the Clock’
                              break.
                           </li>
                           <li>
                              The exam timer will not stop for any breaks beyond the one five-minute unscheduled break. Any subsequent breaks will
                              not be given the ‘Pause the clock’ opportunity.
                           </li>
                           <li>
                              If a candidate does happen to lose connection to the exam momentarily, whether due to internet issues or a loss of
                              power, the exam will temporarily shut down. Once internet or power is regained, the candidate will be able to relaunch
                              the exam from where he/she left off and the progress will have been saved.
                           </li>
                           <li>
                              In the event of a disconnection, the exam timer will stop and once re-launched, the candidate is able to resume from the
                              same question the moment he/she left (got disconnected from) the exam.
                           </li>
                           <li>
                              However, if internet connection and/or power is lost for more than 30 minutes, the exam will be discontinued and the
                              candidate is required to re-take the exam in the next scheduled examination sitting.
                           </li>
                           <li>
                              If internet connection and/or power is lost permanently less than 80% into the time after an examination has
                              commenced, the candidate is required to re-take the exam in the next scheduled examination sitting.
                           </li>
                           <li>
                              If internet connection and/or power is lost permanently more than 80% into the time after an examination has
                              commenced, the candidate is required to submit the exam as is.
                           </li>
                        </ol>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row" id="datadivid" style="display:none;">
               <div class="col-md-8 pt-3">
                  <div class="question-container" id="questionDiv1" style="display: none;">
                     <div class="question-container-inner" style="height:700px;">
                        <div class="d-flex">
                           <p class="mr-2 text-nowrap">1. </p>
                           <div>
                              <p>Apart from the requirement to abide by the <em>Shariah</em>, there are also moral and legal duties required by Islamic financial professionals in fulfilling their agreed promise, and these include</p>
                           </div>
                        </div>
                        <div class="row mt-2">
                           <div class="col-md-12">
                              <div class="answer-radio">
                                 <div class="custom-control custom-radio">
                                    <input type="radio" id="answer452" name="quesion1" class="custom-control-input" 
                                       onclick="updateAnswer(452,2)">
                                    <label class="custom-control-label" for="answer452">
                                       <p>The requirement not to indulge in exploitative practices.</p>
                                    </label>
                                 </div>
                              </div>
                           </div>
                           <div class="col-md-12">
                              <div class="answer-radio">
                                 <div class="custom-control custom-radio">
                                    <input type="radio" id="answer453" name="quesion145" class="custom-control-input" 
                                       onclick="updateAnswer(453,2)">
                                    <label class="custom-control-label" for="answer453">
                                       <p>The presence of Ta’wun (helping others) in cooperating to promote sustainability and CSR and cooperative competition in business activity.</p>
                                    </label>
                                 </div>
                              </div>
                           </div>
                           <div class="col-md-12">
                              <div class="answer-radio">
                                 <div class="custom-control custom-radio">
                                    <input type="radio" id="answer454" name="quesion145" class="custom-control-input" 
                                       onclick="updateAnswer(454,2)">
                                    <label class="custom-control-label" for="answer454">
                                       <p>The additional requirement for transparency and disclosure</p>
                                    </label>
                                 </div>
                              </div>
                           </div>
                           <div class="col-md-12">
                              <div class="answer-radio">
                                 <div class="custom-control custom-radio">
                                    <input type="radio" id="answer455" name="quesion145" class="custom-control-input" 
                                       onclick="updateAnswer(455,2)">
                                    <label class="custom-control-label" for="answer455">
                                       <p>All of these</p>
                                    </label>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="row pt-4 pb-2">
                        <div class="col-6">
                        </div>
                        <div class="col-6 d-flex justify-content-end">
                           <input type="button" class="btn btn-primary" value="NEXT" onclick="nextQuestion(143)">
                        </div>
                     </div>
                  </div>
                  
                  <div class="question-container" id="questionDiv183" style="display: none;">
                     <div class="question-container-inner">
                        <div class="d-flex">
                           <p class="mr-2 text-nowrap">1 -A. </p>
                           <div>
                              <p>What are the moral and legal duties expected of Islamic financial practitioners in fulfilling their agreed promise apart from the requirement to abide by the <em>Shariah</em></p>
                              <ul></ul>
                           </div>
                        </div>
                        <div class="row mt-2">
                           <div class="col-md-12">
                              <textarea name='written_answer1' 
                                 id='written_answer1' 
                                 class='idinstructions' style='width:100%;height:120px;' 
                                 onkeypress='updateAnswertext(1)'></textarea>
                           </div>
                        </div>
                     </div>
                     <div class="row pt-4 pb-2">
                        <div class="col-6">
                           <input type="button" class="btn btn-primary" value="Previous" onclick="nextQuestion(169)">
                        </div>
                        <div class="col-6 d-flex justify-content-end">
                           <input type="button" class="btn btn-primary" value="NEXT" onclick="nextQuestion(185)">
                        </div>
                     </div>
                  </div>
                  <div class="question-container" id="questionDiv1">
                   <span style="display:inline;">
                      <p style="float: left;margin-right: 5px;">1. </p>
                      <p>Mr. Ismail failed to pay his financing installment and was charged a penalty of RM100 for late payment.</p>
                      <p>What type of <em>riba </em>occurs in this scenario?</p>
                   </span>
                   <div class="row mt-2">
                      <div class="col-md-12">
                         <div class="answer-radio">
                            <div class="custom-control custom-radio">
                               <input type="radio" id="answer118" name="quesion1" class="custom-control-input" 
                                  onclick="updateAnswer(1)">
                               <label class="custom-control-label" for="answer118">
                                  <p><em>Riba al-Fadl</em></p>
                               </label>
                            </div>
                         </div>
                      </div>
                      <div class="col-md-12">
                         <div class="answer-radio">
                            <div class="custom-control custom-radio">
                               <input type="radio" id="answer119" name="quesion1" class="custom-control-input" 
                                  onclick="updateAnswer(1)">
                               <label class="custom-control-label" for="answer119">
                                  <p><em>Riba al-Nasiah</em></p>
                               </label>
                            </div>
                         </div>
                      </div>
                      <div class="col-md-12">
                         <div class="answer-radio">
                            <div class="custom-control custom-radio">
                               <input type="radio" id="answer120" name="quesion1" class="custom-control-input" 
                                  onclick="updateAnswer(1)">
                               <label class="custom-control-label" for="answer120">
                                  <p><em>Riba al-Buyu’</em></p>
                               </label>
                            </div>
                         </div>
                      </div>
                      <div class="col-md-12">
                         <div class="answer-radio">
                            <div class="custom-control custom-radio">
                               <input type="radio" id="answer121" name="quesion1" class="custom-control-input" 
                                  onclick="updateAnswer(1)">
                               <label class="custom-control-label" for="answer121">
                                  <p><em>Riba al-Qard </em></p>
                               </label>
                            </div>
                         </div>
                      </div>
                   </div>
                   <div class="row pt-4 pb-2">
                      <div class="col-6">
                      </div>
                      <div class="col-6 d-flex justify-content-end">
                         <input type="button" class="btn btn-primary" value="NEXT" onclick="nextQuestion(2)">
                      </div>
                   </div>
                </div>
                <div class="question-container" id="questionDiv2" style="display:none;">
                   <span style="display:inline;">
                      <p style="float: left;margin-right: 5px;">2. </p>
                      <p>Apart from the Quran and Sunnah, which of these are the sources of <em>Ahkam</em> (ruling) agreed upon by the overwhelming majority of past and present scholars?</p>
                      <ol>
                         <li>Qiyas</li>
                         <li>Urf</li>
                         <li>Ijma’.</li>
                         <li>Istihsan</li>
                      </ol>
                   </span>
                   <div class="row mt-2">
                      <div class="col-md-12">
                         <div class="answer-radio">
                            <div class="custom-control custom-radio">
                               <input type="radio" id="answer126" name="quesion3" class="custom-control-input" 
                                  onclick="updateAnswer(2)">
                               <label class="custom-control-label" for="answer126">
                                  <p>2 and 4</p>
                               </label>
                            </div>
                         </div>
                      </div>
                      <div class="col-md-12">
                         <div class="answer-radio">
                            <div class="custom-control custom-radio">
                               <input type="radio" id="answer127" name="quesion3" class="custom-control-input" 
                                  onclick="updateAnswer(2)">
                               <label class="custom-control-label" for="answer127">
                                  <p>2 and 3</p>
                               </label>
                            </div>
                         </div>
                      </div>
                      <div class="col-md-12">
                         <div class="answer-radio">
                            <div class="custom-control custom-radio">
                               <input type="radio" id="answer128" name="quesion3" class="custom-control-input" 
                                  onclick="updateAnswer(2)">
                               <label class="custom-control-label" for="answer128">
                                  <p>1 and 3</p>
                               </label>
                            </div>
                         </div>
                      </div>
                      <div class="col-md-12">
                         <div class="answer-radio">
                            <div class="custom-control custom-radio">
                               <input type="radio" id="answer129" name="quesion3" class="custom-control-input" 
                                  onclick="updateAnswer(2)">
                               <label class="custom-control-label" for="answer129">
                                  <p>1 and 4</p>
                               </label>
                            </div>
                         </div>
                      </div>
                   </div>
                   <div class="row pt-4 pb-2">
                      <div class="col-6">
                         <input type="button" class="btn btn-primary" value="PREVIOUS" onclick="nextQuestion(1)">
                      </div>
                      <div class="col-6 d-flex justify-content-end">
                         <input type="button" class="btn btn-primary" value="NEXT" onclick="nextQuestion(3)">
                      </div>
                   </div>
                </div>
                <div class="question-container" id="questionDiv3" style="display:none;">
                   <span style="display:inline;">
                      <p style="float: left;margin-right: 5px;">3. </p>
                      <p>What type of <em>riba </em>occurs when 100KG of Grade A dates is exchanged on the spot with 200KG of Grade B dates?</p>
                   </span>
                   <div class="row mt-2">
                      <div class="col-md-12">
                         <div class="answer-radio">
                            <div class="custom-control custom-radio">
                               <input type="radio" id="answer130" name="quesion4" class="custom-control-input" 
                                  onclick="updateAnswer(3)">
                               <label class="custom-control-label" for="answer130">
                                  <p><em>Riba al-Qard </em></p>
                               </label>
                            </div>
                         </div>
                      </div>
                      <div class="col-md-12">
                         <div class="answer-radio">
                            <div class="custom-control custom-radio">
                               <input type="radio" id="answer131" name="quesion4" class="custom-control-input" 
                                  onclick="updateAnswer(3)">
                               <label class="custom-control-label" for="answer131">
                                  <p><em>Riba al-Fadl</em></p>
                               </label>
                            </div>
                         </div>
                      </div>
                      <div class="col-md-12">
                         <div class="answer-radio">
                            <div class="custom-control custom-radio">
                               <input type="radio" id="answer132" name="quesion4" class="custom-control-input" 
                                  onclick="updateAnswer(3)">
                               <label class="custom-control-label" for="answer132">
                                  <p><em>Riba al-Buyu’</em></p>
                               </label>
                            </div>
                         </div>
                      </div>
                      <div class="col-md-12">
                         <div class="answer-radio">
                            <div class="custom-control custom-radio">
                               <input type="radio" id="answer133" name="quesion4" class="custom-control-input" 
                                  onclick="updateAnswer(3)">
                               <label class="custom-control-label" for="answer133">
                                  <p><em>Riba al-Nasiah</em></p>
                               </label>
                            </div>
                         </div>
                      </div>
                   </div>
                   <div class="row pt-4 pb-2">
                      <div class="col-6">
                         <input type="button" class="btn btn-primary" value="PREVIOUS" onclick="nextQuestion(2)">
                      </div>
                      <div class="col-6 d-flex justify-content-end">
                         <input type="button" class="btn btn-primary" value="NEXT" onclick="nextQuestion(4)">
                      </div>
                   </div>
                </div>
                <div class="question-container" id="questionDiv4" style="display:none;">
                   <span style="display:inline;">
                      <p style="float: left;margin-right: 5px;">4. </p>
                      <p><em>Fiqh</em> technically means:</p>
                   </span>
                   <div class="row mt-2">
                      <div class="col-md-12">
                         <div class="answer-radio">
                            <div class="custom-control custom-radio">
                               <input type="radio" id="answer134" name="quesion5" class="custom-control-input" 
                                  onclick="updateAnswer(4)">
                               <label class="custom-control-label" for="answer134">
                                  <p>Knowledge of Islamic beliefs that have been derived from specific sources.</p>
                               </label>
                            </div>
                         </div>
                      </div>
                      <div class="col-md-12">
                         <div class="answer-radio">
                            <div class="custom-control custom-radio">
                               <input type="radio" id="answer135" name="quesion5" class="custom-control-input" 
                                  onclick="updateAnswer(4)">
                               <label class="custom-control-label" for="answer135">
                                  <p>Knowledge of the <em>Shariah</em> pertaining to ethics that have been derived from specific evidence.</p>
                               </label>
                            </div>
                         </div>
                      </div>
                      <div class="col-md-12">
                         <div class="answer-radio">
                            <div class="custom-control custom-radio">
                               <input type="radio" id="answer136" name="quesion5" class="custom-control-input" 
                                  onclick="updateAnswer(4)">
                               <label class="custom-control-label" for="answer136">
                                  <p>Knowledge of the <em>Shariah</em> pertaining to conduct that have been derived from specific evidence.</p>
                               </label>
                            </div>
                         </div>
                      </div>
                      <div class="col-md-12">
                         <div class="answer-radio">
                            <div class="custom-control custom-radio">
                               <input type="radio" id="answer137" name="quesion5" class="custom-control-input" 
                                  onclick="updateAnswer(4)">
                               <label class="custom-control-label" for="answer137">
                                  <p>Knowledge of the <em>Shariah</em> pertaining to conduct that have been derived from the Qur’an and Sunnah only.</p>
                               </label>
                            </div>
                         </div>
                      </div>
                   </div>
                   <div class="row pt-4 pb-2">
                      <div class="col-6">
                         <input type="button" class="btn btn-primary" value="PREVIOUS" onclick="nextQuestion(3)">
                      </div>
                      <div class="col-6 d-flex justify-content-end">
                         <input type="button" class="btn btn-primary" value="NEXT" onclick="nextQuestion(5)">
                      </div>
                   </div>
                </div>
                <div class="question-container" id="questionDiv5" style="display:none;">
                                    <span style="display:inline;">
                        <p style="float: left;margin-right: 5px;">5. </p>
                        <p>What are the moral and legal duties expected of Islamic financial practitioners in fulfilling their agreed promise apart from the requirement to abide by the Shariah</p>
                     </span>
                     <div class="row mt-2">
                        <div class="col-md-12">
                           <textarea name='written_answer' 
                              id='written_answer' 
                              class='idinstructions' style='width:100%;height:120px;' onkeypress="markanswer(5)"></textarea>
                        </div>
                     </div>
                     <div class="row pt-4 pb-2">
                        <div class="col-6">
                           <input type="button" class="btn btn-primary" value="PREVIOUS" onclick="nextQuestion(4)">
                        </div>
                        <div class="col-6 d-flex justify-content-end">
                           <input type="button" class="btn btn-primary" value="NEXT" onclick="nextQuestion(6)">
                        </div>
                     </div>
                  </div>
                  <div class="question-container" id="questionDiv6" style="display:none;">
                      <span style="display:inline;">
                        <p style="float: left;margin-right: 5px;">6. </p>
                        <p>Analyse the nature of unethical conduct in the financial service industries, and demonstrate how unethical conduct can be eliminated in the Islamic financial industry using unique principles of Islamic ethics.</p>
                     </span>
                     <div class="row mt-2">
                        <div class="col-md-12">
                           <textarea name='written_answer' 
                              id='written_answer' 
                              class='idinstructions' style='width:100%;height:120px;' onkeypress="markanswer(6)"></textarea>
                        </div>
                     </div>
                     <div class="row pt-4 pb-2">
                        <div class="col-6">
                           <input type="button" class="btn btn-primary" value="PREVIOUS" onclick="nextQuestion(5)">
                        </div>
                        <div class="col-6 d-flex justify-content-end">
                        </div>
                     </div>
                  </div>
                  <div class="d-flex py-2" style="float:right;">
                     <button type="button" class="btn btn-primary btn-lg" onclick="confirmsubmit()">Submit</button>
                  </div>
               </div>
               <div class="col-md-4">
                  <div id="attachQuestionNumber" style="min-height: 100vh !important;">
                     <div class='questions-list-container'>
                        <h5>MCQ <button class='btn btn-primary btn-sm close-btn' onclick='togglemenu()'>Close</button></h5>
                        <ul class='questions-list'>
                           <li class='' onclick='scrolldiv(1)' id='answer1'>1</li>
                           <li class='' onclick='scrolldiv(2)' id='answer2'>2</li>
                           <li class='' onclick='scrolldiv(3)'  id='answer3'>3</li>
                           <li class='' onclick='scrolldiv(4)'  id='answer4'>4</li>
                        </ul>
                        <h5>Essay <button class='btn btn-primary btn-sm close-btn' onclick='togglemenu()'>Close</button></h5>
                        <ul class='questions-list'>
                           <li class='' onclick='scrolldiv(5)'  id='answer5'>5</li>
                           <li class='' onclick='scrolldiv(6)'  id='answer6'>6</li>
                        </ul>
                        <ul class='question-list-help'>
                           <li>Not Attempted</li>
                           <li class='answered'>Answered</li>
                           <li style='width:100%'>Scroll down at last to submit the answer</li>
                                           <input type="button" value="Go Fullscreen" id="fsbutton" />
                      <p style="display:none;">Status: <span id="fsstatus"></span></p>

                        </ul>
                     </div>
                  </div>
               </div>
            </div>
            <div class="container">
               <!-- Modal -->
               <div class="modal fade" id="myModal" role="dialog">
                  <div class="modal-dialog">
                     <!-- Modal content-->
                     <div class="modal-content">
                        <div class="modal-body">
                           <p><span id='numberofquestion'></span> Number of questions are still pending to answer. Do you really want to submit the exam?</p>
                        </div>
                        <div class="modal-footer">
                           <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                           <button type="button" class="btn btn-success" onclick="closeexam()">Yes</button>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="custom-chatbot">
               <img src="/assets/images/chat.png" style="height: 124px; border-radius: 999px;" class="btn btn-primary chatbot-btn"><br/><span style="font-weight: bold;color:#00bcd2;">Live Support</span>
               <div class="chatbot-container">
                  <div class="chatbot-header">
                     <div class="header-text">Chat with Invigilator</div>
                     <button class="btn chatbot-close">
                        <svg
                           fill="#FFFFFF"
                           height="15"
                           viewBox="0 0 15 15"
                           width="15"
                           xmlns="http://www.w3.org/2000/svg"
                           >
                           <line
                              x1="1"
                              y1="15"
                              x2="15"
                              y2="1"
                              stroke="white"
                              stroke-width="1"
                              ></line>
                           <line
                              x1="1"
                              y1="1"
                              x2="15"
                              y2="15"
                              stroke="white"
                              stroke-width="1"
                              ></line>
                        </svg>
                     </button>
                  </div>
                  <div class="chat-room">
                     <div class="message-area" id='chatdetails'>
                     </div>
                     <div class="input-group mb-3 input-group-lg">
                        <input
                           type="text"
                           class="form-control"
                           id='chat'
                           placeholder="Enter Message"
                           />
                        <div class="input-group-append">
                           <button class="btn btn-primary" type="button" onclick="chatdata()">Send</button>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <script src="http://examination.com/assets/onlineportal/js/jquery-1.12.4.min.js"></script>
      <script src="http://examination.com/assets/onlineportal/js/bootstrap.min.js"></script>
      <script src="http://examination.com/assets/onlineportal/js/main.js"></script>
      <script type="text/javascript" src="http://examination.com/assets/ckeditor/ckeditor.js"></script>
      <style type="text/css">
         .modal-open .modal.modal-center {
         display: flex!important;
         align-items: center!important;
         .modal-dialog {
         flex-grow: 1;
         }
         }
      </style>
      <script>
         /* 
         Native FullScreen JavaScript API
         -------------
         Assumes Mozilla naming conventions instead of W3C for now
         */
         
         (function() {
           var 
             fullScreenApi = { 
               supportsFullScreen: false,
               isFullScreen: function() { return false; }, 
               requestFullScreen: function() {}, 
               cancelFullScreen: function() {},
               fullScreenEventName: '',
               prefix: ''
             },
             browserPrefixes = 'webkit moz o ms khtml'.split(' ');
           
           // check for native support
           if (typeof document.cancelFullScreen != 'undefined') {
             fullScreenApi.supportsFullScreen = true;
           } else {   
             // check for fullscreen support by vendor prefix
             for (var i = 0, il = browserPrefixes.length; i < il; i++ ) {
               fullScreenApi.prefix = browserPrefixes[i];
               
               if (typeof document[fullScreenApi.prefix + 'CancelFullScreen' ] != 'undefined' ) {
                 fullScreenApi.supportsFullScreen = true;
                 
                 break;
               }
             }
           }
           
           // update methods to do something useful
           if (fullScreenApi.supportsFullScreen) {
             fullScreenApi.fullScreenEventName = fullScreenApi.prefix + 'fullscreenchange';
             
             fullScreenApi.isFullScreen = function() {
               switch (this.prefix) {  
                 case '':
                   return document.fullScreen;
                 case 'webkit':
                   return document.webkitIsFullScreen;
                 default:
                   return document[this.prefix + 'FullScreen'];
               }
             }
             fullScreenApi.requestFullScreen = function(el) {
               return (this.prefix === '') ? el.requestFullScreen() : el[this.prefix + 'RequestFullScreen']();
             }
             fullScreenApi.cancelFullScreen = function(el) {
               return (this.prefix === '') ? document.cancelFullScreen() : document[this.prefix + 'CancelFullScreen']();
             }   
           }
         
           // jQuery plugin
           if (typeof jQuery != 'undefined') {
             jQuery.fn.requestFullScreen = function() {
           
               return this.each(function() {
                 var el = jQuery(this);
                 if (fullScreenApi.supportsFullScreen) {
                   fullScreenApi.requestFullScreen(el);
                 }
               });
             };
           }
         
           // export api
           window.fullScreenApi = fullScreenApi; 
         })();
         
      </script>
      <script>
         // do something interesting with fullscreen support
         var fsButton = document.getElementById('fsbutton'),
           fsElement = document.getElementById('specialstuff'),
           fsStatus = document.getElementById('fsstatus');
         
         
         if (window.fullScreenApi.supportsFullScreen) {
           fsStatus.innerHTML = 'YES: Your browser supports FullScreen';
           fsStatus.className = 'fullScreenSupported';
           
           // handle button click
           fsButton.addEventListener('click', function() {
         
             $("#datadivid").show();
             $("#headerdatadiv").show();
         
         
             $("#staticdivid").hide();
             window.fullScreenApi.requestFullScreen(fsElement);
           }, true);
         
         
           
           fsElement.addEventListener(fullScreenApi.fullScreenEventName, function() {
             if (fullScreenApi.isFullScreen()) {
               fsStatus.innerHTML = 'Whoa, you went fullscreen';
                   $("#datadivid").show();   
                    $("#headerdatadiv").show();
         
             $("#staticdivid").hide();
         
             } else {
                   $("#datadivid").hide();
                   $("#staticdivid").show();
                 $("#headerdatadiv").hide();
         
               fsStatus.innerHTML = 'Back to normal';
         
         
               $.get("/studentexamination/exam/lockexam", function(data, status){
               alert('You moved away from screen, So the exam is locked, please ask administrator to unlock it');
                    parent.location='https://ciifiscore.ciif-global.org/studentexamination/exam/start';
                    
               });
         
                         
         
             }
           }, true);
           
         } else {
           fsStatus.innerHTML = 'SORRY: Your browser does not support FullScreen';
         }
         
         
         $(document).on('keydown', function(event) {
                if(event.key=='Control' || event.key=='Meta'  || event.key=='Alt' ) {
         
                  $.get("/studentexamination/exam/lockexam", function(data, status){
                   alert('You moved away from screen, So the exam is locked, please ask administrator to unlock it');
                        parent.location='https://ciifiscore.ciif-global.org/studentexamination/exam/start';
                        
                   });
         
         
                }
            });
         
         
      </script>
      <script>
         function back(){
           history.back();
         }
         var reloaded =0;
           function chatdata(){
         
               var chat = $("#chat").val();
         
                 $.ajax({
           url: "/studentexamination/exam/chat",
           method: "POST",
           data: {
             chat: chat
           },
           success: function(data) {
            
             getchatdata();
                     }
         });
         
         
           }
         
         
           function getchatdata(){
                 $.get("/studentexamination/exam/chatdisplay", function(data, status){
              
                   $("#chatdetails").html(data);
                   $("#chat").val('');
               });
           }
         
         
         function nextQuestion(id) {
         
           scrolldiv(id);  
         }
         $(document).ready(function() {
         
         
         //      $.get("/studentexamination/exam/gettime", function(data, status){
         //   var seconds, upgradeTime;
         
         //   var jsonparseData = jQuery.parseJSON(data);
         
         //    var remainingminute = parseInt(jsonparseData['remainingTime']) + parseInt( jsonparseData['extraTime'])   + parseInt( jsonparseData['extra_two_time'])  + parseInt( jsonparseData['bulk_extra_time_two']) + parseInt(jsonparseData['bulkextraTime'] ) ;
         
         //    // document.getElementById('countdown').innerHTML='';
              
          
         //     seconds = upgradeTime = parseInt(remainingminute)*(60);
         
         //   function timer() {
         //     var minutes = Math.floor(seconds / 60);
         //     var remainingSeconds = seconds % 60;
         //     // add a 0 in front of single digit seconds
         //     if (remainingSeconds < 10) {
         //         remainingSeconds = "0" + remainingSeconds;
         //     }
         //     // add a 0 in front of single digit minutes
         //     if (minutes < 10) {
         //         minutes = "0" + minutes;
         //     }
         
         //     document.getElementById('countdown').innerHTML =  minutes + ":" + remainingSeconds;
         
         //      if (minutes < 1) {
         //         submitallQuestion();
         //     }
         
         //     if (minutes === 0) {
         //         clearInterval(countdownTimer);
         //         document.getElementById('countdown').innerHTML = "Completed";
         //          submitallQuestion();
         //             // reload page
         //     } else {
         //         seconds--;
         //     }
         // }
         // var countdownTimer = setInterval(timer, 1000);
             
         
            
         // });
         
         
         
         
         
         
             var firstid = 145;
             $("#questionDiv"+firstid).show();
         
         
         CKEDITOR.replaceClass="idinstructions";
         
         getchatdata();
         
         
         
           // Mobile show all question fixed position
           $(window).scroll(function() {
             if ($(document).scrollTop() > 80) {
               $("body").addClass("mobile-questions");
             } else {
               $("body").removeClass("mobile-questions");
             }
           });
         
           //
           $('.see-all-questions-btn, .close-btn').on('click', function(e) {
             $('body').toggleClass('push-menu');
           });
         
          
         
         CKEDITOR.on('instanceCreated', function(e) {
           e.editor.on('contentDom', function() {
               e.editor.document.on('keyup', function(event) {
                   // keyup event in ckeditor
                   updateAnswertext(e.editor.name);
               }
           );
         });
         });
         
         
         
         getAllQuestionNumber();
         
         
         
         });
         
         
         
         
         
         
         function togglemenu() {
           $('body').toggleClass('push-menu');
         }
         
          function scrolldiv(id) {
             $('body').toggleClass('push-menu');
         
         
             $(".question-container").hide();
             $("#questionDiv"+id).show();
         
         $('html, body').animate({scrollTop:$('#questionDiv'+id).position().top}, 'slow');
         
         
         }
         
         $(document).on("keydown",function(ev){
         console.log(ev.keyCode);
         if(ev.keyCode===27||ev.keyCode===122) return false
         
         
         })
         
         $(document).on("click","#cust_btn",function(){
         
         $("#myModal").modal("toggle");
         
         })
         
         
         function getAllQuestionNumber() {
         $.get("/studentexamination/exam/getAllQuestionNumber", function(data, status){
              
                   $("#attachQuestionNumberList").html(data);
               });
         }
         
         function updateAnswer(answerid,examquestionid){
         updateAnswerImage(examquestionid);
         $.get("/studentexamination/exam/markanswer/"+answerid+"/"+examquestionid, function(data, status){
              
                   getAllQuestionNumber();
               });
         MeasureConnectionSpeed(examquestionid);
         }
         
         
         
         
         
         
         function updateAnswertext(examquestionid) {
         
         var answer = CKEDITOR.instances[examquestionid].getData();
         
         
         var idexam = examquestionid.replace('written_answer','');
         
         // console.log(value);
         
         
         // var answer = $("#written_answer"+examquestionid).val();
         // console.log(answer);
         $.ajax({
           url: "/studentexamination/exam/markanswertext",
           method: "POST",
           data: {
             answer_written: answer,
             question_id : idexam
           },
           success: function(data) {
            
             getAllQuestionNumber();
           }
         });
         }
         
         
         function confirmsubmit() {
         
         $.get("/studentexamination/exam/pendingquestion", function(data, status){
              $("#numberofquestion").html(data);
              if(data==0) {
               closeexam();
              } else {
              $('#myModal').modal('show')
            }
         
         
          });
         
         
         
         
         }
         
         
         function closeexam(){
         $.get("/studentexamination/exam/submitmarks/", function(data, status){
                    
                           parent.location='/studentexamination/exam/thankyou';
         
                     });
         }
         
         
         function submitallQuestion(){
         $.get("/studentexamination/exam/submitmarksauto/", function(data, status){
                    
                           parent.location='/studentexamination/exam/thankyou';
         
                     });
         }
         
         
         window.onblur = function () {
         }
         
         
         
         function updateAnswerImage(examquestionid){
         
         Webcam.snap( function(data_uri) {
                $(".image-tag").val(data_uri);
                  $.ajax({
                        url: "/studentexamination/exam/answersaveimage",
                        method: "POST",
                        data: {
                          image: data_uri,
                          examquestionid : examquestionid
                        },
                        success: function(data) {
                        
                        }
                      });
            } );
         }
         
         
         var imageAddr = "https://www.rv-vlsi.com/VLSI.jpg"; 
         var downloadSize = 192924; //bytes
         
         function ShowProgressMessage(msg) {
         if (console) {
           if (typeof msg == "string") {
               console.log(msg);
           } else {
               for (var i = 0; i < msg.length; i++) {
                   console.log(msg[i]);
               }
           }
         }
         
         var oProgress = document.getElementById("progress");
         if (oProgress) {
           var actualHTML = (typeof msg == "string") ? msg : msg.join("<br />");
           oProgress.innerHTML = actualHTML;
         }
         }
         
         function InitiateSpeedDetection() {
         ShowProgressMessage("Loading the image, please wait...");
         window.setTimeout(MeasureConnectionSpeed, 1);
         };    
         
         if (window.addEventListener) {
         window.addEventListener('load', InitiateSpeedDetection, false);
         } else if (window.attachEvent) {
         window.attachEvent('onload', InitiateSpeedDetection);
         }
         
         function MeasureConnectionSpeed(examquestionid) {
         var startTime, endTime;
         var download = new Image();
         download.onload = function () {
           endTime = (new Date()).getTime();
           showResults(examquestionid);
         }
         
         download.onerror = function (err, msg) {
           ShowProgressMessage("Invalid image, or error downloading");
         }
         
         startTime = (new Date()).getTime();
         var cacheBuster = "?nnn=" + startTime;
         download.src = imageAddr + cacheBuster;
         
         function showResults(examquestionid) {
           var duration = (endTime - startTime) / 1000;
           var bitsLoaded = downloadSize * 8;
           var speedBps = (bitsLoaded / duration).toFixed(2);
           var speedKbps = (speedBps / 1024).toFixed(2);
           var speedMbps = (speedKbps / 1024).toFixed(2);
                 $.ajax({
                       url: "/studentexamination/exam/internetspeed",
                       method: "POST",
                       data: {
                         image: speedKbps,
                         examquestionid : examquestionid
         
                       },
                       success: function(data) {
                        
                       }
                     });
          
         }
         }
           
           
           
         
         
      </script>
      <script type="text/javascript">
         var countdownTimer,showtime;
         showtime = 0;
         var onloadfirsttime = 0;
               var seconds, upgradeTime;
               var minutes,remainingminute,remainingSeconds;
         
           setInterval(function() {
         
           $.get("/studentexamination/exam/gettime", function(data, status){
         
             var jsonparseData = jQuery.parseJSON(data);
              if((jsonparseData['extra_time_updated_status']!='1' && parseInt( jsonparseData['extraTime'])>0) || 
               (jsonparseData['bulk_extra_time_two_status']!='1' && parseInt( jsonparseData['bulk_extra_time_two'])>0)   ||  
               (jsonparseData['extra_two_time_status']!='1' && parseInt( jsonparseData['extra_two_time'])>0)   || 
               (jsonparseData['bulk_extra_time_status']!='1' && parseInt( jsonparseData['bulkextraTime'])>0 || onloadfirsttime==0))
              {
               onloadfirsttime = 1;
         
                var remainingminute = parseInt(jsonparseData['remainingTime']) + parseInt( jsonparseData['extraTime'])   + parseInt( jsonparseData['extra_two_time'])  + parseInt( jsonparseData['bulk_extra_time_two']) + parseInt(jsonparseData['bulkextraTime'] ) ;
         
         
                  
               seconds = upgradeTime = parseInt(remainingminute)*(60);
         
             function timer() {
                minutes = Math.floor(seconds / 60);
                remainingSeconds = seconds % 60;
               // add a 0 in front of single digit seconds
               if (remainingSeconds < 10) {
                   remainingSeconds = "0" + remainingSeconds;
               }
               // add a 0 in front of single digit minutes
               if (minutes < 10) {
                   minutes = "0" + minutes;
               }
         
               document.getElementById('countdown').innerHTML =  minutes + ":" + remainingSeconds;
         
                if (minutes < 1) {
                   submitallQuestion();
               }
         
               if (minutes === 0) {
                   clearInterval(countdownTimer);
                   document.getElementById('countdown').innerHTML = "Completed";
                    submitallQuestion();
                       // reload page
               } else {
                   seconds--;
               }
           }
         
           if(showtime==0) {
           countdownTimer = setInterval(timer, 1000);
         }
            showtime = 1;
         } 
                 
         
                
           });
         
           $.get("/studentexamination/exam/checkchat", function(data, status){
                 if(data>0) {
         
                   getchatdata();
         
                  
                                    $('.chatbot-container').show();
         
         
                                    $.get("/studentexamination/exam/updatechat", function(data, status){
                     });
         
         
         
         
         
         
         
                 }
         
                
           });
         
         
           $.get("/studentexamination/exam/notification", function(data, status){
                 if(data!=''){
                   $("#showNotificationDivId").show();
                   $("#latestinstruction").html(data);
               }
         
                
           });
         
         
         
         }, 1 * 1000);
         
         
      </script>
      <!-- First, include the Webcam.js JavaScript Library -->
      <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/webcamjs/1.0.25/webcam.js"></script>
      <!-- Configure a few settings and attach camera -->
      <script language="JavaScript">
         Webcam.set({
           width: 320,
           height: 240,
           image_format: 'jpeg',
           jpeg_quality: 90
         });
         Webcam.attach( '#my_camera' );
      </script> 
      <script>
         $('.chatbot-btn').on('click', function () {
           $(this).hide()
           $('.chatbot-container').show()
         })
         $('.chatbot-close').on('click', function () {
           $('.chatbot-container').hide()
           $('.chatbot-btn').show()
         })   
         
         
         
         
      </script>
   </body>
</html>