<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <title>Quiz</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo BASE_PATH; ?>assets/onlineportal/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo BASE_PATH; ?>assets/onlineportal/css/font-awesome.min.css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css2?family=Ubuntu:wght@300;400;500;700&display=swap" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="<?php echo BASE_PATH; ?>assets/onlineportal/css/main.css" rel="stylesheet" />
  </head>
  <body>

    <nav class="navbar navbar-expand-lg navbar-light main-header">
      <div class="container">
        <a class="navbar-brand" href="#">LOGO</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample02" aria-controls="navbarsExample02" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
      
        <div class="collapse navbar-collapse" id="navbarsExample02">
          <ul class="navbar-nav ml-auto align-items-center">
            <li class="nav-item">
              Welcome <?php echo $this->session->userdata['student_name'];?>
            </li>            
                      
          </ul>          
        </div>
      </div>
    </nav> 

    <div class="container">
        <div class="row mt-5">
            
            <div class="col-sm-4">
               <img src='/assets/images/photo1.jpeg' class="img-fluid capture-id-img"/>
               <h4 class="mt-3">Capture your ID / Photo</h4>
           </div>
           
           <div class="col-sm-4" id='capturingpic'>
                  <div id="my_camera"></div>
                    <button id='captimage'   type="button" class="btn btn-primary start-btn mt-3" onclick="take_snapshot()";>Click to Capture</button>
                    <button id='retakeimg'   type="button" class="btn btn-primary start-btn mt-3" style='display:none;' onclick="take_snapshot()";>Retake</button>                  
            </div>
            
            <div class="col-sm-4">
                 <div id="results"></div>    
                <button id='imagebtn'   type="button" class="btn btn-primary start-btn mt-3" onclick="hidetakingpic()">Finalise</button>
            </div>
            
            <div class="col-sm-4" style='display:none;' id='showtokendiv'>
                <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                      <input type='text' name='token' id='token' class="form-control" placeholder="Enter Token" value=''> 
                      (Enter 12345)
                    </div>
                 </div>
                 </div>
                  <div class="row">
                 <div class="col-sm-12">
                      <button  type="button" class="btn btn-primary start-btn" onclick='return redirectexam()'>Click to Start</button>          
                </div>
                </div>
          
            </div>

        </div>
    <div class="row">
       
      <div class="col-sm-12"> <br/>

            <div class="exam-message">
                <h4>Important</h4>
               <ul>
                   <li>Candidates are required to log on to the examination platform and/or video conferencing
platform at least forty-five (45) minutes before the start of the examination. Candidates who log into the examination
platform forty-five (45) minutes after an examination has commenced, shall not be permitted to take the examination.</li><li>
Online examinations allow candidates to take one five-minute unscheduled break with the exam clock paused. For
online proctored exams, you are only able to leave your exam room/desk/screen during the five-minute ‘Pause the Clock’
break.</li><li>
The exam timer will not stop for any breaks beyond the one five-minute unscheduled break. Any subsequent breaks will
not be given the ‘Pause the clock’ opportunity.</li><li>
If a candidate does happen to lose connection to the exam momentarily, whether due to internet issues or a loss of
power, the exam will temporarily shut down. Once internet or power is regained, the candidate will be able to relaunch
the exam from where he/she left off and the progress will have been saved.</li><li>
In the event of a disconnection, the exam timer will stop and once re-launched, the candidate is able to resume from the
same question the moment he/she left (got disconnected from) the exam.</li><li>
However, if internet connection and/or power is lost for more than 30 minutes, the exam will be discontinued and the
candidate is required to re-take the exam in the next scheduled examination sitting.</li><li>
If internet connection and/or power is lost permanently less than 80% into the time after an examination has
commenced, the candidate is required to re-take the exam in the next scheduled examination sitting.</li><li>
If internet connection and/or power is lost permanently more than 80% into the time after an examination has
commenced, the candidate is required to submit the exam as is.</li>
               </ul>
            </div>    
           
            

         
         
      </div>        
    </div>
  </div>

 <script src="<?php echo BASE_PATH; ?>assets/onlineportal/js/jquery-1.12.4.min.js"></script>


<!-- First, include the Webcam.js JavaScript Library -->
      <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/webcamjs/1.0.25/webcam.js"></script>

  
  <!-- Configure a few settings and attach camera -->
  <script language="JavaScript">
    Webcam.set({
      width: 320,
      height: 240,
      image_format: 'jpeg',
      jpeg_quality: 90
    });
    Webcam.attach( '#my_camera' );
  </script>

  <script>

    $( document ).ready(function() {

  checkExamStarted();
});


  function checkExamStarted() {
    $("#startButtonDisabled").hide();
    $("#startButtonenabled").hide();
    $.get("/studentexamination/exam/checkExamStartedOrNot", function(data, status){
         if(data=='1') {
            $("#startButtonenabled").show();
         }
         if(data=='0') {
            $("#startButtonDisabled").show();
         }
    });
  }

  window.setInterval(function(){
  checkExamStarted();
}, 5000);


function hidetakingpic() {
    $("#capturingpic").hide();
    $("#showtokendiv").show();
}

    function take_snapshot() {
        
        $("#captimage").hide();
        $("#retakeimg").show();
      // take snapshot and get image data
      
      Webcam.snap( function(data_uri) {
          
          
           $.ajax({
                     url: "/studentexamination/exam/updateimage",
                     method: "POST",
                     data: {
                       image: data_uri
                     },
                     success: function(data) {
                     
                     }
                   });
     
     
        // display results in page
        document.getElementById('results').innerHTML = 
          '<h2>Here is your image:</h2>' + 
          '<img src="'+data_uri+'"/>';
      } );
    }

 function redirectexam() {

  var token = $("#token").val();
  if(token=='') {
    alert('Please enter the token and click on Start to continue');
    return false;
  }
  
  parent.location='/studentexamination/mockexam/start';
  
 }

 function updateAnswerImage(examquestionid){
      
     Webcam.snap( function(data_uri) {
            $(".image-tag").val(data_uri);
              $.ajax({
                    url: "/studentexamination/exam/answersaveimage",
                    method: "POST",
                    data: {
                      image: data_uri,
                      examquestionid : examquestionid
                    },
                    success: function(data) {
                     
                    }
                  });
        } );
}
 


  </script>