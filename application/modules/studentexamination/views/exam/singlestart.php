<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <title>Quiz</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo BASE_PATH;?>assets/onlineportal/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo BASE_PATH;?>assets/onlineportal/css/font-awesome.min.css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css2?family=Ubuntu:wght@300;400;500;700&display=swap" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="<?php echo BASE_PATH;?>assets/onlineportal/css/main.css" rel="stylesheet" />

      <script src="<?php echo BASE_PATH;?>assets/js/camvas.js"></script>

    <script src="<?php echo BASE_PATH;?>assets/js/pico.js"></script>

    <script src="<?php echo BASE_PATH;?>assets/js/lploc.js"></script>
    
  </head>
  <style type="text/css">
      p{ margin-bottom: 5px !important;}
  </style>
  <body id="specialstuff" style="overflow-x:auto;">

    <nav class="navbar navbar-expand-lg navbar-light main-header" style="padding:2px 2px;">
      <div class="container-fluid">
        <a class="navbar-brand" href="#">LOGO</a>
                         <div id="my_camera" style="display:none;"></div> 

            <p><center><canvas width=640 height=480 style="width:100px;"></canvas></center></p>


        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample02" aria-controls="navbarsExample02" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
         
        <div class="collapse navbar-collapse" id="navbarsExample02" >

            <ul class="navbar-nav header-stats align-items-center" id="headerdatadiv" style="display:none;">
            <li class="nav-item active">
              <img src="<?php echo BASE_PATH; ?>assets/onlineportal/img/total_questions_icon.svg" />Total Questions <span><?php echo count($question)?></span>
            </li>
            <li class="nav-item">
              <img src="<?php echo BASE_PATH; ?>assets/onlineportal/img/total_time_icon.svg" />Total Time(min) <span><?php echo $exam->duration;?></span>
            </li>

          
            <li class="nav-item">
              <img src="<?php echo BASE_PATH; ?>assets/onlineportal/img/total_remainingtime_icon.svg" />Remaining Time  
<span  id="countdown"></span>
<span  id="countdown1"></span>
            </li>            
          </ul>
          
          <ul class="navbar-nav ml-auto align-items-center">
            <li class="nav-item">
              Welcome <?php echo $this->session->userdata['student_name'];?>
                         

            </li>            
            <li class="nav-item active">
              <a href="/studentexamination/exam/submitmarksfinal/" class="nav-link">Submit Answer</a>
            </li>          
          </ul>          
        </div>


          

      </div>
    <button class="btn btn-primary see-all-questions-btn">See All Questions</button>      
    </nav> 
    <div class="quiz-wrapper" style="background:white;" id="quiz-wrapper">
      <div class="container">

          <div id="staticdivid">
         <div class="row" >
         
            
            <div class="col-sm-4">
               <img src='/assets/images/photo1.jpeg' class="img-fluid capture-id-img"/>
               <h4 class="mt-3">Capture your ID / Photo</h4>
           </div>
           

            <div class="col-sm-4">
                 <div id="results"><img src="/<?php echo $exam->image;?>" class="img-fluid capture-id-img"/></div>    
                <button id='imagebtn'   type="button" class="btn btn-primary start-btn mt-3" onclick="back()">Capture New Image</button>
            </div>
            
            <div class="col-sm-4" >
                <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                      <input type='text' name='token' id='token' class="form-control" placeholder="Enter Token" value="<?php echo $exam->token;?>" readonly>            
                    </div>
                 </div>
                 </div>
                  <div class="row">
                 <div class="col-sm-12">
                   <button id='fsbutton'  type="button" class="btn btn-primary start-btn" >Click to Start</button>  
                </div>
                </div>
          
            </div>

        </div>
    <div class="row">
       
      <div class="col-sm-12"> <br/>

              <div class="row exam-message" id='showNotificationDivId' style="display:none;">
               <div class="col-sm-12">
                  <h5 style="color:red;">Notification</h5>
                 <table class="table">
                   <tr>
                    <th style="color:red;" id="latestinstruction"></th>
                  </tr>

                 </table>
            </div>
          </div>

            <div class="exam-message" style="font-size:18px;">
                <h4>Important</h4>
               <ol>
                   <li>Candidates are required to log on to the examination platform and/or video conferencing platform at least forty-five (45) minutes before the start of the examination. Candidates who log into the examination platform <span style='color:red;'>thirty (30) </span> minutes after an examination has commenced, shall not be permitted to take the examination.</li>
                   <li>If a candidate does happen to lose connection to the exam momentarily, whether due to internet issues or a loss of power, the exam will temporarily shut down. Once internet or power is regained, the candidate will be able to relaunch the exam from where he/she left off and the progress will have been saved.</li>
                   <li>In the event of a disconnection, the exam timer will stop and once re-launched, the candidate is able to resume from the same question the moment he/she left (got disconnected from) the exam.</li>
                   <li>However, if internet connection and/or power is lost for more than 30 minutes, the exam will be discontinued and the candidate is required to re-take the exam in the next scheduled examination sitting.</li>
<li>If internet connection and/or power is lost permanently less than 80% into the time after an examination has commenced, the candidate is required to re-take the exam in the next scheduled examination sitting.</li><li>If internet connection and/or power is lost permanently more than 80% into the time after an examination has commenced, the candidate is required to submit the exam as is.</li>
               </ol>
            </div>      
      </div>        
    </div>

        </div>

    

        <div class="row" id="datadivid" style="display:none;">
            

          <div class="col-md-8 pt-3">

          <?php $this->load->model('student/exam_student_tagging_model');?>
          <?php 
                $j=0;
                $mavalue = count($question);

             for ($i=0;$i<count($question);$i++) {


            if($question[$i]['question_type']=='1' || $question[$i]['question_type']=='3') { 
               $j++.' ';

              $next = $i+1;
              $nextquestion = $question[$next]['id'];


                 $previous = $i-1;
              $previousquestion = $question[$previous]['id'];

              ?>






        <div class="question-container" id="questionDiv<?php echo $question[$i]['id']?>" style="display: none;">
             <?php if($question[$i]['id']=='394' || $question[$i]['id']=='395' || $question[$i]['id']=='397' ) { ?>
            <div class="question-container-inner" style="height:1300px;">

              <?php  } else {?>
            <div class="question-container-inner" style="min-height:700px;height:100%">

              <?php } ?> 
               <div class="d-flex">

               <p class="mr-2 text-nowrap"><?php echo $question[$i]['question_number'];?>. </p><div><?php echo $question[$i]['question'];?> (<?php echo $question[$i]['marks'];?> Marks)</div></div>



               <?php if($question[$i]['question_type']=='1' || $question[$i]['question_type']=='3') {
                ?>
              <div class="row mt-2">

                 <?php 
                        $answerList = $this->exam_student_tagging_model->getAnswersByQuestionId($question[$i]['id']);

                         $answeredData = $this->exam_student_tagging_model->checkifAnswered($question[$i]['id'],$this->session->id_student_exam_attempt);

              for ($a=0;$a<count($answerList);$a++) { ?>
                  <div class="col-md-12">
                    <div class="answer-radio">
                      <div class="custom-control custom-radio">
                        <input type="radio" id="answer<?php echo $answerList[$a]->id;?>" name="quesion<?php echo $question[$i][id];?>" class="custom-control-input" 
                       <?php if($answeredData->id_answer==$answerList[$a]->id) { echo "checked=checked";} ?> onclick="updateAnswer(<?php echo $answerList[$a]->id;?>,<?php echo $question[$i]['examstudentid'];?>,<?php echo $question[$i]['id'];?>)">
                    <label class="custom-control-label" for="answer<?php echo $answerList[$a]->id;?>"><?php echo $answerList[$a]->option_description;?></label>
                      </div>
                    </div>
                  </div>
                <?php } ?>
                                         
              </div> 
              <?php } ?> 
              </div>
            <div class="row pt-4 pb-2">
                <div class="col-6">
                    <?php if($i==0) { ?>
        
                    <?php }  else { ?>
                      <input type="button" class="btn btn-primary" value="Previous" onclick="nextQuestion(<?php echo $previousquestion;?>)">
        
        
                    <?php } ?>                  
                </div>
                <div class="col-6 d-flex justify-content-end">
            
              <?php if($mavalue==$next) { ?>
    
                <?php }  else { ?>
                  <input type="button" class="btn btn-primary" value="NEXT" onclick="nextQuestion(<?php echo $nextquestion;?>)">
    
                <?php } ?>  
                </div>
            </div>


            </div> 
 <?php }  } ?>



  <?php 
                 $j=0;
                $mavalue = count($question);

             for ($i=0;$i<count($question);$i++) {

            if($question[$i]['question_type']=='2') { 
               $j++.' ';
                             $next = $i+1;
              $nextquestion = $question[$next]['id'];


                 $previous = $i-1;
              $previousquestion = $question[$previous]['id'];

              ?>

        <div class="question-container" id="questionDiv<?php echo $question[$i]['id']?>" style="display: none;">
            <div class="question-container-inner">
               <div class="d-flex">

               <p class="mr-2 text-nowrap"><?php echo $question[$i]['question_number'];?>. </p><div><?php echo $question[$i]['question'];?> (<?php echo $question[$i]['marks'];?> Marks)</div></div>



               <?php if($question[$i]['question_type']=='2') {
                ?>
              <div class="row mt-2">

                 <?php 
                           $answeredData = $this->exam_student_tagging_model->checkifAnswered($question[$i]['id'],$this->session->id_student_exam_attempt); ?>
                <div class="col-md-12">
                    <textarea name='written_answer<?php echo $question[$i]['examstudentid'];?>' 
                      id='written_answer<?php echo $question[$i]['examstudentid'];?>' 
                      class='idinstructions' style='width:100%;height:120px;' 
                      onkeypress='updateAnswertext(<?php echo $question[$i]['examstudentid'];?>)'><?php echo $answeredData->answer_text;?></textarea>
                  </div>
                                         
              </div> 
              <?php } ?> 
            </div>
            <div class="row pt-4 pb-2">
                <div class="col-6">
                    <?php if($i==0) { ?>
        
                    <?php }  else { ?>
                      <input type="button" class="btn btn-primary" value="Previous" onclick="nextQuestion(<?php echo $previousquestion;?>)">
        
        
                    <?php } ?>                  
                </div>
                
           <div class="col-6 d-flex justify-content-end">
               <?php if($mavalue==$next) { ?>
                 <button type="button" class="btn btn-primary btn-lg" onclick="confirmsubmit()">Submit</button>
            <?php }  else { ?>
                 
              <input type="button" class="btn btn-primary" value="NEXT" onclick="nextQuestion(<?php echo $nextquestion;?>)">

            <?php } ?> 
            </div>
          </div>



            </div> 
 <?php }  } ?>

            <div class="d-flex py-2 justify-content-end">
            <?php if($mavalue==$next) { ?>
             
              <?php } ?>
            </div>  
 <?php if($notification) {?>
           <div class="row exam-message">
               <div class="col-sm-12">
                  <h5 style="color:red;">Notification</h5>
                 <table class="table">
                   <tr>
                    <th style="color:red;"><?php echo $notification->notification;?></th>
                  </tr>

                 </table>
            </div>
          </div>
          <?php } ?> 
 
          </div>
          <div class="col-md-4">
                      <p style="display:none;">Status: <span id="fsstatus"></span></p>

           <div id="attachQuestionNumber" style="min-height: 100vh !important;">
               
               <div id="attachQuestionNumberList">
                   
               </div>
           </div>


          </div>   

        </div>
    

<div class="container">
  
  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-body">
          <p><span id='numberofquestion'></span> Number of questions are still pending to answer. Do you really want to submit the exam?</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
          <button type="button" class="btn btn-success" onclick="closeexam()">Yes</button>
        </div>
      </div>
      
    </div>
  </div>
  
</div>
 
 <div class="container">
  
  <!-- Modal -->
  <div class="modal fade" id="myModalpic" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-body">
          <p>No face detected, please enable camera</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>
         
        </div>
      </div>
      
    </div>
  </div>
  
</div>

 <div class="container">
  
  <!-- Modal -->
  <div class="modal fade" id="timeup" role="timeup">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-body">
          <p>Exam Time is over, please click ok to submit</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" onclick="closeexam()">Ok</button>
         
        </div>
      </div>
      
    </div>
  </div>
  
</div>


    <div class="custom-chatbot">
      <img src="/assets/images/chat.png" style="height: 124px; border-radius: 999px;" class="btn btn-primary chatbot-btn"><br/><span style="font-weight: bold;color:#00bcd2;">Live Support</span>
      <div class="chatbot-container">
        <div class="chatbot-header">
          <div class="header-text">Chat with Invigilator</div>
          <button class="btn chatbot-close">
            <svg
              fill="#FFFFFF"
              height="15"
              viewBox="0 0 15 15"
              width="15"
              xmlns="http://www.w3.org/2000/svg"
            >
              <line
                x1="1"
                y1="15"
                x2="15"
                y2="1"
                stroke="white"
                stroke-width="1"
              ></line>
              <line
                x1="1"
                y1="1"
                x2="15"
                y2="15"
                stroke="white"
                stroke-width="1"
              ></line>
            </svg>
          </button>
        </div>
        <div class="chat-room">
          <div class="message-area" id='chatdetails'>
           
          </div>
          <div class="input-group mb-3 input-group-lg">
            <input
              type="text"
              class="form-control"
              id='chat'
              placeholder="Enter Message"
            />
            <div class="input-group-append">
              <button class="btn btn-primary" type="button" onclick="chatdata()">Send</button>
            </div>
          </div>
        </div>
      </div>
    </div>
      </div>
    </div>
    <script src="<?php echo BASE_PATH; ?>assets/onlineportal/js/jquery-1.12.4.min.js"></script>

    <script src="<?php echo BASE_PATH;?>assets/onlineportal/js/bootstrap.min.js"></script>
    <script src="<?php echo BASE_PATH;?>assets/onlineportal/js/main.js"></script>

    <script type="text/javascript" src="<?php echo BASE_PATH; ?>assets/ckeditor/ckeditor.js"></script>

<style type="text/css">
  .modal-open .modal.modal-center {
    display: flex!important;
    align-items: center!important;
    .modal-dialog {
        flex-grow: 1;
    }
}


</style>
    <script>


/* 
Native FullScreen JavaScript API
-------------
Assumes Mozilla naming conventions instead of W3C for now
*/

(function() {
  var 
    fullScreenApi = { 
      supportsFullScreen: false,
      isFullScreen: function() { return false; }, 
      requestFullScreen: function() {}, 
      cancelFullScreen: function() {},
      fullScreenEventName: '',
      prefix: ''
    },
    browserPrefixes = 'webkit moz o ms khtml'.split(' ');
  
  // check for native support
  if (typeof document.cancelFullScreen != 'undefined') {
    fullScreenApi.supportsFullScreen = true;
  } else {   
    // check for fullscreen support by vendor prefix
    for (var i = 0, il = browserPrefixes.length; i < il; i++ ) {
      fullScreenApi.prefix = browserPrefixes[i];
      
      if (typeof document[fullScreenApi.prefix + 'CancelFullScreen' ] != 'undefined' ) {
        fullScreenApi.supportsFullScreen = true;
        
        break;
      }
    }
  }
  
  // update methods to do something useful
  if (fullScreenApi.supportsFullScreen) {
    fullScreenApi.fullScreenEventName = fullScreenApi.prefix + 'fullscreenchange';
    
    fullScreenApi.isFullScreen = function() {
      switch (this.prefix) {  
        case '':
          return document.fullScreen;
        case 'webkit':
          return document.webkitIsFullScreen;
        default:
          return document[this.prefix + 'FullScreen'];
      }
    }
    fullScreenApi.requestFullScreen = function(el) {
      return (this.prefix === '') ? el.requestFullScreen() : el[this.prefix + 'RequestFullScreen']();
    }
    fullScreenApi.cancelFullScreen = function(el) {
      return (this.prefix === '') ? document.cancelFullScreen() : document[this.prefix + 'CancelFullScreen']();
    }   
  }

  // jQuery plugin
  if (typeof jQuery != 'undefined') {
    jQuery.fn.requestFullScreen = function() {
  
      return this.each(function() {
        var el = jQuery(this);
        if (fullScreenApi.supportsFullScreen) {
          fullScreenApi.requestFullScreen(el);
        }
      });
    };
  }

  // export api
  window.fullScreenApi = fullScreenApi; 
})();

</script>

<script>

// do something interesting with fullscreen support
var fsButton = document.getElementById('fsbutton'),
  fsElement = document.getElementById('specialstuff'),
  fsStatus = document.getElementById('fsstatus');


if (window.fullScreenApi.supportsFullScreen) {
  fsStatus.innerHTML = 'YES: Your browser supports FullScreen';
  fsStatus.className = 'fullScreenSupported';
  
  // handle button click
  fsButton.addEventListener('click', function() {

    $("#datadivid").show();
    $("#headerdatadiv").show();


    $("#staticdivid").hide();
    window.fullScreenApi.requestFullScreen(fsElement);
  }, true);


  
  fsElement.addEventListener(fullScreenApi.fullScreenEventName, function() {
    if (fullScreenApi.isFullScreen()) {
      fsStatus.innerHTML = 'Whoa, you went fullscreen';
          $("#datadivid").show();   
           $("#headerdatadiv").show();

    $("#staticdivid").hide();

    } else {
          $("#datadivid").hide();
          $("#staticdivid").show();
        $("#headerdatadiv").hide();

      fsStatus.innerHTML = 'Back to normal';


      $.get("/studentexamination/exam/lockexam", function(data, status){
      alert('You moved away from screen, So the exam is locked, please ask administrator to unlock it');
           parent.location='https://ciifiscore.ciif-global.org/studentexamination/exam/start';
           
      });

                

    }
  }, true);
  
} else {
  fsStatus.innerHTML = 'SORRY: Your browser does not support FullScreen';
}


$(document).on('keydown', function(event) {
       if(event.key=='Control' || event.key=='Meta'  || event.key=='Alt' ) {

         $.get("/studentexamination/exam/lockexam", function(data, status){
          alert('You moved away from screen, So the exam is locked, please ask administrator to unlock it');
               parent.location='https://ciifiscore.ciif-global.org/studentexamination/exam/start';
               
          });


       }
   });

</script>




    <script>

      function back(){
        history.back();
      }
var reloaded =0;
        function chatdata(){

            var chat = $("#chat").val();
            if(chat!='') {

              $.ajax({
                url: "/studentexamination/exam/chat",
                method: "POST",
                data: {
                  chat: chat
                },
                success: function(data) {
                 
                  getchatdata();
                          }
              });
          }


        }


        function getchatdata(){
              $.get("/studentexamination/exam/chatdisplay", function(data, status){
           
                $("#chatdetails").html(data);
                $("#chat").val('');
            });
        }


      function nextQuestion(id) {

        scrolldiv(id);  
      }

       function checkimage(){
      var update_memory = pico.instantiate_detection_memory(5); // we will use the detecions of the last 5 frames
      var facefinder_classify_region = function(r, c, s, pixels, ldim) {return -1.0;};
      var cascadeurl = 'https://ciifiscore.ciif-global.org/assets/js/facefinder';
      fetch(cascadeurl).then(function(response) {
        response.arrayBuffer().then(function(buffer) {
          var bytes = new Int8Array(buffer);
          facefinder_classify_region = pico.unpack_cascade(bytes);
          console.log('* facefinder loaded');
          
        })
      })
      /*
        (2) initialize the lploc.js library with a pupil localizer
      */
      var do_puploc = function(r, c, s, nperturbs, pixels, nrows, ncols, ldim) {return [-1.0, -1.0];};
      var puplocurl = 'https://ciifiscore.ciif-global.org/assets/js/puploc.bin';
      fetch(puplocurl).then(function(response) {
        response.arrayBuffer().then(function(buffer) {
          var bytes = new Int8Array(buffer);
          do_puploc = lploc.unpack_localizer(bytes);
          console.log('* puploc loaded');
        })
      })
      /*
        (3) get the drawing context on the canvas and define a function to transform an RGBA image to grayscale
      */
      var ctx = document.getElementsByTagName('canvas')[0].getContext('2d');
      function rgba_to_grayscale(rgba, nrows, ncols) {
        var gray = new Uint8Array(nrows*ncols);
        for(var r=0; r<nrows; ++r)
          for(var c=0; c<ncols; ++c)
            // gray = 0.2*red + 0.7*green + 0.1*blue
            gray[r*ncols + c] = (2*rgba[r*4*ncols+4*c+0]+7*rgba[r*4*ncols+4*c+1]+1*rgba[r*4*ncols+4*c+2])/10;
        return gray;
      }
      /*
        (4) this function is called each time a video frame becomes available
      */
      var processfn = function(video, dt) {
        // render the video frame to the canvas element and extract RGBA pixel data
        ctx.drawImage(video, 0, 0);
        var rgba = ctx.getImageData(0, 0, 640, 480).data;
        // prepare input to `run_cascade`
        image = {
          "pixels": rgba_to_grayscale(rgba, 480, 640),
          "nrows": 480,
          "ncols": 640,
          "ldim": 640
        }
        params = {
          "shiftfactor": 0.1, // move the detection window by 10% of its size
          "minsize": 100,     // minimum size of a face
          "maxsize": 1000,    // maximum size of a face
          "scalefactor": 1.1  // for multiscale processing: resize the detection window by 10% when moving to the higher scale
        }
        // run the cascade over the frame and cluster the obtained detections
        // dets is an array that contains (r, c, s, q) quadruplets
        // (representing row, column, scale and detection score)
        dets = pico.run_cascade(image, facefinder_classify_region, params);
        dets = update_memory(dets);
        dets = pico.cluster_detections(dets, 0.2); // set IoU threshold to 0.2
        // draw detections
        for(i=0; i<dets.length; ++i)
          // check the detection score
          // if it's above the threshold, draw it
          // (the constant 50.0 is empirical: other cascades might require a different one)
          if(dets[i][3]>50.0)
          {
            var r, c, s;
            //
            ctx.beginPath();
            ctx.arc(dets[i][1], dets[i][0], dets[i][2]/2, 0, 2*Math.PI, false);
            ctx.lineWidth = 3;
            ctx.strokeStyle = 'red';
            ctx.stroke();
            //
            // find the eye pupils for each detected face
            // starting regions for localization are initialized based on the face bounding box
            // (parameters are set empirically)
            // first eye
            r = dets[i][0] - 0.075*dets[i][2];
            c = dets[i][1] - 0.175*dets[i][2];
            s = 0.35*dets[i][2];
            [r, c] = do_puploc(r, c, s, 63, image)
            if(r>=0 && c>=0)
            {
              ctx.beginPath();
              ctx.arc(c, r, 1, 0, 2*Math.PI, false);
              ctx.lineWidth = 3;
              ctx.strokeStyle = 'red';
              ctx.stroke();
            }
            // second eye
            r = dets[i][0] - 0.075*dets[i][2];
            c = dets[i][1] + 0.175*dets[i][2];
            s = 0.35*dets[i][2];
            [r, c] = do_puploc(r, c, s, 63, image)
            if(r>=0 && c>=0)
            {
              ctx.beginPath();
              ctx.arc(c, r, 1, 0, 2*Math.PI, false);
              ctx.lineWidth = 3;
              ctx.strokeStyle = 'red';
              ctx.stroke();
            }
          }
      }
      /*
        (5) instantiate camera handling (see https://github.com/cbrandolino/camvas)
      */
      var mycamvas = new camvas(ctx, processfn);
    
    console.log(dets);
    if(dets.length>0) {
          return 1;
      } else {
          return 0;
      }

 }


      $(document).ready(function() {


        ///////// FAce recognition/////
         var update_memory = pico.instantiate_detection_memory(5); // we will use the detecions of the last 5 frames
      var facefinder_classify_region = function(r, c, s, pixels, ldim) {return -1.0;};
      var cascadeurl = 'https://ciifiscore.ciif-global.org/assets/js/facefinder';
      fetch(cascadeurl).then(function(response) {
        response.arrayBuffer().then(function(buffer) {
          var bytes = new Int8Array(buffer);
          facefinder_classify_region = pico.unpack_cascade(bytes);
          console.log('* facefinder loaded');
          
        })
      })
      /*
        (2) initialize the lploc.js library with a pupil localizer
      */
      var do_puploc = function(r, c, s, nperturbs, pixels, nrows, ncols, ldim) {return [-1.0, -1.0];};
      var puplocurl = 'https://ciifiscore.ciif-global.org/assets/js/puploc.bin';
      fetch(puplocurl).then(function(response) {
        response.arrayBuffer().then(function(buffer) {
          var bytes = new Int8Array(buffer);
          do_puploc = lploc.unpack_localizer(bytes);
          console.log('* puploc loaded');
        })
      })
      /*
        (3) get the drawing context on the canvas and define a function to transform an RGBA image to grayscale
      */
      var ctx = document.getElementsByTagName('canvas')[0].getContext('2d');
      function rgba_to_grayscale(rgba, nrows, ncols) {
        var gray = new Uint8Array(nrows*ncols);
        for(var r=0; r<nrows; ++r)
          for(var c=0; c<ncols; ++c)
            // gray = 0.2*red + 0.7*green + 0.1*blue
            gray[r*ncols + c] = (2*rgba[r*4*ncols+4*c+0]+7*rgba[r*4*ncols+4*c+1]+1*rgba[r*4*ncols+4*c+2])/10;
        return gray;
      }
      /*
        (4) this function is called each time a video frame becomes available
      */
      var processfn = function(video, dt) {
        // render the video frame to the canvas element and extract RGBA pixel data
        ctx.drawImage(video, 0, 0);
        var rgba = ctx.getImageData(0, 0, 640, 480).data;
        // prepare input to `run_cascade`
        image = {
          "pixels": rgba_to_grayscale(rgba, 480, 640),
          "nrows": 480,
          "ncols": 640,
          "ldim": 640
        }
        params = {
          "shiftfactor": 0.1, // move the detection window by 10% of its size
          "minsize": 100,     // minimum size of a face
          "maxsize": 1000,    // maximum size of a face
          "scalefactor": 1.1  // for multiscale processing: resize the detection window by 10% when moving to the higher scale
        }
        // run the cascade over the frame and cluster the obtained detections
        // dets is an array that contains (r, c, s, q) quadruplets
        // (representing row, column, scale and detection score)
        dets = pico.run_cascade(image, facefinder_classify_region, params);
        dets = update_memory(dets);
        dets = pico.cluster_detections(dets, 0.2); // set IoU threshold to 0.2
        // draw detections
        for(i=0; i<dets.length; ++i)
          // check the detection score
          // if it's above the threshold, draw it
          // (the constant 50.0 is empirical: other cascades might require a different one)
          if(dets[i][3]>50.0)
          {
            var r, c, s;
            //
            ctx.beginPath();
            ctx.arc(dets[i][1], dets[i][0], dets[i][2]/2, 0, 2*Math.PI, false);
            ctx.lineWidth = 3;
            ctx.strokeStyle = 'red';
            ctx.stroke();
            //
            // find the eye pupils for each detected face
            // starting regions for localization are initialized based on the face bounding box
            // (parameters are set empirically)
            // first eye
            r = dets[i][0] - 0.075*dets[i][2];
            c = dets[i][1] - 0.175*dets[i][2];
            s = 0.35*dets[i][2];
            [r, c] = do_puploc(r, c, s, 63, image)
            if(r>=0 && c>=0)
            {
              ctx.beginPath();
              ctx.arc(c, r, 1, 0, 2*Math.PI, false);
              ctx.lineWidth = 3;
              ctx.strokeStyle = 'red';
              ctx.stroke();
            }
            // second eye
            r = dets[i][0] - 0.075*dets[i][2];
            c = dets[i][1] + 0.175*dets[i][2];
            s = 0.35*dets[i][2];
            [r, c] = do_puploc(r, c, s, 63, image)
            if(r>=0 && c>=0)
            {
              ctx.beginPath();
              ctx.arc(c, r, 1, 0, 2*Math.PI, false);
              ctx.lineWidth = 3;
              ctx.strokeStyle = 'red';
              ctx.stroke();
            }
          }
      }

      /*
        (5) instantiate camera handling (see https://github.com/cbrandolino/camvas)
      */
      var mycamvas = new camvas(ctx, processfn);
    

      //////// End of Face Recognition////// 



   

          var firstid = <?php echo $question[0]['id']?>;
          $("#questionDiv"+firstid).show();


CKEDITOR.replaceClass="idinstructions";

    getchatdata();



        // Mobile show all question fixed position
        $(window).scroll(function() {
          if ($(document).scrollTop() > 80) {
            $("body").addClass("mobile-questions");
          } else {
            $("body").removeClass("mobile-questions");
          }
        });

        //
        $('.see-all-questions-btn, .close-btn').on('click', function(e) {
          $('body').toggleClass('push-menu');
        });

       

CKEDITOR.on('instanceCreated', function(e) {
        e.editor.on('contentDom', function() {
            e.editor.document.on('keyup', function(event) {
                // keyup event in ckeditor
                updateAnswertext(e.editor.name);
            }
        );
    });
});



      getAllQuestionNumber();



    });


   



      function togglemenu() {
        $('body').toggleClass('push-menu');
      }

       function scrolldiv(id) {
          $('body').toggleClass('push-menu');


          $(".question-container").hide();
          $("#questionDiv"+id).show();

    $('html, body').animate({scrollTop:$('#questionDiv'+id).position().top}, 'slow');


  }

$(document).on("keydown",function(ev){
  console.log(ev.keyCode);
  if(ev.keyCode===27||ev.keyCode===122) return false


})

$(document).on("click","#cust_btn",function(){
  
  $("#myModal").modal("toggle");
  
})


    function getAllQuestionNumber() {
    $.get("/studentexamination/exam/getAllQuestionNumber", function(data, status){
           
                $("#attachQuestionNumberList").html(data);
            });
  }

  function updateAnswer(answerid,examquestionid,questionid){


     var displaymsg  =  checkimage();


     if(displaymsg==0) {
     
         $("[name='quesion"+questionid+"']").prop("checked", false);


       $('#myModalpic').modal('show')

         return false;
     } else { 

     

      updateAnswerImage(examquestionid);
      $.get("/studentexamination/exam/markanswer/"+answerid+"/"+examquestionid, function(data, status){
             
                  getAllQuestionNumber();
              });
      MeasureConnectionSpeed(examquestionid);
    }
  }






  function updateAnswertext(examquestionid) {

    var answer = CKEDITOR.instances[examquestionid].getData();


var idexam = examquestionid.replace('written_answer','');
    // updateAnswerImage(idexam);

   $.ajax({
        url: "/studentexamination/exam/markanswertext",
        method: "POST",
        data: {
          answer_written: answer,
          question_id : idexam
        },
        success: function(data) {
         
          getAllQuestionNumber();
        }
      });
  }


  function confirmsubmit() {

    $.get("/studentexamination/exam/pendingquestion", function(data, status){
           $("#numberofquestion").html(data);
           if(data==0) {
            closeexam();
           } else {
           $('#myModal').modal('show')
         }


       });



     
  }


  function closeexam(){
     $.get("/studentexamination/exam/submitmarks/", function(data, status){
                 
                        parent.location='/studentexamination/exam/thankyou';

                  });
  }


  function submitallQuestion(){
    $.get("/studentexamination/exam/submitmarksauto/", function(data, status){
                 
                        parent.location='/studentexamination/exam/thankyou';

                  });
  }

    
    window.onblur = function () {
}



function updateAnswerImage(examquestionid){
      
      Webcam.snap( function(data_uri) {
             $(".image-tag").val(data_uri);
               $.ajax({
                     url: "/studentexamination/exam/answersaveimage",
                     method: "POST",
                     data: {
                       image: data_uri,
                       examquestionid : examquestionid
                     },
                     success: function(data) {
                     
                     }
                   });
         } );
}
 

var imageAddr = "https://ciifiscore.ciif-global.org/assets/onlineportal/img/logocpif-1.png"; 
var downloadSize = 192924; //bytes

function ShowProgressMessage(msg) {
    if (console) {
        if (typeof msg == "string") {
            console.log(msg);
        } else {
            for (var i = 0; i < msg.length; i++) {
                console.log(msg[i]);
            }
        }
    }
    
    var oProgress = document.getElementById("progress");
    if (oProgress) {
        var actualHTML = (typeof msg == "string") ? msg : msg.join("<br />");
        oProgress.innerHTML = actualHTML;
    }
}

function InitiateSpeedDetection() {
    ShowProgressMessage("Loading the image, please wait...");
    window.setTimeout(MeasureConnectionSpeed, 1);
};    

if (window.addEventListener) {
    window.addEventListener('load', InitiateSpeedDetection, false);
} else if (window.attachEvent) {
    window.attachEvent('onload', InitiateSpeedDetection);
}

function MeasureConnectionSpeed(examquestionid) {
    var startTime, endTime;
    var download = new Image();
    download.onload = function () {
        endTime = (new Date()).getTime();
        showResults(examquestionid);
    }
    
    download.onerror = function (err, msg) {
        ShowProgressMessage("Invalid image, or error downloading");
    }
    
    startTime = (new Date()).getTime();
    var cacheBuster = "?nnn=" + startTime;
    download.src = imageAddr + cacheBuster;
    
    function showResults(examquestionid) {
        var duration = (endTime - startTime) / 1000;
        var bitsLoaded = downloadSize * 8;
        var speedBps = (bitsLoaded / duration).toFixed(2);
        var speedKbps = (speedBps / 1024).toFixed(2);
        var speedMbps = (speedKbps / 1024).toFixed(2);
              $.ajax({
                    url: "/studentexamination/exam/internetspeed",
                    method: "POST",
                    data: {
                      image: speedKbps,
                      examquestionid : examquestionid

                    },
                    success: function(data) {
                     
                    }
                  });
       
    }
}
        
        
        


    </script>

<script type="text/javascript">
  var countdownTimer,showtime;
  showtime = 0;
  var onloadfirsttime = 0;
        var seconds, upgradeTime;
        var minutes,remainingminute,remainingSeconds;

    setInterval(function() {

    $.get("/studentexamination/exam/gettime", function(data, status){

      var jsonparseData = jQuery.parseJSON(data);
       if((jsonparseData['extra_time_updated_status']!='1' && parseInt( jsonparseData['extraTime'])>0) || 
        (jsonparseData['bulk_extra_time_two_status']!='1' && parseInt( jsonparseData['bulk_extra_time_two'])>0)   ||  
        (jsonparseData['extra_two_time_status']!='1' && parseInt( jsonparseData['extra_two_time'])>0)   || 
        (jsonparseData['bulk_extra_time_status']!='1' && parseInt( jsonparseData['bulkextraTime'])>0 || onloadfirsttime==0))
       {
        onloadfirsttime = 1;

         var remainingminute = parseInt(jsonparseData['remainingTime']) + parseInt( jsonparseData['extraTime'])   + parseInt( jsonparseData['extra_two_time'])  + parseInt( jsonparseData['bulk_extra_time_two']) + parseInt(jsonparseData['bulkextraTime'] ) ;


           
        seconds = upgradeTime = parseInt(remainingminute)*(60);


        if(jsonparseData['examover']=='1') {
          alert("Exam is completed");
                     parent.location='https://ciifiscore.ciif-global.org/studentexamination/exam/start';
        }

      function timer() {
         minutes = Math.floor(seconds / 60);
         remainingSeconds = seconds % 60;
        // add a 0 in front of single digit seconds
        if (remainingSeconds < 10) {
            remainingSeconds = "0" + remainingSeconds;
        }
        // add a 0 in front of single digit minutes
        if (minutes < 10) {
            minutes = "0" + minutes;
        }

        document.getElementById('countdown').innerHTML =  minutes + ":" + remainingSeconds;

        //  if (minutes < 1) {
        //     submitallQuestion();
        // }

        if (parseInt(minutes) == 0 && parseInt(seconds) == 0) {
                 $('#timeup').modal('show')

            // clearInterval(countdownTimer);
            // document.getElementById('countdown').innerHTML = "Completed";
             submitallQuestion();
                // reload page
        } else {
            seconds--;
        }
    }

    if(showtime==0) {
    countdownTimer = setInterval(timer, 1000);
  }
     showtime = 1;
  } 
          

         
    });

    $.get("/studentexamination/exam/checkchat", function(data, status){
          if(data>0) {

            getchatdata();

           
                             $('.chatbot-container').show();


                             $.get("/studentexamination/exam/updatechat", function(data, status){
              });







          }

         
    });

}, 1 * 10000);



</script>
     
  <!-- First, include the Webcam.js JavaScript Library -->
      <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/webcamjs/1.0.25/webcam.js"></script>

  
  <!-- Configure a few settings and attach camera -->
  <script language="JavaScript">
    Webcam.set({
      width: 320,
      height: 295,
      image_format: 'jpeg',
      jpeg_quality: 90
    });
    Webcam.attach( '#my_camera' );
  </script> 
  
  
  <script>
      
        $('.chatbot-btn').on('click', function () {
          $(this).hide()
          $('.chatbot-container').show()
        })
        $('.chatbot-close').on('click', function () {
          $('.chatbot-container').hide()
          $('.chatbot-btn').show()
        })   


    
   
  </script>



  </body>
</html>