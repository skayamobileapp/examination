<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <title>Quiz</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo BASE_PATH; ?>assets/onlineportal/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo BASE_PATH; ?>assets/onlineportal/css/font-awesome.min.css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css2?family=Ubuntu:wght@300;400;500;700&display=swap" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="<?php echo BASE_PATH; ?>assets/onlineportal/css/main.css" rel="stylesheet" />
  </head>
  <body>

    <nav class="navbar navbar-expand-lg navbar-light main-header">
      <div class="container">
        <a class="navbar-brand" href="#">LOGO</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample02" aria-controls="navbarsExample02" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
      
        <div class="collapse navbar-collapse" id="navbarsExample02">
          <ul class="navbar-nav ml-auto align-items-center">
            <li class="nav-item">
              Welcome <?php echo $this->session->userdata['student_name'];?>
            </li>            
            <li class="nav-item active">
              <a href="/studentexamination/exam/logout" class="nav-link">Logout</a>
            </li>          
          </ul>          
        </div>
      </div>
    </nav> 

    <div class="container">
    <div class="row align-items-center">
      <div class="col-md-4 mt-5">
        <img src="<?php echo BASE_PATH; ?>assets/img/instructions.svg" alt="" class="img-responsive">
      </div>
      <div class="col-md-8 mt-5">
        <h4>Instructions</h4>
        <ul class="instructions-list">
        <?php
            echo $instruction;
          ?>
          </ul>

            <div class="row" id='captureimae'>
                <div class="col-md-7 col-lg-6">
                  <div><img src='/assets/images/images.jpeg' /></div>
              </div>
              <div class="col-md-7 col-lg-6">
                  <div id="my_camera"></div>
              </div>
               <div><button id='captimage'   type="button" class="btn btn-primary start-btn" onclick="take_snapshot()";>Click to Capture</button>
               <button id='retakeimg'   type="button" class="btn btn-primary start-btn" style='display:none;' onclick="take_snapshot()";>Retake</button>
               </div>
            </div>
            
             <div class="row">
              <div class="col-md-7 col-lg-6">
                  <div id="results"></div>
              </div>
              <div class="col-sm-3">
                  <button id='imagebtn'   type="button" class="btn btn-primary start-btn" onclick="hidetakingpic()">Finalise</button>
              </div>
               
            </div>


          <div class="row">
          <div class="col-md-7 col-lg-6">
            <div class="form-group">
              <label for="token">Enter Token Number</label>
              <input type='text' name='token' id='token' class="form-control" placeholder="Enter Token Number" value=''>            
            </div>
          </div>
          <div class="col-md-5 col-lg-6">
          <button id='startButtonDisabled' style="pointer-events: none;" type="button" class="btn btn-primary start-btn" style="background: #dddddd;" >Click to Start</button>          
          <button id='startButtonenabled' type="button" class="btn btn-primary start-btn" onclick='return redirectexam()'>Click to Start</button>          
          </div>
          </div>
      </div>
    </div>
  </div>

 <script src="<?php echo BASE_PATH; ?>assets/onlineportal/js/jquery-1.12.4.min.js"></script>


<!-- First, include the Webcam.js JavaScript Library -->
      <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/webcamjs/1.0.25/webcam.js"></script>

  
  <!-- Configure a few settings and attach camera -->
  <script language="JavaScript">
    Webcam.set({
      width: 320,
      height: 240,
      image_format: 'jpeg',
      jpeg_quality: 90
    });
    Webcam.attach( '#my_camera' );
  </script>

  <script>

    $( document ).ready(function() {

  checkExamStarted();
});


  function checkExamStarted() {
    $("#startButtonDisabled").hide();
    $("#startButtonenabled").hide();
    $.get("/studentexamination/exam/checkExamStartedOrNot", function(data, status){
         if(data=='1') {
            $("#startButtonenabled").show();
         }
         if(data=='0') {
            $("#startButtonDisabled").show();
         }
    });
  }

  window.setInterval(function(){
  checkExamStarted();
}, 5000);


function hidetakingpic() {
    $("#captureimae").hide();
}

    function take_snapshot() {
        
        $("#captimage").hide();
        $("#retakeimg").show();
      // take snapshot and get image data
      Webcam.snap( function(data_uri) {
        // display results in page
        document.getElementById('results').innerHTML = 
          '<h2>Here is your image:</h2>' + 
          '<img src="'+data_uri+'"/>';
      } );
    }

 function redirectexam() {

  var token = $("#token").val();
  if(token=='') {
    alert('Please enter the token and click on Start to continue');
    return false;
  }
  $.get("/studentexamination/exam/checktoken/"+token, function(data, status){
         if(data=='1') {
           parent.location='/studentexamination/exam/start';
         }
         if(data=='0') {
            alert('Please enter valid token');
            return false;
         }
    });

 }

 function updateAnswerImage(examquestionid){
      
     Webcam.snap( function(data_uri) {
            $(".image-tag").val(data_uri);
              $.ajax({
                    url: "/studentexamination/exam/answersaveimage",
                    method: "POST",
                    data: {
                      image: data_uri,
                      examquestionid : examquestionid
                    },
                    success: function(data) {
                     
                    }
                  });
        } );
}
 


  </script>