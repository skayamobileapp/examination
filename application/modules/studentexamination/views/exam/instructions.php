<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <title>Quiz</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo BASE_PATH; ?>assets/onlineportal/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo BASE_PATH; ?>assets/onlineportal/css/font-awesome.min.css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css2?family=Ubuntu:wght@300;400;500;700&display=swap" rel="stylesheet">

     <link href="https://fonts.google.com/specimen/Orbitron?vfonly=true" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo BASE_PATH; ?>assets/onlineportal/css/main.css" rel="stylesheet" />
  </head>
  <body>

    <nav class="navbar navbar-expand-lg navbar-light main-header">
      <div class="container">
        <a class="navbar-brand" href="#">LOGO</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample02" aria-controls="navbarsExample02" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
      
        <div class="collapse navbar-collapse" id="navbarsExample02">
          <ul class="navbar-nav ml-auto align-items-center">
            <li class="nav-item">
              Welcome <?php echo $this->session->userdata['student_name'];?>
            </li>            
            <li class="nav-item active">
              <a href="https://register.ciif-global.org/" class="nav-link">Logout</a>
            </li>          
          </ul>          
        </div>
      </div>
    </nav> 

    <div class="container">
        <div class="row mt-5">
            
            <div class="col-sm-4">
               <img src='/assets/images/photo1.jpeg' class="img-fluid capture-id-img" style="height:295px;"/ >
               <h4 class="mt-3">Capture your ID / Photo</h4>
           </div>
           
           <div class="col-sm-4" id='capturingpic'>
                  <div id="my_camera" style="height:295px !important;"></div>
                    <button id='captimage'   type="button" class="btn btn-primary start-btn mt-3" onclick="take_snapshot()";>Click to Capture</button>
                                  
            </div>
            
            <div class="col-sm-4" id='finalisepic' style='display:none;'>
                 <div id="results"></div>    
                 <div id='btns'>
                <button id='imagebtn'   type="button" class="btn btn-primary start-btn mt-3" onclick="hidetakingpic()">Finalise</button>
                <button id='imagebtn'   type="button" class="btn btn-primary start-btn mt-3" onclick="takenew()">Retake</button>
                </div>
            </div>


              <div class="col-sm-4">
                  <div >
                    <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                          <input type='text' name='token' id='token' class="form-control" placeholder="Enter Token" value='' onkeyup="redirectexam()" autocomplete="off">   
                          <span id="errormsg" style="color:red;"></span>         
                        </div>
                     </div>
                     </div>
                      <div class="row">
                     <div class="col-sm-12">
                        <button id='startButtonDisabled' style="pointer-events: none;background-color: gainsboro;" type="button" class="btn btn-primary start-btn"  >Click to Start</button>          
    
                    </div>
                    </div>                      
                  </div>                  
                  <div class="countdown-card" id="countdownshow" style="display:none;">
                Click to Capture the picture <br/> COUNT DOWN TO START YOUR EXAM. ONCE THE COUNT REACHES 0. YOU NEED TO CLICK ON START BUTTON TO START THE EXAM <br/>
                 <p  id="countdown" class='countdowntimer' style="margin-top:-20px;"></p>
                 </div>

                  <div id="picturedivid" style="color:red;display:none;">
                Click to capture the image to proceed further <br/>
                 </div>


            </div>

        </div>
    <div class="row">
       
      <div class="col-sm-12"> <br/>

            <div class="row exam-message" id='showNotificationDivId' style="display:none;">
               <div class="col-sm-12">
                  <h5 style="color:red;">Notification</h5>
                 <table class="table">
                   <tr>
                    <th style="color:red;" id="latestinstruction"></th>
                  </tr>

                 </table>
            </div>
          </div>

            <div class="exam-message" style="font-size:18px;">
                <h4>Important</h4>
               <ol>
                   <li>Candidates are required to log on to the examination platform and/or video conferencing platform at least forty-five (45) minutes before the start of the examination. Candidates who log into the examination platform <span style='color:red;'>thirty (30) </span> minutes after an examination has commenced, shall not be permitted to take the examination.</li>
                   <li>If a candidate does happen to lose connection to the exam momentarily, whether due to internet issues or a loss of power, the exam will temporarily shut down. Once internet or power is regained, the candidate will be able to relaunch the exam from where he/she left off and the progress will have been saved.</li>
                   <li>In the event of a disconnection, the exam timer will stop and once re-launched, the candidate is able to resume from the same question the moment he/she left (got disconnected from) the exam.</li>
                   <li>However, if internet connection and/or power is lost for more than 30 minutes, the exam will be discontinued and the candidate is required to re-take the exam in the next scheduled examination sitting.</li>
<li>If internet connection and/or power is lost permanently less than 80% into the time after an examination has commenced, the candidate is required to re-take the exam in the next scheduled examination sitting.</li><li>If internet connection and/or power is lost permanently more than 80% into the time after an examination has commenced, the candidate is required to submit the exam as is.</li>
               </ol>
            </div>    
           
            

         
         
      </div>        
     

    </div>
  </div>
  <div class="custom-chatbot">
      <img src="/assets/images/chat.png" style="height: 124px; border-radius: 999px;" class="btn btn-primary chatbot-btn"><br/><span style="font-weight: bold;color:#00bcd2;">Live Support</span>
      <div class="chatbot-container">
        <div class="chatbot-header">
          <div class="header-text">Chat with Invigilator</div>
          <button class="btn chatbot-close">
            <svg
              fill="#FFFFFF"
              height="15"
              viewBox="0 0 15 15"
              width="15"
              xmlns="http://www.w3.org/2000/svg"
            >
              <line
                x1="1"
                y1="15"
                x2="15"
                y2="1"
                stroke="white"
                stroke-width="1"
              ></line>
              <line
                x1="1"
                y1="1"
                x2="15"
                y2="15"
                stroke="white"
                stroke-width="1"
              ></line>
            </svg>
          </button>
        </div>
        <div class="chat-room">
          <div class="message-area" id='chatdetails'>
           
          </div>
          <div class="input-group mb-3 input-group-lg">
            <input
              type="text"
              class="form-control"
              id='chat'
              placeholder="Enter Message"
            />
            <div class="input-group-append">
              <button class="btn btn-primary" type="button" onclick="chatdata()">Send</button>
            </div>
          </div>
        </div>
      </div>
    </div>
 <script src="<?php echo BASE_PATH; ?>assets/onlineportal/js/jquery-1.12.4.min.js"></script>
    <script src="<?php echo BASE_PATH;?>assets/onlineportal/js/bootstrap.min.js"></script>
    <script src="<?php echo BASE_PATH;?>assets/onlineportal/js/main.js"></script>


<!-- First, include the Webcam.js JavaScript Library -->
      <script type="text/javascript" src="<?php echo BASE_PATH; ?>assets/onlineportal/js/webcam.js"></script>

  
  <!-- Configure a few settings and attach camera -->
  <script language="JavaScript">
    Webcam.set({
      width: 320,
      height: 294,
      image_format: 'jpeg',
      jpeg_quality: 90
    });
    Webcam.attach( '#my_camera' );
  </script>

 <script>
 var countdownTimer,showtime;
  showtime = 0;
  var onloadfirsttime = 0;
        var seconds, upgradeTime;
        var minutes,remainingminute,remainingSeconds;
    $(document).ready(function() {


         $.get("/studentexamination/exam/getstarttimetime", function(data, status){

      var jsonparseData = jQuery.parseJSON(data);
      
       var remainingminute = parseInt(jsonparseData['remainingTimeInSec']);

       console.log(remainingminute);

       if(remainingminute==0){
                        $("#showtokendiv").show();
                $("#countdownshow").hide();

       }
                           $("#countdownshow").show();

       
        seconds = upgradeTime = parseInt(remainingminute)*(60);

      function timer() {
        var minutes = Math.floor(seconds / 60);
        var remainingSeconds = seconds % 60;
        // add a 0 in front of single digit seconds
        if (remainingSeconds < 10) {
            remainingSeconds = "0" + remainingSeconds;
        }
        // add a 0 in front of single digit minutes
        if (minutes < 10) {
            minutes = "0" + minutes;
        }

        document.getElementById('countdown').innerHTML =  minutes + ":" + remainingSeconds;

        
        if (parseInt(minutes) == 0 && parseInt(seconds)==0) {
                $("#showtokendiv").show();
                $("#countdownshow").hide();
                // reload page
        } else {
            seconds--;
        }
    }
    var countdownTimer = setInterval(timer, 1000);
          

         
    });
       });

      
        $('.chatbot-btn').on('click', function () {
          $(this).hide()
          $('.chatbot-container').show()
        })
        $('.chatbot-close').on('click', function () {
          $('.chatbot-container').hide()
          $('.chatbot-btn').show()
        })   


    
   
  </script>

  <script>

function chatdata(){

            var chat = $("#chat").val();

              $.ajax({
        url: "/studentexamination/exam/chat",
        method: "POST",
        data: {
          chat: chat
        },
        success: function(data) {
         
          getchatdata();
                  }
      });


        }


        function getchatdata(){
              $.get("/studentexamination/exam/chatdisplay", function(data, status){
           
                $("#chatdetails").html(data);
                $("#chat").val('');
            });
        }


function hidetakingpic() {
    $("#capturingpic").hide();
    $("#btns").hide();
}

function takenew(){
     $("#capturingpic").show();
        $("#finalisepic").hide();
}

    function take_snapshot() {
        
        $("#capturingpic").hide();
        $("#finalisepic").show();
      // take snapshot and get image data
      
      Webcam.snap( function(data_uri) {
          
          
           $.ajax({
                     url: "/studentexamination/exam/updateimageprofilepic",
                     method: "POST",
                     data: {
                       image: data_uri
                     },
                     success: function(data) {
                     
                     }
                   });
     
     
        // display results in page
        document.getElementById('results').innerHTML = 
          '<h2>Here is your image:</h2>' + 
          '<img src="'+data_uri+'"/>';
      } );
    }

 function redirectexam() {

  var token = $("#token").val();
  if(token=='') {
    $("#errormsg").html("Please enter the token");
    return false;
  }
  $.get("/studentexamination/exam/checktoken/"+token, function(newdata, status){
         if(newdata=='1') {

            $.get("/studentexamination/exam/checkexam/"+token, function(data, status){
                 if(data=='1') {


                   $.get("/studentexamination/exam/checkpicture/"+token, function(datap, statusp){
                   if(datap=='1') {
                                        parent.location='/studentexamination/exam/start';
                   } else {
                     $("#picturedivid").show();
                   }
                 });
                 }
                 if(data!='1') {
                    $("#errormsg").html(data);
                    return false;
                 }
            });


         }
         if(newdata=='0') {
            $("#errormsg").html("Please enter valid token");
            return false;
         }
    });

 }



  function getnotification() {
    $.get("/studentexamination/exam/notification", function(data, status){
        if(data!=''){
            $("#showNotificationDivId").show();
            $("#latestinstruction").html(data);
        }
         
        
    });
  }





  </script>

  <script type="text/javascript">
    setInterval(function() {
        $.get("/studentexamination/exam/checkchat", function(data, status){
          if(data>0) {

            getchatdata();

           
                             $('.chatbot-container').show();


                             $.get("/studentexamination/exam/updatechat", function(data, status){
              });







          }

         
    });


    redirectexam();



}, 10000);

</script>