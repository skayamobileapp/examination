<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


class Logistics extends BaseController
{
    

    function index($id=NULL,$md5one=NULL,$mdtwo=NULL,$uniquenumber=NULL)
    {
        // error_reporting(0);




        $splitnumbers = explode('-', $uniquenumber);

        if(md5($splitnumbers[0])!=$md5one) {
                    echo "<script>alert('This is not a valid link, please click the link from the Exam slip')</script>";
        echo "<script>parent.location='https://register.ciif-global.org/login'</script>";
        exit;
        }

         if(md5($splitnumbers[1])!=$mdtwo) {
                    echo "<script>alert('This is not a valid link, please click the link from the Exam slip')</script>";
        echo "<script>parent.location='https://register.ciif-global.org/login'</script>";
        exit;
        }
        $this->load->model('student/exam_student_tagging_model');
        $this->load->model('student/exam_set_model');

            $state = $this->exam_student_tagging_model->getAllStates();
        
       $resultsreg = $this->exam_student_tagging_model->getExamEvent($id);
       if($resultsreg->reg_state){
        echo "<script>alert('You have already defined the exam location')</script>";
        echo "<script>parent.location='https://register.ciif-global.org/login'</script>";
        exit;
       }
        $data['state'] = $state;
        // $data['city'] = $city;
        $data['error'] = $id;
        // print_r($data);exit;

        if($_POST) {
            
            $idstudentexamtagging = $id;
            $datas['reg_country'] = 1;
            $datas['reg_state'] = $_POST['state'];
            $datas['reg_city'] = $_POST['id_city'];
            $datas['ip'] = $_SERVER['REMOTE_ADDR'];

        $this->exam_set_model->updateAnswer($datas,$idstudentexamtagging);
        echo "<script>parent.location='https://register.ciif-global.org/login'</script>";
        exit;
        }


            $this->loadExamViews("logistics/index", $this->global, $data, NULL);
    }


     function thankyou($id=NULL)
    {
        $data = '';//https://exam.camsedu.com/studentexamination/logistics/index/199

        $this->global='';
                    $this->loadExamViews("logistics/thankyou", $this->global, $data, NULL);

    }


    function check($id=NULL)
    {
        $data = '';//https://exam.camsedu.com/studentexamination/logistics/index/199

        $this->global='';
                    $this->loadExamViews("logistics/check", $this->global, $data, NULL);

    }

    function allcity($id) {
        $this->load->model('student/exam_student_tagging_model');

      
            
            $results = $this->exam_student_tagging_model->getAllCity($id);

            $table="

         <script type='text/javascript'>
                 $('select').select2();
         </script>


        <select name='id_city' id='id_city' class='form-control'>
            <option value=''>Select</option>
            ";

        for($i=0;$i<count($results);$i++)
        {

        $id = $results[$i]->id;
        $name = $results[$i]->name;
        $table.="<option value=".$id.">".$name.
                "</option>";

        }
        $table.="

        </select>";

        echo $table;
        exit;
    }
}
?>
       
