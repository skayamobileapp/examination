<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


class Mockexam extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('student/exam_student_tagging_model');
        $this->load->model('student/exam_set_model');
        
        error_reporting(0);
    }

    function instructions($id=NULL,$mdencry=NULL)
    {
        

        $encrypted = md5(date('Y-m-d'));

        if($encrypted!=$mdencry) {
             echo "<script>alert('Not a valid exam link')</script>";
                 echo "<script>parent.location='https://register.ciif-global.org/login'</script>";
                 exit;
        }
        $user_id = $this->session->id_student;
        $examDetails = $this->exam_student_tagging_model->getExamDetails($user_id);


        $attemptnumber = 1;

            $user_id = $this->session->id_student;
            $examset_code = $this->session->examset_code;
            $exam = $this->exam_student_tagging_model->getStudentExam($user_id);



            $examDetails = $this->exam_student_tagging_model->getExamDetails($user_id);



            $examEvent = $examDetails->id_exam_event;
            //check the exam started or not for that exam event
            

            $studentExamAttemptData = $this->exam_set_model->studentExamAttempt($exam->examstudenttagging,$user_id);



             $this->session->id_student_exam_attempt = $studentExamAttemptData->id;

           if($studentExamAttemptData->exam_submitted==1) {
           
               if($exam->attempts==1) {
                 echo "<script>alert('You have already completed the exam')</script>";
                 echo "<script>parent.location='/studentLogin'</script>";
                 exit;
               }
           }



        $examEvent = $examDetails->id_exam_event;
            //check the exam started or not for that exam event
        $examCenterDetails = $this->exam_student_tagging_model->checkExamStarted($examEvent);
        $data['instruction'] = $examDetails->instructions;
        $data['error'] = $id;

            $this->loadExamViews("mockexam/instructions", $this->global, $data, NULL);
        
    }

    function checkExamStartedOrNot(){

        $examStarted = 0; 
        $user_id = $this->session->id_student;
        $examDetails = $this->exam_student_tagging_model->getExamDetails($user_id);

        $examEvent = $examDetails->id_exam_event;

            //check the exam started or not for that exam event
        // $examCenterDetails = $this->exam_student_tagging_model->checkExamStarted($examEvent);



        $user_id = $this->session->id_student;


        $examsetDetails = $this->exam_student_tagging_model->getExamSetDetails($examEvent);

        $exam = $this->exam_student_tagging_model->getStudentExamstart($user_id,$examsetDetails->tos_status);

        $time1 = strtotime($exam->exam_date.' '.$exam->from_tm);  
        $time2 = strtotime(date('Y-m-d H:i:s'));
        $completedTimeValid =  ($time2 - $time1);
        $currentDate = date('Y-m-d');


        $currentdatetime = strtotime(date('Y-m-d',$currentDate));

        $startdatetime = strtotime(date('Y-m-d',$examsetDetails->exam_date));
        $enddatetime = strtotime(date('Y-m-d',$examsetDetails->exam_end_date));


         if($startdatetime<=$currentdatetime && $enddatetime>=$currentdatetime) {
             
            if($completedTimeValid>0) {
              $examStarted = 1;
            }
            
         }


        echo $examStarted;
        exit;
    }

    function start()
    {
        
            $attemptnumber = 1;

            $user_id = $this->session->id_student;
            $examset_code = $this->session->examset_code;
            $exam = $this->exam_student_tagging_model->getStudentExam($user_id);

            $examDetails = $this->exam_student_tagging_model->getExamDetails($user_id);



            $examEvent = $examDetails->id_exam_event;

            //check the exam started or not for that exam event
            // $examStarted = 0;
           $examCenterDetails = $this->exam_student_tagging_model->checkExamStarted($examEvent);


              $currentdate = date('Y-m-d H:i');


             $durationtime =  '+ '.$examDetails->duration.' minutes';

              $examStartedDate = date('Y-m-d H:i',strtotime($examCenterDetails->exam_start_time));


             $todatetime =  date('Y-m-d H:i',strtotime($durationtime,strtotime($examStartedDate)));

             $fromdatetime = date('Y-m-d H:i',strtotime($currentdate));


            $idtos = $exam->id_tos;

            $time1 = strtotime($todatetime);  
            $time2 = strtotime($fromdatetime);
            $completedTimeValid =  ($time1 - $time2);
            $remainingTime = 5;

            


             $question = $this->exam_set_model->getAssignedMockQuestions();

             $this->session->id_student_exam_attempt = $studentExamAttemptData->id;
            // print_r($question);exit; 

            // save question set to student
            $data['completedTime'] = $completedTime;
            $data['remainingTime'] = $remainingTime;
            $data['extraTime'] = $exam->extra_time;
            $data['exam'] = $exam;
            $data['question'] = $question;
            $data['student_id'] = $user_id;

            $this->global['pageTitle'] = 'Student Exam : Start';
            $this->global['userInfo'] = $this->session->get_userdata();
            $this->loadExamViews("mockexam/start", $this->global, $data, NULL);
        
    }

    function creationofQuestion($attemptnumber){
            $user_id = $this->session->id_student;
            $examset_code = $this->session->examset_code;
            $exam = $this->exam_student_tagging_model->getStudentExam($user_id);
            $idtos = $exam->id_tos;
            $examdatastarted = array(
                    'id_student' => $user_id,
                    'id_exam_student_tagging' => $exam->examstudenttagging,
                    'start_time' =>date('Y-m-d H:i:s'),
                    'end_time' => $exam->examid,
                    'attempt_number' => $attemptnumber
                );
            $id_student_exam_attempt = $this->exam_set_model->studentStartedExam($examdatastarted);

            if($exam->behaviour=='2' && $attemptnumber>1) {
              $oldattemptNumber = $attemptnumber-1;
              $idoldStudentExamAttempt = $this->exam_set_model->getOldExamAttempt($exam->examid,$oldattemptNumber);


            $getDataFromOld = $this->exam_set_model->getOldQuestionByAttemptId($idoldStudentExamAttempt->id);
            shuffle($getDataFromOld);
            for($q=0;$q<count($getDataFromOld);$q++) {

                     $examdata = array(
                    'id_student' => $user_id,
                    'id_question' => $getDataFromOld[$q]['id_question'],
                    'question_order' => $q+1,
                    'id_exam_student_tagging' => $exam->examstudenttagging,
                    'id_answer' => 0,
                    'id_student_exam_attempt'=>$id_student_exam_attempt
                );

                $current_exam = $this->exam_set_model->saveUserExam($examdata);
            }




        } else {


          if($exam->tos_status=='1') {
            $id_manual_tos = $exam->id_manual_tos;
            $question = $this->exam_set_model->getQuestionByManualTos($id_manual_tos);

           $order = 0;
           $question_number = 1;
            for($q=0;$q<count($question);$q++) {


                $idquestion = $question[$q]['id_question'];
                $questionDetails = $this->exam_set_model->getQuestionDetailById($idquestion);

                if($questionDetails['question_type']=='2') {
                    $getChildQuestions = $this->exam_set_model->getAllChildQuestions($idquestion);

                    for($m=0;$m<count($getChildQuestions);$m++) {

                          if($m==0) {
                             $alphabet = '-A';
                          }
                          if($m==1) {
                             $alphabet = '-B';
                          }
                          if($m==2) {
                             $alphabet = '-C';
                          }
                          if($m==3) {
                             $alphabet = '-D';
                          }
                          if($m==4) {
                             $alphabet = '-E';
                          }

                     $examdata = array(
                        'id_student' => $user_id,
                        'id_question' => $getChildQuestions[$m]['id'],
                        'question_order' => $order++,
                        'id_exam_student_tagging' => $exam->examstudenttagging,
                        'id_answer' => 0,
                        'id_student_exam_attempt'=>$id_student_exam_attempt,
                        'question_number' =>  $question_number.' '.$alphabet
                      );


                    $current_exam = $this->exam_set_model->saveUserExam($examdata);
                    }
                } else {
                         $examdata = array(
                        'id_student' => $user_id,
                        'id_question' => $idquestion,
                        'question_order' => $order++,
                        'id_exam_student_tagging' => $exam->examstudenttagging,
                        'id_answer' => 0,
                        'id_student_exam_attempt'=>$id_student_exam_attempt,
                        'question_number' =>  $question_number

                    );

                    $current_exam = $this->exam_set_model->saveUserExam($examdata);
                }
                $question_number++;

              }
          }

          if($exam->tos_status=='0') {
            $tosdetails = $this->exam_set_model->getQuestionByTos($idtos);
              $question = array();
              for($i=0;$i<count($tosdetails);$i++) {
                    $questionList = $this->exam_set_model->getQuestionFromLogic($tosdetails[$i]);
                    for($l=0;$l<count($questionList);$l++) {
                          array_push($question, $questionList[$l]);
                    }
              }

              for($q=0;$q<count($question);$q++) {

                       $examdata = array(
                      'id_student' => $user_id,
                      'id_question' => $question[$q]['id'],
                      'question_order' => $q+1,
                      'id_exam_student_tagging' => $exam->examstudenttagging,
                      'id_answer' => 0,
                      'id_student_exam_attempt'=>$id_student_exam_attempt
                  );

                  $current_exam = $this->exam_set_model->saveUserExam($examdata);
              }
              
          }
        }
        $this->session->student_exam_time =  $data['student_exam_time'];

    }

    function markanswer($answerid = NULL,$examid=NULL) {
      //echo $answerid;
      //echo $examid;
       $data['id_answer'] = $answerid;
       $data['datetime'] = date('Y-m-d H:i:s');

              $getCorrectAnswerId = $this->exam_set_model->getCorrectAnswerById($examid);

               $data['marks'] = 0;
        if($answerid==$getCorrectAnswerId['id']) {
          $data['marks'] = 2;
        }


       $data['datetime'] = date('Y-m-d H:i:s');

       $tosdetails = $this->exam_set_model->updateAnswerOption($data,$examid);
       return 1;     
    }



    function checktoken($token){
      $user_id = $this->session->id_student;
            $exam = $this->exam_student_tagging_model->getStudentExam($user_id);
            if($exam->token==$token) {


               $startedExamOrNot = $this->exam_student_tagging_model->getexamStudentDetails($exam->examstudenttagging);

                if($startedExamOrNot->attendence_status=='') {
                     $data = array(
                      'attendence_status' =>1,
                      'exam_started_datetime' =>date('Y-m-d H:i:s')
                    );
                    $this->exam_student_tagging_model->editExamEvent($data,$exam->examstudenttagging);

                  }



              $returnvalue =  1;
            } else {
              $returnvalue =  0;
            }

      echo $returnvalue;
      exit;

             // $tosdetails = $this->exam_set_model->updateAnswerOption($data,$examid);

    }


    function markanswertext(){

      
      $examid = $_POST['question_id'];
      $data['answer_text'] = $_POST['answer_written'];
       $data['datetime'] = date('Y-m-d H:i:s');
       $tosdetails = $this->exam_set_model->updateAnswerOption($data,$examid);
       return 1; 

    }

    function getAllQuestionNumber(){

         $user_id = $this->session->id_student;
            $examset_code = $this->session->examset_code;
            $question = $this->exam_set_model->getAssignedMockQuestions();

        $table = "<div class='questions-list-container'>
              <h5>Section A <button class='btn btn-primary btn-sm close-btn' onclick='togglemenu()'>Close</button></h5>
              <ul class='questions-list'>";
              $j = 0;
              for($i=0;$i<count($question);$i++) {
                 if($question[$i]['question_type']=='1') {
                $j++;
                $idquestion = $question[$i]['id'];
                $question_number = $j;

                  $class ='';
                  if($question[$i]['id_answer']!=0) {
                      $class ='answered';
                  }

                  if($question[$i]['id_answer']==0 && $question[$i]['answer_text']!='') {
                      $class ='answered';                    
                  }
                $table.="<li class='$class' onclick='scrolldiv($idquestion)'>$question_number</li>";
              }
            }

                
               
              $table.="</ul>
                <h5>Section B <button class='btn btn-primary btn-sm close-btn' onclick='togglemenu()'>Close</button></h5>
               <ul class='questions-list'>";
                $j = 0;
              for($i=0;$i<count($question);$i++) {
                 if($question[$i]['question_type']=='2') {
               $j++;
                $idquestion = $question[$i]['id'];
                $question_number = $j;
                  $class ='';
                  if($question[$i]['id_answer']!=0) {
                      $class ='answered';
                  }

                  if($question[$i]['id_answer']==0 && $question[$i]['answer_text']!='') {
                      $class ='answered';                    
                  }
                $table.="<li class='$class' onclick='scrolldiv($idquestion)'>$question_number</li>";
              }
            }

                
               
              $table.="</ul>

               <ul class='question-list-help'>
                <li>Not Attempted</li>
                <li class='answered'>Answered</li>
                <li style='width:100%'>Scroll down at last to submit the answer</li>
              </ul>
            </div>             <div id='my_camera'></div>
";
            echo $table;
            exit;
    }

    function internetspeed() {
         $baseFromJavascript = $_POST['image'];
        $questionid = $_POST['examquestionid'];
              $datas['speed_test'] = $baseFromJavascript;

 $tosdetails = $this->exam_set_model->updateAnswerOption($datas,$questionid);        
    }

    function updateimage(){

                $user_id = $this->session->id_student;

        $baseFromJavascript = $_POST['image'];

     $savedtime = date('Ymdhis');

$data = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $baseFromJavascript));

$filepath = "examimages/$savedtime.jpeg"; ; // or image.jpg

// Save the image in a defined path
 file_put_contents($filepath,$data);
      $datas['image'] = $filepath;
       $tosdetails = $this->exam_set_model->updateAnswer($datas,$user_id);
       return 1; 
    }
    function answersaveimage() {
        
        
        $baseFromJavascript = $_POST['image'];
        $questionid = $_POST['examquestionid'];


$data = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $baseFromJavascript));

$filepath = "examimages/$questionid.jpeg"; // or image.jpg

// Save the image in a defined path
 file_put_contents($filepath,$data);
      $datas['student_image'] = $filepath;
       $tosdetails = $this->exam_set_model->updateAnswerOption($datas,$questionid);
       return 1; 

    }


    function getAllQuestionNumberOldWrking() {
      $table = " <div class='questions-list-container'>
              <h5>Questions <button class='btn btn-primary btn-sm close-btn' onclick='togglemenu()'>Close</button></h5>
              <ul class='questions-list'>
                <li class='answered'>1</li>
                <li class='answered'>2</li>
                <li class='not-answered'>3</li>
                <li class='answered'>4</li>
                <li class='not-answered'>5</li>
                <li class='current'>6</li>
                <li>7</li>
                <li>8</li>
                <li>9</li>
                <li>10</li>
                <li>11</li>
                <li>12</li>   
                <li>13</li>
                <li>14</li>
                <li>15</li>
                <li>16</li>
                <li>17</li>
                <li>18</li> 
                <li>19</li>
                <li>20</li>
                <li>21</li>
                <li>22</li>
                <li>23</li>
                <li>24</li>   
                <li>25</li>
                <li>26</li>
                <li>27</li>
                <li>28</li>
                <li>29</li>
                <li>30</li>  
                <li>31</li>
                <li>32</li>
                <li>33</li>   
                <li>34</li>
                <li>35</li>
                <li>36</li>
                <li>37</li>
                <li>38</li>
                <li>39</li> 
                <li>40</li>
                <li>41</li>
                <li>42</li>
                <li>43</li>
                <li>44</li>
                <li>45</li>   
                <li>46</li>
                <li>47</li>
                <li>48</li>
                <li>49</li>
                <li>50</li>                                                                               
              </ul>
              <ul class='question-list-help'>
                <li class='current'>Current</li>
                <li>Not Attempted</li>
                <li class='answered'>Answered</li>
                <li class='not-answered'>Not Answered</li>
              </ul>
            </div>";
            echo $table;
            exit;  
    }


    function submitmarks() {

                 $user_id = $this->session->id_student;
            $examset_code = $this->session->examset_code;


            $exam = $this->exam_student_tagging_model->getStudentExam($user_id);
  $examdatastarted = array(
                    'exam_submitted' => 1,
                    'submit_type' => 1,
                    'submitted_time' =>date('Y-m-d H:i:s')
                );
            $this->exam_set_model->studentExamClosed($examdatastarted,$this->session->student_exam_time);

         echo "1";
         return;


    }

    function submitmarksauto() {

                 $user_id = $this->session->id_student;
            $examset_code = $this->session->examset_code;


            $exam = $this->exam_student_tagging_model->getStudentExam($user_id);
  $examdatastarted = array(
                    'exam_submitted' => 1,
                    'submit_type' => 2,
                    'submitted_time' =>date('Y-m-d H:i:s')
                );
            $this->exam_set_model->studentExamClosed($examdatastarted,$this->session->student_exam_time);

         echo "1";
         return;


    }


    function pendingquestion() {
        $user_id = $this->session->id_student;
            $examset_code = $this->session->examset_code;
            $exam = $this->exam_student_tagging_model->getStudentExam($user_id);
        $id_student_exam_attempt = $this->session->id_student_exam_attempt;


                            $question = $this->exam_set_model->getAssignedQuestions($this->session->id_student_exam_attempt);
                $totalQuestionInExam = count($question);  


        $answeredQuestionMCQ = $this->exam_set_model->getAnsweredByStudent($id_student_exam_attempt);


        $answeredQuestionESsay = $this->exam_set_model->getAnsweredByStudentEssay($id_student_exam_attempt);

        $answeredQuestionTotal = count($answeredQuestionMCQ) + count($answeredQuestionESsay);

    if($totalQuestionInExam==$answeredQuestionTotal) {
        $answeredFull = 0;
    } else {
        $answeredFull = $totalQuestionInExam - $answeredQuestionTotal;
    }

     echo $answeredFull;
     return;


    }
    function thankyou()
    {

          $user_id = $this->session->id_student;
            $examset_code = $this->session->examset_code;
            $exam = $this->exam_student_tagging_model->getStudentExam($user_id);
        $totalQuestionInExam = $exam->question_count;
        $id_student_exam_attempt = $this->session->id_student_exam_attempt;
        $correctAnswerList = $this->exam_set_model->getCorrectAnswer($id_student_exam_attempt);
        $percentage = (count($correctAnswerList))/$totalQuestionInExam * 100;
        $data['correctAnswer'] = count($correctAnswerList);
        $data['percentage'] = $percentage;
        $data['totalQuestionInExam'] = $totalQuestionInExam;
        $data['exam'] = $exam;

        if($percentage>50) {
            $data['result'] = 'Pass';
            $data['result_status'] = '1';
        } else {
            $data['result'] = 'Fail';
            $data['result_status'] = '0';            
        }

        $this->global['pageTitle'] = 'Student Exam : Thank You';
        $this->global['userInfo'] = $this->session->get_userdata();
        $data['userInfo'] = $this->session->get_userdata();
            $this->loadExamViews("exam/thankyou", $this->global, $data, NULL);
    }


      function logout()
    {
        $sessionArray = array('userId'=> '',                    
                    'role'=> '',
                    'roleText'=> '',
                    'name'=> '',
                    'lastLogin'=>  '',
                    'isLoggedIn' => FALSE
            );
     $this->session->set_userdata($sessionArray);
     redirect('/studentLogin');
    }
    
}
