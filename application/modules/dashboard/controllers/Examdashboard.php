<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Examdashboard extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('exam_event_model');
        $this->isLoggedIn();
        error_reporting(0);
    }

    function list()
    {
        if ($this->checkAccess('exam_event.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        { 
            $formData['id_location'] = $this->security->xss_clean($this->input->post('id_location'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_exam_center'] = $this->security->xss_clean($this->input->post('id_exam_center'));

            $data['searchParam'] = $formData;

            $data['examEventList'] = $this->exam_event_model->examEventListSearchDashboard($formData);
            $data['locationList'] = $this->exam_event_model->examCenterLocationList();
            $data['examCenterList'] = $this->exam_event_model->examCenterList();

            $this->global['pageCode'] = 'examdashboard.list';
            $this->global['pageTitle'] = 'Examination Management System : Exam Events';
            $this->loadViews("examdashboard/list", $this->global, $data, NULL);
        }
    }

    function maildescription($id) {
      if ($this->checkAccess('exam_event.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        { 

            $data['emailList'] = $this->exam_event_model->getEmailById($id);

            $this->global['pageCode'] = 'examdashboard.list';
            $this->global['pageTitle'] = 'Examination Management System : Exam Events';
            $this->loadViews("examdashboard/maildescription", $this->global, $data, NULL);
        }
    }

    function getemail($id)
    {
        if ($this->checkAccess('exam_event.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        { 

            $data['emailList'] = $this->exam_event_model->getEmailHistoryByStudentId($id);

            $this->global['pageCode'] = 'examdashboard.list';
            $this->global['pageTitle'] = 'Examination Management System : Exam Events';
            $this->loadViews("examdashboard/email", $this->global, $data, NULL);
        }
    }


      function grader() {


                          


                           $file_data="<table align='center' width='100%' style='background-color:#00bcd2;'>
                                  <tr>
                                    <td style='text-align: left' width='60%' ><img src='https://ciifiscore.ciif-global.org/assets/images/logo.png' width='200px' /></td>
                                    
                                  </tr>
                                  </table> <br/>";


                           
                          $file_data.="<table width='100%'> 
                                      
                                        <tr>
                                        <td><br/> Dear Dr. Aishath Muneeza,,</td>
                                        </tr>

                                        <tr>
                                        <td><br/>The answer script for the subject of Economics & the Financial System (13th December 2022) is ready for grading.</span></td>
                                        <tr>
                                        <td><br/>Please find the details to access the system as per details below.</td>
                                        </tr>
                                        <tr>
                                        <td><br/>Username is: muneeza@inceif.org <br/>
                                            Password: c11f1234<br/>
                                            Total exam script: 1<br/><br/>
</td>
                                        </tr>
                                        <tr><td>For your convenience, we have attached the Grader's Guide on the usage of the system. Please feel free to go through the document.</td></tr>
                                        <tr>
                                        <td><br/>Should you require any assistance, feel free to contact me at 014-5067965.</td>
                                        </tr>
                                        <tr>
                                        <td><br/We look forward to having the graded exam script ready by Tuesday, 27th December 2022.
</b>  </td></tr>
                                        <tr>
                                        <td><br/>Thank you<br/>   <br/>  <b>Muhammad Ali Imran Mohamad</b><br/>Programmes and Assessments</td></tr>

                                      
                                        </table>
                                       
                                        ";


                           $file_data.="<table align='center' width='100%' style='background-color:#00bcd2;'>
                           <tr><td>&nbsp;<br/></td></tr>
                                  <tr>
                                    <td style='text-align: center;color:white;' height='50px;' >© 2022 Membership and Qualification Management System. All rights reserved.</td>
                                    
                                  </tr>
                                   <tr><td>&nbsp;<br/></td></tr>
                                  </table>";

                        $this->load->library('phpmailer_lib');

                      // PHPMailer object
                      $mail = $this->phpmailer_lib->load();

                      // SMTP configuration
                      $mail->isSMTP();
                      $mail->Host     = 'smtp.office365.com';
                      $mail->SMTPAuth = true;
                      $mail->Username = 'membership@ciif-global.org';
                      $mail->Password = 'fecmi1-fidmIs-vicjas';
                      $mail->SMTPSecure = 'tls';
                      $mail->Port     = 587;

                      $mail->setFrom('membership@ciif-global.org', 'CIIF');
                      // $mail->addReplyTo('info@example.com', 'CodexWorld');

                      // Add a recipient
                       //$mail->addAddress($examstudentDetails->email_id);
                      $mail->addAddress('muneeza@inceif.org');
                      $mail->addAttachment('assets/images/CIIF iScore_Exam Grader User Guide.pdf');   // I took this fr
                      // Add cc or bcc 
                      // $mail->addBCC('hartinah.annuar@ciif-global.org');
                      // $mail->addBCC('aimi.ghapor@ciif-global.org');
                      // $mail->addBCC('afif.akbal@ciif-global.org');
                      // $mail->addBCC('shaidatul-ic@inceif.org');
                      $mail->addBCC('askiran123@gmail.com');

                      // Email subject
                      $mail->Subject = 'CPIF: Submission of Exam Answer Scripts (Economics & the Financial System)';

                      // Set email format to HTML
                      $mail->isHTML(true);

                     
                      $mail->Body = $file_data;
                      
                      // Send email
                 $mail->send();

                 exit;

    }


      function students($id) {


                    $data['examStudentTaggingList'] = $this->exam_event_model->examStudentTaggingListSearch($id);
            $this->global['pageTitle'] = 'Examination Management System : Edit Exam Event';
            $this->global['pageCode'] = 'exam_event.edit';

            $this->loadViews("examdashboard/student", $this->global, $data, NULL);

          $randone = rand(000000000,999999999);
           $parameterone = md5($randone);

                    $randtwo = rand(000000000,999999999);
           $parametertwo = md5($randtwo);

           $uniquenumber = $randone.'-'.$randtwo;


            if($_POST) {




              for($i=0;$i<count($_POST['studentlist']);$i++) {
                $id = $_POST['studentlist'][$i];

                  $examstudentDetails = $this->exam_event_model->examStudentTaggingListSearchByStudentExamid($id);

                  $fromTime =  date('h:i a', strtotime($examstudentDetails->from_tm));
                  $toTime =  date('h:i a', strtotime($examstudentDetails->to_tm));


                          $currentDate = date('d-m-Y');
                          $currentTime = date('H:i:s');

                          $examDate = date('d-m-Y',strtotime($examstudentDetails->exam_date));

                         $dayname =  date('l', strtotime($examDate));
                          


                           $file_data="<table align='center' width='100%' style='background-color:#00bcd2;'>
                                  <tr>
                                    <td style='text-align: left' width='60%' ><img src='https://ciifiscore.ciif-global.org/assets/images/logo.png' width='200px' /></td>
                                    
                                  </tr>
                                  </table> <br/>";


                           
                          $file_data.="<table width='100%'> 
                                       <tr>
                                        <td><br/>  <span style='color:black;'>Exam</span> Registration for $examstudentDetails->coursename</td>
                                        </tr>
                                        <tr>
                                        <td><br/> Dear $examstudentDetails->student_name,</td>
                                        </tr>

                                        <tr>
                                        <td><br/>Please find your Examination Registration details <span style='color:black;'>as per below.</span></td>
                                        </tr>

                                        <tr>
                                        <td><br/><span style='color:black;'>Kindly</span> verify the location of your <span style='color:black;'>examination</span> at least <b>ONE DAY</b> before the examination. Click on the URL to verify your <span style='color:black;'>location:</span>  <span style='color:#00bcd2;'><a href='https://ciifiscore.ciif-global.org/studentexamination/logistics/index/$id/$parameterone/$parametertwo/$uniquenumber'>Click Here</span></td>
                                        </tr>
                                        <tr>
                                        <td><br/><span style='color:black;'>To ensure your internet connection is compatible with our operating system (this includes webcam and audio setting), please go through the Online Examination Compatibility Test. The test can be accessed before sitting for the examination via this link</span>: <span style='color:#00bcd2;'><a href='https://ciifiscore.ciif-global.org/studentexamination/logistics/check'>Click here</a></span></td>
                                        </tr>
                                        <tr><td>Candidates are required to log on to the examination platform and/or video conferencing platform <b>at least forty-five (45) minutes before</b> the start of the examination. Candidates who log into the examination platform forty-five (45) minutes after an examination has commenced, shall not be permitted to take the examination.</td></tr>
                                        <tr>
                                        <td><br/>Please note that <span style='color:black;'>CIIF</span> has the rights to make changes to the date, venue, and time of the examination which you will be notified accordingly.</td>
                                        </tr>
                                      
                                        </table>
                                        <table>
                                        <tr><td><br/></td></tr>
                                        </table>
                                        <table width='90%' border='1' style='border-collapse: collapse;' cellpadding='10'>
                                        <tr>
                                         <th colspan='3' style='background-color:#ebebeb'>EXAMINATION REGISTRATION DETAILS </th>
                                         </tr>
                                        <tr>
                                        <td width='30%'>TOKEN NUMBER </td>
                                        <td width='5%' style='text-align:center;'> : </td>
                                        <td>$examstudentDetails->token</td>
                                        </tr>
                                         <tr>
                                        <td>NAME </td>
                                          <td width='5%' style='text-align:center;'> : </td>
                                        <td>$examstudentDetails->student_name</td>
                                        </tr>
                                         <tr>
                                        <td>NRIC </td>
                                          <td width='5%' style='text-align:center;'> : </td>
                                        <td>$examstudentDetails->nric</td>
                                        </tr>
                                         <tr>
                                        <td>MODULE </td>
                                          <td width='5%' style='text-align:center;'> : </td>
                                        <td>$examstudentDetails->coursename</td>
                                        </tr>
                                         <tr>
                                        <td>VENUE </td>
                                          <td width='5%' style='text-align:center;'> : </td>
                                        <td>$examstudentDetails->examcentername</td>
                                        </tr>
                                        <tr>
                                        <td>DATE </td>
                                          <td width='5%' style='text-align:center;'> : </td>
                                        <td>$examDate, $dayname</td>
                                        </tr>
                                        </table> 
                                        <table>
                                        <tr><td><br/></td></tr>
                                        </table>
                                        <table width='90%' border='1' style='border-collapse: collapse;' cellpadding='10' >
                                        <tr style='background-color:#ebebeb'>
                                         <th style='text-align:center;'>DATE </th>
                                         <th style='text-align:center;'>MODULE </th>
                                         <th style='text-align:center;'>SESSION </th>
                                         <th style='text-align:center;'>EXAM CENTER </th>
                                         </tr>
                                        
                                        <tr>
                                        <td style='text-align:center;'>$examDate, $dayname</td>
                                        <td style='text-align:center;'>$examstudentDetails->coursename </td>
                                        <td style='text-align:center;'> $fromTime - $toTime (Malaysian Time)</td>
                                        <td style='text-align:center;'>$examstudentDetails->examcentername </td>
                                        
                                         
                                        </tr>
                                        </table>
                                        <table>
                                        <tr>
                                        <td><br/><br/><br/></td></tr>
                                        </table>
                                        ";


                           $file_data.="<table align='center' width='100%' style='background-color:#00bcd2;'>
                           <tr><td>&nbsp;<br/></td></tr>
                                  <tr>
                                    <td style='text-align: center;color:white;' height='50px;' >© 2022 Membership and Qualification Management System. All rights reserved.</td>
                                    
                                  </tr>
                                   <tr><td>&nbsp;<br/></td></tr>
                                  </table>";

                        $this->load->library('phpmailer_lib');

                      // PHPMailer object
                      $mail = $this->phpmailer_lib->load();

                      // SMTP configuration
                      $mail->isSMTP();
                      $mail->Host     = 'smtp.office365.com';
                      $mail->SMTPAuth = true;
                      $mail->Username = 'membership@ciif-global.org';
                      $mail->Password = 'fecmi1-fidmIs-vicjas';
                      $mail->SMTPSecure = 'tls';
                      $mail->Port     = 587;

                      $mail->setFrom('membership@ciif-global.org', 'CIIF');
                      // $mail->addReplyTo('info@example.com', 'CodexWorld');

                      // Add a recipient
                       $mail->addAddress($examstudentDetails->email_id);
                      // $mail->addAddress('askiran123@gmail.com');

                       $mail->addAttachment('assets/images/CIIF iScore Online Exam_Candidates User Guide 2022.pdf');   // I took this fr


                      // Add cc or bcc 
                       $mail->addBCC('askiran123@gmail.com');
                      // $mail->addBCC('bcc@example.com');


                      // Email subject
                      $mail->Subject = 'CIIF - Registration slip';

                      // Set email format to HTML
                      $mail->isHTML(true);

                     
                      $mail->Body = $file_data;
                      
                      // Send email
                  if (!$mail->send()) {
                      echo 'Message could not be sent.';
                      echo 'Mailer Error: ' . $mail->ErrorInfo;
                  } else {
                     $data = array(
                    'email' => $examstudentDetails->email_id,
                    'mail_description' => $file_data,
                    'date_time' => date('Y-m-d H:i:s'),
                    'subject' => 'CIIF - Registration slip',
                    'student_id' => $id
                );


                $this->exam_event_model->addEmailHistory($data);

                    

                 echo "<Pre>";print_r($data);exit();

               

                      //echo 'Message has been sent';
                  }
        
              }
            }

    }

      function downloadQuestion($id)
    {




        // To Get Mpdf Library
        $this->getMpdfLibrary();

        $examstudentDetails = $this->exam_event_model->examStudentTaggingListSearchByStudentExamid($id);
         // print_r($examstudentDetails);exit;

$fromTime =  date('h:i a', strtotime($examstudentDetails->from_tm));
$toTime =  date('h:i a', strtotime($examstudentDetails->to_tm));

         $mpdf=new \Mpdf\Mpdf(); 

        $currentDate = date('d-m-Y');
        $currentTime = date('H:i:s');

        $examDate = date('d-m-Y',strtotime($examstudentDetails->exam_date));

       $dayname =  date('l', strtotime($examDate));
        

        $signature = $_SERVER['DOCUMENT_ROOT']."/assets/images/logo.png";

                $signature = $_SERVER['DOCUMENT_ROOT']."/assets/images/ciif_logo.svg";


         $file_data.="<table align='center' width='100%' style='background-color:#00bcd2;'>
                <tr>
                  <td style='text-align: left' width='60%' ><img src='$signature' width='200px' /></td>
                  
                </tr>
                </table> <br/>";


         
        $file_data.="<table width='100%'> 
                     <tr>
                      <td><br/>  <span style='color:black;'>Exam</span> Registration for $examstudentDetails->coursename</td>
                      </tr>
                      <tr>
                      <td><br/> Dear $examstudentDetails->student_name,</td>
                      </tr>

                      <tr>
                      <td><br/>Please find your Examination Registration details <span style='color:black;'>as per below.</span></td>
                      </tr>

                      <tr>
                      <td><br/><span style='color:black;'>Kindly</span> verify the location of your <span style='color:black;'>examination</span> at least <b>ONE DAY</b> before the examination. Click on the URL to verify your <span style='color:black;'>location:</span>  <span style='color:#00bcd2;'>https://ciifiscore.ciif-global.org/studentexamination/logistics/index/$id</span></td>
                      </tr>
                      <tr>
                      <td><br/><span style='color:black;'>To ensure your internet connection is compatible with our operating system (this includes webcam and audio setting), please go through the Online Examination Compatibility Test. The test can be accessed before sitting for the examination via this link</span>: <span style='color:#00bcd2;'>https://ciifiscore.ciif-global.org/studentexamination/logistics/check</span></td>
                      </tr>
                      <tr>
                      <td><br/>Please note that <span style='color:black;'>CIIF</span> has the rights to make changes to the date, venue, and time of the examination which you will be notified accordingly.</td>
                      </tr>
                    
                      </table>
                      <table>
                      <tr><td><br/></td></tr>
                      </table>
                      <table width='90%' border='1' style='border-collapse: collapse;' cellpadding='10'>
                      <tr>
                       <th colspan='3' style='background-color:#ebebeb'>EXAMINATION REGISTRATION DETAILS </th>
                       </tr>
                      <tr>
                      <td width='30%'>TOKEN NUMBER </td>
                      <td width='5%' style='text-align:center;'> : </td>
                      <td>$examstudentDetails->token</td>
                      </tr>
                       <tr>
                      <td>NAME </td>
                        <td width='5%' style='text-align:center;'> : </td>
                      <td>$examstudentDetails->student_name</td>
                      </tr>
                       <tr>
                      <td>NRIC </td>
                        <td width='5%' style='text-align:center;'> : </td>
                      <td>$examstudentDetails->nric</td>
                      </tr>
                       <tr>
                      <td>MODULE </td>
                        <td width='5%' style='text-align:center;'> : </td>
                      <td>$examstudentDetails->coursename</td>
                      </tr>
                       <tr>
                      <td>VENUE </td>
                        <td width='5%' style='text-align:center;'> : </td>
                      <td>$examstudentDetails->examcentername</td>
                      </tr>
                      <tr>
                      <td>DATE </td>
                        <td width='5%' style='text-align:center;'> : </td>
                      <td>$examDate, $dayname</td>
                      </tr>
                      </table> 
                      <table>
                      <tr><td><br/></td></tr>
                      </table>
                      <table width='90%' border='1' style='border-collapse: collapse;' cellpadding='10' >
                      <tr style='background-color:#ebebeb'>
                       <th style='text-align:center;'>DATE </th>
                       <th style='text-align:center;'>MODULE </th>
                       <th style='text-align:center;'>SESSION </th>
                       <th style='text-align:center;'>EXAM CENTER </th>
                       </tr>
                      
                      <tr>
                      <td style='text-align:center;'>$examDate, $dayname</td>
                      <td style='text-align:center;'>$examstudentDetails->coursename </td>
                      <td style='text-align:center;'> $fromTime - $toTime (Malaysian Time)</td>
                      <td style='text-align:center;'>$examstudentDetails->examcentername </td>
                      
                       
                      </tr>
                      </table>
                      <table>
                      <tr>
                      <td><br/><br/><br/></td></tr>
                      </table>
                      ";


         $file_data.="<table align='center' width='100%' style='background-color:#00bcd2;'>
         <tr><td>&nbsp;<br/></td></tr>
                <tr>
                  <td style='text-align: center;color:white;' height='200px;' >© 2022 Membership and Qualification Management System. All rights reserved.</td>
                  
                </tr>
                 <tr><td>&nbsp;<br/></td></tr>
                </table>";

         $mpdf->WriteHTML($file_data);
            $mpdf->Output($nric . 'Exam_slip.pdf', 'D');
            exit;
    }
   
}
