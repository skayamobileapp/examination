<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
  <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
    <h1 class="h3">Student Email History</h1>
       <a href='../list' class="btn btn-link ml-auto">
            <i class="fa fa-chevron-left" aria-hidden="true"></i>
          Back
        </a>

  </div>
 
  <div class="form-container">

   <form method="POST" action="">
    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
                            
            <th>Date Time</th>
            <th>Subject </th>
          </tr>
        </thead>
        <tbody>
         
              <tr>
                
                <td><?php echo date('d-m-Y H:i:s',strtotime($emailList->date_time)); ?></td>
                <td><?php echo $emailList->subject; ?></td>
              </tr>
               <tr>
                
                <td colspan="4"><?php echo $emailList->mail_description; ?></td>
              </tr>
              

                               
             
        </tbody>
      </table>
    </div>
  </form>
  </div>
</main>
<script>
  $('select').select2();

  function clearSearchForm() {
    window.location.reload();
  }


  function validate()
  {
    var statuscheck = $("#checkAll").is(':checked');

    if(statuscheck==true)
    {
        $('input:checkbox').prop('checked', true);
    }

    if(statuscheck==false)
    {
        $('input:checkbox').prop('checked', false);
    }    
  }

  
</script>