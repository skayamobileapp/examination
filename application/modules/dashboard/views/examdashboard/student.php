<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
  <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
    <h1 class="h3">Student Exam Registration</h1>
       <a href='../list' class="btn btn-link ml-auto">
            <i class="fa fa-chevron-left" aria-hidden="true"></i>
          Back
        </a>

  </div>
  <div class="form-container">

        <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Exam Event</th>
                        <th>Module</th>

            <th>Exam Date</th>
            <th>Timings</th>
          </tr>
              <?php
         
            for($i=0;$i<1;$i++) {
          ?>
          <tr>
                                <td><?php echo $examStudentTaggingList[0]->exam_event ?></td>

           <td><?php echo $examStudentTaggingList[0]->coursename ?></td>

                <td><?php echo date('d-m-Y', strtotime($examStudentTaggingList[0]->exam_date));?></td>
                <td><?php echo $examStudentTaggingList[0]->from_tm ?> - <?php echo $examStudentTaggingList[0]->to_tm ?></td>
                
          </tr>
        <?php } ?> 
        </thead>
        
      </table>
    </div>
</div>
  <div class="form-container">

   <form method="POST" action="">
    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Sl. No</th>
                              <th>Check All<input type='checkbox' name='checkAll' id='checkAll' onclick="validate()" /></th>
            <th>Student</th>
            <th>Email</th>
            <th>Membership Number</th>
            <th>Batch</th>
            <th>Token <br/> Logistic Confirmation Link</th>

            <th>Internet Speed</th>
                        <th>IP</th>

            <th>MAC ID</th>
            <th>Location</th>
            <th>Exam Slip Download</th>
            <th>Email History</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($examStudentTaggingList)) {
            $i = 1;
            foreach ($examStudentTaggingList as $record) {
          ?>
              <tr>
                <td><?php echo $i ?></td>
                <td><input type='checkbox' name='studentlist[]' value="<?php echo $record->id;?>" /></td>
                <td><?php echo $record->student_name; ?></td>
                <td><?php echo $record->email_id; ?></td>
                <td><?php echo $record->membership_number; ?></td>

                <td><?php echo $record->batch; ?></td>
                <td><?php echo $record->token; ?>  / 
                  <a href="/studentexamination/logistics/index/<?php echo $record->id;?>">Click to Confirm</td>


                <td><?php echo $record->internet_speed; ?></td>
                <td><?php echo $record->ip; ?></td>
                <td><?php echo $record->mac_id; ?></td>
                <td><?php echo $record->statename; ?> <br/>
                  <?php echo $record->cityname; ?>
                </td>
                 <td><a href="/dashboard/Examdashboard/downloadQuestion/<?php echo $record->id;?>">Download</a></td>
 <td><a href="/dashboard/Examdashboard/getemail/<?php echo $record->id;?>">View</a></td>


                               
              </tr>
          <?php
              $i++;
            }
          }
          ?>
          <tr>
            <td colspan="2"><input type='submit' class="btn btn-primary" value='Send Email'></td>
        </tbody>
      </table>
    </div>
  </form>
  </div>
</main>
<script>
  $('select').select2();

  function clearSearchForm() {
    window.location.reload();
  }


  function validate()
  {
    var statuscheck = $("#checkAll").is(':checked');

    if(statuscheck==true)
    {
        $('input:checkbox').prop('checked', true);
    }

    if(statuscheck==false)
    {
        $('input:checkbox').prop('checked', false);
    }    
  }

  
</script>