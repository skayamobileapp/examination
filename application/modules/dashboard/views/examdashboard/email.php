<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
  <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
    <h1 class="h3">Student Email History</h1>
       <a href='../list' class="btn btn-link ml-auto">
            <i class="fa fa-chevron-left" aria-hidden="true"></i>
          Back
        </a>

  </div>
 
  <div class="form-container">

   <form method="POST" action="">
    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Sl. No</th>
                            
            <th>Date Time</th>
            <th>Subject </th>
            <th>Mail Description</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($emailList)) {
            $i = 1;
            foreach ($emailList as $record) {
          ?>
              <tr>
                <td><?php echo $i ?></td>
                
                <td><?php echo date('d-m-Y H:i:s',strtotime($record->date_time)); ?></td>
                <td><?php echo $record->subject; ?></td>

                <td><?php echo $record->token; ?>  / 
                  <a href="/dashboard/Examdashboard/maildescription/<?php echo $record->id;?>">View Description</td>

                               
              </tr>
          <?php
              $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>
  </form>
  </div>
</main>
<script>
  $('select').select2();

  function clearSearchForm() {
    window.location.reload();
  }


  function validate()
  {
    var statuscheck = $("#checkAll").is(':checked');

    if(statuscheck==true)
    {
        $('input:checkbox').prop('checked', true);
    }

    if(statuscheck==false)
    {
        $('input:checkbox').prop('checked', false);
    }    
  }

  
</script>