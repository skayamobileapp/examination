<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
  <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
    <h1 class="h3">Exam Event Dashboard</h1>
  </div>
  <div class="page-container">
    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">

    </div>
    <?php
    if ($this->session->flashdata('success')) {
    ?>
      <div class="alert alert-success alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
      </div>
    <?php
    }
    if ($this->session->flashdata('error')) {
    ?>
      <div class="alert alert-danger alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Failure!</strong> <?php echo $this->session->flashdata('error'); ?>
      </div>
    <?php
    }
    ?>
    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Sl. No</th>
          
            <th>Exam Sitting</th>
            <th>Exam Event Name</th>
            <th>Exam Center</th>
            <th>Exam Date</th>
            <th>Timings</th>

            <th>Total Students</th>
                        <th>Exam Set</th>
                        <th>Grade</th>
                        <th style="text-align: center;">View Students</th>

          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($examEventList)) {
            $i = 1;
                    $this->load->model('exam_event_model');
                    $this->load->model('exam/grader_exam_model');

            foreach ($examEventList as $record) {

              $totalStuents = 0;
              $totalStudetnsObject = $this->exam_event_model->examStudentTaggingListSearch($record->id);

              $totalStuents = count($totalStudetnsObject);




               $totalStudentforexamevent = 0;
              $totalAssignedGrader = 0;
              $totalStudentPergrader = 0;


              $countOfStudent = $this->grader_exam_model->getCountOfQuestions($record->id);
              if($countOfStudent) {
                 $totalStudentforexamevent = $countOfStudent[0]->totalcount;
              }


               $getGraderForExamEvent = $this->grader_exam_model->getGraderCount($record->id);
               
               if($getGraderForExamEvent) {

                for($l=0;$l<count($getGraderForExamEvent);$l++) {

                  $id_grader_exam = $getGraderForExamEvent[$l]->id;

                  $getGraderForExamEventResults = $this->grader_exam_model->getGraderStudentCount($id_grader_exam);

                   $totalStudentPergrader = $getGraderForExamEventResults[0]->totalcount;

                    $totalAssignedGrader = $totalAssignedGrader + $totalStudentPergrader;
                }
              }
               $programmeName = '';
              if($record->mqms_type=='CPIF') {
                 $programmeName = $record->programme_level_ciif."<br/>".$record->programme_name_ciif."<br/>".$record->name;
              } else  {
                 $programmeName = $record->name;
              }

          ?>
              <tr>
                <td><?php echo $i ?></td>
                <td><?php echo $record->exam_sitting_name ?> - <?php echo date('Y', strtotime($record->exam_date)) ?></td>                
                <td><?php echo $programmeName ?></td>
                <td><?php echo $record->examcenternae ?></td>
                <td><?php echo date('d-m-Y', strtotime($record->exam_date)) ?></td>
                <td><?php echo date("H:i", strtotime($record->from_tm)) ?> - <?php echo date("H:i", strtotime($record->to_tm)) ?></td>

               
                <td>
                  <?php echo $totalStuents;?>
                </td>

                <td>
                  <?php if($record->id_exam_set) { ?> 

                    <p style='color:green;'>Exam Set Defined</p>
                   <?php } else { ?>
                    <p><a href='/question/examset/list'  style='color:red;'>Create Exam Question Set</a></p> <br/>

                    <p><a href='/exam/examEvent/edit/<?php echo $record->id ?>'  style='color:red;'>Tag Exam set to Event</a></p>

                   <?php } ?> 
                </td>
                
                
                  <td> 
                    <?php 

                     if($totalStudentforexamevent==0) { ?>
                   <a href="/exam/graderExam/list"  style='color:red;'>Assign Grader to Students</a>

                    <?php  } else if ($totalStudentforexamevent!=$totalAssignedGrader) { ?>
                   <a href="/exam/graderExam/list"  style='color:red;'>Assign Grader to Students</a>
                   <?php } else { ?>
                   <a href="/exam/graderExam/list" style='color:green;'>Grader has been Assigned to All Students</a>

                   <?php } ?> 
                </td>
               
               
                

                 <td class="text-center">

                  <a href="<?php echo 'students/' . $record->id; ?>" class="btn" type="" data-toggle="tooltip" data-placement="top" title="Edit">
                    View Students
                  </a>

                </td>


              </tr>
          <?php
              $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
</main>
<script>
  $('select').select2();

  function clearSearchForm() {
    window.location.reload();
  }
</script>