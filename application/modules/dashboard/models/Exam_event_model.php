<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Exam_event_model extends CI_Model
{
    function getCentersByLocatioin($id_location)
    {
        $this->db->select('DISTINCT(a.id) as id, a.*');
        $this->db->from('exam_center as a');
        $this->db->join('exam_center_location as ecl', 'a.id_location = ecl.id');
            $this->db->where('a.id_location', $id_location);
         $query = $this->db->get();
         $result = $query->result();
         // print_r($result);exit();      
         return $result;
    }

  function getCourseListByType($id) {
         $this->db->select('*');
        $this->db->from('course');
        $this->db->order_by("name", "ASC");
        $this->db->where('type', $id);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }


     function getExamSitting() {
         $this->db->select('*');
        $this->db->from('exam_sitting');
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }



     function examStudentTaggingListSearch($id_exam_event)
    {
        // $date = 
        $this->db->select(' a.*, s.full_name as student_name, s.nric,s.batch,s.membership_number, ee.name as exam_event, ee.exam_date,ee.from_tm,c.name as coursename,s.ip,s.mac_id,s.location,st.name as statename,ct.name as cityname,s.email_id');
        $this->db->from('exam_student_tagging as a');
        $this->db->join('exam_event as ee', 'a.id_exam_event = ee.id','left');
        $this->db->join('examset as es', 'ee.id_exam_set = es.id','left');
        $this->db->join('course as c', 'ee.id_course = c.id','left');
        $this->db->join('student as s', 'a.id_student = s.id','left');
        $this->db->join('state as st', 'a.reg_state = st.id','left');
        $this->db->join('city as ct', 'a.reg_city = ct.id','left');
       
            $this->db->where('a.id_exam_event', $id_exam_event);
       
        $query = $this->db->get();
        $result = $query->result();

        return $result;
    }



     function examStudentTaggingListSearchByStudentExamid($studentId)
    {
        // $date = 
        $this->db->select(' a.*, s.email_id,s.full_name as student_name, s.nric,s.batch,s.membership_number, ee.name as exam_event, ee.exam_date,ee.from_tm,c.name as coursename,s.ip,s.mac_id,s.location,ec.name as examcentername,ee.to_tm,s.email_id');
        $this->db->from('exam_student_tagging as a');
        $this->db->join('exam_event as ee', 'a.id_exam_event = ee.id','left');
        $this->db->join('exam_center as ec', 'ee.id_exam_center = ec.id','left');
        $this->db->join('examset as es', 'ee.id_exam_set = es.id','left');
        $this->db->join('course as c', 'ee.id_course = c.id','left');
        $this->db->join('student as s', 'a.id_student = s.id','left');
       
            $this->db->where('a.id', $studentId);
       
        $query = $this->db->get();
        $result = $query->row();

        return $result;
    }




      function getExamsetById($id)
    {
        $this->db->select('c.*');
        $this->db->from('examset as c');
        $this->db->where('c.id', $id);
        $query = $this->db->get();
        $result = $query->row();

        return $result;
    }

     function getExamSetByCourse($id)
    {
        $this->db->select('c.*');
        $this->db->from('examset as c');
        $this->db->where('c.id_course', $id);
        $this->db->where("c.status='1'");
        $query = $this->db->get();
        $result = $query->result();

        return $result;
    }


    function getEmailById($id) {
        // $date = 
        $this->db->select(' a.*, s.full_name as student_name, s.nric,s.batch,s.membership_number,s.email_id');
        $this->db->from('email_history as a');
        $this->db->join('student as s', 'a.student_id = s.id','left');
       
            $this->db->where('a.id', $id);
       
        $query = $this->db->get();
        $result = $query->row();

        return $result;
    }


    function getEmailHistoryByStudentId($id) {
        // $date = 
        $this->db->select(' a.*, s.full_name as student_name, s.nric,s.batch,s.membership_number,s.email_id');
        $this->db->from('email_history as a');
        $this->db->join('student as s', 'a.student_id = s.id','left');
       
            $this->db->where('a.student_id', $id);
       
        $query = $this->db->get();
        $result = $query->result();

        return $result;


    }


    function examEventListSearchDashboard($data){
        // $date = 
        $this->db->select('DISTINCT(a.id) as id, a.*,esit.exam_sitting_name as exam_sitting_name,c.name as coursename,c.code as coursecode,ecl.name as examcenternae');
        $this->db->from('exam_event as a');
        $this->db->join('exam_center as ecl', 'a.id_exam_center = ecl.id','left');
        $this->db->join('exam_sitting as esit', 'a.id_exam_sitting = esit.id','left');
        $this->db->join('examset as es', 'es.id=a.id_exam_set','left');
        $this->db->join('course as c', 'c.id=a.id_course','left');

        if ($data['name'] != '')
        {
            $likeCriteria = "(name  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['id_location'] != '')
        {
            $this->db->where('a.id_location', $data['id_location']);
        }
        if ($data['id_exam_center'] != '')
        {
            $this->db->where('a.id_exam_center', $data['id_exam_center']);
        }
     $this->db->where("date(a.exam_date) >= ", date('Y-m-d'));
        $this->db->order_by("a.exam_date", "ASC");
         $query = $this->db->get();
         $list = $query->result();  
         return $list;
    }

    

    function examEventListSearch($data)
    {
        // $date = 
        $this->db->select('DISTINCT(a.id) as id, a.*,esit.exam_sitting_name as exam_sitting_name,c.name as coursename,c.code as coursecode,ecl.name as examcenternae');
        $this->db->from('exam_event as a');
        $this->db->join('exam_center as ecl', 'a.id_exam_center = ecl.id','left');
        $this->db->join('exam_sitting as esit', 'a.id_exam_sitting = esit.id','left');
        $this->db->join('examset as es', 'es.id=a.id_exam_set','left');
        $this->db->join('course as c', 'c.id=a.id_course','left');

        if ($data['name'] != '')
        {
            $likeCriteria = "(name  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['id_location'] != '')
        {
            $this->db->where('a.id_location', $data['id_location']);
        }
        if ($data['id_exam_center'] != '')
        {
            $this->db->where('a.id_exam_center', $data['id_exam_center']);
        }
            // $this->db->where("date(a.exam_date) >= ", date('Y-m-d'));
        $this->db->order_by("a.exam_date", "ASC");
         $query = $this->db->get();
         $list = $query->result();  
         return $list;
    }

    function getExamEvent($id)
    {
        $this->db->select('*');
        $this->db->from('exam_event');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function addEmailHistory($data) {
         $this->db->trans_start();
        $this->db->insert('email_history', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }
    
    function addExamEvent($data)
    {
        $this->db->trans_start();
        $this->db->insert('exam_event', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function editExamEvent($data, $id)
    {
        $this->db->where('id', $id);
        $result = $this->db->update('exam_event', $data);
        return $result;
    }

    function updateToken($data, $id)
    {
        $this->db->where('id', $id);
        $result = $this->db->update('exam_student_tagging', $data);
        return $result;
    }

    function updateAttendence($data, $id)
    {
        $this->db->where('id', $id);
        $result = $this->db->update('exam_student_tagging', $data);
        return $result;
    }


    function getStateByCountryId($id_country)
    {
        $this->db->select('*');
        $this->db->from('state');
        $this->db->where('id_country', $id_country);
        $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        return $query->result();
    }

    function examCenterLocationListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('exam_center_location');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getExamSet() {
        $this->db->select('*');
        $this->db->from('examset');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }


    function examCenterLocationList()
    {
        $this->db->select('*');
        $this->db->from('exam_center_location');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function examCenterListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('exam_center');
        $this->db->order_by("name", "ASC");
        $this->db->where('status', $status);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }


    function examCenterList()
    {
        $this->db->select('*');
        $this->db->from('exam_center');
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getExamCenterNLocationByCenterId($id)
    {
        $this->db->select('*');
        $this->db->from('exam_center');
        $this->db->where('id', $id);
        $query = $this->db->get();
        $data['exam_center'] = $query->row()->name;



        $id_location = $query->row()->id_location;

        $this->db->select('*');
        $this->db->from('exam_center_location');
        $this->db->where('id', $id_location);
        $query = $this->db->get();
        $data['location'] = $query->row()->name;

        return $data;
    }
}