<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Grade_model extends CI_Model
{
    function gradeListByStatus($status)
    {
        $this->db->select('c.*');
        $this->db->from('grade as c');
        $this->db->where('c.status', $status);
        $this->db->order_by("c.name", "ASC");

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function gradeListSearch($data)
    {
        $this->db->select('c.*, cre.name as created_by, upd.name as updated_by,d.name as coursename,d.code as coursecode');
        $this->db->from('grade as c');
        $this->db->join('users as cre', 'c.created_by = cre.id','left');
        $this->db->join('users as upd', 'c.updated_by = upd.id','left');
        $this->db->join('course as d', 'd.id=c.id_course');
        // $this->db->join('staff as s', 'c.id_staff_coordinator = s.id');
        if (!empty($data['name']))
        {
            $likeCriteria = "(c.name  LIKE '%" . $data['name'] . "%' or c.name_optional_language  LIKE '%" . $data['name'] . "%' or c.code  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        // if ($data['id_staff'] != '')
        // {
        //     $this->db->where('c.id_staff_coordinator', $data['id_staff']);
        // }
        // if ($data['id_department'] != '')
        // {
        //     $this->db->where('c.id_department', $data['id_department']);
        // }
        $this->db->order_by("d.name", "ASC");        
        $this->db->group_by("c.id_course", "ASC");

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }


     function gradeListSearchByIdCourse($idcourse)
    {
        $this->db->select('c.*');
        $this->db->from('grade as c');
        $this->db->where('c.id_course', $idcourse);


        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }


     function getGrade($id)
    {
         $this->db->select('c.*');
        $this->db->from('grade as c');
        // $this->db->join('department as d', 'c.id_department = d.id','left');
        // $this->db->join('staff as s', 'c.id_staff_coordinator = s.id','left');
        $this->db->where('c.id', $id);
        $query = $this->db->get();
        $result = $query->row();
        // echo "<pre>";print_r($result);die;

        return $result;
    }

    function addNewGrade($data)
    {
        $this->db->trans_start();
        $this->db->insert('grade', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function editGrade($data, $id)
    {
        $this->db->where('id', $id);
        $result = $this->db->update('grade', $data);

        return $result;
    }

    function deleteGrade($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('grade', $data);

        return $this->db->affected_rows();
    }
}
