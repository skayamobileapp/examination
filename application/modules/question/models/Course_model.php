<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Course_model extends CI_Model
{
    function courseListByStatus($status)
    {
        $this->db->select('c.*');
        $this->db->from('course as c');
        $this->db->where('c.status', $status);
        $this->db->where("c.type='1'");
        
        $this->db->order_by("c.name", "ASC");

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function courseListSearch($data)
    {
        $this->db->select('c.*, cre.name as created_by, upd.name as updated_by');
        $this->db->from('course as c');
        $this->db->join('users as cre', 'c.created_by = cre.id','left');
        $this->db->join('users as upd', 'c.updated_by = upd.id','left');
        // $this->db->join('staff as s', 'c.id_staff_coordinator = s.id');
        if (!empty($data['name']))
        {
            $likeCriteria = "(c.name  LIKE '%" . $data['name'] . "%' or c.name_optional_language  LIKE '%" . $data['name'] . "%' or c.code  LIKE '%" . $data['name'] . "%')";
            $this->db->where($likeCriteria);
        }
        // if ($data['id_staff'] != '')
        // {
        //     $this->db->where('c.id_staff_coordinator', $data['id_staff']);
        // }
        // if ($data['id_department'] != '')
        // {
        //     $this->db->where('c.id_department', $data['id_department']);
        // }
        $this->db->order_by("c.name", "ASC");
        $this->db->where("c.type='1'");

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

     function getCourse($id)
    {
         $this->db->select('c.*');
        $this->db->from('course as c');
        // $this->db->join('department as d', 'c.id_department = d.id','left');
        // $this->db->join('staff as s', 'c.id_staff_coordinator = s.id','left');
        $this->db->where('c.id', $id);
        $query = $this->db->get();
        $result = $query->row();
        // echo "<pre>";print_r($result);die;

        return $result;
    }

    function addNewCourse($data)
    {
        $this->db->trans_start();
        $this->db->insert('course', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function addcourseClo($data){
         $this->db->trans_start();
        $this->db->insert('course_has_clo', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

      function deleteCourseHasClo($id)
    {
        $this->db->where('id', $id);
         $this->db->delete('course_has_clo');
    }

      function getCloDetails($id) {
         $this->db->select('c.*');
        $this->db->from('course_has_clo as c');
        $this->db->where('c.id', $id);
         $query = $this->db->get();
        $result = $query->row();
        return $result;
    }

    function getCourseClo($id){
         $this->db->select('c.*');
        $this->db->from('course_has_clo as c');
        $this->db->where('c.id_course', $id);
         $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function editCourse($data, $id)
    {
        $this->db->where('id', $id);
        $result = $this->db->update('course', $data);

        return $result;
    }

    function deleteCourse($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('course', $data);

        return $this->db->affected_rows();
    }
}
