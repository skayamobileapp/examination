<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Tos_model extends CI_Model
{
    function tosListSearch($data)
    {
        $this->db->select('c.*, cre.name as created_by, upd.name as updated_by');
        $this->db->from('tos as c');
        $this->db->join('users as cre', 'c.created_by = cre.id','left');
        $this->db->join('users as upd', 'c.updated_by = upd.id','left');
        if (!empty($data['name'])) {
            $likeCriteria = "(c.name  LIKE '%" . $data['name'] . "%'";
            $this->db->where($likeCriteria);
        }

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }


    function manualtosListSearch($data)
    {
        $this->db->select('c.*, cre.name as created_by, upd.name as updated_by');
        $this->db->from('manualtos as c');
        $this->db->join('users as cre', 'c.created_by = cre.id','left');
        $this->db->join('users as upd', 'c.updated_by = upd.id','left');
        if (!empty($data['name'])) {
            $likeCriteria = "(c.name  LIKE '%" . $data['name'] . "%'";
            $this->db->where($likeCriteria);
        }

         if ($data['type'] != '') {
            $this->db->where('c.type', $data['type']);
        }

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function getAllQuestionByManualTos($id){

        $this->db->select('c.*,question_pool.name as PoolName, cre.name as created_by, upd.name as updated_by,tpc.name as topicname, crs.name as coursename,d.name as difficultyname,bt.name as bloomtaxanomyname,mq.id as mqid');
        $this->db->from('question as c');

        $this->db->join('manualtos_has_question as mq', 'mq.id_question = c.id');


        $this->db->join('question_pool', 'question_pool.id = c.id_pool','left');

        $this->db->join('course as crs', 'crs.id = c.id_course','left');
        $this->db->join('topic as tpc', 'tpc.id = c.id_topic','left');

        $this->db->join('difficult_level as d', 'd.id = c.id_difficult_level','left');
        $this->db->join('bloom_taxonomy as bt', 'bt.id = c.id_bloom_taxonomy','left');



        $this->db->join('users as cre', 'c.created_by = cre.id','left');
        $this->db->join('users as upd', 'c.updated_by = upd.id','left');

        $this->db->where('mq.id_manualtos', $id);


        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function questionListSearch($data,$id)
    {
        $this->db->select('c.*,question_pool.name as PoolName, cre.name as created_by, upd.name as updated_by,tpc.name as topicname, crs.name as coursename,d.name as difficultyname,bt.name as bloomtaxanomyname');
        $this->db->from('question as c');
        $this->db->join('question_pool', 'question_pool.id = c.id_pool','left');

        $this->db->join('course as crs', 'crs.id = c.id_course','left');
        $this->db->join('topic as tpc', 'tpc.id = c.id_topic','left');

        $this->db->join('difficult_level as d', 'd.id = c.id_difficult_level','left');
        $this->db->join('bloom_taxonomy as bt', 'bt.id = c.id_bloom_taxonomy','left');


        $this->db->join('users as cre', 'c.created_by = cre.id','left');
        $this->db->join('users as upd', 'c.updated_by = upd.id','left');

        if ($data['id_pool'] != '') {
            $this->db->where('c.id_pool', $data['id_pool']);
        }
         if ($data['question_type'] != '') {
            $this->db->where('c.question_type', $data['question_type']);
        }
        if ($data['id_course'] != '') {
            $this->db->where('c.id_course', $data['id_course']);
        }
        if ($data['id_topic'] != '') {
            $this->db->where('c.id_topic', $data['id_topic']);
        }
        if ($data['id_course_learning_objective'] != '') {
            $this->db->where('c.id_course_learning_objective', $data['id_course_learning_objective']);
        }
        if ($data['id_bloom_taxonomy'] != '') {
            $this->db->where('c.id_bloom_taxonomy', $data['id_bloom_taxonomy']);
        }
        if ($data['id_difficult_level'] != '') {
            $this->db->where('c.id_difficult_level', $data['id_difficult_level']);
        }
            $this->db->where("c.parent_id='0' and c.id not in (select id_question from manualtos_has_question where id_manualtos='$id')");

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }



    function addNewTos($main_data)
    {
        $this->db->trans_start();
        $this->db->insert('tos', $main_data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function addNewManualTos($main_data)
    {
        $this->db->trans_start();
        $this->db->insert('manualtos', $main_data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function addmanualTosQuestion($main_data){
        $this->db->trans_start();
        $this->db->insert('manualtos_has_question', $main_data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function addNewTosDetails($main_data)
    {
        $this->db->trans_start();
        $this->db->insert('tos_details', $main_data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function getTos($id)
    {
        $this->db->select('c.*');
        $this->db->from('tos as c');
        $this->db->where('c.id', $id);
        $query = $this->db->get();
        $result = $query->row();

        return $result;
    }


    function getManualTosById($id)
    {
        $this->db->select('c.*');
        $this->db->from('manualtos as c');
        $this->db->where('c.id', $id);
        $query = $this->db->get();
        $result = $query->row();
        return $result;
    }

    function getTosDetails($id)
    {
        $this->db->select('c.*,course.name as courseName,topic.name as topicName,bloom_taxonomy.name as taxonomyName,difficult_level.name as levelName');
        $this->db->from('tos_details as c');
        $this->db->join('course', 'c.id_course = course.id');
        $this->db->join('topic', 'c.id_topic = topic.id');
        $this->db->join('bloom_taxonomy', 'c.id_bloom_taxonomy = bloom_taxonomy.id');
        $this->db->join('difficult_level', 'c.id_difficult_level = difficult_level.id');
        $this->db->where('c.id_tos', $id);
        $query = $this->db->get();
        $result = $query->result();

        return $result;
    }

   
   function updateCountTos($main_data, $id)
    {

         $this->db->where('id', $id);
        $result = $this->db->update('tos', $main_data);
        return $result;
      }

    function updatetosmanual($data,$id) {
                 $this->db->where('id', $id);
        $result = $this->db->update('manualtos', $data);
        return $result;

    }

    function editTos($main_data, $detail_data)
    {
        $this->db->trans_start();
        $this->db->where('id', $main_data['id']);
        $result = $this->db->update('tos', $main_data);

        foreach($detail_data as $row){
            $this->db->where('id_details', $row['id_details']);
            $result = $this->db->update('tos_details', $row);
        }
        $this->db->trans_complete();
        return $result;
    }

    function deleteTos($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('tos', $data);
        return $this->db->affected_rows();
    }


    function deletetemptos($id){
        $this->db->where('questionid', $id);
        $this->db->delete('tos_temp_entered');
        return TRUE;
    }


function deletetemptosBySession($id){
        $this->db->where('session_id', $id);
        $this->db->delete('tos_temp_entered');
        return TRUE;
    }


     function getCount($session_id,$question_type)
    {
        $this->db->select('c.*');
        $this->db->from('tos_temp_entered as c');
        $this->db->where('c.session_id', $session_id);
        $this->db->where('c.question_type', $question_type);
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }




    function deletemanualtosid($id) {
        
         $this->db->where('id', $id);
        $this->db->delete('manualtos_has_question');
        return TRUE;
    }


      function delteTosDetailsById($id){
        $this->db->where('id_tos', $id);
        $this->db->delete('tos_details');
        return TRUE;
    }

   

    function getTosQuestions($pools)
    {


         $this->db->select('c.*,course.name as courseName,topic.name as topicName,bloom_taxonomy.name as taxonomyName,difficult_level.name as levelName');
        $this->db->from('tos_details as c');
        $this->db->join('course', 'c.id_course = course.id');
        $this->db->join('topic', 'c.id_topic = topic.id');
        $this->db->join('bloom_taxonomy', 'c.id_bloom_taxonomy = bloom_taxonomy.id');
        $this->db->join('difficult_level', 'c.id_difficult_level = difficult_level.id');
        $this->db->where('c.id_tos', $id);
        $query = $this->db->get();
        $result = $query->result();

    }


     function getQuestionCountMCQ($idtaxanomy,$idcourse,$idtopic,$difficult_level,$idpools,$questiontype) {
        $this->db->select('c.id');
        $this->db->from('question as c');
        $this->db->where('c.id_course', $idcourse);
        $this->db->where('c.id_topic', $idtopic);
        $this->db->where('c.id_bloom_taxonomy', $idtaxanomy);
        $this->db->where('c.id_difficult_level', $difficult_level);
        $this->db->where("c.id_pool in ($idpools)");
        $this->db->where("c.parent_id='0'");
        $this->db->where("c.question_type=",$questiontype);

        $query = $this->db->get();

        $result = $query->result();
        return count($result);

    }

    function getQuestionCountAll($idtaxanomy,$idcourse,$idtopic,$idpools,$questiontype) {
        $this->db->select('c.id');
        $this->db->from('question as c');
        $this->db->where('c.id_course', $idcourse);
        $this->db->where('c.id_topic', $idtopic);
        $this->db->where('c.id_bloom_taxonomy', $idtaxanomy);
        $this->db->where("c.id_pool in ($idpools)");
        $this->db->where("c.parent_id='0'");
        $this->db->where("c.question_type=",$questiontype);

        $query = $this->db->get();

        $result = $query->result();
        return count($result);

    }


    function addtempquestion($data){
        $this->db->trans_start();
        $this->db->insert('tos_temp_entered', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;

    }


    function getQuestionCount($idtaxanomy,$idcourse,$idtopic,$difficult_level,$idpools) {
        $this->db->select('c.id');
        $this->db->from('question as c');
        $this->db->where('c.id_course', $idcourse);
        $this->db->where('c.id_topic', $idtopic);
        $this->db->where('c.id_bloom_taxonomy', $idtaxanomy);
        $this->db->where('c.id_difficult_level', $difficult_level);
        $this->db->where("c.id_pool in ($idpools)");

        $query = $this->db->get();

        $result = $query->result();
        return count($result);

    }


     function getTaxanomylist($status)
    {
        $this->db->select('c.*');
        $this->db->from('bloom_taxonomy as c');
        $this->db->where('c.status', $status);
        $this->db->order_by("c.code", "ASC");

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function getAllcourse($id){
        $this->db->select('t.name as topicname,c.name as coursename,q.id_course,q.id_topic,t.id');
        $this->db->from('course as c');
        $this->db->join('topic as t ', 't.id_course = c.id');
        $this->db->join('question as q', 'q.id_course = c.id and q.id_topic=t.id');
        $this->db->where("q.id_pool in ($id)");
        $this->db->group_by("t.id");

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

     function getAllcourseDetails($id){
        $this->db->select('t.name as topicname,c.name as coursename,q.id_course,q.id_topic,t.id');
        $this->db->from('course as c');
        $this->db->join('topic as t ', 't.id_course = c.id');
        $this->db->join('question as q', 'q.id_course = c.id and q.id_topic=t.id');
        $this->db->where("q.id_pool in ($id)");
        $this->db->group_by("t.id");

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }


    function getTosQuestionCount($idtos) {
         $this->db->select('sum(c.questions_selected) as totalquestion');
        $this->db->from('tos_details as c');
        $this->db->where('c.id_tos', $idtos);

        $query = $this->db->get();
        $result = $query->row();
return $result;
    }


    function getQuestionCountFromData($idtaxanomy,$id_course,$id_topic,$difficult_level,$tosid,$questiontype){
         $this->db->select('c.questions_selected');
        $this->db->from('tos_details as c');
        $this->db->where('c.id_course', $id_course);
        $this->db->where('c.id_topic', $id_topic);
        $this->db->where('c.id_bloom_taxonomy', $idtaxanomy);
        $this->db->where('c.id_difficult_level', $difficult_level);
        $this->db->where('c.id_tos', $tosid);
        $this->db->where('c.question_type', $questiontype);

        $query = $this->db->get();
        $result = $query->row();
return $result;

    }

     function getQuestionCountFromDataAll($idtaxanomy,$id_course,$id_topic,$tosid,$questiontype){
         $this->db->select('c.questions_selected');
        $this->db->from('tos_details as c');
        $this->db->where('c.id_course', $id_course);
        $this->db->where('c.id_topic', $id_topic);
        $this->db->where('c.id_bloom_taxonomy', $idtaxanomy);
        $this->db->where('c.id_tos', $tosid);
        $this->db->where('c.question_type', $questiontype);

        $query = $this->db->get();
        $result = $query->row();
return $result;

    }
}
