<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Question_model extends CI_Model
{
    function courseListByStatus($status)
    {
        $this->db->select('c.*');
        $this->db->from('course as c');
        $this->db->where('c.status', $status);
        $this->db->order_by("c.name", "ASC");

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function getLastQuestion() {
        $this->db->select('c.*');
        $this->db->from('question as c');
        $this->db->order_by("c.id", "DESC");
        $query = $this->db->get();
        $result = $query->row();
        return $result;

    }

    function getDifficultyLevelByName($name) {
        $this->db->select('c.*');
        $this->db->from('difficult_level as c');
        $this->db->where("c.name like '%$name%'");
        $query = $this->db->get();
        $result = $query->row();
        return $result;
    }

    function getTopicLevelByName($name){
        $this->db->select('c.*');
        $this->db->from('topic as c');
        $this->db->where("c.name like '%$name%'");
        $query = $this->db->get();
        $result = $query->row();
        return $result;
    }

    function getCourseId($code) {
        $this->db->select('c.*');
        $this->db->from('course as c');
        $this->db->where('c.code', $code);
        $query = $this->db->get();
        $result = $query->row();
        return $result;
    }

   function getTopicId($code) {
        $this->db->select('c.*');
        $this->db->from('topic as c');
        $this->db->where('c.code', $code);
        $query = $this->db->get();
        $result = $query->row();
        return $result;
    }

    function getTaxanomyId($code){
        $this->db->select('c.*');
        $this->db->from('bloom_taxonomy as c');
        $this->db->where('c.code', $code);
        $query = $this->db->get();
        $result = $query->row();
        return $result;
    }
    function topicListByCourse($course)
    {
        $this->db->select('c.*');
        $this->db->from('topic as c');
        $this->db->where('c.id_course', $course);
        $this->db->order_by("c.name", "ASC");

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }
    function poolListByStatus($status)
    {
        $this->db->select('c.*');
        $this->db->from('question_pool as c');
        $this->db->where('c.status', $status);
        $this->db->order_by("c.name", "ASC");

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function topicListByStatus($status)
    {
        $this->db->select('c.*');
        $this->db->from('topic as c');
        $this->db->where('c.status', $status);
        $this->db->order_by("c.name", "ASC");

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function courseLearningObjectListByStatus($status)
    {
        $this->db->select('c.*');
        $this->db->from('course_learning_objective as c');
        $this->db->where('c.status', $status);
        $this->db->order_by("c.name", "ASC");

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function bloomTaxonomyListByStatus($status)
    {
        $this->db->select('c.*');
        $this->db->from('bloom_taxonomy as c');
        $this->db->where('c.status', $status);
        $this->db->order_by("c.code", "ASC");

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function difficultLevelListByStatus($status)
    {
        $this->db->select('c.*');
        $this->db->from('difficult_level as c');
        $this->db->where('c.status', $status);
        $this->db->order_by("c.name", "ASC");

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function answerStatusListByStatus($status)
    {
        $this->db->select('c.*');
        $this->db->from('answer_status as c');
        $this->db->where('c.status', $status);
        $this->db->order_by("c.name", "ASC");

        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function getChildQuestionByParent($id) {
        $this->db->select('c.*');
        $this->db->from('question as c');
        $this->db->where('c.parent_id', $id);
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }
    function questionListSearchLog($id_question)
    {
        $this->db->select('c.*,question_pool.name as PoolName, cre.name as created_by, upd.name as updated_by');
        $this->db->from('question_log as c');
        $this->db->join('question_pool', 'question_pool.id = c.id_pool');
        $this->db->join('users as cre', 'c.created_by = cre.id','left');
        $this->db->join('users as upd', 'c.updated_by = upd.id','left');

     
            $this->db->where('c.id_question', $id_question);
        
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function questionListSearch($data)
    {
        $this->db->select('c.*,question_pool.name as PoolName, cre.name as created_by, upd.name as updated_by');
        $this->db->from('question as c');
        $this->db->join('question_pool', 'question_pool.id = c.id_pool');
        $this->db->join('users as cre', 'c.created_by = cre.id','left');
        $this->db->join('users as upd', 'c.updated_by = upd.id','left');

        if ($data['id_pool'] != '') {
            $this->db->where('c.id_pool', $data['id_pool']);
        }
        if ($data['id_course'] != '') {
            $this->db->where('c.id_course', $data['id_course']);
        }
        if ($data['id_topic'] != '') {
            $this->db->where('c.id_topic', $data['id_topic']);
        }
        if ($data['id_course_learning_objective'] != '') {
            $this->db->where('c.id_course_learning_objective', $data['id_course_learning_objective']);
        }
        if ($data['id_bloom_taxonomy'] != '') {
            $this->db->where('c.id_bloom_taxonomy', $data['id_bloom_taxonomy']);
        }
        if ($data['id_difficult_level'] != '') {
            $this->db->where('c.id_difficult_level', $data['id_difficult_level']);
        }
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function getQuestion($id)
    {
        $this->db->select('c.*');
        $this->db->from('question as c');
        $this->db->where('c.id', $id);
        $query = $this->db->get();
        $result = $query->row();

        return $result;
    }

    function getQuestionHasOptionById($id)
    {
        $this->db->select('c.*, anst.name as answer_status');
        $this->db->from('question_has_option as c');
        $this->db->join('answer_status as anst', 'c.is_correct_answer = anst.id','left');
        $this->db->where('c.id', $id);
        $query = $this->db->get();
        $result = $query->row();

        return $result;
    }

    function addNewQuestion($data)
    {
        $this->db->trans_start();
        $this->db->insert('question', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function addQuestionLog($data)
    {
        $this->db->trans_start();
        $this->db->insert('question_log', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function editQuestion($data, $id)
    {
        $this->db->where('id', $id);
        $result = $this->db->update('question', $data);

        return $result;
    }

    function deleteQuestion($data, $id)
    {
        $this->db->where('id', $id);
        $this->db->update('question', $data);
        return $this->db->affected_rows();
    }

    function getQuestionHasOption($id_question)
    {
        $this->db->select('c.*, anst.name as answer_status');
        $this->db->from('question_has_option as c');
        $this->db->join('answer_status as anst', 'c.is_correct_answer = anst.id','left');
        $this->db->where('c.id_question', $id_question);
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }


    function getFrequency($id) {
        $this->db->select('count(c.id) as totalCount');
        $this->db->from('student_question_set as c');
        $this->db->where('c.id_question', $id);
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function addQuestionHasOption($data)
    {
        $this->db->trans_start();
        $this->db->insert('question_has_option', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function deleteOption($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('question_has_option');
        return TRUE;
    }

    function editQuestionHasOption($data,$id)
    {
        $this->db->where('id', $id);
        $result = $this->db->update('question_has_option', $data);

        return $result;
    }

    function tempsaveDataQuestions($data){
        $this->db->trans_start();
        $this->db->insert('temp_exam_question', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();

        return $insert_id;
    }

    function delelteQuestion($id){
        $this->db->where('id', $id);
        $this->db->delete('temp_exam_question');
        return TRUE;
    }

    function gettempsaveQuestions($idsession){
        $this->db->select('c.*');
        $this->db->from('temp_exam_question as c');
        $this->db->where('c.session_id', $idsession);
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

}
