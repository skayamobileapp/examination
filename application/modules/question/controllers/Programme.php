<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Programme extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('programme_model');
        $this->isLoggedIn();
        error_reporting(0);
    }

    function list()
    {
        if ($this->checkAccess('programme.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));

            $data['searchParam'] = $formData;
            $data['courseList'] = $this->programme_model->courseListSearch($formData);

            $this->global['pageTitle'] = 'Examination Management System : Course List';
            $this->global['pageCode'] = 'programme.list';
            $this->loadViews("programme/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('programme.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;

            if($this->input->post())
            {
            
                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'status' => $status,
                    'type'=>2,
                    'created_by' => $user_id
                );
                
                $result = $this->programme_model->addNewCourse($data);
                if ($result > 0) {
                    $this->session->set_flashdata('success', 'New Course created successfully');
                } else {
                    $this->session->set_flashdata('error', 'Course creation failed');
                }


                redirect('/question/programme/list');
            }
           
            $this->global['pageCode'] = 'programme.add';
            $this->global['pageTitle'] = 'Examination Management System : Add Course';
            $this->loadViews("programme/add", $this->global, NULL, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('programme.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/question/programme/list');
            }

            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;

            if($this->input->post())
            {

                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'status' => $status,
                    'updated_dt_tm'=>date('Y-m-d H:i:s'),
                    'updated_by' => $user_id
                );
                
                $result = $this->programme_model->editCourse($data,$id);
                if ($result) {
                    $this->session->set_flashdata('success', 'Course edited successfully');
                } else {
                    $this->session->set_flashdata('error', 'Course edit failed');
                }
                redirect('/question/programme/list');
            }

            $data['courseid'] = $id;
            $data['courseDetails'] = $this->programme_model->getCourse($id);
            $this->global['pageCode'] = 'programme.list';
            $this->global['pageTitle'] = 'Examination Management System : Edit Course';
            $this->loadViews("programme/edit", $this->global, $data, NULL);
        }
    }

    function editclo($id = NULL,$cloid=NULL)
    {
        if ($this->checkAccess('programme.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/question/programme/list');
            }

            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;

            if($this->input->post())
            {



                 $name = $this->security->xss_clean($this->input->post('name'));
                 $code = $this->security->xss_clean($this->input->post('code'));
                 $status = $this->security->xss_clean($this->input->post('status'));
               
                 $data = array(
                    'id_programme' => $id,
                    'name' => $name,
                    'code' => $code,
                    'status' => $status
                );
                 if($cloid) {
                $result = $this->programme_model->editCourseClo($data,$cloid);

                 } else {
                
                $result = $this->programme_model->addcourseClo($data);
            }
                if ($result) {
                    $this->session->set_flashdata('success', 'Course edited successfully');
                } else {
                    $this->session->set_flashdata('error', 'Course edit failed');
                }
                redirect('/question/programme/editclo/'.$id);
            }


            $courseDetails = $this->programme_model->getCourseClo($id);
            if($cloid) {
                $clodetails = $this->programme_model->getCloDetails($cloid);
            $data['clodetails'] = $clodetails;

            }


            $data['courseid'] = $id;
            $data['courseDetails'] = $courseDetails;
            $this->global['pageCode'] = 'programme.list';
            $this->global['pageTitle'] = 'Examination Management System : Edit Course';
            $this->loadViews("programme/plo", $this->global, $data, NULL);
        }
    }

    function addclo(){
        if($this->input->post())
            {

                $clo = $this->security->xss_clean($this->input->post('clo'));
                $courseid = $this->security->xss_clean($this->input->post('courseid'));

                 $data = array(
                    'id_course' => $courseid,
                    'clo_name' => $clo
                );
                
                $result = $this->programme_model->addcourseClo($data);
            }

             $courseDetails = $this->programme_model->getCourseClo($id);


    }

    function deleteclo(){

        $id = $_POST['id'];

        $this->programme_model->deleteCourseHasClo($id);
        return 1;


    }
}
