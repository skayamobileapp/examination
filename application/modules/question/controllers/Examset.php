<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


class Examset extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('examset_model');
        $this->load->model('tos_model');
        $this->isLoggedIn();
        error_reporting(0);
    }

    function list()
    {
        if ($this->checkAccess('examset.list') == 1) {
            $this->loadAccessRestricted();
        } else {
             $formData['id_exam_sitting'] = $this->security->xss_clean($this->input->post('id_exam_sitting')); 
            $formData['exam_event_name'] = $this->security->xss_clean($this->input->post('exam_event_name')); 
            $data['searchParam'] = $formData;
            $data['ExamsetList'] = $this->examset_model->examsetListSearch($formData);
            $data['examSitting'] = $this->examset_model->getExamSitting($formData);

            $this->global['pageTitle'] = 'Examination Management System : Examset List';
            $this->global['pageCode'] = 'examset.list';
            $this->loadViews("examset/list", $this->global, $data, NULL);
        }
    }

    function getmoduledropdown() {
        $type = $_POST['id'];
        if($_POST['selectedid']) {
            $selectedid = $_POST['selectedid'];
        } else {
            $selectedid = 0;
        }

         $results = $this->examset_model->getCourseListByType($type);

            $table="   
                <script type='text/javascript'>
                     $('select').select2();
                 </script>
         ";

            $table.="
            <select name='id_course' id='id_course' class='form-control' required>
                <option value=''>Select</option>
                ";

            for($i=0;$i<count($results);$i++)
            {

            $id = $results[$i]->id;
            $name = $results[$i]->name;
            $checked="";
            if($id==$selectedid) {
                $checked ="selected=selected";
            }
            $table.="<option value=".$id." $checked>".$name.
                    "</option>";

            }
            $table.="

            </select>";

            echo $table;
            exit;


    }

    function add()
    {
        if ($this->checkAccess('examset.add') == 1) {
            $this->loadAccessRestricted();
        } else {

            $user_id = $this->session->userId;

            if ($this->input->post()) {
$name = $this->security->xss_clean($this->input->post('name'));
                $id_tos = $this->security->xss_clean($this->input->post('id_tos'));
                $instructions = $this->security->xss_clean($this->input->post('instructions'));
                $duration = $this->security->xss_clean($this->input->post('duration'));
                $pass_grade = $this->security->xss_clean($this->input->post('pass_grade'));
                $attempts = $this->security->xss_clean($this->input->post('attempts'));
                $attempt_duration = $this->security->xss_clean($this->input->post('attempt_duration'));
                $grading_method = $this->security->xss_clean($this->input->post('grading_method'));
                $layout = $this->security->xss_clean($this->input->post('layout'));
                $behaviour = $this->security->xss_clean($this->input->post('behaviour'));
                $status = $this->security->xss_clean($this->input->post('status'));

   $tos_status = $this->security->xss_clean($this->input->post('tos_status'));
                $id_manual_tos = $this->security->xss_clean($this->input->post('id_manual_tos'));
                $id_manual_tos_set = $this->security->xss_clean($this->input->post('id_manual_tos_set'));

                $repetative_behaviour = $this->security->xss_clean($this->input->post('repetative_behaviour'));
                $id_course = $this->security->xss_clean($this->input->post('id_course'));
                $type_of_exam = $this->security->xss_clean($this->input->post('type_of_exam'));
                $id_mark_display = $this->security->xss_clean($this->input->post('id_mark_display'));
                $id_communication = $this->security->xss_clean($this->input->post('id_communication'));



                $data = array(
                    'name' => $name,
                    'id_tos' => $id_tos,
                    'instructions' => $instructions,
                    'duration' => $duration,
                    'pass_grade' => $pass_grade,
                    'attempts' => $attempts,
                    'attempt_duration' => $attempt_duration,
                    'grading_method' => $grading_method,
                    'layout' => $layout,
                    'behaviour' => $behaviour,
                    'status' => $status,
                    'created_by' => $user_id,
                    'tos_status' => $tos_status,
                    'id_manual_tos' => $id_manual_tos,
                    'id_manual_tos_set' => $id_manual_tos_set,
                    'repetative_behaviour' => $repetative_behaviour,
                    'id_course' => $id_course,
                    'type_of_exam' => $type_of_exam,
                    'id_mark_display'=>$id_mark_display,
                    'id_communication'=>$id_communication
                );
                $result = $this->examset_model->addNewExamset($data);
                if ($result > 0) {
                    $this->session->set_flashdata('success', 'New Examset added successfully');
                } else {
                    $this->session->set_flashdata('error', 'Examset add failed');
                }
                redirect('/question/examset/list');
            }

            $data['toslist'] = $this->tos_model->tosListSearch(array());
                        $data['manualExamSet'] = $this->examset_model->getManualTos('1');
                        $data['communication'] = $this->examset_model->getCommunication('1');

            $this->global['pageCode'] = 'examset.add';
            $this->global['pageTitle'] = 'Examination Management System : Add Examset';
            $this->loadViews("examset/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('examset.edit') == 1) {
            $this->loadAccessRestricted();
        } else {
            if ($id == null) {
                redirect('/question/examset/list');
            }
            if ($this->input->post()) {
                $id_session = $this->session->my_session_id;
                $user_id = $this->session->userId;


                $name = $this->security->xss_clean($this->input->post('name'));
                $id_tos = $this->security->xss_clean($this->input->post('id_tos'));
                $instructions = $this->security->xss_clean($this->input->post('instructions'));
                $duration = $this->security->xss_clean($this->input->post('duration'));
                $pass_grade = $this->security->xss_clean($this->input->post('pass_grade'));
                $attempts = $this->security->xss_clean($this->input->post('attempts'));
                $attempt_duration = $this->security->xss_clean($this->input->post('attempt_duration'));
                $grading_method = $this->security->xss_clean($this->input->post('grading_method'));
                $layout = $this->security->xss_clean($this->input->post('layout'));
                $behaviour = $this->security->xss_clean($this->input->post('behaviour'));
                $status = $this->security->xss_clean($this->input->post('status'));


   $tos_status = $this->security->xss_clean($this->input->post('tos_status'));
                $id_manual_tos = $this->security->xss_clean($this->input->post('id_manual_tos'));
                $id_manual_tos_set = $this->security->xss_clean($this->input->post('id_manual_tos_set'));

                

                $repetative_behaviour = $this->security->xss_clean($this->input->post('repetative_behaviour'));
                $id_course = $this->security->xss_clean($this->input->post('id_course'));
                $type_of_exam = $this->security->xss_clean($this->input->post('type_of_exam'));
                




               $id_mark_display = $this->security->xss_clean($this->input->post('id_mark_display'));
$id_communication = $this->security->xss_clean($this->input->post('id_communication'));



              
                $data = array(
                    'name' => $name,
                    'id_tos' => $id_tos,
                    'instructions' => $instructions,
                    'duration' => $duration,
                    'pass_grade' => $pass_grade,
                    'attempts' => $attempts,
                    'attempt_duration' => $attempt_duration,
                    'grading_method' => $grading_method,
                    'layout' => $layout,
                    'behaviour' => $behaviour,
                    'status' => $status,
                    'created_by' => $user_id,
                    'tos_status' => $tos_status,
                    'id_manual_tos' => $id_manual_tos,
                    'id_manual_tos_set' => $id_manual_tos_set,                    
                    'repetative_behaviour' => $repetative_behaviour,
                    'id_course' => $id_course,
                    'type_of_exam' => $type_of_exam,
                    'id_mark_display'=>$id_mark_display,
                    'id_communication'=>$id_communication
                );

                // print_r($data);exit;
                $result = $this->examset_model->editExamset($data, $id);
                if ($result) {
                    $this->session->set_flashdata('success', 'Examset edited successfully');
                } else {
                    $this->session->set_flashdata('error', 'Examset edit failed');
                }

               if($status=='1') {
                    $this->examset_model->updateOtherStatustoInactive($id_course,$id);
                }
                redirect('/question/examset/list');
            }
            $data['toslist'] = $this->tos_model->tosListSearch(array());
            $data['examset'] = $this->examset_model->getExamset($id);
                        $data['manualExamSet'] = $this->examset_model->getManualTos('1');
                        $data['communication'] = $this->examset_model->getCommunication('1');

            $this->global['pageTitle'] = 'Examination Management System : Edit Examset';
            $this->global['pageCode'] = 'examset.edit';
            $this->loadViews("examset/edit", $this->global, $data, NULL);
        }
    }
}
