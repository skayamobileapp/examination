<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Grade extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('grade_model');
        $this->load->model('question_model');
        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('grade.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));

            $data['searchParam'] = $formData;
            $data['gradeList'] = $this->grade_model->gradeListSearch($formData);

            $this->global['pageTitle'] = 'Examination Management System : Grade List';
            $this->global['pageCode'] = 'grade.list';
            $this->loadViews("grade/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('grade.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;

            if($this->input->post())
            {
            
                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $min_percentage = $this->security->xss_clean($this->input->post('min_percentage'));
                $max_percentage = $this->security->xss_clean($this->input->post('max_percentage'));
                $status = $this->security->xss_clean($this->input->post('status'));
                            $id_course = $this->security->xss_clean($this->input->post('id_course'));

                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'min_percentage' => $min_percentage,
                    'max_percentage' => $max_percentage,
                    'status' => 1,
                    'id_course' => $id_course,
                    'created_by' => $user_id,
                    'created_dt_tm' =>date('Y-m-d H:i:s')
                );
                
                $result = $this->grade_model->addNewGrade($data);
                if ($result > 0) {
                    $this->session->set_flashdata('success', 'New Grade created successfully');
                } else {
                    $this->session->set_flashdata('error', 'Grade creation failed');
                }
                redirect('/question/grade/list');
            }
           
                       $data['courseList'] = $this->question_model->courseListByStatus('1');

            $this->global['pageCode'] = 'grade.add';
            $this->global['pageTitle'] = 'Examination Management System : Add Grade';
            $this->loadViews("grade/add", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('grade.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
             $formData['name'] = $this->security->xss_clean($this->input->post('name'));

            $data['gradeList'] = $this->grade_model->gradeListSearchByIdCourse($id);

           
            $this->global['pageTitle'] = 'Examination Management System : Edit Grade';
            $this->global['pageCode'] = 'grade.list';

            $this->loadViews("grade/edit", $this->global, $data, NULL);
        }
    }
}
