<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Course extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('course_model');
        $this->isLoggedIn();
        error_reporting(0);
    }

    function list()
    {
        if ($this->checkAccess('course.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));

            $data['searchParam'] = $formData;
            $data['courseList'] = $this->course_model->courseListSearch($formData);

            $this->global['pageTitle'] = 'Examination Management System : Course List';
            $this->global['pageCode'] = 'course.list';
            $this->loadViews("course/list", $this->global, $data, NULL);
        }
    }
    
    function add()
    {
        if ($this->checkAccess('course.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {

            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;

            if($this->input->post())
            {
            
                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'status' => $status,
                    'created_by' => $user_id
                );
                
                $result = $this->course_model->addNewCourse($data);
                if ($result > 0) {
                    $this->session->set_flashdata('success', 'New Course created successfully');
                } else {
                    $this->session->set_flashdata('error', 'Course creation failed');
                }
                redirect('/question/course/list');
            }
           
            $this->global['pageCode'] = 'course.add';
            $this->global['pageTitle'] = 'Examination Management System : Add Course';
            $this->loadViews("course/add", $this->global, NULL, NULL);
        }
    }

    
    function deleteclo(){

        $id = $_POST['id'];

        $this->course_model->deleteCourseHasClo($id);
        return 1;


    }


    function editclo($id = NULL,$cloid=NULL)
    {
        if ($this->checkAccess('course.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/question/course/list');
            }

            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;

            if($this->input->post())
            {



                 $name = $this->security->xss_clean($this->input->post('name'));
                 $code = $this->security->xss_clean($this->input->post('code'));
                 $status = $this->security->xss_clean($this->input->post('status'));
               
                 $data = array(
                    'id_course' => $id,
                    'name' => $name,
                    'code' => $code,
                    'status' => $status
                );
                 if($cloid) {
                $result = $this->course_model->editCourseClo($data,$cloid);

                 } else {
                
                $result = $this->course_model->addcourseClo($data);
            }
                if ($result) {
                    $this->session->set_flashdata('success', 'Course edited successfully');
                } else {
                    $this->session->set_flashdata('error', 'Course edit failed');
                }
                redirect('/question/course/editclo/'.$id);
            }


            $courseDetails = $this->course_model->getCourseClo($id);
            if($cloid) {
                $clodetails = $this->course_model->getCloDetails($cloid);
            $data['clodetails'] = $clodetails;

            }


            $data['courseid'] = $id;
            $data['courseDetails'] = $courseDetails;
            $this->global['pageCode'] = 'course.edit';
            $this->global['pageTitle'] = 'Examination Management System : Edit Course';
            $this->loadViews("course/clo", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('course.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/question/course/list');
            }

            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;

            if($this->input->post())
            {

                $name = $this->security->xss_clean($this->input->post('name'));
                $code = $this->security->xss_clean($this->input->post('code'));
                $status = $this->security->xss_clean($this->input->post('status'));
            
                $data = array(
                    'name' => $name,
                    'code' => $code,
                    'status' => $status,
                    'updated_dt_tm'=>date('Y-m-d H:i:s'),                    
                    'updated_by' => $user_id
                );
                
                $result = $this->course_model->editCourse($data,$id);
                if ($result) {
                    $this->session->set_flashdata('success', 'Course edited successfully');
                } else {
                    $this->session->set_flashdata('error', 'Course edit failed');
                }
                redirect('/question/course/list');
            }

            $data['courseid'] = $id;
            $data['courseDetails'] = $this->course_model->getCourse($id);
            $this->global['pageCode'] = 'course.list';
            $this->global['pageTitle'] = 'Examination Management System : Edit Course';
            $this->loadViews("course/edit", $this->global, $data, NULL);
        }
    }

    function addclo(){
        if($this->input->post())
            {

                $clo = $this->security->xss_clean($this->input->post('clo'));
                $courseid = $this->security->xss_clean($this->input->post('courseid'));

                 $data = array(
                    'id_course' => $courseid,
                    'clo_name' => $clo
                );
                
                $result = $this->course_model->addcourseClo($data);
            }

            $this->viewclotable($courseid);


    }

    function viewclotable($id){


            $courseDetails = $this->course_model->getCourseClo($id);
            

          $table="<table class='table' width='100%'>
                   <tr> <th>Sl No</th><th>Course Learning Outcome </th></tr>";    
          $j = 1;
           for($i=0;$i<count($courseDetails);$i++) {
            $id = $courseDetails[$i]->id;
            $label = $courseDetails[$i]->clo_name;
              $table.="<tr><td>$j</td><td>$label</td></tr>";
              $j++;
           }
           $table.="</table>";

           echo $table;

    }
}
