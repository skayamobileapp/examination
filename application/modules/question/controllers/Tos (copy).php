<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


class Tos extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('tos_model');
        $this->load->model('question_model');
        $this->isLoggedIn();
                error_reporting(0);

    }

    function list()
    {
        if ($this->checkAccess('tos.list') == 1) {
            $this->loadAccessRestricted();
        } else {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));

            $data['searchParam'] = $formData;
            $data['tosList'] = $this->tos_model->tosListSearch($formData);
            $this->global['pageTitle'] = 'Examination Management System : TOS List';
            $this->global['pageCode'] = 'tos.list';
            $this->loadViews("tos/list", $this->global, $data, NULL);
        }
    }

    function add()
    {
        if ($this->checkAccess('tos.add') == 1) {
            $this->loadAccessRestricted();
        } else {

            $user_id = $this->session->userId;

            if ($this->input->post()) {
            
                 $name = $this->security->xss_clean($this->input->post('name'));
                 $mcq_qestion = $this->security->xss_clean($this->input->post('enter_mcq_questions1'));
                 $essay_question = $this->security->xss_clean($this->input->post('enter_mcq_questions2'));
                 $true_false_question = $this->security->xss_clean($this->input->post('enter_mcq_questions3'));

                 $mcq_marks = $this->security->xss_clean($this->input->post('marks_mcq_questions1'));
                 $essay_marks = $this->security->xss_clean($this->input->post('marks_mcq_questions2'));
                 $true_false_marks = $this->security->xss_clean($this->input->post('marks_mcq_questions3'));


                 $total_marks = $this->security->xss_clean($this->input->post('total_marks'));
                 $question_order = $this->security->xss_clean($this->input->post('question_order'));
                 $name = $this->security->xss_clean($this->input->post('name'));
                $question_count = $this->security->xss_clean($this->input->post('question_count'));
                $id_pool = implode(',', $this->security->xss_clean($this->input->post('id_pool')));
                $status = $this->security->xss_clean($this->input->post('status'));
                $main_data = array(
                    'name' => $name,
                    'question_count' => $question_count,
                    'id_pool' => $id_pool,
                    'status' => $status,
                    'created_by' => $user_id,
                    'mcq_qestion' => $mcq_qestion,
                    'essay_question' => $essay_question,
                    'true_false_question' => $true_false_question,
                    'mcq_marks' => $mcq_marks,
                    'essay_marks' => $essay_marks,
                    'true_false_marks' => $true_false_marks,
                    'total_marks' => $total_marks,
                    'question_order' => $question_order
                );

                $tosid = $this->tos_model->addNewTos($main_data);

                for($i=1;$i<=count($_POST['easy']);$i++) {



                    for($j=0;$j<count($_POST['easy'][$i]);$j++) {

                        if($_POST['easy'][$i][$j]!='') {
                            $detail_data = array();
                            $otherids = $_POST['easy_id'][$i][$j];
                            $splittedString = explode('@', $otherids);
                            $id_course = $splittedString[0];
                            $id_topic = $splittedString[1];
                            $idtaxanomy = $splittedString[2];
                            $question_type = $i;
                            $difficult_level = 1;
                            $questions_selected = $_POST['easy'][$i][$j];
                            $detail_data['id_course'] = $id_course;
                            $detail_data['id_pool'] = 0;
                            $detail_data['id_topic'] = $id_topic;
                            $detail_data['id_bloom_taxonomy'] = $idtaxanomy;
                            $detail_data['id_difficult_level'] = $difficult_level;
                            $detail_data['questions_available'] = 0;
                            $detail_data['questions_selected'] = $questions_selected;
                            $detail_data['status'] = 1;
                            $detail_data['question_type'] = $question_type;
                            $detail_data['id_tos'] = $tosid;
                            $result = $this->tos_model->addNewTosDetails($detail_data);

                        }
                    }
                    
                }

                for($i=1;$i<=count($_POST['medium']);$i++) {

                    for($j=0;$j<count($_POST['medium'][$i]);$j++) {

                        if($_POST['medium'][$i][$j]!='') {
                            $detail_data = array();
                            $otherids = $_POST['medium_id'][$i][$j];
                            $splittedString = explode('@', $otherids);
                            $id_course = $splittedString[0];
                            $id_topic = $splittedString[1];
                            $idtaxanomy = $splittedString[2];
                            $question_type = $i;
                            $difficult_level = 2;
                            $questions_selected = $_POST['medium'][$i][$j];
                            $detail_data['id_course'] = $id_course;
                            $detail_data['id_pool'] = 0;
                            $detail_data['id_topic'] = $id_topic;
                            $detail_data['id_bloom_taxonomy'] = $idtaxanomy;
                            $detail_data['id_difficult_level'] = $difficult_level;
                            $detail_data['questions_available'] = 0;
                            $detail_data['questions_selected'] = $questions_selected;
                            $detail_data['status'] = 1;
                            $detail_data['question_type'] = $question_type;
                            $detail_data['id_tos'] = $tosid;

                            $result = $this->tos_model->addNewTosDetails($detail_data);
                        }
                    }
                    
                }

                 for($i=1;$i<=count($_POST['difficult']);$i++) {

                    for($j=0;$j<count($_POST['difficult'][$i]);$j++) {

                        if($_POST['difficult'][$i][$j]!='') {
                            $detail_data = array();
                            $otherids = $_POST['difficult_id'][$i][$j];
                            $splittedString = explode('@', $otherids);
                            $id_course = $splittedString[0];
                            $id_topic = $splittedString[1];
                            $idtaxanomy = $splittedString[2];
                            $question_type = $i;
                            $difficult_level = 3;
                            $questions_selected = $_POST['difficult'][$i][$j];
                            $detail_data['id_course'] = $id_course;
                            $detail_data['id_pool'] = 0;
                            $detail_data['id_topic'] = $id_topic;
                            $detail_data['id_bloom_taxonomy'] = $idtaxanomy;
                            $detail_data['id_difficult_level'] = $difficult_level;
                            $detail_data['questions_available'] = 0;
                            $detail_data['questions_selected'] = $questions_selected;
                            $detail_data['status'] = 1;
                            $detail_data['question_type'] = $question_type;
                            $detail_data['id_tos'] = $tosid;
                            $result = $this->tos_model->addNewTosDetails($detail_data);
                        }
                    }
                    
                }

                // for($i=0;$i<count($_POST['easy']);$i++) {
                //     //fetch all 
                //     if($_POST['easy'][$i]!='') {
                //         $detail_data = array();
                //         $otherids = $_POST['easy_id'][$i];
                //         $splittedString = explode('@', $otherids);
                //         $id_course = $splittedString[0];
                //         $id_topic = $splittedString[1];
                //         $idtaxanomy = $splittedString[2];
                //         $question_type = $splittedString[3];

                //         $difficult_level = 1;
                //         $questions_selected = $_POST['easy'][$i];


                //         $detail_data['id_course'] = $id_course;
                //         $detail_data['id_pool'] = 0;
                //         $detail_data['id_topic'] = $id_topic;
                //         $detail_data['id_bloom_taxonomy'] = $idtaxanomy;
                //         $detail_data['id_difficult_level'] = $difficult_level;
                //         $detail_data['questions_available'] = 0;
                //         $detail_data['questions_selected'] = $questions_selected;
                //         $detail_data['status'] = 1;
                //         $detail_data['question_type'] = $question_type;

                //         $detail_data['id_tos'] = $tosid;

                //          $result = $this->tos_model->addNewTosDetails($detail_data);
                //     }

                //     if($_POST['medium'][$i]!='') {
                //         $detail_data = array();
                //         $otherids = $_POST['medium_id'][$i];
                //         $splittedString = explode('@', $otherids);
                //         $id_course = $splittedString[0];
                //         $id_topic = $splittedString[1];
                //         $idtaxanomy = $splittedString[2];
                //         $question_type = $splittedString[3];
                //         $difficult_level = 2;
                //         $questions_selected = $_POST['medium'][$i];


                //         $detail_data['id_course'] = $id_course;
                //         $detail_data['id_pool'] = 0;
                //         $detail_data['id_topic'] = $id_topic;
                //         $detail_data['id_bloom_taxonomy'] = $idtaxanomy;
                //         $detail_data['id_difficult_level'] = $difficult_level;
                //         $detail_data['questions_available'] = 0;
                //         $detail_data['questions_selected'] = $questions_selected;
                //         $detail_data['status'] = 1;
                //         $detail_data['question_type'] = $question_type;
                //         $detail_data['id_tos'] = $tosid;

                //          $result = $this->tos_model->addNewTosDetails($detail_data);
                //     }

                //     if($_POST['difficult'][$i]!='') {
                //         $detail_data = array();
                //         $otherids = $_POST['difficult_id'][$i];
                //         $splittedString = explode('@', $otherids);
                //         $id_course = $splittedString[0];
                //         $id_topic = $splittedString[1];
                //         $idtaxanomy = $splittedString[2];
                //         $question_type = $splittedString[3];

                //         $difficult_level = 3;
                //         $questions_selected = $_POST['difficult'][$i];


                //         $detail_data['id_course'] = $id_course;
                //         $detail_data['id_pool'] = 0;
                //         $detail_data['id_topic'] = $id_topic;
                //         $detail_data['id_bloom_taxonomy'] = $idtaxanomy;
                //         $detail_data['id_difficult_level'] = $difficult_level;
                //         $detail_data['questions_available'] = 0;
                //         $detail_data['questions_selected'] = $questions_selected;
                //         $detail_data['status'] = 1;
                //         $detail_data['question_type'] = $question_type;

                //         $detail_data['id_tos'] = $tosid;

                //          $result = $this->tos_model->addNewTosDetails($detail_data);
                //     }

                // }


                $easyQuestionCount = $this->tos_model->getTosQuestionCount($tosid);

                 $main_datanew = array(
                    'question_count' => $easyQuestionCount->totalquestion
                );


           
                $this->tos_model->updateCountTos($main_datanew,$tosid);

                
                redirect('/question/tos/list');
            }

            $id_session = $this->session->my_session_id;

$this->tos_model->deletetemptosBySession($id_session);
            $data['poolList'] = $this->question_model->poolListByStatus('1');
            $this->global['pageCode'] = 'tos.add';
            $this->global['pageTitle'] = 'Examination Management System : Add TOS';
            $this->loadViews("tos/add", $this->global, $data, NULL);
        }
    }

      function getTosQuestions(){
        $pools = $this->security->xss_clean($this->input->post('pools'));
        $question_type = $this->security->xss_clean($this->input->post('question_type'));
        $tosid = $this->security->xss_clean($this->input->post('tosid'));

        $tosDetails = $this->tos_model->getTos($tosid);




        //fetch all courses based on pools
        $getAllCourse = $this->tos_model->getAllcourse($pools);



        $taxanomyDetails = $this->tos_model->getTaxanomylist('1');

        $totalQustionFrompool = 0;
        $table="
                  <div class='row'>
                        <div class='col-sm-4'>Selected Question  <input type='text' name='selques$question_type' id='selques$question_type' readonly='readonly' value='0'/></div>
                           <div class='col-sm-4'>Pending Question  <input type='text' name='penques$question_type' readonly='readonly' id='penques$question_type' value='0'/>
                           </div>
                         </div>
                <table class='table' id='list-table' width='100%' border='1'>
                 <tr><th rowspan='2' style='text-align:center;'>Course</th><th rowspan='2' style='text-align:center;'>Topic</th>";
                for($t=0;$t<count($taxanomyDetails);$t++) {
                    $idtaxanomy = $taxanomyDetails[$t]->id;
                    $taxanomyName = $taxanomyDetails[$t]->name;
                    $table.="<th colspan='3' style='text-align:center;'>$taxanomyName</th>";
                }

                   $table.="</tr><tr>";

                for($t=0;$t<count($taxanomyDetails);$t++) {
                    $idtaxanomy = $taxanomyDetails[$t]->id;
                    $taxanomyName = $taxanomyDetails[$t]->name;
                    $table.="<th>Easy</th><th>Medium</th><th>Difficult</th>";
                }
        $table.="</tr>";

        for($i=0;$i<count($getAllCourse);$i++) {
            $topicName = $getAllCourse[$i]->topicname;
            $courseName = $getAllCourse[$i]->coursename;
            $id_course = $getAllCourse[$i]->id_course;
            $id_topic = $getAllCourse[$i]->id_topic;
              $table.="<tr><td>$courseName</td><td>$topicName</td>";

               for($t=0;$t<count($taxanomyDetails);$t++) {
                    $idtaxanomy = $taxanomyDetails[$t]->id;
                    $taxanomyName = $taxanomyDetails[$t]->name;
                    $easyQuestionCount = array();
                    $mediumQuestionCount = array();
                    $difficultQuestionCount = array();

                    // easy question
                    $easyQuestionCount = $this->tos_model->getQuestionCountMCQ($idtaxanomy,$id_course,$id_topic,'1',$pools,$question_type);



                    // difficult questin
                    $mediumQuestionCount = $this->tos_model->getQuestionCountMCQ($idtaxanomy,$id_course,$id_topic,'2',$pools,$question_type);


                    // medium questions
                    $difficultQuestionCount = $this->tos_model->getQuestionCountMCQ($idtaxanomy,$id_course,$id_topic,'3',$pools,$question_type);

                    //get previous question 
                    if($tosid>0) {
                        $easypreviousquestionobject = $this->tos_model->getQuestionCountFromData($idtaxanomy,$id_course,$id_topic,'1',$tosid,$question_type);


                        $easypreviousquestion = $easypreviousquestionobject->questions_selected;


                        $mediumpreviousquestionobject = $this->tos_model->getQuestionCountFromData($idtaxanomy,$id_course,$id_topic,'2',$tosid,$question_type);
                        $mediumpreviousquestion = $mediumpreviousquestionobject->questions_selected;



                        $difficultpreviousquestionobject = $this->tos_model->getQuestionCountFromData($idtaxanomy,$id_course,$id_topic,'3',$tosid,$question_type);
                        $difficultpreviousquestion = $difficultpreviousquestionobject->questions_selected;



                    }

                    if($difficultpreviousquestion=='0') {
                        $difficultpreviousquestion ='';
                    }
                     if($mediumpreviousquestion=='0') {
                        $mediumpreviousquestion ='';
                    }
                     if($easypreviousquestion=='0') {
                        $easypreviousquestion ='';
                    }

                    $easyreadonly="";
                    if($easyQuestionCount==0) {
                    $easyreadonly="readonly=readonly";
                }


                 $mediumreadonly="";
                    if($mediumQuestionCount==0) {
                    $mediumreadonly="readonly=readonly";
                }

                $difficultreadonly="";
                    if($difficultQuestionCount==0) {
                    $difficultreadonly="readonly=readonly";
                }


                    $totalQustionFrompool = 
                     $totalQustionFrompool + $difficultQuestionCount +$mediumQuestionCount+  $easyQuestionCount;

                    $easylabelid = "1".$id_course.'_'.$id_topic.'_'.$idtaxanomy.'-'.$question_type;
                    $mediumlabelid = "2".$id_course.'_'.$id_topic.'_'.$idtaxanomy.'-'.$question_type;
                    $difficultlabelid = "3".$id_course.'_'.$id_topic.'_'.$idtaxanomy.'-'.$question_type;
                    $labelid = $id_course.'_'.$id_topic.'_'.$idtaxanomy.'-'.$question_type;

                    $table.="<td>$easyQuestionCount : 
                    <input type='text' name='easy[$question_type][]'  $easyreadonly style='width:25px;'  id='a$easylabelid'  
                    value='$easypreviousquestion'  
                    onkeyup='validateQtn(this.value,\"$easylabelid\",$question_type)'/>

                    <input type='hidden' name='easy_id[$question_type][]' id='' style='width:25px;' value='$id_course@$id_topic@$idtaxanomy@$question_type@1'/>

                    <input type='hidden' id='$easylabelid' style='width:25px;' value='$easyQuestionCount'/>


                    </td>
                    <td>$mediumQuestionCount : <input type='text' name='medium[$question_type][]'  style='width:25px;' $mediumreadonly 
                    value='$mediumpreviousquestion'    id='a$mediumlabelid'
                    onkeyup='validateQtn(this.value,\"$mediumlabelid\",$question_type)'/>

                    <input type='hidden' name='medium_id[$question_type][]' id='' style='width:25px;' value='$id_course@$id_topic@$idtaxanomy@$question_type@2'/>

                     <input type='hidden' id='$mediumlabelid' style='width:25px;' value='$mediumQuestionCount'/>



</td><td>$difficultQuestionCount : <input type='text' name='difficult[$question_type][]' $difficultreadonly id='' style='width:25px;' value='$difficultpreviousquestion'  id='a$difficultlabelid'
                    onkeyup='validateQtn(this.value,\"$difficultlabelid\",$question_type)/>
                  
                    <input type='hidden' name='difficult_id[$question_type][]' id='' style='width:25px;' value='$id_course@$id_topic@$idtaxanomy@$question_type'/>

                    </td>";
                }



               $table.="</tr>";
            }
        $table.="</table>";

        if($question_type=='1') {
            $mcqquestion = $tosDetails->mcq_qestion;
        }
        if($question_type=='2') {
            $mcqquestion = $tosDetails->essay_question;
        }
        if($question_type=='3') {
            $mcqquestion = $tosDetails->true_false_question;
        }


        if($question_type=='1') {
            $marksquestion = $tosDetails->mcq_marks;
        }
        if($question_type=='2') {
            $marksquestion = $tosDetails->essay_marks;
        }
        if($question_type=='3') {
            $marksquestion = $tosDetails->true_false_marks;
        }


        $topheader =" <p>Total Question From Pool : <input type='text' name='mcq_total_questions$question_type' id='mcq_total_questions$question_type' value='$totalQustionFrompool'/> </p>
                       <p>Enter Question : <input type='text' name='enter_mcq_questions$question_type' id='enter_mcq_questions$question_type' onblur='validateMCQQuestions($question_type)' value='$mcqquestion'> </p>
                        <p>Total Marks :<input type='text' readonly='readonly' name='marks_mcq_questions$question_type' id='marks_mcq_questions$question_type' value='$marksquestion'/></p>";

        echo $topheader.$table;
    }  
    function edit($id = NULL)
    {
        if ($this->checkAccess('tos.edit') == 1) {
            $this->loadAccessRestricted();
        } else {
            if ($id == null) {
                redirect('/setup/tos/list');
            }

            $user_id = $this->session->userId;
            $data['tosid'] = $id;

             if ($this->input->post()) {



                 
                 $name = $this->security->xss_clean($this->input->post('name'));
                 $mcq_qestion = $this->security->xss_clean($this->input->post('enter_mcq_questions1'));
                 $essay_question = $this->security->xss_clean($this->input->post('enter_mcq_questions2'));
                 $true_false_question = $this->security->xss_clean($this->input->post('enter_mcq_questions3'));

                 $mcq_marks = $this->security->xss_clean($this->input->post('marks_mcq_questions1'));
                 $essay_marks = $this->security->xss_clean($this->input->post('marks_mcq_questions2'));
                 $true_false_marks = $this->security->xss_clean($this->input->post('marks_mcq_questions3'));


                 $total_marks = $this->security->xss_clean($this->input->post('total_marks'));
                 $question_order = $this->security->xss_clean($this->input->post('question_order'));
                 $name = $this->security->xss_clean($this->input->post('name'));
                $question_count = $this->security->xss_clean($this->input->post('question_count'));
                $id_pool = implode(',', $this->security->xss_clean($this->input->post('id_pool')));
                $status = $this->security->xss_clean($this->input->post('status'));
                $main_data = array(
                    'name' => $name,
                    'question_count' => $question_count,
                    'id_pool' => $id_pool,
                    'status' => $status,
                    'updated_by' => $user_id,
                    'mcq_qestion' => $mcq_qestion,
                    'essay_question' => $essay_question,
                    'true_false_question' => $true_false_question,
                    'mcq_marks' => $mcq_marks,
                    'essay_marks' => $essay_marks,
                    'true_false_marks' => $true_false_marks,
                    'total_marks' => $total_marks,
                    'question_order' => $question_order
                );


                $this->tos_model->updateCountTos($main_data,$id);

                $tosid = $id;

              $this->tos_model->delteTosDetailsById($tosid);


               for($i=1;$i<=count($_POST['easy']);$i++) {



                    for($j=0;$j<count($_POST['easy'][$i]);$j++) {

                        if($_POST['easy'][$i][$j]!='') {
                            $detail_data = array();
                            $otherids = $_POST['easy_id'][$i][$j];
                            $splittedString = explode('@', $otherids);
                            $id_course = $splittedString[0];
                            $id_topic = $splittedString[1];
                            $idtaxanomy = $splittedString[2];
                            $question_type = $i;
                            $difficult_level = 1;
                            $questions_selected = $_POST['easy'][$i][$j];
                            $detail_data['id_course'] = $id_course;
                            $detail_data['id_pool'] = 0;
                            $detail_data['id_topic'] = $id_topic;
                            $detail_data['id_bloom_taxonomy'] = $idtaxanomy;
                            $detail_data['id_difficult_level'] = $difficult_level;
                            $detail_data['questions_available'] = 0;
                            $detail_data['questions_selected'] = $questions_selected;
                            $detail_data['status'] = 1;
                            $detail_data['question_type'] = $question_type;
                            $detail_data['id_tos'] = $tosid;
                            $result = $this->tos_model->addNewTosDetails($detail_data);

                        }
                    }
                    
                }

                for($i=1;$i<=count($_POST['medium']);$i++) {

                    for($j=0;$j<count($_POST['medium'][$i]);$j++) {

                        if($_POST['medium'][$i][$j]!='') {
                            $detail_data = array();
                            $otherids = $_POST['medium_id'][$i][$j];
                            $splittedString = explode('@', $otherids);
                            $id_course = $splittedString[0];
                            $id_topic = $splittedString[1];
                            $idtaxanomy = $splittedString[2];
                            $question_type = $i;
                            $difficult_level = 2;
                            $questions_selected = $_POST['medium'][$i][$j];
                            $detail_data['id_course'] = $id_course;
                            $detail_data['id_pool'] = 0;
                            $detail_data['id_topic'] = $id_topic;
                            $detail_data['id_bloom_taxonomy'] = $idtaxanomy;
                            $detail_data['id_difficult_level'] = $difficult_level;
                            $detail_data['questions_available'] = 0;
                            $detail_data['questions_selected'] = $questions_selected;
                            $detail_data['status'] = 1;
                            $detail_data['question_type'] = $question_type;
                            $detail_data['id_tos'] = $tosid;

                            $result = $this->tos_model->addNewTosDetails($detail_data);
                        }
                    }
                    
                }

                 for($i=1;$i<=count($_POST['difficult']);$i++) {

                    for($j=0;$j<count($_POST['difficult'][$i]);$j++) {

                        if($_POST['difficult'][$i][$j]!='') {
                            $detail_data = array();
                            $otherids = $_POST['difficult_id'][$i][$j];
                            $splittedString = explode('@', $otherids);
                            $id_course = $splittedString[0];
                            $id_topic = $splittedString[1];
                            $idtaxanomy = $splittedString[2];
                            $question_type = $i;
                            $difficult_level = 3;
                            $questions_selected = $_POST['difficult'][$i][$j];
                            $detail_data['id_course'] = $id_course;
                            $detail_data['id_pool'] = 0;
                            $detail_data['id_topic'] = $id_topic;
                            $detail_data['id_bloom_taxonomy'] = $idtaxanomy;
                            $detail_data['id_difficult_level'] = $difficult_level;
                            $detail_data['questions_available'] = 0;
                            $detail_data['questions_selected'] = $questions_selected;
                            $detail_data['status'] = 1;
                            $detail_data['question_type'] = $question_type;
                            $detail_data['id_tos'] = $tosid;
                            $result = $this->tos_model->addNewTosDetails($detail_data);
                        }
                    }
                    
                }

           
                $this->tos_model->updateCountTos($main_datanew,$tosid);

                
                redirect('/question/tos/list');
            }

            $data['tos'] = $this->tos_model->getTos($id);
            $data['tosdetails'] = $this->tos_model->getTosDetails($id);
            $data['poolList'] = $this->question_model->poolListByStatus('1');
            $this->global['pageCode'] = 'tos.edit';
            $this->global['pageTitle'] = 'Examination Management System : Edit TOS';
            $this->loadViews("tos/edit", $this->global, $data, NULL);
        }
    }

    function getcountentered() {
            $id_session = $this->session->my_session_id;

        $count = $_POST['count'];
        $questionid = $_POST['questionid'];
        $question_type = $_POST['question_type'];


        $this->tos_model->deletetemptos($questionid);

        $data = array(
                    'count' => $count,
                    'questionid' => $questionid,
                    'session_id' => $id_session,
                    'question_type' => $question_type,
                );
        $this->tos_model->addtempquestion($data);

        $totalentered = 0;
        $totalcount = $this->tos_model->getCount($id_session,$question_type);

        for($i=0;$i<count($totalcount);$i++) {
            $totalentered = $totalentered + $totalcount[$i]->count;
        }
        


        echo  $totalentered;
    }
}
