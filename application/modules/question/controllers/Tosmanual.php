<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


class Tosmanual extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('tos_model');
        $this->load->model('question_model');
        $this->load->model('examset_model');
        error_reporting(0);

        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('tosmanual.list') == 1) {
            $this->loadAccessRestricted();
        } else {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['type'] = 1;
            $data['searchParam'] = $formData;
            $data['tosList'] = $this->tos_model->manualtosListSearch($formData);
            $this->global['pageTitle'] = 'Examination Management System : TOS List';
            $this->global['pageCode'] = 'tosmanual.list';
            $this->loadViews("tosmanual/list", $this->global, $data, NULL);
        }
    }


     function getmoduledropdown() {
        $type = $_POST['id'];
       
         $results = $this->examset_model->getCourseListByType($type);

            $table="   
                <script type='text/javascript'>
                     $('select').select2();
                 </script>
         ";

            $table.="
            <select name='id_course' id='id_course' class='form-control'>
                <option value=''>Select</option>
                ";

            for($i=0;$i<count($results);$i++)
            {

            $id = $results[$i]->id;
            $name = $results[$i]->name;
           
            $table.="<option value=".$id.">".$name.
                    "</option>";

            }
            $table.="

            </select>";

            echo $table;
            exit;


    }


    function add()
    {
        if ($this->checkAccess('tosmanual.add') == 1) {
            $this->loadAccessRestricted();
        } else {
            $user_id = $this->session->userId;
            if ($this->input->post()) {

                 $name = $this->security->xss_clean($this->input->post('name'));
                 $code = $this->security->xss_clean($this->input->post('code'));
                 $mcq_question = $this->security->xss_clean($this->input->post('mcq_question'));
                 $essay_question = $this->security->xss_clean($this->input->post('essay_question'));
                 $true_false_question = $this->security->xss_clean($this->input->post('true_false_question'));

                 $mcq_marks = $this->security->xss_clean($this->input->post('mcq_marks'));
                 $essay_marks = $this->security->xss_clean($this->input->post('essay_marks'));
                 $true_false_marks = $this->security->xss_clean($this->input->post('true_false_marks'));


                 $total_marks = $this->security->xss_clean($this->input->post('total_marks'));
                 $question_order = $this->security->xss_clean($this->input->post('question_order'));
                 $name = $this->security->xss_clean($this->input->post('name'));
                $question_count = $this->security->xss_clean($this->input->post('question_count'));
                $status = $this->security->xss_clean($this->input->post('status'));

                if($question_count=='') {
                    echo "<script>alert('Please enter atleast one type of question')</script>";
                    echo "<script>parent.location='/question/tosmanual/add'</script>";
                    exit;
                }
                $main_data = array(
                    'prepared_from'=>'2',                    
                    'name' => $name,
                    'code' =>$code,
                    'question_count' => $question_count,
                    'status' => $status,
                    'created_by' => $user_id,
                    'type' =>1,
                    'mcq_question' => $mcq_question,
                    'essay_question' => $essay_question,
                    'true_false_question' => $true_false_question,
                    'mcq_marks' => $mcq_marks,
                    'essay_marks' => $essay_marks,
                    'true_false_marks' => $true_false_marks,
                    'total_marks' => $total_marks,
                    'question_order' => $question_order
                );
                $tosid = $this->tos_model->addNewManualTos($main_data);


                redirect('/question/tosmanual/edit/'.$tosid);
            }
        }
        $data['poolList'] = $this->question_model->poolListByStatus('1');
        $this->global['pageCode'] = 'tosmanual.add';
        $this->global['pageTitle'] = 'Examination Management System : Add TOS';
        $this->loadViews("tosmanual/add", $this->global, $data, NULL);
    }



    function edittos($id)
    {
        if ($this->checkAccess('tosmanual.add') == 1) {
            $this->loadAccessRestricted();
        } else {
            $user_id = $this->session->userId;
            if ($this->input->post()) {

                  $name = $this->security->xss_clean($this->input->post('name'));
                 $code = $this->security->xss_clean($this->input->post('code'));
                 $mcq_question = $this->security->xss_clean($this->input->post('mcq_question'));
                 $essay_question = $this->security->xss_clean($this->input->post('essay_question'));
                 $true_false_question = $this->security->xss_clean($this->input->post('true_false_question'));

                 $mcq_marks = $this->security->xss_clean($this->input->post('mcq_marks'));
                 $essay_marks = $this->security->xss_clean($this->input->post('essay_marks'));
                 $true_false_marks = $this->security->xss_clean($this->input->post('true_false_marks'));


                 $total_marks = $this->security->xss_clean($this->input->post('total_marks'));
                 $question_order = $this->security->xss_clean($this->input->post('question_order'));
                 $name = $this->security->xss_clean($this->input->post('name'));
                $question_count = $this->security->xss_clean($this->input->post('question_count'));
                $status = $this->security->xss_clean($this->input->post('status'));

                if($question_count=='') {
                    echo "<script>alert('Please enter atleast one type of question')</script>";
                    echo "<script>parent.location='/question/tosmanual/add'</script>";
                    exit;
                }
                $main_data = array(
                    'name' => $name,
                    'code' =>$code,
                    'question_count' => $question_count,
                    'status' => $status,
                    'created_by' => $user_id,
                    'mcq_question' => $mcq_question,
                    'essay_question' => $essay_question,
                    'true_false_question' => $true_false_question,
                    'mcq_marks' => $mcq_marks,
                    'essay_marks' => $essay_marks,
                    'true_false_marks' => $true_false_marks,
                    'total_marks' => $total_marks,
                    'question_order' => $question_order
                );

                $tosid = $this->tos_model->updatetosmanual($main_data,$id);


                redirect('/question/tosmanual/list');
            }
        }
            $data['tos'] = $this->tos_model->getManualTosById($id);
        $this->global['pageCode'] = 'tosmanual.edit';
        $this->global['pageTitle'] = 'Examination Management System : Add TOS';
        $this->loadViews("tosmanual/edittos", $this->global, $data, NULL);
    }

    function deleteids() {
        $id = $_POST['id'];
        $this->tos_model->deletemanualtosid($id);
    }

  

    function edit($id = NULL)
    {
        if ($this->checkAccess('tosmanual.edit') == 1) {
            $this->loadAccessRestricted();
        } else {
            if ($id == null) {
                redirect('/setup/tos/list');
            }

            $formData['id_pool'] = $this->security->xss_clean($this->input->post('id_pool'));
            $formData['id_course'] = $this->security->xss_clean($this->input->post('id_course'));
            $formData['id_topic'] = $this->security->xss_clean($this->input->post('id_topic'));
            $formData['question_type'] = $this->security->xss_clean($this->input->post('question_type'));
            $formData['id_course_learning_objective'] = $this->security->xss_clean($this->input->post('id_course_learning_objective'));
            $formData['id_bloom_taxonomy'] = $this->security->xss_clean($this->input->post('id_bloom_taxonomy'));
            $formData['id_difficult_level'] = $this->security->xss_clean($this->input->post('id_difficult_level'));
            $data['searchParam'] = $formData;

            if ($this->input->post()) {
                if($_POST['save']=='save') {

               if(!$_POST['question']) {
 echo "<script>alert('Please select atleast one question');</script>";
                            echo "<script>parent.location='/question/tosmanual/edit/$id'</script>";
                            exit;
                }

                        // if(count($_POST['question'])) {
                        //     echo "<script>alert('You cannot select more question than the pending');</script>";
                        //     echo "<script>parent.location='/question/tosmanual/edit/$id'</script>";
                        //     exit;
                        // }


                    if($_POST['question_type']=='1') {

                        if($_POST['pending_mcq']<count($_POST['question'])) {
                            echo "<script>alert('You cannot select more question than the pending');</script>";
                            echo "<script>parent.location='/question/tosmanual/edit/$id'</script>";
                            exit;
                        }
                    }

                    if($_POST['question_type']=='2') {

                        if($_POST['pending_essay']<count($_POST['question'])) {
                            echo "<script>alert('You cannot select more question than the pending');</script>";
                            echo "<script>parent.location='/question/tosmanual/edit/$id'</script>";
                            exit;
                        }
                    }

                    if($_POST['question_type']=='3') {

                        if($_POST['pending_true']<count($_POST['question'])) {
                            echo "<script>alert('You cannot select more question than the pending');</script>";
                            echo "<script>parent.location='/question/tosmanual/edit/$id'</script>";
                            exit;
                        }
                    }
                   for($i=0;$i<count($_POST['question']);$i++){
                        $main_data = array(
                            'id_manualtos' => $id,
                            'id_question' => $_POST['question'][$i]
                        );
                        $tosid = $this->tos_model->addmanualTosQuestion($main_data);
                    }
                }
                $data['questionList'] = $this->tos_model->questionListSearch($formData,$id);
            }
            $data['poolList'] = $this->question_model->poolListByStatus('1');
            $data['courseList'] = $this->question_model->courseListByStatus('1');
            $data['topicList'] = $this->question_model->topicListByStatus('1');
            $data['courseLearningObjectList'] = $this->question_model->courseLearningObjectListByStatus('1');
            $data['bloomTaxonomyList'] = $this->question_model->bloomTaxonomyListByStatus('1');
            $data['difficultLevelList'] = $this->question_model->difficultLevelListByStatus('1');         
            $this->global['pageTitle'] = 'Examination Management System : Question List';
            $this->global['pageCode'] = 'question.list';



            $user_id = $this->session->userId;
            $data['tosid'] = $id;

            $data['tos'] = $this->tos_model->getManualTosById($id);

            $totalAssignedMCQ = 0;
            $totalAssignedEssay = 0;
            $totalAssignedTrue = 0;
            $data['quesiontosdetails'] = $quesiontosdetails = $this->tos_model->getAllQuestionByManualTos($id);

            for($i=0;$i<count($quesiontosdetails);$i++) {

                if($quesiontosdetails[$i]->question_type=='1') {
                    $totalAssignedMCQ = $totalAssignedMCQ+1;
                }
                 if($quesiontosdetails[$i]->question_type=='2') {
                    $totalAssignedEssay = $totalAssignedEssay+1;
                }
                 if($quesiontosdetails[$i]->question_type=='3') {
                    $totalAssignedTrue = $totalAssignedTrue+1;
                }
            }

             $data['totalAssignedMCQ']= $totalAssignedMCQ;
            $data['totalAssignedEssay']= $totalAssignedEssay;
            $data['totalAssignedTrue']= $totalAssignedTrue;

            $this->global['pageCode'] = 'tosmanual.edit';
            $this->global['pageTitle'] = 'Examination Management System : Edit TOS';
            $this->loadViews("tosmanual/edit", $this->global, $data, NULL);
        }
    }


     function view($id = NULL)
    {
        if ($this->checkAccess('tosmanual.edit') == 1) {
            $this->loadAccessRestricted();
        } else {
            if ($id == null) {
                redirect('/setup/tos/list');
            }

            $formData['id_pool'] = $this->security->xss_clean($this->input->post('id_pool'));
            $formData['id_course'] = $this->security->xss_clean($this->input->post('id_course'));
            $formData['id_topic'] = $this->security->xss_clean($this->input->post('id_topic'));
            $formData['id_course_learning_objective'] = $this->security->xss_clean($this->input->post('id_course_learning_objective'));
            $formData['id_bloom_taxonomy'] = $this->security->xss_clean($this->input->post('id_bloom_taxonomy'));
            $formData['id_difficult_level'] = $this->security->xss_clean($this->input->post('id_difficult_level'));
            $data['searchParam'] = $formData;

            if ($this->input->post()) {
                if($_POST['save']=='save') {
                   for($i=0;$i<count($_POST['question']);$i++){
                        $main_data = array(
                            'id_manualtos' => $id,
                            'id_question' => $_POST['question'][$i]
                        );
                        $tosid = $this->tos_model->addmanualTosQuestion($main_data);
                    }
                }
                $data['questionList'] = $this->tos_model->questionListSearch($formData,$id);
            }
            $data['poolList'] = $this->question_model->poolListByStatus('1');
            $data['courseList'] = $this->question_model->courseListByStatus('1');
            $data['topicList'] = $this->question_model->topicListByStatus('1');
            $data['courseLearningObjectList'] = $this->question_model->courseLearningObjectListByStatus('1');
            $data['bloomTaxonomyList'] = $this->question_model->bloomTaxonomyListByStatus('1');
            $data['difficultLevelList'] = $this->question_model->difficultLevelListByStatus('1');         
            $this->global['pageTitle'] = 'Examination Management System : Question List';
            $this->global['pageCode'] = 'question.list';



            $user_id = $this->session->userId;
            $data['tosid'] = $id;

            $data['tos'] = $this->tos_model->getTos($id);
            $data['quesiontosdetails'] = $this->tos_model->getAllQuestionByManualTos($id);
            $this->global['pageCode'] = 'tosmanual.edit';
            $this->global['pageTitle'] = 'Examination Management System : Edit TOS';
            $this->loadViews("tosmanual/view", $this->global, $data, NULL);
        }
    }
}
