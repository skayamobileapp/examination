<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


class Questionbulk extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('question_model');
        $this->isLoggedIn();
        error_reporting(0);

    }

    function list()
    {
                    $data['poolList'] = $this->question_model->poolListByStatus('1');
            $data['courseList'] = $this->question_model->courseListByStatus('1');


        if ($this->checkAccess('questionbulk.list') == 1) {
            $this->loadAccessRestricted();
        } else {

            $user_id = $this->session->userId;

            if ($this->input->post()) {
                  $fileName = $_FILES["file"]["tmp_name"];
    
                    if ($_FILES["file"]["size"] > 0) {
                        
                        $file = fopen($fileName, "r");

                             $k=0;
                             while(($data = fgetcsv($file, 1000, ',')) !== FALSE) {
                                // number of fields in the csv
                               //Question

                                if($k>0) {

                                    $questionType = 1;//$data[$k][0];

                                    // id course
                                    $moduleName = $data[1];
                                    $courseDetails =  $this->question_model->getCourseId($moduleName);
                                    $courseId = $courseDetails->id;
                                    //getModule Id 

                                    $topicName = $data[2];
                                     $topicDetails =  $this->question_model->getTopicId($topicName);
                                    $topicId = $topicDetails->id;


                                    $taxanomyName = $data[3];
                                     $taxanomyDetails =  $this->question_model->getTaxanomyId($taxanomyName);
                                    $taxanomyId = $taxanomyDetails->id;
                                    $question_type = 1;
                                    $marks = 2;

                                    $difficultyId = 1;
                                    if($data[4]=='C1') {
                                        $difficultyId = 1;
                                    }
                                    if($data[4]=='C2') {
                                        $difficultyId = 2;
                                    }
                                    if($data[4]=='C3') {
                                        $difficultyId = 3;
                                    }
                                    $questionName = $data[5];


                                        $dataquestionsinsert = array(
                                        'question' => $questionName,
                                        'question_type' =>$question_type,
                                        'id_pool' => 3,
                                        'id_course' => $courseId,
                                        'id_topic' => $topicId,
                                        'id_course_learning_objective' => 1,
                                        'id_bloom_taxonomy' => $taxanomyId,
                                        'id_difficult_level' => $difficultyId,
                                        'marks' => $marks,
                                        'status' => '1',
                                        'created_by' => $user_id,
                                        'parent_id' => 0
                                    );

                                  

                                    $result = $question_id = $this->question_model->addNewQuestion($dataquestionsinsert);


                                    $answerName1 = $data[6];
                                    $answerName2 = $data[7];
                                    $answerName3 = $data[8];
                                    $answerName4 = $data[9];

                                    $answerOne =0;
                                    $answerTwo =0;
                                    $answerThree =0;
                                    $answerFour =0;

                                    if($data[10]=='A1') {
                                        $answerOne =1;

                                    }

                                    $datafirstSet = array(
                                            'id_question' => $question_id,
                                            'option_description' => $answerName1,
                                            'is_correct_answer' => $answerOne
                                        );
                                    $this->question_model->addQuestionHasOption($datafirstSet);

                                    if($data[10]=='A2') {
                                        $answerTwo =1;
                                    }
                                    $datafirstSet = array(
                                            'id_question' => $question_id,
                                            'option_description' => $answerName2,
                                            'is_correct_answer' => $answerTwo
                                        );
                                    $this->question_model->addQuestionHasOption($datafirstSet);


                                    if($data[10]=='A3') {
                                        $answerThree = 1;
                                    }
                                    $datafirstSet = array(
                                            'id_question' => $question_id,
                                            'option_description' => $answerName3,
                                            'is_correct_answer' => $answerThree
                                        );
                                    $this->question_model->addQuestionHasOption($datafirstSet);
                                    if($data[10]=='A4') {
                                        $answerFour = 1;
                                    }
                                    $datafirstSet = array(
                                            'id_question' => $question_id,
                                            'option_description' => $answerName4,
                                            'is_correct_answer' => $answerFour
                                        );
                                    $this->question_model->addQuestionHasOption($datafirstSet);
                                }
                                $k++;
                            }

                            echo "<script>alert('Question has been uploaded')</script>";
                            echo "<script>parent.location='/question/question/list'</script>";
                            exit;
                    }
            }
            
            $this->global['pageTitle'] = 'Examination Management System : Question List';
            $this->global['pageCode'] = 'questionbulk.list';

            // print_r($data);exit;

            $this->loadViews("questionbulk/list", $this->global, $data, NULL);
        }
    }

    function add()
    {
      
            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;

            if ($this->input->post()) {
                if ($_FILES['image_file'])
                {
                    $certificate_name = $_FILES['image_file']['name'];
                    $certificate_size = $_FILES['image_file']['size'];
                    $certificate_tmp = $_FILES['image_file']['tmp_name'];

                    // echo "<Pre>"; print_r($certificate_tmp);exit();

                    $certificate_ext = explode('.', $certificate_name);
                    $certificate_ext = end($certificate_ext);
                    $certificate_ext = strtolower($certificate_ext);


                 

                    $image = $this->uploadFile($certificate_name, $certificate_tmp, 'Image File');


$id_pool = $_POST['id_pool'];
$id_course = $_POST['id_course'];
$marks = $_POST['marks'];
     $filename = "/var/www/html/examination/assets/images/".$image;
     // or /var/www/html/file.docx  
 $content = $this->read_file_docx($filename);  
 if($content !== false) {  
    $questionbank =  nl2br($content);  


    $totalQuestionArray = explode("QUESTION",$questionbank);

    for($i=1;$i<count($totalQuestionArray);$i++) {

        $firstQuestion = $totalQuestionArray[$i];


        $questionid =  $this->getquestion($firstQuestion,$id_pool,$id_course,$marks);
        // get the 4 options
         $this->getfouroptions($firstQuestion,$questionid);

        // get the correct answer


    }

 }  
  else {  
      echo 'Couldn\'t the file. Please check that file.';  
           }  

                   
                }
            }
               


            $data['poolList'] = $this->question_model->poolListByStatus('1');
            $data['courseList'] = $this->question_model->courseListByStatus('1');


              
            $this->global['pageTitle'] = 'Examination Management System : Add Question';
            $this->global['pageCode'] = 'question.add';

            $this->loadViews("questionbulk/add", $this->global, $data, NULL);
        
    }


    function read_file_docx($filename){  
      $striped_content = '';  
      $content = '';  
      if(!$filename || !file_exists($filename)) return false;  
      $zip = zip_open($filename);  
      if (!$zip || is_numeric($zip)) return false;  
      while ($zip_entry = zip_read($zip)) {  
      if (zip_entry_open($zip, $zip_entry) == FALSE) continue;  
      if (zip_entry_name($zip_entry) != "word/document.xml") continue;  
      $content .= zip_entry_read($zip_entry, zip_entry_filesize($zip_entry));  
      zip_entry_close($zip_entry);  
      }// end while  
      zip_close($zip);  
      $content = str_replace('</w:r></w:p></w:tc><w:tc>', " ", $content);  
      $content = str_replace('</w:r></w:p>', "\r\n", $content);  
      $striped_content = strip_tags($content);  
      return $striped_content;  
 }  




function getquestion($string,$id_pool,$id_course,$marks) {
    $totalQuestionArray = explode(")",$string);

    $firstquestion = $totalQuestionArray[1];
    $firstQuestionArray = explode('Answers',$firstquestion);
    $question = $firstQuestionArray[0];



    $parsed = $this->get_string_between($string, '(', ')');
    $topicdifficultarray  = explode(',',$parsed);
    $difficultyLevel = $topicdifficultarray[0];
     $difficultlevelDB = $this->question_model->getDifficultyLevelByName($difficultyLevel);


    $topicLevel = $topicdifficultarray[1];
    $topicLevelNumber = preg_match_all('/\d+/', $topicLevel, $matches);
     $topicLevelDB = $this->question_model->getTopicLevelByName($matches[0][0]);



    //


                    $data = array(
                    'question' => $question,
                    'question_type' =>1,
                    'id_pool' => $id_pool,
                    'id_course' => $id_course,
                    'id_topic' => $topicLevelDB->id,
                    'id_course_learning_objective' => 1,
                    'id_bloom_taxonomy' => 1,
                    'id_difficult_level' => $difficultlevelDB->id,
                    'marks' => $marks,
                    'status' => 1,
                    'created_by' => 1,
                    'parent_id' => 0
                );

              

              $question_id = $this->question_model->addNewQuestion($data);


             return $question_id;
    
}


function get_string_between($string, $start, $end){
    $string = ' ' . $string;
    $ini = strpos($string, $start);
    if ($ini == 0) return '';
    $ini += strlen($start);
    $len = strpos($string, $end, $ini) - $ini;
    return substr($string, $ini, $len);
}



function getfouroptions($text,$questionid) {

$start = "Answers";
$end = "Correct Answer";


$correctAnswerArray = explode('Correct Answer',$text);

    $correctOption = preg_match_all('/\d+/', $correctAnswerArray[1], $matches);

 $correctAnswer = $matches[0][0];
$start_pos = strpos($text, $start);
$end_pos = strpos($text, $end);
 $substring = substr($text, $start_pos, $end_pos - $start_pos + strlen($end));
    $totalQuestionArray = explode("<br />",$substring);

    

    for($j=1;$j<count($totalQuestionArray);$j++) {
        $firstCorrectAnswer = 0;
        if($correctAnswer==$j) {
             $firstCorrectAnswer = 1;
        }

        // echo trim($totalQuestionArray[$j])  ;
     $totallength =  strlen($totalQuestionArray[$j]);

     // check for 1st character
        $read = 0;
       $fstchar = substr(trim($totalQuestionArray[$j]), 0,1);

      if(is_numeric($fstchar)) {
        $read = 1;
      }


     // check for 2nd character
      $secondchar = substr(trim($totalQuestionArray[$j]), 1,1);

      if($secondchar=='.') {
        $read = $read + 1;
      }

    // read full string 
     $answerstring = substr(trim($totalQuestionArray[$j]), $read,$totallength);


        $datafirstSet = array(
                            'id_question' => $questionid,
                            'option_description' => $answerstring,
                            'is_correct_answer' => $firstCorrectAnswer
                        );
                        $result = $this->question_model->addQuestionHasOption($datafirstSet);

    }

 }


}
