<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


class Question extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('question_model');
        $this->isLoggedIn();
        error_reporting(0);

    }

    function list()
    {
        
        if ($this->checkAccess('question.list') == 1) {
            $this->loadAccessRestricted();
        } else {
            $formData['id_pool'] = $this->security->xss_clean($this->input->post('id_pool'));
            $formData['id_course'] = $this->security->xss_clean($this->input->post('id_course'));
            $formData['id_topic'] = $this->security->xss_clean($this->input->post('id_topic'));
            $formData['id_course_learning_objective'] = $this->security->xss_clean($this->input->post('id_course_learning_objective'));
            $formData['id_bloom_taxonomy'] = $this->security->xss_clean($this->input->post('id_bloom_taxonomy'));
            $formData['id_difficult_level'] = $this->security->xss_clean($this->input->post('id_difficult_flevel'));


            $data['searchParam'] = $formData;
            $data['questionList'] = $this->question_model->questionListSearch($formData);
            $data['poolList'] = $this->question_model->poolListByStatus('1');
            $data['courseList'] = $this->question_model->courseListByStatus('1');
            $data['topicList'] = array(); //$this->question_model->topicListByStatus('1');
            $data['courseLearningObjectList'] = $this->question_model->courseLearningObjectListByStatus('1');
            $data['bloomTaxonomyList'] = $this->question_model->bloomTaxonomyListByStatus('1');
            $data['difficultLevelList'] = $this->question_model->difficultLevelListByStatus('1');
            
            $this->global['pageTitle'] = 'Examination Management System : Question List';
            $this->global['pageCode'] = 'question.list';

            // print_r($data);exit;

            $this->loadViews("questions/list", $this->global, $data, NULL);
        }
    }


      function auditlog($idquestion)
    {
        
       
        

            $data['questionList'] = $this->question_model->questionListSearchLog($idquestion);
            $data['poolList'] = $this->question_model->poolListByStatus('1');
            $data['courseList'] = $this->question_model->courseListByStatus('1');
            $data['topicList'] = array(); //$this->question_model->topicListByStatus('1');
            $data['courseLearningObjectList'] = $this->question_model->courseLearningObjectListByStatus('1');
            $data['bloomTaxonomyList'] = $this->question_model->bloomTaxonomyListByStatus('1');
            $data['difficultLevelList'] = $this->question_model->difficultLevelListByStatus('1');
            
            $this->global['pageTitle'] = 'Examination Management System : Question List';
            $this->global['pageCode'] = 'question.list';

            // print_r($data);exit;

            $this->loadViews("questions/auditlog", $this->global, $data, NULL);
        
    }


    function add()
    {
        if ($this->checkAccess('question.add') == 1) {
            $this->loadAccessRestricted();
        } else {

            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;

            if ($this->input->post()) {
                if ($_FILES['image_file'])
                {
                    $certificate_name = $_FILES['image_file']['name'];
                    $certificate_size = $_FILES['image_file']['size'];
                    $certificate_tmp = $_FILES['image_file']['tmp_name'];

                    // echo "<Pre>"; print_r($certificate_tmp);exit();

                    $certificate_ext = explode('.', $certificate_name);
                    $certificate_ext = end($certificate_ext);
                    $certificate_ext = strtolower($certificate_ext);


                    $this->fileFormatNSizeValidation($certificate_ext, $certificate_size, 'Image File');

                    $image = $this->uploadFile($certificate_name, $certificate_tmp, 'Image File');
                }



                $question = $this->security->xss_clean($this->input->post('question'));
                $id_pool = $this->security->xss_clean($this->input->post('id_pool'));
                $id_course = $this->security->xss_clean($this->input->post('id_course'));
                $id_topic = $this->security->xss_clean($this->input->post('id_topic'));
                $id_course_learning_objective = $this->security->xss_clean($this->input->post('id_course_learning_objective'));
                $id_bloom_taxonomy = $this->security->xss_clean($this->input->post('id_bloom_taxonomy'));
                $id_difficult_level = $this->security->xss_clean($this->input->post('id_difficult_level'));
                $marks = $this->security->xss_clean($this->input->post('marks'));
                $status = $this->security->xss_clean($this->input->post('status'));
            $question_type = $this->security->xss_clean($this->input->post('question_type'));

            $answer_1 = $this->security->xss_clean($this->input->post('answer_1'));
            $answer_2 = $this->security->xss_clean($this->input->post('answer_2'));
            $answer_3 = $this->security->xss_clean($this->input->post('answer_3'));
            $answer_4 = $this->security->xss_clean($this->input->post('answer_4'));


            $mcq_correct_answer = $this->security->xss_clean($this->input->post('mcq_correct_answer'));
            $true_correct_answer = $this->security->xss_clean($this->input->post('true_correct_answer'));




                $data = array(
                    'question' => $question,
                    'question_type' =>$question_type,
                    'id_pool' => $id_pool,
                    'id_course' => $id_course,
                    'id_topic' => $id_topic,
                    'id_course_learning_objective' => $id_course_learning_objective,
                    'id_bloom_taxonomy' => $id_bloom_taxonomy,
                    'id_difficult_level' => $id_difficult_level,
                    'marks' => $marks,
                    'status' => $status,
                    'created_by' => $user_id,
                    'parent_id' => 0
                );

                if ($image != '') {
                    $data['image'] = $image;
                }

                $result = $question_id = $this->question_model->addNewQuestion($data);
                if($result) {
                    // for MCQ 
                    if($question_type=='1') {

                        $firstCorrectAnswer = 0;
                        if($mcq_correct_answer=='1') {
                            $firstCorrectAnswer = 1;
                        }

                         $datafirstSet = array(
                            'id_question' => $question_id,
                            'option_description' => $answer_1,
                            'is_correct_answer' => $firstCorrectAnswer
                        );
                        $result = $this->question_model->addQuestionHasOption($datafirstSet);


                        $secondCorrectAnswer = 0;
                        if($mcq_correct_answer=='2') {
                            $secondCorrectAnswer = 1;
                        }

                         $datasecondSet = array(
                            'id_question' => $question_id,
                            'option_description' => $answer_2,
                            'is_correct_answer' => $secondCorrectAnswer
                        );
                        $result = $this->question_model->addQuestionHasOption($datasecondSet);

                        $thirdCorrectAnswer = 0;
                        if($mcq_correct_answer=='3') {
                            $thirdCorrectAnswer = 1;
                        }

                         $datathirdSet = array(
                            'id_question' => $question_id,
                            'option_description' => $answer_3,
                            'is_correct_answer' => $thirdCorrectAnswer
                        );
                        $result = $this->question_model->addQuestionHasOption($datathirdSet);

                        $fourCorrectAnswer = 0;
                        if($mcq_correct_answer=='4') {
                            $fourCorrectAnswer = 1;
                        }

                         $datafourthset = array(
                            'id_question' => $question_id,
                            'option_description' => $answer_4,
                            'is_correct_answer' => $fourCorrectAnswer
                        );
                        $result = $this->question_model->addQuestionHasOption($datafourthset);


                    }

                    if($question_type=='3') {

                        $firstCorrectAnswer = 0;
                        if($true_correct_answer=='1') {
                            $firstCorrectAnswer = 1;
                        }

                        $datafirstSet = array(
                            'id_question' => $question_id,
                            'option_description' => 'TRUE',
                            'is_correct_answer' => $firstCorrectAnswer
                        );
                        $result = $this->question_model->addQuestionHasOption($datafirstSet);


                        $secondCorrectAnswer = 0;
                        if($true_correct_answer=='2') {
                            $secondCorrectAnswer = 1;
                        }

                         $datasecondSet = array(
                            'id_question' => $question_id,
                            'option_description' => 'FALSE',
                            'is_correct_answer' => $secondCorrectAnswer
                        );
                        $result = $this->question_model->addQuestionHasOption($datasecondSet);


                    }

                    if($question_type=='2') {
                        $sessionid = session_id();
                        $listofData = $this->question_model->gettempsaveQuestions($sessionid);

                        for($r=0;$r<count($listofData);$r++) {
                            $qmarks = $listofData[$r]->qmarks;
                            $answer_question = $listofData[$r]->answer_question;
                            $answer_scheme = $listofData[$r]->answer_scheme;
                            $totalmarks = $totalmarks + $qmarks;


                            $dataQuestions = array(
                                'question' => $answer_question,
                                'marks' => $qmarks,
                                'answer_scheme' => $answer_scheme,
                                'created_by' => $user_id,
                                'parent_id' => $question_id
                            );

                       $this->question_model->addNewQuestion($dataQuestions);
                    }
                }
            }
                if ($result > 0) {
                    $this->session->set_flashdata('success', 'New Question added successfully');
                } else {
                    $this->session->set_flashdata('error', 'Question add failed');
                }
                redirect('/question/question/list');
            }
            $data['answerStatusList'] = $this->question_model->answerStatusListByStatus('1');

            $data['poolList'] = $this->question_model->poolListByStatus('1');
            $data['courseList'] = $this->question_model->courseListByStatus('1');
            $data['topicList'] = array(); //$this->question_model->topicListByStatus('1');
            $data['courseLearningObjectList'] = $this->question_model->courseLearningObjectListByStatus('1');
            $data['bloomTaxonomyList'] = $this->question_model->bloomTaxonomyListByStatus('1');
            $data['difficultLevelList'] = $this->question_model->difficultLevelListByStatus('1');

            $this->global['pageTitle'] = 'Examination Management System : Add Question';
            $this->global['pageCode'] = 'question.add';

            $this->loadViews("questions/add", $this->global, $data, NULL);
        }
    }



    function view($id = NULL)
    {
        if ($this->checkAccess('question.edit') == 1) {
            $this->loadAccessRestricted();
        } else {
            if ($id == null) {
                redirect('/question/question/list');
            }

            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;

         
            $question = $data['question'] = $this->question_model->getQuestion($id);

            $data['questionHasOption'] = $this->question_model->getQuestionHasOption($id);

            $data['poolList'] = $this->question_model->poolListByStatus('1');
            $data['courseList'] = $this->question_model->courseListByStatus('1');
            $data['topicList'] = $this->question_model->topicListByCourse($data['question']->id_course);
            $data['courseLearningObjectList'] = $this->question_model->courseLearningObjectListByStatus('1');
            $data['bloomTaxonomyList'] = $this->question_model->bloomTaxonomyListByStatus('1');
            $data['difficultLevelList'] = $this->question_model->difficultLevelListByStatus('1');
            $data['answerStatusList'] = $this->question_model->answerStatusListByStatus('1');



            $data['questionList'] = $this->question_model->getChildQuestionByParent($question->id);

            for($k=0;$k<count($data['questionHasOption']);$k++) {
                if($data['questionHasOption'][$k]->is_correct_answer=='1') {
                    $data['correctAnswer'] = $k+1;
                }
            }


            $this->global['pageTitle'] = 'Examination Management System : Edit Question';
            $this->global['pageCode'] = 'question.edit';

            $this->loadViews("questions/view", $this->global, $data, NULL);
        }
    }


    function edit($id = NULL)
    {
        if ($this->checkAccess('question.edit') == 1) {
            $this->loadAccessRestricted();
        } else {
            if ($id == null) {
                redirect('/question/question/list');
            }

            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;

            if ($this->input->post())
            {
                
                if ($_FILES['image_file'])
                {
                    $certificate_name = $_FILES['image_file']['name'];
                    $certificate_size = $_FILES['image_file']['size'];
                    $certificate_tmp = $_FILES['image_file']['tmp_name'];
                    $certificate_ext = explode('.', $certificate_name);
                    $certificate_ext = end($certificate_ext);
                    $certificate_ext = strtolower($certificate_ext);


                    $this->fileFormatNSizeValidation($certificate_ext, $certificate_size, 'Image File');

                    $image = $this->uploadFile($certificate_name, $certificate_tmp, 'Image File');
                }


                $question = $this->security->xss_clean($this->input->post('question'));
                $id_pool = $this->security->xss_clean($this->input->post('id_pool'));
                $id_course = $this->security->xss_clean($this->input->post('id_course'));
                $id_topic = $this->security->xss_clean($this->input->post('id_topic'));
                $id_course_learning_objective = $this->security->xss_clean($this->input->post('id_course_learning_objective'));
                $id_bloom_taxonomy = $this->security->xss_clean($this->input->post('id_bloom_taxonomy'));
                $id_difficult_level = $this->security->xss_clean($this->input->post('id_difficult_level'));
                $marks = $this->security->xss_clean($this->input->post('marks'));
                $status = $this->security->xss_clean($this->input->post('status'));
                $question_type = $this->security->xss_clean($this->input->post('question_type'));

                $answer_1 = $this->security->xss_clean($this->input->post('answer_1'));
                $answer_2 = $this->security->xss_clean($this->input->post('answer_2'));
                $answer_3 = $this->security->xss_clean($this->input->post('answer_3'));
                $answer_4 = $this->security->xss_clean($this->input->post('answer_4'));

                $answer_option_1 = $this->security->xss_clean($this->input->post('answer_option_1'));
                $answer_option_2 = $this->security->xss_clean($this->input->post('answer_option_2'));
                $answer_option_3 = $this->security->xss_clean($this->input->post('answer_option_3'));
                $answer_option_4 = $this->security->xss_clean($this->input->post('answer_option_4'));


                $true_option_1 = $this->security->xss_clean($this->input->post('true_option_1'));
                $true_option_2 = $this->security->xss_clean($this->input->post('true_option_2'));



                $mcq_correct_answer = $this->security->xss_clean($this->input->post('mcq_correct_answer'));
                $true_correct_answer = $this->security->xss_clean($this->input->post('true_correct_answer'));


                $data = array(
                    'question' => $question,
                    'id_pool' => $id_pool,
                    'id_course' => $id_course,
                    'id_topic' => $id_topic,
                    'id_course_learning_objective' => $id_course_learning_objective,
                    'id_bloom_taxonomy' => $id_bloom_taxonomy,
                    'id_difficult_level' => $id_difficult_level,
                    'marks' => $marks,
                    'status' => $status,
                    'updated_by' => $user_id,
                    'updated_dt_tm'=>date('Y-m-d H:i:s')                    
                    
                );

                if ($image != '') {
                    $data['image'] = $image;
                }

                $result = $this->question_model->editQuestion($data, $id);
                $data['id_question'] = $id;
                // $this->question_model->addQuestionLog($data);
                if($result) {
                    if($question_type=='1') {

                        $firstCorrectAnswer = 0;
                        if($mcq_correct_answer=='1') {
                            $firstCorrectAnswer = 1;
                        }

                         $datafirstSet = array(
                            'option_description' => $answer_1,
                            'is_correct_answer' => $firstCorrectAnswer
                        );
                        $result = $this->question_model->editQuestionHasOption($datafirstSet,$answer_option_1);


                        $secondCorrectAnswer = 0;
                        if($mcq_correct_answer=='2') {
                            $secondCorrectAnswer = 1;
                        }

                         $datasecondSet = array(
                            'option_description' => $answer_2,
                            'is_correct_answer' => $secondCorrectAnswer
                        );
                        $result = $this->question_model->editQuestionHasOption($datasecondSet,$answer_option_2);

                        $thirdCorrectAnswer = 0;
                        if($mcq_correct_answer=='3') {
                            $thirdCorrectAnswer = 1;
                        }

                         $datathirdSet = array(
                            'option_description' => $answer_3,
                            'is_correct_answer' => $thirdCorrectAnswer
                        );
                        $result = $this->question_model->editQuestionHasOption($datathirdSet,$answer_option_3);

                        $fourCorrectAnswer = 0;
                        if($mcq_correct_answer=='4') {
                            $fourCorrectAnswer = 1;
                        }

                         $datafourthset = array(
                            'option_description' => $answer_4,
                            'is_correct_answer' => $fourCorrectAnswer
                        );
                        $result = $this->question_model->editQuestionHasOption($datafourthset,$answer_option_4);


                    }


                     if($question_type=='3') {

                          $firstCorrectAnswer = 0;
                        if($true_correct_answer=='1') {
                            $firstCorrectAnswer = 1;
                        }

                         $datafirstSet = array(
                            'option_description' => 'TRUE',
                            'is_correct_answer' => $firstCorrectAnswer
                        );
                        $result = $this->question_model->editQuestionHasOption($datafirstSet,$true_option_1);


                        $secondCorrectAnswer = 0;
                        if($true_correct_answer=='2') {
                            $secondCorrectAnswer = 1;
                        }

                         $datasecondSet = array(
                            'option_description' => 'FALSE',
                            'is_correct_answer' => $secondCorrectAnswer
                        );
                        $result = $this->question_model->editQuestionHasOption($datasecondSet,$true_option_2);


                    }

                }
                if ($result) {
                    $this->session->set_flashdata('success', 'Question edited successfully');
                } else {
                    $this->session->set_flashdata('error', 'Question edit failed');
                }
                redirect('/question/question/list');
            }

            $question = $data['question'] = $this->question_model->getQuestion($id);

            $data['questionHasOption'] = $this->question_model->getQuestionHasOption($id);

            $data['poolList'] = $this->question_model->poolListByStatus('1');
            $data['courseList'] = $this->question_model->courseListByStatus('1');
            $data['topicList'] = $this->question_model->topicListByCourse($data['question']->id_course);
            $data['courseLearningObjectList'] = $this->question_model->courseLearningObjectListByStatus('1');
            $data['bloomTaxonomyList'] = $this->question_model->bloomTaxonomyListByStatus('1');
            $data['difficultLevelList'] = $this->question_model->difficultLevelListByStatus('1');
            $data['answerStatusList'] = $this->question_model->answerStatusListByStatus('1');



            $data['questionList'] = $this->question_model->getChildQuestionByParent($question->id);

            for($k=0;$k<count($data['questionHasOption']);$k++) {
                if($data['questionHasOption'][$k]->is_correct_answer=='1') {
                    $data['correctAnswer'] = $k+1;
                }
            }


            $this->global['pageTitle'] = 'Examination Management System : Edit Question';
            $this->global['pageCode'] = 'question.edit';

            $this->loadViews("questions/edit", $this->global, $data, NULL);
        }
    }

    function addOptions($id = NULL)
    {
        if ($this->checkAccess('questionhasoption.add') == 1) {
            $this->loadAccessRestricted();
        } else {
            if ($id == null) {
                redirect('/question/question/list');
            }

            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;

            if ($this->input->post())
            {

                // if ($_FILES['image']) {
                //     $certificate_name = $_FILES['image']['name'];
                //     $certificate_size = $_FILES['image']['size'];
                //     $certificate_tmp = $_FILES['image']['tmp_name'];
                //     $certificate_ext = explode('.', $certificate_name);
                //     $certificate_ext = end($certificate_ext);
                //     $certificate_ext = strtolower($certificate_ext);


                //     $this->fileFormatNSizeValidation($certificate_ext, $certificate_size, 'Image File');

                //     $image = $this->uploadFile($certificate_name, $certificate_tmp, 'Image File');
                // }


                $option_description = $this->security->xss_clean($this->input->post('option_description'));
                $is_correct_answer = $this->security->xss_clean($this->input->post('is_correct_answer'));

                $data = array(
                    'id_question' => $id,
                    'option_description' => $option_description,
                    'is_correct_answer' => $is_correct_answer
                );

                // if ($image != '') {
                //     $data['image'] = $image;
                // }

                $result = $this->question_model->addQuestionHasOption($data);

                redirect('/question/question/addOptions/' . $id);
            }

            $data['question'] = $this->question_model->getQuestion($id);
            $data['poolList'] = $this->question_model->poolListByStatus('1');
            $data['questionHasOption'] = $this->question_model->getQuestionHasOption($id);
            $data['poolList'] = $this->question_model->poolListByStatus('1');
            $data['courseList'] = $this->question_model->courseListByStatus('1');
            $data['topicList'] = $this->question_model->topicListByStatus('1');
            $data['courseLearningObjectList'] = $this->question_model->courseLearningObjectListByStatus('1');
            $data['bloomTaxonomyList'] = $this->question_model->bloomTaxonomyListByStatus('1');
            $data['difficultLevelList'] = $this->question_model->difficultLevelListByStatus('1');

            $data['answerStatusList'] = $this->question_model->answerStatusListByStatus('1');

            // echo "<pre>";print_r($data['questionHasOption']);die;


            $this->global['pageTitle'] = 'Examination Management System : Add Question';
            $this->global['pageCode'] = 'questionhasoption.add';

            $this->loadViews("questions/question_has_option", $this->global, $data, NULL);
        }
    }

    function editOptions($id_option,$id_question)
    {
        if ($this->checkAccess('questionhasoption.add') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id_option == null)
            {
                redirect('/question/question/addOptions/'.$id_question);
            }

            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;

            if ($this->input->post())
            {
                $option_description = $this->security->xss_clean($this->input->post('option_description'));
                $is_correct_answer = $this->security->xss_clean($this->input->post('is_correct_answer'));

                $data = array(
                    'id_question' => $id_question,
                    'option_description' => $option_description,
                    'is_correct_answer' => $is_correct_answer
                );

                $result = $this->question_model->editQuestionHasOption($data,$id_option);

                redirect('/question/question/addOptions/' . $id_question);
            }


            $data['answerStatusList'] = $this->question_model->answerStatusListByStatus('1');
            $data['question'] = $this->question_model->getQuestion($id_question);
            $data['questionHasOption'] = $this->question_model->getQuestionHasOptionById($id_option);

            $data['id_option'] = $id_option;
            $data['id_question'] = $id_question;

            $this->global['pageTitle'] = 'Examination Management System : Edit Question';
            $this->global['pageCode'] = 'questionhasoption.edit';

            $this->loadViews("questions/edit_option", $this->global, $data, NULL);
        }
    }

    function deleteOption($id_option)
    {
        $inserted_id = $this->question_model->deleteOption($id_option);
        echo "Success";
    }

    function getquestiondetailsbyid() {
        $id = $_POST['id'];
        $questionDetails = $this->question_model->getQuestion($id);

        echo json_encode($questionDetails);
    }


    function updateanswer(){
         $sessionid = session_id();
            $user_id = $this->session->userId;

        $id = $_POST['id'];;
        $tempdata['marks'] = $_POST['qmarks'];
        $tempdata['question'] = $_POST['answer_question'];
        $tempdata['answer_scheme'] = $_POST['answer_scheme'];

        if($id>0) {
        $inserted_id = $this->question_model->editQuestion($tempdata, $id);

        } else {

                    $tempdata['questionId'] = $_POST['questionId'];
                     $dataQuestions = array(
                                'question' => $_POST['answer_question'],
                                'marks' =>  $_POST['qmarks'],
                                'answer_scheme' => $_POST['answer_scheme'],
                                'created_by' => $user_id,
                                'parent_id' => $_POST['questionId']
                            );

                       $this->question_model->addNewQuestion($dataQuestions);

        }


    }

    function updatetempanswer() {

        $sessionid = session_id();

        $tempdata['session_id'] = $sessionid;
        $tempdata['qmarks'] = $_POST['qmarks'];
        $tempdata['answer_question'] = $_POST['answer_question'];
        $tempdata['answer_scheme'] = $_POST['answer_scheme'];

        $inserted_id = $this->question_model->tempsaveDataQuestions($tempdata);


    }

      function deletetempdata() {

        $sessionid = session_id();

        $deleteId = $_POST['id'];

        $inserted_id = $this->question_model->delelteQuestion($deleteId);


    }



     function gettempdata() {

            $sessionid = session_id();

            $listofData = $this->question_model->gettempsaveQuestions($sessionid);

        $table = "<div class='custom-table'>
      <table class='table' id='list-table'><thead><tr> <th>Questions</th><th>Answer Scheme</th><th>Marks</th><th>Action</th></tr></thead><tbody>";
         $totalmarks = 0;
        for($r=0;$r<count($listofData);$r++) {
        $qmarks = $listofData[$r]->qmarks;
        $answer_question = $listofData[$r]->answer_question;
        $answer_scheme = $listofData[$r]->answer_scheme;
        $id = $listofData[$r]->id;
        $totalmarks = $totalmarks + $qmarks;


            $table.="<tr><td>$answer_question</td><td>$answer_scheme</td><td>$qmarks</td><td><a href='javascript:onclick=deletetemp($id)'>Delete</a></td></tr>";
        }
 
         $table.="</tbody></table></div><input type='hidden' id='totalhidenmarks' value='$totalmarks' />";
         echo $table;exit;


    }
}
