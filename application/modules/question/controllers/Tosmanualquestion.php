<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


class Tosmanualquestion extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('tos_model');
        $this->load->model('question_model');
        $this->load->model('examset_model');
        error_reporting(0);

        $this->isLoggedIn();
    }

    function list()
    {
        if ($this->checkAccess('tosmanualquestion.list') == 1) {
            $this->loadAccessRestricted();
        } else {
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['type'] = 2;
            $data['searchParam'] = $formData;
            $data['tosList'] = $this->tos_model->manualtosListSearch($formData);
            $this->global['pageTitle'] = 'Examination Management System : TOS List';
            $this->global['pageCode'] = 'tosmanualquestion.list';
            $this->loadViews("tosmanualquestion/list", $this->global, $data, NULL);
        }
    }


     function getmoduledropdown() {
        $type = $_POST['id'];
       
         $results = $this->examset_model->getCourseListByType($type);

            $table="   
                <script type='text/javascript'>
                     $('select').select2();
                 </script>
         ";

            $table.="
            <select name='id_course' id='id_course' class='form-control'>
                <option value=''>Select</option>
                ";

            for($i=0;$i<count($results);$i++)
            {

            $id = $results[$i]->id;
            $name = $results[$i]->name;
           
            $table.="<option value=".$id.">".$name.
                    "</option>";

            }
            $table.="

            </select>";

            echo $table;
            exit;


    }


    function add()
    {
        if ($this->checkAccess('tosmanualquestion.add') == 1) {
            $this->loadAccessRestricted();
        } else {
            $user_id = $this->session->userId;
            if ($this->input->post()) {

                 $name = $this->security->xss_clean($this->input->post('name'));
                 $code = $this->security->xss_clean($this->input->post('code'));
                 $mcq_question = $this->security->xss_clean($this->input->post('mcq_question'));
                 $essay_question = $this->security->xss_clean($this->input->post('essay_question'));
                 $true_false_question = $this->security->xss_clean($this->input->post('true_false_question'));

                 $mcq_marks = $this->security->xss_clean($this->input->post('mcq_marks'));
                 $essay_marks = $this->security->xss_clean($this->input->post('essay_marks'));
                 $true_false_marks = $this->security->xss_clean($this->input->post('true_false_marks'));


                 $total_marks = $this->security->xss_clean($this->input->post('total_marks'));
                 $question_order = $this->security->xss_clean($this->input->post('question_order'));
                 $name = $this->security->xss_clean($this->input->post('name'));
                $question_count = $this->security->xss_clean($this->input->post('question_count'));
                $status = $this->security->xss_clean($this->input->post('status'));

               
                $main_data = array(
                    'prepared_from'=>'1',
                    'name' => $name,
                    'code' =>$code,
                    'question_count' => $question_count,
                    'status' => $status,
                    'created_by' => $user_id,
                    'type' =>2,
                    'mcq_question' => $mcq_question,
                    'essay_question' => $essay_question,
                    'true_false_question' => $true_false_question,
                    'mcq_marks' => $mcq_marks,
                    'essay_marks' => $essay_marks,
                    'true_false_marks' => $true_false_marks,
                    'total_marks' => $total_marks,
                    'question_order' => $question_order
                );
                $tosid = $this->tos_model->addNewManualTos($main_data);


                redirect('/question/tosmanualquestion/edittos/'.$tosid);
            }
        }
        $data['poolList'] = $this->question_model->poolListByStatus('1');
        $this->global['pageCode'] = 'tosmanualquestion.add';
        $this->global['pageTitle'] = 'Examination Management System : Add TOS';
        $this->loadViews("tosmanualquestion/add", $this->global, $data, NULL);
    }



    function edittos($id,$tosprevious=NULL)
    {
        if ($this->checkAccess('tosmanualquestion.add') == 1) {
            $this->loadAccessRestricted();
        } else {

            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;

            if ($this->input->post()) {

                // print_r($this->input->post());exit;
                if ($_FILES['image_file'])
                {
                    $certificate_name = $_FILES['image_file']['name'];
                    $certificate_size = $_FILES['image_file']['size'];
                    $certificate_tmp = $_FILES['image_file']['tmp_name'];

                    // echo "<Pre>"; print_r($certificate_tmp);exit();

                    $certificate_ext = explode('.', $certificate_name);
                    $certificate_ext = end($certificate_ext);
                    $certificate_ext = strtolower($certificate_ext);


                    $this->fileFormatNSizeValidation($certificate_ext, $certificate_size, 'Image File');

                    $image = $this->uploadFile($certificate_name, $certificate_tmp, 'Image File');
                }



                $question = $this->security->xss_clean($this->input->post('question'));
                $id_pool = $this->security->xss_clean($this->input->post('id_pool'));
                $id_course = $this->security->xss_clean($this->input->post('id_course'));
                $id_topic = $this->security->xss_clean($this->input->post('id_topic'));
                $id_course_learning_objective = $this->security->xss_clean($this->input->post('id_course_learning_objective'));
                $id_bloom_taxonomy = $this->security->xss_clean($this->input->post('id_bloom_taxonomy'));
                $id_difficult_level = $this->security->xss_clean($this->input->post('id_difficult_level'));
                $marks = $this->security->xss_clean($this->input->post('marks'));
                $status = $this->security->xss_clean($this->input->post('status'));
                $question_type = $this->security->xss_clean($this->input->post('question_type'));

                $answer_1 = $this->security->xss_clean($this->input->post('answer_1'));
                $answer_2 = $this->security->xss_clean($this->input->post('answer_2'));
                $answer_3 = $this->security->xss_clean($this->input->post('answer_3'));
                $answer_4 = $this->security->xss_clean($this->input->post('answer_4'));


                $mcq_correct_answer = $this->security->xss_clean($this->input->post('mcq_correct_answer'));
                $true_correct_answer = $this->security->xss_clean($this->input->post('true_correct_answer'));




                $data = array(
                    'question' => $question,
                    'question_type' =>$question_type,
                    'id_pool' => $id_pool,
                    'id_course' => $id_course,
                    'id_topic' => $id_topic,
                    'id_course_learning_objective' => $id_course_learning_objective,
                    'id_bloom_taxonomy' => $id_bloom_taxonomy,
                    'id_difficult_level' => $id_difficult_level,
                    'marks' => $marks,
                    'status' => $status,
                    'created_by' => $user_id,
                    'parent_id' => 0
                );

                if ($image != '') {
                    $data['image'] = $image;
                }

                $result = $question_id = $this->question_model->addNewQuestion($data);

                        $main_data = array(
                            'id_manualtos' => $id,
                            'id_question' => $result
                        );
                    $this->tos_model->addmanualTosQuestion($main_data);



                if($result) {
                    // for MCQ 
                    if($question_type=='1') {

                        $firstCorrectAnswer = 0;
                        if($mcq_correct_answer=='1') {
                            $firstCorrectAnswer = 1;
                        }

                         $datafirstSet = array(
                            'id_question' => $question_id,
                            'option_description' => $answer_1,
                            'is_correct_answer' => $firstCorrectAnswer
                        );
                        $result = $this->question_model->addQuestionHasOption($datafirstSet);


                        $secondCorrectAnswer = 0;
                        if($mcq_correct_answer=='2') {
                            $secondCorrectAnswer = 1;
                        }

                         $datasecondSet = array(
                            'id_question' => $question_id,
                            'option_description' => $answer_2,
                            'is_correct_answer' => $secondCorrectAnswer
                        );
                        $result = $this->question_model->addQuestionHasOption($datasecondSet);

                        $thirdCorrectAnswer = 0;
                        if($mcq_correct_answer=='3') {
                            $thirdCorrectAnswer = 1;
                        }

                         $datathirdSet = array(
                            'id_question' => $question_id,
                            'option_description' => $answer_3,
                            'is_correct_answer' => $thirdCorrectAnswer
                        );
                        $result = $this->question_model->addQuestionHasOption($datathirdSet);

                        $fourCorrectAnswer = 0;
                        if($mcq_correct_answer=='4') {
                            $fourCorrectAnswer = 1;
                        }

                         $datafourthset = array(
                            'id_question' => $question_id,
                            'option_description' => $answer_4,
                            'is_correct_answer' => $fourCorrectAnswer
                        );
                        $result = $this->question_model->addQuestionHasOption($datafourthset);


                    }

                    if($question_type=='3') {

                        $firstCorrectAnswer = 0;
                        if($true_correct_answer=='1') {
                            $firstCorrectAnswer = 1;
                        }

                        $datafirstSet = array(
                            'id_question' => $question_id,
                            'option_description' => 'TRUE',
                            'is_correct_answer' => $firstCorrectAnswer
                        );
                        $result = $this->question_model->addQuestionHasOption($datafirstSet);


                        $secondCorrectAnswer = 0;
                        if($true_correct_answer=='2') {
                            $secondCorrectAnswer = 1;
                        }

                         $datasecondSet = array(
                            'id_question' => $question_id,
                            'option_description' => 'FALSE',
                            'is_correct_answer' => $secondCorrectAnswer
                        );
                        $result = $this->question_model->addQuestionHasOption($datasecondSet);


                    }

                    if($question_type=='2') {
                        $sessionid = session_id();
                        $listofData = $this->question_model->gettempsaveQuestions($sessionid);

                        for($r=0;$r<count($listofData);$r++) {
                            $qmarks = $listofData[$r]->qmarks;
                            $answer_question = $listofData[$r]->answer_question;
                            $answer_scheme = $listofData[$r]->answer_scheme;
                            $totalmarks = $totalmarks + $qmarks;


                            $dataQuestions = array(
                                'question' => $answer_question,
                                'marks' => $qmarks,
                                'answer_scheme' => $answer_scheme,
                                'created_by' => $user_id,
                                'parent_id' => $question_id
                            );

                       $this->question_model->addNewQuestion($dataQuestions);
                    }
                }
            }
                if ($result > 0) {
                    $this->session->set_flashdata('success', 'New Question added successfully');
                } else {
                    $this->session->set_flashdata('error', 'Question add failed');
                }
                redirect('/question/tosmanualquestion/edittos/'.$id.'/1');
            }
            $data['answerStatusList'] = $this->question_model->answerStatusListByStatus('1');

            $data['poolList'] = $this->question_model->poolListByStatus('1');
            $data['courseList'] = $this->question_model->courseListByStatus('1');
            $data['topicList'] = array(); //$this->question_model->topicListByStatus('1');
            $data['courseLearningObjectList'] = $this->question_model->courseLearningObjectListByStatus('1');
            $data['bloomTaxonomyList'] = $this->question_model->bloomTaxonomyListByStatus('1');
            $data['difficultLevelList'] = $this->question_model->difficultLevelListByStatus('1');


            $data['tosid'] = $id;

             $data['tos'] = $this->tos_model->getManualTosById($id);

            $totalAssignedMCQ = 0;
            $totalAssignedEssay = 0;
            $totalAssignedTrue = 0;
            $data['quesiontosdetails'] = $quesiontosdetails = $this->tos_model->getAllQuestionByManualTos($id);

            for($i=0;$i<count($quesiontosdetails);$i++) {

                if($quesiontosdetails[$i]->question_type=='1') {
                    $totalAssignedMCQ = $totalAssignedMCQ+1;
                }
                 if($quesiontosdetails[$i]->question_type=='2') {
                    $totalAssignedEssay = $totalAssignedEssay+1;
                }
                 if($quesiontosdetails[$i]->question_type=='3') {
                    $totalAssignedTrue = $totalAssignedTrue+1;
                }
            }


            if($tosprevious=='1') {

                $result = $this->question_model->getLastQuestion();
                $data['previousdata'] = $result;

                // $previousdata = 
            }

             $data['totalAssignedMCQ']= $totalAssignedMCQ;
            $data['totalAssignedEssay']= $totalAssignedEssay;
            $data['totalAssignedTrue']= $totalAssignedTrue;


// print_R($data);

            $this->global['pageTitle'] = 'Examination Management System : Add Question';
            $this->global['pageCode'] = 'tosmanualquestion.add';

            $this->loadViews("tosmanualquestion/edittos", $this->global, $data, NULL);
        }
    }

    function deleteids() {
        $id = $_POST['id'];
        $this->tos_model->deletemanualtosid($id);
    }

  

    function edit($id = NULL)
    {
        if ($this->checkAccess('question.edit') == 1) {
            $this->loadAccessRestricted();
        } else {
            if ($id == null) {
                redirect('/question/question/list');
            }

            $id_session = $this->session->my_session_id;
            $user_id = $this->session->userId;

            if ($this->input->post())
            {
                
                if ($_FILES['image_file'])
                {
                    $certificate_name = $_FILES['image_file']['name'];
                    $certificate_size = $_FILES['image_file']['size'];
                    $certificate_tmp = $_FILES['image_file']['tmp_name'];
                    $certificate_ext = explode('.', $certificate_name);
                    $certificate_ext = end($certificate_ext);
                    $certificate_ext = strtolower($certificate_ext);


                    $this->fileFormatNSizeValidation($certificate_ext, $certificate_size, 'Image File');

                    $image = $this->uploadFile($certificate_name, $certificate_tmp, 'Image File');
                }


                $question = $this->security->xss_clean($this->input->post('question'));
                $id_pool = $this->security->xss_clean($this->input->post('id_pool'));
                $id_course = $this->security->xss_clean($this->input->post('id_course'));
                $id_topic = $this->security->xss_clean($this->input->post('id_topic'));
                $id_course_learning_objective = $this->security->xss_clean($this->input->post('id_course_learning_objective'));
                $id_bloom_taxonomy = $this->security->xss_clean($this->input->post('id_bloom_taxonomy'));
                $id_difficult_level = $this->security->xss_clean($this->input->post('id_difficult_level'));
                $marks = $this->security->xss_clean($this->input->post('marks'));
                $status = $this->security->xss_clean($this->input->post('status'));
                $question_type = $this->security->xss_clean($this->input->post('question_type'));

                $answer_1 = $this->security->xss_clean($this->input->post('answer_1'));
                $answer_2 = $this->security->xss_clean($this->input->post('answer_2'));
                $answer_3 = $this->security->xss_clean($this->input->post('answer_3'));
                $answer_4 = $this->security->xss_clean($this->input->post('answer_4'));

                $answer_option_1 = $this->security->xss_clean($this->input->post('answer_option_1'));
                $answer_option_2 = $this->security->xss_clean($this->input->post('answer_option_2'));
                $answer_option_3 = $this->security->xss_clean($this->input->post('answer_option_3'));
                $answer_option_4 = $this->security->xss_clean($this->input->post('answer_option_4'));


                $true_option_1 = $this->security->xss_clean($this->input->post('true_option_1'));
                $true_option_2 = $this->security->xss_clean($this->input->post('true_option_2'));



                $mcq_correct_answer = $this->security->xss_clean($this->input->post('mcq_correct_answer'));
                $true_correct_answer = $this->security->xss_clean($this->input->post('true_correct_answer'));


                $data = array(
                    'question' => $question,
                    'id_pool' => $id_pool,
                    'id_course' => $id_course,
                    'id_topic' => $id_topic,
                    'id_course_learning_objective' => $id_course_learning_objective,
                    'id_bloom_taxonomy' => $id_bloom_taxonomy,
                    'id_difficult_level' => $id_difficult_level,
                    'marks' => $marks,
                    'status' => $status,
                    'updated_by' => $user_id,
                    'updated_dt_tm'=>date('Y-m-d H:i:s')                    
                    
                );

                if ($image != '') {
                    $data['image'] = $image;
                }

                $result = $this->question_model->editQuestion($data, $id);
                $data['id_question'] = $id;
                $this->question_model->addQuestionLog($data);
                if($result) {
                    if($question_type=='1') {

                        $firstCorrectAnswer = 0;
                        if($mcq_correct_answer=='1') {
                            $firstCorrectAnswer = 1;
                        }

                         $datafirstSet = array(
                            'option_description' => $answer_1,
                            'is_correct_answer' => $firstCorrectAnswer
                        );
                        $result = $this->question_model->editQuestionHasOption($datafirstSet,$answer_option_1);


                        $secondCorrectAnswer = 0;
                        if($mcq_correct_answer=='2') {
                            $secondCorrectAnswer = 1;
                        }

                         $datasecondSet = array(
                            'option_description' => $answer_2,
                            'is_correct_answer' => $secondCorrectAnswer
                        );
                        $result = $this->question_model->editQuestionHasOption($datasecondSet,$answer_option_2);

                        $thirdCorrectAnswer = 0;
                        if($mcq_correct_answer=='3') {
                            $thirdCorrectAnswer = 1;
                        }

                         $datathirdSet = array(
                            'option_description' => $answer_3,
                            'is_correct_answer' => $thirdCorrectAnswer
                        );
                        $result = $this->question_model->editQuestionHasOption($datathirdSet,$answer_option_3);

                        $fourCorrectAnswer = 0;
                        if($mcq_correct_answer=='4') {
                            $fourCorrectAnswer = 1;
                        }

                         $datafourthset = array(
                            'option_description' => $answer_4,
                            'is_correct_answer' => $fourCorrectAnswer
                        );
                        $result = $this->question_model->editQuestionHasOption($datafourthset,$answer_option_4);


                    }


                     if($question_type=='3') {

                          $firstCorrectAnswer = 0;
                        if($true_correct_answer=='1') {
                            $firstCorrectAnswer = 1;
                        }

                         $datafirstSet = array(
                            'option_description' => 'TRUE',
                            'is_correct_answer' => $firstCorrectAnswer
                        );
                        $result = $this->question_model->editQuestionHasOption($datafirstSet,$true_option_1);


                        $secondCorrectAnswer = 0;
                        if($true_correct_answer=='2') {
                            $secondCorrectAnswer = 1;
                        }

                         $datasecondSet = array(
                            'option_description' => 'FALSE',
                            'is_correct_answer' => $secondCorrectAnswer
                        );
                        $result = $this->question_model->editQuestionHasOption($datasecondSet,$true_option_2);


                    }

                }
                if ($result) {
                    $this->session->set_flashdata('success', 'Question edited successfully');
                } else {
                    $this->session->set_flashdata('error', 'Question edit failed');
                }
                redirect('/question/question/list');
            }

            $question = $data['question'] = $this->question_model->getQuestion($id);

            $data['questionHasOption'] = $this->question_model->getQuestionHasOption($id);

            $data['poolList'] = $this->question_model->poolListByStatus('1');
            $data['courseList'] = $this->question_model->courseListByStatus('1');
            $data['topicList'] = $this->question_model->topicListByCourse($data['question']->id_course);
            $data['courseLearningObjectList'] = $this->question_model->courseLearningObjectListByStatus('1');
            $data['bloomTaxonomyList'] = $this->question_model->bloomTaxonomyListByStatus('1');
            $data['difficultLevelList'] = $this->question_model->difficultLevelListByStatus('1');
            $data['answerStatusList'] = $this->question_model->answerStatusListByStatus('1');



            $data['questionList'] = $this->question_model->getChildQuestionByParent($question->id);

            for($k=0;$k<count($data['questionHasOption']);$k++) {
                if($data['questionHasOption'][$k]->is_correct_answer=='1') {
                    $data['correctAnswer'] = $k+1;
                }
            }


            $this->global['pageTitle'] = 'Examination Management System : Edit Question';
            $this->global['pageCode'] = 'tosmanualquestion.edit';

            $this->loadViews("tosmanualquestion/edit", $this->global, $data, NULL);
        }
    }


     function view($id = NULL)
    {
        if ($this->checkAccess('tosmanual.edit') == 1) {
            $this->loadAccessRestricted();
        } else {
            if ($id == null) {
                redirect('/setup/tos/list');
            }

            $formData['id_pool'] = $this->security->xss_clean($this->input->post('id_pool'));
            $formData['id_course'] = $this->security->xss_clean($this->input->post('id_course'));
            $formData['id_topic'] = $this->security->xss_clean($this->input->post('id_topic'));
            $formData['id_course_learning_objective'] = $this->security->xss_clean($this->input->post('id_course_learning_objective'));
            $formData['id_bloom_taxonomy'] = $this->security->xss_clean($this->input->post('id_bloom_taxonomy'));
            $formData['id_difficult_level'] = $this->security->xss_clean($this->input->post('id_difficult_level'));
            $data['searchParam'] = $formData;

           
            $data['poolList'] = $this->question_model->poolListByStatus('1');
            $data['courseList'] = $this->question_model->courseListByStatus('1');
            $data['topicList'] = $this->question_model->topicListByStatus('1');
            $data['courseLearningObjectList'] = $this->question_model->courseLearningObjectListByStatus('1');
            $data['bloomTaxonomyList'] = $this->question_model->bloomTaxonomyListByStatus('1');
            $data['difficultLevelList'] = $this->question_model->difficultLevelListByStatus('1');         

  $data['tosid'] = $id;

             $data['tos'] = $this->tos_model->getManualTosById($id);

            $totalAssignedMCQ = 0;
            $totalAssignedEssay = 0;
            $totalAssignedTrue = 0;

$totalAssignedMCQ = 0;
            $totalAssignedEssay = 0;
            $totalAssignedTrue = 0;
            $data['quesiontosdetails'] = $quesiontosdetails = $this->tos_model->getAllQuestionByManualTos($id);

            for($i=0;$i<count($quesiontosdetails);$i++) {

                if($quesiontosdetails[$i]->question_type=='1') {
                    $totalAssignedMCQ = $totalAssignedMCQ+1;
                }
                 if($quesiontosdetails[$i]->question_type=='2') {
                    $totalAssignedEssay = $totalAssignedEssay+1;
                }
                 if($quesiontosdetails[$i]->question_type=='3') {
                    $totalAssignedTrue = $totalAssignedTrue+1;
                }
            }

             $data['totalAssignedMCQ']= $totalAssignedMCQ;
            $data['totalAssignedEssay']= $totalAssignedEssay;
            $data['totalAssignedTrue']= $totalAssignedTrue;


            $user_id = $this->session->userId;
            $data['tosid'] = $id;

            $data['quesiontosdetails'] = $this->tos_model->getAllQuestionByManualTos($id);

            $this->global['pageCode'] = 'tosmanualquestion.edit';
            $this->global['pageTitle'] = 'Examination Management System : Edit TOS';
            $this->loadViews("tosmanualquestion/view", $this->global, $data, NULL);
        }
    }
}
