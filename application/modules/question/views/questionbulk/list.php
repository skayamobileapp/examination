<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
  <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
    <h1 class="h3">Question Bulk Upload</h1>
  </div>
  <div class="page-container">
    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="row">


        <div class="col-sm-12">
          
        </div>
        <div class="col-sm-12">
  <form id="form_main" action="" method="post" enctype="multipart/form-data">

          <div id="collapseOne" class="panel-collapse collapse show" role="tabpanel" aria-labelledby="headingOne" aria-expanded="true" style="">
            <div class="panel-body p-2">

               <div class="row">

          <div class="col-lg-6">
            <div class="form-group row">
              <label class="col-sm-4 col-form-label">Question Pool <span class="text-danger">*</span></label>
              <div class="col-sm-8">
                <select name="id_pool" id="id_pool" class="form-control">
                  <option value="">Select</option>
                  <?php
                  if (!empty($poolList)) {
                    foreach ($poolList as $record) { ?>
                      <option value="<?php echo $record->id;  ?>">
                        <?php echo $record->name;  ?>
                      </option>
                  <?php
                    }
                  }
                  ?>
                </select>
              </div>
            </div>
          </div>

          <div class="col-lg-6">
            <div class="form-group row">
              <label class="col-sm-4 col-form-label">Module <span class="text-danger">*</span></label>
              <div class="col-sm-8">
                <select name="id_course" id="id_course" class="form-control">
                  <option value="">Select</option>
                  <?php
                  if (!empty($courseList)) {
                    foreach ($courseList as $record) { ?>
                      <option value="<?php echo $record->id;  ?>">
                        <?php echo $record->code.' - '.$record->name;  ?>
                      </option>
                  <?php
                    }
                  }
                  ?>
                </select>
              </div>
            </div>
          </div>


        </div>

              <div class="row">
                <div class="col-lg-6">
            <div class="form-group row">
              <label class="col-sm-4 col-form-label">Question Type <span class="text-danger">*</span></label>
              <div class="col-sm-8">
                <select name="question_type" id="question_type" class="form-control" >
                  <option value="">Select</option>
                  <option value="1">MCQ</option>
                  <option value="2">Essay</option>
                  <option value="3">True / False</option>                 
                </select>
              </div>
            </div>
          </div>

                <div class="col-lg-6">
                  <div class="form-group row">
                    <label class="col-sm-4 col-form-label">Upload File</label>
                    <div class="col-sm-8">
                     <input type="file" name='file' id='file'>
                    </div>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-lg-6">
                  <div class="form-group row">
                    <label class="col-sm-4 col-form-label">Download Template</label>
                    <div class="col-sm-8">
                     <a href="/assets/question_format.csv">Click here to Download</a>
                    </div>
                  </div>
                </div>
              </div>

              <hr />
              <div class="d-flex justify-content-center">
                <button type="submit" class="btn btn-primary">Save</button>
              </div>
            </div>
          </div>

        </form>
        </div>

      </div>
    </div>
    <?php
    if ($this->session->flashdata('success')) {
    ?>
      <div class="alert alert-success alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
      </div>
    <?php
    }
    if ($this->session->flashdata('error')) {
    ?>
      <div class="alert alert-danger alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Failure!</strong> <?php echo $this->session->flashdata('error'); ?>
      </div>
    <?php
    }
    ?>

     <?php
          if (!empty($questionList)) {
            $i = 1; ?>
    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Sl. No</th>
            <th>Question Pool</th>
            <th>Question </th>
            <th>Marks </th>
            <th>Created By</th>
            <th>Updated By</th>
            <th>Created Date</th>
            <th>Updated Date</th>
            <th class="text-center">Status</th>
            <th style="text-align: center; width: 100px;">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php 
         
            foreach ($questionList as $record) {
          ?>
              <tr>
                <td><?php echo $i ?></td>
                <td><?php echo $record->PoolName; ?></td>
                <td><?php echo substr($record->question, 0, 50);; ?></td>
                <td><?php echo $record->marks; ?></td>
                <td><?php echo $record->created_by; ?></td>
                <td><?php echo $record->updated_by; ?></td>
                <td><?php if($record->created_dt_tm){ echo date('d-m-Y', strtotime($record->created_dt_tm)); } ?></td>
                <td><?php if($record->updated_dt_tm){ echo date('d-m-Y', strtotime($record->updated_dt_tm)); } ?></td>
                <td style="text-align: center;">
                  <?php if ($record->status == '1')
                  {
                    echo "Active";
                  } else {
                    echo "In-Active";
                  }
                  ?>
                </td>
                <td class="text-center">

                  <a href="<?php echo 'edit/' . $record->id; ?>" class="btn" type="" data-toggle="tooltip" data-placement="top" title="Edit">
                    <i class="fa fa-pencil-square-o" aria-hidden="true">
                    </i>
                  </a>

                </td>
              </tr>
          <?php
              $i++;
            }
          
          ?>
        </tbody>
      </table>
    </div>
  </div>
    <?php
          }
          ?>
</main>
<script>
  $('select').select2();

  function clearSearchForm() {
    window.location.reload();
  }
</script>
<script type="text/javascript">
        $(document).ready(function(){
 
            $('#id_course').change(function(){ 
                var id=$(this).val();
                $.ajax({
                    url : "<?php echo site_url('setup/ajax/getCategoryTopics');?>",
                    method : "POST",
                    data : {id: id},
                    async : true,
                    dataType : 'json',
                    success: function(data){
                         
                        var html = '';
                        var i;
                        for(i=0; i<data.length; i++){
                            html += '<option value='+data[i].id+'>'+data[i].name+'-'+data[i].name+'</option>';
                        }
                        $('#id_topic').html(html);
 
                    }
                });
                return false;
            }); 
             
        });
    </script>