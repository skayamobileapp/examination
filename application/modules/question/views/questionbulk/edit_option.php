<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
        <h1 class="h3">Edit Answer</h1>
        
        <a href="<?php echo '../../addOptions/' . $id_question; ?>" class="btn btn-link ml-auto">
            <i class="fa fa-chevron-left" aria-hidden="true"></i>
          Back
        </a>

    </div>
    

        <div class="page-container">

          <div>
            <h4 class="form-title">Question Details</h4>
          </div>

            <div class="form-container">
                 <div class="row">
                    <div class="col-lg-12">
                      <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Question<span class="text-danger">*</span></label>
                        <div class="col-sm-10">
                         <textarea name="question" id="question" readonly="readonly"><?php echo $question->question; ?></textarea>
                        </div> 
                      </div>
                    </div>
                  </div>      
            </div>  

              <div>
            
          </div>

          <h4 class="form-title">Answer Details</h4>
            <div class="form-container">
                        <form id="form_comitee" action="" method="post" enctype="multipart/form-data">

                                <div class="row">

                                    <div class="col-sm-9">
                                        <div class="form-group">
                                            <label>Answer <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="option_description" name="option_description" value="<?php echo $questionHasOption->option_description; ?>">
                                        </div>
                                    </div>



                                </div>

                                <br>

                                <div class="row">

                                    <div class="col-lg-6">
                                      <div class="form-group row">
                                        <label class="col-sm-4 col-form-label">Status <span class="text-danger">*</span></label>
                                        <div class="col-sm-8">
                                          <select name="is_correct_answer" id="is_correct_answer" class="form-control">
                                            <option value="">Select</option>
                                            <?php
                                            if (!empty($answerStatusList)) {
                                                foreach ($answerStatusList as $record) {
                                            ?>
                                            
                                                <option value="<?php echo $record->id;  ?>"
                                                  <?php 
                                                  if($questionHasOption->is_correct_answer == $record->id)
                                                  {
                                                    echo "selected";
                                                  } ?>
                                                  >
                                                    <?php echo $record->name;  ?>        
                                                </option>

                                            <?php
                                                }
                                            }
                                            ?>

                                          </select>
                                        </div>
                                      </div>
                                    </div>


                                    <!-- <div class="col-lg-2">
                                      <div class="form-group row align-items-center">
                                        <label class="col-sm-4 col-form-label">Is Correct Answer <span class="text-danger">*</span></label>
                                        <div class="col-sm-8">
                                          <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="customRadioInline1" name="is_correct_answer" class="custom-control-input" value="0" <?php if($questionHasOption->is_correct_answer == 0){
                                                echo "checked=checked";
                                            } ?> >
                                            <label class="custom-control-label" for="customRadioInline1">No</label>
                                          </div>
                                          <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="customRadioInline2" name="is_correct_answer" class="custom-control-input" value="1" <?php if($questionHasOption->is_correct_answer == 1){
                                                echo "checked=checked";
                                            } ?> >
                                            <label class="custom-control-label" for="customRadioInline2">Yes</label>
                                          </div>
                                        </div>
>>>>>>> 1fdce6416dbd1949f7d47ee7aa6ebeee9cc3d643
                                      </div>
                                    </div> -->


                                </div>

<!-- <<<<<<< HEAD
                          <button type="submit" class="btn btn-primary form-row-btn mb-3">Add</button>
======= -->
                                    <div class="col-lg-6">
                                      <div class="form-group row">
                                        <label class="col-sm-4 col-form-label"></label>
                                        <div class="col-sm-8">
                                        <button type="submit" class="btn btn-primary form-row-btn">Add</button>
                                        </div>
                                      </div>
                                    </div>
                                    
                                  </div>
<!-- >>>>>>> 1fdce6416dbd1949f7d47ee7aa6ebeee9cc3d643 -->

                        </form>

            </div>


        </div>
</main>

<script type="text/javascript" src="<?php echo BASE_PATH; ?>assets/ckeditor/ckeditor.js"></script>


<script>
  $(document).ready(function() {
    CKEDITOR.replace( 'question' );
  });


    function deleteOption(id)
    {
            // alert(id);
            $.ajax(
            {
               url: '/setup/question/deleteOption/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    // alert(result);
                    window.location.reload();
               }
            });
    }



    $(document).ready(function()
    {
        $("#form_comitee").validate(
        {
            rules:
            {
                option_description:
                {
                    required: true
                },
                is_correct_answer:
                {
                    required: true
                }
            },
            messages:
            {
                option_description:
                {
                    required: "<p class='error-text'>Option Required</p>",
                },
                is_correct_answer:
                {
                    required: "<p class='error-text'>Select Status</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
<script type="text/javascript">

    $('select').select2();

    function reloadPage()
    {
      window.location.reload();
    }
</script>