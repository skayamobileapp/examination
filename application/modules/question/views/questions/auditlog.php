<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
  <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
    <h1 class="h3">Question List</h1>

    <a href='../list' class="btn btn-link ml-auto">
      <i class="fa fa-chevron-left" aria-hidden="true"></i>
      Back
    </a>
  </div>
  <div class="page-container">

    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Sl. No</th>
            <th>Question Pool</th>
            <th>Question </th>
            <th>Frequency </th>
            <th>Marks </th>
            <th>Created By</th>
            <th>Updated By</th>
            <th>Created Date</th>
            <th>Updated Date</th>
            <th class="text-center">Status</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($questionList)) {
                    $this->load->model('question_model');

            $i = 1;
            foreach ($questionList as $record) {
              $questionCount = 0;


              $questionSql = $this->question_model->getFrequency($record->id);


              $questionCount = $questionSql[0]->totalCount;
          ?>
              <tr>
                <td><?php echo $i ?></td>
                <td><?php echo $record->PoolName; ?></td>
                <td><?php echo substr($record->question, 0, 50);; ?></td>
                <td><?php echo $questionCount;?></td>
                <td><?php echo $record->marks; ?></td>
                <td><?php echo $record->created_by; ?></td>
                <td><?php echo $record->updated_by; ?></td>
                <td><?php if($record->created_dt_tm){ echo date('d-m-Y', strtotime($record->created_dt_tm)); } ?></td>
                <td><?php if($record->updated_dt_tm){ echo date('d-m-Y', strtotime($record->updated_dt_tm)); } ?></td>
                <td style="text-align: center;">
                  <?php if ($record->status == '1')
                  {
                    echo "Active";
                  } else {
                    echo "In-Active";
                  }
                  ?>
                </td>
               
              </tr>
          <?php
              $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
</main>
<script>
  $('select').select2();

  function clearSearchForm() {
    window.location.reload();
  }
</script>
<script type="text/javascript">
        $(document).ready(function(){
 
            $('#id_course').change(function(){ 
                var id=$(this).val();
                $.ajax({
                    url : "<?php echo site_url('question/ajax/getCategoryTopics');?>",
                    method : "POST",
                    data : {id: id},
                    async : true,
                    dataType : 'json',
                    success: function(data){
                         
                        var html = '';
                        var i;
                        for(i=0; i<data.length; i++){
                            html += '<option value='+data[i].id+'>'+data[i].name+'-'+data[i].name+'</option>';
                        }
                        $('#id_topic').html(html);
 
                    }
                });
                return false;
            }); 
             
        });
    </script>