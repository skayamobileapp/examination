<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
  <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
    <h1 class="h3">Question List</h1>
    <a href="add" class="btn btn-primary ml-auto">+ Add Question</a>
  </div>
  <div class="page-container">
    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="">
              Advanced Search
              <i class="fa fa-angle-right" aria-hidden="true"></i>
              <i class="fa fa-angle-down" aria-hidden="true"></i>
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">

          <div id="collapseOne" class="panel-collapse collapse show" role="tabpanel" aria-labelledby="headingOne" aria-expanded="true" style="">
            <div class="panel-body p-2">

              <div class="row">

                <div class="col-lg-6">
                  <div class="form-group row">
                    <label class="col-sm-4 col-form-label">Question Pool</label>
                    <div class="col-sm-8">
                      <select name="id_pool" id="id_pool" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($poolList)) {
                          foreach ($poolList as $record) { ?>
                            <option value="<?php echo $record->id;  ?>" <?php if ($searchParam['id_pool'] == $record->id) {
                                                                          echo "selected";
                                                                        } ?>>
                              <?php echo $record->code . " - " . $record->name;  ?>
                            </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>
                
                <div class="col-lg-6">
                  <div class="form-group row">
                    <label class="col-sm-4 col-form-label">Module</label>
                    <div class="col-sm-8">
                      <select name="id_course" id="id_course" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($courseList)) {
                          foreach ($courseList as $record) { ?>
                            <option value="<?php echo $record->id;  ?>" <?php if ($searchParam['id_course'] == $record->id) {
                                                                          echo "selected";
                                                                        } ?>>
                              <?php echo $record->code . " - " . $record->name;  ?>
                            </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="form-group row">
                    <label class="col-sm-4 col-form-label">Chapter</label>
                    <div class="col-sm-8">
                      <select name="id_topic" id="id_topic" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($topicList)) {
                          foreach ($topicList as $record) { ?>
                            <option value="<?php echo $record->id;  ?>" <?php if ($searchParam['id_topic'] == $record->id) {
                                                                          echo "selected";
                                                                        } ?>>
                              <?php echo $record->code . " - " . $record->name;  ?>
                            </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="form-group row">
                    <label class="col-sm-4 col-form-label">Difficulty Level</label>
                    <div class="col-sm-8">
                      <select name="id_bloom_taxonomy" id="id_bloom_taxonomy" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($bloomTaxonomyList)) {
                          foreach ($bloomTaxonomyList as $record) { ?>
                            <option value="<?php echo $record->id;  ?>" <?php if ($searchParam['id_bloom_taxonomy'] == $record->id) {
                                                                          echo "selected";
                                                                        } ?>>
                              <?php echo $record->code . " - " . $record->name;  ?>
                            </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>


              </div>

              <hr />
              <div class="d-flex justify-content-center">
                <button type="submit" class="btn btn-primary">Search</button>
                <a href="/records/reports/mostQuestions" class="btn btn-link">Clear All Fields</a>
              </div>
            </div>
          </div>

        </form>

      </div>
    </div>
    <?php
    if ($this->session->flashdata('success')) {
    ?>
      <div class="alert alert-success alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
      </div>
    <?php
    }
    if ($this->session->flashdata('error')) {
    ?>
      <div class="alert alert-danger alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Failure!</strong> <?php echo $this->session->flashdata('error'); ?>
      </div>
    <?php
    }
    ?>
    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Sl. No</th>
            <th>Question Pool</th>
            <th>Question Type</th>
            <th>Question </th>
            <th>Frequency </th>
            <th>Marks </th>
            <th>Created By</th>
            <th>Updated By</th>
            <th>Created Date</th>
            <th>Updated Date</th>
            <th class="text-center">Status</th>
            <th style="text-align: center; width: 100px;">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($questionList)) {
                    $this->load->model('question_model');

            $j = 1;
            foreach ($questionList as $record) {
              $questionCount = 0;


              $questionSql = $this->question_model->getFrequency($record->id);



              if($record->question_type=='2') {
              $childQuestion = $this->question_model->getChildQuestionByParent($record->id);


              $totalcountofChildquestion = count($childQuestion);



            }



              $questionCount = $questionSql[0]->totalCount;
          ?>
              <tr>
                <td><?php echo $j ?></td>
                <td><?php echo $record->PoolName; ?></td>

                 <?php if ($record->question_type != '2') { ?> 
                <td><?php echo substr($record->question, 0, 50);; ?></td>
                 <?php } ?> 
                    <?php if ($record->question_type == '2') { ?> 
                <td>
                  <ul>
                  <?php for($i=0;$i<count($childQuestion);$i++){?>
                    <li><?php echo substr($childQuestion[$i]->question, 0, 50);; ?></li>

                  <?php } ?> 
                </ul>
                </td>
                 <?php } ?> 

                 <td style="text-align: center;">
                  <?php if ($record->question_type == '1')
                  {
                    echo "MCQ";
                  } else if ($record->question_type == '2')
                  {
                    echo "ESSAY";
                  } else {
                    echo "TRUE / FALSE";
                  }
                  ?>
                </td>


                <td><?php echo $questionCount;?></td>
                <td><?php echo $record->marks; ?></td>
                <td><?php echo $record->created_by; ?></td>
                <td><?php echo $record->updated_by; ?></td>
                <td><?php if($record->created_dt_tm){ echo date('d-m-Y', strtotime($record->created_dt_tm)); } ?></td>
                <td><?php if($record->updated_dt_tm){ echo date('d-m-Y', strtotime($record->updated_dt_tm)); } ?></td>
                <td style="text-align: center;">
                  <?php if ($record->status == '1')
                  {
                    echo "Active";
                  } else {
                    echo "In-Active";
                  }
                  ?>
                </td>
                <td class="text-center">

                  <a href="<?php echo 'edit/' . $record->id; ?>" class="btn" type="" data-toggle="tooltip" data-placement="top" title="Edit">
                    <i class="fa fa-pencil-square-o" aria-hidden="true">
                    </i>
                  </a>

                </td>
                 
              </tr>
          <?php
              $j++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
</main>
<script>
  $('select').select2();

  function clearSearchForm() {
    window.location.reload();
  }
</script>
<script type="text/javascript">
        $(document).ready(function(){
 
            $('#id_course').change(function(){ 
                var id=$(this).val();
                $.ajax({
                    url : "<?php echo site_url('question/ajax/getCategoryTopics');?>",
                    method : "POST",
                    data : {id: id},
                    async : true,
                    dataType : 'json',
                    success: function(data){
                         
                        var html = '';
                        var i;
                        for(i=0; i<data.length; i++){
                            html += '<option value='+data[i].id+'>'+data[i].name+'-'+data[i].name+'</option>';
                        }
                        $('#id_topic').html(html);
 
                    }
                });
                return false;
            }); 
             
        });
    </script>