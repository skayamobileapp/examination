<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
  <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
    <h1 class="h3">Add Question</h1>

    <a href='list' class="btn btn-link ml-auto">
      <i class="fa fa-chevron-left" aria-hidden="true"></i>
      Back
    </a>

  </div>

  <form id="form_main" action="" method="post" enctype="multipart/form-data">

    <div class="page-container">

      <div>
        <h4 class="form-title">Question details</h4>
      </div>

      <div class="form-container">


        <div class="row">

          <div class="col-lg-6">
            <div class="form-group row">
              <label class="col-sm-4 col-form-label">Question Pool <span class="text-danger">*</span></label>
              <div class="col-sm-8">
                <select name="id_pool" id="id_pool" class="form-control">
                  <option value="">Select</option>
                  <?php
                  if (!empty($poolList)) {
                    foreach ($poolList as $record) { ?>
                      <option value="<?php echo $record->id;  ?>">
                        <?php echo $record->name;  ?>
                      </option>
                  <?php
                    }
                  }
                  ?>
                </select>
              </div>
            </div>
          </div>

          <div class="col-lg-6">
            <div class="form-group row">
              <label class="col-sm-4 col-form-label">Module <span class="text-danger">*</span></label>
              <div class="col-sm-8">
                <select name="id_course" id="id_course" class="form-control">
                  <option value="">Select</option>
                  <?php
                  if (!empty($courseList)) {
                    foreach ($courseList as $record) { ?>
                      <option value="<?php echo $record->id;  ?>">
                        <?php echo $record->code.' - '.$record->name;  ?>
                      </option>
                  <?php
                    }
                  }
                  ?>
                </select>
              </div>
            </div>
          </div>


        </div>


        <div class="row">


          <div class="col-lg-6">
            <div class="form-group row">
              <label class="col-sm-4 col-form-label">Topic <span class="text-danger">*</span></label>
              <div class="col-sm-8">
                <select name="id_topic" id="id_topic" class="form-control">
                  <option value="">Select</option>
                  <?php
                  if (!empty($topicList)) {
                    foreach ($topicList as $record) { ?>
                      <option value="<?php echo $record->id;  ?>">
                        <?php echo $record->name;  ?>
                      </option>
                  <?php
                    }
                  }
                  ?>
                </select>
              </div>
            </div>
          </div>


             <div class="col-lg-6">
            <div class="form-group row">
              <label class="col-sm-4 col-form-label">Question Type <span class="text-danger">*</span></label>
              <div class="col-sm-8">
                <select name="question_type" id="question_type" class="form-control" onchange="showoptiondetails(this.value)">
                  <option value="">Select</option>
                  <option value="1">MCQ</option>
                  <option value="2">Essay</option>
                  <option value="3">True / False</option>                 
                </select>
              </div>
            </div>
          </div>

        </div>

        <div class="row">

          <div class="col-lg-6">
            <div class="form-group row">
              <label class="col-sm-4 col-form-label">Difficulty Level <span class="text-danger">*</span></label>
              <div class="col-sm-8">
                <select name="id_bloom_taxonomy" id="id_bloom_taxonomy" class="form-control">
                  <option value="">Select</option>
                  <?php
                  if (!empty($bloomTaxonomyList)) {
                    foreach ($bloomTaxonomyList as $record) { ?>
                      <option value="<?php echo $record->id;  ?>">
                        <?php echo $record->code;  ?>
                      </option>
                  <?php
                    }
                  }
                  ?>
                </select>
              </div>
            </div>
          </div>
  <div class="col-lg-6" id='divMarks'>
            <div class="form-group row">
              <label class="col-sm-4 col-form-label">Marks <span class="text-danger">*</span></label>
              <div class="col-sm-8">
                <input type="number" class="form-control" id="marks" name="marks" min="1">
              </div>
            </div>
          </div>
         <!--  <div class="col-lg-6">
            <div class="form-group row">
              <label class="col-sm-4 col-form-label">Difficult Level <span class="text-danger">*</span></label>
              <div class="col-sm-8">
                <select name="id_difficult_level" id="id_difficult_level" class="form-control">
                  <option value="">Select</option>
                  <?php
                  if (!empty($difficultLevelList)) {
                    foreach ($difficultLevelList as $record) { ?>
                      <option value="<?php echo $record->id;  ?>">
                        <?php echo $record->code . " - " . $record->name;  ?>
                      </option>
                  <?php
                    }
                  }
                  ?>
                </select>
              </div>
            </div>
          </div> -->

        </div>


        <div class="row">


          <div class="col-lg-6">
            <div class="form-group row">
              <label class="col-sm-4 col-form-label">Upload Image</label>
              <div class="col-sm-8">
                <input type="file" class="form-control" id="image_file" name="image_file">
              </div>
            </div>
          </div>


     
          <div class="col-lg-6">
            <div class="form-group row align-items-center">
              <label class="col-sm-4 col-form-label">Status <span class="text-danger">*</span></label>
              <div class="col-sm-8">
                 <select name="status" id="status" class="form-control" required="required">
                                            <option value="">Select</option>
                                            <?php
                                            if (!empty($answerStatusList)) {
                                                foreach ($answerStatusList as $record) {
                                            ?>

                                                <option value="<?php echo $record->id;  ?>">
                                                    <?php echo $record->name;  ?>        
                                                </option>

                                            <?php
                                                }
                                            }
                                            ?>
                                          </select>
              </div>
            </div>
          </div>

          

        </div>

          <div class="row">
          <div class="col-lg-12">
            <div class="form-group row">
             <!--  <label class="col-sm-2 col-form-label" id="essaydivQuestion">Breif about paragraph  </label> -->

              <label class="col-sm-2 col-form-label" id="essaydivQuestiontrue">Question</label>


              <div class="col-sm-10">
               <textarea name="question" id="question"></textarea>
              </div> 
            </div>
          </div>
        </div>
        

<div id="mcqanswerDiv" style="display: none">

      


          <div class="row">
          <div class="col-lg-12">
            <div class="form-group row">
              <label class="col-sm-2 col-form-label">Answer 1<span class="text-danger">*</span></label>
              <div class="col-sm-10">
               <textarea name="answer_1" id="answer_1"></textarea>
              </div> 
            </div>
          </div>
        </div>

          <div class="row">
          <div class="col-lg-12">
            <div class="form-group row">
              <label class="col-sm-2 col-form-label">Answer 2<span class="text-danger">*</span></label>
              <div class="col-sm-10">
               <textarea name="answer_2" id="answer_2"></textarea>
              </div> 
            </div>
          </div>
        </div>

          <div class="row">
          <div class="col-lg-12">
            <div class="form-group row">
              <label class="col-sm-2 col-form-label">Answer 3<span class="text-danger">*</span></label>
              <div class="col-sm-10">
               <textarea name="answer_3" id="answer_3"></textarea>
              </div> 
            </div>
          </div>
        </div>

          <div class="row">
          <div class="col-lg-12">
            <div class="form-group row">
              <label class="col-sm-2 col-form-label">Answer 4<span class="text-danger">*</span></label>
              <div class="col-sm-10">
               <textarea name="answer_4" id="answer_4" style="height: 100px;"></textarea>
              </div> 
            </div>
          </div>
        </div>
         <div class="row">
          <div class="col-lg-12">
            <div class="form-group row">
              <label class="col-sm-2 col-form-label">Correct Answer<span class="text-danger">*</span></label>
             
              <div class="col-sm-4">
                <select name="mcq_correct_answer" id="mcq_correct_answer" class="form-control">
                  <option value="1">Answer 1</option>
                  <option value="2">Answer 2</option>
                  <option value="3">Answer 3</option>
                  <option value="4">Answer 4</option>                 
                </select>
              </div>
            </div>
              </div> 
            </div>

</div>
<div id="correctAnswerDiv" style="display: none">

  <div class="row">
          <div class="col-lg-12">
            <div class="form-group row">
              <label class="col-sm-2 col-form-label">Option 1<span class="text-danger">*</span></label>
              <div class="col-sm-4">
                 <input type="text" class="form-control" id="" name="" value="TRUE" readonly="readonly">
              </div> 
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-lg-12">
            <div class="form-group row">
              <label class="col-sm-2 col-form-label">Option 2<span class="text-danger">*</span></label>
              <div class="col-sm-4">
                 <input type="text" class="form-control" id="" name="" value="FALSE" readonly="readonly">
              </div> 
            </div>
          </div>
        </div>

              <div class="row">
          <div class="col-lg-12">
            <div class="form-group row">
              <label class="col-sm-2 col-form-label">Correct Answer<span class="text-danger">*</span></label>
             
              <div class="col-sm-4">
                <select name="true_correct_answer" id="true_correct_answer" class="form-control">
                  <option value="1">True</option>
                  <option value="2">False</option>             
                </select>
              </div>
            </div>
              </div> 
            </div>
</div>


<div id="essayanswerDiv" style="display: none">

              <div class="row">
              <div class="col-lg-12">
                <div class="form-group">
                   <button id="myBtn" class="btn btn-primary"  type="button" data-toggle="modal" data-target="#myModal">Add Sub Question</button>
                </div>
              </div> 
            </div>
            <div class="row">
              <div class="col-lg-12">
                <div id="appendTable"></div>
              </div>
            </div>




         
</div>





      </div>
      <div class="button-block clearfix">
        <div class="bttn-group">
          <button type="submit" class="btn btn-primary">Save</button>
          <a href='add' class="btn btn-link">Clear All Fields</a>
        </div>

      </div>
    </div>
  </form>

  <!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                 <h4 class="modal-title" id="myModalLabel">Enter Sub Question</h4>

            </div>
            <div class="modal-body">
              <div class="row">


                <div class="col-lg-6">
                  <div class="form-group row">
                    <label class="col-sm-4 col-form-label"> Image For reference</label>
                    <div class="col-sm-8">
                      <input type="file" class="form-control" id="answer_image" name="answer_image">
                    </div>
                  </div>
                </div>


                <div class="col-lg-6">
                  <div class="form-group row">
                    <label class="col-sm-4 col-form-label">Marks <span class="text-danger">*</span></label>
                    <div class="col-sm-8">
                      <input type="number" class="form-control" id="answer_marks" name="answer_marks" min="1">
                    </div>
                  </div>
                </div>

              </div>


              
              <div class="row">
                <div class="col-lg-12">
                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Question<span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                     <textarea name="answer_question" id="answer_question"></textarea>
                    </div> 
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-lg-12">
                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Answer Scheme<span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                     <textarea name="answer_scheme" id="answer_scheme"></textarea>
                    </div> 
                  </div>
                </div>
              </div>
            </div>
              

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" onclick="return savetempdata()">Save changes</button>
            </div>
        </div>
    </div>
</div>

</main>
<script type="text/javascript" src="<?php echo BASE_PATH; ?>assets/ckeditor/ckeditor.js"></script>
<script>
  $(document).ready(function() {
    CKEDITOR.editorConfig = function( config ) {
  config.language = 'es';
  config.uiColor = '#F7B42C';
  config.height = '50px';
  config.toolbarCanCollapse = true;
};

CKEDITOR.replace( 'answer_1', {
    height: 80,

  toolbar: [                                        // Line break - next group will be placed in new line.
    { name: 'basicstyles', items: [ 'Bold', 'Italic' ] }
  ]
});

CKEDITOR.replace( 'answer_2', {
  height: 80,
  toolbar: [                                        // Line break - next group will be placed in new line.
    { name: 'basicstyles', items: [ 'Bold', 'Italic' ] }
  ]
});

CKEDITOR.replace( 'answer_3', {
    height: 80,
  toolbar: [                                        // Line break - next group will be placed in new line.
    { name: 'basicstyles', items: [ 'Bold', 'Italic' ] }
  ]
});

CKEDITOR.replace( 'answer_4', {
    height: 80,

  toolbar: [                                        // Line break - next group will be placed in new line.
    { name: 'basicstyles', items: [ 'Bold', 'Italic' ] }
  ]
});


CKEDITOR.replace( 'answer_question', {
    height: 120,
});

CKEDITOR.replace( 'answer_scheme', {
    height: 120,
});


CKEDITOR.replace( 'question', {
    height: 120,
});

    $("#form_main").validate({
      rules: {
        id_pool: {
          required: true
        },
        id_course: {
          required: true
        },
        
        id_course_learning_objective: {
          required: true
        },
        id_bloom_taxonomy: {
          required: true
        },
        id_difficult_level: {
          required: true
        },
        marks: {
          required: true
        },
        status:{
          required: true
        }

      },
      messages: {
        id_pool: {
          required: "<p class='error-text'>Question Pool Required</p>",
        },
        id_course: {
          required: "<p class='error-text'>Select Module</p>",
        },
       
        id_course_learning_objective: {
          required: "<p class='error-text'>Select Module Learining Object</p>",
        },
        id_bloom_taxonomy: {
          required: "<p class='error-text'>Select Bloom Taxonomy</p>",
        },
        id_difficult_level: {
          required: "<p class='error-text'>Select Difficult Level</p>",
        },
        marks: {
          required: "<p class='error-text'>Marks Required</p>",
        },
        status: {
          required: "<p class='error-text'>Select Question Status</p>",
        }
      },
      errorElement: "span",
      errorPlacement: function(error, element) {
        error.appendTo(element.parent());
      }

    });
  });

  function showoptiondetails(val) {
      $("#divMarks").show();

    if(val=='1') {
      $("#mcqanswerDiv").show();
      $("#essayanswerDiv").hide();
      $("#correctAnswerDiv").hide();

            // $("essaydivQuestiontrue").show();
      // $("#essaydivQuestion").hide();


    } 
    if(val=='2') {
      $("#mcqanswerDiv").hide();
      $("#essayanswerDiv").show();
      $("#correctAnswerDiv").hide();
      $("#marks").val('0');
      $("#divMarks").hide();


            // $("essaydivQuestiontrue").show();
      // $("#essaydivQuestion").hide();


    }
    if(val=='3') {
      $("#mcqanswerDiv").hide();
      $("#essayanswerDiv").hide();
      $("#correctAnswerDiv").show();

      // $("essaydivQuestiontrue").hide();
      // $("#essaydivQuestion").show();
    }
  }
</script>
<script type="text/javascript">
  $('select').select2();

  function reloadPage() {
    window.location.reload();
  }

  function savetempdata() {

    var qmarks = $("#answer_marks").val();
    var answer_question = CKEDITOR.instances['answer_question'].getData();
    var answer_scheme = CKEDITOR.instances['answer_scheme'].getData();


    if(qmarks=='') {
      alert('Enter marks');
      return false;
    }

    if(answer_question=='') {
      alert('Enter Question');
      return false;
    }

     if(answer_scheme=='') {
      alert('Enter Answer Scheme');
      return false;
    }

    $.ajax({
        url: "<?php echo site_url('/question/question/updatetempanswer'); ?>",
        method: "POST",
        data: {
          qmarks: qmarks,
          answer_question : answer_question,
          answer_scheme : answer_scheme
        },
        success: function(data) {
         getdata();
         $("#myModal").modal('hide');
                  $("#answer_marks").val('');

          CKEDITOR.instances['answer_question'].setData( '' );
          CKEDITOR.instances['answer_scheme'].setData( '' );

        }
      });
  
  }


  function deletetemp(id) {
    var cnf = confirm("Do you really want to delete?");
    if(cnf==true) {
      $.ajax({
        url: "<?php echo site_url('/question/question/deletetempdata'); ?>",
        method: "POST",
        data: {
          id: id
        },
        success: function(data) {
         getdata();
         $("#myModal").modal('hide');
        }
      });
    }

  }

  function getdata() {
    var qmarks = '1';
    $.ajax({
        url: "<?php echo site_url('/question/question/gettempdata'); ?>",
        method: "POST",
        data: {
          qmarks: qmarks
        },
        success: function(data) {
            $("#appendTable").html(data);

            $("#marks").val($("#totalhidenmarks").val());
        }
      });

  }
</script>
<script type="text/javascript">
  $(document).ready(function() {

    $('#id_course').change(function() {
      var id = $(this).val();
      $.ajax({
        url: "<?php echo site_url('question/ajax/getCategoryTopics'); ?>",
        method: "POST",
        data: {
          id: id
        },
        async: true,
        dataType: 'json',
        success: function(data) {

          var html = '';
          var i;
          for (i = 0; i < data.length; i++) {
            html += '<option value=' + data[i].id + '>' + data[i].name + '-' + data[i].name + '</option>';
          }
          $('#id_topic').html(html);

        }
      });
      return false;
    });

  });
</script>