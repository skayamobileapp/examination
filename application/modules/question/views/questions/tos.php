<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
  <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
    <h1 class="h3">Add TOS</h1>

    <a href='../list' class="btn btn-link ml-auto">
      <i class="fa fa-chevron-left" aria-hidden="true"></i>
      Back
    </a>

  </div>


  <div class="page-container">

    <div>
      <h4 class="form-title">Table of Specifications</h4>
    </div>

    <div class="form-container">


      <div class="row">
        <div class="col-lg-6">
          <div class="form-group row">
            <label class="col-sm-4 col-form-label">Name<span class="text-danger">*</span></label>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="name" name="name">
            </div>
          </div>
        </div>
        <div class="col-lg-6">
          <div class="form-group row">
            <label class="col-sm-4 col-form-label">Question Pool <span class="text-danger">*</span></label>
            <div class="col-sm-8">
              <select name="id_pool" id="id_pool" class="form-control" multiple>
                <option value="">Select</option>
                <?php
                if (!empty($poolList)) {
                  foreach ($poolList as $record) { ?>
                    <option value="<?php echo $record->id;  ?>">
                      <?php echo $record->code . " - " . $record->name;  ?>
                    </option>
                <?php
                  }
                }
                ?>
              </select>
            </div>
          </div>
        </div>
        <div class="col-lg-6">
          <div class="form-group row">
            <label class="col-sm-4 col-form-label">Number of Questions<span class="text-danger">*</span></label>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="question_count" name="question_count">
            </div>
          </div>
        </div>
      </div>

      <div class="button-block clearfix">
        <div class="bttn-group">
          <button type="button" class="btn btn-primary" id="fetchQuestions">Fetch Questions</button>
          <!-- <a onclick="reloadPage()" class="btn btn-link">Clear All Fields</a> -->
          <!-- <button onclick="reloadPage()" class="btn btn-link">Clear All Fields</button> -->
        </div>
      </div>

    </div>
    <div>
      <h4 class="form-title">Question Details</h4>
    </div>
    <div class="form-container">


      <div class="col-12">
        <div class="custom-table">
          <table class="table">
            <thead>
              <tr>
                <th>Sl. No</th>
                <th>Course</th>
                <th>Topic</th>
                <th>Taxonomy</th>
                <th>Difficulty</th>
                <th>Availble Questions</th>
                <th>Choose</th>
              </tr>
            </thead>
            <tbody id="QuestionDetailsBody">
              <tr>
                <td colspan="7" class="text-center">No Data found.</td>
              </tr>
            </tbody>
          </table>
        </div>

      </div>
      <div class="button-block clearfix" id="submitDiv">
        <div class="bttn-group">
          <button type="submit" class="btn btn-primary">Save</button>
          <!-- <a onclick="reloadPage()" class="btn btn-link">Clear All Fields</a> -->
          <!-- <button onclick="reloadPage()" class="btn btn-link">Clear All Fields</button> -->
        </div>
      </div>
    </div>
  </div>
</main>

<script>
  $(document).ready(function() {
    $('#submitDiv').hide();
    $("#form_comitee").validate({
      rules: {
        option_description: {
          required: true
        },
        is_correct_answer: {
          required: true
        }
      },
      messages: {
        option_description: {
          required: "<p class='error-text'>Option Required</p>",
        },
        is_correct_answer: {
          required: "<p class='error-text'>Select Is Correct / Not</p>",
        }
      },
      errorElement: "span",
      errorPlacement: function(error, element) {
        error.appendTo(element.parent());
      }

    });
    $('#fetchQuestions').click(function() {
      var selectedOptions = $("#id_pool option:selected").map(function() {
        return this.value
      }).get().join(", ");
      $.ajax({
        url: "<?php echo site_url('setup/ajax/getTosQuestions'); ?>",
        method: "POST",
        data: {
          pools: selectedOptions
        },
        async: true,
        dataType: 'json',
        success: function(data) {
          var html = '';
          var i;
          if (data.length <= 0) {
            html += '<tr><td colspan="7" class="text-center">No Data found.</td></tr>';
          } else {
            for (i = 0; i < data.length; i++) {
              html += '<tr><td>' + (i + 1) + '</td><td>' + data[i].CourseName + '</td><td>' + data[i].TopicName + '</td><td>' + data[i].TaxonomyName + '</td><td>' + data[i].LevelName + "</td><td>" + data[i].QuestionCount + "</td><td><input type='text' name='selectedQuestions[]'></td></tr>";
            }
            $('#submitDiv').show();
          }
          $('#QuestionDetailsBody').html(html);
        }
      });
    });
  });
</script>
<script type="text/javascript">
  $('select').select2();

  function reloadPage() {
    window.location.reload();
  }
</script>