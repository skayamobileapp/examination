<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
        <h1 class="h3">Add Answers</h1>
        
        <a href='../list' class="btn btn-link ml-auto">
            <i class="fa fa-chevron-left" aria-hidden="true"></i>
          Back
        </a>

    </div>
    

        <div class="page-container">

          <div>
            <h4 class="form-title">Question Details</h4>
          </div>

            <div class="form-container">
                 <div class="row">
                    <div class="col-lg-12">
                      <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Question<span class="text-danger">*</span></label>
                        <div class="col-sm-10">
                         <textarea name="question" id="question" readonly="readonly"><?php echo $question->question; ?></textarea>
                        </div> 
                      </div>
                    </div>
                  </div>      
            </div>  


          
            <div class="clearfix">

                <h4 class="form-title">Answer Details</h4>
                <div class="form-container">

                    <div role="tabpanel" class="tab-pane active" id="invoice">
                        <div class="col-12 mt-2">




                        <form id="form_comitee" action="" method="post" enctype="multipart/form-data">

                                <div class="row">

                                    <div class="col-sm-9">
                                        <div class="form-group">
                                            <label>Answer <span class='error-text'>*</span></label>
                                            <input type="text" class="form-control" id="option_description" name="option_description">
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label>Correct Answer <span class='error-text'>*</span></label>

                                            <div class="col-sm-8">
                <div class="custom-control custom-radio custom-control-inline">
                  <input type="radio" id="is_correct_answer1" name="is_correct_answer" class="custom-control-input" value="1" >
                  <label class="custom-control-label" for="is_correct_answer1">Yes</label>
                </div>
                <div class="custom-control custom-radio custom-control-inline">
                  <input type="radio" id="is_correct_answer2" name="is_correct_answer" class="custom-control-input" value="0" >
                  <label class="custom-control-label" for="is_correct_answer2">No</label>
                </div>
              </div>

                                            
                                        </div>                                      
                                    </div>



                                </div>
                                  <div class="col-lg-6">
                                      <div class="form-group row">
                                        <label class="col-sm-4 col-form-label"></label>
                                        <div class="col-sm-8">
                                        <button type="submit" class="btn btn-primary form-row-btn">Add</button>
                                        </div>
                                      </div>
                                    </div>

                                <br>

                        </form>


                        <?php

                            if(!empty($questionHasOption))
                            {
                                ?>
                                <h4 class="form-title">Option Details</h4>
                                <div class="form-container">
                                        

                                    

                                      <div class="custom-table">
                                        <table class="table" width="100%">
                                            <thead>
                                                <tr>
                                                <th>Sl. No</th>
                                                 <th>Option</th>
                                                 <th class="text-center">Correct Answer</th>
                                                 <th class="text-center">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                 <?php
                                             $total = 0;
                                              for($i=0;$i<count($questionHasOption);$i++)
                                             { ?>
                                                <tr>
                                                <td><?php echo $i+1;?></td>
                                                <td><?php echo $questionHasOption[$i]->option_description;?></td>
                                                <td style="text-align: center;"><?php echo $questionHasOption[$i]->answer_status;
                                                 // if($questionHasOption[$i]->is_correct_answer == '1')
                                                 //  {
                                                 //    echo "Yes";
                                                 //  }
                                                 //  else
                                                 //  {
                                                 //    echo "No";
                                                 //  } 
                                                  ?></td>

                                                <td class="text-center">

                                                <a href="<?php echo '../editOptions/' . $questionHasOption[$i]->id . '/' . $question->id; ?>" class="btn" type="" data-toggle="tooltip" data-placement="top" title="Edit">
                                                  <i class="fa fa-pencil-square-o" aria-hidden="true">
                                                  </i>
                                                </a> |
                                                <a onclick="deleteOption(<?php echo $questionHasOption[$i]->id; ?>)" class="btn" type="" data-toggle="tooltip" data-placement="top" title="Delete">
                                                  <i class="fa fa-trash" aria-hidden="true">
                                                  </i>
                                                </a>
                                                

                                               <!--  <a onclick="deleteOption(<?php echo $questionHasOption[$i]->id; ?>)">Delete</a>

                                                <a href=deleteOption(<?php echo $questionHasOption[$i]->id; ?>)>Delete</a>
                                                </td> -->

                                                 </tr>
                                              <?php 
                                          } 
                                          ?>
                                            </tbody>
                                        </table>
                                      </div>

                                    </div>




                            <?php
                            
                            }
                             ?>


                        </div> 
                    </div>




                </div>

            </div>


        </div>
</main>

<script type="text/javascript" src="<?php echo BASE_PATH; ?>assets/ckeditor/ckeditor.js"></script>

<script>
  $(document).ready(function() {
    CKEDITOR.replace( 'question' );
  });


    function deleteOption(id)
    {
            // alert(id);
            $.ajax(
            {
               url: '/setup/question/deleteOption/'+id,
               type: 'GET',
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                    // alert(result);
                    window.location.reload();
               }
            });
    }



    $(document).ready(function()
    {
        $("#form_comitee").validate(
        {
            rules:
            {
                option_description:
                {
                    required: true
                },
                is_correct_answer:
                {
                    required: true
                }
            },
            messages:
            {
                option_description:
                {
                    required: "<p class='error-text'>Option Required</p>",
                },
                is_correct_answer:
                {
                    required: "<p class='error-text'>Select Status</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
<script type="text/javascript">

    $('select').select2();

    function reloadPage()
    {
      window.location.reload();
    }
</script>