<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
   <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
      <h1 class="h3">Edit Question</h1>
      <a href='../list' class="btn btn-link ml-auto">
      <i class="fa fa-chevron-left" aria-hidden="true"></i>
      Back
      </a>
   </div>
   <form id="form_main" action="" method="post" enctype="multipart/form-data">
      <div class="page-container">
         <div>
            <h4 class="form-title">Question details</h4>
         </div>
         <div class="form-container">
            <div class="row">
               <div class="col-lg-6">
                  <div class="form-group row">
                     <label class="col-sm-4 col-form-label">Question Pool <span class="text-danger">*</span></label>
                     <div class="col-sm-8">
                        <select name="id_pool" id="id_pool" class="form-control">
                           <option value="">Select</option>
                           <?php
                              if (!empty($poolList)) {
                                foreach ($poolList as $record) { ?>
                           <option value="<?php echo $record->id;  ?>" <?php
                              if ($record->id == $question->id_pool) {
                                echo "selected=selected";
                              } ?>>
                              <?php echo $record->code . " - " . $record->name;  ?>
                           </option>
                           <?php
                              }
                              }
                              ?>
                        </select>
                     </div>
                  </div>
               </div>
               <div class="col-lg-6">
                  <div class="form-group row">
                     <label class="col-sm-4 col-form-label">Module <span class="text-danger">*</span></label>
                     <div class="col-sm-8">
                        <select name="id_course" id="id_course" class="form-control">
                           <option value="">Select</option>
                           <?php
                              if (!empty($courseList)) {
                                foreach ($courseList as $record) { ?>
                           <option value="<?php echo $record->id;  ?>" <?php
                              if ($record->id == $question->id_course) {
                                echo "selected=selected";
                              } ?>>
                              <?php echo $record->code . " - " . $record->name;  ?>
                           </option>
                           <?php
                              }
                              }
                              ?>
                        </select>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-lg-6">
                  <div class="form-group row">
                     <label class="col-sm-4 col-form-label">Chapter <span class="text-danger">*</span></label>
                     <div class="col-sm-8">
                        <select name="id_topic" id="id_topic" class="form-control">
                           <option value="">Select</option>
                           <?php
                              if (!empty($topicList)) {
                                foreach ($topicList as $record) { ?>
                           <option value="<?php echo $record->id;  ?>" <?php
                              if ($record->id == $question->id_topic) {
                                echo "selected=selected";
                              } ?>>
                              <?php echo $record->code . " - " . $record->name;  ?>
                           </option>
                           <?php
                              }
                              }
                              ?>
                        </select>
                     </div>
                  </div>
               </div>
               <div class="col-lg-6">
                  <div class="form-group row">
                     <label class="col-sm-4 col-form-label">Question Type <span class="text-danger">*</span></label>
                     <div class="col-sm-8">
                        <select name="question_type" id="question_type" class="form-control" onchange="showoptiondetails(this.value)">
                           <option value="">Select</option>
                           <option value="1" <?php if($question->question_type=='1') { echo 'selected=selected';} ?>>MCQ</option>
                           <option value="2" <?php if($question->question_type=='2') { echo 'selected=selected';} ?>>Essay</option>
                           <option value="3" <?php if($question->question_type=='3') { echo 'selected=selected';} ?>>True / False</option>
                        </select>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-lg-6">
                  <div class="form-group row">
                     <label class="col-sm-4 col-form-label">Difficulty Level <span class="text-danger">*</span></label>
                     <div class="col-sm-8">
                        <select name="id_bloom_taxonomy" id="id_bloom_taxonomy" class="form-control">
                           <option value="">Select</option>
                           <?php
                              if (!empty($bloomTaxonomyList)) {
                                foreach ($bloomTaxonomyList as $record) { ?>
                           <option value="<?php echo $record->id;  ?>" <?php
                              if ($record->id == $question->id_bloom_taxonomy) {
                                echo "selected=selected";
                              } ?>>
                              <?php echo $record->code . " - " . $record->name;  ?>
                           </option>
                           <?php
                              }
                              }
                              ?>
                        </select>
                     </div>
                  </div>
               </div>
             <div class="col-lg-6">
                  <div class="form-group row">
                     <label class="col-sm-4 col-form-label">Marks <span class="text-danger">*</span></label>
                     <div class="col-sm-8">
                        <?php if($question->question_type=='2') { ?>

                        <input type="number" class="form-control" id="marks" name="marks" value="<?php echo $question->marks ?>" min="1" readonly="readonly">

                        <?php } else { ?> 

                        <input type="number" class="form-control" id="marks" name="marks" value="<?php echo $question->marks ?>" min="1">
                     <?php } ?> 
                     </div>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-lg-6">
                  <div class="form-group row">
                     <label class="col-sm-4 col-form-label">Upload Image
                     <?php
                        if (!empty($question->image)) {
                        ?>
                     <span class='error-text'>*</span>
                     <?php
                        }
                        ?>
                     </label>
                     <?php
                        if ($question->image) {
                        ?>
                     <a href="<?php echo '/assets/images/' . $question->image; ?>" target="popup" onclick="window.open(<?php echo '/assets/images/' . $question->image; ?>)" title="<?php echo $question->image; ?>">
                     View
                     </a>
                     <?php
                        }
                        ?>
                     </label>
                     <div class="col-sm-8">
                        <input type="file" class="form-control" id="image_file" name="image_file">
                     </div>
                  </div>
               </div>
               
          
               <div class="col-lg-6">
                  <div class="form-group row align-items-center">
                     <label class="col-sm-4 col-form-label">Status <span class="text-danger">*</span></label>
                     <div class="col-sm-8">
                        <select name="status" id="status" class="form-control">
                           <option value="">Select</option>
                           <?php
                              if (!empty($answerStatusList)) {
                                  foreach ($answerStatusList as $record) {
                              ?>
                           <option value="<?php echo $record->id;  ?>"   
                              <?php
                                 if ($record->id == $question->status)
                                 {
                                   echo "selected=selected";
                                 } ?>
                              >
                              <?php echo $record->name;  ?>        
                           </option>
                           <?php
                              }
                              }
                              ?>
                        </select>
                     </div>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-lg-12">
                  <div class="form-group row">
                     <label class="col-sm-2 col-form-label">Question<span class="text-danger">*</span></label>
                     <div class="col-sm-10">
                        <textarea name="question" id="question"><?php echo $question->question; ?></textarea>
                     </div>
                  </div>
               </div>
            </div>
            <div id="mcqanswerDiv" style="display: none;">
               <div class="row">
                  <div class="col-lg-12">
                     <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Answer 1<span class="text-danger">*</span></label>
                        <div class="col-sm-10">
                           <input type="hidden" class="form-control" id="answer_option_1" name="answer_option_1" value="<?php echo $questionHasOption[0]->id;?>">
                           <textarea name="answer_1" id="answer_1"><?php echo $questionHasOption[0]->option_description;?></textarea>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-lg-12">
                     <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Answer 2<span class="text-danger">*</span></label>
                        <div class="col-sm-10">
                           <input type="hidden" class="form-control" id="answer_option_2" name="answer_option_2" value="<?php echo $questionHasOption[1]->id;?>">                
                           <textarea name="answer_2" id="answer_2"><?php echo $questionHasOption[1]->option_description;?></textarea>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-lg-12">
                     <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Answer 3<span class="text-danger">*</span></label>
                        <div class="col-sm-10">
                           <input type="hidden" class="form-control" id="answer_option_3" name="answer_option_3" value="<?php echo $questionHasOption[2]->id;?>">                
                           <textarea name="answer_3" id="answer_3"><?php echo $questionHasOption[2]->option_description;?></textarea>
                        </div>
                     </div>
                  </div>
               </div>
                   <div class="row">
          <div class="col-lg-12">
            <div class="form-group row">
              <label class="col-sm-2 col-form-label">Answer 4<span class="text-danger">*</span></label>
              <div class="col-sm-10">
              <input type="hidden" class="form-control" id="answer_option_4" name="answer_option_4" value="<?php echo $questionHasOption[3]->id;?>">                
               <textarea name="answer_4" id="answer_4" style="height: 100px;"><?php echo $questionHasOption[3]->option_description;?></textarea>
              </div> 
            </div>
          </div>
        </div>
         <div class="row">
          <div class="col-lg-12">
            <div class="form-group row">
              <label class="col-sm-2 col-form-label">Correct Answer<span class="text-danger">*</span></label>
             
              <div class="col-sm-4">
                <select name="mcq_correct_answer" id="mcq_correct_answer" class="form-control">
                  <option value="1" <?php if($correctAnswer=='1') { echo "selected=selected";} ?>>Answer 1</option>
                  <option value="2" <?php if($correctAnswer=='2') { echo "selected=selected";} ?>>Answer 2</option>
                  <option value="3" <?php if($correctAnswer=='3') { echo "selected=selected";} ?>>Answer 3</option>
                  <option value="4" <?php if($correctAnswer=='4') { echo "selected=selected";} ?>>Answer 4</option>                 
                </select>
              </div>
            </div>
            </div>
        </div>
                  
                 
            </div>
            <div id="correctAnswerDiv" style="display: none">
               <div class="row">
                  <div class="col-lg-12">
                     <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Option 1<span class="text-danger">*</span></label>
                        <div class="col-sm-4">
                           <input type="text" class="form-control"  value="TRUE" readonly="readonly">
                           <input type="hidden" class="form-control" id="true_option_1" name="true_option_1" value="<?php echo $questionHasOption[0]->id;?>" readonly="readonly">
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-lg-12">
                     <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Option 2<span class="text-danger">*</span></label>
                        <div class="col-sm-4">
                           <input type="text" class="form-control"  value="FALSE" readonly="readonly">
                           <input type="hidden" class="form-control" id="true_option_2" name="true_option_2" value="<?php echo $questionHasOption[1]->id;?>" readonly="readonly">
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-lg-12">
                     <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Correct Answer<span class="text-danger">*</span></label>
                        <div class="col-sm-4">
                           <select name="true_correct_answer" id="true_correct_answer" class="form-control">
                              <option value="1"  <?php if($correctAnswer=='1') { echo "selected=selected";} ?>>True</option>
                              <option value="2"  <?php if($correctAnswer=='2') { echo "selected=selected";} ?>>False</option>
                           </select>
                        </div>
                     </div>
                  </div>
               </div>
            </div>

<div id="essayanswerDiv" style="display: none">

              <div class="row">
              <div class="col-lg-12">
                <div class="form-group">
                   <button id="myBtn" class="btn btn-primary"  type="button" data-toggle="modal" data-target="#myModal">Add Sub Question</button>
                </div>
              </div> 
            </div>
            <div class="row">
              <div class="col-lg-12">
                <div id="appendTable"></div>
              </div>
            </div>




         
</div>



         </div>
         <div class="row">
            <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Sl. No</th>
            <th>Question </th>
            <th>Answer Scheme </th>
            <th>Marks </th>
           
            <th style="text-align: center; width: 100px;">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($questionList)) {
            $i = 1;
            foreach ($questionList as $record) {
          ?>
              <tr>
                <td><?php echo $i ?></td>
                <td><?php echo substr($record->question, 0, 50);; ?></td>
                                <td><?php echo substr($record->answer_scheme, 0, 50);?></td>
                <td><?php echo $record->marks; ?></td>

                <td class="text-center">

                  <a href="javascript:showpopup(<?php echo $record->id;?>)" class="btn" type="" data-toggle="tooltip" data-placement="top" title="Edit">
                    <i class="fa fa-pencil-square-o" aria-hidden="true">
                    </i>
                  </a> 
                </td>
              </tr>
          <?php
              $i++;
            }
          }
          ?>
        </tbody>
      </table>
         </div>
         <div class="button-block clearfix">
            <div class="bttn-group">
               <button type="submit" class="btn btn-primary">Save</button>
               <a href='add' class="btn btn-link">Clear All Fields</a>
            </div>
         </div>
      </div>
   </form>

</main>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                 <h4 class="modal-title" id="myModalLabel">Enter Sub Question</h4>

            </div>
            <div class="modal-body">
              <div class="row">


                <div class="col-lg-6">
                  <div class="form-group row">
                    <label class="col-sm-4 col-form-label">Marks <span class="text-danger">*</span></label>
                    <div class="col-sm-8">
                      <input type="number" class="form-control" id="answer_marks" name="answer_marks" min="1">
                     <input type="hidden" class="id" id="id" name="id">

                    </div>
                  </div>
                </div>

              </div>


              
              <div class="row">
                <div class="col-lg-12">
                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Question<span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                     <textarea name="answer_question" id="answer_question"></textarea>
                    </div> 
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-lg-12">
                  <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Answer Scheme<span class="text-danger">*</span></label>
                    <div class="col-sm-10">
                     <textarea name="answer_scheme" id="answer_scheme"></textarea>
                    </div> 
                  </div>
                </div>
              </div>
            </div>
              

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" onclick="return savetempdata()">Save changes</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="<?php echo BASE_PATH; ?>assets/ckeditor/ckeditor.js"></script>
<script>


  function savetempdata() {

    var qmarks = $("#answer_marks").val();
    var id = $("#id").val();
    var answer_question = CKEDITOR.instances['answer_question'].getData();
    var answer_scheme = CKEDITOR.instances['answer_scheme'].getData();
    var questionId = <?php echo $question->id;?>

    if(qmarks=='') {
      alert('Enter marks');
      return false;
    }

    if(answer_question=='') {
      alert('Enter Question');
      return false;
    }

     if(answer_scheme=='') {
      alert('Enter Answer Scheme');
      return false;
    }

    $.ajax({
        url: "<?php echo site_url('/question/question/updateanswer'); ?>",
        method: "POST",
        data: {
          qmarks: qmarks,
          id: id,
          answer_question : answer_question,
          answer_scheme : answer_scheme,
          questionId:questionId
        },
        success: function(data) {
         location.reload();

        }
      });
  
  }


   function showpopup(id) {
       $("#myModal").modal('show');

       $.ajax({
        url: "<?php echo site_url('/question/question/getquestiondetailsbyid'); ?>",
        method: "POST",
        data: {
          id: id
        },
        success: function(data) {
         var questiondata = JSON.parse(data);
         console.log(questiondata.question);
         $("#answer_marks").val(questiondata.marks);  
         $("#id").val(questiondata.id);  

CKEDITOR.instances.answer_question.setData(questiondata.question);
CKEDITOR.instances.answer_scheme.setData(questiondata.answer_scheme);

         // $("#answer_question").val(questiondata.question);

         // getdata();
         // $("#myModal").modal('hide');
        }
      });
  
  }

   $(document).ready(function() {
     CKEDITOR.editorConfig = function( config ) {
   config.language = 'es';
   config.uiColor = '#F7B42C';
   config.height = '50px';
   config.toolbarCanCollapse = true;
   };


CKEDITOR.replace( 'answer_question', {
    height: 120,
});

CKEDITOR.replace( 'answer_scheme', {
    height: 120,
});


   
   CKEDITOR.replace( 'answer_1', {
     height: 80,
   
   toolbar: [                                        // Line break - next group will be placed in new line.
     { name: 'basicstyles', items: [ 'Bold', 'Italic' ] }
   ]
   });
   
   CKEDITOR.replace( 'answer_2', {
   height: 80,
   toolbar: [                                        // Line break - next group will be placed in new line.
     { name: 'basicstyles', items: [ 'Bold', 'Italic' ] }
   ]
   });
   
   CKEDITOR.replace( 'answer_3', {
     height: 80,
   toolbar: [                                        // Line break - next group will be placed in new line.
     { name: 'basicstyles', items: [ 'Bold', 'Italic' ] }
   ]
   });
   
   CKEDITOR.replace( 'answer_4', {
     height: 80,
   
   toolbar: [                                        // Line break - next group will be placed in new line.
     { name: 'basicstyles', items: [ 'Bold', 'Italic' ] }
   ]
   });
   
     CKEDITOR.replace( 'question' );
     $("#form_main").validate({
       rules: {
         id_pool: {
           required: true
         },
         id_course: {
           required: true
         },
         id_topic: {
           required: true
         },
         id_course_learning_objective: {
           required: true
         },
         id_bloom_taxonomy: {
           required: true
         },
         id_difficult_level: {
           required: true
         },
         marks: {
           required: true
         }
       },
       messages: {
         id_pool: {
           required: "<p class='error-text'>Question Pool Required</p>",
         },
         id_course: {
           required: "<p class='error-text'>Select Module</p>",
         },
         id_topic: {
           required: "<p class='error-text'>Select Topic</p>",
         },
         id_course_learning_objective: {
           required: "<p class='error-text'>Select Module Learining Object</p>",
         },
         id_bloom_taxonomy: {
           required: "<p class='error-text'>Select Bloom Taxonomy</p>",
         },
         id_difficult_level: {
           required: "<p class='error-text'>Select Difficult Level</p>",
         },
         marks: {
           required: "<p class='error-text'>Marks Required</p>",
         }
       },
       errorElement: "span",
       errorPlacement: function(error, element) {
         error.appendTo(element.parent());
       }
   
     });
   
     var id = "<?php echo $question->question_type;?>";
   
     showoptiondetails(id);
   });
   
   function showoptiondetails(val) {
   
     if(val=='1') {
       $("#mcqanswerDiv").show();
       $("#essayanswerDiv").hide();
       $("#correctAnswerDiv").hide();
   
     } 
     if(val=='2') {
       $("#mcqanswerDiv").hide();
       $("#essayanswerDiv").show();
       $("#correctAnswerDiv").hide();
   
     }
     if(val=='3') {
       $("#mcqanswerDiv").hide();
       $("#essayanswerDiv").hide();
       $("#correctAnswerDiv").show();
   
     }
   }
</script>
<script type="text/javascript">
   $('select').select2();
   
   function reloadPage() {
     window.location.reload();
   }
</script>
<script type="text/javascript">
   $(document).ready(function() {
   
     $('#id_course').change(function() {
       var id = $(this).val();
       $.ajax({
         url: "<?php echo site_url('question/ajax/getCategoryTopics'); ?>",
         method: "POST",
         data: {
           id: id
         },
         async: true,
         dataType: 'json',
         success: function(data) {
   
           var html = '';
           var i;
           for (i = 0; i < data.length; i++) {
             html += '<option value=' + data[i].id + '>' + data[i].name + '-' + data[i].name + '</option>';
           }
           $('#id_topic').html(html);
   
         }
       });
       return false;
     });
   
   });
</script>