<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
  <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
    <h1 class="h3">Add Examset</h1>

    <a href='list' class="btn btn-link ml-auto">
      <i class="fa fa-chevron-left" aria-hidden="true"></i>
      Back
    </a>

  </div>

  <form id="form_main" action="" method="post" enctype="multipart/form-data">

    <div class="page-container">

      <div>
        <h4 class="form-title">Examset details</h4>
      </div>

      <div class="form-container">


        <div class="row">
            <div class="col-lg-6">
               <div class="form-group row">
                  <label class="col-sm-4 col-form-label">Examset Name <span class="text-danger">*</span></label>
                  <div class="col-sm-8">
                     <input type="text" class="form-control" id="name" name="name" placeholder="Examset Name" value="">
                  </div>
               </div>
            </div>
            <div class="col-lg-6">
               <div class="form-group row">
                  <label class="col-sm-4 col-form-label">Duration in Min <span class="text-danger">*</span></label>
                  <div class="col-sm-8">
                     <input type="text" class="form-control" id="duration" name="duration" placeholder="Duration" value="">
                  </div>
               </div>
            </div>

              <div class="col-lg-6">
               <div class="form-group row align-items-center">
                  <label class="col-sm-4 col-form-label">Type of Exam <span class="text-danger">*</span></label>
                  <div class="col-sm-8">
                     <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="customRadioInline112" name="type_of_exam" class="custom-control-input" value="1" onclick="typeofExam(1)" checked="">
                        <label class="custom-control-label" for="customRadioInline112">Module Based</label>
                     </div>
                     <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="customRadioInline223" name="type_of_exam" class="custom-control-input" value="2" onclick="typeofExam(2)">
                        <label class="custom-control-label" for="customRadioInline223">Programme Based</label>
                     </div>
                  </div>
               </div>
            </div>
             <div class="col-lg-6">
               <div class="form-group row">
                  <label class="col-sm-4 col-form-label"><span id="labeldiv"></span><span class="text-danger">*</span></label>
                  <div class="col-sm-8">
                     <div id='idcourseDiv'></div>
                  </div>
               </div>
            </div>

            <div class="col-lg-6">
               <div class="form-group row">
                  <label class="col-sm-4 col-form-label">Attempts <span class="text-danger">*</span></label>
                  <div class="col-sm-8">
                     <select name="attempts" id="attempts" class="form-control" onchange="showunlimited(this.value)">
                        <option value="">Select</option>
                        <option value="1" >Once</option>
                        <option value="4" >Unlimited</option>
                     </select>
                  </div>
               </div>
            </div>
          </div>
          <div class="row" id="unlimitedDiv" style="display: none;">
            <div class="col-lg-6">
               <div class="form-group row">
                  <label class="col-sm-4 col-form-label">Repeat Grading Method <span class="text-danger">*</span></label>
                  <div class="col-sm-8">
                     <select name="grading_method" id="grading_method" class="form-control" style="width:250px;">
                        <option value="">Select</option>
                        <option value="1">Highest Grade</option>
                        <option value="2">Average Grade</option>
                        <option value="3">First Attempt</option>
                        <option value="4">Last Attempt</option>
                     </select>
                  </div>
               </div>
            </div>
              <div class="col-lg-6">
               <div class="form-group row">
                  <label class="col-sm-4 col-form-label">Repeat Question Behaviour <span class="text-danger">*</span></label>
                  <div class="col-sm-8">
                     <select name="repetative_behaviour" id="repetative_behaviour" class="form-control" style="width:250px;">
                        <option value="">Select</option>
                        <option value="2">Randomise the previous attempt question by same candidate in next attempt (multiple attempts)</option>

                     </select>
                  </div>
               </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-6">
               <div class="form-group row">
                  <label class="col-sm-4 col-form-label">Layout <span class="text-danger">*</span></label>
                  <div class="col-sm-8">
                     <select name="layout" id="layout" class="form-control">
                        <option value="">Select</option>
                                      
                        <option value="4">All Question in single page</option>
                     </select>
                  </div>
               </div>
            </div>
            <div class="col-lg-6">
               <div class="form-group row">
                  <label class="col-sm-4 col-form-label">Question Behaviour <span class="text-danger">*</span></label>
                  <div class="col-sm-8">
                     <select name="behaviour" id="behaviour" class="form-control">
                        <option value="">Select</option>
                        <option value="1">Randomise and generate new set for every student</option>
                        <option value="2">Randomise the previous attempt question by same candidate in next attempt (multiple attempts)</option>
                        <option value="3">Generete one set and fixed question number for all
</option> 

                     </select>
                  </div>
               </div>
            </div>
          
         </div>
         <div class="row">
            <div class="col-lg-7">
               <div class="form-group row align-items-center">
                  <label class="col-sm-4 col-form-label">Type of TOS <span class="text-danger">*</span></label>
                  <div class="col-sm-8">
                     <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="customRadioInline11" name="tos_status" class="custom-control-input" value="1" onclick="showexamset(1)">
                        <label class="custom-control-label" for="customRadioInline11">Manual TOS</label>
                     </div>
                     <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="customRadioInline22" name="tos_status" class="custom-control-input" value="0" onclick="showexamset(0)">
                        <label class="custom-control-label" for="customRadioInline22">Auto TOS</label>
                     </div>

                     <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="customRadioInline33" name="tos_status" class="custom-control-input" value="2" onclick="showexamset(2)">
                        <label class="custom-control-label" for="customRadioInline33">Maual Exam Set</label>
                     </div>
                     
                  </div>
               </div>
            </div>
              
         </div>
         <div class="row">
            <div class="col-lg-6" id="manualExamDiv" style="display: none;">
               <div class="form-group row">
                  <label class="col-sm-4 col-form-label">TOS <span class="text-danger">*</span></label>
                  <div class="col-sm-8">
                     <select name="id_manual_tos" id="id_manual_tos" class="form-control">
                        <option value="">Select</option>
                        <?php
                           if (!empty($manualExamSet))
                           {
                               foreach ($manualExamSet as $record)
                               {  
                                  if($record->prepared_from=='2') { ?>
                        <option value="<?php echo $record->id;  ?>">
                           <?php echo $record->name;?>
                        </option>
                        <?php
                           }
                           }
                       }
                           ?>
                     </select>
                  </div>
               </div>
            </div>
            <div class="col-lg-6" id="manualExamDivset" style="display: none;">
               <div class="form-group row">
                  <label class="col-sm-4 col-form-label">TOS <span class="text-danger">*</span></label>
                  <div class="col-sm-8">
                     <select name="id_manual_tos_set" id="id_manual_tos_set" class="form-control">
                        <option value="">Select</option>
                        <?php
                           if (!empty($manualExamSet))
                           {
                               foreach ($manualExamSet as $record)
                               {  
                                  if($record->prepared_from=='1') { ?>
                        <option value="<?php echo $record->id;  ?>">
                           <?php echo $record->name;?>
                        </option>
                        <?php
                           }
                           }
                       }
                           ?>
                     </select>
                  </div>
               </div>
            </div>

            <div class="col-lg-6" id="autoExamDiv" style="display: none;">
               <div class="form-group row">
                  <label class="col-sm-4 col-form-label">TOS <span class="text-danger">*</span></label>
                  <div class="col-sm-8">
                     <select name="id_tos" id="id_tos" class="form-control">
                        <option value="">Select</option>
                        <?php
                           if (!empty($toslist)) {
                             foreach ($toslist as $record) {
                           ?>
                        <option value="<?php echo $record->id; ?>">
                           <?php echo $record->name;  ?>
                        </option>
                        <?php
                           }
                           }
                           ?>
                     </select>
                  </div>
               </div>
            </div>

             <div class="col-lg-6">
               <div class="form-group row">
                  <label class="col-sm-4 col-form-label">Mark Display <span class="text-danger">*</span></label>
                  <div class="col-sm-8">
                     <select name="id_mark_display" id="id_mark_display" class="form-control">
                        <option value="">Select</option>
                        <option value="1">Show exam grade only</option>
                        <option value="2">Show exam result details (grade and mark by section)</option>
                        <option value="3">Do not show result</option>
                       
                     </select>
                  </div>
               </div>
            </div>
              <div class="col-lg-6">
               <div class="form-group row">
                  <label class="col-sm-4 col-form-label">Submission Message  <span class="text-danger">*</span></label>
                  <div class="col-sm-8">
                     <select name="id_communication" id="id_communication" class="form-control">
                        <option value="">Select</option>
                        <?php
                           if (!empty($communication)) {
                             foreach ($communication as $record) {
                           ?>
                        <option value="<?php echo $record->id; ?>">
                           <?php echo $record->name;  ?>
                        </option>
                        <?php
                           }
                           }
                           ?>
                     </select>
                  </div>
               </div>
            </div>
            <div class="col-lg-6">
               <div class="form-group row align-items-center">
                  <label class="col-sm-4 col-form-label">Status <span class="text-danger">*</span></label>
                  <div class="col-sm-8">
                     <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="customRadioInline1" name="status" class="custom-control-input" value="1">
                        <label class="custom-control-label" for="customRadioInline1">Active</label>
                     </div>
                     <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="customRadioInline2" name="status" class="custom-control-input" value="0">
                        <label class="custom-control-label" for="customRadioInline2">In-Active</label>
                     </div>
                  </div>
               </div>
            </div>

          </div>
        <div class="row">
          <div class="col-lg-12">
            <div class="form-group row">
              <label class="col-sm-2 col-form-label">Instructions<span class="text-danger">*</span></label>
              <div class="col-sm-10">
                <textarea name="instructions" id="instructions"></textarea>
              </div>
            </div>
          </div>
        </div>



        <div class="button-block clearfix">
          <div class="bttn-group">
            <button type="submit" class="btn btn-primary">Save</button>
            <a href='add' class="btn btn-link">Clear All Fields</a>
          </div>

        </div>

      </div>
    </div>
  </form>
</main>
<script type="text/javascript" src="<?php echo BASE_PATH; ?>assets/ckeditor/ckeditor.js"></script>
<script>
  $(document).ready(function() {
    CKEDITOR.replace('instructions');

    $("#form_main").validate({
      rules: {
        name: {
          required: true
        },
        instructions: {
          required: true
        },
        duration: {
          required: true
        },
        pass_grade: {
          required: true
        },
        attempts: {
          required: true
        },
        grading_method: {
          required: true
        },
        layout: {
          required: true
        },
        behaviour: {
          required: true
        },
        id_tos: {
          required: true
        },
        id_course:{
          required: true
        }
      },
      messages: {
        name: {
          required: "<p class='error-text'>Examset Name Required</p>",
        },
        instructions: {
          required: "<p class='error-text'>Instructions Required</p>",
        },
        duration: {
          required: "<p class='error-text'>Select Duration</p>",
        },
        pass_grade: {
          required: "<p class='error-text'>Pass Grade Required</p>",
        },
        attempts: {
          required: "<p class='error-text'>Select Attempts</p>",
        },
        grading_method: {
          required: "<p class='error-text'>Select Grading Method</p>",
        },
        layout: {
          required: "<p class='error-text'>Select Layout</p>",
        },
        behaviour: {
          required: "<p class='error-text'>Select Behaviour</p>",
        },
        id_tos: {
          required: "<p class='error-text'>Select TOS</p>",
        },
        id_course:{
          required: "<p class='error-text'>Select Module/Programme</p>",
        }
      },
      errorElement: "span",
      errorPlacement: function(error, element) {
        error.appendTo(element.parent());
      }

    });
  });
</script>
<script type="text/javascript">
  $('select').select2();

  function reloadPage() {
    window.location.reload();
  }
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#attempt_durationdiv').hide();
    typeofExam(1);

    $('#attempts').change(function() {
      var id = $(this).val();
      if (id == 4) {
        $('#attempt_durationdiv').show();
      } else {
        $('#attempt_durationdiv').hide();
      }
    });

  });
</script>
<script type="text/javascript">
   $(document).ready(function() {

     var attempts = $('#attempts').val();
     if(attempts == 4){
       $('#attempt_durationdiv').show();
     }
     else{
       $('#attempt_durationdiv').hide();
     }
   
     $('#attempts').change(function() {
       var id = $(this).val();
       if (id == 4) {
         $('#attempt_durationdiv').show();
       } else {
         $('#attempt_durationdiv').hide();
       }
     });
   
   });
   
    function typeofExam(type) {

      if(type=='1') {
        $("#labeldiv").html("Select Module");
      } else {
        $("#labeldiv").html("Select Programme");

      }

       $.ajax({
        url: "/question/examset/getmoduledropdown",
        method: "POST",
        data: {
          id: type
        },
        success: function(data) {
          $("#idcourseDiv").html(data);
        }
      });
    }
   
    function showexamset(id) {

        $("#manualExamDiv").hide();
        $("#autoExamDiv").hide();
          $("#manualExamDivset").hide();

       if(id=='1') {
          $("#manualExamDiv").show();
       } 
       if(id=='0') {
          $("#autoExamDiv").show();
       }
       if(id=='2') {
          $("#manualExamDivset").show();
       }
     } 

     function showunlimited(id){
      if(id==4) {
        $("#unlimitedDiv").show();
      } else {
        $("#unlimitedDiv").hide();

      }
     }
</script>
