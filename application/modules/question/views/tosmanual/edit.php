<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
        <h1 class="h3">Edit Manual Tos</h1>
        
        <a href='../list' class="btn btn-link ml-auto">
            <i class="fa fa-chevron-left" aria-hidden="true"></i>
          Back
        </a>

    </div>
    
    <form id="form_main" action="" method="post">
             <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
           <tr>
                <th>TOS - <?php echo $tos->name;?></th>
               <th>Code - <?php echo $tos->code;?></th>
               <th>Status - 
                <?php if($tos->status=='1') { echo "Active";}
                if($tos->status=='2') { echo "In-Active";}
                if($tos->status=='3') { echo "Draft";}?></th>
               <th></th>
             </tr>

            <tr>
                <th>Question Type</th>
               <th>MCQ Question</th>
               <th>Essay Question</th>
               <th>True / False Question</th>
             </tr>
          </thead>
          <tbody>
            <tr>
              <td>Assigned </td>
               <td><?php echo $tos->mcq_question;?></td>
               <td><?php echo $tos->essay_question;?></td>
               <td><?php echo $tos->true_false_question;?></td>
             </tr>

             <tr>
              <td>Tagged </td>
               <td><?php echo $totalAssignedMCQ;?></td>
               <td><?php echo $totalAssignedEssay;?></td>
               <td><?php echo $totalAssignedTrue;?></td>
             </tr>
              <tr>
              <th>Pending </th>
               <td><?php echo ($tos->mcq_question-$totalAssignedMCQ);?>
                 <input type='hidden' name='pending_mcq' value="<?php echo $tos->mcq_question-$totalAssignedMCQ;?>" />
               </th>
               <td><?php echo ($tos->essay_question-$totalAssignedEssay);?>
                 <input type='hidden' name='pending_essay' value="<?php echo $tos->essay_question-$totalAssignedEssay;?>" />
               </th>
               <td><?php echo ($tos->true_false_question-$totalAssignedTrue);?>
                 <input type='hidden' name='pending_true' value="<?php echo $tos->true_false_question-$totalAssignedTrue;?>" />
               </th>
             </tr>
         </tbody>
       </table>
     </div>

    

            <div class="form-container">


            <div class="clearfix">

                <ul class="nav nav-tabs" role="tablist" >
                    <li role="presentation" class="nav-item" ><a href="#tab_one" class="nav-link active"
                            aria-controls="tab_one" aria-selected="true"
                            role="tab" data-toggle="tab">Assign Questions</a>
                    </li>
                    <li role="presentation" class="nav-item"><a href="#tab_two" class="nav-link"
                            aria-controls="tab_two" role="tab" data-toggle="tab">Tagged Questions</a>
                    </li>
                </ul>
                 <div class="tab-content offers-tab-content">



                    <div role="tabpanel" class="tab-pane active" id="tab_one">
                        <div class="row">
                             <form action="" method="post" id="searchForm">

          <div id="collapseOne" class="panel-collapse collapse show" role="tabpanel" aria-labelledby="headingOne" aria-expanded="true" style="">
            <div class="panel-body p-2">

              <div class="row">

                <div class="col-lg-6">
                  <div class="form-group row">
                    <label class="col-sm-4 col-form-label">Question Pool</label>
                    <div class="col-sm-8">
                      <select name="id_pool" id="id_pool" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($poolList)) {
                          foreach ($poolList as $record) { ?>
                            <option value="<?php echo $record->id;  ?>" <?php if ($searchParam['id_pool'] == $record->id) {
                                                                          echo "selected";
                                                                        } ?>>
                              <?php echo $record->code . " - " . $record->name;  ?>
                            </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>
              </div>
               <div class="row">

                 <div class="col-lg-6">
               <div class="form-group row align-items-center">
                  <label class="col-sm-4 col-form-label">Course Type <span class="text-danger">*</span></label>
                  <div class="col-sm-8">
                     <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="customRadioInline112" name="type_of_exam" class="custom-control-input" value="1" onclick="typeofExam(1)" checked="">
                        <label class="custom-control-label" for="customRadioInline112">Module Based</label>
                     </div>
                     <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" id="customRadioInline223" name="type_of_exam" class="custom-control-input" value="2" onclick="typeofExam(2)">
                        <label class="custom-control-label" for="customRadioInline223">Programme Based</label>
                     </div>
                  </div>
               </div>
            </div>
             <div class="col-lg-6">
               <div class="form-group row">
                  <label class="col-sm-4 col-form-label"><span id="labeldiv"></span><span class="text-danger">*</span></label>
                  <div class="col-sm-8">
                     <div id='idcourseDiv'></div>
                  </div>
               </div>
            </div>
           
                <div class="col-lg-6">
                  <div class="form-group row">
                    <label class="col-sm-4 col-form-label">Topic</label>
                    <div class="col-sm-8">
                      <select name="id_topic" id="id_topic" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($topicList)) {
                          foreach ($topicList as $record) { ?>
                            <option value="<?php echo $record->id;  ?>" <?php if ($searchParam['id_topic'] == $record->id) {
                                                                          echo "selected";
                                                                        } ?>>
                              <?php echo $record->code . " - " . $record->name;  ?>
                            </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="form-group row">
                    <label class="col-sm-4 col-form-label">Bloom Taxonomy</label>
                    <div class="col-sm-8">
                      <select name="id_bloom_taxonomy" id="id_bloom_taxonomy" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($bloomTaxonomyList)) {
                          foreach ($bloomTaxonomyList as $record) { ?>
                            <option value="<?php echo $record->id;  ?>" <?php if ($searchParam['id_bloom_taxonomy'] == $record->id) {
                                                                          echo "selected";
                                                                        } ?>>
                              <?php echo $record->code . " - " . $record->name;  ?>
                            </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="form-group row">
                    <label class="col-sm-4 col-form-label">Difficulty Level </label>
                    <div class="col-sm-8">
                      <select name="id_difficult_level" id="id_difficult_level" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($difficultLevelList)) {
                          foreach ($difficultLevelList as $record) { ?>
                            <option value="<?php echo $record->id;  ?>" <?php if ($searchParam['id_difficult_level'] == $record->id) {
                                                                          echo "selected";
                                                                        } ?>>
                              <?php echo $record->code . " - " . $record->name;  ?>
                            </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>

                <div class="col-lg-6">
                  <div class="form-group row">
                    <label class="col-sm-4 col-form-label">Question Type  <span class="text-danger">*</span></label>
                    <div class="col-sm-8">
                      <select name="question_type" id="question_type" class="form-control" required="required">
                        <option value="">Select</option>
                        <option value="1" <?php if($searchParam['question_type']=='1') { echo "selected=selected";} ?>>MCQ</option>
                        <option value="2" <?php if($searchParam['question_type']=='2') { echo "selected=selected";} ?>>Essay</option>
                        <option value="3" <?php if($searchParam['question_type']=='3') { echo "selected=selected";} ?>>True / False</option>
                       
                      </select>
                    </div>
                  </div>
                </div>

              </div>

              <hr />
              <div class="d-flex justify-content-center">
                <button type="submit" class="btn btn-primary" name='save' value="search">Search</button>
                <button type="reset" class="btn btn-link" onclick="clearSearchForm()">Clear All Fields</button>
              </div>
            </div>
          </div>

        </form>


                

                   

                </div>



                <div class="row">
                                          <div class="col-sm-12">

                     <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th><input type='checkbox' name='checkAll' id='checkAll' onclick="validate()" />Select All</th>
             <th>Sl. No</th>
            <th>Question Pool</th>
            <th>Module</th>
            <th>Topic</th>
            <th>Question Type</th>

            <th>Question </th>
            <th>Difficulty Level</th>
            <th>Bloom Taxanomy</th>
          
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($questionList)) {
            $i = 1;
            foreach ($questionList as $record) {

             
          ?>
              <tr>
                <td>
                <input type='checkbox' name='question[]' value="<?php echo $record->id; ?>"/></td>
                <td><?php echo $i ?></td>
                <td><?php echo $record->PoolName; ?></td>
                <td><?php echo $record->coursename; ?></td>
                <td><?php echo $record->topicname; ?></td>
                <td><?php if($record->question_type=='1') { echo "MCQ";}
                if($record->question_type=='2') { echo "Essay";}
                if($record->question_type=='3') { echo "True / False";} ?></td>
              <td><?php echo substr($record->question, 0, 100);; ?></td>

                <td><?php echo $record->difficultyname; ?></td>
                <td><?php echo $record->bloomtaxanomyname; ?></td>
               
              </tr>
          <?php
              $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>

                   
                </div>       


                  

               <div class="button-block clearfix">
              <div class="bttn-group">
                  <button type="submit" class="btn btn-primary" name='save' value='save'>Save</button>
              </div>

            </div> 
                    </div>

                     <div role="tabpanel" class="tab-pane" id="tab_two">
                        <div class="row">




           

                </div>

                    <div class="row">
                        <div class="col-sm-12">
                         <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
          <th>Sl. No</th>
            <th>Question Pool</th>
            <th>Module</th>
            <th>Topic</th>
            <th>Question Type</th>

            <th>Question </th>
            <th>Difficulty Level</th>
            <th>Bloom Taxanomy</th>
                        <th>View</th>
            <th>Untag</th>

          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($quesiontosdetails)) {
            $i = 1;
            foreach ($quesiontosdetails as $record){  

             $questionid = $record->id;
              $mqmid= $record->mqid;             
          ?>
              <tr>
                <td><?php echo $i ?></td>
                 <td><?php echo $record->PoolName; ?></td>
                <td><?php echo $record->coursename; ?></td>
                <td><?php echo $record->topicname; ?></td>
                  <td><?php if($record->question_type=='1') { echo "MCQ";}
                if($record->question_type=='2') { echo "Essay";}
                if($record->question_type=='3') { echo "True / False";} ?></td>
              <td><?php echo substr($record->question, 0, 100);; ?></td>

                <td><?php echo $record->difficultyname; ?></td>
                <td><?php echo $record->bloomtaxanomyname; ?></td>
               
              
                <td><a href="/question/question/view/<?php echo $questionid;?>" target="_blank">View</a></td>
                <td><a href="javascript:fndelete(<?php echo $mqmid;?>)">Untag</a></td>

               
              </tr>
          <?php
              $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>
                    </div>

                    </div>
                    </div>
                </div>
            </div>



                
        </div>
    </form>
</main>

<script>
    $(document).ready(function() {
        $("#form_main").validate({
            rules: {
                code: {
                    required: true
                },
                name: {
                    required: true
                }
            },
            messages: {
                code: {
                    required: "<p class='error-text'>Code Required</p>",
                },
                name: {
                    required: "<p class='error-text'>Name Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });

    });
</script>

<script type="text/javascript">

  function fndelete(id) {
    var cnf= confirm("Do you really want to untag?");
    if(cnf==true){

      $.ajax({
        url: "/question/tosmanual/deleteids",
        method: "POST",
        data: {
          id: id
        },
        success: function(data) {
          location.reload();
        }
      });


    }
  }
   function typeofExam(type) {

      if(type=='1') {
        $("#labeldiv").html("Select Module");
      } else {
        $("#labeldiv").html("Select Programme");

      }

       $.ajax({
        url: "/question/tosmanual/getmoduledropdown",
        method: "POST",
        data: {
          id: type
        },
        success: function(data) {
          $("#idcourseDiv").html(data);
        }
      });
    }

        $(document).ready(function(){
          typeofExam(1);
 
            $('#id_course').change(function(){ 
                var id=$(this).val();
                $.ajax({
                    url : "setup/ajax/getCategoryTopics",
                    method : "POST",
                    data : {id: id},
                    async : true,
                    dataType : 'json',
                    success: function(data){
                         
                        var html = '';
                        var i;
                        for(i=0; i<data.length; i++){
                            html += '<option value='+data[i].id+'>'+data[i].name+'-'+data[i].name+'</option>';
                        }
                        $('#id_topic').html(html);
 
                    }
                });
                return false;
            }); 
             
        });


         function validate()
  {
    var statuscheck = $("#checkAll").is(':checked');

    if(statuscheck==true)
    {
        $('input:checkbox').prop('checked', true);
    }

    if(statuscheck==false)
    {
        $('input:checkbox').prop('checked', false);
    }    
  }


    </script>