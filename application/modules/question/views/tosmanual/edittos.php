<style>
  .error {
    display: grid;
  }
</style>
<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
  <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
    <h1 class="h3">Edit Manual TOS</h1>

    <a href='../list' class="btn btn-link ml-auto">
      <i class="fa fa-chevron-left" aria-hidden="true"></i>
      Back
    </a>

  </div>

 <form id="form_unit" action="" method="post">

    <div class="page-container">

      <div>
        <h4 class="form-title">Table of Specifications</h4>
      </div>

      <div class="form-container">


        <div class="row">
          <div class="col-lg-6">
            <div class="form-group row">
              <label class="col-sm-4 col-form-label">Name<span class="text-danger">*</span></label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="name" name="name" value="<?php echo $tos->name;?>">
              </div>
            </div>
          </div>
          <div class="col-lg-6">
            <div class="form-group row">
              <label class="col-sm-4 col-form-label">Code</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="code" name="code" value="<?php echo $tos->code;?>">
              </div>
            </div>
          </div>

             <div class="col-lg-6">
            <div class="form-group row">
              <label class="col-sm-4 col-form-label">Number of MCQ Question<span class="text-danger">*</span></label>
              <div class="col-sm-8">
                <input type="text" class="form-control number" id="mcq_question" name="mcq_question" maxlength="2" onblur="calculatemcqmarks(this.value)" value="<?php echo $tos->mcq_question;?>">
              </div>
            </div>
          </div>

             <div class="col-lg-6">
            <div class="form-group row">
              <label class="col-sm-4 col-form-label">MCQ Marks<span class="text-danger">*</span></label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="mcq_marks" name="mcq_marks" readonly="readonly" value="<?php echo $tos->mcq_marks;?>">
              </div>
            </div>
          </div>

             <div class="col-lg-6">
            <div class="form-group row">
              <label class="col-sm-4 col-form-label">Essay Question<span class="text-danger">*</span></label>
              <div class="col-sm-8">
                <input type="text" class="form-control number" id="essay_question" name="essay_question" maxlength="2" onblur="calculateessaymarks(this.value)" value="<?php echo $tos->essay_question;?>">
              </div>
            </div>
          </div>

             <div class="col-lg-6">
            <div class="form-group row">
              <label class="col-sm-4 col-form-label">Essay Marks<span class="text-danger">*</span></label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="essay_marks" name="essay_marks" readonly="readonly" value="<?php echo $tos->essay_marks;?>">
              </div>
            </div>
          </div>

             <div class="col-lg-6">
            <div class="form-group row">
              <label class="col-sm-4 col-form-label">True / False Question<span class="text-danger">*</span></label>
              <div class="col-sm-8">
                <input type="text" class="form-control number" id="true_false_question" name="true_false_question" maxlength="2" onblur="calculatetruemarks(this.value)" value="<?php echo $tos->true_false_question;?>">
              </div>
            </div>
          </div>

              <div class="col-lg-6">
            <div class="form-group row">
              <label class="col-sm-4 col-form-label">True / False Marks<span class="text-danger">*</span></label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="true_false_marks" name="true_false_marks" readonly="readonly" value="<?php echo $tos->true_false_marks;?>">
              </div>
            </div>
          </div>

             <div class="col-lg-6">
            <div class="form-group row">
              <label class="col-sm-4 col-form-label">Total Question<span class="text-danger">*</span></label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="question_count" name="question_count" readonly="readonly" value="<?php echo $tos->question_count;?>">
              </div>
            </div>
          </div>

             <div class="col-lg-6">
            <div class="form-group row">
              <label class="col-sm-4 col-form-label">Total Marks<span class="text-danger">*</span></label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="total_marks" name="total_marks" readonly="readonly" value="<?php echo $tos->total_marks;?>">
              </div>
            </div>
          </div>


         

            <div class="col-lg-6">
            <div class="form-group row">
              <label class="col-sm-4 col-form-label">Status<span class="text-danger">*</span></label>
              <div class="col-sm-8">
                <select name="status" id="status" class="form-control">
                  <option value="">Select</option>
                  <option value="1" <?php if($tos->status=='1') {echo "selected=selected";} ?>>Active</option>
                  <option value="2" <?php if($tos->status=='2') {echo "selected=selected";} ?>>In-Active</option>
                  <option value="3" <?php if($tos->status=='3') {echo "selected=selected";} ?>>Draft</option>
                  
                </select>
              </div>
            </div>
          </div>
        </div>
        <div class="row">


 <div class="col-lg-6">
            <div class="form-group row">
              <label class="col-sm-4 col-form-label">Display of Question<span class="text-danger">*</span></label>
                <div class="col-sm-8">
                    <div class="custom-control custom-radio custom-control-inline">
                      <input type="radio" id="customRadioInline2" name="question_order" class="custom-control-input" value="1"  <?php if($tos->question_order=='1') { echo "checked=checked"; } ?>>
                      <label class="custom-control-label" for="customRadioInline2">MCQ,True/False in one section, Essay in seperate Section</label>
                    </div> <br/>
                    <div class="custom-control custom-radio custom-control-inline">
                      <input type="radio" id="customRadioInline3" name="question_order" class="custom-control-input" value="2" <?php if($tos->question_order=='2') { echo "checked=checked"; } ?>>
                      <label class="custom-control-label" for="customRadioInline3">All are in seperate Section</label>
                    </div> <br/>

                     <div class="custom-control custom-radio custom-control-inline">
                      <input type="radio" id="customRadioInline4" name="question_order" class="custom-control-input" value="3" <?php if($tos->question_order=='3') { echo "checked=checked"; } ?>>
                      <label class="custom-control-label" for="customRadioInline4">All in Single Section</label>
                    </div>
                  </div>
            </div>
          </div>
        </div>


            

         <div class="row">
              <div class="button-block clearfix" id="submitDiv">
                <div class="bttn-group">
                  <button type="submit" class="btn btn-primary">Save</button>
                </div>
              </div>
          </div>
      </div>
    </div>
  </form>
</main>

<script>
  $(document).ready(function() {
    $("#form_unit").validate({
      rules: {
        name: {
          required: true
        },
        id_pool: {
          required: true
        }
      },
      messages: {
        name: {
          required: "<p class='error-text'>Name Required</p>",
        }
      },
      errorElement: "span",
      errorPlacement: function(error, element) {
        error.appendTo(element.parent());
      }

    });
  });
</script>
<script type="text/javascript">

  function calculatemcqmarks(value) {
    if(parseInt(value)>0) {
      $("#mcq_marks").val(parseInt(value)*2);
      totalQuestion();
    }
  }

  function calculateessaymarks(value){
    if(parseInt(value)>0) {
      $("#essay_marks").val(parseInt(value)*10);
      totalQuestion();
    }
  }

  function calculatetruemarks(value){
    if(parseInt(value)>0) {
      $("#true_false_marks").val(parseInt(value)*2);
      totalQuestion();
    }
  }

  function totalQuestion(){

     var mcq_marks = $("#mcq_marks").val();
     var essay_marks = $("#essay_marks").val();
     var true_false_marks = $("#true_false_marks").val();

     var mcq_question = $("#mcq_question").val();
     var essay_question = $("#essay_question").val();
     var true_false_question = $("#true_false_question").val();

     if(mcq_question==''){
      mcq_question = 0;
     }
     if(essay_question==''){
      essay_question = 0;
     }
     if(true_false_question==''){
      true_false_question = 0;
     }


      if(mcq_marks==''){
      mcq_marks = 0;
     }
     if(essay_marks==''){
      essay_marks = 0;
     }
     if(true_false_marks==''){
      true_false_marks = 0;
     }

     $("#question_count").val(parseInt(mcq_question)+parseInt(essay_question)+parseInt(true_false_question));
     $("#total_marks").val(parseInt(mcq_marks)+parseInt(essay_marks)+parseInt(true_false_marks));


  }

 $('.number').keypress(function (e) {    
    
                var charCode = (e.which) ? e.which : event.keyCode    
    
                if (String.fromCharCode(charCode).match(/[^0-9]/g))    
    
                    return false;                        
    
            }); 


  $('select').select2();

  function reloadPage() {
    window.location.reload();
  }
</script>