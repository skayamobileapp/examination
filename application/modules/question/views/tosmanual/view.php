<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
        <h1 class="h3">Edit Manual Tos</h1>
        
        <a href='../list' class="btn btn-link ml-auto">
            <i class="fa fa-chevron-left" aria-hidden="true"></i>
          Back
        </a>

    </div>
    
    <form id="form_main" action="" method="post">

    

            <div class="form-container">


            <div class="clearfix">

                <ul class="nav nav-tabs" role="tablist" >
                    <li role="presentation" class="nav-item"><a href="#tab_two" class="nav-link active"
                            aria-controls="tab_two" role="tab" data-toggle="tab">Tagged Questions</a>
                    </li>
                </ul>
                 <div class="tab-content offers-tab-content">



                    <div role="tabpanel" class="tab-pane" id="tab_one">

                          
                    </div>

                     <div role="tabpanel" class="tab-pane active" id="tab_two">
                        <div class="row">




           

                </div>

                    <div class="row">
                        <div class="col-sm-12">
                         <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
          <th>Sl. No</th>
            <th>Question Pool</th>
            <th>Module</th>
            <th>Topic</th>
            <th>Question Type</th>

            <th>Question </th>
            <th>Difficulty Level</th>
            <th>Bloom Taxanomy</th>
               <!--          <th>View</th>
            <th>Untag</th> -->

          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($quesiontosdetails)) {
            $i = 1;
            foreach ($quesiontosdetails as $record){  

             $questionid = $record->id;
              $mqmid= $record->mqid;             
          ?>
              <tr>
                <td><?php echo $i ?></td>
                 <td><?php echo $record->PoolName; ?></td>
                <td><?php echo $record->coursename; ?></td>
                <td><?php echo $record->topicname; ?></td>
                 <td><?php if($record->question_type=='1') { echo "MCQ";}
                if($record->question_type=='2') { echo "Essay";}
                if($record->question_type=='3') { echo "True / False";} ?></td>
              <td><?php echo substr($record->question, 0, 100);; ?></td>

                <td><?php echo $record->difficultyname; ?></td>
                <td><?php echo $record->bloomtaxanomyname; ?></td>
               
              
              <!--   <td><a href="/question/question/edit/<?php echo $questionid;?>" target="_blank">View</a></td>
                <td><a href="javascript:fndelete(<?php echo $mqmid;?>)">Untag</a></td> -->

               
              </tr>
          <?php
              $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>
                    </div>

                    </div>
                    </div>
                </div>
            </div>



                
        </div>
    </form>
</main>

<script>
    $(document).ready(function() {
        $("#form_main").validate({
            rules: {
                code: {
                    required: true
                },
                name: {
                    required: true
                }
            },
            messages: {
                code: {
                    required: "<p class='error-text'>Code Required</p>",
                },
                name: {
                    required: "<p class='error-text'>Name Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });

    });
</script>

<script type="text/javascript">

  function fndelete(id) {
    var cnf= confirm("Do you really want to untag?");
    if(cnf==true){

      $.ajax({
        url: "<?php echo site_url('/question/tosmanual/deleteids'); ?>",
        method: "POST",
        data: {
          id: id
        },
        success: function(data) {
          location.reload();
        }
      });


    }
  }
   function typeofExam(type) {

      if(type=='1') {
        $("#labeldiv").html("Select Module");
      } else {
        $("#labeldiv").html("Select Programme");

      }

       $.ajax({
        url: "<?php echo site_url('/question/tosmanual/getmoduledropdown'); ?>",
        method: "POST",
        data: {
          id: type
        },
        success: function(data) {
          $("#idcourseDiv").html(data);
        }
      });
    }

        $(document).ready(function(){
          typeofExam(1);
 
            $('#id_course').change(function(){ 
                var id=$(this).val();
                $.ajax({
                    url : "<?php echo site_url('setup/ajax/getCategoryTopics');?>",
                    method : "POST",
                    data : {id: id},
                    async : true,
                    dataType : 'json',
                    success: function(data){
                         
                        var html = '';
                        var i;
                        for(i=0; i<data.length; i++){
                            html += '<option value='+data[i].id+'>'+data[i].name+'-'+data[i].name+'</option>';
                        }
                        $('#id_topic').html(html);
 
                    }
                });
                return false;
            }); 
             
        });


         function validate()
  {
    var statuscheck = $("#checkAll").is(':checked');

    if(statuscheck==true)
    {
        $('input:checkbox').prop('checked', true);
    }

    if(statuscheck==false)
    {
        $('input:checkbox').prop('checked', false);
    }    
  }


    </script>