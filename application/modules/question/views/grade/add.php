<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
        <h1 class="h3">Add Grade</h1>
        
        <a href='list' class="btn btn-link ml-auto">
            <i class="fa fa-chevron-left" aria-hidden="true"></i>
          Back
        </a>

    </div>
    
    <form id="form_main" action="" method="post">

        <div class="page-container">

          <div>
            <h4 class="form-title">Grade details</h4>
          </div>

            <div class="form-container">


                <div class="row">

                  <div class="col-lg-6">
            <div class="form-group row">
              <label class="col-sm-4 col-form-label">Module <span class="text-danger">*</span></label>
              <div class="col-sm-8">
                <select name="id_course" id="id_course" class="form-control">
                  <option value="">Select</option>
                  <?php
                  if (!empty($courseList)) {
                    foreach ($courseList as $record) { ?>
                      <option value="<?php echo $record->id;  ?>">
                        <?php echo $record->code . " - " . $record->name;  ?>
                      </option>
                  <?php
                    }
                  }
                  ?>
                </select>
              </div>
            </div>
          </div>  


                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Result <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                          <select name="name" id="name" class="form-control">
                  <option value="">Select</option>
                      <option value="PASS">PASS</option>
                      <option value="FAIL">FAIL</option>
                  ?>
                </select>

                        </div>
                      </div>
                    </div>



                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Min. Percentage <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="number" class="form-control" id="min_percentage" name="min_percentage" placeholder="">
                        </div>
                      </div>
                    </div>

              

                    <div class="col-lg-6">
                      <div class="form-group row">
                        <label class="col-sm-4 col-form-label">Max. Percentage <span class="text-danger">*</span></label>
                        <div class="col-sm-8">
                        <input type="number" class="form-control" id="max_percentage" name="max_percentage" placeholder="">
                        </div>
                      </div>
                    </div>

                </div>       


                  
                <div class="button-block clearfix">
                  <div class="bttn-group">
                      <button type="submit" class="btn btn-primary">Save</button>
                      <a href='add' class="btn btn-link">Clear All Fields</a>
                      
                  </div>

                </div> 

            </div>                                
        </div>
    </form>
</main>

<script>
    $(document).ready(function() {
        $("#form_main").validate({
            rules: {
                min_percentage: {
                    required: true
                },
                name: {
                    required: true
                },
                max_percentage: {
                    required: true
                }
            },
            messages: {
                min_percentage: {
                    required: "<p class='error-text'>Min. Percentage Required</p>",
                },
                name: {
                    required: "<p class='error-text'>Name Required</p>",
                },
                max_percentage: {
                    required: "<p class='error-text'>Max. Percentage Required/p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>
<script type="text/javascript">
    function reloadPage()
    {
      window.location.reload();
    }
</script>