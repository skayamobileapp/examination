<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
  <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
    <h1 class="h3">Grade List</h1>
      
        <a href='../list' class="btn btn-link ml-auto">
            <i class="fa fa-chevron-left" aria-hidden="true"></i>
          Back
        </a>

  </div>

    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Sl. No</th>
            <th>Min Percentage</th>
            <th>Max Percentage</th>
            <th>Result</th>
          
          
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($gradeList)) {
            $i = 1;
            foreach ($gradeList as $record) {
          ?>
              <tr>
                <td><?php echo $i ?></td>

                <td><?php echo $record->min_percentage; ?></td>
                <td><?php echo $record->max_percentage; ?></td>
              
                                <td><?php echo $record->name; ?></td>

               
              </tr>
          <?php
              $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
</main>
<script>
  $('select').select2();

  function clearSearchForm() {
    window.location.reload();
  }
</script>