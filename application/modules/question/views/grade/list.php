<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
  <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
    <h1 class="h3">Grade List</h1>
        <a href="add" class="btn btn-primary ml-auto">+ Add Grade</a>

  </div>
  <div class="page-container">

    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Sl. No</th>
            <th>Module Name</th>
            <th>Module Code</th>
          
            <th>Created By</th>
            <th>Updated By</th>
            <th>Created Date</th>
            <th>Updated Date</th>
            <!-- <th>Name In Other Language</th> -->
            <th class="text-center">Status</th>
            <th style="text-align: center;">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($gradeList)) {
            $i = 1;
            foreach ($gradeList as $record) {
          ?>
              <tr>
                <td><?php echo $i ?></td>

                <td><?php echo $record->coursename; ?></td>
                <td><?php echo $record->coursecode; ?></td>
              
                <td><?php echo $record->created_by; ?></td>
                <td><?php echo $record->updated_by; ?></td>
                <td><?php if($record->created_dt_tm){ echo date('d-m-Y', strtotime($record->created_dt_tm)); } ?></td>
                <td><?php if($record->updated_dt_tm){ echo date('d-m-Y', strtotime($record->updated_dt_tm)); } ?></td>
                <td style="text-align: center;">
                  <?php if ($record->status == '1')
                  {
                    echo "Active";
                  } else {
                    echo "In-Active";
                  }
                  ?>
                </td>
                <td class="text-center">

                  <a href="<?php echo 'edit/' . $record->id_course; ?>" class="btn" type="" data-toggle="tooltip" data-placement="top" title="Edit">
                    <i class="fa fa-eye" aria-hidden="true">
                    </i>
                  </a>

                </td>
              </tr>
          <?php
              $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
</main>
<script>
  $('select').select2();

  function clearSearchForm() {
    window.location.reload();
  }
</script>