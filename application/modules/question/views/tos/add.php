<style>
  .error {
    display: grid;
  }
</style>
<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
  <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
    <h1 class="h3">Add TOS</h1>

    <a href='../list' class="btn btn-link ml-auto">
      <i class="fa fa-chevron-left" aria-hidden="true"></i>
      Back
    </a>

  </div>

  <form id="form_unit" action="" method="post">

    <div class="page-container">

      <div>
        <h4 class="form-title">Table of Specifications</h4>
      </div>

      <div class="form-container">


        <div class="row">
          <div class="col-lg-6">
            <div class="form-group row">
              <label class="col-sm-4 col-form-label">Name<span class="text-danger">*</span></label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="name" name="name">
              </div>
            </div>
          </div>
          <div class="col-lg-6">
            <div class="form-group row">
              <label class="col-sm-4 col-form-label">Question Pool <span class="text-danger">*</span></label>
              <div class="col-sm-8">
                <select name="id_pool[]" id="id_pool" class="form-control" multiple>
                  <option value="">Select</option>
                  <?php
                  if (!empty($poolList)) {
                    foreach ($poolList as $record) { ?>
                      <option value="<?php echo $record->id;  ?>">
                        <?php echo $record->code . " - " . $record->name;  ?>
                      </option>
                  <?php
                    }
                  }
                  ?>
                </select>
              </div>
            </div>
          </div>
        
          
        </div>
        <div class="row">
              <div class="col-lg-6">
            <div class="form-group row">
              <label class="col-sm-4 col-form-label">Number of Questions<span class="text-danger">*</span></label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="question_count" name="question_count" readonly>
              </div>
            </div>
          </div>

           <div class="col-lg-6">
            <div class="form-group row">
              <label class="col-sm-4 col-form-label">Total Marks<span class="text-danger">*</span></label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="total_marks" name="total_marks" readonly>
              </div>
            </div>
          </div>

          </div>

           <div class="row">
              <div class="col-lg-6">
            <div class="form-group row">
              <label class="col-sm-4 col-form-label">Display of Question<span class="text-danger">*</span></label>
                <div class="col-sm-8">
                    <div class="custom-control custom-radio custom-control-inline">
                      <input type="radio" id="customRadioInline2" name="question_order" class="custom-control-input" value="1" checked="checked">
                      <label class="custom-control-label" for="customRadioInline2">MCQ,True/False in one section, Essay in seperate Section</label>
                    </div> <br/>
                    <div class="custom-control custom-radio custom-control-inline">
                      <input type="radio" id="customRadioInline3" name="question_order" class="custom-control-input" value="2">
                      <label class="custom-control-label" for="customRadioInline3">All are in seperate Section</label>
                    </div> <br/>

                     <div class="custom-control custom-radio custom-control-inline">
                      <input type="radio" id="customRadioInline4" name="question_order" class="custom-control-input" value="3">
                      <label class="custom-control-label" for="customRadioInline4">All in Single Section</label>
                    </div>
                  </div>
            </div>
          </div>

           <div class="col-lg-6">
            <div class="form-group row">
              <label class="col-sm-4 col-form-label">Status<span class="text-danger">*</span></label>
              <div class="col-sm-8">
                <select name="status" id="status" class="form-control">
                  <option value="">Select</option>
                  <option value="1">Active</option>
                  <option value="2">In-Active</option>
                  <option value="3">Draft</option>
                  
                </select>
              </div>
            </div>
          </div>

          </div>
        </div>
        <!-- <input type="hidden" value="" id="questionList"> -->
        <div class="button-block clearfix">
          <div class="bttn-group">
            <button type="button" class="btn btn-primary" id="fetchQuestions">Fetch Questions</button>
          </div>
        </div>

      </div>
      <div>
        <h4 class="form-title">Question Details</h4>
      </div>
      <div class="form-container">

          <ul class="nav nav-tabs" role="tablist" >
                    <li role="presentation" class="nav-item" ><a href="#tab_one" class="nav-link active"
                            aria-controls="tab_one" aria-selected="true"
                            role="tab" data-toggle="tab">MCQ</a>
                    </li>
                    <li role="presentation" class="nav-item"><a href="#tab_two" class="nav-link"
                            aria-controls="tab_two" role="tab" data-toggle="tab">Essay</a>
                    </li>
                     <li role="presentation" class="nav-item"><a href="#tab_three" class="nav-link"
                            aria-controls="tab_three" role="tab" data-toggle="tab">True or False</a>
                    </li>
                </ul>
                  <div class="tab-content offers-tab-content">



                    <div role="tabpanel" class="tab-pane active" id="tab_one">
                      

                       <div class="custom-table" id="getTosQuestionsDiv1">

                        </div>

                    </div>

                    <div role="tabpanel" class="tab-pane" id="tab_two">
                       <div class="custom-table" id="getTosQuestionsDiv2">

                        </div>
                    </div>

                    <div role="tabpanel" class="tab-pane" id="tab_three">
                      <div class="custom-table" id="getTosQuestionsDiv3">

                        </div>
                    </div>

                </div>
            </div>


        <div class="col-12">
         

        </div>
        <div class="button-block clearfix" id="submitDiv">
          <div class="bttn-group">
            <button type="submit" class="btn btn-primary">Save</button>
            <!-- <a onclick="reloadPage()" class="btn btn-link">Clear All Fields</a> -->
            <!-- <button onclick="reloadPage()" class="btn btn-link">Clear All Fields</button> -->
          </div>
        </div>
      </div>
    </div>
  </form>
</main>

<script>

  function validateMCQQuestions(question_type){
    var numberofmcq = $("#mcq_total_questions"+question_type).val();

    var enter_mcq_questions = $("#enter_mcq_questions"+question_type).val();

    if(parseInt(enter_mcq_questions)>parseInt(numberofmcq)) {
      alert("Enter max upto "+ numberofmcq);
      $("#enter_mcq_questions"+question_type).val('0');
      $("#marks_mcq_questions"+question_type).val('0');
    } else { 
      if(question_type=='1') {
        var marks = 2;
      } 
       if(question_type=='2') {
        var marks = 10;
      } 
      if(question_type=='3') {
        var marks = 2;
      } 

      $("#marks_mcq_questions"+question_type).val(enter_mcq_questions*marks);
    }


  }

  function validateQtn(id,value,question_type) {
     var maxquestionentered = $("#"+value).val();
     console.log(maxquestionentered);
     if(parseInt(id)>parseInt(maxquestionentered)) {
        alert("Enter max upto "+ maxquestionentered);
      $("#a"+value).val('0');

       $.ajax({
            url: "<?php echo site_url('/question/tos/getcountentered'); ?>",
            method: "POST",
            data: {
              count: 0,
              questionid: value,
              question_type:question_type
            },
            success: function(data) {
             showpending(data);
            }
          });

     } else {

         $.ajax({
            url: "<?php echo site_url('/question/tos/getcountentered'); ?>",
            method: "POST",
            data: {
              count: id,
              questionid: value,
              question_type:question_type
            },
            success: function(data) {
             showpending(data,question_type);

               

               var pendingquestion = $("#penques"+question_type).val();

               if(parseInt(pendingquestion)<0) {
                  $.ajax({
                      url: "<?php echo site_url('/question/tos/getcountentered'); ?>",
                      method: "POST",
                      data: {
                        count: 0,
                        questionid: value,
                        question_type:question_type
                      },
                      success: function(data) {
                              $("#a"+value).val('0');

                       showpending(data,question_type);
                      }
                    });
               }

                 
            }
          });


     }




  } 

  function showpending(entered,question_type) {
          var enter_mcq_questions = $("#enter_mcq_questions"+question_type).val();
          $("#selques"+question_type).val(entered);


          var pendingquestion = parseInt(enter_mcq_questions) - parseInt(entered);

          $("#penques"+question_type).val(pendingquestion);


          var selectedeasy = $("#enter_mcq_questions1").val();
          if(selectedeasy=='') {
            selectedeasy=0;
          }
          var selectedmedium = $("#enter_mcq_questions2").val();
                    if(selectedmedium=='') {
            selectedmedium=0;
          }
          var selecteddifficult = $("#enter_mcq_questions3").val();
                    if(selecteddifficult=='') {
            selecteddifficult=0;
          }
          var totalQuestion = parseInt(selectedeasy)+parseInt(selectedmedium)+parseInt(selecteddifficult);

          var markedeasy = $("#marks_mcq_questions1").val();
                    if(markedeasy=='') {
            markedeasy=0;
          }
          var markedmedium = $("#marks_mcq_questions2").val();
                    if(markedmedium=='') {
            markedmedium=0;
          }
          var markeddifficult = $("#marks_mcq_questions3").val();
                    if(markeddifficult=='') {
            markeddifficult=0;
          }
          var markedQuestion = parseInt(markedeasy)+parseInt(markedmedium)+parseInt(markeddifficult);


          
            $("#total_marks").val(markedQuestion);
           $("#question_count").val(totalQuestion);


  }
  $(document).ready(function() {
    $("#form_unit").validate({
      rules: {
        name: {
          required: true
        },
        id_pool: {
          required: true
        }
      },
      messages: {
        name: {
          required: "<p class='error-text'>Name Required</p>",
        },
        id_pool: {
          required: "<p class='error-text'>Pool Required</p>",
        }
      },
      errorElement: "span",
      errorPlacement: function(error, element) {
        error.appendTo(element.parent());
      }

    });
    
    $('#fetchQuestions').click(function() {
      var selectedOptions = $("#id_pool option:selected").map(function() {
        return this.value
      }).get().join(",");
      $.ajax({
        url: "<?php echo site_url('/question/tos/getTosQuestions'); ?>",
        method: "POST",
        data: {
          pools: selectedOptions,
          question_type : 1
        },
        success: function(data) {
         
          $('#getTosQuestionsDiv1').html(data);
        }
      });


      $.ajax({
        url: "<?php echo site_url('/question/tos/getTosQuestions'); ?>",
        method: "POST",
        data: {
          pools: selectedOptions,
          question_type : 2
        },
        success: function(data) {
         
          $('#getTosQuestionsDiv2').html(data);
        }
      });


      $.ajax({
        url: "<?php echo site_url('/question/tos/getTosQuestions'); ?>",
        method: "POST",
        data: {
          pools: selectedOptions,
          question_type : 3
        },
        success: function(data) {
         
          $('#getTosQuestionsDiv3').html(data);
        }
      });
    });



  });
</script>
<script type="text/javascript">
  $('select').select2();

  function reloadPage() {
    window.location.reload();
  }
</script>