 <div class="container-fluid">
      <div class="row">
        <nav id="sidebarMenu" class="col-md-3 d-md-block sidebar collapse">
          <div class="sidebar-sticky pt-3">
            <div class="nav-fold">
              <a href="#" class="d-flex align-items-center">
                <span class="user-avatar"><img src="<?php echo BASE_PATH; ?>assets/img/user.jpg"/></span>
                <span>
                  <?php echo $name; ?>
                  <small class="d-block"><?php echo $role; ?></small>
                  <small class="d-block"></small>                  
                </span>
              </a>
            </div>
            <ul class="nav flex-column mb-2">
             
              
              
              <li class="nav-item">
                <a class="nav-link  <?php 
                  if(in_array($pageCode,array('course.list','course.add','course.edit','topic.list','topic.add','topic.edit','learningobjective.list','learningobjective.add','learningobjective.edit','taxonomy.list','taxonomy.add','taxonomy.edit','difficultylevel.list','difficultylevel.add','difficultylevel.edit','question.list','question.add','question.edit','questionhasoption.add','questionhasoption.edit','questionpool.list','questionpool.add','questionpool.edit','tos.add','tos.list','tos.edit','examset.add','examset.list','examset.edit','grade.add','grade.list','grade.edit','tosmanualquestion.add','tosmanualquestion.list','tosmanualquestion.edit'))){  ?>
                  <?php 
                  } else{ 
                  ?>
                  collapsed
                  <?php
                  }
                  ?>" data-toggle="collapse" href="#collapseResults" role="button">
                  <i class="fa fa-book"></i>
                  <span>Question Block</span>
                  <i class="fa fa-angle-right ml-auto" aria-hidden="true"></i>
                  <i class="fa fa-angle-down ml-auto" aria-hidden="true"></i>
                </a>
                <ul class="collapse nav <?php 
                  if(in_array($pageCode,array('course.list','course.add','course.edit','programme.list','programme.add','programme.edit','topic.list','topic.add','topic.edit','learningobjective.list','learningobjective.add','learningobjective.edit','taxonomy.list','taxonomy.add','taxonomy.edit','difficultylevel.list','difficultylevel.add','difficultylevel.edit','question.list','question.add','question.edit','questionhasoption.add','questionhasoption.edit','questionpool.list','questionpool.add','questionpool.edit','tos.add','tos.list','tos.edit','examset.add','examset.list','examset.edit','grade.add','grade.list','grade.edit','tosmanual.list','tosmanual.add','tosmanual.edit','questionbulk.list','questionbulk.edit','questionbulk.add','tosmanualquestion.add','tosmanualquestion.list','tosmanualquestion.edit'))){  ?>
                  show
                 <?php 
                  } else{ 
                  ?>
                  
                  <?php
                  }
                  ?>" id="collapseResults">

                    <li class="nav-item">
                    <a href="/question/programme/list" class="nav-link <?php if(in_array($pageCode,array('programme.list','programme.edit','programme.add'))){echo 'active';}?>">
                      <i class="fa fa-chevron-right"></i>

                      <span>Programme</span>                      
                    </a>
                  </li>

                  <li class="nav-item">
                    <a href="/question/course/list" class="nav-link <?php if(in_array($pageCode,array('course.list','course.edit','course.add'))){echo 'active';}?>">
                      <i class="fa fa-chevron-right"></i>

                      <span>Module</span>                      
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="/question/topic/list" class="nav-link <?php if(in_array($pageCode,array('topic.list','topic.edit','topic.add'))){echo 'active';}?>">
                      <i class="fa fa-chevron-right"></i>
                      <span>Chapter</span>                      
                    </a>
                  </li>  

                   <li class="nav-item">
                    <a href="/question/bloomTaxonomy/list" class="nav-link <?php if(in_array($pageCode,array('taxonomy.list','taxonomy.edit','taxonomy.add'))){echo 'active';}?>">
                      <i class="fa fa-chevron-right"></i>
                      <span>Difficulty Level</span>                      
                    </a>
                  </li>                  
<!-- 
                   <li class="nav-item">
                    <a href="/question/difficultLevel/list" class="nav-link <?php if(in_array($pageCode,array('difficultylevel.list','difficultylevel.edit','difficultylevel.add'))){echo 'active';}?>">
                      <i class="fa fa-chevron-right"></i>
                      <span>Difficulty Level</span>                      
                    </a>
                  </li> -->
                  <li class="nav-item">
                    <a href="/question/questionPool/list" class="nav-link <?php if(in_array($pageCode,array('questionpool.list','questionpool.edit','questionpool.add'))){echo 'active';}?>">
                      <i class="fa fa-chevron-right"></i>
                      <span>Question Pool</span>                      
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="/question/question/list" class="nav-link <?php if(in_array($pageCode,array('question.list','question.edit','question.add','questionhasoption.add','questionhasoption.edit'))){echo 'active';}?>">
                      <i class="fa fa-chevron-right"></i>
                      <span>Question</span>                      
                    </a>
                  </li>

                   <li class="nav-item">
                    <a href="/question/questionbulk/list" class="nav-link <?php if(in_array($pageCode,array('questionbulk.list','questionbulk.edit','questionbulk.add'))){echo 'active';}?>">
                      <i class="fa fa-chevron-right"></i>
                      <span>Bulk Question</span>                      
                    </a>
                  </li>

               <!--    <li class="nav-item">
                    <a href="/question/tos/list" class="nav-link <?php if(in_array($pageCode,array('tos.add','tos.list','tos.edit'))){echo 'active';}?>">
                      <i class="fa fa-chevron-right"></i>
                      <span>Auto TOS</span>                      
                    </a>
                  </li>
 -->
                  <li class="nav-item">
                    <a href="/question/tosmanual/list" class="nav-link <?php if(in_array($pageCode,array('tosmanual.add','tosmanual.list','tosmanual.edit'))){echo 'active';}?>">
                      <i class="fa fa-chevron-right"></i>
                      <span>Manual TOS</span>                      
                    </a>
                  </li>


                 <!--    <li class="nav-item">
                    <a href="/question/tosmanualquestion/list" class="nav-link <?php if(in_array($pageCode,array('tosmanualquestion.add','tosmanualquestion.list','tosmanualquestion.edit'))){echo 'active';}?>">
                      <i class="fa fa-chevron-right"></i>
                      <span>Manual Question Exam Set</span>                      
                    </a>
                  </li>
 -->


                  <li class="nav-item">
                    <a href="/question/examset/list" class="nav-link <?php if(in_array($pageCode,array('examset.add','examset.list','examset.edit'))){echo 'active';}?>">
                      <i class="fa fa-chevron-right"></i>
                      <span>Exam Set</span>                      
                    </a>
                  </li>

                  <li class="nav-item">
                    <a href="/question/grade/list" class="nav-link <?php if(in_array($pageCode,array('grade.add','grade.list','grade.edit'))){echo 'active';}?>">
                      <i class="fa fa-chevron-right"></i>
                      <span>Grade</span>                      
                    </a>
                  </li>
                </ul>
                             
          </ul>         
        </div>
      </nav>
    </div>
  </div>