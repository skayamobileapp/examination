<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
        <h1 class="h3">Edit Manual Exam Question</h1>
        
        <a href='../list' class="btn btn-link ml-auto">
            <i class="fa fa-chevron-left" aria-hidden="true"></i>
          Back
        </a>

    </div>
           

                    
                   
    <form id="form_main" action="" method="post">
 <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
           <tr>
                <th>TOS - <?php echo $tos->name;?></th>
               <th>Code - <?php echo $tos->code;?></th>
               <th>Status - 
                <?php if($tos->status=='1') { echo "Active";}
                if($tos->status=='2') { echo "In-Active";}
                if($tos->status=='3') { echo "Draft";}?></th>
               <th></th>
             </tr>

            <tr>
                <th>Question Type</th>
               <th>MCQ Question</th>
               <th>Essay Question</th>
               <th>True / False Question</th>
             </tr>
          </thead>
          <tbody>
            <tr>
              <td>Assigned </td>
               <td><?php echo $tos->mcq_question;?></td>
               <td><?php echo $tos->essay_question;?></td>
               <td><?php echo $tos->true_false_question;?></td>
             </tr>

             <tr>
              <td>Tagged </td>
               <td><?php echo $totalAssignedMCQ;?></td>
               <td><?php echo $totalAssignedEssay;?></td>
               <td><?php echo $totalAssignedTrue;?></td>
             </tr>
              <tr>
              <td>Pending </td>
               <td><?php echo ($tos->mcq_question-$totalAssignedMCQ);?>
                 <input type='hidden' name='pending_mcq' value="<?php echo $tos->mcq_question-$totalAssignedMCQ;?>" />
               </td>
               <td><?php echo ($tos->essay_question-$totalAssignedEssay);?>
                 <input type='hidden' name='pending_essay' value="<?php echo $tos->essay_question-$totalAssignedEssay;?>" />
               </td>
               <td><?php echo ($tos->true_false_question-$totalAssignedTrue);?>
                 <input type='hidden' name='pending_true' value="<?php echo $tos->true_false_question-$totalAssignedTrue;?>" />
               </td>
             </tr>
         </tbody>
       </table>
     </div>
    
  <div class="tab-content offers-tab-content">
  <div class="tabs-view">
             <h4><a href="/question/tosmanualquestion/edittos/<?php echo $tosid;?>" >Add Question</a></h4>
            <h4><a href="/question/tosmanualquestion/view/<?php echo $tosid;?>" class="active">Question List</a></h4>
          </div>
                 
            <div class="">


            <div class="clearfix">
                <div class="d-flex justify-content-between mb-3 align-items-center">
                    <h4></h4>
                      <a href="/question/tosmanualquestion/edittos/<?php echo $tosid;?>" class="btn btn-primary">Add Question</a>
                </div>

                 <?php
          if (!empty($quesiontosdetails)) {
            $j = 1;
            foreach ($quesiontosdetails as $record){  

             $questionid = $record->id;
                    $this->load->model('question_model');


                    $questionHasOption = $this->question_model->getQuestionHasOption($questionid);




            

              $mqmid= $record->mqid;             
          ?>

                <div class="card mb-3">
                  <h5 class="card-header d-flex justify-content-between align-items-center">
                      <?php echo  $record->question;?>

                    <a href="/question/tosmanualquestion/edit/<?php echo $record->id;?>" class="ml-auto btn btn-outline-primary btn-sm">Edit</a>


                  </h5>
                  <div class="card-body">

                    <div class="row">

                         <?php  for($i=0;$i<count($questionHasOption);$i++)
                                             { ?>

                        <div class="col">
                            <div class="custom-control custom-radio">
                              <input type="radio" id="customRadio21" name="customRadio2" class="custom-control-input" disabled   <?php if($questionHasOption[$i]->is_correct_answer=='1') { echo "checked=checked"; } ?> >
                              <label class="custom-control-label" for="customRadio21"><?php echo $questionHasOption[$i]->option_description;?></label>
                            </div>
                        </div>
                           
                             <?php } ?>                   
                    </div>
    
                  </div>
                </div> 
            <?php } } ?>
                
               
                </div>
            </div>



                
        </div>
         </div>
    </form>
</main>

<script>
    $(document).ready(function() {
        $("#form_main").validate({
            rules: {
                code: {
                    required: true
                },
                name: {
                    required: true
                }
            },
            messages: {
                code: {
                    required: "<p class='error-text'>Code Required</p>",
                },
                name: {
                    required: "<p class='error-text'>Name Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });

    });
</script>

<script type="text/javascript">

  function fndelete(id) {
    var cnf= confirm("Do you really want to untag?");
    if(cnf==true){

      $.ajax({
        url: "<?php echo site_url('/question/tosmanual/deleteids'); ?>",
        method: "POST",
        data: {
          id: id
        },
        success: function(data) {
          location.reload();
        }
      });


    }
  }
   function typeofExam(type) {

      if(type=='1') {
        $("#labeldiv").html("Select Module");
      } else {
        $("#labeldiv").html("Select Programme");

      }

       $.ajax({
        url: "<?php echo site_url('/question/tosmanual/getmoduledropdown'); ?>",
        method: "POST",
        data: {
          id: type
        },
        success: function(data) {
          $("#idcourseDiv").html(data);
        }
      });
    }

        $(document).ready(function(){
          typeofExam(1);
 
            $('#id_course').change(function(){ 
                var id=$(this).val();
                $.ajax({
                    url : "<?php echo site_url('setup/ajax/getCategoryTopics');?>",
                    method : "POST",
                    data : {id: id},
                    async : true,
                    dataType : 'json',
                    success: function(data){
                         
                        var html = '';
                        var i;
                        for(i=0; i<data.length; i++){
                            html += '<option value='+data[i].id+'>'+data[i].name+'-'+data[i].name+'</option>';
                        }
                        $('#id_topic').html(html);
 
                    }
                });
                return false;
            }); 
             
        });


         function validate()
  {
    var statuscheck = $("#checkAll").is(':checked');

    if(statuscheck==true)
    {
        $('input:checkbox').prop('checked', true);
    }

    if(statuscheck==false)
    {
        $('input:checkbox').prop('checked', false);
    }    
  }


    </script>