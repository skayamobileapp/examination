<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
        <h1 class="h3">Edit Manual Exam Question</h1>
        
        <a href='../list' class="btn btn-link ml-auto">
            <i class="fa fa-chevron-left" aria-hidden="true"></i>
          Back
        </a>

    </div>
    
    <form id="form_main" action="" method="post">

    

            <div class="">


            <div class="clearfix">
                <div class="d-flex justify-content-between mb-3 align-items-center">
                    <h4>Questions List</h4>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addQuestionModal">Add Question</button>
                </div>
                <div class="card mb-3">
                  <h5 class="card-header d-flex justify-content-between align-items-center">
                      1. Question
                      <button type="button" class="ml-auto btn btn-outline-primary btn-sm" data-toggle="modal" data-target="#addQuestionModal">Edit</button>
                  </h5>
                  <div class="card-body">

                    <div class="row">
                        <div class="col">
                            <div class="custom-control custom-radio">
                              <input checked type="radio" id="customRadio1" name="customRadio" class="custom-control-input" disabled>
                              <label class="custom-control-label" for="customRadio1">Option 1</label>
                            </div>
                        </div>
                        <div class="col">
                            <div class="custom-control custom-radio">
                              <input type="radio" id="customRadio2" name="customRadio" class="custom-control-input" disabled>
                              <label class="custom-control-label" for="customRadio2">Option 2</label>
                            </div>
                        </div> 
                        <div class="col">
                            <div class="custom-control custom-radio">
                              <input type="radio" id="customRadio3" name="customRadio" class="custom-control-input" disabled>
                              <label class="custom-control-label" for="customRadio3">Option 3</label>
                            </div>
                        </div>
                        <div class="col">
                            <div class="custom-control custom-radio">
                              <input type="radio" id="customRadio4" name="customRadio" class="custom-control-input" disabled>
                              <label class="custom-control-label" for="customRadio4">Option 4</label>
                            </div>
                        </div>                         
                    </div>
    
                  </div>
                </div> 
                <div class="card mb-3">
                  <h5 class="card-header d-flex justify-content-between align-items-center">
                      2. Question
                      <button type="button" class="ml-auto btn btn-outline-primary btn-sm" data-toggle="modal" data-target="#addQuestionModal">Edit</button>
                  </h5>
                  <div class="card-body">

                    <div class="row">
                        <div class="col">
                            <div class="custom-control custom-radio">
                              <input type="radio" id="customRadio21" name="customRadio2" class="custom-control-input" disabled>
                              <label class="custom-control-label" for="customRadio21">Option 1</label>
                            </div>
                        </div>
                        <div class="col">
                            <div class="custom-control custom-radio">
                              <input type="radio" id="customRadio22" name="customRadio2" class="custom-control-input" disabled>
                              <label class="custom-control-label" for="customRadio22">Option 2</label>
                            </div>
                        </div> 
                        <div class="col">
                            <div class="custom-control custom-radio">
                              <input checked type="radio" id="customRadio23" name="customRadio2" class="custom-control-input" disabled>
                              <label class="custom-control-label" for="customRadio23">Option 3</label>
                            </div>
                        </div>
                        <div class="col">
                            <div class="custom-control custom-radio">
                              <input type="radio" id="customRadio24" name="customRadio2" class="custom-control-input" disabled>
                              <label class="custom-control-label" for="customRadio24">Option 4</label>
                            </div>
                        </div>                         
                    </div>
    
                  </div>
                </div> 
                
                
                
                
                <!-- Add Question Modal -->
                <div class="modal fade" id="addQuestionModal" tabindex="-1"  aria-hidden="true">
                  <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title">Add/Edit Question</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group row">
                                  <label class="col-sm-4 col-form-label">Marks <span class="text-danger">*</span></label>
                                  <div class="col-sm-8">
                                    <input type="number" class="form-control" id="marks" name="marks" min="1" value="">
                                  </div>
                                </div>
                            </div>  
                            <div class="col-lg-6">
                                <div class="form-group row">
                                  <label class="col-sm-4 col-form-label">Marks <span class="text-danger">*</span></label>
                                  <div class="col-sm-8">
                                    <input type="number" class="form-control" id="marks" name="marks" min="1" value="">
                                  </div>
                                </div>
                            </div>                             
                        </div>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-primary">Submit</button>
                      </div>                      
                    </div>
                  </div>
                </div>                
                
                
                
                
                 <div class="tab-content offers-tab-content">
  <div class="tabs-view">
             <h4><a href="/question/tosmanualquestion/edittos/<?php echo $tosid;?>" >Add Question</a></h4>
            <h4><a href="/question/tosmanualquestion/view/<?php echo $tosid;?>" class="active">Question List</a></h4>
          </div>
                        

                    <div class="row">
                        <div class="col-sm-12">
                         <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
          <th>Sl. No</th>
          
            <th>Question </th>
           
          </tr>
        </thead>
        <tbody>
          <?php
          if (!empty($quesiontosdetails)) {
            $j = 1;
            foreach ($quesiontosdetails as $record){  

             $questionid = $record->id;
                    $this->load->model('question_model');


                    $questionHasOption = $this->question_model->getQuestionHasOption($questionid);




            

              $mqmid= $record->mqid;             
          ?>
              <tr>
                <td><?php echo $j++; ?></td>
               
              <td><?php echo  $record->question;?>
                  
                   <?php  for($i=0;$i<count($questionHasOption);$i++)
                                             { ?>
                                               
                                                <p>
                                                  <?php if($questionHasOption[$i]->is_correct_answer=='1') { echo "<b>"; } ?> 
                                                    <?php echo $questionHasOption[$i]->option_description;?>
                                                        
                                                     <?php if($questionHasOption[$i]->is_correct_answer=='1') { echo "</b>"; } ?> 

                                                    </p>


                                              <?php } ?> 

              </td>

            
               
              </tr>
          <?php
              $i++;
            }
          }
          ?>
        </tbody>
      </table>
    </div>
                    </div>

                    </div>
                    </div>
                </div>
            </div>



                
        </div>
    </form>
</main>

<script>
    $(document).ready(function() {
        $("#form_main").validate({
            rules: {
                code: {
                    required: true
                },
                name: {
                    required: true
                }
            },
            messages: {
                code: {
                    required: "<p class='error-text'>Code Required</p>",
                },
                name: {
                    required: "<p class='error-text'>Name Required</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });

    });
</script>

<script type="text/javascript">

  function fndelete(id) {
    var cnf= confirm("Do you really want to untag?");
    if(cnf==true){

      $.ajax({
        url: "<?php echo site_url('/question/tosmanual/deleteids'); ?>",
        method: "POST",
        data: {
          id: id
        },
        success: function(data) {
          location.reload();
        }
      });


    }
  }
   function typeofExam(type) {

      if(type=='1') {
        $("#labeldiv").html("Select Module");
      } else {
        $("#labeldiv").html("Select Programme");

      }

       $.ajax({
        url: "<?php echo site_url('/question/tosmanual/getmoduledropdown'); ?>",
        method: "POST",
        data: {
          id: type
        },
        success: function(data) {
          $("#idcourseDiv").html(data);
        }
      });
    }

        $(document).ready(function(){
          typeofExam(1);
 
            $('#id_course').change(function(){ 
                var id=$(this).val();
                $.ajax({
                    url : "<?php echo site_url('setup/ajax/getCategoryTopics');?>",
                    method : "POST",
                    data : {id: id},
                    async : true,
                    dataType : 'json',
                    success: function(data){
                         
                        var html = '';
                        var i;
                        for(i=0; i<data.length; i++){
                            html += '<option value='+data[i].id+'>'+data[i].name+'-'+data[i].name+'</option>';
                        }
                        $('#id_topic').html(html);
 
                    }
                });
                return false;
            }); 
             
        });


         function validate()
  {
    var statuscheck = $("#checkAll").is(':checked');

    if(statuscheck==true)
    {
        $('input:checkbox').prop('checked', true);
    }

    if(statuscheck==false)
    {
        $('input:checkbox').prop('checked', false);
    }    
  }


    </script>