<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard_model extends CI_Model
{
    function getGrader($id)
    {
        $this->db->select('*');
        $this->db->from('grader');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }
}