<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Grader_exam_model extends CI_Model
{
    function salutationListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('salutation_setup');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function examCenterListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('exam_center');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function graderExamListSearch($data)
    {
        $this->db->select('a.*');
        $this->db->from('grader as a');
        // if ($data['name'] != '')
        // {
        //     $likeCriteria = "(a.full_name  LIKE '%" . $data['name'] . "%' or a.nric  LIKE '%" . $data['name'] . "%')";
        //     $this->db->where($likeCriteria);
        // }
        if($data['id_grader'])
        {
            $this->db->where('a.id', $id);
        }
        $this->db->order_by("a.full_name", "ASC");
         $query = $this->db->get();
         $result = $query->result();
         return $result;
    }
    
    function getGrader($id)
    {
        $this->db->select('*');
        $this->db->from('grader');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function getGraderExamList($id_grader)
    {
        $this->db->select('gm.*, e.name as exam_center_name, ee.name as exam_event_name, ee.exam_date as exam_event_date');
        $this->db->from('grader_exam as gm');
        $this->db->join('exam_center as e','gm.id_exam_center = e.id');
        $this->db->join('exam_event as ee','gm.id_exam_event = ee.id');
        $this->db->where('gm.id_grader', $id_grader);
        // $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }
    
    function addNewGraderExam($data)
    {
        $this->db->trans_start();
        $this->db->insert('grader_exam', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function deleteGraderModule($id)
    {
        $this->db->where('id', $id);
        $result = $this->db->delete('grader_modules');
        return $result;
    }

    function getExamEventByExamCenter($id_exam_center)
    {
        $this->db->select('ee.*');
        $this->db->from('exam_event as ee');
        $this->db->where('ee.id_exam_center', $id_exam_center);
        $this->db->where('ee.status', '1');
        // $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result();  
        return $result;
    }
}