<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Grader_student_tag_model extends CI_Model
{
    function salutationListByStatus($status)
    {
        $this->db->select('*');
        $this->db->from('salutation_setup');
        $this->db->where('status', $status);
        $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function gettotalStudents($id) {
         $this->db->select('*');
        $this->db->from('grader_student_tag');
        $this->db->where('id_grader_exam', $id);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }


    function getExamEventForPdf($id_grader)
    {
        $this->db->select('ee.*,c.name as coursename,c.code as modulecode');
        $this->db->from('exam_event as ee');
        $this->db->join('course as c','ee.id_course = c.id');

        $this->db->where('ee.id', $id_grader);
        // $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->row();  
         return $result;
    }

    
    function getGraderExamList($id_grader)
    {
        $this->db->select('gm.*, e.name as exam_center_name, ee.name as exam_event_name, ee.exam_date as exam_event_date,c.name as coursename,c.code as modulecode,ee.id as exameventid');
        $this->db->from('grader_exam as gm');
        $this->db->join('exam_event as ee','gm.id_exam_event = ee.id','left');
        $this->db->join('exam_center as e','ee.id_exam_center = e.id','left');
        $this->db->join('course as c','ee.id_course = c.id');
        $this->db->where('gm.id_grader', $id_grader);
        // $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }
    
    function getGrader($id)
    {
        $this->db->select('*');
        $this->db->from('grader');
        $this->db->where('id', $id);
        $query = $this->db->get();
        return $query->row();
    }


    function getExamDetailsByIdGrader($idexamgrader) {
        $this->db->select('gm.*, e.name as exam_center_name, ee.name as exam_event_name, ee.exam_date as exam_event_date,c.name as coursename,c.code as coursecode');
        $this->db->from('grader_exam as gm');
        $this->db->join('exam_event as ee','gm.id_exam_event = ee.id');
        $this->db->join('course as c','ee.id_course = c.id');
        $this->db->join('exam_center as e','ee.id_exam_center = e.id');
        $this->db->where('gm.id', $idexamgrader);
        // $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }

    function getGraderStudentTagList($id_grader,$id_exam_event,$idexamgrader)
    {
        $this->db->select('gm.*, s.full_name as student_name, s.nric,s.membership_number, e.name as exam_center_name, ee.name as exam_event_name, ee.exam_date as exam_event_date,est.id as id_exam_student_tagging,c.name as coursename,c.code as coursecode');
        $this->db->from('grader_student_tag as gm');
        $this->db->join('student as s','gm.id_student = s.id');

         $this->db->join('exam_event as ee','gm.id_exam_event = ee.id');
         $this->db->join('exam_center as e','ee.id_exam_center = e.id');
         $this->db->join('course as c','ee.id_course = c.id');


       $this->db->join('exam_student_tagging as est','s.id = est.id_student');
        $this->db->where('gm.id_grader', $id_grader);
        $this->db->where('gm.id_grader_exam', $idexamgrader);
        $this->db->where('gm.id_exam_event', $id_exam_event);
         $query = $this->db->get();
         $result = $query->result();  
         return $result;
    }
    
    function addNewGraderStudentTag($data)
    {
        $this->db->trans_start();
        $this->db->insert('grader_student_tag', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }

    function deleteGraderModule($id)
    {
        $this->db->where('id', $id);
        $result = $this->db->delete('grader_modules');
        return $result;
    }

    function deleteStudentQuestionSetById($id) {
         $this->db->where('id_student_question_set', $id);
        $result = $this->db->delete('student_question_set_marks');
        return $result;
    }

    function getExamEventByExamCenter($id_exam_center)
    {
        $this->db->select('ee.*');
        $this->db->from('exam_event as ee');
        $this->db->where('ee.id_exam_center', $id_exam_center);
        $this->db->where('ee.status', '1');
        // $this->db->order_by("name", "ASC");
        $query = $this->db->get();
        $result = $query->result();  
        return $result;
    }

    function studentSearch($data)
    {
        $this->db->select('s.*');
        // , qs.short_name as qualification_code, qs.name as qualification_name
        $this->db->from('student as s');
        // $this->db->join('education_level as qs', 's.id_degree_type = qs.id');
        if ($data['full_name'] != '')
        {
            $likeCriteria = "(s.full_name  LIKE '%" . $data['full_name'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['email_id'] != '')
        {
            $likeCriteria = "(s.full_name  LIKE '%" . $data['email_id'] . "%')";
            $this->db->where($likeCriteria);
        }
        if ($data['id_grader'] != '')
        {
            $likeCriteria = " s.id NOT IN (SELECT cr.id_student FROM grader_student_tag cr where cr.id_grader = " . $data['id_grader'] . ")";
            $this->db->where($likeCriteria);            
        }

        // if ($data['current_semester'] != '')
        // {
        //     $this->db->where('s.current_semester', $data['current_semester']);
        // }
        // if ($data['id_program'] != '')
        // {
        //     $this->db->where('s.id_program', $data['id_program']);
        // }
        // if ($data['id_intake'] != '')
        // {
        //     $this->db->where('s.id_intake', $data['id_intake']);
        // }
        $query = $this->db->get();
        $result = $query->result(); 

        // echo "<Pre>"; print_r($this->db);exit();

        return $result;
    }

    function getShowStudentQuestionSetForGrader($id_exam_student_tagging,$idexamgrader)
    {

        // , qs.short_name as qualification_code, qs.name as qualification_name
        $this->db->select('sqs.*, q.question,q.answer_scheme,q.marks as questionmarks,s.membership_number');
        $this->db->from('student_question_set as sqs');
        $this->db->join('question as q', 'sqs.id_question = q.id');
        $this->db->join('student as s', 's.id = sqs.id_student');

        $this->db->where('sqs.id_exam_student_tagging',$id_exam_student_tagging);

        $this->db->where('q.question_type', '2');
        
        $query = $this->db->get();
        $results = $query->result(); 

        // echo "<Pre>"; print_r($this->db);exit();

        $details = array();

        foreach ($results as $result)
        {
            $id_student_question_set = $result->id;

            $student_question_set = $this->getStudentQuestionSet($id_student_question_set,$idexamgrader);


            $result->marks = '';
            if($student_question_set)
            {
                $result->marks = $student_question_set->marks;
            }

            array_push($details, $result);
        }

        return $details;
    }

    function getStudentQuestionSet($id,$idexamgrader)
    {
        $this->db->select('*');
        $this->db->from('student_question_set_marks');
        $this->db->where('id_student_question_set', $id);
        $this->db->where('id_grader_exam', $idexamgrader);
        $query = $this->db->get();
        return $query->row();
    }

    function getStudentQuestionQuestionSetForMarksAdd($id,$idexamgrader)
    {
        $this->db->select('sqs.*, q.question, q.marks as max_marks, sqsm.marks as marks_detail, sqsm.id as id_student_question_set_marks,q.answer_scheme');
        $this->db->from('student_question_set as sqs');
        $this->db->join('question as q', 'sqs.id_question = q.id');
        $this->db->join("student_question_set_marks as sqsm", "sqs.id = sqsm.id_student_question_set AND sqsm.id_grader_exam='$idexamgrader'","left");
        $this->db->where('sqs.id',$id);
        $this->db->where('q.question_type', '2');
        // $this->db->where('sqsm.id_grader_exam', $idexamgrader);
        $query = $this->db->get();
        $result = $query->row(); 
        return $result;
    }

    function addNewStudentQuestionSetMarks($data)
    {
        $this->db->trans_start();
        $this->db->insert('student_question_set_marks', $data);
        $insert_id = $this->db->insert_id();
        $this->db->trans_complete();
        return $insert_id;
    }




    function getExamStudentTagging($id)
    {
        $this->db->select('est.*, ee.id as id_exam_event, ec.id as id_exam_center');
        $this->db->from('exam_student_tagging as est');
        $this->db->join('exam_event as ee','est.id_exam_event = ee.id');        
        $this->db->join('exam_center as ec','ee.id_exam_center = ec.id');
        $this->db->where('est.id', $id);
        $query = $this->db->get();
        return $query->row();
    }

    function editStudentQuestionSetMarks($data,$id)
    {
        $this->db->where('id', $id);
        $result = $this->db->update('student_question_set_marks',$data);
        return $result;
    }

    function editGraderStudentTag($data,$id)
    {
        $this->db->where('id', $id);
        $result = $this->db->update('grader_student_tag',$data);
        return $result;
    }


     function updateGrader($data,$id)
    {
        $this->db->where('id', $id);
        $result = $this->db->update('grader',$data);
        return $result;
    }

    function updateGraderExamTagging($data,$id)
    {
        $this->db->where('id', $id);
        $result = $this->db->update('exam_student_tagging',$data);
        return $result;
    }

    function totalMCQQuestion($id_exam_student_tagging) {
        $this->db->select('est.*,q.marks as originalmarks');
        $this->db->from('student_question_set as est');
        $this->db->join('question  as q','q.id = est.id_question');        
        $this->db->where('est.id_exam_student_tagging', $id_exam_student_tagging);
        $this->db->where("q.question_type='2'");
        $query = $this->db->get();
        $results = $query->result(); 
        return $results;
    }

    function totalMCQQuestionMarked($id_exam_student_tagging,$idgraderexam){
        $this->db->select('est.*,qu.marks as originalmarks,q.marks as answermarks');
        $this->db->from('student_question_set as est');
        $this->db->join('student_question_set_marks  as q','q.id_student_question_set = est.id');       
        $this->db->join('question  as qu','qu.id = est.id_question');        

        $this->db->where('est.id_exam_student_tagging', $id_exam_student_tagging);
        $this->db->where('q.id_grader_exam', $idgraderexam);
        $this->db->where("qu.question_type='2'");        
        $query = $this->db->get();
        $results = $query->result(); 
        return $results;
    }
}