<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Profile extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('dashboard_model');
        // $this->isGraderLoggedIn();
        error_reporting(0);

    }

    public function index()
    {
      // print_r(';;sa');exit;
        $this->dashboard();
    }


    function dashboard()
    {
      $id_grader = $this->session->userdata('id_grader');
      $grader_name = $this->session->userdata('grader_name');
      $grader_email = $this->session->userdata('grader_email');

      $currentDate = date('Y-m-d');

      $data['grader'] = $this->dashboard_model->getGrader($id_grader);

      // echo "<Pre>";print_r($data['grader']);exit;
      $this->global['pageCode'] = 'grader.profile';
      $this->global['pageTitle'] = 'Examination Management System : Exam Events';
      $this->loadViews("grader_view/profile", $this->global, $data, NULL);

    }

    function logout()
    {
        // echo "string";exit(); 
      $sessionArray = array(
              'id_grader' => '',
              'grader_name' => '',
              'grader_email'=>  '',
              'grader_nric'=>  '',
              'grader_mobile'=>  '',
              'isGraderLoggedIn' => FALSE
      );

      $this->session->set_userdata($sessionArray);

     redirect('/graderLogin');
    }

}
