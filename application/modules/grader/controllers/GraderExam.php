<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class GraderExam extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('grader_exam_model');
        // $this->isGraderLoggedIn();
        error_reporting(0);

    }

    function list()
    {

    	$id_grader = $this->session->userdata('id_grader');
      	$grader_name = $this->session->userdata('grader_name');
      	$grader_email = $this->session->userdata('grader_email');

      	$formData['id_grader'] = $id_grader;

        $data['graderExamList'] = $this->grader_exam_model->getGraderExamList($id_grader);

        $this->global['pageCode'] = 'grader_exam.view';
        $this->global['pageTitle'] = 'Grader Portal : Grader Exam List';
        $this->loadViews("grader_exam/list", $this->global, $data, NULL);
    }
}