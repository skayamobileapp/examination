<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class GraderStudentTag extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('grader_student_tag_model');
        $id_grader = $this->session->userdata('id_grader');
        if($id_grader=='') {
                 redirect('/graderLogin');
        }
        // $this->isGraderLoggedIn();
        error_reporting(0);

    }

    function list()
    {
        $id_grader = $this->session->userdata('id_grader');
        $grader_name = $this->session->userdata('grader_name');
        $grader_email = $this->session->userdata('grader_email');

            
        $data['graderStudentList'] = $this->grader_student_tag_model->getGraderExamList($id_grader);

        $this->global['pageCode'] = 'grader_student_tag.list';
        $this->global['pageTitle'] = 'Examination Management System : Grader Exam List';
        $this->loadViews("grader_student_tag/list", $this->global, $data, NULL);
    }
    
    function viewStudent($idexamgrader,$id_exam_event)
    {
            
        $id_grader = $this->session->userdata('id_grader');
        $grader_name = $this->session->userdata('grader_name');
        $grader_email = $this->session->userdata('grader_email');

        if($this->input->post())
        {
            // echo "<Pre>";print_r($this->input->post());exit();
            
            $id_grader_student_tags = $this->security->xss_clean($this->input->post('id_grader_student_tag'));

            // $tm_12hr  = date("H:i", strtotime($exam_time));

            foreach ($id_grader_student_tags as $id_grader_student_tag)
            {
                $data = array(
                    'status' => 1
                );


                 $result = $this->grader_student_tag_model->updateGraderExamTagging($data,$id_grader_student_tag);
            
                $result = $this->grader_student_tag_model->editGraderStudentTag($data,$id_grader_student_tag);
            }

            if ($result > 0)
            {
                $this->session->set_flashdata('success', 'New Grader Exam Student Event Approval successfully');
            }
             else
            {
                $this->session->set_flashdata('error', 'Grader Exam Student Event Approval failed');
            }
                redirect('/grader/graderStudentTag/viewStudent/' . $idexamgrader . '/' . $id_exam_event);
        }

        $data['idexamgrader'] = $idexamgrader;
        $data['id_exam_event'] = $id_exam_event;
        $data['grader'] = $this->grader_student_tag_model->getGrader($id_grader);
        
        $data['graderStudentTagList'] = $this->grader_student_tag_model->getGraderStudentTagList($id_grader,$id_exam_event,$idexamgrader);




        $data['graderDetails'] = $this->grader_student_tag_model->getExamDetailsByIdGrader($idexamgrader);


        $data['salutationList'] = $this->grader_student_tag_model->salutationListByStatus('1');

        $this->global['pageCode'] = 'grader_student_tag.list';
        $this->global['pageTitle'] = 'Examination Management System : Add Exam';
        $this->loadViews("grader_student_tag/add", $this->global, $data, NULL);
    }



    function viewStudentQuestionSet($id_exam_student_tagging,$idexamgrader,$id_exam_event)
    {
            
        $id_grader = $this->session->userdata('id_grader');
        $grader_name = $this->session->userdata('grader_name');
        $grader_email = $this->session->userdata('grader_email');

        $data['showStudentQuestionSetForGrader'] = $this->grader_student_tag_model->getShowStudentQuestionSetForGrader($id_exam_student_tagging,$idexamgrader);




        if($this->input->post())
        {

            $formData = $this->input->post();
             $grader['grader_comments'] = $formData['comments'];
             $grader['essay_status'] = 'Marked';
 $this->grader_student_tag_model->updateGraderExamTagging($grader,$id_exam_student_tagging);             

             for($i=0;$i<count($data['showStudentQuestionSetForGrader']);$i++) {

                $id_student_question_set = $data['showStudentQuestionSetForGrader'][$i]->id;
                $marksobtained = $formData['marks'][$data['showStudentQuestionSetForGrader'][$i]->id];
                $datas = array(
                    'id_grader' => $id_grader,
                    'marks' => $marksobtained,
                    'id_student_question_set' => $id_student_question_set,
                    'status' => 1,
                    'id_grader_exam' =>$idexamgrader 
                );

                            $this->grader_student_tag_model->deleteStudentQuestionSetById($id_student_question_set);

                $this->grader_student_tag_model->addNewStudentQuestionSetMarks($datas);

             }


                             redirect('/grader/graderStudentTag/viewStudent/' . $idexamgrader . '/' . $id_exam_event);

        }

    
        
        $data['idexamgrader'] = $idexamgrader;



        for($i=0;$i<count($data['showStudentQuestionSetForGrader']);$i++) {
             if($data['showStudentQuestionSetForGrader'][$i]->answer_text=='') {
                $id_student_question_set = $data['showStudentQuestionSetForGrader'][$i]->id;
                $datas = array(
                    'id_grader' => $id_grader,
                    'marks' => 0,
                    'id_student_question_set' => $id_student_question_set,
                    'status' => 1,
                    'id_grader_exam' =>$idexamgrader 
                );
                            $this->grader_student_tag_model->deleteStudentQuestionSetById($id_student_question_set);

                $this->grader_student_tag_model->addNewStudentQuestionSetMarks($datas);


             }
        }





        $data['id_exam_event'] = $id_exam_event;
        $data['id_exam_student_tagging'] = $id_exam_student_tagging;

        $data['graderDetails'] = $this->grader_student_tag_model->getExamDetailsByIdGrader($idexamgrader);
        
        


        $data['examStudentTaggingDetails'] = $this->grader_student_tag_model->getExamStudentTagging($id_exam_student_tagging);


          $data['graderStudentTagList'] = $this->grader_student_tag_model->getGraderStudentTagList($id_grader,$id_exam_event,$idexamgrader);



        $this->global['pageCode'] = 'grader_student_tag.list';
        $this->global['pageTitle'] = 'Examination Management System : Add Exam';
        
        $this->loadViews("grader_student_tag/student_question_setlist", $this->global, $data, NULL);
    }


    function viewStudentQuestionSetDownload($id_exam_student_tagging,$idexamgrader,$id_exam_event)
    {                    $this->getMpdfLibrary();

        $id_grader = $this->session->userdata('id_grader');
        $grader_name = $this->session->userdata('grader_name');
        $grader_email = $this->session->userdata('grader_email');

        $showStudentQuestionSetForGrader = $data['showStudentQuestionSetForGrader'] = $this->grader_student_tag_model->getShowStudentQuestionSetForGrader($id_exam_student_tagging,$idexamgrader);




     
    
        
        $data['idexamgrader'] = $idexamgrader;



        for($i=0;$i<count($data['showStudentQuestionSetForGrader']);$i++) {
             if($data['showStudentQuestionSetForGrader'][$i]->answer_text=='') {
                $id_student_question_set = $data['showStudentQuestionSetForGrader'][$i]->id;
                $datas = array(
                    'id_grader' => $id_grader,
                    'marks' => 0,
                    'id_student_question_set' => $id_student_question_set,
                    'status' => 1,
                    'id_grader_exam' =>$idexamgrader 
                );
                            $this->grader_student_tag_model->deleteStudentQuestionSetById($id_student_question_set);

                $this->grader_student_tag_model->addNewStudentQuestionSetMarks($datas);


             }
        }




        $examDetails = $this->grader_student_tag_model->getExamEventForPdf($id_exam_event);


         if($examDetails->mqms_type=='CPIF') {
                 $programmeName = $examDetails->programme_level_ciif."<br/>".$examDetails->programme_name_ciif."<br/>".$examDetails->name;
              } else  {
                 $programmeName = $examDetails->name;
              }



      

        $data['id_exam_event'] = $id_exam_event;
        $data['id_exam_student_tagging'] = $id_exam_student_tagging;

        $data['graderDetails'] = $this->grader_student_tag_model->getExamDetailsByIdGrader($idexamgrader);
        
        


        $data['examStudentTaggingDetails'] = $this->grader_student_tag_model->getExamStudentTagging($id_exam_student_tagging);


          $data['graderStudentTagList'] = $this->grader_student_tag_model->getGraderStudentTagList($id_grader,$id_exam_event,$idexamgrader);

          $studentName  = $data['graderDetails'][0]->coursename;
          $membershipNumber  = $data['showStudentQuestionSetForGrader'][0]->membership_number;

         $examdate = date('d-m-Y',strtotime($examDetails->exam_date));


            $q=1;  
            for($i=0;$i<count($showStudentQuestionSetForGrader);$i++) {  
$question = $showStudentQuestionSetForGrader[$i]->question_number.' - '.$showStudentQuestionSetForGrader[$i]->question;
                $answerscheme = $showStudentQuestionSetForGrader[$i]->answer_scheme;
                $answer_text = $showStudentQuestionSetForGrader[$i]->answer_text;
                $questionmarks = $showStudentQuestionSetForGrader[$i]->questionmarks;
                $j = $i+1;
                $t = count($showStudentQuestionSetForGrader)+1;
                                   

                          $table.= "<table width='100%'>
         <tr>
         <th><img src='https://ciifiscore.ciif-global.org/assets/img/logocpif-1.png' style='height:70px;' align='left'/></th>
           <th style='text-align:right;width:75%;'> Page No - $j/$t </th>
         </tr>
         </table>
         <table width='100%'>
         <tr>
           <th style='background-color:#e9ecef'>Exam Name </th> 
           <th style='background-color:#e9ecef'>Candidate ID </th>
           <th style='background-color:#e9ecef'>Exam Date </th>
         </tr>

         <tr> <td style='text-align:center;'>$programmeName</td><td style='text-align:center;'>$membershipNumber</td><td style='text-align:center;'>$examdate</td>
         </tr>
        

         </table><br/>";



                          $table.= "<table width='100%' border='1' border-collapse: collapse; style='page-break-after:avoid'>
                           <tr><td colspan='4' style='color:blue;'>$question</td></tr>
                           <tr>
                             <th style='width:35%;background-color:#e9ecef'>Answer Scheme</th>
                             <th style='width:35%;background-color:#e9ecef'>Answer</th>
                             <th style='width:15%;background-color:#e9ecef'>Total Marks</th>
                             <th style='width:15%;background-color:#e9ecef'>Marks Obtained</th>
                            </tr>

                           <tr>
                            <td>$answerscheme</td>
                          
                             
                            <td style='color:green;'>$answer_text</td>

                              <td style='valign:center;text-align:center;'>$questionmarks</td>
                              <td></td>


                               

                          </tr>
                          </table>
                         "; 
                       
                      }



  $table.= "<table width='100%'>
         <tr>
           <th style='text-align:right'> Page No - $t/$t </th>
         </tr>
         </table>
         <table width='100%'>
         <tr>
           <th style='background-color:#e9ecef'>Module Name </th> 
           <th style='background-color:#e9ecef'>Candidate ID </th>
           <th style='background-color:#e9ecef'>Exam Event Name </th>
         </tr>

         <tr> <td style='text-align:center;'>$studentName</td><td style='text-align:center;'>$membershipNumber</td><td style='text-align:center;'>$examNamePdf</td>
         </tr>
          <tr>
           <th style='background-color:#e9ecef'>Programme </th> 
           <th style='background-color:#e9ecef'>Level </th>
           <th style='background-color:#e9ecef'>Exam date </th>
         </tr>

         <tr> <td style='text-align:center;'>$studentName</td><td style='text-align:center;'>$examDetails->modulecode</td><td style='text-align:center;'>$examdate</td>
         </tr>

         </table><br/>
         <table width='100%' border='1' border-collapse: collapse;>
          <tr>
                             <td><br/><br/>Total Marks</td>
                             </tr>
                           <tr>
                             <td>Overall Comments</td>
                             </tr>
                             <tr>
                             <td><br/><br/><br/><br/><br/><br/></td>
                             </tr>

                         
                      </table>
                      <table width='100%' border='1' border-collapse: collapse;>
                         
                          <tr>
                             <td><br/><br/><br/>Grader signatory</td>
                             </tr>
                        <tr>
                             <td><br/><br/><br/>Grader name</td>
                             </tr>
                        <tr>
                             <td><br/><br/><br/>Institutions</td>
                             </tr>
                        <tr>
                             <td><br/><br/>Date graded</td>
                             </tr>
                      </table>
                      "; 
                     

                   
                        $table.= "";


                 $mpdf=new \Mpdf\Mpdf(); 

        $mpdf->WriteHTML($table);
            $mpdf->Output('Exam_slip.pdf', 'D');
            exit;
    }

     function viewStudentQuestionSetlist($id_exam_student_tagging,$idexamgrader,$id_exam_event)
    {
            
        $id_grader = $this->session->userdata('id_grader');
        $grader_name = $this->session->userdata('grader_name');
        $grader_email = $this->session->userdata('grader_email');

      
        $data['idexamgrader'] = $idexamgrader;

        $data['showStudentQuestionSetForGrader'] = $this->grader_student_tag_model->getShowStudentQuestionSetForGrader($id_exam_student_tagging,$idexamgrader);


        for($i=0;$i<count($data['showStudentQuestionSetForGrader']);$i++) {
             if($data['showStudentQuestionSetForGrader'][$i]->answer_text=='') {
                $id_student_question_set = $data['showStudentQuestionSetForGrader'][$i]->id;
                $datas = array(
                    'id_grader' => $id_grader,
                    'marks' => 0,
                    'id_student_question_set' => $id_student_question_set,
                    'status' => 1,
                    'id_grader_exam' =>$idexamgrader 
                );
                            $this->grader_student_tag_model->deleteStudentQuestionSetById($id_student_question_set);

                $this->grader_student_tag_model->addNewStudentQuestionSetMarks($datas);


             }
        }

        $data['id_exam_event'] = $id_exam_event;
        $data['id_exam_student_tagging'] = $id_exam_student_tagging;

         // echo "<Pre>";print_r($data['showStudentQuestionSetForGrader']);exit();
        $data['graderDetails'] = $this->grader_student_tag_model->getExamDetailsByIdGrader($idexamgrader);

       

        $this->global['pageCode'] = 'grader_student_tag.list';
        $this->global['pageTitle'] = 'Examination Management System : Add Exam';

        
        $this->loadViews("grader_student_tag/student_question_setlist", $this->global, $data, NULL);
    }



    function addStudentQuestionSetMarks($id_student_question_set,$id_exam_student_tagging,$idexamgrader,$idexamevent)
    {
        $id_grader = $this->session->userdata('id_grader');
        $grader_name = $this->session->userdata('grader_name');
        $grader_email = $this->session->userdata('grader_email');
        // $id_exam_student_tagging = 13;

        
        $examStudentTagging = $this->grader_student_tag_model->getExamStudentTagging($id_exam_student_tagging);

        if($this->input->post())
        {
            // echo "<Pre>";print_r($this->input->post());exit();
            
            $marks = $this->security->xss_clean($this->input->post('marks'));
            $id_exam_student_tagging = $this->security->xss_clean($this->input->post('id_exam_student_tagging'));
            $id_student_question_set_marks = $this->security->xss_clean($this->input->post('id_student_question_set_marks'));


            // $tm_12hr  = date("H:i", strtotime($exam_time));

            if($id_student_question_set_marks != '')
            {
                $data = array(
                    'id_grader' => $id_grader,
                    'marks' => $marks,
                    'id_student_question_set' => $id_student_question_set,
                    'status' => 1,
                    'id_grader_exam' =>$idexamgrader 
                );
                
                $result = $this->grader_student_tag_model->editStudentQuestionSetMarks($data,$id_student_question_set_marks);
            }
            else
            {

                $data = array(
                    'id_grader' => $id_grader,
                    'marks' => $marks,
                    'id_student_question_set' => $id_student_question_set,
                    'status' => 1,
                    'id_grader_exam' =>$idexamgrader 
                );
                
                $result = $this->grader_student_tag_model->addNewStudentQuestionSetMarks($data);
            }

            if ($result > 0)
            {
                $this->session->set_flashdata('success', 'New GraderExam created successfully');
            }
             else
            {
                $this->session->set_flashdata('error', 'GraderExam creation failed');
            }
                redirect('/grader/graderStudentTag/viewStudentQuestionSet/' . $id_exam_student_tagging . '/' . $idexamgrader . '/' . $examStudentTagging->id_exam_event);
        }

        // $data['id_exam_center'] = $id_exam_center;
        // $data['id_exam_event'] = $id_exam_event;
        $data['studentQuestionQuestionSetForMarksAdd'] = $this->grader_student_tag_model->getStudentQuestionQuestionSetForMarksAdd($id_student_question_set,$idexamgrader);

        $data['id_exam_student_tagging'] = $id_exam_student_tagging;

        $data['examStudentTagging'] = $examStudentTagging;
        $data['idexamgrader'] = $idexamgrader;
        $data['id_exam_event'] = $idexamevent;


        // echo "<Pre>";print_r($data['studentQuestionQuestionSetForMarksAdd']);exit();


        $this->global['pageCode'] = 'grader_student_tag.list';
        $this->global['pageTitle'] = 'Examination Management System : Add Student uestionset Marks';
        
        $this->loadViews("grader_student_tag/add_student_qustionset_marks", $this->global, $data, NULL);
    }




    function edit($id = NULL)
    {
        if ($this->checkAccess('grader_student_tag.edit') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            if ($id == null)
            {
                redirect('/exam/grader_student_tag/list');
            }
            $user_id = $this->session->userId;

            if($this->input->post())
            {
                $salutation = $this->security->xss_clean($this->input->post('salutation'));
                $first_name = $this->security->xss_clean($this->input->post('first_name'));
                $last_name = $this->security->xss_clean($this->input->post('last_name'));
                $email = $this->security->xss_clean($this->input->post('email'));
                $nric = $this->security->xss_clean($this->input->post('nric'));
                $mobile = $this->security->xss_clean($this->input->post('mobile'));
                $password = $this->security->xss_clean($this->input->post('password'));
                $status = $this->security->xss_clean($this->input->post('status'));

                $salutation_data = $this->grader_student_tag_model->getSalutation($salutation);

                $data = array(
                    'salutation' => $salutation,
                    'first_name' => $first_name,
                    'last_name' => $last_name,
                    'full_name' => $salutation_data->name . ". " . $first_name . " " . $last_name,
                    // 'email' => $email,
                    'nric' => $nric,
                    'mobile' => $mobile,
                    // 'password' => md5($password),
                    'status' => $status,
                    'updated_by' => $user_id,
                );

                $result = $this->grader_student_tag_model->editGraderExam($data,$id);
                if ($result) {
                    $this->session->set_flashdata('success', 'GraderExam edited successfully');
                } else {
                    $this->session->set_flashdata('error', 'GraderExam edit failed');
                }
                redirect('/exam/grader_student_tag/list');
            }

            $data['grader_student_tag'] = $this->grader_student_tag_model->getGraderExam($id);
            $data['grader_student_tagModulesList'] = $this->grader_student_tag_model->getGraderExamModulesList($id);
            $data['salutationList'] = $this->grader_student_tag_model->salutationListByStatus('1');
            $data['courseList'] = $this->grader_student_tag_model->courseListByStatus('1');

            // echo "<Pre>";print_r($data['salutationList']);exit();

            $this->global['pageCode'] = 'grader_student_tag.edit';
            $this->global['pageTitle'] = 'Examination Management System : Edit Exam';
            
            $this->loadViews("grader_student_tag/edit", $this->global, $data, NULL);
        }
    }

    function showStudentQuestionSetForGrader()
    {
        $tempData = $this->security->xss_clean($this->input->post('tempData'));
        
        $student_data = $this->grader_student_tag_model->getshowStudentQuestionSetForGrader($tempData);

        echo "<Pre>";print_r($student_data);exit();

        
        if(!empty($student_data))
        {

         $table = "
         <br>
         <h4> Select Students For Grader Tagging</h4>
         <table  class='table' id='list-table'>
                  <tr>
                    <th>Sl. No</th>
                    <th>Student Name</th>
                    <th>Student NRIC</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>City</th>
                    <th>Status</th>
                    <th style='text-align: center;'><input type='checkbox' id='checkAll' name='checkAll'> Check All</th>
                </tr>";

            for($i=0;$i<count($student_data);$i++)
            {
                $id = $student_data[$i]->id;
                $full_name = $student_data[$i]->full_name;
                $nric = $student_data[$i]->nric;
                $email_id = $student_data[$i]->email_id;
                $phone = $student_data[$i]->phone;
                $permanent_zipcode = $student_data[$i]->permanent_zipcode;
                $city = $student_data[$i]->permanent_city;
                $status = $student_data[$i]->applicant_status;


                $j = $i+1;
                $table .= "
                <tr>
                    <td>$j</td>
                    <td>$full_name</td>
                    <td>$nric</td>                    
                    <td>$email_id</td>
                    <td>$phone</td>
                    <td>$city</td>
                    <td>$status</td>                 
                    
                    <td class='text-center'>
                        <input type='checkbox' id='id_student[]' name='id_student[]' class='check' value='".$id."'>
                    </td>
               
                </tr>";
            }

         $table.= "</table>";
        }
        else
        {
            $table= "<h4> No Data Found For Your Search</h4>";
        }
        echo $table;exit;
    }
}
