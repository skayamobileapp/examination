<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Password extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('grader_student_tag_model');
        $id_grader = $this->session->userdata('id_grader');
        if($id_grader=='') {
            redirect('/graderLogin');
        }
        error_reporting(0);

    }

    function changepwd()
    {
        $id_grader = $this->session->userdata('id_grader');
            
        if($_POST) {
            $pwd = md5($_POST['changepwd']);
            $datas['password'] = $pwd;
            $this->grader_student_tag_model->updateGrader($datas,$id_grader);
            echo "<script>alert('Password has been updated');</script>";
            echo "<script>parent.location='/graderLogin'</script>";
            exit;
        }


        $this->global['pageCode'] = 'grader_change_pwd.view';
        $this->global['pageTitle'] = 'Examination Management System : Grader Exam List';
        $this->loadViews("password/changepwd", $this->global, $data, NULL);
    }
   
}
