<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
        <h1 class="h3">Student Question Set</h1>
        
        <a href="<?php echo '/grader/graderStudentTag/viewStudent/' . $idexamgrader . '/' . $id_exam_event; ?>" class="btn btn-link ml-auto">
            <i class="fa fa-chevron-left" aria-hidden="true"></i>
          Back
        </a>

    </div>
    

        <div class="profile-card">
                <div class="row">
                  <div class="col-lg-6">
                    <div class="form-group row align-items-center">
                      <label class="col-md-4 col-lg-3 col-form-label">Exam Name</label>
                      <div class="col-md-8 col-lg-9 text-view">
                        <?php echo $graderDetails[0]->examname;?>
                      </div>
                    </div>
                  </div>

                  <div class="col-lg-6">
                    <div class="form-group row align-items-center">
                      <label class="col-md-4 col-lg-3 col-form-label">Exam Event Name</label>
                      <div class="col-md-8 col-lg-9 text-view">
                        <?php echo $graderDetails[0]->exam_event_name;?>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-lg-6">
                    <div class="form-group row align-items-center">
                      <label class="col-md-4 col-lg-3 col-form-label">Start Date to Mark</label>
                      <div class="col-md-8 col-lg-9 text-view">
                        <?php echo date('d-m-Y',strtotime($graderDetails[0]->start_date));?>
                      </div>
                    </div>
                  </div>

                  <div class="col-lg-6">
                    <div class="form-group row align-items-center">
                      <label class="col-md-4 col-lg-3 col-form-label">End Date to Mark</label>
                      <div class="col-md-8 col-lg-9 text-view">
                        <?php echo date('d-m-Y',strtotime($graderDetails[0]->end_date));?>
                      </div>
                    </div>
                  </div>
                </div>

              <div class="row">
                <div class="col-lg-6">
                  <div class="form-group row align-items-center">
                    <label class="col-md-4 col-lg-3 col-form-label">Total Students</label>
                    <div class="col-md-8 col-lg-9 text-view">
                      <?php echo $showStudentQuestionSetForGrader[0]->full_name;?>
                    </div>
                  </div>
                </div>
              </div>        
        </div>
        <?php

        if(!empty($showStudentQuestionSetForGrader))
        {
            ?>

                    <h4 class="mt-4">Grader Student Details</h4>

                

                  <div class="custom-table">
                    <table class="table">
                        <thead>
                            <tr>
                            <th width="100">Sl. No</th>
                              <th>Question</th>
                                                                  <th width="120">Marks</th>


                              <th width="150" class="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                              <?php
                          $total = 0;
                          for($i=0;$i<count($showStudentQuestionSetForGrader);$i++)
                          {
                          // onclick="showStudentQuestionSetForGrader(<?php echo $showStudentQuestionSetForGrader[$i]->id_student;
                          ?>
                            <tr>
                            <td><?php echo $i+1;?></td>
                            <td><?php echo $showStudentQuestionSetForGrader[$i]->question;?> </td>
                            <?php if($showStudentQuestionSetForGrader[$i]->marks!='') { ?> 
                            <td><?php echo $showStudentQuestionSetForGrader[$i]->marks;?></td>
                          <?php } else { ?>
                            <td>Yet to Mark</td>
                          <?php } ?> 
                            <td class="text-center">
                              <a href="<?php echo '/grader/graderStudentTag/addStudentQuestionSetMarks/'. $showStudentQuestionSetForGrader[$i]->id . '/' . $showStudentQuestionSetForGrader[$i]->id_exam_student_tagging.'/'.$idexamgrader . '/' . $id_exam_event ?>" class="btn" type="" data-toggle="tooltip" data-placement="top" title="Add Exam">
                              Click to Mark
                            </a>
                            </td>

                              </tr>
                          <?php 
                      } 
                      ?>
                        </tbody>
                    </table>
                  </div>


        <?php
        
        }
        ?>


                    
                            
            




</main>

<script>

    function showStudentQuestionSetForGrader(id_student)
    {
         alert(id_student);
        
        var tempPR = {};
        tempPR['id_student'] = id_student;
        tempPR['id_exam_center'] = '<?php echo $id_exam_center ?>';
        tempPR['id_exam_event'] = '<?php echo $id_exam_event ?>';
            $.ajax(
            {
               url: '/grader/graderStudentTag/showStudentQuestionSetForGrader',
               type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                // alert(result);
                $("#view_student_question_set").show();
                $("#view_student_question_set").html(result);
               }
            });
    }

    function getExamEventByExamCenter(id)
    {
        $.get("/exam/graderStudentTag/getExamEventByExamCenter/"+id, function(data, status)
        {  
            $("#view_exam_event").html(data);
        });
    }
    

    $(document).ready(function() {
        $("#form_main").validate({
            rules: {
                id_exam_center: {
                    required: true
                },
                 id_exam_event: {
                    required: true
                },
                 exam_date: {
                    required: true
                }
            },
            messages: {
                id_exam_center: {
                    required: "<p class='error-text'>Select Exam Center</p>",
                },
                id_exam_event: {
                    required: "<p class='error-text'>Select Exam Event</p>",
                },
                exam_date: {
                    required: "<p class='error-text'>Select Exam Date</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>