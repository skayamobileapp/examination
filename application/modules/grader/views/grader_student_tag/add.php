<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">

        <?php $currentdate = date('Y-m-d');
        if(strtotime($graderDetails[0]->exam_event_date)<strtotime($currentdate)){?>

        <h1 class="h3">Past Exam Record</h1>

            <?php } else { ?>

        <h1 class="h3">Add Grader Student Exam Tagging</h1>

            <?php } ?> 
        
        <a href='../../list' class="btn btn-link ml-auto">
            <i class="fa fa-chevron-left" aria-hidden="true"></i>
          Back
        </a>

    </div>
            <div class="row">
              <div class="col-sm-12">
                 <div class="custom-table form-container">
                    <table class="table">
                        <thead>
                            <tr>
                             <th>Exam Name</th>
                             <th>Exam Date</th>
                             <th>Start Date to Mark</th>
                             <th>End Date to Mark</th>
                             <th>Total Question</th>
                             <th>Total Marks For Essay</th>
                             <th>Student Count</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            $totalQuestion = 0;
                              for($i=0;$i<1;$i++)
                         {

                            $this->load->model('grader_student_tag_model');
                            $totalQuestion = $this->grader_student_tag_model->totalMCQQuestion($graderStudentTagList[$i]->id_exam_student_tagging);


                            $originalMarks = 0;
                            

                            for($q=0;$q<count($totalQuestion);$q++) {
                               $originalMarks = $originalMarks + $totalQuestion[$q]->originalmarks;
                            }
                            
                            
                            }?>
                          <tr>
                            <td><?php echo $graderDetails[0]->coursename;?> <br/> <?php echo $graderDetails[0]->coursecode;?></td>
                            <td><?php echo date('d-m-Y',strtotime($graderDetails[0]->exam_event_date));?></td>
                            <td><?php echo date('d-m-Y',strtotime($graderDetails[0]->start_date));?></td>
                            <td><?php echo date('d-m-Y',strtotime($graderDetails[0]->end_date));?></td>
                            
                            <td><?php echo count($totalQuestion);?></td>
                            <td><?php echo $originalMarks;?></td>

                            <td><?php echo count($graderStudentTagList);?></td>

                          </tr>
                        </tbody>
                      </table>
                    </div>
              </div>
            </div>

      <form id="form_main" action="" method="post">

        <?php

        if(!empty($graderStudentTagList))
        {
            ?>

            <div class="form-container">             
                  <div class="custom-table">
                    <table class="table">
                        <thead>
                            <tr>
                            <th>Sl. No </th>
                             <th>Candidate ID</th>
<!--                              <th>Exam Center</th>
                             <th>Exam Event Name</th>
 -->                             <th>Exam Event Date</th>
                             <th>Essay Marks By Grader</th>
                             <th>Status</th>
                             <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                             <?php
                         $total = 0;
                         $statusofSubmit = 2;
                          for($i=0;$i<count($graderStudentTagList);$i++)
                         {

                            $this->load->model('grader_student_tag_model');
                            $totalQuestion = $this->grader_student_tag_model->totalMCQQuestion($graderStudentTagList[$i]->id_exam_student_tagging);


                            $originalMarks = 0;

                            for($q=0;$q<count($totalQuestion);$q++) {
                               $originalMarks = $originalMarks + $totalQuestion[$q]->originalmarks;
                            }

                            $totalmarkedQuestion = $this->grader_student_tag_model->totalMCQQuestionMarked($graderStudentTagList[$i]->id_exam_student_tagging,$idexamgrader);



                            $answermarks = 0;
                            for($m=0;$m<count($totalmarkedQuestion);$m++) {
                              $answermarks = $answermarks + $totalmarkedQuestion[$m]->answermarks;                              

                            }

                            $markedQuestionCount = count($totalmarkedQuestion);


                          ?>
                            <tr>

                          <?php if($graderStudentTagList[$i]->status=='0') { ?>

                            <td style="display:none;"> 

                              <input type='checkbox' id='id_grader_student_tag[]' name='id_grader_student_tag[]' class='check' checked value="<?php echo $graderStudentTagList[$i]->id; ?>" readonly><?php echo $i+1;?></td>
                              <td><?php echo $i+1;?></td>

                            <?php } else if($graderStudentTagList[$i]->status=='1') {?> 
                              <td> 

                              <?php echo $i+1;?></td>

                            <?php } ?> 

                            <td>
                                <?php 

                            if(isset($graderStudentTagList[$i]->membership_number)) {

                                echo $graderStudentTagList[$i]->membership_number;
                            } else {
                                echo $graderStudentTagList[$i]->nric;
                            }
                              ?>
                          </td>
                             <td><?php echo date('d-m-Y',strtotime($graderStudentTagList[$i]->exam_event_date));?></td>
                            <td><?php echo $answermarks;?></td>
                          
                            
                            <td><?php   
                            if($markedQuestionCount==0 && count($totalQuestion)==0) {
                                 echo "Candidate has not submitted";
                            } else if($markedQuestionCount==0 && count($totalQuestion)>0) {
                                 echo "Yet to Mark";
                                 $statusofSubmit = '1';
                            } else if ($markedQuestionCount==count($totalQuestion)) {
                                 echo  "All Marked";
                            } else {
                                 echo  "Partially Marked";
                                 $statusofSubmit = '1';

                            }



                            ?>
                              

                            </td>

                            <?php  
                            $currentdate = date('Y-m-d');
                            if(strtotime($graderStudentTagList[$i]->exam_event_date)<=strtotime($currentdate)) {  ?>

                            <?php if($graderStudentTagList[$i]->status=='0') { ?>
                            <td>
                              <a href="<?php echo '../../viewStudentQuestionSet/'. $graderStudentTagList[$i]->id_exam_student_tagging . '/' . $idexamgrader . '/' . $graderStudentTagList[$i]->id_exam_event; ?>" class="btn" type="" data-toggle="tooltip" data-placement="top" title="Add Exam">
                               View
                            </a>
                            </td>
                          <?php } ?>
                           <?php if($graderStudentTagList[$i]->status=='1') { ?>
                            <td>
                               <a href="<?php echo '../../viewStudentQuestionSet/'. $graderStudentTagList[$i]->id_exam_student_tagging . '/' . $idexamgrader . '/' . $graderStudentTagList[$i]->id_exam_event; ?>" class="btn" type="" data-toggle="tooltip" data-placement="top" title="Add Exam">

                              Submitted</a>
                            </td>
                          <?php } ?>

                      <?php } else { ?>
                          <td></td>

                  <?php } ?> 
                          


                             </tr>
                          <?php 
                      } 
                      ?>
                        </tbody>
                    </table>

                   

                  </div>

                </div>

                 
                                <div class="button-block clearfix">
                  <?php if($statusofSubmit=='1'){?>
<p style="color: #da4141;">You cannot submit untill you mark all the students questions to "All Marked"</p>
                    <div class="bttn-group">
                        <button type="button" class="btn btn-primary" disabled="disabled">Submit</button>
                    </div>
                  <?php }  else {?> 

                    <div class="bttn-group">
                        <button type="button" class="btn btn-primary" onclick="validatetwice()">Submit</button>
                    </div>


                  <?php } ?> 
                  
                  </div> 



        <?php
        
        }
        ?>


      </form>




</main>

<script>
    $(document).ready(function() {
        $("#form_main").validate({
            rules: {
                id_grader_student: {
                    required: true
                }
            },
            messages: {
                id_grader_student: {
                    required: "<p class='error-text'>Select Grader Student For Approval</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


    function validatetwice(){

      var cnf = confirm("Do you really want to submit?");
      if(cnf==true) {
        $( "#form_main" ).submit();
      }

    }
</script>