<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
  <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
    <h1 class="h3">Grader Exam List</h1>
    <!-- <a href="add" class="btn btn-primary ml-auto">+ Add Grader</a> -->
  </div>
  <div class="page-container">
    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
   <h4>Current Exam Event</h4>

    <?php
    if ($this->session->flashdata('success')) {
    ?>
      <div class="alert alert-success alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Success!</strong> <?php echo $this->session->flashdata('success'); ?>
      </div>
    <?php
    }
    if ($this->session->flashdata('error')) {
    ?>
      <div class="alert alert-danger alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Failure!</strong> <?php echo $this->session->flashdata('error'); ?>
      </div>
    <?php
    }
    ?>
    <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Sl. No</th>
           <th>Exam Event Name</th>
           <th>Exam Event Date</th>
           <th>Start Date to Mark</th>
           <th>End Date to Mark</th>
           <th>Total Candidates</th>
            <th style="text-align: center;">View</th>
          </tr>
        </thead>
        <tbody>
          <?php
                  $this->load->model('grader_student_tag_model');
          $currentdate = date('Y-m-d');
          if (!empty($graderStudentList)) {
            $i = 1;

            foreach ($graderStudentList as $record) {

              $studentAssigned = $this->grader_student_tag_model->gettotalStudents($record->id);



   $programmeName = '';
              if($record->exameventid=='1') {
                 $programmeName = "Professional Ethics Module";
              } else  {
                 $programmeName = "CPIF <br/> Intermediate";
              }


                if(strtotime($record->exam_event_date)>=strtotime($currentdate)) {
          ?>
              <tr>
                <td><?php echo $i ?></td>
                <td><?php echo $programmeName;?> <br/><?php echo $record->coursename;?>  <br/> <?php echo $record->modulecode;?></td>
                <td><?php echo date('d-m-Y',strtotime($record->exam_event_date));?></td>
                <td><?php echo date('d-m-Y',strtotime($record->start_date));?></td>
                <td><?php echo date('d-m-Y',strtotime($record->end_date));?></td>
                <td><?php echo count($studentAssigned);?></td>

                <td class="text-center">

                  <a href="<?php echo 'viewStudent/' . $record->id . '/' .  $record->id_exam_event; ?>" class="btn" type="" data-toggle="tooltip" data-placement="top" title="View Student">
                    <i class="fa fa-eye" aria-hidden="true">
                    </i>
                  </a>

                </td>
              </tr>
          <?php
              $i++;
            }
          }
          }
          ?>
        </tbody>
      </table>
    </div>
<br/>
   <h4>Past Exam Event</h4>
      <div class="custom-table">
      <table class="table" id="list-table">
        <thead>
          <tr>
            <th>Sl. No</th>
           <th>Exam Event Name</th>
           <th>Exam Event Date</th>
           <th>Start Date to Mark</th>
           <th>End Date to Mark</th>
           <th>Total Candidates</th>
            <th style="text-align: center;">View</th>
          </tr>
        </thead>
        <tbody>
          <?php
                  $this->load->model('grader_student_tag_model');

          if (!empty($graderStudentList)) {
            $i = 1;

            foreach ($graderStudentList as $record) {


               $programmeName = '';
              if($record->graderStudentList=='1') {
                 $programmeName = "Professional Ethics Module";
              } else  {
                 $programmeName = "CPIF <br/> Intermediate";
              }


              $studentAssigned = $this->grader_student_tag_model->gettotalStudents($record->id);
                if(strtotime($record->exam_event_date)<strtotime($currentdate)) {

          ?>
              <tr>
                <td><?php echo $i ?></td>
 <td><?php echo $record->coursename;?>  <br/> <?php echo $record->modulecode;?></td>                <td><?php echo date('d-m-Y',strtotime($record->exam_event_date));?></td>
                <td><?php echo date('d-m-Y',strtotime($record->start_date));?></td>
                <td><?php echo date('d-m-Y',strtotime($record->end_date));?></td>
                <td><?php echo count($studentAssigned);?></td>

                <td class="text-center">

                  <a href="<?php echo 'viewStudent/' . $record->id . '/' .  $record->id_exam_event; ?>" class="btn" type="" data-toggle="tooltip" data-placement="top" title="View Student">
                    <i class="fa fa-eye" aria-hidden="true">
                    </i>
                  </a>

                </td>
              </tr>
          <?php
              $i++;
            }
          }
          }
          ?>
        </tbody>
      </table>
    </div>
  </div>
</main>
<script>
  $('select').select2();

  function clearSearchForm()
  {
    window.location.reload();
  }
</script>