<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
        <h1 class="h3">Student Question Set</h1>
        
        <a href="<?php echo '/grader/graderStudentTag/viewStudentQuestionSet/' . $examStudentTagging->id . '/' .$idexamgrader . '/' . $examStudentTagging->id_exam_event; ?>" class="btn btn-link ml-auto">
            <i class="fa fa-chevron-left" aria-hidden="true"></i>
          Back
        </a>

    </div>
    


      <form id="target" action="" method="post">

           


      
        <div class="page-container">

                <?php

                if($studentQuestionQuestionSetForMarksAdd)
                {
                    ?>

                    <input type="hidden" class="form-control" id="id_exam_student_tagging" name="id_exam_student_tagging" placeholder="Last Name" value="<?php echo $id_exam_student_tagging; ?>" readonly>
                    <input type="hidden" class="form-control" id="id_student_question_set_marks" name="id_student_question_set_marks" placeholder="Last Name" value="<?php echo $studentQuestionQuestionSetForMarksAdd->id_student_question_set_marks; ?>" readonly>



                    <div class="form-container">
                            <h4 class="form-group-title" style="font-size:16px;"><?php echo $studentQuestionQuestionSetForMarksAdd->question;?></h4>

                        

                          <div class="custom-table">
                            <table class="table" border="1">
                                <thead>
                                    <tr>
                                      <th style="width:50%">Student Answer</th>
                                       <th style="width:50%">Answer Scheme</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                    <td><?php echo $studentQuestionQuestionSetForMarksAdd->answer_text;?></td>
                                    <td><?php echo $studentQuestionQuestionSetForMarksAdd->answer_scheme;?></td>

                                   
                                    </tr>
                                </tbody>
                              </table>
                              <table class="table">
                                <thead>
                                    <tr>
                                      <th width='20%'>Student Marks Scored</th>
                                       <th>Total Marks</th>
                                    </tr>
                                </thead>
                                    <tr>
                                       <td>
                                      <input type="text" class="form-control numberonly" id="marks" name="marks" placeholder="Marks" min="0" max="3" value="<?php if($studentQuestionQuestionSetForMarksAdd->marks == '')
                                    {
                                      echo $studentQuestionQuestionSetForMarksAdd->marks_detail;
                                    }
                                    else
                                    {
                                      echo $studentQuestionQuestionSetForMarksAdd->marks;
                                    }
                                    ?>">
                                  </td>
                                  <td><input type='hidden' name='marmarks' id='maxmarks' value="<?php echo $studentQuestionQuestionSetForMarksAdd->max_marks;?>" />
                                  <?php echo $studentQuestionQuestionSetForMarksAdd->max_marks;?>
                                </td>
                                </tr>
                                </tbody>
                            </table>
                          </div>

                        </div>


                <?php
                
                }
                ?>


                    

                </div> 



                <?php if($studentQuestionQuestionSetForMarksAdd->marks == '')
                {
                  ?>

                  <div class="button-block clearfix">
                    <div class="bttn-group">
                        <button type="button" class="btn btn-primary" onclick="validateMarks()">Save</button>
                    </div>

                  </div> 

                  <?php
                }
                else
                {
                  ?>
                    <h5 class="form-group-title">Marks Already Approved, Modification Not Permitted</h5>

                  <?php
                }
                ?>




            

        </div>


      </form>



</main>

<script>


$(document).ready(function () {    
    
            $('.numberonly').keypress(function (e) {    
    
                var charCode = (e.which) ? e.which : event.keyCode    
    
                if (String.fromCharCode(charCode).match(/[^0-9]/g))    
    
                    return false;                        
    
            });    
    
        });   



  function reloadPage()
  {
    window.location.reload();
  }


   function validateMarks() {
    var marks = $("#marks").val();
    var maxmarks = $("#maxmarks").val();

    if(marks=='') {
      alert("Please enter the marks");
      return false;
    }
    if(parseInt(marks)>parseInt(maxmarks)) {
      alert("Please enter the obtained marks less than the allocated marks");
      return false;

    }

     $( "#target" ).submit();
   }

</script>
 