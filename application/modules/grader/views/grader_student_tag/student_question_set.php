<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
        <h1 class="h3">Student Question Set</h1>
        
        <a href="<?php echo '/grader/graderStudentTag/viewStudent/' . $idexamgrader . '/' . $id_exam_event; ?>" class="btn btn-link ml-auto">
            <i class="fa fa-chevron-left" aria-hidden="true"></i>
          Back
        </a>

    </div>
    

 <div class="row">
              <div class="col-sm-12">
                 <div class="custom-table form-container">
                    <table class="table">
                        <thead>
                            <tr>
                             <th>Exam Event Name</th>
                             <th>Start Date to Mark</th>
                             <th>End Date to Mark</th>
                             <th>Student Name</th>
                            </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td> <?php echo $graderDetails[0]->exam_event_name;?></td>
                            <td><?php echo date('d-m-Y',strtotime($graderDetails[0]->start_date));?></td>
                            <td><?php echo date('d-m-Y',strtotime($graderDetails[0]->end_date));?></td>
                            <td><?php echo $showStudentQuestionSetForGrader[0]->full_name;?></td>

                          </tr>
                        </tbody>
                      </table>
                    </div>
              </div>
            </div>

        <div class="page-container">

       
                         <?php $q=1;  for($i=0;$i<count($showStudentQuestionSetForGrader);$i++)
                                 {  



                                    ?>
    <div class="row">
              <div class="col-sm-12">
                 <div class="custom-table form-container">
                    <table class="table">

                        <thead>
                            <tr>
                             <th colspan="4"><?php echo $q;?><?php echo $showStudentQuestionSetForGrader[$i]->question;?></th>
                            </tr>
                             <tr>
                             <th style="width:40%">Answer Scheme</th>
                             <th style="width:40%">Answer</th>
                             <th>Total Marks</th>
                             <th>Marks Obtained</th>
                            </tr>
                        </thead>
                        <tbody>

                          

                          <tr>
                            <td> <?php echo $showStudentQuestionSetForGrader[$i]->answer_scheme;?></td>
                          
                             <td> <?php echo $showStudentQuestionSetForGrader[$i]->answer_text;?></td>

                              <td> <?php echo $showStudentQuestionSetForGrader[$i]->questionmarks;?></td>

                               <td> <input type="text" class="form-control numberonly" id="marks" name="marks" placeholder="Marks" min="0" max="3" value="<?php echo $showStudentQuestionSetForGrader[$i]->marks;?>"  /> </td>

                          </tr>

                   
                        </tbody>
                      </table>
                    </div>
              </div>
            </div>
   <?php  $q++;} ?> 
                          
             <div class="row">
              <div class="col-sm-12">
                Overall Comments
                 <div class="custom-table form-container">
                    <textarea class="form-control" name='comments' id='comments'>
                        
                    </textarea>
                 </div>
                </div>
                 <div class="col-sm-12">
                    <div class="button-block clearfix">
                    <div class="bttn-group">
                        <button type="button" class="btn btn-primary" onclick="validateMarks()">Save</button>
                    </div>

                  </div>    
                </div>
            </div>

        </div>



</main>

<script>

    function showStudentQuestionSetForGrader(id_student)
    {
         alert(id_student);
        
        var tempPR = {};
        tempPR['id_student'] = id_student;
        tempPR['id_exam_center'] = '<?php echo $id_exam_center ?>';
        tempPR['id_exam_event'] = '<?php echo $id_exam_event ?>';
            $.ajax(
            {
               url: '/grader/graderStudentTag/showStudentQuestionSetForGrader',
               type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                // alert(result);
                $("#view_student_question_set").show();
                $("#view_student_question_set").html(result);
               }
            });
    }

    function getExamEventByExamCenter(id)
    {
        $.get("/exam/graderStudentTag/getExamEventByExamCenter/"+id, function(data, status)
        {  
            $("#view_exam_event").html(data);
        });
    }
    

    $(document).ready(function() {
        $("#form_main").validate({
            rules: {
                id_exam_center: {
                    required: true
                },
                 id_exam_event: {
                    required: true
                },
                 exam_date: {
                    required: true
                }
            },
            messages: {
                id_exam_center: {
                    required: "<p class='error-text'>Select Exam Center</p>",
                },
                id_exam_event: {
                    required: "<p class='error-text'>Select Exam Event</p>",
                },
                exam_date: {
                    required: "<p class='error-text'>Select Exam Date</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });
</script>