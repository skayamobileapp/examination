<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
        <h1 class="h3">Student Question Set</h1>
        
        <a href="<?php echo '/grader/graderStudentTag/viewStudent/' . $idexamgrader . '/' . $id_exam_event; ?>" class="btn btn-link ml-auto">
            <i class="fa fa-chevron-left" aria-hidden="true"></i>
          Back
        </a>

    </div>
    <form method="post" action="">


 <div class="row">
              <div class="col-sm-12">
                 <div class="custom-table">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                             <th>Module Name</th>
                             <th>Start Date to Mark</th>
                             <th>End Date to Mark</th>
                             <th>Candidate ID</th>
                             <th>Download</th>
                            </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td> <?php echo $graderDetails[0]->coursename;?></td>
                            <td><?php echo date('d-m-Y',strtotime($graderDetails[0]->start_date));?></td>
                            <td><?php echo date('d-m-Y',strtotime($graderDetails[0]->end_date));?></td>
                            <td><?php 

                            if(isset($showStudentQuestionSetForGrader[0]->membership_number)) {

                                echo $showStudentQuestionSetForGrader[0]->membership_number;
                            } else {
                                echo $showStudentQuestionSetForGrader[0]->nric;
                            }
                              ?>
                            </td>

                            <td><a href="/grader/graderStudentTag/viewStudentQuestionSetDownload/<?php echo $id_exam_student_tagging;?>/<?php echo $idexamgrader;?>/<?php echo $id_exam_event;?>">Download</a></td>

                          </tr>
                        </tbody>
                      </table>
                    </div>
              </div>
            </div>

        <div class="page-container">

                         <?php $q=1;  for($i=0;$i<count($showStudentQuestionSetForGrader);$i++)
                                 {  
                                    ?>
    <div class="row">
              <div class="col-sm-12">
                 <div class="custom-table form-container">
                     <div style="color: #4276ef">
                         <?php echo $showStudentQuestionSetForGrader[$i]->question_number;?><?php echo $showStudentQuestionSetForGrader[$i]->question;?>
                     </div>
                    <table class="table table-bordered" style="width: 100%;">

                        <thead>
                             <tr>
                             <th style="width:45%">Answer Scheme</th>
                             <th style="width:45%">Answer</th>
                             <th style="width:5%">Total Marks</th>
                             <th style="width:5%">Marks Obtained</th>
                            </tr>
                        </thead>
                        <tbody>

                          

                          <tr>
                            <td> <?php echo $showStudentQuestionSetForGrader[$i]->answer_scheme;?></td>
                          
                             <?php if($showStudentQuestionSetForGrader[$i]->answer_text=='') { ?>
                                <td style="color: red;"> Candidate did not answer
                                </td>

                             <?php } else { ?>
                             <td style="color: #229116"> 
                                <?php echo $showStudentQuestionSetForGrader[$i]->answer_text;?></td>
                            <?php } ?> 

                              <td> <?php echo $showStudentQuestionSetForGrader[$i]->questionmarks;?></td>

                               <td> <input type="text" class="form-control numberonly" id="marks<?php echo $showStudentQuestionSetForGrader[$i]->id;?>" name="marks[<?php echo $showStudentQuestionSetForGrader[$i]->id;?>]" placeholder="Marks" min="0" max="3" value="<?php echo $showStudentQuestionSetForGrader[$i]->marks;?>"  <?php if($showStudentQuestionSetForGrader[$i]->answer_text=='' ){ echo "readonly=readonly";} ?> onblur="validateMarks( <?php echo $showStudentQuestionSetForGrader[$i]->id;?>,<?php echo $showStudentQuestionSetForGrader[$i]->questionmarks;?>,this.value);"/> </td>

                          </tr>

                   
                        </tbody>
                      </table>
                    </div>
              </div>
            </div>
   <?php  $q++;} ?> 
                          
             <div class="row">
              <div class="col-sm-12">
                Overall Comments
                 <div class="form-group">
                    <textarea class="form-control" name='comments' id='comments'><?php echo $examStudentTaggingDetails->grader_comments;?></textarea>
                 </div>
                </div>
                 <div class="col-sm-12" id='savebuttonid'>
                    <div class="button-block clearfix">
                    <div class="bttn-group">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>

                  </div>    
                </div>
            </div>

        </div>

</form>

</main>

<script>

var status = <?php echo $graderStudentTagList[0]->status;?>

if(status=='1') {
    $("#savebuttonid").hide();
     $(".numberonly").attr('disabled','disabled');

    
}
    function showStudentQuestionSetForGrader(id_student)
    {

        var tempPR = {};
        tempPR['id_student'] = id_student;
        tempPR['id_exam_center'] = '<?php echo $id_exam_center ?>';
        tempPR['id_exam_event'] = '<?php echo $id_exam_event ?>';
            $.ajax(
            {
               url: '/grader/graderStudentTag/showStudentQuestionSetForGrader',
               type: 'POST',
               data:
               {
                tempData: tempPR
               },
               error: function()
               {
                alert('Something is wrong');
               },
               success: function(result)
               {
                // alert(result);
                $("#view_student_question_set").show();
                $("#view_student_question_set").html(result);
               }
            });
    }

    function getExamEventByExamCenter(id)
    {
        $.get("/exam/graderStudentTag/getExamEventByExamCenter/"+id, function(data, status)
        {  
            $("#view_exam_event").html(data);
        });
    }
    

    $(document).ready(function() {
        $("#form_main").validate({
            rules: {
                id_exam_center: {
                    required: true
                },
                 id_exam_event: {
                    required: true
                },
                 exam_date: {
                    required: true
                }
            },
            messages: {
                id_exam_center: {
                    required: "<p class='error-text'>Select Exam Center</p>",
                },
                id_exam_event: {
                    required: "<p class='error-text'>Select Exam Event</p>",
                },
                exam_date: {
                    required: "<p class='error-text'>Select Exam Date</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });

    function validateMarks(id, maxmarks,obtainedmarks) {
        if(maxmarks<obtainedmarks) {
            alert("Obtained marks cannot be greater than the maximum marks");
            $("#marks"+id).val(0);
        }
    }
</script>