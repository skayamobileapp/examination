<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
        <h1 class="h3">Add Grader Student Exam Tagging</h1>
        
        <a href='../../list' class="btn btn-link ml-auto">
            <i class="fa fa-chevron-left" aria-hidden="true"></i>
          Back
        </a>

    </div>

    <div class="profile-card">              
      <div class="row">
        <div class="col-lg-6">
          <div class="form-group row align-items-center">
            <label class="col-md-4 col-lg-3 col-form-label">Exam Name</label>
            <div class="col-md-8 col-lg-9 text-view">
              <?php echo $graderDetails[0]->examname;?>
            </div>
          </div>
        </div>

        <div class="col-lg-6">
          <div class="form-group row align-items-center">
            <label class="col-md-4 col-lg-3 col-form-label">Exam Event Name</label>
            <div class="col-md-8 col-lg-9 text-view">
              <?php echo $graderDetails[0]->exam_event_name;?>
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-lg-6">
          <div class="form-group row align-items-center">
            <label class="col-md-4 col-lg-3 col-form-label">Start Date to Mark</label>
            <div class="col-md-8 col-lg-9 text-view">
              <?php echo date('d-m-Y',strtotime($graderDetails[0]->start_date));?>
            </div>
          </div>
        </div>

        <div class="col-lg-6">
          <div class="form-group row align-items-center">
            <label class="col-md-4 col-lg-3 col-form-label">End Date to Mark</label>
            <div class="col-md-8 col-lg-9 text-view">
              <?php echo date('d-m-Y',strtotime($graderDetails[0]->end_date));?>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-6">
          <div class="form-group row align-items-center">
            <label class="col-md-4 col-lg-3 col-form-label">Student Count</label>
            <div class="col-md-8 col-lg-9 text-view">
              <?php echo count($graderStudentTagList);?>
            </div>
          </div>
        </div> 
      </div>
    </div>







    


      <form id="form_main" action="" method="post">

        <?php

        if(!empty($graderStudentTagList))
        {
            ?>

            <div class="form-container">             
                  <div class="custom-table">
                    <table class="table">
                        <thead>
                            <tr>
                            <th>Sl. No</th>
                             <th>Student</th>
<!--                              <th>Exam Center</th>
                             <th>Exam Event Name</th>
 -->                             <th>Exam Event Date</th>
                             <th>Total Question</th>
                             <th>Total Marks</th>
                             <th>Obtained Marks</th>
                             <th>Status</th>
                             <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                             <?php
                         $total = 0;
                          for($i=0;$i<count($graderStudentTagList);$i++)
                         {

                            $this->load->model('grader_student_tag_model');
                            $totalQuestion = $this->grader_student_tag_model->totalMCQQuestion($graderStudentTagList[$i]->id_exam_student_tagging);
                            $originalMarks = 0;

                            for($q=0;$q<count($totalQuestion);$q++) {
                               $originalMarks = $originalMarks + $totalQuestion[$q]->originalmarks;
                            }

                            $totalmarkedQuestion = $this->grader_student_tag_model->totalMCQQuestionMarked($graderStudentTagList[$i]->id_exam_student_tagging,$idexamgrader);



                            $answermarks = 0;
                            for($m=0;$m<count($totalmarkedQuestion);$m++) {
                              $answermarks = $answermarks + $totalmarkedQuestion[$m]->answermarks;                              

                            }

                            $markedQuestionCount = count($totalmarkedQuestion);


                          ?>
                            <tr>

                          <?php if($graderStudentTagList[$i]->status=='0') { ?>

                            <td> 

                              <input type='checkbox' id='id_grader_student_tag[]' name='id_grader_student_tag[]' class='check' value="<?php echo $graderStudentTagList[$i]->id; ?>"><?php echo $i+1;?></td>

                            <?php } else if($graderStudentTagList[$i]->status=='1') {?> 
                              <td> 

                              <?php echo $i+1;?></td>

                            <?php } ?> 

                            <td><?php echo $graderStudentTagList[$i]->student_name . " - " . $graderStudentTagList[$i]->nric;?></td>
<!--                             <td><?php echo $graderStudentTagList[$i]->exam_center_name;?></td>
                            <td><?php echo $graderStudentTagList[$i]->exam_event_name;?></td>
 -->                            <td><?php echo $graderStudentTagList[$i]->exam_event_date;?></td>
                            <td><?php echo count($totalQuestion);?></td>
                            <td><?php echo $originalMarks;?></td>
                            <td><?php echo $answermarks;?></td>
                          
                            
                            <td><?php   
                            if($markedQuestionCount==0) {
                                 echo "Yet to Mark";
                            } else if ($markedQuestionCount==count($totalQuestion)) {
                                 echo  "All Marked";
                            } else {
                                 echo  "Partially Marked";
                            }



                            ?>
                              

                            </td>

                            <?php if($graderStudentTagList[$i]->status=='0') { ?>
                            <td>
                              <a href="<?php echo '../../viewStudentQuestionSet/'. $graderStudentTagList[$i]->id_exam_student_tagging . '/' . $idexamgrader . '/' . $graderStudentTagList[$i]->id_exam_event; ?>" class="btn" type="" data-toggle="tooltip" data-placement="top" title="Add Exam">
                               View
                            </a>
                            </td>
                          <?php } ?>
                           <?php if($graderStudentTagList[$i]->status=='1') { ?>
                            <td>
                              Submitted
                            </td>
                          <?php } ?>
                          


                             </tr>
                          <?php 
                      } 
                      ?>
                        </tbody>
                    </table>

                   

                  </div>

                </div>


                 <div class="button-block clearfix">
                    <div class="bttn-group">
                        <button type="button" class="btn btn-primary" onclick="validatetwice()">Submit</button>
                    </div>
                  </div> 


        <?php
        
        }
        ?>


      </form>




</main>

<script>
    $(document).ready(function() {
        $("#form_main").validate({
            rules: {
                id_grader_student: {
                    required: true
                }
            },
            messages: {
                id_grader_student: {
                    required: "<p class='error-text'>Select Grader Student For Approval</p>",
                }
            },
            errorElement: "span",
            errorPlacement: function(error, element) {
                error.appendTo(element.parent());
            }

        });
    });


    function validatetwice(){

      var cnf = confirm("Do you really want to submit?");
      if(cnf==true) {
        $( "#form_main" ).submit();
      }

    }
</script>