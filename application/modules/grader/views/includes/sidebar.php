 <div class="container-fluid">
      <div class="row">
        <nav id="sidebarMenu" class="col-md-3 d-md-block sidebar collapse">
          <div class="sidebar-sticky pt-3">
            <div class="nav-fold">
              <a href="#" class="d-flex align-items-center">
                <span class="user-avatar"><img src="<?php echo BASE_PATH; ?>assets/img/user.jpg"/></span>
                <span>
                  <?php echo $grader_name; ?>
                  <small class="d-block"><?php echo $grader_email; ?></small>
                  <small class="d-block"></small>                  
                </span>
              </a>
            </div>
            <ul class="nav flex-column mb-2">
            
              
              <li class="nav-item">
                <a class="nav-link <?php 
                  if(in_array($pageCode,array('grader.profile','grader_exam.view','grader_student_tag.view','grader_student_tag.list'))){  ?>
                  <?php 
                  } else{ 
                  ?>
                  collapsed
                  <?php
                  }
                  ?>" data-toggle="collapse" href="#collapseResults" role="button">
                  <i class="fa fa-file-text"></i>
                  <span>Grader</span>
                  <i class="fa fa-angle-right ml-auto" aria-hidden="true"></i>
                  <i class="fa fa-angle-down ml-auto" aria-hidden="true"></i>
                </a>
                <ul class="collapse nav <?php 
                  if(in_array($pageCode,array('grader.profile','grader_exam.view','grader_student_tag.view','grader_student_tag.list','grader_change_pwd.view'))){  ?>
                  show
                 <?php 
                  } else{ 
                  ?>
                  
                  <?php
                  }
                  ?>" id="collapseResults">
                  <li class="nav-item">
                    <a href="/grader/graderStudentTag/list" class="nav-link <?php if(in_array($pageCode,array('grader_student_tag.view','grader_student_tag.list'))){echo 'active';}?>">
                      <i class="fa fa-file-text"></i>
                      <span>Dashboard</span>                      
                    </a>
                  </li>
                   <li class="nav-item">
                    <a href="/grader/password/changepwd" class="nav-link <?php if(in_array($pageCode,array('grader_change_pwd.view'))){echo 'active';}?>">
                      <i class="fa fa-file-text"></i>
                      <span>Change Password</span>                      
                    </a>
                  </li>

                </ul>
              </li>                
                                     
          </ul>         
        </div>
      </nav>
    </div>
  </div>