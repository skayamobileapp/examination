
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <title>Exam</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo BASE_PATH; ?>assets/onlineportal/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo BASE_PATH; ?>assets/onlineportal/css/font-awesome.min.css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css2?family=Ubuntu:wght@300;400;500;700&display=swap" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="<?php echo BASE_PATH; ?>assets/onlineportal/css/main.css" rel="stylesheet" />
  </head>
  <body>

    <nav class="navbar navbar-expand-lg navbar-light main-header">
      <div class="container">
        <a class="navbar-brand" href="#">LOGO</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample02" aria-controls="navbarsExample02" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
      
        <div class="collapse navbar-collapse" id="navbarsExample02">
          <ul class="navbar-nav admin-nav ml-auto align-items-center">

            <li class="nav-item">
              <a href="/examcenter/examcenter/dashboard" class="nav-link">Dashboard</a>
            </li> 
             <li class="nav-item">
              <a href="/examcenter/examcenter/issue" class="nav-link">Report Issue</a>
            </li>
             <li class="nav-item">
              <a href="/examcenter/examcenter/examlist" class="nav-link">Exam Details</a>
            </li>  
                                 
            <li class="nav-item">
              Welcome <?php echo $this->session->contact_person;?>
            </li>            
            <li class="nav-item">
              <a href="/examcenter/logout" class="nav-link">Logout</a>
            </li>          
          </ul> 
          <br/>

        </div>
        <br/>
      </div>
    </nav> 
    
    <div class="container">
      <div class="pt-4">
        <h3 style="text-align: center;color: #00bcd2;">Issue Details</h3>
        <div class="row">
          <div class="col-md-6">
             <label>Exam Name</label>
            <input type="text" class="form-control mr-sm-3" id="exam_name" name="exam_name" placeholder="">

          </div>
            <div class="col-md-6">
             <label>Date</label>
            <input type="text" class="form-control mr-sm-3" id="exam_date" name="exam_date" placeholder="">

          </div>
                 
        </div>
        <div class="row">
          <div class="col-md-12">
             <label>Issue Description</label>
            <textarea type="text" class="form-control mr-sm-3" value='description'></textarea>

          </div>
                 
        </div>
         <div class="row">
          <div class="col-md-12">
            <br/>
            <button type="submit" class="btn btn-primary">Save</button>

          </div>
                 
        </div>
     
     
        <hr/>
         
      </div>
       <div class="tab-content" id="myTabContent">
          <div class="tab-pane fade show active" id="studentlist" role="tabpanel" aria-labelledby="studentlist-tab">
            <table class="table">
              <thead class="thead-light">
                <tr>
                  <th scope="col">Exam Name</th>
                  <th scope="col">Issue</th>
                  <th scope="col">Created Date</th>
                  <th scope="col">Comments From Admin</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Demo CIIF Exam</td>
                  <td>Internet issue need to add the extra time for all students for about 30 mins</td>
                  <td>10-10-2020</td>
                  
                  <td>-</td>
                </tr>                              
              </tbody>
            </table>
          </div>
         
        </div>
    </div>
 

  </body>
</html>

