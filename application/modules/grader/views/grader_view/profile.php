<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
        <h1 class="h3">Grader Dashboard</h1>
    </div>



                <div class="profile-card">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group row align-items-center">
                                <label class="col-md-4 col-lg-3 col-form-label">Grader Name :</label>
                                <div class="col-md-8 col-lg-9 text-view">
                                <?php echo ucwords($grader->full_name); ?>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <div class="form-group row align-items-center">
                                <label class="col-md-4 col-lg-3 col-form-label">Grader NRIC :</label>
                                <div class="col-md-8 col-lg-9 text-view">
                                <?php echo $grader->nric ?>
                                </div>
                            </div>
                        </div>
                    </div>  
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group row align-items-center">
                                <label class="col-md-4 col-lg-3 col-form-label">Grader Email :</label>
                                <div class="col-md-8 col-lg-9 text-view">
                                <?php echo $grader->email; ?>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <div class="form-group row align-items-center">
                                <label class="col-md-4 col-lg-3 col-form-label">Grader Mobile :</label>
                                <div class="col-md-8 col-lg-9 text-view">
                                <?php echo $grader->mobile; ?>
                                </div>
                            </div>
                        </div>
                    </div>                                    
                </div>

</main>
<script>
    $('select').select2();

    $( function() {
      $( ".datepicker" ).datepicker({
          changeYear: true,
          changeMonth: true,
      });
    });
</script>