<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
  <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center page-title">
    <h1 class="h3">Change Password</h1>
    <!-- <a href="add" class="btn btn-primary ml-auto">+ Add Grader</a> -->
  </div>
  <div class="page-container">
    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
  <form action="" method="POST">
     <div class="row">
    <div class="col-sm-12">
       <div class="custom-table form-container">
          <table class="table">
          <tr> 
             <td>Change Password </td>
             <td><input type="password" name='changepwd' id='changepwd' value=''/></td>
          </tr>
          <tr> 
             <td><button type='Submit' name='Save' id='Save' class="btn-btn-primary">Save</button></td>
             <td></td>
          </tr>      
      </table>
    </div>
  </div>
</div>
  </form>
  </div>
</main>
<script>
  $('select').select2();

  function clearSearchForm()
  { //6366947015
    window.location.reload();
  }
</script>