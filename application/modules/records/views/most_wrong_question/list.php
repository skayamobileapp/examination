<main role="main" class="col-md-9 ml-sm-auto main-container px-md-4">
<div class="page-container">
    <div class="panel-group advanced-search" id="accordion" role="tablist" aria-multiselectable="true">
      <div class="panel panel-default">
        <div class="panel-heading" role="tab" id="headingOne">
          <h4 class="panel-title">
            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne" class="">
              Advanced Search
              <i class="fa fa-angle-right" aria-hidden="true"></i>
              <i class="fa fa-angle-down" aria-hidden="true"></i>
            </a>
          </h4>
        </div>
        <form action="" method="post" id="searchForm">

          <div id="collapseOne" class="panel-collapse collapse show" role="tabpanel" aria-labelledby="headingOne" aria-expanded="true" style="">
            <div class="panel-body p-2">

              <div class="row">

                <div class="col-lg-6">
                  <div class="form-group row">
                    <label class="col-sm-4 col-form-label">Question Pool</label>
                    <div class="col-sm-8">
                      <select name="id_pool" id="id_pool" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($poolList)) {
                          foreach ($poolList as $record) { ?>
                            <option value="<?php echo $record->id;  ?>" <?php if ($searchParam['id_pool'] == $record->id) {
                                                                          echo "selected";
                                                                        } ?>>
                              <?php echo $record->code . " - " . $record->name;  ?>
                            </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>
                
                <div class="col-lg-6">
                  <div class="form-group row">
                    <label class="col-sm-4 col-form-label">Module</label>
                    <div class="col-sm-8">
                      <select name="id_course" id="id_course" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($courseList)) {
                          foreach ($courseList as $record) { ?>
                            <option value="<?php echo $record->id;  ?>" <?php if ($searchParam['id_course'] == $record->id) {
                                                                          echo "selected";
                                                                        } ?>>
                              <?php echo $record->code . " - " . $record->name;  ?>
                            </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="form-group row">
                    <label class="col-sm-4 col-form-label">Topic</label>
                    <div class="col-sm-8">
                      <select name="id_topic" id="id_topic" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($topicList)) {
                          foreach ($topicList as $record) { ?>
                            <option value="<?php echo $record->id;  ?>" <?php if ($searchParam['id_topic'] == $record->id) {
                                                                          echo "selected";
                                                                        } ?>>
                              <?php echo $record->code . " - " . $record->name;  ?>
                            </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="form-group row">
                    <label class="col-sm-4 col-form-label">Bloom Taxonomy</label>
                    <div class="col-sm-8">
                      <select name="id_bloom_taxonomy" id="id_bloom_taxonomy" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($bloomTaxonomyList)) {
                          foreach ($bloomTaxonomyList as $record) { ?>
                            <option value="<?php echo $record->id;  ?>" <?php if ($searchParam['id_bloom_taxonomy'] == $record->id) {
                                                                          echo "selected";
                                                                        } ?>>
                              <?php echo $record->code . " - " . $record->name;  ?>
                            </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="col-lg-6">
                  <div class="form-group row">
                    <label class="col-sm-4 col-form-label">Difficult Level </label>
                    <div class="col-sm-8">
                      <select name="id_difficult_level" id="id_difficult_level" class="form-control">
                        <option value="">Select</option>
                        <?php
                        if (!empty($difficultLevelList)) {
                          foreach ($difficultLevelList as $record) { ?>
                            <option value="<?php echo $record->id;  ?>" <?php if ($searchParam['id_difficult_level'] == $record->id) {
                                                                          echo "selected";
                                                                        } ?>>
                              <?php echo $record->code . " - " . $record->name;  ?>
                            </option>
                        <?php
                          }
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>

              </div>

              <hr />
              <div class="d-flex justify-content-center">
                <button type="submit" class="btn btn-primary">Search</button>
                      <a href='mostWrongQuestions' class="btn btn-link">Clear All Fields</a>
              </div>
            </div>
          </div>

        </form>

      </div>
    </div>  


            <?php
            if(!empty($accountCodeList))
            {
              ?>


              <div class="custom-table">
                <table class="table" id="list-table">
                  <thead>
                    <tr>
                      <th>Sl. No</th>
                      <th>Question</th>
                      <th>Count</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    $i=1;
                      foreach ($accountCodeList as $record)
                      {
                    ?>
                        <tr>
                          <td><?php echo $i ?></td>
                          <td><?php echo $record->question ?></td>
                          <td><?php echo $record->totalquestion ?></td>  
                        </tr>
                    <?php
                      $i++;
                    }
                    ?>
                  </tbody>
                </table>
              </div>   

               <?php
            }
            ?>      


            </div>                                
        </div>
    </form>
</main>


<script type="text/javascript">


    $( function() {
      $( ".datepicker" ).datepicker({
          changeYear: true,
          changeMonth: true,
      });
    } );

    $('select').select2();

    function reloadPage()
    {
      window.location.reload();
    }
</script>

<script type="text/javascript">


    $( function() {
      $( ".datepicker" ).datepicker({
          changeYear: true,
          changeMonth: true,
      });
    } );

   
</script>
<script type="text/javascript">
        $(document).ready(function(){
 
            $('#id_course').change(function(){ 
                var id=$(this).val();
                $.ajax({
                    url : "<?php echo site_url('question/ajax/getCategoryTopics');?>",
                    method : "POST",
                    data : {id: id},
                    async : true,
                    dataType : 'json',
                    success: function(data){
                         
                        var html = '';
                        var i;
                        for(i=0; i<data.length; i++){
                            html += '<option value='+data[i].id+'>'+data[i].name+'-'+data[i].name+'</option>';
                        }
                        $('#id_topic').html(html);
 
                    }
                });
                return false;
            }); 
             
        });
    </script>