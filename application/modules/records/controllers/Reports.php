<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Reports extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('reports_model');
        $this->load->model('question/question_model');
                $this->load->model('exam/exam_event_model');

        $this->isLoggedIn();
    }

    function mostWrongQuestions()
    {
        if ($this->checkAccess('most_wrong_question.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        { $formData = array();
 
        

                 $formData['id_pool'] = $this->security->xss_clean($this->input->post('id_pool'));
            $formData['id_course'] = $this->security->xss_clean($this->input->post('id_course'));
            $formData['id_topic'] = $this->security->xss_clean($this->input->post('id_topic'));
            $formData['id_course_learning_objective'] = $this->security->xss_clean($this->input->post('id_course_learning_objective'));
            $formData['id_bloom_taxonomy'] = $this->security->xss_clean($this->input->post('id_bloom_taxonomy'));
            $formData['id_difficult_level'] = $this->security->xss_clean($this->input->post('id_difficult_level'));

            if($_POST)
            {
                $data['accountCodeList'] = $this->reports_model->getMostWrongQuestions($formData);
            }
               $data['searchParam'] = $formData;
            $data['poolList'] = $this->question_model->poolListByStatus('1');
            $data['courseList'] = $this->question_model->courseListByStatus('1');
            $data['topicList'] = array(); //$this->question_model->topicListByStatus('1');
            $data['courseLearningObjectList'] = $this->question_model->courseLearningObjectListByStatus('1');
            $data['bloomTaxonomyList'] = $this->question_model->bloomTaxonomyListByStatus('1');
            $data['difficultLevelList'] = $this->question_model->difficultLevelListByStatus('1');


            $this->global['pageTitle'] = 'Examination Management System : Most Wrong Questions List';
            $this->global['pageCode'] = 'most_wrong_question.list';
            //print_r($subjectDetails);exit;
            $this->loadViews("most_wrong_question/list", $this->global, $data, NULL);
        }
    }

    function mostQuestions()
    {
        if ($this->checkAccess('most_question.list') == 1)
        {
            $this->loadAccessRestricted();
        }
        else
        {
            $formData = array();
 
        

                 $formData['id_pool'] = $this->security->xss_clean($this->input->post('id_pool'));
            $formData['id_course'] = $this->security->xss_clean($this->input->post('id_course'));
            $formData['id_topic'] = $this->security->xss_clean($this->input->post('id_topic'));
            $formData['id_course_learning_objective'] = $this->security->xss_clean($this->input->post('id_course_learning_objective'));
            $formData['id_bloom_taxonomy'] = $this->security->xss_clean($this->input->post('id_bloom_taxonomy'));
            $formData['id_difficult_level'] = $this->security->xss_clean($this->input->post('id_difficult_level'));
    if($_POST)
            {

            
                $data['accountCodeList'] = $this->reports_model->getMostRepeatedQuestions($formData);
            }


            $data['searchParam'] = $formData;
            $data['poolList'] = $this->question_model->poolListByStatus('1');
            $data['courseList'] = $this->question_model->courseListByStatus('1');
            $data['topicList'] = array(); //$this->question_model->topicListByStatus('1');
            $data['courseLearningObjectList'] = $this->question_model->courseLearningObjectListByStatus('1');
            $data['bloomTaxonomyList'] = $this->question_model->bloomTaxonomyListByStatus('1');
            $data['difficultLevelList'] = $this->question_model->difficultLevelListByStatus('1');



            $this->global['pageTitle'] = 'Examination Management System : Most Questions List';
            $this->global['pageCode'] = 'most_repeated_question.list';
            //print_r($subjectDetails);exit;
            $this->loadViews("most_question/list", $this->global, $data, NULL);
        }
    }


    function report(){

         $formData['id_location'] = $this->security->xss_clean($this->input->post('id_location'));
            $formData['name'] = $this->security->xss_clean($this->input->post('name'));
            $formData['id_exam_center'] = $this->security->xss_clean($this->input->post('id_exam_center'));

            $data['searchParam'] = $formData;

            $data['examEventList'] = $this->exam_event_model->examEventListSearch($formData);
            $data['locationList'] = $this->exam_event_model->examCenterLocationList();
            $data['examCenterList'] = $this->exam_event_model->examCenterList();

            $this->global['pageCode'] = 'report.list';
            $this->global['pageTitle'] = 'Examination Management System : Exam Events';
                        $this->loadViews("most_question/report", $this->global, $data, NULL);

      }       
     function downloadreport($id_exam_event)
   
    {


                      
                            $examEventDetails = $this->reports_model->getEventDetails($id_exam_event);



                            $examSetDetails = $this->reports_model->getEventDetails($id_exam_event);



        $fp = fopen('php://output', 'w');

         $header = array();

        array_push($header, "Exam Report");
        $file_name = 'report.csv';

        header("Content-type: application/csv");
        header("Content-Disposition: attachment; filename=" . $file_name);
        header("Pragma: no-cache");
        header("Expires: 0");

        fputcsv($fp, $header);

        $empty_header = array();

        array_push($empty_header, " ");
        fputcsv($fp, $empty_header);


        $dtheader = array();

        array_push($dtheader, "Name");
        array_push($dtheader, $examEventDetails[0]->name);

        fputcsv($fp, $dtheader);

          $dtheader = array();

        array_push($dtheader, "Exam Date");
        array_push($dtheader, date('d-m-Y',strtotime($examEventDetails[0]->exam_date)));

        fputcsv($fp, $dtheader);

          $dtheader = array();

     

        array_push($dtheader, "Candidates");
        array_push($dtheader, $examEventDetails[0]->max_count);

        fputcsv($fp, $dtheader);


       $dtheader = array();

     

        array_push($dtheader, "Full Name");
        array_push($dtheader, "Attendence Status");
        array_push($dtheader, "Batch Number ");
        array_push($dtheader, "MCQ Marks");
        array_push($dtheader, "Essay Marks");
        array_push($dtheader, "Total Marks");
        array_push($dtheader, "Grade");

        fputcsv($fp, $dtheader);



        $getAllStudentForExamId = $this->reports_model->getAllStudentsByExamEventId($id_exam_event);




        for($i=0;$i<count($getAllStudentForExamId);$i++) {
           $dtheader = array();

            $attendenceStatus = 'N/A';

            if($getAllStudentForExamId[$i]->attendence_status=='1') {
                $attendenceStatus = 'Present';
            }
             if($getAllStudentForExamId[$i]->attendence_status=='2') {
                $attendenceStatus = 'Absent';
            }
             if($getAllStudentForExamId[$i]->attendence_status=='3') {
                $attendenceStatus = 'Absent with Valid Reason -'.$getAllStudentForExamId[$i]->attendence_status_reason;
            }


           array_push($dtheader, $getAllStudentForExamId[$i]->full_name);
           array_push($dtheader, $attendenceStatus);
           array_push($dtheader, $getAllStudentForExamId[$i]->batch);
           array_push($dtheader, $getAllStudentForExamId[$i]->mcq_total);
           array_push($dtheader, $getAllStudentForExamId[$i]->essay_total);
           array_push($dtheader, $getAllStudentForExamId[$i]->final_total);



                $overallMArks = $getAllStudentForExamId[$i]->final_total;



                if($overallMArks>69.9) {
                  $grade = "PASS";
                } else {
                  $grade = "FAIL";
                }

                if($attendenceStatus == 'N/A'){
                    $grade='N/A';
                }


           array_push($dtheader, $grade);
fputcsv($fp, $dtheader);


        }


        }
    

}
