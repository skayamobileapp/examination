<?php 
if(!defined('BASEPATH')) exit('No direct script access allowed');

class Reports_model extends CI_Model
{
    

    function getAnswerToTheQuestion($id_question)
    {
        $this->db->select('qho.*');
        $this->db->from('question_has_option as qho');
        $this->db->where('qho.id_question', $id_question);
        $this->db->where('qho.is_correct_answer', 1);
        // $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->row();

         return $result;
    }

    function getOptionsByQuestionId($id_question)
    {
        $this->db->select('qho.*');
        $this->db->from('question_has_option as qho');
        $this->db->where('qho.id_question', $id_question);
        // $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();

         return $result;
    }

    function getMostRepeatedQuestions($data)
    {
        $is_correct_answer = 0; 
        $this->db->select('SUM(que.status) as totalquestion,a.id_question,que.*');
        $this->db->from('student_question_set as a');
        $this->db->join('question as que','a.id_question = que.id');
        $this->db->join('question_has_option as qho','a.id_answer = qho.id');
        $this->db->where('qho.is_correct_answer=', $is_correct_answer);

           if ($data['id_pool'] != '') {
            $this->db->where('que.id_pool', $data['id_pool']);
        }
        if ($data['id_course'] != '') {
            $this->db->where('que.id_course', $data['id_course']);
        }
        if ($data['id_topic'] != '') {
            $this->db->where('que.id_topic', $data['id_topic']);
        }
        if ($data['id_course_learning_objective'] != '') {
            $this->db->where('que.id_course_learning_objective', $data['id_course_learning_objective']);
        }
        if ($data['id_bloom_taxonomy'] != '') {
            $this->db->where('que.id_bloom_taxonomy', $data['id_bloom_taxonomy']);
        }
        if ($data['id_difficult_level'] != '') {
            $this->db->where('que.id_difficult_level', $data['id_difficult_level']);
        }


        $this->db->group_by('a.id_question');
        $this->db->order_by("totalquestion", "desc");
        // $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();

         return $result;

    }


    function getMostWrongQuestions($data)
    {
        $this->db->select('SUM(que.status) as totalquestion,a.id_question,que.*');
        $this->db->from('student_question_set as a');
        $this->db->join('question as que','a.id_question = que.id');
        $this->db->group_by('a.id_question');
            $this->db->order_by("totalquestion", "desc");

                if ($data['id_pool'] != '') {
            $this->db->where('que.id_pool', $data['id_pool']);
        }
        if ($data['id_course'] != '') {
            $this->db->where('que.id_course', $data['id_course']);
        }
        if ($data['id_topic'] != '') {
            $this->db->where('que.id_topic', $data['id_topic']);
        }
        if ($data['id_course_learning_objective'] != '') {
            $this->db->where('que.id_course_learning_objective', $data['id_course_learning_objective']);
        }
        if ($data['id_bloom_taxonomy'] != '') {
            $this->db->where('que.id_bloom_taxonomy', $data['id_bloom_taxonomy']);
        }
        if ($data['id_difficult_level'] != '') {
            $this->db->where('que.id_difficult_level', $data['id_difficult_level']);
        }


        // $this->db->order_by("name", "ASC");
         $query = $this->db->get();
         $result = $query->result();

         return $result;

    }

    function getQuestionCount($id_question)
    {
            $this->db->select('*');
            $this->db->from('student_question_set');
            $this->db->where('id_question', $id_question);
            $this->db->order_by("id", "desc");
            $query = $this->db->get();

            $result = $query->num_rows();
            return $result;
    }

    function getQuestion($id_question)
    {
        $this->db->select('qho.*');
        $this->db->from('question as qho');
        $this->db->where('qho.id', $id_question);
         $query = $this->db->get();
         $result = $query->row();

         return $result;
    }


     function examEventList()
    {
        // $date = 
        $this->db->select('a.*');
        $this->db->from('exam_event as a');
        $this->db->order_by("a.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  

        
         return $result;
    }

    function getEventDetails($id) {
         $this->db->select('DISTINCT(a.id) as id, a.*,examname.name as examname');
        $this->db->from('exam_event as a');
        $this->db->join('exam_center_location as ecl', 'a.id_location = ecl.id');
        $this->db->join('exam_name as examname', 'a.id_exam_name = examname.id');
        $this->db->join('exam_center as ec', 'a.id_exam_center = ec.id');
       
            $this->db->where('a.id', $id);
       
        $this->db->order_by("a.name", "ASC");
         $query = $this->db->get();
         $result = $query->result();  

       
         return $result;

    }

    function getAllStudentsByExamEventId($id_exam_event) {

        $this->db->select('a.*,a.id as id_exam_student_tagging, s.full_name, s.nric,s.batch,s.membership_number');
        $this->db->from('exam_student_tagging as a');
        $this->db->join('student as s','a.id_student = s.id');
        $this->db->join('exam_event as ee','a.id_exam_event = ee.id');
        $this->db->where('a.id_exam_event', $id_exam_event);
        
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

    function allmcqcorrectAnswer($id_exam_student_tagging) {

         $this->db->select('est.*');
        $this->db->from('student_question_set as est');
        $this->db->join('question  as qu','qu.id = est.id_question');        
        $this->db->where('est.id_exam_student_tagging', $id_exam_student_tagging);
        $this->db->where("qu.question_type='1'");        
        $query = $this->db->get();
        $results = $query->result(); 
        return $results;
    
    }


    function totalMCQQuestionMarked($id_exam_student_tagging){
        $this->db->select('est.*,qu.marks as originalmarks,q.marks as answermarks');
        $this->db->from('student_question_set as est');
        $this->db->join('student_question_set_marks  as q','q.id_student_question_set = est.id');       
        $this->db->join('question  as qu','qu.id = est.id_question');        

        $this->db->where('est.id_exam_student_tagging', $id_exam_student_tagging);
        $this->db->where("qu.question_type='2'");        
        $query = $this->db->get();
        $results = $query->result(); 
        return $results;
    }



}