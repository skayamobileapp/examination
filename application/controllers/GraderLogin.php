<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class GraderLogin extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('grader_login_model');
    }
 
    public function index()
    {
        $this->checkGraderLoggedIn();

    }
    

    function checkGraderLoggedIn()
    {
        $isGraderLoggedIn = $this->session->userdata('isGraderLoggedIn');
        
        if(!isset($isGraderLoggedIn) || $isGraderLoggedIn != TRUE)
        {
            $this->load->view('grader_login');
        }
        else
        {

                redirect('/grader/graderStudentTag/list');                
        }
    }


    public function graderLogin()
    {

        $formData = $this->input->post();
        // echo "<Pre>"; print_r($formData);exit;
        $domain = $this->getDomainName();

        $this->load->library('form_validation');
        
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email|max_length[128]|trim');
        $this->form_validation->set_rules('password', 'Password', 'required|max_length[32]');
        
        if($this->form_validation->run() == FALSE)
        {
            $this->index();
        }
        else
        {
            $email = strtolower($this->security->xss_clean($this->input->post('email')));
            $password = $this->input->post('password');
            
            $result = $this->grader_login_model->loginExamCenter($email, $password);
        
            
            if(!empty($result))
            {

                $sessionArray = array(
                                    'id_grader'=>$result->id,                    
                                    'grader_name'=>$result->full_name,
                                    'grader_email'=>$result->email,
                                    'grader_nric'=>$result->nric,
                                    'grader_mobile'=>$result->mobile,
                                    'isGraderLoggedIn'=> TRUE

                                );

                $this->session->set_userdata($sessionArray);


                $loginInfo = array("id_grader"=>$result->id, "session_data" => json_encode($sessionArray), "machine_ip"=>$_SERVER['REMOTE_ADDR'], "user_agent"=>getBrowserAgent(), "agent_string"=>$this->agent->agent_string(), "platform"=>$this->agent->platform());

                $this->grader_login_model->addGraderLastLogin($loginInfo);

                // echo "Login";exit();
                // echo md5($uniqueId);exit();
                redirect('/grader/graderStudentTag/list');                
            }
            else
            {
                $this->session->set_flashdata('error', 'Email or password mismatch');
                
                $this->index();
            }
        }
    }
}

?>