<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class : Login (LoginController)
 * Login class to control to authenticate user credentials and starts user's session.
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class StudentLogin extends BaseController
{
    /**
     * This is default constructor of the class
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('student_login_model');
    }
 
    /**
     * Index Page for this controller.
     */
    public function index()
    {
                    $this->load->view('student_login');

    }
    

    function checkStudentLoggedIn()
    {
        $isStudentLoggedIn = $this->session->userdata('isStudentLoggedIn');
        
        if(!isset($isStudentLoggedIn) || $isStudentLoggedIn != TRUE)
        {
            $this->load->view('student_login');
        }
        else
        {
                redirect('/student_login');                
        }
    }


     function studentautologin($key,$token)
    {

            if($key!='XCD7LjfYU2PyYeV20unG') {
                     $tokenArray['status'] = '402';
                     $tokenArray['description'] = 'Not a Valid Token';
            }
            else {

                $result = $this->student_login_model->checktokenlogin($token);
            
            if(!empty($result))
            {

                $sessionArray = array('id_student'=>$result->id,                    
                                        'isStudentLoggedIn' => TRUE,
                                        'student_name'=>$result->full_name
                                );

                $this->session->set_userdata($sessionArray);


                $loginInfo = array("id_student"=>$result->id, "session_data" => json_encode($sessionArray), "machine_ip"=>$_SERVER['REMOTE_ADDR'], "user_agent"=>getBrowserAgent(), "agent_string"=>$this->agent->agent_string(), "platform"=>$this->agent->platform());

                $uniqueId = rand(0000000000,9999999999);
                $this->session->set_userdata("my_student_session_id", md5($uniqueId));


                // $this->student_login_model->addStudentLastLogin($loginInfo);

                // echo "Login";exit();
                // echo md5($uniqueId);exit();
                 $input = md5($result->full_name);
                redirect('/studentexamination/exam/instructions/'.$input); 
            }
             else {
                 $tokenArray['status'] = '402';
                     $tokenArray['tokenid'] = "Not a valid User"; 


             }

                
            }

            
            
          
       
    }


    function getToken()
    {
        

 $tokenArray = array();

             $token =$_POST['token'];

          $user_module_id = strtolower($this->security->xss_clean($this->input->post('user_module_id')));

           $mqms_assessment_component_id = strtolower($this->security->xss_clean($this->input->post('mqms_assessment_component_id')));




             if($token!='XCD7LjfYU2PyYeV20unG') {

                     $tokenArray['status'] = '402';
                     $tokenArray['description'] = 'Not a Valid Token';
            } else {

            
                    $result = $this->student_login_model->getToken($user_module_id, $mqms_assessment_component_id);

                    if(!empty($result))
                    {

                                $tokenArray['status'] = '200';
                     $tokenArray['tokenid'] = $result->token;        
                    }
                    else
                    {
                        $tokenArray['status'] = '402';
                     $tokenArray['tokenid'] = "Not a valid User module id or Assessment Id";     
                    }
                }


   echo  json_encode($tokenArray);
    exit;

       
    }
      function getMasterClassToken()
    {
        

 $tokenArray = array();

             $token =$_POST['token'];

          $user_module_id = strtolower($this->security->xss_clean($this->input->post('user_masterclass_id')));

           $mqms_assessment_component_id = strtolower($this->security->xss_clean($this->input->post('mqms_assessment_component_id')));




             if($token!='XCD7LjfYU2PyYeV20unG') {

                     $tokenArray['status'] = '402';
                     $tokenArray['description'] = 'Not a Valid Token';
            } else {

            
                    $result = $this->student_login_model->getMasterClassToken($user_module_id, $mqms_assessment_component_id);

                    if(!empty($result))
                    {

                                $tokenArray['status'] = '200';
                     $tokenArray['tokenid'] = $result->token;        
                    }
                    else
                    {
                        $tokenArray['status'] = '402';
                     $tokenArray['tokenid'] = "Not a valid User module id or Assessment Id";     
                    }
                }


   echo  json_encode($tokenArray);
    exit;

       
    }


    public function studentLogin()
    {
        $formData = $this->input->post();
        // echo "<Pre>"; print_r($formData);exit;
        $domain = $this->getDomainName();

        $this->load->library('form_validation');


        $token = strtolower($this->security->xss_clean($this->input->post('token')));

        $result = $this->student_login_model->checktokenlogin($token);
            
        if(!empty($result))
        {

            $sessionArray = array('id_student'=>$result->id,                    
                                    'isStudentLoggedIn' => TRUE,
                                    'student_name'=>$result->full_name
                            );

            $this->session->set_userdata($sessionArray);


            $loginInfo = array("id_student"=>$result->id, "session_data" => json_encode($sessionArray), "machine_ip"=>$_SERVER['REMOTE_ADDR'], "user_agent"=>getBrowserAgent(), "agent_string"=>$this->agent->agent_string(), "platform"=>$this->agent->platform());

            $uniqueId = rand(0000000000,9999999999);
            $this->session->set_userdata("my_student_session_id", md5($uniqueId));


            // $this->student_login_model->addStudentLastLogin($loginInfo);

            // echo "Login";exit();
            // echo md5($uniqueId);exit();
             $input = md5($result->full_name);
            redirect('/studentexamination/exam/instructions/'.$input); 
        }else
            {
                $this->session->set_flashdata('error', 'Either Token or Exam Date Not matching');
                
                $this->index();
            }
        }
    
}

?>