<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class : Login (LoginController)
 * Login class to control to authenticate user credentials and starts user's session.
 * @author : Kishor Mali
 * @version : 1.1
 * @since : 15 November 2016
 */
class Cron extends BaseController
{
    

    public function index(){
        //
        $currentDateTime = date('Y-m-d H:i');
        $this->load->model('exam_center_model');


        $exameventlist = $this->exam_center_model->getAllExamEventList($currentDateTime);



         for($i=0;$i<count($exameventlist);$i++) {
            $examEventId = $exameventlist[$i]->id;

                   $data = array(
                    'id_exam_event' => $examEventId,
                    'exam_start_time' => date('Y-m-d H:i:s'),
                    'exam_status' => 1,
                    'id_exam_center' => 0
                   );
                  $this->exam_center_model->addExamStart($data);

         }
        
    }

   
}

?>