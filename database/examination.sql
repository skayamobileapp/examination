-- phpMyAdmin SQL Dump
-- version 4.9.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 18, 2021 at 03:22 AM
-- Server version: 10.2.37-MariaDB-log
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `camsedu_examination`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_last_login`
--

CREATE TABLE `admin_last_login` (
  `id` bigint(20) NOT NULL,
  `id_user` bigint(20) NOT NULL,
  `session_data` varchar(2048) NOT NULL,
  `machine_ip` varchar(1024) NOT NULL,
  `user_agent` varchar(128) NOT NULL,
  `agent_string` varchar(1024) NOT NULL,
  `platform` varchar(128) NOT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `answer_status`
--

CREATE TABLE `answer_status` (
  `id` int(11) NOT NULL,
  `code` varchar(1024) DEFAULT '',
  `name` varchar(1024) DEFAULT '',
  `status` int(11) DEFAULT 0,
  `created_by` int(11) DEFAULT NULL,
  `reason` varchar(2048) DEFAULT '',
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `answer_status`
--

INSERT INTO `answer_status` (`id`, `code`, `name`, `status`, `created_by`, `reason`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, '', 'Active', 1, 1, '', '2020-10-07 12:32:42', NULL, '2020-10-07 12:32:42'),
(2, '', 'In-active', 1, 1, '', '2020-10-07 12:34:26', NULL, '2020-10-07 12:34:26'),
(3, '', 'Draft', 1, 1, '', '2020-10-07 12:34:26', NULL, '2020-10-07 12:34:26'),
(4, '', 'Moderated', 1, 1, '', '2020-10-07 12:34:26', NULL, '2020-10-07 12:34:26'),
(5, '', 'Active But Re-moderated', 1, 1, '', '2020-10-07 12:34:26', NULL, '2020-10-07 12:34:26');

-- --------------------------------------------------------

--
-- Table structure for table `bank_registration`
--

CREATE TABLE `bank_registration` (
  `id` int(11) NOT NULL,
  `name` varchar(580) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `account_no` varchar(512) DEFAULT '',
  `bank_id` varchar(50) DEFAULT '',
  `address` varchar(580) DEFAULT '',
  `landmark` varchar(580) DEFAULT '',
  `city` varchar(580) DEFAULT '',
  `id_state` int(11) DEFAULT NULL,
  `id_country` int(11) DEFAULT NULL,
  `zipcode` int(11) DEFAULT NULL,
  `cr_fund` varchar(50) DEFAULT '',
  `cr_department` varchar(50) DEFAULT '',
  `cr_activity` varchar(50) DEFAULT '',
  `cr_account` varchar(50) DEFAULT '',
  `status` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bloom_taxonomy`
--

CREATE TABLE `bloom_taxonomy` (
  `id` int(11) NOT NULL,
  `name` varchar(1024) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `name_optional_language` varchar(1024) DEFAULT '',
  `status` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bloom_taxonomy`
--

INSERT INTO `bloom_taxonomy` (`id`, `name`, `code`, `name_optional_language`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Recall', 'Recall', '', 1, 1, '2020-08-14 18:17:14', NULL, '2020-08-14 18:17:14'),
(2, 'Understanding', 'Understanding', '', 1, 1, '2020-08-14 18:18:53', NULL, '2020-08-14 18:18:53'),
(3, 'Application', 'Application', '', 1, 1, '2020-08-14 18:19:05', NULL, '2020-08-14 18:19:05'),
(4, 'Analysis', 'Analysis', '', 1, 1, '2020-08-14 18:19:18', 1, '2020-08-14 18:19:18');

-- --------------------------------------------------------

--
-- Table structure for table `communication_group`
--

CREATE TABLE `communication_group` (
  `id` int(11) NOT NULL,
  `id_template` int(11) DEFAULT 0,
  `name` varchar(1024) DEFAULT '',
  `type` varchar(200) DEFAULT '',
  `status` int(11) DEFAULT 0,
  `created_by` int(11) DEFAULT NULL,
  `reason` varchar(2048) DEFAULT '',
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `communication_group`
--

INSERT INTO `communication_group` (`id`, `id_template`, `name`, `type`, `status`, `created_by`, `reason`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 'Student Final Year 2020', 'Student', 1, 1, '', '2020-10-02 15:52:05', NULL, '2020-10-02 15:52:05');

-- --------------------------------------------------------

--
-- Table structure for table `communication_group_message`
--

CREATE TABLE `communication_group_message` (
  `id` int(11) NOT NULL,
  `id_template` int(11) DEFAULT 0,
  `name` varchar(1024) DEFAULT '',
  `type` varchar(200) DEFAULT '',
  `status` int(11) DEFAULT 0,
  `created_by` int(11) DEFAULT NULL,
  `reason` varchar(2048) DEFAULT '',
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `communication_group_message`
--

INSERT INTO `communication_group_message` (`id`, `id_template`, `name`, `type`, `status`, `created_by`, `reason`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 'Group One', 'Student', 1, 1, '', '2020-10-28 02:16:08', NULL, '2020-10-28 02:16:08');

-- --------------------------------------------------------

--
-- Table structure for table `communication_group_message_recepients`
--

CREATE TABLE `communication_group_message_recepients` (
  `id` int(11) NOT NULL,
  `id_group` int(11) DEFAULT 0,
  `id_recepient` int(11) DEFAULT 0,
  `type` varchar(200) DEFAULT '',
  `status` int(11) DEFAULT 0,
  `created_by` int(11) DEFAULT NULL,
  `reason` varchar(2048) DEFAULT '',
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `communication_group_message_recepients`
--

INSERT INTO `communication_group_message_recepients` (`id`, `id_group`, `id_recepient`, `type`, `status`, `created_by`, `reason`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 2, 'Student', 1, 1, '', '2020-10-28 02:22:46', NULL, '2020-10-28 02:22:46'),
(2, 1, 3, 'Student', 1, 1, '', '2020-10-28 02:22:46', NULL, '2020-10-28 02:22:46');

-- --------------------------------------------------------

--
-- Table structure for table `communication_group_recepients`
--

CREATE TABLE `communication_group_recepients` (
  `id` int(11) NOT NULL,
  `id_group` int(11) DEFAULT 0,
  `id_recepient` int(11) DEFAULT 0,
  `type` varchar(200) DEFAULT '',
  `status` int(11) DEFAULT 0,
  `created_by` int(11) DEFAULT NULL,
  `reason` varchar(2048) DEFAULT '',
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `communication_group_recepients`
--

INSERT INTO `communication_group_recepients` (`id`, `id_group`, `id_recepient`, `type`, `status`, `created_by`, `reason`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(2, 1, 4, 'Student', 1, 1, '', '2020-10-02 15:52:22', NULL, '2020-10-02 15:52:22');

-- --------------------------------------------------------

--
-- Table structure for table `communication_template`
--

CREATE TABLE `communication_template` (
  `id` int(11) NOT NULL,
  `name` text DEFAULT NULL,
  `subject` text DEFAULT NULL,
  `message` text DEFAULT NULL,
  `status` int(11) DEFAULT 0,
  `created_by` int(11) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `communication_template`
--

INSERT INTO `communication_template` (`id`, `name`, `subject`, `message`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Fee Instruction', 'Fee Payment', '<p>Pay Fee Amount</p>\r\n', 1, 1, '2020-10-02 15:51:01', NULL, '2020-10-02 15:51:01');

-- --------------------------------------------------------

--
-- Table structure for table `communication_template_message`
--

CREATE TABLE `communication_template_message` (
  `id` int(11) NOT NULL,
  `name` text DEFAULT NULL,
  `subject` text DEFAULT NULL,
  `message` text DEFAULT NULL,
  `status` int(11) DEFAULT 0,
  `created_by` int(11) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `communication_template_message`
--

INSERT INTO `communication_template_message` (`id`, `name`, `subject`, `message`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Template', 'Subject', '<p>Sample Template </p>\r\n', 1, 1, '2020-10-28 02:15:34', NULL, '2020-10-28 02:15:34');

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `id` int(11) NOT NULL,
  `name` varchar(120) DEFAULT '',
  `status` int(11) DEFAULT 1,
  `created_by` int(11) DEFAULT 0,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT 0,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`id`, `name`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Malaysia', 1, 0, '2020-02-27 15:59:29', 0, '2020-02-27 15:59:29');

-- --------------------------------------------------------

--
-- Table structure for table `course`
--

CREATE TABLE `course` (
  `id` int(11) NOT NULL,
  `name` varchar(1024) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `name_optional_language` varchar(1024) DEFAULT '',
  `status` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `course`
--

INSERT INTO `course` (`id`, `name`, `code`, `name_optional_language`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(3, 'FOUNDATION LEVELSHARIAH', 'FOUNDATION LEVELSHARIAH.', '', 1, 1, '2020-08-14 18:04:56', 1, '2020-08-14 18:04:56'),
(4, 'FOUNDATION LEVEL OVERVIEW OF FINANCIAL PRODUCTS', 'FOUNDATION LEVELOVERVIEW OF FINANCIAL PRODUCTS', '', 1, 1, '2020-08-14 18:05:07', 1, '2020-08-14 18:05:07');

-- --------------------------------------------------------

--
-- Table structure for table `course_learning_objective`
--

CREATE TABLE `course_learning_objective` (
  `id` int(11) NOT NULL,
  `name` varchar(1024) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `name_optional_language` varchar(1024) DEFAULT '',
  `status` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `course_learning_objective`
--

INSERT INTO `course_learning_objective` (`id`, `name`, `code`, `name_optional_language`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Understanding', 'Understanding', '', 1, 1, '2020-08-14 18:17:50', NULL, '2020-08-14 18:17:50'),
(2, 'Application', 'Application', '', 1, 1, '2020-08-14 18:18:04', NULL, '2020-08-14 18:18:04'),
(3, 'Recall', 'Recall', '', 1, 1, '2020-08-14 18:18:17', NULL, '2020-08-14 18:18:17'),
(4, 'Analysis', 'Analysis', '', 1, 1, '2020-08-14 18:18:33', NULL, '2020-08-14 18:18:33');

-- --------------------------------------------------------

--
-- Table structure for table `currency_rate_setup`
--

CREATE TABLE `currency_rate_setup` (
  `id` int(11) NOT NULL,
  `id_currency` int(11) DEFAULT 0,
  `exchange_rate` varchar(200) DEFAULT '',
  `min_rate` varchar(200) DEFAULT '',
  `max_rate` varchar(200) DEFAULT '',
  `effective_date` datetime DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `currency_setup`
--

CREATE TABLE `currency_setup` (
  `id` int(11) NOT NULL,
  `code` varchar(2048) DEFAULT '',
  `name` varchar(2048) DEFAULT '',
  `name_optional_language` varchar(2048) DEFAULT '',
  `prefix` varchar(200) DEFAULT '',
  `suffix` varchar(200) DEFAULT '',
  `decimal_place` int(11) DEFAULT 0,
  `default` int(11) DEFAULT 0,
  `status` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `difficult_level`
--

CREATE TABLE `difficult_level` (
  `id` int(11) NOT NULL,
  `name` varchar(1024) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `name_optional_language` varchar(1024) DEFAULT '',
  `status` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `difficult_level`
--

INSERT INTO `difficult_level` (`id`, `name`, `code`, `name_optional_language`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'C1', 'C1', '', 1, 1, '2020-08-14 18:21:39', 1, '2020-08-14 18:21:39'),
(2, 'C2', 'C2', '', 1, 1, '2020-08-14 18:21:48', 1, '2020-08-14 18:21:48'),
(3, 'C3', 'C3', '', 1, 1, '2020-08-14 18:21:58', 1, '2020-08-14 18:21:58');

-- --------------------------------------------------------

--
-- Table structure for table `examset`
--

CREATE TABLE `examset` (
  `id` bigint(20) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `instructions` text DEFAULT NULL,
  `duration` int(11) DEFAULT NULL,
  `pass_grade` int(11) DEFAULT NULL,
  `attempts` varchar(100) DEFAULT NULL,
  `grading_method` int(11) DEFAULT NULL,
  `layout` int(11) DEFAULT NULL,
  `behaviour` int(11) DEFAULT NULL,
  `status` tinyint(4) NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `updated_by` bigint(20) NOT NULL,
  `created_dt_tm` datetime NOT NULL,
  `updated_dt_tm` datetime NOT NULL,
  `attempt_duration` int(11) DEFAULT NULL,
  `id_tos` bigint(20) DEFAULT NULL,
  `tos_status` int(11) DEFAULT NULL,
  `id_manual_tos` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `examset`
--

INSERT INTO `examset` (`id`, `name`, `instructions`, `duration`, `pass_grade`, `attempts`, `grading_method`, `layout`, `behaviour`, `status`, `created_by`, `updated_by`, `created_dt_tm`, `updated_dt_tm`, `attempt_duration`, `id_tos`, `tos_status`, `id_manual_tos`) VALUES
(1, 'Demo CIIF', '<p>This question paper is divided into two sections:</p>\r\n\r\n<ul>\r\n <li>\r\n <p><strong>Section A </strong>– ALL 30 questions are compulsory and MUST be attempted</p>\r\n </li>\r\n <li>\r\n <p><strong>Section B</strong> – ALL four (4) questions are compulsory and MUST be attempted</p>\r\n </li>\r\n</ul>\r\n\r\n<p><strong>Important:</strong> The Chartered Institute of Islamic Finance Professionals reserves the right to withdraw or invalidate results at any time pertaining to any alleged breach of examination regulations and procedures.</p>\r\n\r\n<p> </p>\r\n\r\n<p> </p>\r\n', 200, 0, '1', 1, 4, 1, 1, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 6, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `examset_questions`
--

CREATE TABLE `examset_questions` (
  `id` bigint(20) NOT NULL,
  `examset_code` varchar(50) DEFAULT NULL,
  `id_examset` bigint(20) DEFAULT NULL,
  `id_question` bigint(20) DEFAULT NULL,
  `id_user` bigint(20) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `created_dt_tm` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_dt_tm` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `exam_center`
--

CREATE TABLE `exam_center` (
  `id` int(11) NOT NULL,
  `name` varchar(580) DEFAULT '',
  `id_tos` int(11) DEFAULT 0,
  `contact_person` varchar(512) DEFAULT '',
  `contact_number` varchar(20) DEFAULT '',
  `email` varchar(512) DEFAULT '',
  `user_name` varchar(512) DEFAULT '',
  `password` varchar(512) DEFAULT '',
  `exam_type` varchar(512) DEFAULT '',
  `rooms` int(11) DEFAULT 0,
  `room_capacity` int(11) DEFAULT 0,
  `id_country` int(11) DEFAULT NULL,
  `id_state` int(11) DEFAULT NULL,
  `city` varchar(250) DEFAULT '',
  `id_location` int(11) DEFAULT 0,
  `zipcode` varchar(50) DEFAULT '',
  `address` varchar(580) DEFAULT '',
  `status` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exam_center`
--

INSERT INTO `exam_center` (`id`, `name`, `id_tos`, `contact_person`, `contact_number`, `email`, `user_name`, `password`, `exam_type`, `rooms`, `room_capacity`, `id_country`, `id_state`, `city`, `id_location`, `zipcode`, `address`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updeated_dt_tm`) VALUES
(1, 'CIIF Exam Center KL', 0, 'Person 1', 'PErson 2', 'klexamcenter@gmail.com', 'klexamcenter@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'Online', 3, 0, 1, 2, 'KL', 1, '2312312', 'KL', 1, 1, '2020-10-27 10:29:41', NULL, '2020-10-27 10:29:41');

-- --------------------------------------------------------

--
-- Table structure for table `exam_center_has_room`
--

CREATE TABLE `exam_center_has_room` (
  `id` int(11) NOT NULL,
  `id_exam_center` int(11) DEFAULT 0,
  `room` varchar(120) DEFAULT '',
  `capacity` int(11) DEFAULT 0,
  `status` int(11) DEFAULT 1,
  `created_by` int(11) DEFAULT 0,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT 0,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `exam_center_last_login`
--

CREATE TABLE `exam_center_last_login` (
  `id` bigint(20) NOT NULL,
  `id_exam_center` bigint(20) NOT NULL,
  `session_data` varchar(2048) NOT NULL,
  `machine_ip` varchar(1024) NOT NULL,
  `user_agent` varchar(128) NOT NULL,
  `agent_string` varchar(1024) NOT NULL,
  `platform` varchar(128) NOT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `exam_center_location`
--

CREATE TABLE `exam_center_location` (
  `id` int(11) NOT NULL,
  `name` varchar(250) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `status` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exam_center_location`
--

INSERT INTO `exam_center_location` (`id`, `name`, `code`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Kuala Lumpur', '', 1, NULL, '2020-10-27 10:28:36', NULL, '2020-10-27 10:28:36');

-- --------------------------------------------------------

--
-- Table structure for table `exam_center_start_exam`
--

CREATE TABLE `exam_center_start_exam` (
  `id` int(11) NOT NULL,
  `id_exam_event` int(11) DEFAULT NULL,
  `exam_start_time` datetime DEFAULT NULL,
  `exam_status` int(11) DEFAULT NULL,
  `id_exam_center` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exam_center_start_exam`
--

INSERT INTO `exam_center_start_exam` (`id`, `id_exam_event`, `exam_start_time`, `exam_status`, `id_exam_center`) VALUES
(1, 1, '2020-12-28 12:06:27', 1, 1),
(2, NULL, '2020-12-28 17:32:50', 1, 1),
(3, NULL, '2020-12-28 17:33:03', 1, 1),
(4, 2, '2020-12-28 17:35:04', 1, 0),
(5, 3, '2020-12-28 22:33:59', 1, 0),
(6, 4, '2020-12-30 23:06:46', 1, 0),
(7, 5, '2021-01-01 11:50:19', 1, 0),
(8, 6, '2021-02-10 21:54:47', 1, 0),
(9, 7, '2021-02-24 08:35:00', 1, 0),
(10, 8, '2021-02-28 17:24:09', 1, 0),
(11, 9, '2021-03-15 23:32:11', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `exam_event`
--

CREATE TABLE `exam_event` (
  `id` int(11) NOT NULL,
  `id_location` int(11) DEFAULT 0,
  `id_exam_center` int(11) DEFAULT 0,
  `id_exam_set` int(11) DEFAULT 0,
  `max_count` int(11) DEFAULT 0,
  `name` varchar(250) DEFAULT '',
  `type` varchar(250) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `exam_date` date DEFAULT NULL,
  `to_dt` varchar(50) DEFAULT '',
  `from_tm` varchar(50) DEFAULT '',
  `to_tm` varchar(50) DEFAULT '',
  `status` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp(),
  `id_exam_name` int(11) DEFAULT NULL,
  `tos_status` int(11) DEFAULT NULL,
  `id_manual_tos` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exam_event`
--

INSERT INTO `exam_event` (`id`, `id_location`, `id_exam_center`, `id_exam_set`, `max_count`, `name`, `type`, `code`, `exam_date`, `to_dt`, `from_tm`, `to_tm`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`, `id_exam_name`, `tos_status`, `id_manual_tos`) VALUES
(8, 1, 1, 1, 10, '28th Exam for Shairah', '2', '', '2021-03-14', '', '21:30', '22:00', 1, 1, '2021-02-28 17:20:30', NULL, '2021-02-28 17:20:30', 1, NULL, NULL),
(9, 1, 1, 1, 1, 'Shariah Exam', '2', '', '2021-03-15', '', '23:00', '23:55', 1, 1, '2021-03-15 10:26:00', NULL, '2021-03-15 10:26:00', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `exam_has_question`
--

CREATE TABLE `exam_has_question` (
  `id` int(11) NOT NULL,
  `id_exam_name` int(11) DEFAULT 0,
  `id_exam_has_count_details` int(11) DEFAULT 0,
  `id_question` int(11) DEFAULT 0,
  `id_course` int(11) DEFAULT 0,
  `id_topic` int(11) DEFAULT 0,
  `id_course_learning_objective` int(11) DEFAULT 0,
  `id_bloom_taxonomy` int(11) DEFAULT 0,
  `id_difficult_level` int(11) DEFAULT 0,
  `question_count` int(11) DEFAULT 0,
  `status` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `exam_has_question_count`
--

CREATE TABLE `exam_has_question_count` (
  `id` int(11) NOT NULL,
  `id_exam_name` int(11) DEFAULT 0,
  `id_course` int(11) DEFAULT 0,
  `id_topic` int(11) DEFAULT 0,
  `id_course_learning_objective` int(11) DEFAULT 0,
  `id_bloom_taxonomy` int(11) DEFAULT 0,
  `id_difficult_level` int(11) DEFAULT 0,
  `question_count` int(11) DEFAULT 0,
  `status` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `exam_module`
--

CREATE TABLE `exam_module` (
  `module_id` bigint(20) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `code` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exam_module`
--

INSERT INTO `exam_module` (`module_id`, `name`, `code`) VALUES
(1, 'Shariah', 'FG-SH-001'),
(2, 'Economics & the Financial System', 'FG-EC-001');

-- --------------------------------------------------------

--
-- Table structure for table `exam_name`
--

CREATE TABLE `exam_name` (
  `id` int(11) NOT NULL,
  `name` varchar(1024) DEFAULT '',
  `description` varchar(4096) DEFAULT '',
  `total_marks` int(11) DEFAULT 0,
  `total_question` int(11) DEFAULT 0,
  `total_time` int(11) DEFAULT 0,
  `min_marks` int(11) DEFAULT 0,
  `display_result_immediately` int(11) DEFAULT 0,
  `image` varchar(1024) DEFAULT '',
  `status` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exam_name`
--

INSERT INTO `exam_name` (`id`, `name`, `description`, `total_marks`, `total_question`, `total_time`, `min_marks`, `display_result_immediately`, `image`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'FOUNDATION OF  SHARIAH EXAM', 'FOUNDATION OF  SHARIAH EXAM', 0, 0, 0, 0, 0, '', 1, 1, '2020-12-27 22:31:11', 1, '2020-12-27 22:31:11');

-- --------------------------------------------------------

--
-- Table structure for table `exam_schedule`
--

CREATE TABLE `exam_schedule` (
  `schedule_id` bigint(20) NOT NULL,
  `module_id` bigint(20) DEFAULT NULL,
  `exam_start_date` timestamp NULL DEFAULT NULL,
  `exam_end_date` timestamp NULL DEFAULT NULL,
  `venue` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `exam_session`
--

CREATE TABLE `exam_session` (
  `id` int(11) NOT NULL,
  `name` varchar(512) DEFAULT '',
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `id_exam_center` int(11) DEFAULT 0,
  `id_exam_set` int(11) DEFAULT 0,
  `status` int(11) DEFAULT 1,
  `created_by` int(11) DEFAULT 0,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT 0,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `exam_student_tagging`
--

CREATE TABLE `exam_student_tagging` (
  `id` int(11) NOT NULL,
  `id_student` int(11) DEFAULT 0,
  `id_exam_event` int(11) DEFAULT 0,
  `status` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp(),
  `attendence_status` int(11) DEFAULT NULL,
  `extra_time` varchar(20) DEFAULT NULL,
  `token` varchar(250) DEFAULT NULL,
  `result_status` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exam_student_tagging`
--

INSERT INTO `exam_student_tagging` (`id`, `id_student`, `id_exam_event`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`, `attendence_status`, `extra_time`, `token`, `result_status`) VALUES
(12, 12, 8, 1, 1, '2021-02-28 17:21:24', NULL, '2021-02-28 17:21:24', 1, NULL, 'kiran', NULL),
(13, 13, 8, 1, 1, '2021-03-15 14:21:38', NULL, '2021-03-15 14:21:38', 1, NULL, NULL, NULL),
(14, 14, 9, 1, 1, '2021-03-15 11:00:39', NULL, '2021-03-15 11:00:39', 1, NULL, 'kiran', 13);

-- --------------------------------------------------------

--
-- Table structure for table `exam_users`
--

CREATE TABLE `exam_users` (
  `user_id` bigint(20) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `schedule_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `exam_venue`
--

CREATE TABLE `exam_venue` (
  `venue_id` bigint(20) NOT NULL,
  `name` int(11) NOT NULL,
  `address_1` text NOT NULL,
  `address_2` text NOT NULL,
  `city` int(11) NOT NULL,
  `country` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `fee_category`
--

CREATE TABLE `fee_category` (
  `id` int(11) NOT NULL,
  `code` varchar(520) DEFAULT '',
  `name` varchar(520) DEFAULT '',
  `name_optional_language` varchar(520) DEFAULT '',
  `fee_group` varchar(200) DEFAULT '',
  `sequence` int(11) DEFAULT 0,
  `status` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `fee_setup`
--

CREATE TABLE `fee_setup` (
  `id` int(11) NOT NULL,
  `code` varchar(50) DEFAULT '',
  `name` varchar(520) DEFAULT '',
  `name_optional_language` varchar(520) DEFAULT '',
  `id_fee_category` int(11) DEFAULT NULL,
  `id_amount_calculation_type` int(11) DEFAULT NULL,
  `id_frequency_mode` int(11) DEFAULT NULL,
  `account_code` varchar(50) DEFAULT '',
  `is_refundable` int(11) DEFAULT NULL,
  `is_non_invoice` int(11) DEFAULT NULL,
  `is_gst` int(11) DEFAULT NULL,
  `gst_tax` varchar(50) DEFAULT '',
  `effective_date` datetime DEFAULT current_timestamp(),
  `status` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `financial_account_code`
--

CREATE TABLE `financial_account_code` (
  `id` int(11) NOT NULL,
  `name` varchar(520) DEFAULT '',
  `type` varchar(512) DEFAULT '',
  `name_optional_language` varchar(520) DEFAULT '',
  `short_code` varchar(50) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `level` int(11) DEFAULT 0,
  `id_parent` int(11) DEFAULT 0,
  `status` int(11) DEFAULT 1,
  `created_by` int(11) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `grade`
--

CREATE TABLE `grade` (
  `id` int(11) NOT NULL,
  `name` varchar(1024) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `name_optional_language` varchar(1024) DEFAULT '',
  `min_percentage` int(11) DEFAULT NULL,
  `max_percentage` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp(),
  `id_course` int(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `grade`
--

INSERT INTO `grade` (`id`, `name`, `code`, `name_optional_language`, `min_percentage`, `max_percentage`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`, `id_course`) VALUES
(1, 'FAIL', '', '', 1, 69, 1, 1, '2020-12-27 22:30:17', 1, '2020-12-27 22:30:17', 3),
(2, 'PASS', '', '', 70, 100, 1, 1, '2020-12-27 22:30:26', NULL, '2020-12-27 22:30:26', 3);

-- --------------------------------------------------------

--
-- Table structure for table `grader`
--

CREATE TABLE `grader` (
  `id` int(11) NOT NULL,
  `full_name` varchar(1024) DEFAULT '',
  `salutation` varchar(200) DEFAULT '',
  `first_name` varchar(1024) DEFAULT '',
  `last_name` varchar(1024) DEFAULT '',
  `nric` varchar(200) DEFAULT '',
  `mobile` varchar(50) DEFAULT '',
  `email` varchar(1024) DEFAULT '',
  `password` varchar(1024) DEFAULT '',
  `status` int(11) DEFAULT 0,
  `created_by` int(11) DEFAULT NULL,
  `reason` varchar(2048) DEFAULT '',
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp(),
  `type` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `grader`
--

INSERT INTO `grader` (`id`, `full_name`, `salutation`, `first_name`, `last_name`, `nric`, `mobile`, `email`, `password`, `status`, `created_by`, `reason`, `created_dt_tm`, `updated_by`, `updated_dt_tm`, `type`) VALUES
(4, 'Mr. grader  one', '1', 'grader ', 'one', '1', '2', 'grader1@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 1, 1, '', '2021-02-28 17:22:01', NULL, '2021-02-28 17:22:01', '1'),
(5, 'Mr. grader 2 2', '1', 'grader 2', '2', 'grader2', '123123123123', 'grader2@gmail.com', 'ac3846c516c85ed2d63bb6db3e0548af', 1, 1, '', '2021-03-18 03:23:29', NULL, '2021-03-18 03:23:29', '1');

-- --------------------------------------------------------

--
-- Table structure for table `grader_exam`
--

CREATE TABLE `grader_exam` (
  `id` int(11) NOT NULL,
  `id_grader` int(11) DEFAULT 0,
  `id_exam_name` int(11) DEFAULT 0,
  `id_exam_center` int(11) DEFAULT 0,
  `id_exam_event` int(11) DEFAULT 0,
  `exam_time` varchar(200) DEFAULT '',
  `exam_date` varchar(200) DEFAULT '',
  `status` int(11) DEFAULT 0,
  `created_by` int(11) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp(),
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `id_grader_type` varchar(20) DEFAULT NULL,
  `student_selection` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `grader_exam`
--

INSERT INTO `grader_exam` (`id`, `id_grader`, `id_exam_name`, `id_exam_center`, `id_exam_event`, `exam_time`, `exam_date`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`, `start_date`, `end_date`, `id_grader_type`, `student_selection`) VALUES
(7, 4, 0, 1, 8, '', '', 1, 1, '2021-02-28 17:23:17', NULL, '2021-02-28 17:23:17', '2021-02-28', '2021-02-28', NULL, 'on'),
(8, 4, 0, 1, 9, '', '', 1, 1, '2021-03-16 00:16:26', NULL, '2021-03-16 00:16:26', '2021-03-15', '2021-03-18', NULL, 'on'),
(10, 5, NULL, NULL, 9, '', '', 1, 1, '2021-03-18 03:35:33', NULL, '2021-03-18 03:35:33', '2021-03-15', '2021-03-19', NULL, '1');

-- --------------------------------------------------------

--
-- Table structure for table `grader_last_login`
--

CREATE TABLE `grader_last_login` (
  `id` bigint(20) NOT NULL,
  `id_grader` bigint(20) NOT NULL,
  `session_data` varchar(2048) NOT NULL,
  `machine_ip` varchar(1024) NOT NULL,
  `user_agent` varchar(128) NOT NULL,
  `agent_string` varchar(1024) NOT NULL,
  `platform` varchar(128) NOT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `grader_last_login`
--

INSERT INTO `grader_last_login` (`id`, `id_grader`, `session_data`, `machine_ip`, `user_agent`, `agent_string`, `platform`, `created_dt_tm`) VALUES
(8, 4, '{\"id_grader\":\"4\",\"grader_name\":\"Mr. grader  one\",\"grader_email\":\"grader1@gmail.com\",\"grader_nric\":\"1\",\"grader_mobile\":\"2\",\"isGraderLoggedIn\":true}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2021-03-01 11:51:33'),
(9, 4, '{\"id_grader\":\"4\",\"grader_name\":\"Mr. grader  one\",\"grader_email\":\"grader1@gmail.com\",\"grader_nric\":\"1\",\"grader_mobile\":\"2\",\"isGraderLoggedIn\":true}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2021-03-01 15:58:16'),
(10, 4, '{\"id_grader\":\"4\",\"grader_name\":\"Mr. grader  one\",\"grader_email\":\"grader1@gmail.com\",\"grader_nric\":\"1\",\"grader_mobile\":\"2\",\"isGraderLoggedIn\":true}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2021-03-01 22:07:00'),
(11, 4, '{\"id_grader\":\"4\",\"grader_name\":\"Mr. grader  one\",\"grader_email\":\"grader1@gmail.com\",\"grader_nric\":\"1\",\"grader_mobile\":\"2\",\"isGraderLoggedIn\":true}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2021-03-16 00:18:17'),
(12, 4, '{\"id_grader\":\"4\",\"grader_name\":\"Mr. grader  one\",\"grader_email\":\"grader1@gmail.com\",\"grader_nric\":\"1\",\"grader_mobile\":\"2\",\"isGraderLoggedIn\":true}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2021-03-17 12:51:27'),
(13, 4, '{\"id_grader\":\"4\",\"grader_name\":\"Mr. grader  one\",\"grader_email\":\"grader1@gmail.com\",\"grader_nric\":\"1\",\"grader_mobile\":\"2\",\"isGraderLoggedIn\":true}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2021-03-17 23:15:31'),
(14, 5, '{\"id_grader\":\"5\",\"grader_name\":\"Mr. grader 2 2\",\"grader_email\":\"grader2@gmail.com\",\"grader_nric\":\"grader2\",\"grader_mobile\":\"123123123123\",\"isGraderLoggedIn\":true}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2021-03-18 03:35:59'),
(15, 4, '{\"id_grader\":\"4\",\"grader_name\":\"Mr. grader  one\",\"grader_email\":\"grader1@gmail.com\",\"grader_nric\":\"1\",\"grader_mobile\":\"2\",\"isGraderLoggedIn\":true}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2021-03-17 20:13:23'),
(16, 4, '{\"id_grader\":\"4\",\"grader_name\":\"Mr. grader  one\",\"grader_email\":\"grader1@gmail.com\",\"grader_nric\":\"1\",\"grader_mobile\":\"2\",\"isGraderLoggedIn\":true}', '49.206.13.154', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '2021-03-17 23:07:40');

-- --------------------------------------------------------

--
-- Table structure for table `grader_modules`
--

CREATE TABLE `grader_modules` (
  `id` int(11) NOT NULL,
  `id_grader` int(11) DEFAULT 0,
  `id_course` int(11) DEFAULT 0,
  `status` int(11) DEFAULT 0,
  `created_by` int(11) DEFAULT NULL,
  `reason` varchar(2048) DEFAULT '',
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `grader_modules`
--

INSERT INTO `grader_modules` (`id`, `id_grader`, `id_course`, `status`, `created_by`, `reason`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(3, 4, 4, 1, NULL, '', '2021-02-28 17:22:06', NULL, '2021-02-28 17:22:06');

-- --------------------------------------------------------

--
-- Table structure for table `grader_student_tag`
--

CREATE TABLE `grader_student_tag` (
  `id` int(11) NOT NULL,
  `id_grader` int(11) DEFAULT 0,
  `id_grader_exam` int(11) DEFAULT 0,
  `id_student` int(11) DEFAULT 0,
  `id_exam` int(11) DEFAULT 0,
  `id_exam_center` int(11) DEFAULT 0,
  `id_exam_event` int(11) DEFAULT 0,
  `exam_time` varchar(200) DEFAULT '',
  `exam_date` varchar(200) DEFAULT '',
  `status` int(11) DEFAULT 0,
  `created_by` int(11) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp(),
  `id_exam_student_tagging` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `grader_student_tag`
--

INSERT INTO `grader_student_tag` (`id`, `id_grader`, `id_grader_exam`, `id_student`, `id_exam`, `id_exam_center`, `id_exam_event`, `exam_time`, `exam_date`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`, `id_exam_student_tagging`) VALUES
(13, 4, 8, 14, 0, 1, 9, '', '', 1, 1, '2021-03-16 00:16:38', NULL, '2021-03-16 00:16:38', 14),
(14, 5, NULL, 14, 0, 0, 9, '', '', 0, 1, '2021-03-18 03:34:56', NULL, '2021-03-18 03:34:56', NULL),
(15, 5, 10, 14, 0, 0, 9, '', '', 0, 1, '2021-03-18 03:35:33', NULL, '2021-03-18 03:35:33', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `main_invoice`
--

CREATE TABLE `main_invoice` (
  `id` int(11) NOT NULL,
  `type` varchar(50) DEFAULT '',
  `invoice_number` varchar(50) DEFAULT '',
  `date_time` datetime DEFAULT current_timestamp(),
  `remarks` varchar(520) DEFAULT '',
  `id_student` int(11) DEFAULT NULL,
  `currency` varchar(200) DEFAULT '',
  `total_amount` float(20,2) DEFAULT 0.00,
  `invoice_total` float(20,2) DEFAULT 0.00,
  `total_discount` float(20,2) DEFAULT 0.00,
  `balance_amount` float(20,2) DEFAULT 0.00,
  `paid_amount` float(20,2) DEFAULT 0.00,
  `reason` varchar(2048) DEFAULT '',
  `status` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `main_invoice_details`
--

CREATE TABLE `main_invoice_details` (
  `id` int(11) NOT NULL,
  `id_main_invoice` int(11) DEFAULT NULL,
  `id_fee_item` int(11) DEFAULT NULL,
  `price` int(11) DEFAULT 0,
  `quantity` int(11) DEFAULT 0,
  `amount` float(20,2) DEFAULT NULL,
  `id_reference` int(11) DEFAULT 0,
  `description` varchar(1024) DEFAULT '',
  `status` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `manualtos`
--

CREATE TABLE `manualtos` (
  `id` bigint(20) NOT NULL,
  `name` varchar(200) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `question_count` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `manualtos`
--

INSERT INTO `manualtos` (`id`, `name`, `created_by`, `status`, `created_dt_tm`, `updated_dt_tm`, `updated_by`, `question_count`) VALUES
(1, 'DEC 28th DEMO', 1, 1, NULL, NULL, NULL, NULL),
(2, 'Shariah Demo Exam', 1, 1, '2021-03-15 23:16:53', '2021-03-15 23:16:53', 1, 34);

-- --------------------------------------------------------

--
-- Table structure for table `manualtos_has_question`
--

CREATE TABLE `manualtos_has_question` (
  `id` bigint(20) NOT NULL,
  `id_manualtos` int(11) DEFAULT NULL,
  `id_question` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `manualtos_has_question`
--

INSERT INTO `manualtos_has_question` (`id`, `id_manualtos`, `id_question`) VALUES
(2, 1, 2),
(3, 2, 1),
(4, 2, 3),
(5, 2, 4),
(6, 2, 5),
(7, 2, 6),
(8, 2, 7),
(9, 2, 8),
(10, 2, 9),
(11, 2, 10),
(12, 2, 11),
(13, 2, 12),
(14, 2, 13),
(15, 2, 14),
(16, 2, 15),
(17, 2, 16),
(18, 2, 17),
(19, 2, 18),
(20, 2, 19),
(21, 2, 20),
(22, 2, 21),
(23, 2, 22),
(24, 2, 23),
(25, 2, 24),
(26, 2, 25),
(27, 2, 26),
(28, 2, 27),
(29, 2, 28),
(30, 2, 29),
(31, 2, 30),
(32, 2, 35),
(33, 2, 70),
(34, 2, 74),
(35, 2, 77);

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `menu_name` varchar(200) DEFAULT NULL,
  `module_name` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `menu_name`, `module_name`) VALUES
(1, 'User', 'Setup'),
(2, 'Course', NULL),
(3, 'Topic', 'Setup'),
(4, 'Bloom Taxonomy', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `payment_type`
--

CREATE TABLE `payment_type` (
  `id` int(11) NOT NULL,
  `name` varchar(520) DEFAULT '',
  `description` varchar(2048) DEFAULT '',
  `description_optional_language` varchar(2048) DEFAULT '',
  `payment_group` varchar(512) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `name_in_malay` varchar(520) DEFAULT '',
  `status` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment_type`
--

INSERT INTO `payment_type` (`id`, `name`, `description`, `description_optional_language`, `payment_group`, `code`, `name_in_malay`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updeated_dt_tm`) VALUES
(1, '', 'Cash Payment', '', 'Cash', 'Cash', '', 1, NULL, '2020-08-20 10:08:10', NULL, '2020-08-20 10:08:10'),
(2, '', 'Bank Checue Transfer', '', 'Checque', 'Bank Transfer', '', 1, NULL, '2020-08-20 10:09:30', NULL, '2020-08-20 10:09:30');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) NOT NULL,
  `code` varchar(100) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `status` tinyint(4) NOT NULL,
  `id_menu` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `code`, `description`, `status`, `id_menu`) VALUES
(5, 'student.studentexams', 'Student Exam page view', 1, NULL),
(6, 'role.list', 'List Role', 1, NULL),
(7, 'permission.add', 'Add Permission', 1, NULL),
(8, 'permission.list', 'View Permission', 1, NULL),
(9, 'permission.edit', 'Edit Permission', 1, NULL),
(10, 'user.edit', 'Edit User', 1, 1),
(11, 'user.add', 'Add User', 1, 1),
(12, 'user.list', 'View User', 1, 1),
(13, 'course.list', 'View Course', 1, 2),
(14, 'course.add', 'Add Course', 1, 2),
(15, 'course.edit', 'Edit Course', 1, 2),
(16, 'topic.edit', 'Edit Topic', 1, NULL),
(17, 'topic.add', 'Add Topic', 1, NULL),
(18, 'topic.list', 'View Topic', 1, NULL),
(19, 'learningobjective.list', 'View Learning Objective', 1, NULL),
(20, 'learningobjective.add', 'Add Learning Objective', 1, NULL),
(21, 'learningobjective.list', 'View Learning Objective', 1, NULL),
(22, 'taxonomy.list', 'View Blood Taxonomy', 1, NULL),
(23, 'taxonomy.add', 'Add Blood Taxonomy', 1, NULL),
(24, 'taxonomy.edit', 'Edit Blood Taxonomy', 1, NULL),
(25, 'difficultylevel.edit', 'Edit Difficulty Level', 1, NULL),
(26, 'difficultylevel.add', 'Add Difficulty Level', 1, NULL),
(27, 'difficultylevel.list', 'View Difficulty Level', 1, NULL),
(28, 'question.list', 'View Question', 1, NULL),
(29, 'question.add', 'Add Question', 1, NULL),
(30, 'question.edit', 'Edit Question', 1, NULL),
(31, 'questionhasoption.add', 'Add Question Options', 1, NULL),
(33, 'exam_name.list', 'List Exam Name', 1, NULL),
(34, 'exam_name.add', 'Add Exam Name', 1, NULL),
(35, 'exam_name.edit', 'Edit Exam Name', 1, NULL),
(36, 'exam_center.list', 'View Exam Center', 1, NULL),
(37, 'exam_center.add', 'Add Exam Center', 1, NULL),
(38, 'exam_center.edit', 'Edit Exam Center', 1, NULL),
(39, 'exam_event.edit', 'Edit Exam Event', 1, NULL),
(40, 'exam_event.list', 'View Exam Event', 1, NULL),
(41, 'exam_event.add', 'Add Exam Event', 1, NULL),
(42, 'exam_has_question.add', 'Add Exam Has Question', 1, NULL),
(43, 'exam_has_question.edit', 'Edit Exam Has Question', 1, NULL),
(44, 'role.add', 'Add Role', 1, NULL),
(45, 'exam_has_question.list', 'View Exam Has Question', 1, NULL),
(46, 'grade.list', 'View Grade', 1, NULL),
(47, 'grade.add', 'Add Grade', 1, NULL),
(48, 'grade.edit', 'Edit Grade', 1, NULL),
(49, 'location.edit', 'Edit Location', 1, NULL),
(50, 'location.add', 'Add Location', 1, NULL),
(51, 'location.list', 'View Location', 1, NULL),
(52, 'questionpool.list', 'List Question Pool', 1, NULL),
(53, 'questionpool.add', 'Add Question Pool', 1, NULL),
(54, 'questionpool.edit', 'Edit Question Pool', 1, NULL),
(55, 'tos.add', 'Add TOS', 1, NULL),
(56, 'tos.list', 'View TOS', 1, NULL),
(57, 'tos.edit', 'Edit TOS', 1, NULL),
(58, 'examset.add', 'Add Exam Set', 1, NULL),
(59, 'examset.list', 'View Exam Set', 1, NULL),
(60, 'examset.edit', 'Edit Exam Set', 1, NULL),
(61, 'student.list', 'List Student', 1, NULL),
(62, 'student.add', 'Add Student', 1, NULL),
(63, 'student.edit', 'Edit Student', 1, NULL),
(64, 'exam_student_tagging.add', 'Add Exam Student Tagging', 1, NULL),
(65, 'exam_student_tagging.list', 'View Exam Student Tagging', 1, NULL),
(66, 'exam_student_tagging.edit', 'Edit Exam Student Tagging', 1, NULL),
(67, 'role.edit', 'Role Edit', 1, NULL),
(68, 'student.examanswers', 'Student Exam answer', 1, NULL),
(69, 'grader.list', 'Exam Grader List', 1, NULL),
(70, 'grader.add', 'Exam Grader Add', 1, NULL),
(71, 'grader.edit', 'Exam Grader Edit', 1, NULL),
(72, 'grader_exam.list', 'Grader Exam List', 1, NULL),
(73, 'grader_exam.add', 'Grader Exam Add', 1, NULL),
(74, 'grader_student_tag.add', 'Grader Student Tag Add', 1, NULL),
(75, 'grader_student_tag.list', 'Grader Student Tag List', 1, NULL),
(76, 'tosmanual.list', 'Exam Grader List', 1, NULL),
(77, 'tosmanual.add', 'Exam Grader Add', 1, NULL),
(78, 'tosmanual.edit', 'Exam Grader Edit', 1, NULL),
(79, 'grader_marks_approval.list', 'Exam Grader List', 1, NULL),
(80, 'grader_marks_approval.add', 'Exam Grader Add', 1, NULL),
(81, 'grader_marks_approval.edit', 'Exam Grader Edit', 1, NULL),
(82, 'final_marks_approval.list', 'Exam Grader List', 1, NULL),
(83, 'final_marks_approval.add', 'Exam Grader Add', 1, NULL),
(84, 'final_marks_approval.edit', 'Exam Grader Edit', 1, NULL),
(85, 'secondgrader.list', 'secondgrader List', 1, NULL),
(86, 'secondgrader.add', 'secondgrader Add', 1, NULL),
(87, 'secondgrader.edit', 'secondgrader Edit', 1, NULL),
(88, 'marksadjustment.list', 'marksadjustment List', 1, NULL),
(89, 'marksadjustment.add', 'marksadjustment Add', 1, NULL),
(90, 'marksadjustmentapproval.list', 'marksadjustmentapproval list', 1, NULL),
(91, 'marksadjustmentapproval.add', 'marksadjustmentapproval add', 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `question`
--

CREATE TABLE `question` (
  `id` int(11) NOT NULL,
  `question` text NOT NULL,
  `id_pool` bigint(20) NOT NULL,
  `id_course` int(11) DEFAULT 0,
  `id_topic` int(11) DEFAULT 0,
  `id_course_learning_objective` int(11) DEFAULT 0,
  `id_bloom_taxonomy` int(11) DEFAULT 0,
  `id_difficult_level` int(11) DEFAULT 0,
  `image` varchar(1024) DEFAULT '',
  `status` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp(),
  `marks` int(11) DEFAULT NULL,
  `question_type` int(11) DEFAULT NULL,
  `parent_id` int(20) DEFAULT NULL,
  `answer_scheme` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `question`
--

INSERT INTO `question` (`id`, `question`, `id_pool`, `id_course`, `id_topic`, `id_course_learning_objective`, `id_bloom_taxonomy`, `id_difficult_level`, `image`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`, `marks`, `question_type`, `parent_id`, `answer_scheme`) VALUES
(1, '<p>Mr. Ismail failed to pay his financing installment and was charged a penalty of RM100 for late payment.</p>\r\n\r\n<p>What type of <em>riba </em>occurs in this scenario?</p>\r\n', 1, 3, 2, 0, 4, 2, '', 1, 1, '2020-12-22 09:42:34', 1, '2020-12-22 09:42:34', 2, 1, 0, NULL),
(3, '<p>Apart from the Quran and Sunnah, which of these are the sources of <em>Ahkam</em> (ruling) agreed upon by the overwhelming majority of past and present scholars?</p>\r\n\r\n<ol>\r\n <li>Qiyas</li>\r\n <li>Urf</li>\r\n <li>Ijma’.</li>\r\n <li>Istihsan</li>\r\n</ol>\r\n', 1, 3, 2, 0, 4, 1, '', 1, 1, '2020-12-22 09:50:24', 1, '2020-12-22 09:50:24', 2, 1, 0, NULL),
(4, '<p>What type of <em>riba </em>occurs when 100KG of Grade A dates is exchanged on the spot with 200KG of Grade B dates?</p>\r\n', 1, 3, 16, 0, 4, 2, '', 1, 1, '2020-12-24 08:50:44', NULL, '2020-12-24 08:50:44', 2, 1, 0, NULL),
(5, '<p><em>Fiqh</em> technically means:</p>\r\n', 1, 3, 14, 0, 4, 1, '', 1, 1, '2020-12-24 08:51:53', NULL, '2020-12-24 08:51:53', 2, 1, 0, NULL),
(6, '<p><em>Ijtihad</em> may be permitted in the these circumstances:</p>\r\n\r\n<ol>\r\n <li>Where the legal text has originated from a speculative source and there is a decisive indication on the ruling</li>\r\n <li>Where <em>ijtihad</em> is exercised on a matter, issue, or case for which there is no text or consensus by past scholars to be referred to</li>\r\n <li>Where the legal text has originated from an authentic source and there is a decisive indication on the ruling</li>\r\n <li>Where the legal text has originated from a speculative source and evidence is unclear on the ruling</li>\r\n</ol>\r\n', 1, 3, 15, 0, 4, 1, '', 1, 1, '2020-12-24 08:53:47', 1, '2020-12-24 08:53:47', 2, 1, 0, NULL),
(7, '<p>What is meant by the timeless feature of <em>Shariah</em>?</p>\r\n', 1, 3, 14, 0, 4, 2, '', 1, 1, '2020-12-24 08:53:47', 1, '2020-12-24 08:53:47', 2, 1, 0, NULL),
(8, '<p>According to the Hanafi school of thought, an act concerning mu’amalat could be classified as defective (<em>fasid</em>). What does this mean?</p>\r\n', 1, 3, 16, 0, 4, 2, '', 1, 1, '2020-12-24 08:54:35', 1, '2020-12-24 08:54:35', 2, 1, 0, NULL),
(9, '<p>Differences in <em>ijtihad</em> arise from various meanings of Arabic words. This is due to the fact that some Arabic words are either:</p>\r\n\r\n<ol>\r\n <li><em>Mujmal </em>(precise) or<em> Mushtarak </em>(common)</li>\r\n <li><em>‘Umum </em>(general) and<em> Khusus </em>(specific)</li>\r\n <li><em>Haqiqah </em>(true literal sense) and<em> Majaz </em>(metaphor)</li>\r\n <li><em>Mujmal</em> (precise)or<em> Muqayyad </em>(qualified)</li>\r\n</ol>\r\n', 1, 3, 15, 0, 4, 2, '', 1, 1, '2020-12-24 09:00:11', NULL, '2020-12-24 09:00:11', 2, 1, 0, NULL),
(10, '<p>From the jurists’ perspective, the words <em>khilaf</em> and <em>ikhtilaf </em>denote:</p>\r\n', 1, 3, 15, 0, 4, 1, '', 1, 1, '2020-12-24 09:01:12', NULL, '2020-12-24 09:01:12', 2, 1, 0, NULL),
(11, '<p>“The deposited money in <em>wadi’ah</em> account turns into a loan when utilised by the banks”</p>\r\n\r\n<p>The above scenario is permissible based on the fiqh maxim:</p>\r\n', 1, 3, 16, 0, 4, 2, '', 1, 1, '2020-12-24 09:02:34', NULL, '2020-12-24 09:02:34', 2, 1, 0, NULL),
(12, '<p>“The bank will purchase the asset from the customer at a purchase price which will be determined later based on the terms and conditions of the agreement.”</p>\r\n\r\n<p>Identify the prohibited element in the above contract clause.</p>\r\n', 1, 3, 13, 0, 4, 2, '', 1, 1, '2020-12-24 09:03:31', NULL, '2020-12-24 09:03:31', 2, 1, 0, NULL),
(13, '<p>In which of these contracts does the “Milkiyyah al-raqabah” remain with the owner of the asset, while “Milkiyyah al-manfa’ah” is transferred?</p>\r\n', 1, 3, 13, 0, 4, 2, '', 1, 1, '2020-12-24 09:04:34', NULL, '2020-12-24 09:04:34', 2, 1, 0, NULL),
(14, '<p>What is an example of <em>hukm wad’i – ‘azimah</em>?</p>\r\n', 1, 3, 14, 0, 4, 2, '', 1, 1, '2020-12-24 09:05:35', NULL, '2020-12-24 09:05:35', 2, 1, 0, NULL),
(15, '<p>“Stamp duty and other direct and related expenses are borne by the purchaser in accordance with predominant practices although it is not stipulated in the agreement.”</p>\r\n\r\n<p>Identify the fiqh maxim applied in this statement.</p>\r\n', 1, 3, 16, 0, 4, 2, '', 1, 1, '2020-12-24 09:07:05', NULL, '2020-12-24 09:07:05', 2, 1, 0, NULL),
(16, '<p>“Unqualified people could be prohibited from practicing their professions in order to safeguard the interests of the general public.”</p>\r\n\r\n<p>Identify the <em>fiqh</em> maxim applied in this statement.</p>\r\n', 1, 3, 16, 0, 4, 2, '', 1, 1, '2020-12-24 09:08:09', NULL, '2020-12-24 09:08:09', 2, 1, 0, NULL),
(17, '<p>Which of these are essential pillars of a contract?</p>\r\n\r\n<ol>\r\n <li>Contracting/transacting parties</li>\r\n <li>Warranty</li>\r\n <li>Statement</li>\r\n <li>Subject of contract</li>\r\n</ol>\r\n', 1, 3, 14, 0, 4, 1, '', 1, 1, '2020-12-25 21:08:56', NULL, '2020-12-25 21:08:56', 2, 1, 0, NULL),
(18, '<p>Identify the type of defect in this contract clause.</p>\r\n\r\n<p>“The Bank will purchase the asset from the customer at a purchase price which will be determined at a later date after the execution of the contract, based on the terms and conditions of the agreement”</p>\r\n', 1, 3, 14, 0, 4, 2, '', 1, 1, '2020-12-25 21:09:57', NULL, '2020-12-25 21:09:57', 2, 1, 0, NULL),
(19, '<p><em>Ijma’ Al-sukuti</em> means <em>Ijma’</em> where:</p>\r\n', 1, 3, 14, 0, 4, 1, '', 1, 1, '2020-12-25 21:11:01', NULL, '2020-12-25 21:11:01', 2, 1, 0, NULL),
(20, '<p>What is the approach of <em>Shariah</em> to commercial transactions?</p>\r\n', 1, 3, 14, 0, 4, 2, '', 1, 1, '2020-12-25 21:11:46', NULL, '2020-12-25 21:11:46', 2, 1, 0, NULL),
(21, '<p>A <em>Takaful </em>company is allowed to distribute its risk via outward re-<em>takaful </em>to a conventional insurance and re-insurance company. Which principle is this ruling based on?</p>\r\n', 1, 3, 14, 0, 4, 2, '', 1, 1, '2020-12-25 21:12:38', NULL, '2020-12-25 21:12:38', 2, 1, 0, NULL),
(22, '<p>What is the logic behind the prohibition of alcohol consumption and other intoxicating substances?</p>\r\n', 1, 3, 14, 0, 4, 2, '', 1, 1, '2020-12-25 21:13:26', NULL, '2020-12-25 21:13:26', 2, 1, 0, NULL),
(23, '<p>The settlement in foreign exchange transaction, the date of settlement is two (2) business days after transaction. This is considered <em>Shariah</em>-compliant due to the following <em>fiqh </em>maxim.</p>\r\n\r\n<p>The ruling was made based on:</p>\r\n', 1, 3, 16, 0, 4, 2, '', 1, 1, '2020-12-25 21:14:18', NULL, '2020-12-25 21:14:18', 2, 1, 0, NULL),
(24, '<p>Which of these is <strong>TRUE </strong>about <em>Shariah</em>-compliant currency exchange?</p>\r\n', 1, 3, 13, 0, 4, 2, '', 1, 1, '2020-12-25 21:15:19', NULL, '2020-12-25 21:15:19', 2, 1, 0, NULL),
(25, '<p>Which of theseis the basis for the permissibility of <em>Salam</em> (Islamic forward sale) or <em>Ijarah</em> (lease) contract?</p>\r\n', 1, 3, 14, 0, 4, 2, '', 1, 1, '2020-12-25 21:16:11', NULL, '2020-12-25 21:16:11', 2, 1, 0, NULL),
(26, '<p>What is the correct sequence of the steps in identifying truth or rulings according to the Islamic logic?</p>\r\n\r\n<ol>\r\n <li><em>Istidlal</em></li>\r\n <li><em>Tasawwur</em></li>\r\n <li><em>Tasdiq</em></li>\r\n</ol>\r\n', 1, 3, 17, 0, 4, 1, '', 1, 1, '2020-12-25 21:17:55', NULL, '2020-12-25 21:17:55', 2, 1, 0, NULL),
(27, '<p>The burden of proof is on the person who accuses.</p>\r\n\r\n<p>This is an example of the application of:</p>\r\n', 1, 3, 16, 0, 4, 2, '', 1, 1, '2020-12-25 21:18:41', NULL, '2020-12-25 21:18:41', 2, 1, 0, NULL),
(28, '<p>Which of these is <strong>TRUE </strong>about the application of logical reasoning on contemporary fatwas and <em>ijtih?d </em>in Islamic finance?</p>\r\n', 1, 3, 15, 0, 4, 2, '', 1, 1, '2020-12-25 21:19:34', NULL, '2020-12-25 21:19:34', 2, 1, 0, NULL),
(29, '<p>Which of the following refers to an optional <em>Hukm at-Taklifi</em>?</p>\r\n', 1, 3, 14, 0, 4, 1, '', 1, 1, '2020-12-25 21:20:18', NULL, '2020-12-25 21:20:18', 2, 1, 0, NULL),
(30, '<p>What is the difference between Ibnu Rushd and Al-Ghazali in applying logic in Islamic philosophy?</p>\r\n', 1, 3, 17, 0, 4, 1, '', 1, 1, '2020-12-25 21:22:10', NULL, '2020-12-25 21:22:10', 2, 1, 0, NULL),
(35, '<p>“Safeguarding people’s interests in this world and in the Hereafter”</p>\r\n\r\n<p>This statement is part of the:</p>\r\n', 1, 3, 14, 0, 4, 2, '', 1, 1, '2020-12-25 21:32:21', NULL, '2020-12-25 21:32:21', 2, 1, 0, NULL),
(70, '<p>Overview of Question 1</p>\r\n', 1, 3, 14, 0, 4, 1, '', 1, 1, '2021-03-15 10:50:47', NULL, '2021-03-15 10:50:47', 10, 2, 0, NULL),
(71, '<p>sdffssdsfssd</p>\n', 0, 3, 14, 0, 4, 1, '', 1, 1, '2021-03-15 10:50:47', NULL, '2021-03-15 10:50:47', 10, 2, 70, '<ol>\n	<li>\n	<p>Differences in linguistic interpretation</p>\n\n	<ol>\n		<li>\n		<p>Mujmal or mushtarak</p>\n		</li>\n		<li>\n		<p>Umum or khusus</p>\n		</li>\n		<li>\n		<p>Haqiqah or majaz</p>\n		</li>\n		<li>\n		<p>Haqiqah or urf</p>\n		</li>\n	</ol>\n	</li>\n	<li>\n	<p>Differences in the authority of hadith</p>\n\n	<ol>\n		<li>\n		<p>Knowledge</p>\n		</li>\n		<li>\n		<p>Authenticity</p>\n		</li>\n	</ol>\n	</li>\n	<li>\n	<p>Differences relating to secondary sources</p>\n	</li>\n	<li>\n	<p>Differences to logical reasoning</p>\n	</li>\n</ol>\n'),
(72, '<p><strong>Identify and explain two (2) differences between </strong><em><strong>Ifta&rsquo; </strong></em><strong>and </strong><em><strong>Qada&rsquo; </strong></em></p>\n', 0, 3, 14, 0, 4, 1, '', 1, 1, '2021-03-15 10:50:47', NULL, '2021-03-15 10:50:47', 5, 2, 70, '	<p>Differences in linguistic interpretation</p>'),
(74, '<p>Overview of Question 3</p>\r\n', 1, 3, 14, 0, 4, 1, '', 1, 1, '2021-03-15 10:53:53', NULL, '2021-03-15 10:53:53', 10, 2, 0, NULL),
(75, '<p><strong>Briefly explain the three types of Shariah rules that govern different aspects of human life. </strong></p>\n', 0, 3, 14, 0, 4, 1, '', 1, 1, '2021-03-15 10:53:53', NULL, '2021-03-15 10:53:53', 6, 2, 74, '<p>a) <em>al-Ahk?m al-I&rsquo;tiqadiyyah </em>&ndash; Rulings relating to beliefs such as the belief in Allah and the Day of Judgment.</p>\n\n<p>b) <em>al-Ahk?m al-Akhlaqiyyah </em>&ndash; Rulings relating to morality and ethics such as the injunction to tell the truth, sincerity, honesty, etc.</p>\n\n<p>c) <em>al-Ahk?m al-Amaliyyah </em>&ndash; Rulings relating to the conduct of individuals which include the conduct between man and God <em>(al-Ahk?m al-Ibadiyyah) </em>and his relations with others <em>(al-Ahk?m al-Mu&rsquo;amalat) </em>such as those relating to marriage, transactions, finance and governance.</p>\n\n<p><br />\n&nbsp;</p>\n'),
(76, '<p><strong>Shariah rulings can either be detailed, or only govern the principles of a certain aspect. </strong></p>\n\n<p><strong>Provide examples of both, that would serve to differentiate between the two. </strong></p>\n', 0, 3, 14, 0, 4, 1, '', 1, 1, '2021-03-15 10:53:53', NULL, '2021-03-15 10:53:53', 4, 2, 74, '<p>Examples of detailed rulings are the rules under the concept of <em>al-Ahk?m al-I&rsquo;tiqadiyyah, al-Ahk?m al-Ibadiyah, </em>and some in the areas of <em>al-Ahk?m al-Amaliyyah </em>and <em>al-Ahk?m al-Akhlaqiyyah </em>such as inheritance, marriage, divorce, prohibition of <em>riba</em>, and the punishment of certain criminal offences. These are mandatory rules in which no changes are allowed.</p>\n\n<p><strong>Example: </strong>The ruling on the five daily prayers and how they are conducted are described in detail in the Qur&rsquo;an and the <em>Sunnah </em>of the Prophet, and as such any innovation in the conduct of prayer is prohibited.</p>\n\n<p>Meanwhile, most of the conduct under the components of <em>al-Ahk?m al-Amaliyy</em>ah and <em>al-Ahk?m al-Akhlaqiyyah </em>are not specifically detailed by <em>Shariah</em>. However, <em>Shariah </em>provides the general principles that need to be followed. Under these concepts, <em>Shariah </em>gives man room to innovate and change the standards or practices based on the general rulings stated by <em>Shariah</em>, as stated in the Qur&rsquo;an and <em>Sunnah </em>of the Prophet.</p>\n\n<p><strong>Example: </strong>The Qur&rsquo;an establishes rules of Consultation <em>(Shura) </em>in matters of public concern <em>(?l &lsquo;Imr?n: verse 159). </em>However, the method of implementing such principles has not been provided in detail. Therefore, humans are allowed to create different standards or practices of consultation in each country according to their custom.</p>\n\n<p><br />\n&nbsp;</p>\n'),
(77, '<p>Overview of Question 4</p>\r\n', 1, 3, 14, 0, 4, 2, '', 1, 1, '2021-03-15 10:55:37', NULL, '2021-03-15 10:55:37', 10, 2, 0, NULL),
(78, '<p>&ldquo;<strong>O you who believe! Eat not up your property&mdash;among yourselves in vanities: but let there be amongst you traffic and trade by mutual good-will:&hellip;&rdquo; (Al-Nis?&rsquo;: 29)</strong></p>\n\n<p><strong>Referring to the verse above, the following are the principles that can be derived.</strong></p>\n\n<ol>\n	<li>\n	<p><strong>Justice</strong></p>\n	</li>\n	<li>\n	<p><strong>Consent</strong></p>\n	</li>\n	<li>\n	<p><strong>Discuss what you understand by these two principles </strong></p>\n	</li>\n</ol>\n', 0, 3, 14, 0, 4, 1, '', 1, 1, '2021-03-15 10:55:37', NULL, '2021-03-15 10:55:37', 6, 2, 77, '<p>Consent &ndash; This? rule? from? the? Quran? state? the? requirement? for? taking? other&rsquo;s? property? either? by? trade or gift should be done by the consent of the parties. Therefore, taking property such as by theft or illegal consumption is prohibited in Islam. [Student nee to provide example of situation where this rule is not abide with. Example may be breach of trust in safekeeping of investment money]</p>\n\n<p>Justice? - means? giving? everyone? their? due? rights.? Hence,? injustice? means? not? giving? one&rsquo;s? right.? In? a contract, justice means to fulfil the responsibilities and to give or uphold the right of other parties in the contract as agreed. [Student need to provide example of situation where this rule is not abide with. Example is withheld the payment of debt although one is able to repay his or her debt]</p>\n'),
(79, '<ol>\n	<li>\n	<p><strong>What are some of the salient features of the Shariah? Describe them briefly. </strong></p>\n	</li>\n</ol>\n', 0, 3, 14, 0, 4, 1, '', 1, 1, '2021-03-15 10:55:37', NULL, '2021-03-15 10:55:37', 4, 2, 77, '<p><strong>Universality of </strong><em>Shariah</em></p>\n\n<p><em>Shariah </em>is a principle that can be applied to the whole of mankind regardless of nationality, race or tribe. Therefore, all the <em>Shariah </em>rulings in the Qur&rsquo;an and <em>Sunnah </em>are universally applicable unless specifically mentioned otherwise.</p>\n\n<p><strong>Comprehensiveness of </strong><em>Shariah</em></p>\n\n<p>The <em>Shariah </em>goes beyond religious aspects and encompasses all stages of life and all human conduct including public and personal affairs.</p>\n\n<p><em>Shariah </em><strong>Aims to Ease Human Life</strong></p>\n\n<p>The <em>Shariah </em>is about making life easy for all mankind through its divine rules which prioritises human needs and comforts.</p>\n');

-- --------------------------------------------------------

--
-- Table structure for table `question_has_option`
--

CREATE TABLE `question_has_option` (
  `id` int(11) NOT NULL,
  `id_question` int(11) DEFAULT 0,
  `option_description` varchar(1024) DEFAULT '',
  `is_correct_answer` int(11) DEFAULT 0,
  `image` varchar(1024) DEFAULT '',
  `status` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `question_has_option`
--

INSERT INTO `question_has_option` (`id`, `id_question`, `option_description`, `is_correct_answer`, `image`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(118, 1, '<p><em>Riba al-Fadl</em></p>\r\n', 0, '', NULL, NULL, '2020-12-22 09:42:34', NULL, '2020-12-22 09:42:34'),
(119, 1, '<p><em>Riba al-Nasiah</em></p>\r\n', 1, '', NULL, NULL, '2020-12-22 09:42:34', NULL, '2020-12-22 09:42:34'),
(120, 1, '<p><em>Riba al-Buyu’</em></p>\r\n', 0, '', NULL, NULL, '2020-12-22 09:42:34', NULL, '2020-12-22 09:42:34'),
(121, 1, '<p><em>Riba al-Qard </em></p>\r\n', 0, '', NULL, NULL, '2020-12-22 09:42:34', NULL, '2020-12-22 09:42:34'),
(126, 3, '<p>2 and 4</p>\r\n', 0, '', NULL, NULL, '2020-12-22 09:50:24', NULL, '2020-12-22 09:50:24'),
(127, 3, '<p>2 and 3</p>\r\n', 0, '', NULL, NULL, '2020-12-22 09:50:24', NULL, '2020-12-22 09:50:24'),
(128, 3, '<p>1 and 3</p>\r\n', 1, '', NULL, NULL, '2020-12-22 09:50:24', NULL, '2020-12-22 09:50:24'),
(129, 3, '<p>1 and 4</p>\r\n', 0, '', NULL, NULL, '2020-12-22 09:50:24', NULL, '2020-12-22 09:50:24'),
(130, 4, '<p><em>Riba al-Qard </em></p>\r\n', 0, '', NULL, NULL, '2020-12-24 08:50:44', NULL, '2020-12-24 08:50:44'),
(131, 4, '<p><em>Riba al-Fadl</em></p>\r\n', 1, '', NULL, NULL, '2020-12-24 08:50:44', NULL, '2020-12-24 08:50:44'),
(132, 4, '<p><em>Riba al-Buyu’</em></p>\r\n', 0, '', NULL, NULL, '2020-12-24 08:50:44', NULL, '2020-12-24 08:50:44'),
(133, 4, '<p><em>Riba al-Nasiah</em></p>\r\n', 0, '', NULL, NULL, '2020-12-24 08:50:44', NULL, '2020-12-24 08:50:44'),
(134, 5, '<p>Knowledge of Islamic beliefs that have been derived from specific sources.</p>\r\n', 0, '', NULL, NULL, '2020-12-24 08:51:53', NULL, '2020-12-24 08:51:53'),
(135, 5, '<p>Knowledge of the <em>Shariah</em> pertaining to ethics that have been derived from specific evidence.</p>\r\n', 0, '', NULL, NULL, '2020-12-24 08:51:53', NULL, '2020-12-24 08:51:53'),
(136, 5, '<p>Knowledge of the <em>Shariah</em> pertaining to conduct that have been derived from specific evidence.</p>\r\n', 1, '', NULL, NULL, '2020-12-24 08:51:53', NULL, '2020-12-24 08:51:53'),
(137, 5, '<p>Knowledge of the <em>Shariah</em> pertaining to conduct that have been derived from the Qur’an and Sunnah only.</p>\r\n', 0, '', NULL, NULL, '2020-12-24 08:51:53', NULL, '2020-12-24 08:51:53'),
(138, 6, '<p>1 and 3 only</p>\r\n', 0, '', NULL, NULL, '2020-12-24 08:53:47', NULL, '2020-12-24 08:53:47'),
(139, 6, '<p>1 and 4 only</p>\r\n', 0, '', NULL, NULL, '2020-12-24 08:53:47', NULL, '2020-12-24 08:53:47'),
(140, 6, '<p>2 and 3 only</p>\r\n', 1, '', NULL, NULL, '2020-12-24 08:53:47', NULL, '2020-12-24 08:53:47'),
(141, 6, '<p>2 and 4 only</p>\r\n', 0, '', NULL, NULL, '2020-12-24 08:53:47', NULL, '2020-12-24 08:53:47'),
(142, 7, '<p>Making things easy</p>\r\n', 0, '', NULL, NULL, '2020-12-24 08:53:47', NULL, '2020-12-24 08:53:47'),
(143, 7, '<p>Having values not limited to a specific period</p>\r\n', 1, '', NULL, NULL, '2020-12-24 08:53:47', NULL, '2020-12-24 08:53:47'),
(144, 7, '<p>Removing burdens</p>\r\n', 0, '', NULL, NULL, '2020-12-24 08:53:47', NULL, '2020-12-24 08:53:47'),
(145, 7, '<p>Having unchangeable main principles</p>\r\n', 0, '', NULL, NULL, '2020-12-24 08:53:47', NULL, '2020-12-24 08:53:47'),
(146, 8, '<p>that the action is void and does not produce any legal consequences.</p>\r\n', 0, '', NULL, NULL, '2020-12-24 08:54:35', NULL, '2020-12-24 08:54:35'),
(147, 8, '<p>that there is a defect in the contract that does not affect the pillars and is rectifiable.</p>\r\n', 1, '', NULL, NULL, '2020-12-24 08:54:35', NULL, '2020-12-24 08:54:35'),
(148, 8, '<p>that none of the pillars or conditions of the contract have been fulfilled.</p>\r\n', 0, '', NULL, NULL, '2020-12-24 08:54:35', NULL, '2020-12-24 08:54:35'),
(149, 8, '<p>that the act has been performed properly, but all its essential requirements and conditions have not yet materialised.</p>\r\n', 0, '', NULL, NULL, '2020-12-24 08:54:35', NULL, '2020-12-24 08:54:35'),
(150, 9, '<p>i, ii and iv</p>\r\n', 0, '', NULL, NULL, '2020-12-24 09:00:11', NULL, '2020-12-24 09:00:11'),
(151, 9, '<p>i, ii and iii</p>\r\n', 1, '', NULL, NULL, '2020-12-24 09:00:11', NULL, '2020-12-24 09:00:11'),
(152, 9, '<p>ii, iii and iv</p>\r\n', 0, '', NULL, NULL, '2020-12-24 09:00:11', NULL, '2020-12-24 09:00:11'),
(153, 9, '<p>i, iii and iv</p>\r\n', 0, '', NULL, NULL, '2020-12-24 09:00:11', NULL, '2020-12-24 09:00:11'),
(154, 10, '<p>disagreement in fundamental issues</p>\r\n', 0, '', NULL, NULL, '2020-12-24 09:01:12', NULL, '2020-12-24 09:01:12'),
(155, 10, '<p>divergence of opinion insecondary issues</p>\r\n', 1, '', NULL, NULL, '2020-12-24 09:01:12', NULL, '2020-12-24 09:01:12'),
(156, 10, '<p>the consensus of only the Companions on a particular issue</p>\r\n', 0, '', NULL, NULL, '2020-12-24 09:01:12', NULL, '2020-12-24 09:01:12'),
(157, 10, '<p>the commentary on a verse by using hadith</p>\r\n', 0, '', NULL, NULL, '2020-12-24 09:01:12', NULL, '2020-12-24 09:01:12'),
(158, 11, '<p>Custom is an arbiter.</p>\r\n', 0, '', NULL, NULL, '2020-12-24 09:02:34', NULL, '2020-12-24 09:02:34'),
(159, 11, '<p>Harm is to be eliminated.</p>\r\n', 0, '', NULL, NULL, '2020-12-24 09:02:34', NULL, '2020-12-24 09:02:34'),
(160, 11, '<p>Certainty cannot be overruled by doubt.</p>\r\n', 0, '', NULL, NULL, '2020-12-24 09:02:34', NULL, '2020-12-24 09:02:34'),
(161, 11, '<p>Matters are determined by intention.</p>\r\n', 1, '', NULL, NULL, '2020-12-24 09:02:34', NULL, '2020-12-24 09:02:34'),
(162, 12, '<p><em>Riba</em></p>\r\n', 0, '', NULL, NULL, '2020-12-24 09:03:31', NULL, '2020-12-24 09:03:31'),
(163, 12, '<p><em>Maysir</em></p>\r\n', 0, '', NULL, NULL, '2020-12-24 09:03:31', NULL, '2020-12-24 09:03:31'),
(164, 12, '<p><em>Gharar</em></p>\r\n', 1, '', NULL, NULL, '2020-12-24 09:03:31', NULL, '2020-12-24 09:03:31'),
(165, 12, '<p><em>Ghurm</em></p>\r\n', 0, '', NULL, NULL, '2020-12-24 09:03:31', NULL, '2020-12-24 09:03:31'),
(166, 13, '<p><em>Wadiah</em></p>\r\n', 0, '', NULL, NULL, '2020-12-24 09:04:34', NULL, '2020-12-24 09:04:34'),
(167, 13, '<p><em>Bai’</em></p>\r\n', 0, '', NULL, NULL, '2020-12-24 09:04:34', NULL, '2020-12-24 09:04:34'),
(168, 13, '<p><em>Hiba bi-shart il-‘Ewadh</em></p>\r\n', 0, '', NULL, NULL, '2020-12-24 09:04:34', NULL, '2020-12-24 09:04:34'),
(169, 13, '<p><em>Ijarah</em></p>\r\n', 1, '', NULL, NULL, '2020-12-24 09:04:34', NULL, '2020-12-24 09:04:34'),
(170, 14, '<p>Permissibility of eating haram food at the point of starvation</p>\r\n', 0, '', NULL, NULL, '2020-12-24 09:05:35', NULL, '2020-12-24 09:05:35'),
(171, 14, '<p>Prohibition of haram acts and food</p>\r\n', 1, '', NULL, NULL, '2020-12-24 09:05:35', NULL, '2020-12-24 09:05:35'),
(172, 14, '<p>Concessions granted to a person under compulsion</p>\r\n', 0, '', NULL, NULL, '2020-12-24 09:05:35', NULL, '2020-12-24 09:05:35'),
(173, 14, '<p>Permissibility of leaving the obligatory fast during Ramadan for travelers</p>\r\n', 0, '', NULL, NULL, '2020-12-24 09:05:35', NULL, '2020-12-24 09:05:35'),
(174, 15, '<p>Certainty cannot be overruled by doubt (<em>Al-Yaqin la Yuzal bi Al-Shak</em>)</p>\r\n', 0, '', NULL, NULL, '2020-12-24 09:07:05', NULL, '2020-12-24 09:07:05'),
(175, 15, '<p>Custom is an arbiter (<em>Al-‘Adah Mahakkamah</em>)</p>\r\n', 1, '', NULL, NULL, '2020-12-24 09:07:05', NULL, '2020-12-24 09:07:05'),
(176, 15, '<p>Hardship begets facility (<em>Al-Mashaqqah Tajlib Al Taysir</em>)</p>\r\n', 0, '', NULL, NULL, '2020-12-24 09:07:05', NULL, '2020-12-24 09:07:05'),
(177, 15, '<p>Harm is to be eliminated (<em>Al-Darar Yuzal</em>)</p>\r\n', 0, '', NULL, NULL, '2020-12-24 09:07:05', NULL, '2020-12-24 09:07:05'),
(178, 16, '<p>Custom is an arbiter (<em>Al-‘Adah Mahakkamah</em>).</p>\r\n', 0, '', NULL, NULL, '2020-12-24 09:08:09', NULL, '2020-12-24 09:08:09'),
(179, 16, '<p>Harm is to be eliminated (<em>Al-Darar Yuzal</em>)</p>\r\n', 1, '', NULL, NULL, '2020-12-24 09:08:09', NULL, '2020-12-24 09:08:09'),
(180, 16, '<p>Certainty cannot be overruled by doubt (<em>Al-Yaqin la Yuzal bi Al-Shak</em>)</p>\r\n', 0, '', NULL, NULL, '2020-12-24 09:08:09', NULL, '2020-12-24 09:08:09'),
(181, 16, '<p>Hardship begets facility (<em>Al-Mashaqqah Tajlib Al Taysir</em>)</p>\r\n', 0, '', NULL, NULL, '2020-12-24 09:08:09', NULL, '2020-12-24 09:08:09'),
(182, 17, '<p>i, ii and iii</p>\r\n', 0, '', NULL, NULL, '2020-12-25 21:08:56', NULL, '2020-12-25 21:08:56'),
(183, 17, '<p>i, ii and iv</p>\r\n', 0, '', NULL, NULL, '2020-12-25 21:08:56', NULL, '2020-12-25 21:08:56'),
(184, 17, '<p>ii, iii and iv</p>\r\n', 0, '', NULL, NULL, '2020-12-25 21:08:56', NULL, '2020-12-25 21:08:56'),
(185, 17, '<p>i, iii and iv</p>\r\n', 1, '', NULL, NULL, '2020-12-25 21:08:56', NULL, '2020-12-25 21:08:56'),
(186, 18, '<p><em>Gharar</em> in subject matter</p>\r\n', 0, '', NULL, NULL, '2020-12-25 21:09:57', NULL, '2020-12-25 21:09:57'),
(187, 18, '<p><em>Gharar</em> in purchase price</p>\r\n', 1, '', NULL, NULL, '2020-12-25 21:09:57', NULL, '2020-12-25 21:09:57'),
(188, 18, '<p><em>Ribaal-fadl </em>due to later settlement</p>\r\n', 0, '', NULL, NULL, '2020-12-25 21:09:57', NULL, '2020-12-25 21:09:57'),
(189, 18, '<p>Maysir in terms and conditions of the agreement</p>\r\n', 0, '', NULL, NULL, '2020-12-25 21:09:57', NULL, '2020-12-25 21:09:57'),
(190, 19, '<p>every <em>mujtahid</em> expresses his opinion verbally.</p>\r\n', 0, '', NULL, NULL, '2020-12-25 21:11:01', NULL, '2020-12-25 21:11:01'),
(191, 19, '<p>every <em>mujtahid</em> expresses his opinion by way of action</p>\r\n', 0, '', NULL, NULL, '2020-12-25 21:11:01', NULL, '2020-12-25 21:11:01'),
(192, 19, '<p>Some <em>mujtahids</em> of a certain periodexpress their opinion, whereas the rest remain silent.</p>\r\n', 1, '', NULL, NULL, '2020-12-25 21:11:01', NULL, '2020-12-25 21:11:01'),
(193, 19, '<p>A<em>mujtahid</em> gives an opinion, whereas others expressly disagree with it.</p>\r\n', 0, '', NULL, NULL, '2020-12-25 21:11:01', NULL, '2020-12-25 21:11:01'),
(194, 20, '<p>Permissibility as an exception to the general rule</p>\r\n', 0, '', NULL, NULL, '2020-12-25 21:11:46', NULL, '2020-12-25 21:11:46'),
(195, 20, '<p>Permissibility as a general principle</p>\r\n', 1, '', NULL, NULL, '2020-12-25 21:11:46', NULL, '2020-12-25 21:11:46'),
(196, 20, '<p>Prohibition unless proven to be permissible</p>\r\n', 0, '', NULL, NULL, '2020-12-25 21:11:46', NULL, '2020-12-25 21:11:46'),
(197, 20, '<p>Impermissibility as a general principle</p>\r\n', 0, '', NULL, NULL, '2020-12-25 21:11:46', NULL, '2020-12-25 21:11:46'),
(198, 21, '<p>Embellishments (<em>Tahsiniyyat</em>).</p>\r\n', 0, '', NULL, NULL, '2020-12-25 21:12:38', NULL, '2020-12-25 21:12:38'),
(199, 21, '<p>Necessities (<em>Hajiyyat</em>).</p>\r\n', 1, '', NULL, NULL, '2020-12-25 21:12:38', NULL, '2020-12-25 21:12:38'),
(200, 21, '<p>Juristic preferences (<em>Istihsan</em>)</p>\r\n', 0, '', NULL, NULL, '2020-12-25 21:12:38', NULL, '2020-12-25 21:12:38'),
(201, 21, '<p>Customary practice (<em>‘Urf</em>).</p>\r\n', 0, '', NULL, NULL, '2020-12-25 21:12:38', NULL, '2020-12-25 21:12:38'),
(202, 22, '<p>To protect one’s intellect from being corrupted and weakened</p>\r\n', 1, '', NULL, NULL, '2020-12-25 21:13:26', NULL, '2020-12-25 21:13:26'),
(203, 22, '<p>To protect one’s lineage and procreation</p>\r\n', 0, '', NULL, NULL, '2020-12-25 21:13:26', NULL, '2020-12-25 21:13:26'),
(204, 22, '<p>To protect one’s wealth from usurpation</p>\r\n', 0, '', NULL, NULL, '2020-12-25 21:13:26', NULL, '2020-12-25 21:13:26'),
(205, 22, '<p>To protect one’s health from deterioration</p>\r\n', 0, '', NULL, NULL, '2020-12-25 21:13:26', NULL, '2020-12-25 21:13:26'),
(206, 23, '<p>Harm is to be eliminated.</p>\r\n', 0, '', NULL, NULL, '2020-12-25 21:14:18', NULL, '2020-12-25 21:14:18'),
(207, 23, '<p>Custom is an arbiter.</p>\r\n', 1, '', NULL, NULL, '2020-12-25 21:14:18', NULL, '2020-12-25 21:14:18'),
(208, 23, '<p>Hardship begets facility.</p>\r\n', 0, '', NULL, NULL, '2020-12-25 21:14:18', NULL, '2020-12-25 21:14:18'),
(209, 23, '<p>Certainty cannot be overruled by doubt.</p>\r\n', 0, '', NULL, NULL, '2020-12-25 21:14:18', NULL, '2020-12-25 21:14:18'),
(210, 24, '<p>Exchange between same currencies with the different amount and on spot-basis.</p>\r\n', 0, '', NULL, NULL, '2020-12-25 21:15:19', NULL, '2020-12-25 21:15:19'),
(211, 24, '<p>Exchange between two different currencies, not necessarily of the same amount and not on spot-basis.</p>\r\n', 0, '', NULL, NULL, '2020-12-25 21:15:19', NULL, '2020-12-25 21:15:19'),
(212, 24, '<p>Exchange between two different currencies, not necessarily of the same amount but on spot-basis.</p>\r\n', 1, '', NULL, NULL, '2020-12-25 21:15:19', NULL, '2020-12-25 21:15:19'),
(213, 24, '<p>Exchange between two different currencies with the value of the second currency to be determined in the future.</p>\r\n', 0, '', NULL, NULL, '2020-12-25 21:15:19', NULL, '2020-12-25 21:15:19'),
(214, 25, '<p><em>Dharuriyyat</em></p>\r\n', 0, '', NULL, NULL, '2020-12-25 21:16:11', NULL, '2020-12-25 21:16:11'),
(215, 25, '<p>Necessities (<em>Hajiyyat</em>)</p>\r\n', 1, '', NULL, NULL, '2020-12-25 21:16:11', NULL, '2020-12-25 21:16:11'),
(216, 25, '<p>Embellishment (<em>Tahsiniyyat</em>)</p>\r\n', 0, '', NULL, NULL, '2020-12-25 21:16:11', NULL, '2020-12-25 21:16:11'),
(217, 25, '<p><em>Maqasid al-Shariah</em></p>\r\n', 0, '', NULL, NULL, '2020-12-25 21:16:11', NULL, '2020-12-25 21:16:11'),
(218, 26, '<p>ii, i and iii</p>\r\n', 0, '', NULL, NULL, '2020-12-25 21:17:55', NULL, '2020-12-25 21:17:55'),
(219, 26, '<p>i, ii and iii</p>\r\n', 0, '', NULL, NULL, '2020-12-25 21:17:55', NULL, '2020-12-25 21:17:55'),
(220, 26, '<p>ii, iii and i</p>\r\n', 1, '', NULL, NULL, '2020-12-25 21:17:55', NULL, '2020-12-25 21:17:55'),
(221, 26, '<p>iii, ii and i</p>\r\n', 0, '', NULL, NULL, '2020-12-25 21:17:55', NULL, '2020-12-25 21:17:55'),
(222, 27, '<p>Custom is an arbiter</p>\r\n', 0, '', NULL, NULL, '2020-12-25 21:18:41', NULL, '2020-12-25 21:18:41'),
(223, 27, '<p>Certainty cannot be overruled by doubt</p>\r\n', 1, '', NULL, NULL, '2020-12-25 21:18:41', NULL, '2020-12-25 21:18:41'),
(224, 27, '<p>Harm is to be eliminated</p>\r\n', 0, '', NULL, NULL, '2020-12-25 21:18:41', NULL, '2020-12-25 21:18:41'),
(225, 27, '<p>Hardship begets facility</p>\r\n', 0, '', NULL, NULL, '2020-12-25 21:18:41', NULL, '2020-12-25 21:18:41'),
(226, 28, '<p>Logical reasoning is used to reinforce the Western framework of logic against the Islamic framework of logic.</p>\r\n', 0, '', NULL, NULL, '2020-12-25 21:19:34', NULL, '2020-12-25 21:19:34'),
(227, 28, '<p>Application of <em>mantiq </em>in Islamic finance provides the avenue to discuss contemporary development such as blockchain and crypto assets.</p>\r\n', 1, '', NULL, NULL, '2020-12-25 21:19:34', NULL, '2020-12-25 21:19:34'),
(228, 28, '<p>The argumentation algorithms used should not possess an Islamic epistemology and <em>tasawwur </em>(apprehension).</p>\r\n', 0, '', NULL, NULL, '2020-12-25 21:19:34', NULL, '2020-12-25 21:19:34'),
(229, 28, '<p>Sources of <em>Shariah</em>are prohibited from being used to discuss and apply logical reasoning.</p>\r\n', 0, '', NULL, NULL, '2020-12-25 21:19:34', NULL, '2020-12-25 21:19:34'),
(230, 29, '<p><em>Wujub</em></p>\r\n', 0, '', NULL, NULL, '2020-12-25 21:20:18', NULL, '2020-12-25 21:20:18'),
(231, 29, '<p><em>Tahrim</em></p>\r\n', 0, '', NULL, NULL, '2020-12-25 21:20:18', NULL, '2020-12-25 21:20:18'),
(232, 29, '<p><em>Karahah</em></p>\r\n', 0, '', NULL, NULL, '2020-12-25 21:20:18', NULL, '2020-12-25 21:20:18'),
(233, 29, '<p><em>Mubah</em></p>\r\n', 1, '', NULL, NULL, '2020-12-25 21:20:18', NULL, '2020-12-25 21:20:18'),
(234, 30, '<p>Al-Ghazali believed that logic should be applied in Islamic philosophy;<br>\r\nIbnu Rushd totally rejected the teaching of logic.</p>\r\n', 0, '', NULL, NULL, '2020-12-25 21:22:10', NULL, '2020-12-25 21:22:10'),
(235, 30, '<p>Al-Ghazali denied the need to learn logic in Islam;Ibnu Rushd opined that logic is part of Islamic philosophy.</p>\r\n', 0, '', NULL, NULL, '2020-12-25 21:22:10', NULL, '2020-12-25 21:22:10'),
(236, 30, '<p>Al-Ghazali believed that logic is an important tool in extracting knowledge but not to be part of Islamic philosophy;Ibnu Rushd opined that logic is part of Islamic philosophy.</p>\r\n', 1, '', NULL, NULL, '2020-12-25 21:22:10', NULL, '2020-12-25 21:22:10'),
(237, 30, '<p>Al-Ghazali and Ibnu Rushd did not have any difference in applying logic in Islamic philosophy where both agree that logic should be taught in Islamic philosophy.</p>\r\n', 0, '', NULL, NULL, '2020-12-25 21:22:10', NULL, '2020-12-25 21:22:10'),
(238, 35, '<p>Highest level of objectives of <em>Shariah</em> (<em>al-maqsad al-a’zam</em>)</p>\r\n', 1, '', NULL, NULL, '2020-12-25 21:32:21', NULL, '2020-12-25 21:32:21'),
(239, 35, '<p>General purposes of the <em>Shariah</em> (<em>al-maqasid al-ammah</em>)</p>\r\n', 0, '', NULL, NULL, '2020-12-25 21:32:21', NULL, '2020-12-25 21:32:21'),
(240, 35, '<p>Specific purposes of <em>Shariah</em> (<em>al-maqasid al-khassah</em>)</p>\r\n', 0, '', NULL, NULL, '2020-12-25 21:32:21', NULL, '2020-12-25 21:32:21'),
(241, 35, '<p>Subspecific purposes of <em>Shariah</em> (<em>al-maqasid al-juz’iyyah</em>)</p>\r\n', 0, '', NULL, NULL, '2020-12-25 21:32:21', NULL, '2020-12-25 21:32:21');

-- --------------------------------------------------------

--
-- Table structure for table `question_pool`
--

CREATE TABLE `question_pool` (
  `id` bigint(20) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `code` varchar(50) DEFAULT NULL,
  `status` tinyint(4) NOT NULL,
  `created_by` bigint(20) NOT NULL,
  `updated_by` bigint(20) NOT NULL,
  `created_dt_tm` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_dt_tm` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `question_pool`
--

INSERT INTO `question_pool` (`id`, `name`, `code`, `status`, `created_by`, `updated_by`, `created_dt_tm`, `updated_dt_tm`) VALUES
(1, 'FOUNDATION LEVEL\r\nSHARIAH\r\n', 'FOUNDATION LEVEL\r\nSHARIAH\r\n', 1, 0, 0, '2020-08-14 18:22:16', '2020-08-14 18:22:16'),
(2, 'FOUNDATION LEVEL\r\nOVERVIEW OF FINANCIAL PRODUCTS\r\n', 'FOUNDATION LEVEL\r\nOVERVIEW OF FINANCIAL PRODUCTS\r\n', 1, 0, 0, '2020-08-14 18:22:25', '2020-08-14 18:22:25');

-- --------------------------------------------------------

--
-- Table structure for table `receipt`
--

CREATE TABLE `receipt` (
  `id` int(11) NOT NULL,
  `type` varchar(50) DEFAULT '',
  `receipt_number` varchar(50) DEFAULT '',
  `receipt_amount` float(20,2) DEFAULT NULL,
  `remarks` varchar(520) DEFAULT '',
  `id_student` int(11) DEFAULT NULL,
  `id_examination` int(11) DEFAULT 0,
  `receipt_date` datetime DEFAULT current_timestamp(),
  `approval_status` varchar(50) DEFAULT '',
  `status` int(11) DEFAULT NULL,
  `reason` varchar(2048) DEFAULT '',
  `created_by` int(11) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `receipt`
--

INSERT INTO `receipt` (`id`, `type`, `receipt_number`, `receipt_amount`, `remarks`, `id_student`, `id_examination`, `receipt_date`, `approval_status`, `status`, `reason`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Student', 'REC000001/2020', 123.00, '', 1, 0, '2020-10-01 02:28:24', '', 1, '', NULL, '2020-10-01 02:28:24', NULL, '2020-10-01 02:28:24');

-- --------------------------------------------------------

--
-- Table structure for table `receipt_details`
--

CREATE TABLE `receipt_details` (
  `id` int(11) NOT NULL,
  `id_receipt` int(11) DEFAULT NULL,
  `id_main_invoice` int(11) DEFAULT NULL,
  `invoice_amount` float DEFAULT NULL,
  `paid_amount` float DEFAULT NULL,
  `approval_status` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `receipt_details`
--

INSERT INTO `receipt_details` (`id`, `id_receipt`, `id_main_invoice`, `invoice_amount`, `paid_amount`, `approval_status`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 1, 1, 0, 123, NULL, 1, 1, '2020-10-01 02:28:24', NULL, '2020-10-01 02:28:24');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `role` varchar(250) DEFAULT '',
  `status` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `role`, `status`) VALUES
(1, 'Administrator', 1),
(13, 'Examination Chief', 1);

-- --------------------------------------------------------

--
-- Table structure for table `role_permissions`
--

CREATE TABLE `role_permissions` (
  `id` bigint(20) NOT NULL,
  `id_role` bigint(20) DEFAULT NULL,
  `id_permission` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `role_permissions`
--

INSERT INTO `role_permissions` (`id`, `id_role`, `id_permission`) VALUES
(84, 12, 5),
(85, 12, 7),
(459, 1, 5),
(460, 1, 6),
(461, 1, 7),
(462, 1, 8),
(463, 1, 9),
(464, 1, 10),
(465, 1, 11),
(466, 1, 12),
(467, 1, 13),
(468, 1, 14),
(469, 1, 15),
(470, 1, 16),
(471, 1, 17),
(472, 1, 18),
(473, 1, 19),
(474, 1, 20),
(475, 1, 21),
(476, 1, 22),
(477, 1, 23),
(478, 1, 24),
(479, 1, 25),
(480, 1, 26),
(481, 1, 27),
(482, 1, 28),
(483, 1, 29),
(484, 1, 30),
(485, 1, 31),
(486, 1, 33),
(487, 1, 34),
(488, 1, 35),
(489, 1, 36),
(490, 1, 37),
(491, 1, 38),
(492, 1, 39),
(493, 1, 40),
(494, 1, 41),
(495, 1, 42),
(496, 1, 43),
(497, 1, 44),
(498, 1, 45),
(499, 1, 46),
(500, 1, 47),
(501, 1, 48),
(502, 1, 49),
(503, 1, 50),
(504, 1, 51),
(505, 1, 52),
(506, 1, 53),
(507, 1, 54),
(508, 1, 55),
(509, 1, 56),
(510, 1, 57),
(511, 1, 58),
(512, 1, 59),
(513, 1, 60),
(514, 1, 61),
(515, 1, 62),
(516, 1, 63),
(517, 1, 64),
(518, 1, 65),
(519, 1, 66),
(520, 1, 67),
(532, 13, 14),
(533, 1, 69),
(534, 1, 70),
(535, 1, 71),
(536, 1, 72),
(537, 1, 73),
(538, 1, 74),
(539, 1, 75),
(540, 1, 76),
(541, 1, 77),
(542, 1, 78),
(543, 1, 79),
(545, 1, 80),
(546, 1, 81),
(547, 1, 82),
(548, 1, 83),
(549, 1, 84),
(550, 1, 85),
(551, 1, 86),
(552, 1, 87),
(553, 1, 88),
(554, 1, 89),
(555, 1, 90),
(556, 1, 91);

-- --------------------------------------------------------

--
-- Table structure for table `salutation_setup`
--

CREATE TABLE `salutation_setup` (
  `id` int(11) NOT NULL,
  `name` varchar(250) DEFAULT '',
  `sequence` int(11) DEFAULT 0,
  `status` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `salutation_setup`
--

INSERT INTO `salutation_setup` (`id`, `name`, `sequence`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Mr', 1, 1, NULL, '2020-07-30 21:10:18', NULL, '2020-07-30 21:10:18'),
(2, 'Ms', 2, 1, NULL, '2020-07-30 21:10:51', NULL, '2020-07-30 21:10:51'),
(3, 'Mrs', 3, 1, NULL, '2020-07-30 21:10:51', NULL, '2020-07-30 21:10:51');

-- --------------------------------------------------------

--
-- Table structure for table `state`
--

CREATE TABLE `state` (
  `id` int(11) NOT NULL,
  `name` varchar(120) DEFAULT '',
  `id_country` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updeated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `state`
--

INSERT INTO `state` (`id`, `name`, `id_country`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updeated_dt_tm`) VALUES
(1, 'Kuala Lumpur', 1, 1, NULL, NULL, NULL, '2020-02-10 23:10:24'),
(2, 'Kedah', 1, 1, NULL, NULL, NULL, '2020-02-10 23:22:35'),
(3, 'Selangoor', 1, 1, NULL, NULL, NULL, '2020-02-27 17:13:26'),
(5, 'Sidney', 5, 1, NULL, NULL, NULL, '2020-03-29 19:15:12');

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `id` int(11) NOT NULL,
  `full_name` varchar(580) DEFAULT '',
  `salutation` varchar(120) DEFAULT '',
  `first_name` varchar(120) DEFAULT '',
  `last_name` varchar(120) DEFAULT '',
  `nric` varchar(120) DEFAULT '',
  `phone` varchar(120) DEFAULT '',
  `email_id` varchar(120) DEFAULT '',
  `password` varchar(120) DEFAULT '',
  `gender` varchar(120) DEFAULT '',
  `permanent_address1` varchar(120) DEFAULT '',
  `permanent_address2` varchar(120) DEFAULT '',
  `permanent_country` int(11) DEFAULT NULL,
  `permanent_state` int(11) DEFAULT NULL,
  `permanent_city` varchar(120) DEFAULT '',
  `permanent_zipcode` varchar(120) DEFAULT '',
  `applicant_status` varchar(50) DEFAULT 'Draft',
  `status` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp(),
  `company_name` varchar(500) DEFAULT NULL,
  `designation` varchar(500) DEFAULT NULL,
  `experience` varchar(500) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `company_address` varchar(500) DEFAULT NULL,
  `company_address_two` varchar(500) DEFAULT NULL,
  `end_date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`id`, `full_name`, `salutation`, `first_name`, `last_name`, `nric`, `phone`, `email_id`, `password`, `gender`, `permanent_address1`, `permanent_address2`, `permanent_country`, `permanent_state`, `permanent_city`, `permanent_zipcode`, `applicant_status`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`, `company_name`, `designation`, `experience`, `start_date`, `company_address`, `company_address_two`, `end_date`) VALUES
(12, 'Mr. testing one', '1', 'testing', 'one', '1', '1', 'student1@gmail.com', '213ee683360d88249109c2f92789dbc3', '', '', '', 1, 1, 'city', '1', 'Draft', 1, 1, '2021-02-28 17:21:13', 1, '2021-02-28 17:21:13', 'compn', 'design', 'exper', '2020-01-01', 'address one', 'address two', '2020-10-10'),
(13, 'Mr. Student2 student 2', '1', 'Student2', 'student 2', 'student22', '222', 'askiran123@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '', '', '', 1, 2, 'Bangalore', '9878787', 'Draft', 1, 1, '2021-03-15 14:21:28', 1, '2021-03-15 14:21:28', '', '', '', '0000-00-00', '', '', '0000-00-00'),
(14, 'Mr. demo 1', '1', 'demo', '1', 'demo1', '123123123123', 'demo1@gmail.com', 'e368b9938746fa090d6afd3628355133', '', '', '', 1, 2, 'city', '12312', 'Draft', 1, 1, '2021-03-15 11:00:31', NULL, '2021-03-15 11:00:31', NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `student_exam_attempt`
--

CREATE TABLE `student_exam_attempt` (
  `id` int(11) NOT NULL,
  `id_student` int(11) DEFAULT NULL,
  `id_exam_student_tagging` int(11) DEFAULT NULL,
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `attempt_number` int(11) DEFAULT NULL,
  `extra_time` varchar(20) DEFAULT NULL,
  `exam_result` varchar(20) DEFAULT NULL,
  `exam_submitted` int(11) DEFAULT NULL COMMENT '0:Not submitted, 1:submitted',
  `submit_type` int(11) DEFAULT NULL COMMENT '1:manual submit,2:auto submit',
  `submitted_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_exam_attempt`
--

INSERT INTO `student_exam_attempt` (`id`, `id_student`, `id_exam_student_tagging`, `start_time`, `end_time`, `attempt_number`, `extra_time`, `exam_result`, `exam_submitted`, `submit_type`, `submitted_time`) VALUES
(2, 14, 14, '2021-03-16 00:13:23', NULL, 1, NULL, NULL, 1, 1, '2021-03-16 00:15:11'),
(3, 12, 12, '2021-03-17 13:23:40', NULL, 1, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `student_question_set`
--

CREATE TABLE `student_question_set` (
  `id` bigint(20) NOT NULL,
  `id_student` int(11) DEFAULT NULL,
  `id_question` int(11) DEFAULT NULL,
  `question_order` int(11) DEFAULT NULL,
  `datetime` datetime DEFAULT NULL,
  `id_exam_student_tagging` int(11) DEFAULT NULL,
  `id_answer` int(11) DEFAULT NULL,
  `id_student_exam_attempt` int(11) DEFAULT NULL,
  `answer_text` text DEFAULT NULL,
  `student_image` varchar(500) DEFAULT NULL,
  `marks` varchar(20) DEFAULT NULL,
  `question_number` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_question_set`
--

INSERT INTO `student_question_set` (`id`, `id_student`, `id_question`, `question_order`, `datetime`, `id_exam_student_tagging`, `id_answer`, `id_student_exam_attempt`, `answer_text`, `student_image`, `marks`, `question_number`) VALUES
(1, 14, 1, 0, '2021-03-16 00:14:55', 14, 119, 2, NULL, NULL, '2', '1'),
(2, 14, 3, 1, NULL, 14, 0, 2, NULL, NULL, NULL, '2'),
(3, 14, 4, 2, NULL, 14, 0, 2, NULL, NULL, NULL, '3'),
(4, 14, 5, 3, '2021-03-16 00:14:18', 14, 136, 2, NULL, NULL, '2', '4'),
(5, 14, 6, 4, '2021-03-16 00:13:47', 14, 138, 2, NULL, NULL, '0', '5'),
(6, 14, 7, 5, NULL, 14, 0, 2, NULL, NULL, NULL, '6'),
(7, 14, 8, 6, '2021-03-16 00:14:21', 14, 147, 2, NULL, NULL, '2', '7'),
(8, 14, 9, 7, NULL, 14, 0, 2, NULL, NULL, NULL, '8'),
(9, 14, 10, 8, NULL, 14, 0, 2, NULL, NULL, NULL, '9'),
(10, 14, 11, 9, '2021-03-16 00:13:44', 14, 159, 2, NULL, NULL, '0', '10'),
(11, 14, 12, 10, NULL, 14, 0, 2, NULL, NULL, NULL, '11'),
(12, 14, 13, 11, '2021-03-16 00:14:52', 14, 167, 2, NULL, NULL, '0', '12'),
(13, 14, 14, 12, '2021-03-16 00:14:23', 14, 171, 2, NULL, NULL, '2', '13'),
(14, 14, 15, 13, NULL, 14, 0, 2, NULL, NULL, NULL, '14'),
(15, 14, 16, 14, '2021-03-16 00:14:46', 14, 179, 2, NULL, NULL, '2', '15'),
(16, 14, 17, 15, NULL, 14, 0, 2, NULL, NULL, NULL, '16'),
(17, 14, 18, 16, '2021-03-16 00:14:49', 14, 187, 2, NULL, NULL, '2', '17'),
(18, 14, 19, 17, NULL, 14, 0, 2, NULL, NULL, NULL, '18'),
(19, 14, 20, 18, NULL, 14, 0, 2, NULL, NULL, NULL, '19'),
(20, 14, 21, 19, '2021-03-16 00:13:52', 14, 199, 2, NULL, NULL, '2', '20'),
(21, 14, 22, 20, '2021-03-16 00:14:26', 14, 203, 2, NULL, NULL, '0', '21'),
(22, 14, 23, 21, NULL, 14, 0, 2, NULL, NULL, NULL, '22'),
(23, 14, 24, 22, '2021-03-16 00:14:29', 14, 211, 2, NULL, NULL, '0', '23'),
(24, 14, 25, 23, NULL, 14, 0, 2, NULL, NULL, NULL, '24'),
(25, 14, 26, 24, '2021-03-16 00:14:33', 14, 219, 2, NULL, NULL, '0', '25'),
(26, 14, 27, 25, NULL, 14, 0, 2, NULL, NULL, NULL, '26'),
(27, 14, 28, 26, '2021-03-16 00:14:38', 14, 227, 2, NULL, NULL, '2', '27'),
(28, 14, 29, 27, NULL, 14, 0, 2, NULL, NULL, NULL, '28'),
(29, 14, 30, 28, NULL, 14, 0, 2, NULL, NULL, NULL, '29'),
(30, 14, 35, 29, '2021-03-16 00:14:42', 14, 239, 2, NULL, NULL, '0', '30'),
(31, 14, 71, 30, '2021-03-16 00:13:59', 14, 0, 2, '<p>31 a answer goes here</p>\n', NULL, NULL, '31 -A'),
(32, 14, 72, 31, '2021-03-16 00:14:05', 14, 0, 2, '<p><strong>Identify and explain two (2) differences between&nbsp;</strong><em><strong>Ifta&rsquo;&nbsp;</strong></em><strong>and&nbsp;</strong><em><strong>Qada&rsquo;</strong></em></p>\n\n<p><a href=\"javascript:void(\'Cut\')\" onclick=\"CKEDITOR.tools.callFunction(96,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Copy\')\" onclick=\"CKEDITOR.tools.callFunction(99,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Paste\')\" onclick=\"CKEDITOR.tools.callFunction(102,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Paste as plain text\')\" onclick=\"CKEDITOR.tools.callFunction(105,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Paste from Word\')\" onclick=\"CKEDITOR.tools.callFunction(108,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Undo\')\" onclick=\"CKEDITOR.tools.callFunction(111,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Redo\')\" onclick=\"CKEDITOR.tools.callFunction(114,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Spell Checker\')\" onclick=\"CKEDITOR.tools.callFunction(117,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Link\')\" onclick=\"CKEDITOR.tools.callFunction(120,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Unlink\')\" onclick=\"CKEDITOR.tools.callFunction(123,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Anchor\')\" onclick=\"CKEDITOR.tools.callFunction(126,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Image\')\" onclick=\"CKEDITOR.tools.callFunction(129,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Table\')\" onclick=\"CKEDITOR.tools.callFunction(132,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Insert Horizontal Line\')\" onclick=\"CKEDITOR.tools.callFunction(135,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Insert Special Character\')\" onclick=\"CKEDITOR.tools.callFunction(138,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Maximise\')\" onclick=\"CKEDITOR.tools.callFunction(141,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Source\')\" onclick=\"CKEDITOR.tools.callFunction(144,this);return false;\">&nbsp;Source</a><a href=\"javascript:void(\'Bold\')\" onclick=\"CKEDITOR.tools.callFunction(147,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Italic\')\" onclick=\"CKEDITOR.tools.callFunction(150,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Strike Through\')\" onclick=\"CKEDITOR.tools.callFunction(153,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Remove Format\')\" onclick=\"CKEDITOR.tools.callFunction(156,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Insert/Remove Numbered List\')\" onclick=\"CKEDITOR.tools.callFunction(159,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Insert/Remove Bulleted List\')\" onclick=\"CKEDITOR.tools.callFunction(162,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Decrease Indent\')\" onclick=\"CKEDITOR.tools.callFunction(165,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Increase Indent\')\" onclick=\"CKEDITOR.tools.callFunction(168,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Block Quote\')\" onclick=\"CKEDITOR.tools.callFunction(171,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Formatting Styles\')\" onclick=\"CKEDITOR.tools.callFunction(172,this);return false;\">Styles</a><a href=\"javascript:void(\'Paragraph Format\')\" onclick=\"CKEDITOR.tools.callFunction(175,this);return false;\">Normal</a><a href=\"javascript:void(\'About CKEditor 4\')\" onclick=\"CKEDITOR.tools.callFunction(180,this);return false;\">&nbsp;</a></p>\n\n<p><strong>Identify and explain two (2) differences between&nbsp;</strong><em><strong>Ifta&rsquo;&nbsp;</strong></em><strong>and&nbsp;</strong><em><strong>Qada&rsquo;</strong></em></p>\n\n<p><a href=\"javascript:void(\'Cut\')\" onclick=\"CKEDITOR.tools.callFunction(96,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Copy\')\" onclick=\"CKEDITOR.tools.callFunction(99,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Paste\')\" onclick=\"CKEDITOR.tools.callFunction(102,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Paste as plain text\')\" onclick=\"CKEDITOR.tools.callFunction(105,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Paste from Word\')\" onclick=\"CKEDITOR.tools.callFunction(108,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Undo\')\" onclick=\"CKEDITOR.tools.callFunction(111,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Redo\')\" onclick=\"CKEDITOR.tools.callFunction(114,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Spell Checker\')\" onclick=\"CKEDITOR.tools.callFunction(117,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Link\')\" onclick=\"CKEDITOR.tools.callFunction(120,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Unlink\')\" onclick=\"CKEDITOR.tools.callFunction(123,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Anchor\')\" onclick=\"CKEDITOR.tools.callFunction(126,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Image\')\" onclick=\"CKEDITOR.tools.callFunction(129,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Table\')\" onclick=\"CKEDITOR.tools.callFunction(132,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Insert Horizontal Line\')\" onclick=\"CKEDITOR.tools.callFunction(135,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Insert Special Character\')\" onclick=\"CKEDITOR.tools.callFunction(138,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Maximise\')\" onclick=\"CKEDITOR.tools.callFunction(141,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Source\')\" onclick=\"CKEDITOR.tools.callFunction(144,this);return false;\">&nbsp;Source</a><a href=\"javascript:void(\'Bold\')\" onclick=\"CKEDITOR.tools.callFunction(147,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Italic\')\" onclick=\"CKEDITOR.tools.callFunction(150,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Strike Through\')\" onclick=\"CKEDITOR.tools.callFunction(153,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Remove Format\')\" onclick=\"CKEDITOR.tools.callFunction(156,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Insert/Remove Numbered List\')\" onclick=\"CKEDITOR.tools.callFunction(159,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Insert/Remove Bulleted List\')\" onclick=\"CKEDITOR.tools.callFunction(162,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Decrease Indent\')\" onclick=\"CKEDITOR.tools.callFunction(165,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Increase Indent\')\" onclick=\"CKEDITOR.tools.callFunction(168,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Block Quote\')\" onclick=\"CKEDITOR.tools.callFunction(171,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Formatting Styles\')\" onclick=\"CKEDITOR.tools.callFunction(172,this);return false;\">Styles</a><a href=\"javascript:void(\'Paragraph Format\')\" onclick=\"CKEDITOR.tools.callFunction(175,this);return false;\">Normal</a><a href=\"javascript:void(\'About CKEditor 4\')\" onclick=\"CKEDITOR.tools.callFunction(180,this);return false;\">&nbsp;</a></p>\n\n<p><strong>Identify and explain two (2) differences between&nbsp;</strong><em><strong>Ifta&rsquo;&nbsp;</strong></em><strong>and&nbsp;</strong><em><strong>Qada&rsquo;</strong></em></p>\n\n<p><a href=\"javascript:void(\'Cut\')\" onclick=\"CKEDITOR.tools.callFunction(96,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Copy\')\" onclick=\"CKEDITOR.tools.callFunction(99,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Paste\')\" onclick=\"CKEDITOR.tools.callFunction(102,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Paste as plain text\')\" onclick=\"CKEDITOR.tools.callFunction(105,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Paste from Word\')\" onclick=\"CKEDITOR.tools.callFunction(108,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Undo\')\" onclick=\"CKEDITOR.tools.callFunction(111,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Redo\')\" onclick=\"CKEDITOR.tools.callFunction(114,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Spell Checker\')\" onclick=\"CKEDITOR.tools.callFunction(117,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Link\')\" onclick=\"CKEDITOR.tools.callFunction(120,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Unlink\')\" onclick=\"CKEDITOR.tools.callFunction(123,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Anchor\')\" onclick=\"CKEDITOR.tools.callFunction(126,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Image\')\" onclick=\"CKEDITOR.tools.callFunction(129,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Table\')\" onclick=\"CKEDITOR.tools.callFunction(132,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Insert Horizontal Line\')\" onclick=\"CKEDITOR.tools.callFunction(135,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Insert Special Character\')\" onclick=\"CKEDITOR.tools.callFunction(138,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Maximise\')\" onclick=\"CKEDITOR.tools.callFunction(141,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Source\')\" onclick=\"CKEDITOR.tools.callFunction(144,this);return false;\">&nbsp;Source</a><a href=\"javascript:void(\'Bold\')\" onclick=\"CKEDITOR.tools.callFunction(147,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Italic\')\" onclick=\"CKEDITOR.tools.callFunction(150,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Strike Through\')\" onclick=\"CKEDITOR.tools.callFunction(153,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Remove Format\')\" onclick=\"CKEDITOR.tools.callFunction(156,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Insert/Remove Numbered List\')\" onclick=\"CKEDITOR.tools.callFunction(159,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Insert/Remove Bulleted List\')\" onclick=\"CKEDITOR.tools.callFunction(162,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Decrease Indent\')\" onclick=\"CKEDITOR.tools.callFunction(165,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Increase Indent\')\" onclick=\"CKEDITOR.tools.callFunction(168,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Block Quote\')\" onclick=\"CKEDITOR.tools.callFunction(171,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Formatting Styles\')\" onclick=\"CKEDITOR.tools.callFunction(172,this);return false;\">Styles</a><a href=\"javascript:void(\'Paragraph Format\')\" onclick=\"CKEDITOR.tools.callFunction(175,this);return false;\">Normal</a><a href=\"javascript:void(\'About CKEditor 4\')\" onclick=\"CKEDITOR.tools.callFunction(180,this);return false;\">&nbsp;</a></p>\n', NULL, NULL, '31 -B'),
(33, 14, 75, 32, NULL, 14, 0, 2, NULL, NULL, NULL, '32 -A'),
(34, 14, 76, 33, '2021-03-16 00:14:14', 14, 0, 2, '<ol>\n	<li>\n	<p><strong>What are some of the salient features of the Shariah? Describe them briefly.</strong></p>\n	</li>\n</ol>\n\n<p><a href=\"javascript:void(\'Cut\')\" onclick=\"CKEDITOR.tools.callFunction(464,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Copy\')\" onclick=\"CKEDITOR.tools.callFunction(467,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Paste\')\" onclick=\"CKEDITOR.tools.callFunction(470,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Paste as plain text\')\" onclick=\"CKEDITOR.tools.callFunction(473,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Paste from Word\')\" onclick=\"CKEDITOR.tools.callFunction(476,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Undo\')\" onclick=\"CKEDITOR.tools.callFunction(479,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Redo\')\" onclick=\"CKEDITOR.tools.callFunction(482,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Spell Checker\')\" onclick=\"CKEDITOR.tools.callFunction(485,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Link\')\" onclick=\"CKEDITOR.tools.callFunction(488,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Unlink\')\" onclick=\"CKEDITOR.tools.callFunction(491,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Anchor\')\" onclick=\"CKEDITOR.tools.callFunction(494,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Image\')\" onclick=\"CKEDITOR.tools.callFunction(497,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Table\')\" onclick=\"CKEDITOR.tools.callFunction(500,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Insert Horizontal Line\')\" onclick=\"CKEDITOR.tools.callFunction(503,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Insert Special Character\')\" onclick=\"CKEDITOR.tools.callFunction(506,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Maximise\')\" onclick=\"CKEDITOR.tools.callFunction(509,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Source\')\" onclick=\"CKEDITOR.tools.callFunction(512,this);return false;\">&nbsp;Source</a><a href=\"javascript:void(\'Bold\')\" onclick=\"CKEDITOR.tools.callFunction(515,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Italic\')\" onclick=\"CKEDITOR.tools.callFunction(518,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Strike Through\')\" onclick=\"CKEDITOR.tools.callFunction(521,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Remove Format\')\" onclick=\"CKEDITOR.tools.callFunction(524,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Insert/Remove Numbered List\')\" onclick=\"CKEDITOR.tools.callFunction(527,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Insert/Remove Bulleted List\')\" onclick=\"CKEDITOR.tools.callFunction(530,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Decrease Indent\')\" onclick=\"CKEDITOR.tools.callFunction(533,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Increase Indent\')\" onclick=\"CKEDITOR.tools.callFunction(536,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Block Quote\')\" onclick=\"CKEDITOR.tools.callFunction(539,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Formatting Styles\')\" onclick=\"CKEDITOR.tools.callFunction(540,this);return false;\">Styles</a><a href=\"javascript:void(\'Paragraph Format\')\" onclick=\"CKEDITOR.tools.callFunction(543,this);return false;\">Normal</a><a href=\"javascript:void(\'About CKEditor 4\')\" onclick=\"CKEDITOR.tools.callFunction(548,this);return false;\">&nbsp;</a></p>\n', NULL, NULL, '32 -B'),
(35, 14, 78, 34, '2021-03-16 00:14:11', 14, 0, 2, '<ol>\n	<li>\n	<p><strong>What are some of the salient features of the Shariah? Describe them briefly.</strong></p>\n	</li>\n</ol>\n\n<p><a href=\"javascript:void(\'Cut\')\" onclick=\"CKEDITOR.tools.callFunction(464,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Copy\')\" onclick=\"CKEDITOR.tools.callFunction(467,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Paste\')\" onclick=\"CKEDITOR.tools.callFunction(470,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Paste as plain text\')\" onclick=\"CKEDITOR.tools.callFunction(473,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Paste from Word\')\" onclick=\"CKEDITOR.tools.callFunction(476,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Undo\')\" onclick=\"CKEDITOR.tools.callFunction(479,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Redo\')\" onclick=\"CKEDITOR.tools.callFunction(482,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Spell Checker\')\" onclick=\"CKEDITOR.tools.callFunction(485,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Link\')\" onclick=\"CKEDITOR.tools.callFunction(488,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Unlink\')\" onclick=\"CKEDITOR.tools.callFunction(491,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Anchor\')\" onclick=\"CKEDITOR.tools.callFunction(494,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Image\')\" onclick=\"CKEDITOR.tools.callFunction(497,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Table\')\" onclick=\"CKEDITOR.tools.callFunction(500,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Insert Horizontal Line\')\" onclick=\"CKEDITOR.tools.callFunction(503,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Insert Special Character\')\" onclick=\"CKEDITOR.tools.callFunction(506,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Maximise\')\" onclick=\"CKEDITOR.tools.callFunction(509,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Source\')\" onclick=\"CKEDITOR.tools.callFunction(512,this);return false;\">&nbsp;Source</a><a href=\"javascript:void(\'Bold\')\" onclick=\"CKEDITOR.tools.callFunction(515,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Italic\')\" onclick=\"CKEDITOR.tools.callFunction(518,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Strike Through\')\" onclick=\"CKEDITOR.tools.callFunction(521,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Remove Format\')\" onclick=\"CKEDITOR.tools.callFunction(524,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Insert/Remove Numbered List\')\" onclick=\"CKEDITOR.tools.callFunction(527,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Insert/Remove Bulleted List\')\" onclick=\"CKEDITOR.tools.callFunction(530,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Decrease Indent\')\" onclick=\"CKEDITOR.tools.callFunction(533,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Increase Indent\')\" onclick=\"CKEDITOR.tools.callFunction(536,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Block Quote\')\" onclick=\"CKEDITOR.tools.callFunction(539,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Formatting Styles\')\" onclick=\"CKEDITOR.tools.callFunction(540,this);return false;\">Styles</a><a href=\"javascript:void(\'Paragraph Format\')\" onclick=\"CKEDITOR.tools.callFunction(543,this);return false;\">Normal</a><a href=\"javascript:void(\'About CKEditor 4\')\" onclick=\"CKEDITOR.tools.callFunction(548,this);return false;\">&nbsp;</a></p>\n\n<ol>\n	<li>\n	<p><strong>What are some of the salient features of the Shariah? Describe them briefly.</strong></p>\n	</li>\n</ol>\n\n<p><a href=\"javascript:void(\'Cut\')\" onclick=\"CKEDITOR.tools.callFunction(464,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Copy\')\" onclick=\"CKEDITOR.tools.callFunction(467,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Paste\')\" onclick=\"CKEDITOR.tools.callFunction(470,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Paste as plain text\')\" onclick=\"CKEDITOR.tools.callFunction(473,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Paste from Word\')\" onclick=\"CKEDITOR.tools.callFunction(476,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Undo\')\" onclick=\"CKEDITOR.tools.callFunction(479,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Redo\')\" onclick=\"CKEDITOR.tools.callFunction(482,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Spell Checker\')\" onclick=\"CKEDITOR.tools.callFunction(485,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Link\')\" onclick=\"CKEDITOR.tools.callFunction(488,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Unlink\')\" onclick=\"CKEDITOR.tools.callFunction(491,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Anchor\')\" onclick=\"CKEDITOR.tools.callFunction(494,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Image\')\" onclick=\"CKEDITOR.tools.callFunction(497,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Table\')\" onclick=\"CKEDITOR.tools.callFunction(500,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Insert Horizontal Line\')\" onclick=\"CKEDITOR.tools.callFunction(503,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Insert Special Character\')\" onclick=\"CKEDITOR.tools.callFunction(506,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Maximise\')\" onclick=\"CKEDITOR.tools.callFunction(509,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Source\')\" onclick=\"CKEDITOR.tools.callFunction(512,this);return false;\">&nbsp;Source</a><a href=\"javascript:void(\'Bold\')\" onclick=\"CKEDITOR.tools.callFunction(515,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Italic\')\" onclick=\"CKEDITOR.tools.callFunction(518,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Strike Through\')\" onclick=\"CKEDITOR.tools.callFunction(521,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Remove Format\')\" onclick=\"CKEDITOR.tools.callFunction(524,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Insert/Remove Numbered List\')\" onclick=\"CKEDITOR.tools.callFunction(527,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Insert/Remove Bulleted List\')\" onclick=\"CKEDITOR.tools.callFunction(530,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Decrease Indent\')\" onclick=\"CKEDITOR.tools.callFunction(533,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Increase Indent\')\" onclick=\"CKEDITOR.tools.callFunction(536,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Block Quote\')\" onclick=\"CKEDITOR.tools.callFunction(539,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Formatting Styles\')\" onclick=\"CKEDITOR.tools.callFunction(540,this);return false;\">Styles</a><a href=\"javascript:void(\'Paragraph Format\')\" onclick=\"CKEDITOR.tools.callFunction(543,this);return false;\">Normal</a><a href=\"javascript:void(\'About CKEditor 4\')\" onclick=\"CKEDITOR.tools.callFunction(548,this);return false;\">&nbsp;</a></p>\n\n<ol>\n	<li>\n	<p><strong>What are some of the salient features of the Shariah? Describe them briefly.</strong></p>\n	</li>\n</ol>\n\n<p><a href=\"javascript:void(\'Cut\')\" onclick=\"CKEDITOR.tools.callFunction(464,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Copy\')\" onclick=\"CKEDITOR.tools.callFunction(467,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Paste\')\" onclick=\"CKEDITOR.tools.callFunction(470,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Paste as plain text\')\" onclick=\"CKEDITOR.tools.callFunction(473,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Paste from Word\')\" onclick=\"CKEDITOR.tools.callFunction(476,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Undo\')\" onclick=\"CKEDITOR.tools.callFunction(479,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Redo\')\" onclick=\"CKEDITOR.tools.callFunction(482,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Spell Checker\')\" onclick=\"CKEDITOR.tools.callFunction(485,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Link\')\" onclick=\"CKEDITOR.tools.callFunction(488,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Unlink\')\" onclick=\"CKEDITOR.tools.callFunction(491,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Anchor\')\" onclick=\"CKEDITOR.tools.callFunction(494,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Image\')\" onclick=\"CKEDITOR.tools.callFunction(497,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Table\')\" onclick=\"CKEDITOR.tools.callFunction(500,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Insert Horizontal Line\')\" onclick=\"CKEDITOR.tools.callFunction(503,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Insert Special Character\')\" onclick=\"CKEDITOR.tools.callFunction(506,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Maximise\')\" onclick=\"CKEDITOR.tools.callFunction(509,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Source\')\" onclick=\"CKEDITOR.tools.callFunction(512,this);return false;\">&nbsp;Source</a><a href=\"javascript:void(\'Bold\')\" onclick=\"CKEDITOR.tools.callFunction(515,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Italic\')\" onclick=\"CKEDITOR.tools.callFunction(518,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Strike Through\')\" onclick=\"CKEDITOR.tools.callFunction(521,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Remove Format\')\" onclick=\"CKEDITOR.tools.callFunction(524,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Insert/Remove Numbered List\')\" onclick=\"CKEDITOR.tools.callFunction(527,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Insert/Remove Bulleted List\')\" onclick=\"CKEDITOR.tools.callFunction(530,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Decrease Indent\')\" onclick=\"CKEDITOR.tools.callFunction(533,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Increase Indent\')\" onclick=\"CKEDITOR.tools.callFunction(536,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Block Quote\')\" onclick=\"CKEDITOR.tools.callFunction(539,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Formatting Styles\')\" onclick=\"CKEDITOR.tools.callFunction(540,this);return false;\">Styles</a><a href=\"javascript:void(\'Paragraph Format\')\" onclick=\"CKEDITOR.tools.callFunction(543,this);return false;\">Normal</a><a href=\"javascript:void(\'About CKEditor 4\')\" onclick=\"CKEDITOR.tools.callFunction(548,this);return false;\">&nbsp;</a></p>\n\n<ol>\n	<li>\n	<p><strong>What are some of the salient features of the Shariah? Describe them briefly.</strong></p>\n	</li>\n</ol>\n\n<p><a href=\"javascript:void(\'Cut\')\" onclick=\"CKEDITOR.tools.callFunction(464,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Copy\')\" onclick=\"CKEDITOR.tools.callFunction(467,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Paste\')\" onclick=\"CKEDITOR.tools.callFunction(470,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Paste as plain text\')\" onclick=\"CKEDITOR.tools.callFunction(473,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Paste from Word\')\" onclick=\"CKEDITOR.tools.callFunction(476,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Undo\')\" onclick=\"CKEDITOR.tools.callFunction(479,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Redo\')\" onclick=\"CKEDITOR.tools.callFunction(482,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Spell Checker\')\" onclick=\"CKEDITOR.tools.callFunction(485,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Link\')\" onclick=\"CKEDITOR.tools.callFunction(488,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Unlink\')\" onclick=\"CKEDITOR.tools.callFunction(491,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Anchor\')\" onclick=\"CKEDITOR.tools.callFunction(494,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Image\')\" onclick=\"CKEDITOR.tools.callFunction(497,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Table\')\" onclick=\"CKEDITOR.tools.callFunction(500,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Insert Horizontal Line\')\" onclick=\"CKEDITOR.tools.callFunction(503,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Insert Special Character\')\" onclick=\"CKEDITOR.tools.callFunction(506,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Maximise\')\" onclick=\"CKEDITOR.tools.callFunction(509,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Source\')\" onclick=\"CKEDITOR.tools.callFunction(512,this);return false;\">&nbsp;Source</a><a href=\"javascript:void(\'Bold\')\" onclick=\"CKEDITOR.tools.callFunction(515,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Italic\')\" onclick=\"CKEDITOR.tools.callFunction(518,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Strike Through\')\" onclick=\"CKEDITOR.tools.callFunction(521,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Remove Format\')\" onclick=\"CKEDITOR.tools.callFunction(524,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Insert/Remove Numbered List\')\" onclick=\"CKEDITOR.tools.callFunction(527,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Insert/Remove Bulleted List\')\" onclick=\"CKEDITOR.tools.callFunction(530,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Decrease Indent\')\" onclick=\"CKEDITOR.tools.callFunction(533,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Increase Indent\')\" onclick=\"CKEDITOR.tools.callFunction(536,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Block Quote\')\" onclick=\"CKEDITOR.tools.callFunction(539,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Formatting Styles\')\" onclick=\"CKEDITOR.tools.callFunction(540,this);return false;\">Styles</a><a href=\"javascript:void(\'Paragraph Format\')\" onclick=\"CKEDITOR.tools.callFunction(543,this);return false;\">Normal</a><a href=\"javascript:void(\'About CKEditor 4\')\" onclick=\"CKEDITOR.tools.callFunction(548,this);return false;\">&nbsp;</a></p>\n', NULL, NULL, '33 -A'),
(36, 14, 79, 35, '2021-03-16 00:14:08', 14, 0, 2, '<p><strong>Identify and explain two (2) differences between&nbsp;</strong><em><strong>Ifta&rsquo;&nbsp;</strong></em><strong>and&nbsp;</strong><em><strong>Qada&rsquo;</strong></em></p>\n\n<p><a href=\"javascript:void(\'Cut\')\" onclick=\"CKEDITOR.tools.callFunction(96,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Copy\')\" onclick=\"CKEDITOR.tools.callFunction(99,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Paste\')\" onclick=\"CKEDITOR.tools.callFunction(102,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Paste as plain text\')\" onclick=\"CKEDITOR.tools.callFunction(105,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Paste from Word\')\" onclick=\"CKEDITOR.tools.callFunction(108,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Undo\')\" onclick=\"CKEDITOR.tools.callFunction(111,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Redo\')\" onclick=\"CKEDITOR.tools.callFunction(114,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Spell Checker\')\" onclick=\"CKEDITOR.tools.callFunction(117,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Link\')\" onclick=\"CKEDITOR.tools.callFunction(120,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Unlink\')\" onclick=\"CKEDITOR.tools.callFunction(123,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Anchor\')\" onclick=\"CKEDITOR.tools.callFunction(126,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Image\')\" onclick=\"CKEDITOR.tools.callFunction(129,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Table\')\" onclick=\"CKEDITOR.tools.callFunction(132,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Insert Horizontal Line\')\" onclick=\"CKEDITOR.tools.callFunction(135,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Insert Special Character\')\" onclick=\"CKEDITOR.tools.callFunction(138,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Maximise\')\" onclick=\"CKEDITOR.tools.callFunction(141,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Source\')\" onclick=\"CKEDITOR.tools.callFunction(144,this);return false;\">&nbsp;Source</a><a href=\"javascript:void(\'Bold\')\" onclick=\"CKEDITOR.tools.callFunction(147,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Italic\')\" onclick=\"CKEDITOR.tools.callFunction(150,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Strike Through\')\" onclick=\"CKEDITOR.tools.callFunction(153,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Remove Format\')\" onclick=\"CKEDITOR.tools.callFunction(156,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Insert/Remove Numbered List\')\" onclick=\"CKEDITOR.tools.callFunction(159,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Insert/Remove Bulleted List\')\" onclick=\"CKEDITOR.tools.callFunction(162,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Decrease Indent\')\" onclick=\"CKEDITOR.tools.callFunction(165,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Increase Indent\')\" onclick=\"CKEDITOR.tools.callFunction(168,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Block Quote\')\" onclick=\"CKEDITOR.tools.callFunction(171,this);return false;\">&nbsp;</a><a href=\"javascript:void(\'Formatting Styles\')\" onclick=\"CKEDITOR.tools.callFunction(172,this);return false;\">Styles</a><a href=\"javascript:void(\'Paragraph Format\')\" onclick=\"CKEDITOR.tools.callFunction(175,this);return false;\">Normal</a><a href=\"javascript:void(\'About CKEditor 4\')\" onclick=\"CKEDITOR.tools.callFunction(180,this);return false;\">&nbsp;</a></p>\n', NULL, NULL, '33 -B'),
(37, 12, 1, 0, NULL, 12, 0, 3, NULL, NULL, NULL, '1'),
(38, 12, 3, 1, NULL, 12, 0, 3, NULL, NULL, NULL, '2'),
(39, 12, 4, 2, NULL, 12, 0, 3, NULL, NULL, NULL, '3'),
(40, 12, 5, 3, NULL, 12, 0, 3, NULL, NULL, NULL, '4'),
(41, 12, 6, 4, NULL, 12, 0, 3, NULL, NULL, NULL, '5'),
(42, 12, 7, 5, NULL, 12, 0, 3, NULL, NULL, NULL, '6'),
(43, 12, 8, 6, NULL, 12, 0, 3, NULL, NULL, NULL, '7'),
(44, 12, 9, 7, NULL, 12, 0, 3, NULL, NULL, NULL, '8'),
(45, 12, 10, 8, NULL, 12, 0, 3, NULL, NULL, NULL, '9'),
(46, 12, 11, 9, NULL, 12, 0, 3, NULL, NULL, NULL, '10'),
(47, 12, 12, 10, NULL, 12, 0, 3, NULL, NULL, NULL, '11'),
(48, 12, 13, 11, NULL, 12, 0, 3, NULL, NULL, NULL, '12'),
(49, 12, 14, 12, NULL, 12, 0, 3, NULL, NULL, NULL, '13'),
(50, 12, 15, 13, NULL, 12, 0, 3, NULL, NULL, NULL, '14'),
(51, 12, 16, 14, NULL, 12, 0, 3, NULL, NULL, NULL, '15'),
(52, 12, 17, 15, NULL, 12, 0, 3, NULL, NULL, NULL, '16'),
(53, 12, 18, 16, NULL, 12, 0, 3, NULL, NULL, NULL, '17'),
(54, 12, 19, 17, NULL, 12, 0, 3, NULL, NULL, NULL, '18'),
(55, 12, 20, 18, NULL, 12, 0, 3, NULL, NULL, NULL, '19'),
(56, 12, 21, 19, NULL, 12, 0, 3, NULL, NULL, NULL, '20'),
(57, 12, 22, 20, NULL, 12, 0, 3, NULL, NULL, NULL, '21'),
(58, 12, 23, 21, NULL, 12, 0, 3, NULL, NULL, NULL, '22'),
(59, 12, 24, 22, NULL, 12, 0, 3, NULL, NULL, NULL, '23'),
(60, 12, 25, 23, NULL, 12, 0, 3, NULL, NULL, NULL, '24'),
(61, 12, 26, 24, NULL, 12, 0, 3, NULL, NULL, NULL, '25'),
(62, 12, 27, 25, NULL, 12, 0, 3, NULL, NULL, NULL, '26'),
(63, 12, 28, 26, NULL, 12, 0, 3, NULL, NULL, NULL, '27'),
(64, 12, 29, 27, NULL, 12, 0, 3, NULL, NULL, NULL, '28'),
(65, 12, 30, 28, NULL, 12, 0, 3, NULL, NULL, NULL, '29'),
(66, 12, 35, 29, NULL, 12, 0, 3, NULL, NULL, NULL, '30'),
(67, 12, 71, 30, NULL, 12, 0, 3, NULL, NULL, NULL, '31 -A'),
(68, 12, 72, 31, NULL, 12, 0, 3, NULL, NULL, NULL, '31 -B'),
(69, 12, 75, 32, NULL, 12, 0, 3, NULL, NULL, NULL, '32 -A'),
(70, 12, 76, 33, NULL, 12, 0, 3, NULL, NULL, NULL, '32 -B'),
(71, 12, 78, 34, NULL, 12, 0, 3, NULL, NULL, NULL, '33 -A'),
(72, 12, 79, 35, NULL, 12, 0, 3, NULL, NULL, NULL, '33 -B');

-- --------------------------------------------------------

--
-- Table structure for table `student_question_set_marks`
--

CREATE TABLE `student_question_set_marks` (
  `id` bigint(20) NOT NULL,
  `id_student_question_set` bigint(20) DEFAULT 0,
  `id_grader` bigint(20) DEFAULT 0,
  `marks` varchar(200) DEFAULT '',
  `status` int(11) DEFAULT 0,
  `created_by` int(11) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp(),
  `id_grader_exam` int(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `student_question_set_marks`
--

INSERT INTO `student_question_set_marks` (`id`, `id_student_question_set`, `id_grader`, `marks`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`, `id_grader_exam`) VALUES
(1, 16, 4, '2', 1, NULL, '2021-03-01 16:48:03', NULL, '2021-03-01 16:48:03', NULL),
(2, 17, 4, '4', 1, NULL, '2021-03-01 17:25:04', NULL, '2021-03-01 17:25:04', NULL),
(9, 31, 5, '6', 1, NULL, '2021-03-18 01:44:26', NULL, '2021-03-18 01:44:26', NULL),
(10, 32, 4, '5', 0, NULL, '2021-03-18 01:44:26', NULL, '2021-03-18 01:44:26', NULL),
(11, 33, 4, '5', 0, NULL, '2021-03-18 01:44:26', NULL, '2021-03-18 01:44:26', NULL),
(12, 34, 4, '5', 0, NULL, '2021-03-18 01:44:26', NULL, '2021-03-18 01:44:26', NULL),
(13, 35, 4, '5', 0, NULL, '2021-03-18 01:44:26', NULL, '2021-03-18 01:44:26', NULL),
(14, 36, 4, '5', 0, NULL, '2021-03-18 01:44:26', NULL, '2021-03-18 01:44:26', NULL),
(15, 31, 5, '3', 1, NULL, '2021-03-18 03:59:43', NULL, '2021-03-18 03:59:43', 10),
(16, 32, 5, '5', 1, NULL, '2021-03-18 04:00:35', NULL, '2021-03-18 04:00:35', 10);

-- --------------------------------------------------------

--
-- Table structure for table `student_question_set_marks_adjustment`
--

CREATE TABLE `student_question_set_marks_adjustment` (
  `id` bigint(20) NOT NULL,
  `id_student_question_set` bigint(20) DEFAULT 0,
  `marks` varchar(200) DEFAULT '',
  `status` int(11) DEFAULT 0,
  `created_by` int(11) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `student_question_set_marks_adjustment`
--

INSERT INTO `student_question_set_marks_adjustment` (`id`, `id_student_question_set`, `marks`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(37, 31, '4', 0, NULL, '2021-03-18 01:58:55', NULL, '2021-03-18 01:58:55'),
(38, 32, '5', 0, NULL, '2021-03-18 01:58:55', NULL, '2021-03-18 01:58:55'),
(39, 33, '4', 0, NULL, '2021-03-18 01:58:55', NULL, '2021-03-18 01:58:55'),
(40, 34, '5', 0, NULL, '2021-03-18 01:58:55', NULL, '2021-03-18 01:58:55'),
(41, 35, '4', 0, NULL, '2021-03-18 01:58:55', NULL, '2021-03-18 01:58:55'),
(42, 36, '5', 0, NULL, '2021-03-18 01:58:55', NULL, '2021-03-18 01:58:55');

-- --------------------------------------------------------

--
-- Table structure for table `temp_exam_question`
--

CREATE TABLE `temp_exam_question` (
  `id` int(20) NOT NULL,
  `session_id` varchar(250) DEFAULT NULL,
  `qmarks` varchar(20) DEFAULT NULL,
  `answer_question` text DEFAULT NULL,
  `answer_scheme` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `temp_exam_question`
--

INSERT INTO `temp_exam_question` (`id`, `session_id`, `qmarks`, `answer_question`, `answer_scheme`) VALUES
(11, '7f0s86na1uoppelqsqh9o65l9n0udeh3', '4', '<p>Paragraph question 1</p>\n', '<p>Answer scheme 1</p>\n'),
(12, '7f0s86na1uoppelqsqh9o65l9n0udeh3', '6', '<p>Paragraph question 2</p>\n', '<p>Answer scheme 2</p>\n'),
(13, 'e47e84252ernf7fnfug05d9hq8t94gkm', '5', '<p>Paragraph 2 question a</p>\n', '<p>paragraph 2 scheme a</p>\n'),
(14, 'e47e84252ernf7fnfug05d9hq8t94gkm', '5', '<p>Paragraph 2 question b</p>\n', '<p>paragraph 2 scheme b</p>\n'),
(15, 'g8bcsc44aft7c2ep8lmirulbdgbp16tp', '5', '<p>ASDF Question 1</p>\n', '<p>asfsdfsdfdas</p>\n'),
(16, 'rofvaedsg78hprgjce4rndbihdmj8fqm', '6', '<p>Question one goes here</p>\n', '<p>Answer schemen</p>\n'),
(17, 'rofvaedsg78hprgjce4rndbihdmj8fqm', '4', '<p>Question two goes here</p>\n', '<p>Answer schemen two</p>\n'),
(22, '0fbe9e9e3a4db271291cf94ac6cb4ba489d99257', '4', '<p><strong>Define </strong><em><strong>hukm taklifi</strong></em><strong> (obligatory rule) and </strong><em><strong>hukm wad&rsquo;i</strong></em><strong> (declaratory rule)</strong></p>\n', '<p>Al-Ahk?m at-Taklifiyyah (obligatory rules) refers to the communication from the Lawgiver that consists of a command</p>\n\n<p>and an option.</p>\n\n<p><em>al-Ahk?m al-Wad&rsquo;iyyah </em>(declaratory rules) refers to &ldquo;a communication</p>\n\n<p>from the Lawgiver that enacts something into a cause, or a condition, or an obstacle, or validity or non-validity, or <em>&lsquo;Azimah </em>(strict rule) and <em>Rukhsah </em>(concessionary rule)&rdquo;.</p>\n'),
(23, '0fbe9e9e3a4db271291cf94ac6cb4ba489d99257', '6', '<p><strong>Identify 3 differences between </strong><em><strong>hukm taklifi </strong></em><strong>(obligatory rule) and </strong><em><strong>hukm wad&rsquo;i</strong></em><strong> (declaratory rule) and provide an example for each difference. ) </strong></p>\n', '<table cellpadding=\"7\" cellspacing=\"0\" style=\"width:601px\">\n	<tbody>\n		<tr>\n			<td>\n			<p>3.</p>\n			</td>\n			<td>\n			<p>Action of Mukallaf</p>\n			</td>\n			<td>\n			<p><em>hukm taklifi </em>is attached to action and direct involvement of the <em>mukallaf.</em></p>\n			</td>\n			<td>\n			<p><em>Hukm wad&rsquo;i does not necessarily directly involve the mukallaf</em>.</p>\n\n			<p>&nbsp;</p>\n\n			<p>E.g. people are responsible for damage caused by their relatives, therefore, <em>diyah </em>(blood money) is mandatory. The payment is not due to action of the payer but due to an act of his relatives,</p>\n			</td>\n		</tr>\n	</tbody>\n</table>\n'),
(26, '4a6c45a753059154fa604cc168dfb8263c1ddb6a', '6', '<p>&ldquo;<strong>O you who believe! Eat not up your property&mdash;among yourselves in vanities: but let there be amongst you traffic and trade by mutual good-will:&hellip;&rdquo; (Al-Nis?&rsquo;: 29)</strong></p>\n\n<p><strong>Referring to the verse above, the following are the principles that can be derived.</strong></p>\n\n<ol>\n	<li>\n	<p><strong>Justice</strong></p>\n	</li>\n	<li>\n	<p><strong>Consent</strong></p>\n	</li>\n	<li>\n	<p><strong>Discuss what you understand by these two principles </strong></p>\n	</li>\n</ol>\n', '<p>Consent &ndash; This? rule? from? the? Quran? state? the? requirement? for? taking? other&rsquo;s? property? either? by? trade or gift should be done by the consent of the parties. Therefore, taking property such as by theft or illegal consumption is prohibited in Islam. [Student nee to provide example of situation where this rule is not abide with. Example may be breach of trust in safekeeping of investment money]</p>\n\n<p>Justice? - means? giving? everyone? their? due? rights.? Hence,? injustice? means? not? giving? one&rsquo;s? right.? In? a contract, justice means to fulfil the responsibilities and to give or uphold the right of other parties in the contract as agreed. [Student need to provide example of situation where this rule is not abide with. Example is withheld the payment of debt although one is able to repay his or her debt]</p>\n'),
(27, '4a6c45a753059154fa604cc168dfb8263c1ddb6a', '4', '<ol>\n	<li>\n	<p><strong>What are some of the salient features of the Shariah? Describe them briefly. </strong></p>\n	</li>\n</ol>\n', '<p><strong>Universality of </strong><em>Shariah</em></p>\n\n<p><em>Shariah </em>is a principle that can be applied to the whole of mankind regardless of nationality, race or tribe. Therefore, all the <em>Shariah </em>rulings in the Qur&rsquo;an and <em>Sunnah </em>are universally applicable unless specifically mentioned otherwise.</p>\n\n<p><strong>Comprehensiveness of </strong><em>Shariah</em></p>\n\n<p>The <em>Shariah </em>goes beyond religious aspects and encompasses all stages of life and all human conduct including public and personal affairs.</p>\n\n<p><em>Shariah </em><strong>Aims to Ease Human Life</strong></p>\n\n<p>The <em>Shariah </em>is about making life easy for all mankind through its divine rules which prioritises human needs and comforts.</p>\n');

-- --------------------------------------------------------

--
-- Table structure for table `temp_main_invoice_details`
--

CREATE TABLE `temp_main_invoice_details` (
  `id` int(11) NOT NULL,
  `id_session` varchar(512) DEFAULT NULL,
  `id_fee_item` int(11) DEFAULT NULL,
  `price` int(11) DEFAULT 0,
  `quantity` int(11) DEFAULT 0,
  `amount` float(20,2) DEFAULT NULL,
  `id_reference` int(11) DEFAULT 0,
  `description` varchar(1024) DEFAULT '',
  `status` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `temp_receipt_details`
--

CREATE TABLE `temp_receipt_details` (
  `id` int(11) NOT NULL,
  `id_session` varchar(120) DEFAULT '',
  `id_main_invoice` int(11) DEFAULT NULL,
  `invoice_amount` float DEFAULT NULL,
  `paid_amount` float DEFAULT NULL,
  `approval_status` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `topic`
--

CREATE TABLE `topic` (
  `id` int(11) NOT NULL,
  `name` varchar(1024) DEFAULT '',
  `code` varchar(50) DEFAULT '',
  `id_course` bigint(20) NOT NULL,
  `name_optional_language` varchar(1024) DEFAULT '',
  `status` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `topic`
--

INSERT INTO `topic` (`id`, `name`, `code`, `id_course`, `name_optional_language`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`) VALUES
(1, 'Topic 4', 'Topic 4', 3, '', 1, 1, '2020-12-22 09:36:03', NULL, '2020-12-22 09:36:03'),
(2, 'Topic 1', 'Topic 1', 3, '', 1, 1, '2020-12-22 09:48:27', 1, '2020-12-22 09:48:27'),
(13, 'Topic 1', 'Topic 1', 3, '', 1, 1, '2020-12-25 21:33:59', 1, '2020-12-25 21:33:59'),
(14, 'Topic 2', 'Topic 2', 4, '', 1, 1, '2020-12-25 21:34:20', NULL, '2020-12-25 21:34:20'),
(15, 'Topic 3', 'Topic 3', 3, '', 1, 1, '2020-12-22 09:48:41', NULL, '2020-12-22 09:48:41'),
(16, 'Topic 2', 'Topic 2', 3, '', 1, 1, '2020-12-22 09:48:54', 1, '2020-12-22 09:48:54'),
(17, 'Topic 5', 'Topic 5', 3, '', 1, 1, '2020-12-25 21:17:01', NULL, '2020-12-25 21:17:01'),
(20, 'Topic 3', 'Topic 3', 4, '', 1, 1, '2020-12-25 21:34:34', NULL, '2020-12-25 21:34:34'),
(21, 'Topic 4', 'Topic 4', 4, '', 1, 1, '2020-12-25 21:34:47', NULL, '2020-12-25 21:34:47'),
(22, 'Topic 5', 'Topic 5', 4, '', 1, 1, '2020-12-25 21:34:57', NULL, '2020-12-25 21:34:57');

-- --------------------------------------------------------

--
-- Table structure for table `tos`
--

CREATE TABLE `tos` (
  `id` bigint(20) NOT NULL,
  `name` varchar(100) NOT NULL,
  `id_pool` varchar(50) NOT NULL,
  `question_count` bigint(20) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_dt_tm` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_dt_tm` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tos`
--

INSERT INTO `tos` (`id`, `name`, `id_pool`, `question_count`, `status`, `created_by`, `updated_by`, `created_dt_tm`, `updated_dt_tm`) VALUES
(6, 'CIIF DEMO', '1,2', 6, 1, 1, 1, '2020-08-15 00:28:05', '2020-08-15 00:28:05');

-- --------------------------------------------------------

--
-- Table structure for table `tos_details`
--

CREATE TABLE `tos_details` (
  `id_details` bigint(20) NOT NULL,
  `id_course` int(11) NOT NULL,
  `id_topic` int(11) NOT NULL,
  `id_bloom_taxonomy` int(11) NOT NULL,
  `id_difficult_level` int(11) NOT NULL,
  `id_pool` int(11) NOT NULL,
  `questions_available` int(11) NOT NULL,
  `questions_selected` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `id_tos` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tos_details`
--

INSERT INTO `tos_details` (`id_details`, `id_course`, `id_topic`, `id_bloom_taxonomy`, `id_difficult_level`, `id_pool`, `questions_available`, `questions_selected`, `status`, `id_tos`) VALUES
(6, 1, 2, 1, 2, 1, 3, 2, 1, 6),
(7, 1, 2, 1, 3, 1, 1, 1, 1, 6),
(8, 1, 3, 1, 3, 1, 1, 1, 1, 6),
(9, 1, 5, 1, 1, 1, 2, 1, 1, 6),
(10, 2, 1, 4, 3, 2, 1, 1, 1, 6);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `email` varchar(1024) DEFAULT '',
  `password` varchar(500) DEFAULT '',
  `name` varchar(1024) DEFAULT '',
  `salutation` varchar(20) DEFAULT '',
  `first_name` varchar(1024) DEFAULT '',
  `last_name` varchar(1024) DEFAULT '',
  `mobile` varchar(50) DEFAULT '',
  `id_role` int(11) DEFAULT NULL,
  `is_deleted` int(11) DEFAULT 0,
  `status` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_dt_tm` datetime DEFAULT current_timestamp(),
  `updated_by` int(11) DEFAULT NULL,
  `updated_dt_tm` datetime DEFAULT current_timestamp(),
  `lms_password` varchar(100) DEFAULT NULL,
  `lms_username` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `name`, `salutation`, `first_name`, `last_name`, `mobile`, `id_role`, `is_deleted`, `status`, `created_by`, `created_dt_tm`, `updated_by`, `updated_dt_tm`, `lms_password`, `lms_username`) VALUES
(1, 'admin@exam.com', '7fef6171469e80d32c0559f88b377245', 'System Admin', '1', 'System', 'Admin', '88888888', 1, 0, 1, 1, '2020-07-26 23:20:26', NULL, '2020-07-26 23:20:26', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_last_login`
--

CREATE TABLE `user_last_login` (
  `id` bigint(20) NOT NULL,
  `id_user` bigint(20) NOT NULL,
  `session_data` varchar(2048) NOT NULL,
  `machine_ip` varchar(1024) NOT NULL,
  `user_agent` varchar(128) NOT NULL,
  `agent_string` varchar(1024) NOT NULL,
  `platform` varchar(128) NOT NULL,
  `created_dt_tm` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_last_login`
--

INSERT INTO `user_last_login` (`id`, `id_user`, `session_data`, `machine_ip`, `user_agent`, `agent_string`, `platform`, `created_dt_tm`) VALUES
(1, 1, '{\"role\":\"1\",\"roleText\":\"Examination Administrator\",\"name\":\"admin\"}', '127.0.0.1', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(2, 1, '{\"role\":\"1\",\"roleText\":\"Examination Administrator\",\"name\":\"admin\"}', '127.0.0.1', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(3, 1, '{\"role\":\"1\",\"roleText\":\"Examination Administrator\",\"name\":\"admin\"}', '157.45.19.112', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(4, 1, '{\"role\":\"1\",\"roleText\":\"Examination Administrator\",\"name\":\"admin\"}', '157.45.19.112', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(5, 1, '{\"role\":\"1\",\"roleText\":\"Examination Administrator\",\"name\":\"admin\"}', '49.206.7.23', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(6, 1, '{\"role\":\"1\",\"roleText\":\"Examination Administrator\",\"name\":\"admin\"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(7, 1, '{\"role\":\"1\",\"roleText\":\"Examination Administrator\",\"name\":\"admin\"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(8, 1, '{\"role\":\"1\",\"roleText\":\"Examination Administrator\",\"name\":\"admin\"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(9, 1, '{\"role\":\"1\",\"roleText\":\"Examination Administrator\",\"name\":\"admin\"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(10, 1, '{\"role\":\"1\",\"roleText\":\"Examination Administrator\",\"name\":\"admin\"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(11, 1, '{\"role\":\"2\",\"roleText\":\"Ex\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(12, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(13, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(14, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(15, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(16, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(17, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(18, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(19, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(20, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '117.230.180.35', 'Chrome 83.0.4103.116', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(21, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(22, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(23, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(24, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(25, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(26, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(27, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(28, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(29, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(30, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 84.0.4147.89', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.89 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(31, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(32, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(33, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(34, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(35, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '157.49.131.37', 'Chrome 85.0.4183.102', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(36, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '157.49.167.112', 'Chrome 85.0.4183.102', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(37, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(38, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(39, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '157.49.87.249', 'Chrome 85.0.4183.102', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.102 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(40, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(41, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '157.49.67.173', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(42, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '157.49.94.242', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(43, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(44, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '112.79.50.205', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(45, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(46, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(47, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(48, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(49, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '157.49.91.150', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(50, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '112.79.49.123', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(51, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(52, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(53, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(54, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(55, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(56, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(57, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(58, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(59, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '157.49.185.42', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(60, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(61, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '157.49.185.42', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(62, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(63, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(64, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(65, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '157.49.169.78', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(66, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '117.230.63.106', 'Chrome 85.0.4183.121', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(67, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(68, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"Mr. Virat Kohli\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(69, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"System Admin\"}', '42.190.22.135', 'Chrome 86.0.4240.111', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(70, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"System Admin\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(71, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"System Admin\"}', '42.190.22.135', 'Chrome 86.0.4240.111', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(72, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"System Admin\"}', '42.190.22.135', 'Chrome 86.0.4240.111', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(73, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"System Admin\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(74, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"System Admin\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(75, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"System Admin\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(76, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"System Admin\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(77, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"System Admin\"}', '49.206.4.152', 'Chrome 87.0.4280.88', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(78, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"System Admin\"}', '117.230.150.76', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(79, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"System Admin\"}', '49.206.4.152', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(80, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"System Admin\"}', '49.206.4.152', 'Chrome 87.0.4280.88', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(81, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"System Admin\"}', '117.230.38.240', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(82, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"System Admin\"}', '49.206.4.152', 'Chrome 87.0.4280.88', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(83, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"System Admin\"}', '42.105.125.15', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(84, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"System Admin\"}', '49.206.4.152', 'Chrome 87.0.4280.88', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(85, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"System Admin\"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(86, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"System Admin\"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(87, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"System Admin\"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(88, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"System Admin\"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(89, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"System Admin\"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(90, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"System Admin\"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(91, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"System Admin\"}', '157.45.64.44', 'Chrome 87.0.4280.66', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.66 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(92, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"System Admin\"}', '49.206.10.196', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(93, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"System Admin\"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(94, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"System Admin\"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(95, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"System Admin\"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(96, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"System Admin\"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(97, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"System Admin\"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(98, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"System Admin\"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(99, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"System Admin\"}', '49.206.15.200', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(100, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"System Admin\"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(101, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"System Admin\"}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(102, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"System Admin\",\"lms_username\":null,\"lms_password\":null}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(103, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"System Admin\",\"lms_username\":null,\"lms_password\":null}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(104, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"System Admin\",\"lms_username\":null,\"lms_password\":null}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(105, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"System Admin\",\"lms_username\":null,\"lms_password\":null}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(106, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"System Admin\",\"lms_username\":null,\"lms_password\":null}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(107, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"System Admin\",\"lms_username\":null,\"lms_password\":null}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(108, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"System Admin\",\"lms_username\":null,\"lms_password\":null}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(109, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"System Admin\",\"lms_username\":null,\"lms_password\":null}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(110, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"System Admin\",\"lms_username\":null,\"lms_password\":null}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(111, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"System Admin\",\"lms_username\":null,\"lms_password\":null}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(112, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"System Admin\",\"lms_username\":null,\"lms_password\":null}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(113, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"System Admin\",\"lms_username\":null,\"lms_password\":null}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(114, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"System Admin\",\"lms_username\":null,\"lms_password\":null}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(115, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"System Admin\",\"lms_username\":null,\"lms_password\":null}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(116, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"System Admin\",\"lms_username\":null,\"lms_password\":null}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(117, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"System Admin\",\"lms_username\":null,\"lms_password\":null}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(118, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"System Admin\",\"lms_username\":null,\"lms_password\":null}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(119, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"System Admin\",\"lms_username\":null,\"lms_password\":null}', '49.206.15.166', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(120, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"System Admin\",\"lms_username\":null,\"lms_password\":null}', '49.206.15.166', 'Chrome 89.0.4389.82', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.82 Safari/537.36', 'Windows 10', '0000-00-00 00:00:00'),
(121, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"System Admin\",\"lms_username\":null,\"lms_password\":null}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(122, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"System Admin\",\"lms_username\":null,\"lms_password\":null}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(123, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"System Admin\",\"lms_username\":null,\"lms_password\":null}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(124, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"System Admin\",\"lms_username\":null,\"lms_password\":null}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00'),
(125, 1, '{\"role\":\"1\",\"roleText\":\"Administrator\",\"name\":\"System Admin\",\"lms_username\":null,\"lms_password\":null}', '127.0.0.1', 'Chrome 78.0.3904.97', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36', 'Linux', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_last_login`
--
ALTER TABLE `admin_last_login`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `answer_status`
--
ALTER TABLE `answer_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bank_registration`
--
ALTER TABLE `bank_registration`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bloom_taxonomy`
--
ALTER TABLE `bloom_taxonomy`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `communication_group`
--
ALTER TABLE `communication_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `communication_group_message`
--
ALTER TABLE `communication_group_message`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `communication_group_message_recepients`
--
ALTER TABLE `communication_group_message_recepients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `communication_group_recepients`
--
ALTER TABLE `communication_group_recepients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `communication_template`
--
ALTER TABLE `communication_template`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `communication_template_message`
--
ALTER TABLE `communication_template_message`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `course`
--
ALTER TABLE `course`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `course_learning_objective`
--
ALTER TABLE `course_learning_objective`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `currency_rate_setup`
--
ALTER TABLE `currency_rate_setup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `currency_setup`
--
ALTER TABLE `currency_setup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `difficult_level`
--
ALTER TABLE `difficult_level`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `examset`
--
ALTER TABLE `examset`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `examset_questions`
--
ALTER TABLE `examset_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `exam_center`
--
ALTER TABLE `exam_center`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `exam_center_has_room`
--
ALTER TABLE `exam_center_has_room`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `exam_center_location`
--
ALTER TABLE `exam_center_location`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `exam_center_start_exam`
--
ALTER TABLE `exam_center_start_exam`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `exam_event`
--
ALTER TABLE `exam_event`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `exam_has_question`
--
ALTER TABLE `exam_has_question`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `exam_has_question_count`
--
ALTER TABLE `exam_has_question_count`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `exam_module`
--
ALTER TABLE `exam_module`
  ADD PRIMARY KEY (`module_id`);

--
-- Indexes for table `exam_name`
--
ALTER TABLE `exam_name`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `exam_schedule`
--
ALTER TABLE `exam_schedule`
  ADD PRIMARY KEY (`schedule_id`);

--
-- Indexes for table `exam_session`
--
ALTER TABLE `exam_session`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `exam_student_tagging`
--
ALTER TABLE `exam_student_tagging`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `exam_users`
--
ALTER TABLE `exam_users`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `exam_venue`
--
ALTER TABLE `exam_venue`
  ADD PRIMARY KEY (`venue_id`);

--
-- Indexes for table `fee_category`
--
ALTER TABLE `fee_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fee_setup`
--
ALTER TABLE `fee_setup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `financial_account_code`
--
ALTER TABLE `financial_account_code`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `grade`
--
ALTER TABLE `grade`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `grader`
--
ALTER TABLE `grader`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `grader_exam`
--
ALTER TABLE `grader_exam`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `grader_last_login`
--
ALTER TABLE `grader_last_login`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `grader_modules`
--
ALTER TABLE `grader_modules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `grader_student_tag`
--
ALTER TABLE `grader_student_tag`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `main_invoice`
--
ALTER TABLE `main_invoice`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `main_invoice_details`
--
ALTER TABLE `main_invoice_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `manualtos`
--
ALTER TABLE `manualtos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `manualtos_has_question`
--
ALTER TABLE `manualtos_has_question`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_type`
--
ALTER TABLE `payment_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `question`
--
ALTER TABLE `question`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `question_has_option`
--
ALTER TABLE `question_has_option`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `question_pool`
--
ALTER TABLE `question_pool`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `receipt`
--
ALTER TABLE `receipt`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `receipt_details`
--
ALTER TABLE `receipt_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `role` (`role`);

--
-- Indexes for table `role_permissions`
--
ALTER TABLE `role_permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `salutation_setup`
--
ALTER TABLE `salutation_setup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `state`
--
ALTER TABLE `state`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_exam_attempt`
--
ALTER TABLE `student_exam_attempt`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_question_set`
--
ALTER TABLE `student_question_set`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_question_set_marks`
--
ALTER TABLE `student_question_set_marks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_question_set_marks_adjustment`
--
ALTER TABLE `student_question_set_marks_adjustment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp_exam_question`
--
ALTER TABLE `temp_exam_question`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp_main_invoice_details`
--
ALTER TABLE `temp_main_invoice_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temp_receipt_details`
--
ALTER TABLE `temp_receipt_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `topic`
--
ALTER TABLE `topic`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tos`
--
ALTER TABLE `tos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tos_details`
--
ALTER TABLE `tos_details`
  ADD PRIMARY KEY (`id_details`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_last_login`
--
ALTER TABLE `user_last_login`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_last_login`
--
ALTER TABLE `admin_last_login`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `answer_status`
--
ALTER TABLE `answer_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `bank_registration`
--
ALTER TABLE `bank_registration`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bloom_taxonomy`
--
ALTER TABLE `bloom_taxonomy`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `communication_group`
--
ALTER TABLE `communication_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `communication_group_message`
--
ALTER TABLE `communication_group_message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `communication_group_message_recepients`
--
ALTER TABLE `communication_group_message_recepients`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `communication_group_recepients`
--
ALTER TABLE `communication_group_recepients`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `communication_template`
--
ALTER TABLE `communication_template`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `communication_template_message`
--
ALTER TABLE `communication_template_message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `course`
--
ALTER TABLE `course`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `course_learning_objective`
--
ALTER TABLE `course_learning_objective`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `currency_rate_setup`
--
ALTER TABLE `currency_rate_setup`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `currency_setup`
--
ALTER TABLE `currency_setup`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `difficult_level`
--
ALTER TABLE `difficult_level`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `examset`
--
ALTER TABLE `examset`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `examset_questions`
--
ALTER TABLE `examset_questions`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `exam_center`
--
ALTER TABLE `exam_center`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `exam_center_has_room`
--
ALTER TABLE `exam_center_has_room`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `exam_center_location`
--
ALTER TABLE `exam_center_location`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `exam_center_start_exam`
--
ALTER TABLE `exam_center_start_exam`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `exam_event`
--
ALTER TABLE `exam_event`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `exam_has_question`
--
ALTER TABLE `exam_has_question`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `exam_has_question_count`
--
ALTER TABLE `exam_has_question_count`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `exam_module`
--
ALTER TABLE `exam_module`
  MODIFY `module_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `exam_name`
--
ALTER TABLE `exam_name`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `exam_schedule`
--
ALTER TABLE `exam_schedule`
  MODIFY `schedule_id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `exam_session`
--
ALTER TABLE `exam_session`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `exam_student_tagging`
--
ALTER TABLE `exam_student_tagging`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `exam_users`
--
ALTER TABLE `exam_users`
  MODIFY `user_id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `exam_venue`
--
ALTER TABLE `exam_venue`
  MODIFY `venue_id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fee_category`
--
ALTER TABLE `fee_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fee_setup`
--
ALTER TABLE `fee_setup`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `financial_account_code`
--
ALTER TABLE `financial_account_code`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `grade`
--
ALTER TABLE `grade`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `grader`
--
ALTER TABLE `grader`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `grader_exam`
--
ALTER TABLE `grader_exam`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `grader_last_login`
--
ALTER TABLE `grader_last_login`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `grader_modules`
--
ALTER TABLE `grader_modules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `grader_student_tag`
--
ALTER TABLE `grader_student_tag`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `main_invoice`
--
ALTER TABLE `main_invoice`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `main_invoice_details`
--
ALTER TABLE `main_invoice_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `manualtos`
--
ALTER TABLE `manualtos`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `manualtos_has_question`
--
ALTER TABLE `manualtos_has_question`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `payment_type`
--
ALTER TABLE `payment_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92;

--
-- AUTO_INCREMENT for table `question`
--
ALTER TABLE `question`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=80;

--
-- AUTO_INCREMENT for table `question_has_option`
--
ALTER TABLE `question_has_option`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=362;

--
-- AUTO_INCREMENT for table `question_pool`
--
ALTER TABLE `question_pool`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `receipt`
--
ALTER TABLE `receipt`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `receipt_details`
--
ALTER TABLE `receipt_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `role_permissions`
--
ALTER TABLE `role_permissions`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=557;

--
-- AUTO_INCREMENT for table `salutation_setup`
--
ALTER TABLE `salutation_setup`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `state`
--
ALTER TABLE `state`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `student`
--
ALTER TABLE `student`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `student_exam_attempt`
--
ALTER TABLE `student_exam_attempt`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `student_question_set`
--
ALTER TABLE `student_question_set`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT for table `student_question_set_marks`
--
ALTER TABLE `student_question_set_marks`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `student_question_set_marks_adjustment`
--
ALTER TABLE `student_question_set_marks_adjustment`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `temp_exam_question`
--
ALTER TABLE `temp_exam_question`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `temp_main_invoice_details`
--
ALTER TABLE `temp_main_invoice_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `temp_receipt_details`
--
ALTER TABLE `temp_receipt_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `topic`
--
ALTER TABLE `topic`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `tos`
--
ALTER TABLE `tos`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tos_details`
--
ALTER TABLE `tos_details`
  MODIFY `id_details` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user_last_login`
--
ALTER TABLE `user_last_login`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=126;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
